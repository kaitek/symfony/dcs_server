<?php
namespace App\Controller;

use App\Entity\ReworkType;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReworkTypeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:ReworkType';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/ReworkType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ReworkType-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ReworkType/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="ReworkType-getComboValues", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesReworktype(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = ''){
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val," and finish is null ");
    }

    public function getNewEntity() {
        return new ReworkType();
    }

    public function getQBQuery() {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('rt.id,rt.code,rt.description,rt.start,rt.finish'
                . ',rt.version')
                ->from('App:ReworkType', 'rt')
                ->where('rt.deleteuserId is null')
                ->orderBy('rt.code', 'ASC');
        $queries['ReworkType'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ReworkType/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ReworkType-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ReworkType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ReworkType-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ReworkType", name="ReworkType-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/ReworkType.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ReworkType/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ReworkType-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ReworkType', $id);

            return $this->render('Modules/ReworkType.html.twig', $data);
        } else {
            return $cbg;
        }
    }
    
    /**
     * @Route(path="/ReworkType/{id}", requirements={"id": "\d+"}, name="ReworkType-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ReworkType', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ReworkType/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ReworkType-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

}