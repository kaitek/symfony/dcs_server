<?php

namespace App\Controller;

use App\Entity\Mould;
use App\Entity\MouldGroup;
use App\Entity\MouldDetail;
use App\Entity\ClientMouldDetail;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MouldController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:Mould';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/Mould/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Mould-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entityM = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        $cbd = $this->checkBeforeDelete($request, $id, $entityM, $v);
        if ($cbd === true) {
            $user = $this->getUser();
            $userId = $user->getId();
            $entityM->setDeleteuserId($userId);
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $em->persist($entityM);
                $em->flush();
                $em->remove($entityM);
                $em->flush();
                //grup kayıtları tespit ediliyor
                $qb_mouldg=$this->getDoctrine()
                    ->getRepository('App:MouldGroup')
                    ->findBy(array('mould' => $entityM->getCode()));
                if(count($qb_mouldg)!==0){
                    foreach($qb_mouldg as $rowg){
                        //grubun detay kayıtları siliniyor
                        $qb_mouldd=$this->getDoctrine()
                            ->getRepository('App:MouldDetail')
                            ->findBy(array('mould' => $entityM->getCode(), 'mouldgroup' => $rowg->getCode()));
                        if(count($qb_mouldd)!==0){
                            foreach($qb_mouldd as $rowd){
                                $rowd->setDeleteuserId($userId);
                                $em->persist($rowd);
                                $em->flush();
                                $em->remove($rowd);
                                $em->flush();
                            }
                        }
                        $rowg->setDeleteuserId($userId);
                        $em->persist($rowg);
                        $em->flush();
                        $em->remove($rowg);
                        $em->flush();
                    }
                }
                //throw new \Exception('deneme'); 
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();

                // Foreign key violation mesajı mı?
                $msg = $e->getMessage();

                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbd;
        }
    }

    public function getNewEntity()
    {
        return new Mould();
    }

    public function getQBQuery()
    {
        return array();
    }

    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT Mould.* 
        FROM (
            select m.* 
            ,(select string_agg(mg.opgroups, ',') from (
                select string_agg(concat(mg.code,':',(select string_agg(opname,'|') from mould_details md where md.mould=mg.mould and md.mouldgroup=mg.code and md.delete_user_id is null)),',')opgroups
                from mould_groups mg 
                where mg.mould=m.code and mg.delete_user_id is null 
                group by mg.mould,mg.code)mg
            ) opnames
            from moulds m
            where m.delete_user_id is null ) Mould
        WHERE 1=1 @@where@@ 
        ORDER BY Mould.code ASC";
        $queries['Mould'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/Mould/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Mould-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba=$this->checkBeforeAdd($request);
        if($cba===true){
            $entityM = new Mould();
            $this->setEntityWithRequest($entityM, $request);
            $user = $this->getUser();
            $userId = $user->getId();
            $validator = ($this->_container==null?$this->container:$this->_container)->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entityM));
            if ($errors!==false)
                return $errors;
            $groups=$this->_requestData->groups;
            foreach($groups as $group){
                $entityMG = new MouldGroup();
                $entityMG->setCode($group->code);
                $entityMG->setMould($entityM->getCode());
                $entityMG->setProductionmultiplier($group->productionmultiplier);
                $entityMG->setIntervalmultiplier($group->intervalmultiplier);
                $entityMG->setCounter(0);
                $entityMG->setSetup($group->setup);
                $entityMG->setCycletime($group->cycletime);
                $entityMG->setLotcount($group->lotcount);
                $entityMG->setProgram((isset($group->program)?$group->program:null));
                $entityMG->setStart($entityM->getStart());
                $entityMG->setCreateuserId($userId);
                $errors = $this->getValidateMessage($validator->validate($entityMG));
                if ($errors!==false)
                    return $errors;
                $details=$group->_details;
                foreach($details as $detail){
                    $arr=explode('-',$detail->opname);
                    $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                    $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                    $entityMD = new MouldDetail();
                    $entityMD->setMould($entityM->getCode());
                    $entityMD->setMouldgroup($group->code);
                    $entityMD->setOpcode($opcode);
                    $entityMD->setOpnumber($opnumber);
                    $entityMD->setOpname($detail->opname);
                    $entityMD->setLeafmask($detail->leafmask);
                    $entityMD->setLeafmaskcurrent($detail->leafmaskcurrent);
                    $entityMD->setStart($entityM->getStart());
                    $entityMD->setCreateuserId($userId);
                    $errors = $this->getValidateMessage($validator->validate($entityMD));
                    if ($errors!==false)
                        return $errors;
                }
            }
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $qb_mould = $em->createQueryBuilder();
            $qb_mould = $qb_mould->select('m.id')
                ->from('App:Mould', 'm')
                ->where('m.code=:code or m.serialnumber=:serialnumber')
                ->setParameters(array('code' => $this->_requestData->code, 'serialnumber' => $this->_requestData->serialnumber));
            $tl=$qb_mould->getQuery()->getArrayResult();
            if(count($tl)===0){
                $conn = $em->getConnection();
                $conn->beginTransaction();
                try {
                    $em->persist($entityM);
                    $em->flush();
                    foreach($groups as $group){
                        $entityMG = new MouldGroup();
                        $qb_mouldg = $em->createQueryBuilder();
                        $qb_mouldg = $qb_mouldg->select('mg.id')
                            ->from('App:MouldGroup', 'mg')
                            ->where('mg.code=:code and mg.mould=:mould')
                            ->setParameters(array('code' => $group->code, 'mould' => $entityM->getCode()));
                        $tlg=$qb_mouldg->getQuery()->getArrayResult();
                        if(count($tlg)!==0){
                            throw new \Error(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Mould.errRecordGroup', array(), 'Mould'));
                        }
                        $entityMG->setCreateuserId($userId);
                        $entityMG->setCode($group->code);
                        $entityMG->setMould($entityM->getCode());
                        $entityMG->setProductionmultiplier($group->productionmultiplier);
                        $entityMG->setIntervalmultiplier($group->intervalmultiplier);
                        $entityMG->setCounter(0);
                        $entityMG->setSetup($group->setup);
                        $entityMG->setCycletime($group->cycletime);
                        $entityMG->setLotcount($group->lotcount);
                        $entityMG->setProgram((isset($group->program)?$group->program:null));
                        $entityMG->setStart($entityM->getStart());
                        $errors = $this->getValidateMessage($validator->validate($entityMG));
                        $em->persist($entityMG);
                        $em->flush();
                        $details=$group->_details;
                        foreach($details as $detail){
                            $arr=explode('-',$detail->opname);
                            $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                            $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                            $entityMD = new MouldDetail();
                            $qb_mouldd = $em->createQueryBuilder();
                            $qb_mouldd = $qb_mouldd->select('md.id')
                                ->from('App:MouldDetail', 'md')
                                ->where('md.mould=:mould and md.mouldgroup=:mouldgroup and md.opname=:opname')
                                ->setParameters(array('mould' => $entityM->getCode(), 'mouldgroup' => $group->code,'opname'=>$detail->opname));
                            $tld=$qb_mouldd->getQuery()->getArrayResult();
                            if(count($tld)!==0){
                                throw new \Error(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Mould.errRecordDetail', array(), 'Mould'));
                            }
                            $entityMD->setCreateuserId($userId);
                            $entityMD->setMould($entityM->getCode());
                            $entityMD->setMouldgroup($group->code);
                            $entityMD->setOpcode($opcode);
                            $entityMD->setOpnumber($opnumber);
                            $entityMD->setOpname($detail->opname);
                            $entityMD->setLeafmask($detail->leafmask);
                            $entityMD->setLeafmaskcurrent($detail->leafmaskcurrent);
                            $entityMD->setStart($entityM->getStart());
                            $em->persist($entityMD);
                            $em->flush();
                        }
                    }
                    $conn->commit();
                } catch (\Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                } catch (\Error $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
                if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                    return $this->showAllAction($request, $_locale, $pg, $lm);
                } else {
                    return $this->msgSuccess();
                }
            }else{
                return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Mould.errRecordAdd', array(), 'Mould') );
            }
        } else {
            return $cba;
        }
    }

    /**
     * @Route(path="/Mould/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Mould-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entityM = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbu=$this->checkBeforeUpdate($request,$id,$entityM,$v);
        if($cbu===true){
            $this->setEntityWithRequest($entityM, $request);
            $user = $this->getUser();
            $userId = $user->getId();
            $validator = ($this->_container==null?$this->container:$this->_container)->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entityM));
            if ($errors!==false)
                return $errors;
            $groups=$this->_requestData->groups;
            foreach($groups as $group){
                $entityMG = new MouldGroup();
                $entityMG->setCode($group->code);
                $entityMG->setMould($entityM->getCode());
                $entityMG->setProductionmultiplier($group->productionmultiplier);
                $entityMG->setIntervalmultiplier($group->intervalmultiplier);
                $entityMG->setCounter(0);
                $entityMG->setSetup($group->setup);
                $entityMG->setCycletime($group->cycletime);
                $entityMG->setLotcount($group->lotcount);
                $entityMG->setProgram((isset($group->program)?$group->program:null));
                $entityMG->setStart($entityM->getStart());
                $entityMG->setCreateuserId($userId);
                $errors = $this->getValidateMessage($validator->validate($entityMG));
                if ($errors!==false)
                    return $errors;
                $details=$group->_details;
                foreach($details as $detail){
                    $arr=explode('-',$detail->opname);
                    $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                    $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                    $entityMD = new MouldDetail();
                    $entityMD->setMould($entityM->getCode());
                    $entityMD->setMouldgroup($group->code);
                    $entityMD->setOpcode($opcode);
                    $entityMD->setOpnumber($opnumber);
                    $entityMD->setOpname($detail->opname);
                    $entityMD->setLeafmask($detail->leafmask);
                    $entityMD->setLeafmaskcurrent($detail->leafmaskcurrent);
                    $entityMD->setStart($entityM->getStart());
                    $entityMD->setCreateuserId($userId);
                    $errors = $this->getValidateMessage($validator->validate($entityMD));
                    if ($errors!==false)
                        return $errors;
                }
            }
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $qb_mould = $em->createQueryBuilder();
            $qb_mould = $qb_mould->select('m.id')
                ->from('App:Mould', 'm')
                ->where('(m.code=:code or m.serialnumber=:serialnumber) and m.id<>:id')
                ->setParameters(array('code' => $this->_requestData->code, 'serialnumber' => $this->_requestData->serialnumber,'id'=>$id));
            $tl=$qb_mould->getQuery()->getArrayResult();
            if(count($tl)===0){
                $conn = $em->getConnection();
                $conn->beginTransaction();
                try {
                    $em->persist($entityM);
                    $em->flush();
                    $arr_g=[];
                    foreach($groups as $group){
                        $arr_g[]=$group->code;
                        $qb_mouldg = $em->createQueryBuilder();
                        if(isset($group->id)&&$group->id!==''&&$group->id!==null){
                            $entityMG = $this->getDoctrine()
                                ->getRepository('App:MouldGroup')
                                ->find($group->id);
                            $qb_mouldg = $qb_mouldg->select('mg.id')
                                ->from('App:MouldGroup', 'mg')
                                ->where('mg.code=:code and mg.mould=:mould and mg.id<>:id')
                                ->setParameters(array('code' => $group->code, 'mould' => $entityM->getCode(),'id'=>$group->id));
                            $entityMG->setUpdateuserId($userId);
                            //güncellenenen grubun client_mould_details kayıtları güncelleniyor
                            $sql_g="select * from client_mould_details where finish is null and mouldgroup=:mouldgroup";
                            $stmt_g = $conn->prepare($sql_g);
                            $stmt_g->bindValue('mouldgroup', $entityMG->getCode());
                            $stmt_g->execute();
                            $records=$stmt_g->fetchAll();
                            foreach($records as $record){
                                $entityCMD = $this->getDoctrine()
                                ->getRepository('App:ClientMouldDetail')
                                ->find($record["id"]);
                                if($entityCMD){
                                    $entityCMD->setMouldgroup($group->code);
                                    $em->persist($entityCMD);
                                    $em->flush();
                                }
                            }
                        }else{
                            $entityMG = new MouldGroup();
                            $qb_mouldg = $qb_mouldg->select('mg.id')
                                ->from('App:MouldGroup', 'mg')
                                ->where('mg.code=:code and mg.mould=:mould')
                                ->setParameters(array('code' => $group->code, 'mould' => $entityM->getCode()));
                            $entityMG->setCreateuserId($userId);
                        }
                        $tlg=$qb_mouldg->getQuery()->getArrayResult();
                        if(count($tlg)!==0){
                            throw new \Error(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Mould.errRecordGroup', array(), 'Mould'));
                        }
                        $entityMG->setCode($group->code);
                        $entityMG->setMould($entityM->getCode());
                        $entityMG->setProductionmultiplier($group->productionmultiplier);
                        $entityMG->setIntervalmultiplier($group->intervalmultiplier);
                        $entityMG->setSetup($group->setup);
                        $entityMG->setCycletime($group->cycletime);
                        $entityMG->setLotcount($group->lotcount);
                        $entityMG->setProgram((isset($group->program)?$group->program:null));
                        $entityMG->setStart($entityM->getStart());
                        $errors = $this->getValidateMessage($validator->validate($entityMG));
                        $em->persist($entityMG);
                        $em->flush();
                        $details=$group->_details;
                        $arr_d=[];
                        foreach($details as $detail){
                            $arr_d[]=$detail->opname;
                            $arr=explode('-',$detail->opname);
                            $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                            $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                            $qb_mouldd = $em->createQueryBuilder();
                            if(isset($detail->id)&&$detail->id!==''&&$detail->id!==null){
                                $entityMD = $this->getDoctrine()
                                    ->getRepository('App:MouldDetail')
                                    ->find($detail->id);
                                $qb_mouldd = $qb_mouldd->select('md.id')
                                    ->from('App:MouldDetail', 'md')
                                    ->where('md.mould=:mould and md.mouldgroup=:mouldgroup and md.opname=:opname and md.id<>:id')
                                    ->setParameters(array('mould' => $entityM->getCode(), 'mouldgroup' => $group->code,'opname'=>$detail->opname,'id'=>$detail->id));
                                $entityMD->setUpdateuserId($userId);
                            }else{
                                $entityMD = new MouldDetail();
                                $qb_mouldd = $qb_mouldd->select('md.id')
                                    ->from('App:MouldDetail', 'md')
                                    ->where('md.mould=:mould and md.mouldgroup=:mouldgroup and md.opname=:opname')
                                    ->setParameters(array('mould' => $entityM->getCode(), 'mouldgroup' => $group->code,'opname'=>$detail->opname));
                                $entityMD->setCreateuserId($userId);
                            }
                            $tld=$qb_mouldd->getQuery()->getArrayResult();
                            if(count($tld)!==0){
                                throw new \Error(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Mould.errRecordDetail', array(), 'Mould'));
                            }
                            $entityMD->setMould($entityM->getCode());
                            $entityMD->setMouldgroup($group->code);
                            $entityMD->setOpcode($opcode);
                            $entityMD->setOpnumber($opnumber);
                            $entityMD->setOpname($detail->opname);
                            $entityMD->setLeafmask($detail->leafmask);
                            $entityMD->setLeafmaskcurrent($detail->leafmaskcurrent);
                            $entityMD->setStart($entityM->getStart());
                            $em->persist($entityMD);
                            $em->flush();
                        }
                        //silinen detay kayıtları tespit ediliyor
                        $qb_mouldd = $em->createQueryBuilder();
                        $qb_mouldd = $qb_mouldd->select('md.id')
                            ->from('App:MouldDetail', 'md')
                            ->where(count($details)>0?'md.mould=:mould and md.mouldgroup=:mouldgroup and md.opname not in (:opname)':'md.mould=:mould and md.mouldgroup=:mouldgroup')
                            ->setParameters(count($details)>0?array('mould' => $entityM->getCode(), 'mouldgroup' => $group->code,'opname'=>$arr_d):array('mould' => $entityM->getCode(), 'mouldgroup' => $group->code));
                        $tld=$qb_mouldd->getQuery()->getArrayResult();
                        if(count($tld)!==0){
                            foreach($tld as $rowd){
                                $entity_md_del = $this->getDoctrine()
                                    ->getRepository('App:MouldDetail')
                                    ->find($rowd['id']);
                                $entity_md_del->setDeleteuserId($userId);
                                $em->persist($entity_md_del);
                                $em->flush();
                                $em->remove($entity_md_del);
                                $em->flush();
                            }
                        }
                    }
                    //silinen grup kayıtları tespit ediliyor
                    $qb_mouldg = $em->createQueryBuilder();
                    $qb_mouldg = $qb_mouldg->select('mg.id,mg.code')
                        ->from('App:MouldGroup', 'mg')
                        ->where(count($groups)>0?'mg.code not in (:code) and mg.mould=:mould':'mg.mould=:mould')
                        ->setParameters(count($groups)>0?array('code' => $arr_g, 'mould' => $entityM->getCode()):array('mould' => $entityM->getCode()));
                    $tlg=$qb_mouldg->getQuery()->getArrayResult();
                    if(count($tlg)!==0){
                        foreach($tlg as $rowg){
                            //grubun detay kayıtları siliniyor
                            $qb_mouldd = $em->createQueryBuilder();
                            $qb_mouldd = $qb_mouldd->select('md.id')
                                ->from('App:MouldDetail', 'md')
                                ->where('md.mould=:mould and md.mouldgroup=:mouldgroup')
                                ->setParameters(array('mould' => $entityM->getCode(), 'mouldgroup' => $rowg['code']));
                            $tld=$qb_mouldd->getQuery()->getArrayResult();
                            if(count($tld)!==0){
                                foreach($tld as $rowd){
                                    $entity_md_del = $this->getDoctrine()
                                        ->getRepository('App:MouldDetail')
                                        ->find($rowd['id']);
                                    $entity_md_del->setDeleteuserId($userId);
                                    $em->persist($entity_md_del);
                                    $em->flush();
                                    $em->remove($entity_md_del);
                                    $em->flush();
                                }
                            }
                            $entity_mg_del = $this->getDoctrine()
                                ->getRepository('App:MouldGroup')
                                ->find($rowg['id']);
                            $entity_mg_del->setDeleteuserId($userId);
                            $em->persist($entity_mg_del);
                            $em->flush();
                            $em->remove($entity_mg_del);
                            $em->flush();
                        }
                    }
                    $conn->commit();
                } catch (\Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                } catch (\Error $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
                if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                    return $this->showAllAction($request, $_locale, $pg, $lm);
                } else {
                    return $this->msgSuccess();
                }
            }else{
                return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Mould.errRecordAdd', array(), 'Mould') );
            }
        }else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/Mould", name="Mould-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $mould_types = $this->getComboValues($request, $_locale, 1, 100, 'mould_types');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['mould_types']=json_decode($mould_types->getContent())->records;
            return $this->render('Modules/Mould.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Mould/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Mould-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Mould', $id);

            return $this->render('Modules/Mould.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Mould/{id}", requirements={"id": "\d+"}, name="Mould-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Mould', $id);
            if($records["Mould"]["totalProperty"]>0){
                $records["Mould"]["records"][0]["_groups"]=[];
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                /** @var QueryBuilder $qb */
                $qb = $em->createQueryBuilder();
                $qb = $qb->select('mg.id,mg.code,mg.mould,mg.productionmultiplier'
                    .',mg.intervalmultiplier,mg.counter,mg.setup,mg.cycletime'
                    .',mg.lotcount,mg.program,mg.start,mg.finish,mg.version')
                    ->from('App:MouldGroup', 'mg')
                    ->where('mg.deleteuserId is null and mg.finish is null ')
                    ->andWhere('mg.mould=:mould')
                    ->orderBy('mg.code', 'ASC');
                $qb=$qb->getQuery();
                $qb->setParameter('mould', $records["Mould"]["records"][0]["code"]);
                $groups=$qb->getArrayResult();
                foreach($groups as $group){
                    /** @var QueryBuilder $qbd */
                    $qbd = $em->createQueryBuilder();
                    $qbd = $qbd->select('md.id,md.mould,md.mouldgroup,md.opcode,md.opnumber'
                        .',md.opname,md.leafmask,md.leafmaskcurrent,md.start,md.finish'
                        . ',md.version')
                        ->from('App:MouldDetail', 'md')
                        ->where('md.deleteuserId is null and md.finish is null')
                        ->andWhere('md.mould=:mould')
                        ->andWhere('md.mouldgroup=:mouldgroup')
                        ->orderBy('md.opcode', 'ASC');
                    $qbd=$qbd->getQuery();
                    $qbd->setParameter('mould', $group["mould"]);
                    $qbd->setParameter('mouldgroup', $group["code"]);
                    $details=$qbd->getArrayResult();
                    $group['_details']=$details;
                    $records["Mould"]["records"][0]["_groups"][]=$group;
                }
            }
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Mould/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="Mould-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
