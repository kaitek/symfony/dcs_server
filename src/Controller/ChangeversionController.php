<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\KanbanOperation;
use App\Entity\MouldDetail;
use App\Entity\OperationAuthorization;
use App\Entity\ProductTree;

class ChangeversionController extends ControllerBase
{
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/Changeversion/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Changeversion-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $uri = strtolower($request->getBaseUrl());
        $cba=$this->checkBeforeAdd($request);
        if($cba===true) {
            $_data=json_decode($request->getContent());
            $versionold=$_data->versionold;
            $versionnew=$_data->versionnew;
            if($versionold==null||$versionold==''||$versionnew==null||$versionnew=='') {
                return $this->msgError(
                    ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $conn->beginTransaction();
            try {
                $sql_pt = 'SELECT * 
                    from product_trees 
                    where delete_user_id is null and finish is null and code=:code';
                $stmt_pt = $conn->prepare($sql_pt);
                $stmt_pt->bindValue('code', $versionnew);
                $stmt_pt->execute();
                $records_pt = $stmt_pt->fetchAll();
                if(count($records_pt)>0) {
                    throw new \Exception(
                        ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('Changeversion.errNewCodeAlreadyExists', array(), 'Changeversion')
                    );
                }
                //$sql="SELECT * from product_trees where finish is null and code = :code";
                //$stmt = $conn->prepare($sql);
                //$stmt->bindValue('code', $versionold);
                //$stmt->execute();
                //$product_trees = $stmt->fetchAll();
                $product_trees =$this->getDoctrine()
                    ->getRepository('App:ProductTree')
                    ->findBy(array('finish' => null, 'code'=>$versionold));
                foreach($product_trees as $row_pt) {
                    $entity_new = new ProductTree();
                    $entity_new->setCreateuserId($userId);
                    $entity_new->setParent($row_pt->getMaterialtype()=='M' ? null : $versionnew);
                    $entity_new->setCode($versionnew);
                    $entity_new->setNumber($row_pt->getNumber());
                    $entity_new->setDescription($row_pt->getDescription());
                    $entity_new->setMaterialtype($row_pt->getMaterialtype());
                    $entity_new->setName($versionnew.(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')||$row_pt->getMaterialtype()=='M' ? '' : '-'.$row_pt->getNumber()));
                    $entity_new->setStockcode(implode($versionnew, explode($versionold, $row_pt->getStockcode())));
                    $entity_new->setStockname(implode($versionnew, explode($versionold, $row_pt->getStockname())));
                    $entity_new->setTpp($row_pt->getTpp());
                    $entity_new->setQuantity($row_pt->getQuantity());
                    $entity_new->setUnit_quantity($row_pt->getUnit_quantity());
                    $entity_new->setOpcounter($row_pt->getOpcounter());
                    $entity_new->setPack($row_pt->getPack());
                    $entity_new->setPackcapacity($row_pt->getPackcapacity());
                    $entity_new->setUnit_packcapacity($row_pt->getUnit_packcapacity());
                    $entity_new->setOporder($row_pt->getOporder());
                    $entity_new->setPreptime($row_pt->getPreptime());
                    $entity_new->setQualitycontrolrequire($row_pt->getQualitycontrolrequire());
                    $entity_new->setTask($row_pt->getTask());
                    $entity_new->setFeeder($row_pt->getFeeder());
                    $entity_new->setCarrierfeeder($row_pt->getCarrierfeeder());
                    $entity_new->setDrainer($row_pt->getDrainer());
                    $entity_new->setCarrierdrainer($row_pt->getCarrierdrainer());
                    $entity_new->setGoodsplanner($row_pt->getGoodsplanner());
                    $entity_new->setProductionmultiplier($row_pt->getProductionmultiplier());
                    $entity_new->setIntervalmultiplier($row_pt->getIntervalmultiplier());
                    $entity_new->setStart(new \DateTime());
                    $entity_new->setDelivery($row_pt->getDelivery());
                    $entity_new->setLocationsource($row_pt->getLocationsource());
                    $entity_new->setLocationdestination($row_pt->getLocationdestination());
                    $entity_new->setEmpcount($row_pt->getEmpcount());
                    $entity_new->setIsdefault($row_pt->getIsdefault());
                    $entity_new->setClient($row_pt->getClient());
                    $em->persist($entity_new);
                    /* @var $repo ProductTree */
                    $repo = $this->getDoctrine()
                        ->getRepository('App:ProductTree');
                    $entity_1=$repo->find($row_pt->getId());
                    if($entity_1!==null) {
                        $entity_1->setFinish(new \DateTime());
                        $em->persist($entity_1);
                        $em->flush();
                        $em->clear();
                    }
                    $em->flush();
                    $em->clear();
                    if($row_pt->getMaterialtype()=='O') {
                        $mould_details =$this->getDoctrine()
                            ->getRepository('App:MouldDetail')
                            ->findBy(array('finish' => null, 'opname'=>$row_pt->getName()));
                        foreach($mould_details as $row_md) {
                            $row_md->setOpcode($versionnew);
                            $row_md->setOpname($versionnew.(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149') ? '' : '-'.$row_pt->getNumber()));
                            $em->persist($row_md);
                        }
                        $em->flush();
                        $em->clear();
                        $control_questions =$this->getDoctrine()
                            ->getRepository('App:ControlQuestion')
                            ->findBy(array('opname'=>$row_pt->getName()));
                        foreach($control_questions as $row_cq) {
                            $row_cq->setOpname($versionnew.(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149') ? '' : '-'.$row_pt->getNumber()));
                            $em->persist($row_cq);
                        }
                        $em->flush();
                        $em->clear();
                        $operation_authorizations =$this->getDoctrine()
                            ->getRepository('App:OperationAuthorization')
                            ->findBy(array('opname'=>$row_pt->getName()));
                        foreach($operation_authorizations as $row_oa) {
                            $row_oa->setOpname($versionnew.(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149') ? '' : '-'.$row_pt->getNumber()));
                            $em->persist($row_oa);
                        }
                        $em->flush();
                        $em->clear();
                    }
                }
                $kanban_operations =$this->getDoctrine()
                    ->getRepository('App:KanbanOperation')
                    ->findBy(array('product'=>$versionold));
                foreach($kanban_operations as $row_ko) {
                    $row_ko->setProduct($versionnew);
                    $em->persist($row_ko);
                }
                $em->flush();
                $em->clear();
                $conn->commit();
                return $this->msgSuccess();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                $mtitle='Hata';
                $resp = array(
                    'title' => $mtitle,
                    'success' => false,
                    'message' => $e->getMessage()." \n",
                    'file' => $e->getfile()."-".$e->getLine()
                );
                //$resp['arr_err']=json_encode($_arr_err);
                $statusCode=400;
                return new JsonResponse($resp, $statusCode);
            }

        } else {
            return $cba;
        }
    }

    /**
     * @Route(path="/Changeversion", name="Changeversion-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            //json_decode($clients->getContent())->records
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0, //$this instanceof BaseAuditControllerInterface,
                'data' => array('clients'=>json_decode($clients->getContent())->records),
                'extras' => array()
            );

            return $this->render('Modules/Changeversion.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Changeversion/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="Changeversion-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

}
