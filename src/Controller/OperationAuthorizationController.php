<?php

namespace App\Controller;

use App\Entity\ClientParam;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OperationAuthorizationController extends ControllerBase
{
    public const ENTITY = 'App:ClientParam';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/OperationAuthorization", name="OperationAuthorization-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $r=new \StdClass();
            $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->findBy(array('name' => 'task_OperationAuthorization','value'=>'true' ));
            $r->status=($entity ? 'OPEN' : 'CLOSE');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0, //$this instanceof BaseAuditControllerInterface,
                'data' => array('OperationAuthorization'=>array("totalProperty"=>1,"records"=>array($r))),
                'extras' => array()
            );

            return $this->render('Modules/OperationAuthorization.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/OperationAuthorization/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="OperationAuthorization-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $data = $this->clearLookup(json_decode($request->getContent()));
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->findBy(array('name' => 'task_OperationAuthorization','value'=>($data->status=='OPEN' ? 'false' : 'true') ));
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, -1);
        if ($cbu===true) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $sql_6="select * from client_params where name=:name and value=:valueold";
                $stmt_6 = $conn->prepare($sql_6);
                $stmt_6->bindValue('name', 'task_OperationAuthorization');
                $stmt_6->bindValue('valueold', ($data->status=='OPEN' ? 'false' : 'true'));
                $stmt_6->execute();
                $records_6 = $stmt_6->fetchAll();
                $d = new \DateTime();
                $_d=$d->format("Y-m-d H:i:s.u");
                foreach ($records_6 as $row) {
                    /* @var $repo ClientParam */
                    $repo = $this->getDoctrine()
                        ->getRepository('App:ClientParam');
                    $entity_1=$repo->find($row["id"]);
                    if ($entity_1!==null) {
                        $entity_1->setValue(($data->status=='OPEN' ? 'true' : 'false'));
                        $em->persist($entity_1);
                        $em->flush();
                        $em->clear();
                    }
                    $_data=array("tableName"=>'client_params',"procType"=>'update',"id"=>null,"record_id"=>null,"data"=>array("client"=>$row["client"],"name"=>"task_OperationAuthorization","value"=>($data->status=='OPEN' ? 'true' : 'false')));
                    //$conn->insert('_sync_messages', array('ip'=>($row["client"].'_server'),'uuid' => '', 'ack' => 'true','queued'=>0, 'type' => 'sync_message', 'data' => json_encode($_data), '"createdAt"' => $_d, '"updatedAt"' => $_d));
                    //if ($_SERVER['HTTP_HOST']=="192.168.1.10"||$_SERVER['HTTP_HOST']=="192.168.1.40"||$_SERVER['HTTP_HOST']=="127.0.0.1") {
                    $conn->insert('__sync_messages', array('queuename'=>($row["client"].'_server'), 'priority'=>999, 'messagetype' => 'sync_message', 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data["data"]),'info'=>json_encode(array("tableName"=>"client_params")) ));
                    //}
                }
                $sql_update="update client_params set value=:valuenew where name=:name and value=:valueold";
                $stmt_update = $conn->prepare($sql_update);
                $stmt_update->bindValue('valuenew', ($data->status=='OPEN' ? 'true' : 'false'));
                $stmt_update->bindValue('name', 'task_OperationAuthorization');
                $stmt_update->bindValue('valueold', ($data->status=='OPEN' ? 'false' : 'true'));
                $stmt_update->execute();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            return $this->msgSuccess();
        } else {
            $message=($this->_container==null ? $this->container : $this->_container)->get('translator')->trans(($data->status=='OPEN' ? 'OperationAuthorization.errAlreadyOpen' : 'OperationAuthorization.errAlreadyClose'), array(), 'OperationAuthorization');
            return $this->msgError($message);
        }
    }
}
