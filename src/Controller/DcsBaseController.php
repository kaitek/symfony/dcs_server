<?php

namespace App\Controller;

use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class DcsBaseController extends ControllerBase
{
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    public function efficiencyCalculate($_day)
    {
        if ($_day instanceof DateTime || gettype($_day) === 'object') {
            $_day = $_day->format('Y-m-d');
        }
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $conn->beginTransaction();
        try {
            //$sql_del_c = "delete from clients where code=''";
            //$stmt = $conn->prepare($sql_del_c);
            //$stmt->execute();

            $sql_del_eld = 'delete from employee_lost_details where day=:day';
            $stmt = $conn->prepare($sql_del_eld);
            $stmt->bindValue('day', $_day);
            $stmt->execute();

            $sql_del_ee = 'delete from employee_efficiency where day=:day';
            $stmt = $conn->prepare($sql_del_ee);
            $stmt->bindValue('day', $_day);
            $stmt->execute();

            $sql_ins_eld = "insert into employee_lost_details (day,jobrotation,week,client,employee,start,finish,duration,status,ismandatory,isrework,description,teamleader)
            SELECT cld.day,cld.jobrotation,EXTRACT(WEEK FROM cld.day)week,cld.client,cld.employee,cld.start,cld.finish
                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) duration
                ,COALESCE(cld.losttype,'') status,case when lgd.lostgroupcode='zorunlu' then true else false end ismandatory
                ,case when c.workflow='El İşçiliği' then true else false end isrework
                ,'' description,e.teamleader
            from client_lost_details cld 
            left join clients c on c.code=cld.client and c.workflow<>'Gölge Cihaz'
            left join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup='OPVERIM'
            left join employees e on e.code=cld.employee and e.finish is null
            where cld.type in ('l_e','l_e_out') and cld.finish is not null and cld.client not in (SELECT code from clients where workflow='CNC')
            and cld.employee is not null and cld.day<CURRENT_DATE and cld.day=:day ";
            if ($_SERVER['HTTP_HOST'] === '10.0.0.101') {
                $sql_ins_eld = "insert into employee_lost_details (day,jobrotation,week,client,employee,start,finish,duration,status,ismandatory,isrework,description,teamleader)
                SELECT cld.day,cld.jobrotation,EXTRACT(WEEK FROM cld.day)week,cld.client,cld.employee,cld.start,cld.finish
                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) duration
                    ,COALESCE(cld.losttype,'') status
                    ,case when lgd.lostgroupcode='zorunlu' then true else false end ismandatory
                    ,case when c.workflow='El İşçiliği' then true else false end isrework
                    ,case when exists( SELECT cpd.id
                    from client_production_details cpd 
                    where cpd.day=cld.day and cpd.employee=cld.employee and cld.finish between cpd.start and cpd.finish
                    and cpd.client not in (SELECT code from clients where workflow='CNC') ) then 'UretimIci' else 'UretimDısı' end description,e.teamleader
                from client_lost_details cld 
                left join clients c on c.code=cld.client and c.workflow<>'Gölge Cihaz'
                left join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup='OPVERIM'
                left join employees e on e.code=cld.employee and e.finish is null
                where cld.type in ('l_e','l_e_out') and cld.finish is not null and cld.client not in (SELECT code from clients where workflow='CNC')
                and cld.employee is not null and cld.day<CURRENT_DATE and cld.day=:day ";
            }
            $stmt = $conn->prepare($sql_ins_eld);
            $stmt->bindValue('day', $_day);
            $stmt->execute();
            //optime hesabındaki /count(*) askı5 benzeri üretimlerde hataya neden olduğundan kaldırıldı 2021-01-27
            $sql_ins_ee = "insert into employee_efficiency (day,jobrotation,week,employee,isovertime,fulltime,breaktime,worktime,optime,losttime
                ,reworkoptime,mreworkduration,mlosttime,rlosttime,rmlosttime,discontinuity,teamleader,worktimewithoutlost,efficiency,tempo) 
            select *,b.worktime-b.losttime-b.discontinuity worktimewithoutlost
                ,round(case when b.worktime>0 and (b.worktime-b.mlosttime-b.discontinuity)>0 then (100*(b.optime)/(b.worktime-b.mlosttime-b.discontinuity))else 0 end,2) efficiency
                ,round(case when b.worktime>0 and (b.worktime-b.mlosttime-b.discontinuity)>0 then case when b.worktime-b.mlosttime-b.losttime-b.discontinuity>0 then (100*(b.optime)/(b.worktime-b.mlosttime-b.losttime-b.discontinuity)) else 0 end else 0 end,2) tempo 
            from (	
                select a.day,a.jobrotation,a.week,a.employee,a.isovertime,a.fulltime,a.breaktime,case when a.worktime>0 then a.worktime else 0 end worktime,a.optime,a.losttime
                    ,a.reworkoptime,a.mreworkduration,a.mlosttime,a.rlosttime,a.rmlosttime,a.discontinuity,a.teamleader
                from (
                    SELECT jre.day,jre.jobrotation,jre.week,jre.employee,jre.isovertime,jre.worktime+jre.breaktime as fulltime,jre.breaktime,jre.worktime
                        ,round(COALESCE(w.optime,0),2)optime,round(COALESCE(w.reworkoptime,0),2)reworkoptime,round(COALESCE(w.mreworkduration,0),2)mreworkduration
                        ,round(COALESCE(l.losttime,0),2)losttime,round(COALESCE(l.mlosttime,0),2)mlosttime
                        ,round(COALESCE(l.rlosttime,0),2)rlosttime,round(COALESCE(l.rmlosttime,0),2)rmlosttime
                        ,case when round(COALESCE(l.discontinuity,0),2)<jre.worktime+jre.breaktime then round(COALESCE(l.discontinuity,0),2) else jre.worktime+jre.breaktime end discontinuity
                        ,e.teamleader
                    from job_rotation_employees jre 
                    left join (
                        select y.employee,y.jobrotation,round(sum(y.optime))optime
                            ,sum(y.reworkoptime)reworkoptime,sum(y.mreworkduration)mreworkduration 
                        from (
                            select aaa.employee,aaa.jobrotation,round(sum(aaa.optime)/count(*))optime
                                ,sum(aaa.reworkoptime)reworkoptime,sum(aaa.mreworkduration)mreworkduration 
                            from (
                                SELECT z.employee,z.jobrotation,round(sum(z.proctimecurrent)/60)optime
                                    ,sum(z.reworkoptime)reworkoptime,sum(z.mreworkduration)mreworkduration,z.opname
                                from (
                                    SELECT cpd.client,cpd.day,cpd.jobrotation,coalesce(cpd.mould,cpd.opname) mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent
                                        ,cpd.start,cpd.finish,cpd.employee,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime
                                        ,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber,cpd.opname
                                        ,coalesce(cpd.opsayi,0)opsayi,COALESCE(cpd.losttime,0)losttime
                                        ,concat('Ref:',cpd.opname,'|Uretim:',cast(productioncurrent as varchar(10)),'|tpp:',cast(round(coalesce(cpd.calculatedtpp,pt.tpp),2) as varchar(10))) task
                                        ,round(((case when r.ismandatory=true then cpd.productioncurrent*(case when coalesce(cpd.calculatedtpp,0)>0 then cpd.calculatedtpp else pt.tpp end) else 0 end))/60,2) reworkoptime
                                        ,round(((case when r.id is not null then cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) else 0 end))/60,2) mreworkduration
                                    from client_production_details cpd 
                                    left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' ".($_SERVER['HTTP_HOST'] == '10.10.0.10' ? " and pt.isdefault=true " : "").($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                                    left join reworks r on r.code=cpd.tasklist 
                                    where cpd.type in ('e_p','e_p_confirm','e_p_out') and cpd.day=:day and cpd.productioncurrent>0
                                    and cpd.client not in (SELECT code from clients where workflow='CNC')
                                )z
                                group by z.client,z.day,z.jobrotation,z.start,z.finish,z.employee,z.losttime,z.mould,z.opname
                            )aaa
                            group by aaa.jobrotation,aaa.employee
                        )y
                        group by y.employee,y.jobrotation
                    )w on w.employee=jre.employee and w.jobrotation=jre.jobrotation
                    left join (
                        SELECT employee,jobrotation
                            ,round(cast(100*sum(case when isrework=false and ismandatory=false and status not in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) losttime
                            ,round(cast(100*sum(case when isrework=false and ismandatory=true and status not in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) mlosttime
                            ,round(cast(100*sum(case when isrework=true and ismandatory=false then duration else 0 end)/60 as decimal(20,4))/100,2) rlosttime
                            ,round(cast(100*sum(case when isrework=true and ismandatory=true then duration else 0 end)/60 as decimal(20,4))/100,2) rmlosttime
                            ,round(cast(100*sum(case when status in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) discontinuity
                        from employee_lost_details 
                        where day=:day and status not in ('ÇAY MOLASI','YEMEK MOLASI') 
                        GROUP BY employee,jobrotation)l on l.employee=jre.employee and l.jobrotation=jre.jobrotation
                    left join employees e on e.code=jre.employee and e.finish is null
                    where jre.day=:day
                )a
            )b";
            $sql_ins_ee = "insert into employee_efficiency (day,jobrotation,week,employee,isovertime,fulltime,breaktime,worktime,optime,losttime
                ,reworkoptime,mreworkduration,mlosttime,rlosttime,rmlosttime,discontinuity,teamleader,worktimewithoutlost,efficiency,tempo) 
            select *,b.worktime-b.losttime-b.discontinuity worktimewithoutlost
                ,round(case when b.worktime>0 and (b.worktime-b.mlosttime-b.discontinuity)>0 then (100*(b.optime)/(b.worktime-b.mlosttime-b.discontinuity))else 0 end,2) efficiency
                ,round(case when b.worktime>0 and (b.worktime-b.mlosttime-b.discontinuity)>0 then case when b.worktime-b.mlosttime-b.losttime-b.discontinuity>0 then (100*(b.optime)/(b.worktime-b.mlosttime-b.losttime-b.discontinuity)) else 0 end else 0 end,2) tempo 
            from (	
                select a.day,a.jobrotation,a.week,a.employee,a.isovertime,a.fulltime,a.breaktime,case when a.worktime>0 then a.worktime else 0 end worktime,a.optime,a.losttime
                    ,a.reworkoptime,a.mreworkduration,a.mlosttime,a.rlosttime,a.rmlosttime,a.discontinuity,a.teamleader
                from (
                    SELECT jre.day,jre.jobrotation,jre.week,jre.employee,jre.isovertime,jre.worktime+jre.breaktime as fulltime,jre.breaktime,jre.worktime
                        ,round(COALESCE(w.optime,0),2)optime,round(COALESCE(w.reworkoptime,0),2)reworkoptime,round(COALESCE(w.mreworkduration,0),2)mreworkduration
                        ,round(COALESCE(l.losttime,0),2)losttime,round(COALESCE(l.mlosttime,0),2)mlosttime
                        ,round(COALESCE(l.rlosttime,0),2)rlosttime,round(COALESCE(l.rmlosttime,0),2)rmlosttime
                        ,case when round(COALESCE(l.discontinuity,0),2)<jre.worktime+jre.breaktime then round(COALESCE(l.discontinuity,0),2) else jre.worktime+jre.breaktime end discontinuity
                        ,e.teamleader
                    from job_rotation_employees jre 
                    left join (
                        select y.employee,y.jobrotation,round(sum(y.optime))optime
                            ,sum(y.reworkoptime)reworkoptime,sum(y.mreworkduration)mreworkduration 
                        from (
                            SELECT z.employee,z.jobrotation,round(sum(z.proctimecurrent)/60)optime
                                ,sum(z.reworkoptime)reworkoptime,sum(z.mreworkduration)mreworkduration
                            from (
                                SELECT cpd.client,cpd.day,cpd.jobrotation,coalesce(cpd.mould,cpd.opname) mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent
                                    ,cpd.start,cpd.finish,cpd.employee,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime
                                    ,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber
                                    ,coalesce(cpd.opsayi,0)opsayi,COALESCE(cpd.losttime,0)losttime
                                    ,concat('Ref:',cpd.opname,'|Uretim:',cast(productioncurrent as varchar(10)),'|tpp:',cast(round(coalesce(cpd.calculatedtpp,pt.tpp),2) as varchar(10))) task
                                    ,round(((case when r.ismandatory=true then cpd.productioncurrent*(case when coalesce(cpd.calculatedtpp,0)>0 then cpd.calculatedtpp else pt.tpp end) else 0 end))/60,2) reworkoptime
                                    ,round(((case when r.id is not null then cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) else 0 end))/60,2) mreworkduration
                                from client_production_details cpd 
                                left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' ".($_SERVER['HTTP_HOST'] == '10.10.0.10' ? " and pt.isdefault=true " : "").($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                                left join reworks r on r.code=cpd.tasklist 
                                where cpd.type in ('e_p','e_p_confirm','e_p_out') and cpd.day=:day and cpd.productioncurrent>0
                                and cpd.client not in (SELECT code from clients where workflow='CNC')
                            )z
                            group by z.client,z.day,z.jobrotation,z.start,z.finish,z.employee,z.losttime,z.mould
                        )y
                        group by y.employee,y.jobrotation
                    )w on w.employee=jre.employee and w.jobrotation=jre.jobrotation
                    left join (
                        SELECT employee,jobrotation
                            ,round(cast(100*sum(case when isrework=false and ismandatory=false and status not in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) losttime
                            ,round(cast(100*sum(case when isrework=false and ismandatory=true and status not in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) mlosttime
                            ,round(cast(100*sum(case when isrework=true and ismandatory=false then duration else 0 end)/60 as decimal(20,4))/100,2) rlosttime
                            ,round(cast(100*sum(case when isrework=true and ismandatory=true then duration else 0 end)/60 as decimal(20,4))/100,2) rmlosttime
                            ,round(cast(100*sum(case when status in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) discontinuity
                        from employee_lost_details 
                        where day=:day and status not in ('ÇAY MOLASI','YEMEK MOLASI') 
                        GROUP BY employee,jobrotation)l on l.employee=jre.employee and l.jobrotation=jre.jobrotation
                    left join employees e on e.code=jre.employee and e.finish is null
                    where jre.day=:day
                )a
            )b";
            if ($_SERVER['HTTP_HOST'] === '10.0.0.101') {
                $sql_ins_ee = "insert into employee_efficiency (day,jobrotation,week,employee,isovertime,fulltime,breaktime,worktime,optime,losttime
                    ,reworkoptime,mreworkduration,mlosttime,rlosttime,rmlosttime,discontinuity,teamleader,worktimewithoutlost,efficiency,tempo) 
                select *,b.optime worktimewithoutlost,100*b.optime/(b.fulltime-b.breaktime) efficiency,0 tempo
                from (	
                    select a.day,a.jobrotation,a.week,a.employee,a.isovertime,a.fulltime,a.breaktime,case when a.worktime>0 then a.worktime else 0 end worktime,a.optime,a.losttime
                        ,a.reworkoptime,a.mreworkduration,a.mlosttime,a.rlosttime,a.rmlosttime,a.discontinuity,a.teamleader
                    from ( 
                        SELECT jre.day,jre.jobrotation,jre.week,jre.employee,jre.isovertime,jre.worktime+jre.breaktime as fulltime,jre.breaktime,COALESCE(w.optime,0) worktime
                            ,COALESCE(w.optime,0)-round(COALESCE(l.losttimeinproduction,0),2) optime,round(COALESCE(w.reworkoptime,0),2)reworkoptime,round(COALESCE(w.mreworkduration,0),2)mreworkduration
                            ,round(COALESCE(l.losttimeoutproduction,0),2)losttime,round(COALESCE(l.mlosttimeinproduction,0),2)mlosttime
                            ,round(COALESCE(l.rlosttime,0),2)rlosttime,round(COALESCE(l.rmlosttime,0),2)rmlosttime
                            ,case when round(COALESCE(l.discontinuity,0),2)<jre.worktime+jre.breaktime then round(COALESCE(l.discontinuity,0),2) else jre.worktime+jre.breaktime end discontinuity
                            ,e.teamleader
                        from job_rotation_employees jre 
                        left join (
                            select y.employee,y.jobrotation,round(sum(y.optime))optime
                                ,sum(y.reworkoptime)reworkoptime,sum(y.mreworkduration)mreworkduration 
                            from (
                                SELECT z.employee,z.jobrotation,round(sum(z.proctimecurrent)/count(*)/60)optime
                                        ,sum(z.reworkoptime)reworkoptime,sum(z.mreworkduration)mreworkduration
                                from (
                                    select x.type,x.day,x.jobrotation,x.employee
                                        ,case when x.type in ('e_p','e_p_confirm','e_p_out') then 'ÜRETİM' when x.type in ('e_p_unconfirm') then 'ÜRETİM-Onaysız' when x.type in ('l_e','l_e_out') then 'OPERATÖR KAYIP' else 'GÖREV KAYIP' end tip
                                        ,x.client, x.opname
                                        ,case when x.type in ('e_p','e_p_confirm','e_p_out') then x.productioncurrent else null end productioncurrent
                                        ,x.scrap
                                        ,case when x.type in ('e_p','e_p_confirm','e_p_out') then null else x.losttype end losttype
                                        ,x.start,x.finish,x.suresn proctimecurrent,round(x.suresn/60)suredk,x.calculatedtpp,x.reworkoptime,x.mreworkduration,x.mould
                                    from (	
                                        SELECT cpd.type,cpd.client,cpd.day,cpd.jobrotation,cpd.employee,cpd.opname,cpd.productioncurrent,''losttype,cpd.start,cpd.finish,cpd.calculatedtpp 
                                            ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                            ,coalesce((SELECT sum(trd.quantityreject) 
                                                from task_reject_details trd 
                                                where trd.type='e_r' and trd.client=cpd.client and trd.day=cpd.day and trd.jobrotation=cpd.jobrotation and trd.employee=cpd.employee and trd.erprefnumber=cpd.erprefnumber),0)scrap
                                            ,round(((case when r.ismandatory=true then cpd.productioncurrent*(case when coalesce(cpd.calculatedtpp,0)>0 then cpd.calculatedtpp else 1 end) else 0 end))/60,2) reworkoptime
                                            ,round(((case when r.id is not null then cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) else 0 end))/60,2) mreworkduration,coalesce(cpd.mould,cpd.opname) mould
                                        from client_production_details cpd 
                                        left join reworks r on r.code=cpd.tasklist 
                                        where cpd.day=:day
                                        and cpd.client not in (SELECT code from clients where workflow='CNC')
                                        union all
                                        SELECT case when cld.losttype in ('DENEMELER','NUMUNE URETIM','ROBOT KAYNAK PROGRAM YAZMA','TEK TARAFLI BURÇ ÇAKMA') then 'e_p' else cld.type end as \"type\"
                                            ,cld.client,cld.day,cld.jobrotation,cld.employee,cld.opname,0 productioncurrent,cld.losttype,cld.start,cld.finish,0 calculatedtpp
                                            ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                            ,null scrap,0 reworkoptime,0 mreworkduration,'' mould
                                        from client_lost_details cld 
                                        where cld.type='l_e' and cld.day=:day
                                        and cld.client not in (SELECT code from clients where workflow='CNC')
                                    )x
                                    where x.suresn>0 and x.type='e_p'
                                )z
                                group by z.client,z.day,z.jobrotation,z.start,z.finish,z.employee,z.mould
                            )y
                            group by y.employee,y.jobrotation
                        )w on w.employee=jre.employee and w.jobrotation=jre.jobrotation
                        left join (
                            SELECT employee,jobrotation
                                ,round(cast(100*sum(case when isrework=false and ismandatory=false and status not in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) losttime
                                ,round(cast(100*sum(case when isrework=false and ismandatory=true and status not in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) mlosttime
                                ,round(cast(100*sum(case when isrework=true and ismandatory=false then duration else 0 end)/60 as decimal(20,4))/100,2) rlosttime
                                ,round(cast(100*sum(case when isrework=true and ismandatory=true then duration else 0 end)/60 as decimal(20,4))/100,2) rmlosttime
                                ,round(cast(100*sum(case when status in ('İZİN','RAPOR') then duration else 0 end)/60 as decimal(20,4))/100,2) discontinuity
                                ,round(cast(100*sum(case when description='UretimIci' and ismandatory=true then duration else 0 end)/60 as decimal(20,4))/100,2) mlosttimeinproduction
                                ,round(cast(100*sum(case when description='UretimIci' then duration else 0 end)/60 as decimal(20,4))/100,2) losttimeinproduction
                                ,round(cast(100*sum(case when description='UretimDısı' then duration else 0 end)/60 as decimal(20,4))/100,2) losttimeoutproduction
                            from employee_lost_details 
                            where day=:day
                            GROUP BY employee,jobrotation)l on l.employee=jre.employee and l.jobrotation=jre.jobrotation
                        left join employees e on e.code=jre.employee and e.finish is null
                        where jre.day=:day
                    )a
                )b";
            }
            $stmt = $conn->prepare($sql_ins_ee);
            $stmt->bindValue('day', $_day);
            $stmt->execute();
            $conn->commit();
            return $this->msgSuccess("");
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            $conn->rollback();
            return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('EmployeeEfficiencyCalculate.errRecordAdd', array(), 'EmployeeEfficiencyCalculate'));
        }
    }

    public function getSqlLost($_shortlost, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $fieldname_opname)
    {
        $wh1 = '';
        $wh1_p = '';
        if ($_jobrotation != '' && $_jobrotation != null) {
            $wh1 = ' and cld.jobrotation=\''.$_jobrotation.'\' ';
            $wh1_p = ' and cpd.jobrotation=\''.$_jobrotation.'\' ';
        }
        $sql_j = '';
        $sql_j_p = '';
        if ($_jobrotationteam != '' && $_jobrotationteam != null) {
            $sql_j = " join (
                SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam 
                from job_rotation_employees jre
                join employees e on e.code=jre.employee
                where jre.day between '".$_s."' and '".$_f."' and e.jobrotationteam='".$_jobrotationteam."'
                GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
            ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            $sql_j_p = " join (
                SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam 
                from job_rotation_employees jre
                join employees e on e.code=jre.employee
                where jre.day between '".$_s."' and '".$_f."' and e.jobrotationteam='".$_jobrotationteam."'
                GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
            ) jobrotationteam on cpd.day=jobrotationteam.day and cpd.jobrotation=jobrotationteam.jobrotation ";
        }
        $sql_emp = " ,'' as employee ";
        if (($_SERVER['HTTP_HOST'] == '10.10.0.10')) {
            $sql_emp = ",coalesce(
                (
                SELECT string_agg(distinct concat(e.code,'-',e.name),',') employee 
                from client_lost_details cld1 
                left join employees e on e.code=cld1.employee
                where cld1.type='l_e_t' and cld1.client=z.client and cld1.day=z.day and cld1.jobrotation=z.jobrotation and cld1.start>=z.start and cld1.finish<=z.finish
                ),'')as employee";
        }
        $sql = "select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
            ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
            ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null)) then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
            ,a.task
            ,a.employee
            ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=".$_shortlost." and lt.isshortlost=true) then 'KISA DURUS' else '' end info
            ,a.opsayi,a.descriptionlost
            ,a.faultdevice,a.ftdescription,a.faulttype
            ,cgd.clientgroupcode
        from (
            select z.client,z.day,z.jobrotation
                ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=".(60 * (int)$_shortlost)." and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                    when z.losttype='' and z.suresn>".(60 * (int)$_shortlost)." and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                    else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                ,z.descriptionlost
                ,z.faultdevice,z.faulttype
                ,coalesce((SELECT string_agg(distinct ".$fieldname_opname.",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                ".$sql_emp."
                ,z.endval,z.beginval,z.ftdescription
            from (
                select losts.client,losts.day,losts.jobrotation,losts.losttype,losts.start,losts.finish
                    ,losts.suresn,losts.endval,losts.beginval,max(losts.opsayi)opsayi,max(losts.refsayi)refsayi
                    ,max(losts.faultdevice)faultdevice,max(losts.ftdescription)ftdescription,max(losts.faulttype)faulttype 
                    ,max(losts.descriptionlost)descriptionlost
                from (
                    SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                        ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                            +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                        ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                        ,replace(cld.descriptionlost,'null','')descriptionlost 
                        ,cld.faultdevice,ft.description ftdescription,cld.faulttype 
                    from client_lost_details cld 
                    left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                    left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                    left join fault_types ft on ft.code=cld.faulttype 
                        and case cld.faultgroupcode 
                            when 'ARIZA KALIP-APARAT' then 'Kalıp-Fikstür-Aparat'
                            when 'ARIZA MAKİNE ELEKTRİK' then 'İstasyon Elektrik'
                            when 'ARIZA MAKİNE MEKANİK' then 'İstasyon Mekanik'
                            when 'ARIZA ÜRETİM' then 'Üretim' end=ft.faultgroupcode
                    ".$sql_j."
                    where cld.type='l_c' and cld.day between '".$_s."' and '".$_f."' 
                        and not EXISTS(SELECT * from client_lost_details where client=cld.client and day=cld.day and jobrotation=cld.jobrotation and type='l_c_t' and start>=cld.start and finish<=cld.finish)
                        and cld.client not in (SELECT code from clients where workflow='CNC')
                        and cld.client in ('".implode("','", explode(',', $_cl))."') 
                        ".$wh1."
                    union all
                    SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                        ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                            +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                        ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                        ,replace(max(cld.descriptionlost),'null','')descriptionlost 
                        ,cld.faultdevice,ft.description ftdescription,cld.faulttype 
                    from client_lost_details cld 
                    left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                    left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                    left join product_trees pt on pt.name=cld.opname and pt.materialtype='O' and pt.finish is null ".($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cld.client)=cld.client " : "")."
                    left join fault_types ft on ft.code=cld.faulttype 
                        and case cld.faultgroupcode 
                            when 'ARIZA KALIP-APARAT' then 'Kalıp-Fikstür-Aparat'
                            when 'ARIZA MAKİNE ELEKTRİK' then 'İstasyon Elektrik'
                            when 'ARIZA MAKİNE MEKANİK' then 'İstasyon Mekanik'
                            when 'ARIZA ÜRETİM' then 'Üretim' end=ft.faultgroupcode
                    ".$sql_j."
                    where cld.type='l_c_t' and cld.day between '".$_s."' and '".$_f."' 
                        and cld.client not in (SELECT code from clients where workflow='CNC')
                        and cld.client in ('".implode("','", explode(',', $_cl))."') 
                        and coalesce(pt.name,'') not like 'DENEME%'
                        ".$wh1."
                    group by cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish,jr.endval,jrs.beginval
                        ,cld.opsayi,cld.refsayi,cld.faultdevice,ft.description,cld.faulttype
                )losts
                group by losts.client,losts.day,losts.jobrotation,losts.losttype
                    ,losts.start,losts.finish,losts.suresn,losts.endval,losts.beginval
                union all
			    SELECT cpd.client,cpd.day,cpd.jobrotation,'DENEME' losttype,cpd.start,cpd.finish
					,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                        +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
					,coalesce(cpd.opsayi,0)opsayi,coalesce(cpd.refsayi,0)refsayi
					,'DENEME ÜRETİMİ' descriptionlost 
					,'' faultdevice,'' ftdescription,'' faulttype 
                from client_production_details cpd 
                left join job_rotations jr on substr(cast(cpd.finish as varchar),12,5)=jr.endval
                left join job_rotations jrs on substr(cast(cpd.start as varchar),12,5)=jrs.beginval
                left join product_trees pt on pt.name=cpd.opname and pt.materialtype='O' and pt.finish is null ".($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                ".$sql_j_p."
                where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>=0 
					and cpd.client not in (SELECT code from clients where workflow='CNC')
					and cpd.day between '".$_s."' and '".$_f."' and cpd.client in ('".implode("','", explode(',', $_cl))."') and pt.name like 'DENEME%'
                ".$wh1_p."
            )z
        )a
        left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
        left join lost_types lt on lt.code=a.losttype and lt.finish is null
        left join client_group_details cgd on cgd.client=a.client and cgd.clientgroup='watch'
        where a.suresn>0 ";
        return $sql;
    }

    public function getSqlProd($uri, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $fieldname_opname, $prod_concat_type)
    {
        $isLaser = strpos($_cl, 'LT0');
        $wh2 = '';
        if ($_jobrotation != '' && $_jobrotation != null) {
            $wh2 = ' and cpd.jobrotation=\''.$_jobrotation.'\' ';
        }
        $sql_j = '';
        if ($_jobrotationteam != '' && $_jobrotationteam != null) {
            $sql_j = " join (
                SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                from job_rotation_employees jre
                join employees e on e.code=jre.employee
                where jre.day between '".$_s."' and '".$_f."' and e.jobrotationteam='".$_jobrotationteam."'
                GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
            ) jobrotationteam on cpd.day=jobrotationteam.day and cpd.jobrotation=jobrotationteam.jobrotation ";
        }
        $sql = "select x.client,x.day,x.jobrotation,x.start,min(x.finish)finish,sum(x.suresn)suresn,sum(x.losttime)losttime
            ,string_agg(distinct x.mould,',') mould
            ,string_agg(distinct x.mouldgroup,',') mouldgroup
            ,string_agg(x.production,',') production
            ,string_agg(x.productioncurrent,',') productioncurrent
            ,string_agg(distinct x.opname,',') opname
            ,string_agg(distinct x.erprefnumber,',') erprefnumber
            ,round(sum(x.proctime))proctime,round(sum(x.proctimecurrent))proctimecurrent
            ,sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)sss
            ,case when sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0 then sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent) else 0 end slowdown
            ,string_agg(distinct concat(x.task,'|Tpp:',round(x.calculatedtpp/x.operasyonsayisi,2)),',') task
            ,round(min(x.calculatedtpp/x.operasyonsayisi),2)calculatedtpp
            ,x.operasyonsayisi,x.kalipsayisi
            ,max(cgd.clientgroupcode)clientgroupcode
        from (
            SELECT z.client,z.day,z.jobrotation,z.start,max(z.finish)finish,round(max(z.suresn)/(z.operasyonsayisi*z.kalipsayisi)) suresn,z.mould,z.mouldgroup
                ,string_agg(cast(z.production as varchar(10)),',') production
                ,string_agg(cast(z.productioncurrent as varchar(10)),',') productioncurrent
                ,string_agg(distinct z.opname,',') opname
                ,string_agg(distinct z.erprefnumber,',') erprefnumber
                ,round(sum(z.proctime)/z.operasyonsayisi)proctime
                ,round(sum(z.proctimecurrent)/z.operasyonsayisi)proctimecurrent
                ,string_agg(distinct z.task,',') task
                ,round(avg(z.calculatedtpp),2) calculatedtpp
                ,z.operasyonsayisi,z.kalipsayisi
                ,round(cast((SELECT sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) )losttimex
                from client_lost_details cld 
                where type='l_c' and day=z.day and client=z.client and start>=z.start and finish<=max(z.finish) and losttype not in ('YETKİNLİK GÖZLEM'))/(z.operasyonsayisi*z.kalipsayisi) as decimal),2) losttime
            from (
                SELECT cpd.id,cpd.client,cpd.day,cpd.jobrotation,coalesce(cpd.mould,'')mould,coalesce(cpd.mouldgroup,'')mouldgroup,cpd.production,cpd.productioncurrent,cpd.start,cpd.finish,cpd.opname
                    ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                    ,coalesce(cpd.calculatedtpp,pt.tpp)calculatedtpp
                    ,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber
                    ,coalesce(cpd.opsayi,0)opsayi
                    ,concat(".($prod_concat_type == 'all' ?
                        "'Ref:',".($fieldname_opname === 'opdescription' ?
                            'case when opdescription is null then pt.description else opdescription end'
                            : $fieldname_opname).",'|Uretim:',cast(productioncurrent as varchar(10))"
                        : "".$fieldname_opname."").") task
                    
                    ,".(($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? "1" : "coalesce(nullif((
                        select count(*) from (
                            select client,day,jobrotation,mould,mouldgroup,opname,start
                            from client_production_details 
                            where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type and coalesce(mouldgroup,'')=coalesce(cpd.mouldgroup,'')
                            and client not in (SELECT code from clients where workflow='CNC')
                            GROUP BY client,day,jobrotation,mould,mouldgroup,opname,start
                        )aaa
                    ),0),1)")." operasyonsayisi
                    ,coalesce(nullif((
                        select count(*) from (
                        select mould
                        from client_production_details 
                        where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type 
                        and client not in (SELECT code from clients where workflow='CNC')
                        group by mould)m
                    ),0),1) kalipsayisi
                from client_production_details cpd 
                left join product_trees pt on pt.name=cpd.opname and pt.materialtype='O' and pt.finish is null ".($_SERVER['HTTP_HOST'] == '10.10.0.10' ? " and pt.isdefault=true " : "").($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                ".$sql_j."
                where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>0 
                    and cpd.client not in (SELECT code from clients where workflow='CNC')
                    and cpd.day between '".$_s."' and '".$_f."' and cpd.client in ('".implode("','", explode(',', $_cl))."') 
                    and coalesce(pt.name,'') not like 'DENEME%'
                    ".$wh2."
            )z
            group by z.client,z.day,z.jobrotation,z.start,z.mould,z.mouldgroup".((($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') || $isLaser !== false) ? "" : ",z.opname").",z.operasyonsayisi,z.kalipsayisi
        )x
        left join client_group_details cgd on cgd.client=x.client and cgd.clientgroup='watch'
        group by x.client,x.day,x.jobrotation,x.mould,x.mouldgroup,x.opname,x.start,x.operasyonsayisi,x.kalipsayisi
        order by x.start";
        if ($uri == '/kaynak1' || $uri == '/kaynak2' || $uri == '/kaynak3' || $uri == '/mekanik') {
            //üretimler çoklu satır
            $sql = "select x.client,x.day,x.jobrotation,x.start,min(x.finish)finish,sum(x.suresn)suresn,sum(x.losttime)losttime
                ,string_agg(distinct x.mould,',') mould
                ,string_agg(distinct x.mouldgroup,',') mouldgroup
                ,string_agg(x.production,',') production
                ,string_agg(x.productioncurrent,',') productioncurrent
                ,string_agg(distinct x.opname,',') opname
                ,string_agg(distinct x.erprefnumber,',') erprefnumber
                ,round(sum(x.proctime))proctime,round(sum(x.proctimecurrent))proctimecurrent
                ,sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)sss
                ,case when sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0 then sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent) else 0 end slowdown
                ,string_agg(distinct concat(x.task,'|Tpp:',round(x.calculatedtpp,2)),',') task
                ,round(min(x.calculatedtpp),2)calculatedtpp
                ,x.operasyonsayisi,x.kalipsayisi
            from (
                SELECT z.client,z.day,z.jobrotation,z.start,max(z.finish)finish,round(max(z.suresn)/(z.operasyonsayisi)) suresn,z.mould,z.mouldgroup
                    ,string_agg(cast(z.production as varchar(10)),',') production
                    ,string_agg(cast(z.productioncurrent as varchar(10)),',') productioncurrent
                    ,string_agg(distinct z.opname,',') opname
                    ,string_agg(distinct z.erprefnumber,',') erprefnumber
                    ,round(sum(z.proctime))proctime
                    ,round(sum(z.proctimecurrent))proctimecurrent
                    ,string_agg(distinct z.task,',') task
                    ,round(avg(z.calculatedtpp),2) calculatedtpp
                    ,z.operasyonsayisi,z.kalipsayisi
                    ,round(cast((SELECT sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) )losttimex
                    from client_lost_details cld 
                    where type='l_c' and day=z.day and client=z.client and start>=z.start and finish<=max(z.finish) and losttype not in ('YETKİNLİK GÖZLEM'))/(z.operasyonsayisi) as decimal),2) losttime
                from (
                    SELECT cpd.id,cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent,cpd.start,cpd.finish,cpd.opname
                        ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                        ,coalesce(cpd.calculatedtpp,pt.tpp)calculatedtpp
                        ,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber
                        ,coalesce(cpd.opsayi,0)opsayi
                        ,concat(".($prod_concat_type == 'all' ?
                            "'Ref:',".($fieldname_opname === 'opdescription' ?
                                'case when opdescription is null then pt.description else opdescription end'
                                : $fieldname_opname).",'|Uretim:',cast(productioncurrent as varchar(10))"
                            : "".$fieldname_opname."").") task
                        
                        ,".(($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? "1" : "coalesce(nullif((
                            select count(*)
                            from client_production_details 
                            where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type 
                            and client not in (SELECT code from clients where workflow='CNC')
                        ),0),1)")."operasyonsayisi
                        ,coalesce(nullif((
                            select count(*) from (
                            select mould
                            from client_production_details 
                            where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type 
                            and client not in (SELECT code from clients where workflow='CNC')
                            group by mould)m
                        ),0),1) kalipsayisi
                    from client_production_details cpd 
                    left join product_trees pt on pt.name=cpd.opname and pt.materialtype='O' and pt.finish is null ".($_SERVER['HTTP_HOST'] == '10.10.0.10' ? " and pt.isdefault=true " : "").($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                    ".$sql_j."
                    where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>0 
                        and cpd.client not in (SELECT code from clients where workflow='CNC')
                        and cpd.day between '".$_s."' and '".$_f."' and cpd.client in ('".implode("','", explode(',', $_cl))."') ".$wh2."
                )z
                group by z.client,z.day,z.jobrotation,z.start,z.mould,z.mouldgroup".(($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? "" : ",z.opname").",z.operasyonsayisi,z.kalipsayisi
            )x
            group by x.client,x.day,x.jobrotation,x.mould,x.mouldgroup,x.opname,x.start,x.operasyonsayisi,x.kalipsayisi
            order by x.start";
        }
        return $sql;
    }

    public function getSqlReject($_s, $_f, $_cl, $_jobrotation, $_jobrotationteam)
    {
        $wh3 = '';
        if ($_jobrotation != '' && $_jobrotation != null) {
            $wh3 = ' and trd.jobrotation=\''.$_jobrotation.'\' ';
        }
        $sql_j = '';
        if ($_jobrotationteam != '' && $_jobrotationteam != null) {
            $sql_j = " join (
                SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                from job_rotation_employees jre
                join employees e on e.code=jre.employee
                where jre.day between '".$_s."' and '".$_f."' and e.jobrotationteam='".$_jobrotationteam."'
                GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
            ) jobrotationteam on trd.day=jobrotationteam.day and trd.jobrotation=jobrotationteam.jobrotation ";
        }
        $sql = "
        SELECT trd.client,trd.day,trd.jobrotation,trd.time,trd.erprefnumber,trd.opname,trd.rejecttype,trd.quantityreject,pt.tpp
            ,trd.quantityreject*pt.tpp iskartasure
            ,trd.importfilename,trd.rejectlot
            ,cgd.clientgroupcode
        from task_reject_details trd 
        join product_trees pt on pt.name=trd.opname and pt.materialtype='O' and pt.finish is null ".($_SERVER['HTTP_HOST'] == '10.10.0.10' || $_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,trd.client)=trd.client " : "")."
        left join client_group_details cgd on cgd.client=trd.client and cgd.clientgroup='watch'
        ".$sql_j."
        where trd.type='t_r' and trd.day between '".$_s."' and '".$_f."' and trd.client in('".implode("','", explode(',', $_cl))."') ".$wh3."
        order by trd.time
        ";
        // ".($_SERVER['HTTP_HOST']=='10.10.0.10' ? " and pt.isdefault=true " : "")."
        return $sql;
    }

    public function getSqlMissingClients($_s, $_f, $_cl, $_jobrotation, $_jobrotationteam)
    {
        $sql = "
        select * from (
            SELECT c.code client,jr.code jobrotation,beginval,endval
                ,cast(days.sday as varchar(10)) \"day\" 
                ,concat(cast(case when beginval>endval then days.sday - INTERVAL '1 day' else days.sday end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                ,concat(cast(case when beginval>endval then days.sday  else days.sday end as varchar(10)),' ',endval,':59')::timestamp jr_end
            from job_rotations jr
            join (select cast(st.day+(sm.day1*-1*(interval '1 day')) as date) sday
                from (select CURRENT_DATE as day) st
                join (select s.t as day1 from generate_series(0,365) as s(t)) sm on 1=1) days on 1=1
            join clients c on 1=1 and c.workflow not in ('El İşçiliği','Gölge Cihaz','Kataforez') and c.code in('".implode("','", explode(',', $_cl))."')
            left join client_lost_details cld on cld.client=c.code and cld.day=days.sday and cld.jobrotation=jr.code and cld.type='l_c' and cld.finish is not null
            where (jr.finish is null) and days.sday between '".$_s."' and '".$_f."' and cld.id is null
        )jrs
        order by jrs.jr_start,jrs.client asc
        ";
        return $sql;
    }

    public function oeeCalculate($_shortlost, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $fieldname_opname, $prod_concat_type, $uri)
    {
        $ondalikhane = 4;
        $ondalikhanegoster = 1;
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        if ($_jobrotationteam === null) {
            $date1 = new \DateTime($_s);
            $date2 = new \DateTime($_f);
            $gun = $date2->diff($date1)->format("%a");
            $sure = 24;
            if ($_jobrotation !== null) {
                $arr_sure = explode('-', preg_replace('/:/', '', $_jobrotation));
                $arr_sure[0] = intval($arr_sure[0]);
                $arr_sure[1] = intval($arr_sure[1]);
                if ($arr_sure[0] > $arr_sure[1]) {
                    $arr_sure[1] += 2400;
                }
                $sure = round(($arr_sure[1] - $arr_sure[0]) / 100);
            }
            $_cks = round((intval($gun) + 1) * $sure, $ondalikhane);
        } else {
            $sql_sure = "select sum(aa.suredk)/60 sure 
            from (
                SELECT (jre.worktime+jre.breaktime) suredk
                from job_rotation_employees jre
                join employees e on e.code=jre.employee
                where jre.day between '".$_s."' and '".$_f."' and e.jobrotationteam='".$_jobrotationteam."'
                GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam,jre.worktime,jre.breaktime)aa";
            $stmt_sure = $conn->prepare($sql_sure);
            $stmt_sure->execute();
            $records_sure = $stmt_sure->fetchAll();
            $_cks = round($records_sure[0]['sure'], $ondalikhane);
        }
        $_toplamsureler = array(
            "C_K_S" => array("toplam" => $_cks,"header" => "ÇKS","title" => "Toplam Süre")
            ,"C_S" => array("toplam" => 0,"header" => "ÇS","title" => "Üretime Kapalı Zaman","data" => array())
            ,"C_N_S" => array("toplam" => 0,"header" => "ÇNS","title" => "Planlı Duruşlar","data" => array())
            ,"I_S" => array("toplam" => 0,"header" => "İS","title" => "Kullanılabilirlik","data" => array())
            ,"P_C_S" => array("toplam" => 0,"header" => "PÇS","title" => "Performans","data" => array())
            ,"E_C_S" => array("toplam" => 0,"header" => "EÇS","title" => "Kalitesiz Üretim","data" => array("iskarta" => array("title" => 'Iskarta',"val" => 0)))
        );
        $sql_oee_grup = "select lostgroup,lostgroupcode
            from lost_group_details 
            where lostgroup like 'OEE%' and delete_user_id is null
            GROUP BY lostgroup,lostgroupcode
            order by case 
                when lostgroup='OEE-performans' then 1 
                when lostgroup='OEE-kullanilabilirlik' then 2 
                when lostgroup='OEE-planlidurus' then 3 
                when lostgroup='OEE-uretimekapali' then 4 
                else 5 end,lostgroupcode";

        $stmt = $conn->prepare($sql_oee_grup);
        $stmt->execute();
        $records = $stmt->fetchAll();
        foreach ($records as $row => $data) {
            $val = $this->replace_tr($data['lostgroupcode']);
            switch ($data['lostgroup']) {
                case 'OEE-performans':
                    $_toplamsureler['P_C_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-kullanilabilirlik':
                    $_toplamsureler['I_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-planlidurus':
                    $_toplamsureler['C_N_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-uretimekapali':
                    $_toplamsureler['C_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-kalitesizlik':
                    $_toplamsureler['E_C_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
            }
        }
        $_toplamsureler['P_C_S']['data']['HIZ KAYBI'] = array("title" => "Hız Kaybı","val" => 0);
        $_toplamsureler['P_C_S']['data']['KISA DURUS'] = array("title" => "KISA DURUŞ","val" => 0);
        $_toplamsureler['I_S']['data']['KISA DURUS'] = array("title" => "KISA DURUŞ","val" => 0);
        $clients = array();
        //kayıplar
        $sql = "
        select b.client,b.lostgroup,b.lostgroupcode,b.losttype,b.info,sum(b.suresn)suresn from (
            ".$this->getSqlLost($_shortlost, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $fieldname_opname)."
        )b
        group by b.client,b.lostgroup,b.lostgroupcode,b.losttype,b.info
        order by b.client,case when b.lostgroup='OEE-uretimekapali' then 1 when b.lostgroup='OEE-planlidurus' then 2 when b.lostgroup='OEE-kullanilabilirlik' then 3 else 4 end,b.lostgroupcode,b.losttype,b.info
        ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        foreach ($records as $item => $row) {
            if (!isset($clients[$row['client']])) {
                $clients[$row['client']] = $_toplamsureler;
            }
            $main_item = null;
            if ($row['lostgroup'] != null && $row['lostgroup'] != '') {
                switch ($row['lostgroup']) {
                    case 'OEE-performans':
                        $main_item = 'P_C_S';
                        break;
                    case 'OEE-kullanilabilirlik':
                        $main_item = 'I_S';
                        break;
                    case 'OEE-planlidurus':
                        $main_item = 'C_N_S';
                        break;
                    case 'OEE-uretimekapali':
                        $main_item = 'C_S';
                        break;
                    case 'OEE-kalitesizlik':
                        $main_item = 'E_C_S';
                        break;
                }
                $lgc = $this->replace_tr($row['lostgroupcode']);

                if ($row["info"] == "KISA DURUS") {
                    $clients[$row['client']][$main_item]['data']["KISA DURUS"]["val"] += $row["suresn"];
                } else {
                    //if(!isset($_toplamsureler[$row["lostgroup"]]["data"][$row["kayiptipi"]])){
                    //    $_toplamsureler[$row["lostgroup"]]["data"][$row["kayiptipi"]]=0;
                    //}
                    $clients[$row['client']][$main_item]['data'][$lgc]["title"] = $row['lostgroupcode'];
                    $clients[$row['client']][$main_item]['data'][$lgc]["val"] += $row["suresn"];
                }
            }
        }
        //$clients[$row['client']]["sql"]=$sql;
        foreach ($clients as $row => $item) {
            foreach ($clients[$row] as $key => $val) {
                if (isset($clients[$row][$key]["data"]) && count($clients[$row][$key]["data"]) > 0) {
                    foreach ($clients[$row][$key]["data"] as $_r => $_i) {
                        $clients[$row][$key]["data"][$_r]["val"] = round($_i["val"] / 3600, $ondalikhane);
                        $clients[$row][$key]["toplam"] += round($_i["val"] / 3600, $ondalikhane);
                    }
                }
            }
            $clients[$row]["_cks"] = $_cks;
            $clients[$row]["_cs"] = $clients[$row]["_cks"] - $clients[$row]["C_S"]["toplam"];
            $clients[$row]["_cns"] = $clients[$row]["_cs"] - $clients[$row]["C_N_S"]["toplam"];
            $clients[$row]["_is"] = $clients[$row]["_cns"] - $clients[$row]["I_S"]["toplam"];
            //$clients[$row]["_pcs"]=$_pcs;
            //$clients[$row]["_ecs"]=$_ecs;
        }
        //üretim süreleri
        $sql = "
        select y.client,round(sum(y.proctime))proctime,round(sum(y.proctimecurrent))proctimecurrent,round(sum(y.slowdown))slowdown 
        from (
            ".$this->getSqlProd($uri, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $fieldname_opname, $prod_concat_type)."
        )y
        group by y.client
        ";
        //echo $sql;
        //die;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        foreach ($records as $item => $row) {
            //hız kaybı hesaplanması
            //$hiz_kaybi=$clients[$row['client']]["_is"]-$clients[$row['client']]["P_C_S"]["toplam"]-round($row['proctimecurrent']/3600,$ondalikhane);
            //$hiz_kaybi=$hiz_kaybi<0?0:$hiz_kaybi;
            $clients[$row['client']]['P_C_S']['data']['HIZ KAYBI']["val"] = round($row['slowdown'] / 3600, $ondalikhane);
            $clients[$row['client']]['P_C_S']['toplam'] += round($row['slowdown'] / 3600, $ondalikhane);
        }
        //ıskarta süreleri
        $sql = "
        select a.client,sum(a.iskartasure)iskartasure from (
            ".$this->getSqlReject($_s, $_f, $_cl, $_jobrotation, $_jobrotationteam)."
        )a
        group by a.client
        ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        foreach ($records as $item => $row) {
            $clients[$row['client']]['E_C_S']['data']['iskarta']["val"] = round($row["iskartasure"] / 3600, $ondalikhane);
            $clients[$row['client']]['E_C_S']['toplam'] = round($row["iskartasure"] / 3600, $ondalikhane);
        }
        foreach ($clients as $row => $client) {
            $clients[$row]["_pcs"] = $clients[$row]["_is"] - $clients[$row]["P_C_S"]["toplam"];
            $clients[$row]["_ecs"] = $clients[$row]["_pcs"] - $clients[$row]['E_C_S']['toplam'];

            $clients[$row]["kayiptoplam_ur_kul"] = $clients[$row]["I_S"]["toplam"];
            $clients[$row]["kayiptoplam_ur_per"] = $clients[$row]["P_C_S"]["toplam"];
            $clients[$row]["kayiptoplam_ur_kal"] = $clients[$row]['E_C_S']['toplam'];
            $clients[$row]["kayiptoplam_fab_kul"] = $clients[$row]["kayiptoplam_ur_kul"] + $clients[$row]["C_N_S"]["toplam"];
            $clients[$row]["kayiptoplam_fab_per"] = $clients[$row]["P_C_S"]["toplam"];
            $clients[$row]["kayiptoplam_fab_kal"] = $clients[$row]['E_C_S']['toplam'];
            $clients[$row]["kayiptoplam_gen_kul"] = $clients[$row]["kayiptoplam_fab_kul"] + $clients[$row]["C_S"]["toplam"];
            $clients[$row]["kayiptoplam_gen_per"] = $clients[$row]["P_C_S"]["toplam"];
            $clients[$row]["kayiptoplam_gen_kal"] = $clients[$row]['E_C_S']['toplam'];
            $clients[$row]["oee_ur"] = round(100 * ($clients[$row]['_cns'] > 0 ? ($clients[$row]['_ecs'] / $clients[$row]['_cns']) : 1), $ondalikhane);
            $clients[$row]["oee_fab"] = round(100 * ($clients[$row]['_cs'] > 0 ? ($clients[$row]['_ecs'] / $clients[$row]['_cs']) : 1), $ondalikhane);
            $clients[$row]["oee_gen"] = round(100 * ($clients[$row]['_cks'] > 0 ? ($clients[$row]['_ecs'] / $clients[$row]['_cks']) : 1), $ondalikhane);

            $clients[$row]["oee_carpan_kul_ur"] = round((100 * ($clients[$row]["_cns"] > 0 ? ($clients[$row]["_is"] / $clients[$row]["_cns"]) : 1)), $ondalikhanegoster);
            $clients[$row]["oee_carpan_kul_fab"] = round((100 * ($clients[$row]["_cs"] > 0 ? ($clients[$row]["_is"] / $clients[$row]["_cs"]) : 1)), $ondalikhanegoster);
            $clients[$row]["oee_carpan_kul_gen"] = round((100 * ($clients[$row]["_cks"] > 0 ? ($clients[$row]["_is"] / $clients[$row]["_cks"]) : 1)), $ondalikhanegoster);

            $clients[$row]["oee_carpan_per_ur"] = round((100 * ($clients[$row]["_is"] > 0 ? ($clients[$row]["_pcs"] / $clients[$row]["_is"]) : 1)), $ondalikhanegoster);
            $clients[$row]["oee_carpan_per_fab"] = round((100 * ($clients[$row]["_is"] > 0 ? ($clients[$row]["_pcs"] / $clients[$row]["_is"]) : 1)), $ondalikhanegoster);
            $clients[$row]["oee_carpan_per_gen"] = round((100 * ($clients[$row]["_is"] > 0 ? ($clients[$row]["_pcs"] / $clients[$row]["_is"]) : 1)), $ondalikhanegoster);

            $clients[$row]["oee_carpan_kal_ur"] = round((100 * ($clients[$row]["_pcs"] > 0 ? ($clients[$row]["_ecs"] / $clients[$row]["_pcs"]) : 1)), $ondalikhanegoster);
            $clients[$row]["oee_carpan_kal_fab"] = round((100 * ($clients[$row]["_pcs"] > 0 ? ($clients[$row]["_ecs"] / $clients[$row]["_pcs"]) : 1)), $ondalikhanegoster);
            $clients[$row]["oee_carpan_kal_gen"] = round((100 * ($clients[$row]["_pcs"] > 0 ? ($clients[$row]["_ecs"] / $clients[$row]["_pcs"]) : 1)), $ondalikhanegoster);

            $clients[$row]["oee_kayip_kul_ur"] = round((100 - $clients[$row]["oee_carpan_kul_ur"]), $ondalikhanegoster);
            $clients[$row]["oee_kayip_kul_fab"] = round((100 - $clients[$row]["oee_carpan_kul_fab"]), $ondalikhanegoster);
            $clients[$row]["oee_kayip_kul_gen"] = round((100 - $clients[$row]["oee_carpan_kul_gen"]), $ondalikhanegoster);

            $clients[$row]["oee_kayip_per_ur"] = round((100 - $clients[$row]["oee_carpan_per_ur"]), $ondalikhanegoster);
            $clients[$row]["oee_kayip_per_fab"] = round((100 - $clients[$row]["oee_carpan_per_fab"]), $ondalikhanegoster);
            $clients[$row]["oee_kayip_per_gen"] = round((100 - $clients[$row]["oee_carpan_per_gen"]), $ondalikhanegoster);

            $clients[$row]["oee_kayip_kal_ur"] = round((100 - $clients[$row]["oee_carpan_kal_ur"]), $ondalikhanegoster);
            $clients[$row]["oee_kayip_kal_fab"] = round((100 - $clients[$row]["oee_carpan_kal_fab"]), $ondalikhanegoster);
            $clients[$row]["oee_kayip_kal_gen"] = round((100 - $clients[$row]["oee_carpan_kal_gen"]), $ondalikhanegoster);

            $clients[$row]["oee_kayip_ur"] = $clients[$row]["oee_kayip_kul_ur"] + $clients[$row]["oee_kayip_per_ur"] + $clients[$row]["oee_kayip_kal_ur"];
            $clients[$row]["oee_kayip_fab"] = $clients[$row]["oee_kayip_kul_fab"] + $clients[$row]["oee_kayip_per_fab"] + $clients[$row]["oee_kayip_kal_fab"];
            $clients[$row]["oee_kayip_gen"] = $clients[$row]["oee_kayip_kul_gen"] + $clients[$row]["oee_kayip_per_gen"] + $clients[$row]["oee_kayip_kal_gen"];

            $clients[$row]["kayiptoplam_ur"] = $clients[$row]["kayiptoplam_ur_kul"] + $clients[$row]["kayiptoplam_ur_per"] + $clients[$row]["kayiptoplam_ur_kal"];
            $clients[$row]["kayiptoplam_fab"] = $clients[$row]["kayiptoplam_fab_kul"] + $clients[$row]["kayiptoplam_fab_per"] + $clients[$row]["kayiptoplam_fab_kal"];
            $clients[$row]["kayiptoplam_gen"] = $clients[$row]["kayiptoplam_gen_kul"] + $clients[$row]["kayiptoplam_gen_per"] + $clients[$row]["kayiptoplam_gen_kal"];
        }
        return $clients;
    }
}
