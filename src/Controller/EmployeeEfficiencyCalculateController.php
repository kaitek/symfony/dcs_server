<?php

namespace App\Controller;

use App\Entity\EmployeeEfficiencyCalculate;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmployeeEfficiencyCalculateController extends DcsBaseController
{
    CONST ENTITY = 'App:EmployeeEfficiency';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    public function getNewEntity()
    {
        return new EmployeeEfficiencyCalculate();
    }

    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT COALESCE(max(day),cast(concat(cast(current_date as varchar(4)),'-01-01') as date)) as \"start\" from employee_efficiency ";
        $queries['EmployeeEfficiencyCalculate'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/EmployeeEfficiencyCalculate/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="EmployeeEfficiencyCalculate-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $this->_requestData = json_decode($request->getContent());
        if($this->_requestData == null){
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);
        $_day=$this->_requestData->finish;
        return $this->efficiencyCalculate($_day);
    }

    /**
     * @Route(path="/EmployeeEfficiencyCalculate", name="EmployeeEfficiencyCalculate-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            //$data = $this->getBackendData($request, $_locale, self::ENTITY);
            $records=array();
            $queries=$this->getSqlStr();
            foreach($queries as $key=>$row){
                if($row["getAll"]==true){
                    $rsql=$row["sql"];
                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();
                    $stmt = $conn->prepare($rsql);
                    $stmt->execute();
                    $records[$key]=array("totalProperty"=>1,"records"=>$stmt->fetchAll());
                }
            }
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0, //$this instanceof BaseAuditControllerInterface,
                'data' => array('EmployeeEfficiencyCalculate'=>($records['EmployeeEfficiencyCalculate'])),
                'extras' => array()
            );
            return $this->render('Modules/EmployeeEfficiencyCalculate.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/EmployeeEfficiencyCalculate/{id}", requirements={"id": "\d+"}, name="EmployeeEfficiencyCalculate-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'EmployeeEfficiencyCalculate', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/EmployeeEfficiencyCalculate/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="EmployeeEfficiencyCalculate-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

}
