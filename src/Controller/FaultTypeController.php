<?php

namespace App\Controller;

use App\Entity\FaultType;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FaultTypeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:FaultType';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/FaultType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="FaultType-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new FaultType();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('ft.id,ft.code,ft.description,ft.client,ft.faultgroupcode,ft.start,ft.finish,ft.version')
                ->from('App:FaultType', 'ft')
                ->where('ft.deleteuserId is null')
                ->orderBy('ft.code', 'ASC');
        $queries['FaultType'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/FaultType/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="FaultType-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/FaultType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="FaultType-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/FaultType", name="FaultType-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/FaultType.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultType/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="FaultType-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'FaultType', $id);

            return $this->render('Modules/FaultType.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultType/{id}", requirements={"id": "\d+"}, name="FaultType-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'FaultType', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultType/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="FaultType-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
