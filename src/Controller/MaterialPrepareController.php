<?php

namespace App\Controller;

use App\Entity\ClientParam;
use App\Entity\TaskCase;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DateTime;

class MaterialPrepareController extends ControllerBase
{
    public $customer;
    public $boskasasure;
    public $boshammaddekasasuresn;
    public $boshammaddekasalokasyon;
    public $kapalioturumisbosaltmasure;
    public $arrnodeinfo;
    public $urtmamb;
    public $muayenealani;
    public $mamulambari;
    public $ymamulambari;
    public $user_mh;
    public $exportpath;
    public $importpathnode;
    public $pc;
    public $stoklar;
    public $conn_prod;
    public $conn_ozlm;
    public $resp;
    public const ENTITY = 'App:ClientParam';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->pc = ($_SERVER['HTTP_HOST']);
        $this->arrnodeinfo = array();
        $this->stoklar = array();
        $this->resp = array();
    }
    /**
     * @Route(path="/MaterialPrepare/DeviceData/{id}",requirements={"id": "\d+"}, name="MaterialPrepare-DeviceData", options={"expose"=true}, methods={"GET"})
     */
    public function getDeviceDataAction(Request $request, $_locale, $id)
    {
        $statusCode = 200;
        $message = 'OK';
        $mtitle = 'Hata';
        $resp = array(
            'title' => $mtitle,
            'success' => true,
            'message' => $message
        );
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = "SELECT * from carriers where id=:id";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id', $id);
        $stmt->execute();
        $records = $stmt->fetchAll();
        if (count($records) > 0) {
            $carrier = $records[0];
            $resp['name'] = $carrier['code'];
            if ($carrier['code'] !== '' && $carrier['workflow'] !== '') {
            } else {
                $resp['message'] = 'Cihaz boşta, tanımlama yapılması bekleniyor!';
            }
        } else {
            $resp['message'] = 'Cihazın sistemde kaydı bulunamadı!';
        }
        $resp['deviceId'] = $id;
        return new JsonResponse($resp, $statusCode);
    }

    /**
     * @Route(path="/MaterialPrepare/PrepareData", name="MaterialPrepare-PrepareData", options={"expose"=true}, methods={"GET"})
     */
    public function getPrepareDataAction(Request $request, $_locale)
    {
        $dbg = 0;
        $d = new \DateTime();
        $time_start = microtime();
        $this->customer = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_customer');
        $this->boskasasure = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_boskasasure');
        $this->boshammaddekasasuresn = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_boshammaddekasasuresn');
        $this->boshammaddekasalokasyon = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_boshammaddekasalokasyon');
        $this->kapalioturumisbosaltmasure = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_kapalioturumisbosaltmasure');
        $this->urtmamb = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_urtmamb');
        $this->muayenealani = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_muayenealani');
        $this->mamulambari = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_mamulambari');
        $this->ymamulambari = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_ymamulambari');
        $this->user_mh = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_user_mh');
        $this->exportpath = ($this->_container == null ? $this->container : $this->_container)->getParameter('mh_exportpath');
        $this->importpathnode = ($this->_container == null ? $this->container : $this->_container)->getParameter('verimotRT_path');
        $statusCode = 200;
        $this->resp = array(
            'time' => date('d.m.Y H:i:s'),
            'message' => '',
            'errors' => array()
        );
        if ($this->pc != "192.168.1.40" && $this->customer == 'ERMETAL') {
            try {
                //$this->conn_prod = odbc_connect("ermprod_live", 'sysprogress', '123');
                $this->conn_prod = odbc_connect("adp_mfg", 'sysprogress', 'GL*kh2_a');
            } catch (Exception $e) {
                $this->resp['errors'][] = $e->getMessage();
            }
            try {
                //$this->conn_ozlm = odbc_connect("ermozlm_live", 'sysprogress', '123');
                $this->conn_ozlm = odbc_connect("adp_ext", 'sysprogress', 'GL*kh2_a');
            } catch (Exception $e) {
                $this->resp['errors'][] = $e->getMessage();
            }
            if (is_bool($this->conn_prod) === true) {
                $this->resp['errors'][] = "ermprod_live bağlantı kurulamadı";
            }
            if (is_bool($this->conn_ozlm) === true) {
                $this->resp['errors'][] = "ermozlm_live bağlantı kurulamadı";
            }
        }
        try {
            $time_start_1 = microtime();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $boskasasure = $this->boskasasure;
            $boshammaddekasasuresn = $this->boshammaddekasasuresn;
            $boshammaddekasalokasyon = $this->boshammaddekasalokasyon;
            $kapalioturumisbosaltmasure = $this->kapalioturumisbosaltmasure;
            $bekleyen = false;
            $sistemacikmi = false;
            if ($this->customer == 'ERMETAL') {
                $ret_stok = $this->mhstokkontrol();
                //$this->resp['stok']=$ret_stok;
            }
            $this->resp['duration_0'] = round($this->microtime_diff($time_start, microtime()), 2);
            $time_start_1 = microtime();
            if ($this->pc === "172.16.1.142") {
                return new JsonResponse($this->resp, $statusCode);
            }
            //$sql="update task_cases set status='CLOSED' where  movementdetail is not null and casetype='I' and status in ('WAIT','WORK') and erprefnumber in (SELECT erprefnumber from task_lists where finish is not null and finish<CURRENT_DATE - interval '1 day')";
            //$stmt = $conn->prepare($sql);
            //$stmt->execute();
            $sql = "delete FROM case_movements where status='PROCURED' and starttime<CURRENT_DATE- interval '7 day'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            //carrier_sessions day update
            $sql = "update carrier_sessions cs
            set day = sv.sv_day,jobrotation=sv.sv_jr  
            from (
                SELECT cs.id
                    ,(SELECT case when beginval>endval and cast(cast(cs.start as time)+interval '3 minutes' as varchar(5))>endval then cast(cast(cs.start as varchar(10)) as date) + INTERVAL '1 day' else cast(cast(cs.start as varchar(10)) as date) end \"day\" 
                     from job_rotations 	
                     where (finish is null or finish<now())	
                        and (	
                        (endval>beginval and cast(cast(cs.start as time)+interval '3 minutes' as varchar(5)) between beginval and endval)	
                        or (endval<beginval and (cast(cast(cs.start as time)+interval '3 minutes' as varchar(5))>=beginval or cast(cast(cs.start as time)+interval '3 minutes' as varchar(5))<=endval) )	
                     )) sv_day
                     ,(SELECT code 
                     from job_rotations 	
                     where (finish is null or finish<now())	
                        and (	
                        (endval>beginval and cast(cast(cs.start as time)+interval '3 minutes' as varchar(5)) between beginval and endval)	
                        or (endval<beginval and (cast(cast(cs.start as time)+interval '3 minutes' as varchar(5))>=beginval or cast(cast(cs.start as time)+interval '3 minutes' as varchar(5))<=endval) )	
                     )) sv_jr
                from carrier_sessions cs
                where cs.day is null and cs.start>'2020-01-01' 
            ) sv
            where cs.id=sv.id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            //carrier_empty_sessions day update
            $sql = "update carrier_empty_sessions ces
            set day = sv.sv_day,jobrotation=sv.sv_jr  
            from (
                SELECT ces.id
                    ,(SELECT case when beginval>endval and cast(cast(ces.start as time)+interval '3 minutes' as varchar(5))>endval then cast(cast(ces.start as varchar(10)) as date) + INTERVAL '1 day' else cast(cast(ces.start as varchar(10)) as date) end \"day\" 
                     from job_rotations 	
                     where (finish is null or finish<now())	
                        and (	
                        (endval>beginval and cast(cast(ces.start as time)+interval '3 minutes' as varchar(5)) between beginval and endval)	
                        or (endval<beginval and (cast(cast(ces.start as time)+interval '3 minutes' as varchar(5))>=beginval or cast(cast(ces.start as time)+interval '3 minutes' as varchar(5))<=endval) )	
                     )) sv_day
                     ,(SELECT code 
                     from job_rotations 	
                     where (finish is null or finish<now())	
                        and (	
                        (endval>beginval and cast(cast(ces.start as time)+interval '3 minutes' as varchar(5)) between beginval and endval)	
                        or (endval<beginval and (cast(cast(ces.start as time)+interval '3 minutes' as varchar(5))>=beginval or cast(cast(ces.start as time)+interval '3 minutes' as varchar(5))<=endval) )	
                     )) sv_jr
                from carrier_empty_sessions ces
                where ces.day is null and ces.start>'2020-01-01' 
            ) sv
            where ces.id=sv.id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $sql = "select aa.erprefnumber,aa.opname 
            from (
                select tc.erprefnumber,tc.opname
                from task_cases tc 
                where tc.movementdetail is not null and tc.casetype='I' and tc.status in ('WAIT','WORK')
                GROUP BY tc.erprefnumber,tc.opname)aa
            join (SELECT tl.erprefnumber,tl.opname 
            from task_lists tl 
            where tl.finish is not null and tl.delete_user_id is null and tl.finish<CURRENT_DATE - interval '5 day' and tl.finish>CURRENT_DATE - interval '30 day')bb on bb.erprefnumber=aa.erprefnumber and bb.opname=aa.opname";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            foreach ($stmt->fetchAll() as $row) {
                $sql = "update task_cases 
                set status='CLOSED' 
                where movementdetail is not null and casetype='I' and status in ('WAIT','WORK') and erprefnumber=:erprefnumber and opname=:opname ";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('erprefnumber', $row["erprefnumber"]);
                $stmt->bindValue('opname', $row["opname"]);
                $stmt->execute();
            }
            $sql = "SELECT c.code,c.ip,c.info
                ,case when split_part(split_part(c.status,'|',2),':',2)='-' then 'idle' else 'work' end status
                ,case when exists(select id from client_group_details where deleted_at is null and clientgroup='materialprepare' and clientgroupcode='multiple' and client=c.code) then 'multiple' else 'single' end controlmethod
                ,c.workflow,split_part(split_part(c.status,'|',5),':',2)tpp,'' controlnext
            FROM clients c
            where c.isactive=true and c.status not like '%OP-OUT-SYSTEM%'
            and c.code in (SELECT client from client_params where name='task_materialpreparation' and value='true' GROUP BY client order by client)
            and c.workflow<>'El İşçiliği' and c.uuid is not null
            order by c.code ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $clients = $stmt->fetchAll();
            if (count($clients) > 0) {
                $sistemacikmi = true;
            }
            $this->resp['clients'] = [];
            $this->resp['duration_1'] = round($this->microtime_diff($time_start_1, microtime()), 2);
            $time_start_1 = microtime();
            foreach ($clients as $rowstation) {
                $this->controlStationRequirements($rowstation);
            }
            $this->resp['duration_2'] = round($this->microtime_diff($time_start_1, microtime()), 2);
            $time_start_1 = microtime();
            if ($sistemacikmi) {
                //dolu kasalar için hareket görevlerinin oluşturulması
                $sql_tc_o_full = "SELECT tc.*
                    ,case when exists(SELECT id from product_trees where finish is null and materialtype='O' and code=pt.code and oporder<pt.oporder order by oporder desc limit 1) then 1 else 0 end op_pre
                    ,case when exists(SELECT id from product_trees where finish is null and materialtype='O' and code=pt.code and oporder>pt.oporder order by oporder asc limit 1) then 1 else 0 end op_after
                    ,case when exists(SELECT id from product_trees where finish is null and code=ptk.stockname and materialtype='Y' limit 1) then 1 else 0 end op_is_semifinished
                    ,tl.productcount isemri
                    ,(SELECT cast(sum(packcapacity) as integer)sumpack 
                        from task_cases 
                        where casetype='O' and movementdetail is not null and finish<=tc.finish and erprefnumber=tc.erprefnumber and client=tc.client and opname=tc.opname) toplamcikti
                from task_cases tc
                join task_lists tl on tl.erprefnumber=tc.erprefnumber and tl.opname=tc.opname
                ".($this->customer == 'ERMETAL' ? " join case_labels cl on cl.erprefnumber=tc.erprefnumber " : "")."
                join product_trees pt on pt.code=tc.opcode and pt.number=tc.opnumber and pt.materialtype in ('O')
                join product_trees ptk on ptk.code=tc.opcode and ptk.number=tc.opnumber and ptk.materialtype in ('K') and coalesce(ptk.carrierdrainer,'')<>''
                where tc.movementdetail is not null and tc.casetype='O' 
                    and tc.status not in ('WAITFORDELIVERY','DELIVERED','LABELDELETED','SYSTEMCLOSE','CLOSE','CLOSED') ".($this->customer == 'ERMETAL' ? " and cl.status='OPEN' " : "")." 
                    and tc.packcapacity=tc.quantityremaining and pt.finish is null and tc.finish is not null and tc.finish>CURRENT_DATE - interval '1 day'";
                $stmt_tc_o_full = $conn->prepare($sql_tc_o_full);
                $stmt_tc_o_full->execute();
                foreach ($stmt_tc_o_full->fetchAll() as $rowdolukasa) {
                    $code = $rowdolukasa["opcode"];
                    $stockcode = $rowdolukasa["stockcode"];
                    $number = $rowdolukasa["opnumber"];
                    $erprefnumber = $rowdolukasa["erprefnumber"];
                    $client = $rowdolukasa["client"];
                    $packcapacity = $rowdolukasa["packcapacity"];
                    $sonoperasyon = intval($rowdolukasa["op_after"]) == 0;
                    $sonrakioperasyon = intval($rowdolukasa["op_after"]) == 1;
                    $yarimamul = intval($rowdolukasa["op_is_semifinished"]) == 1;
                    $sql_pt_k = "SELECT *
                        ,COALESCE(preptime,".$this->boskasasure.")*60 preptimeseconds,CURRENT_TIMESTAMP(0) starttime
                        ,current_timestamp(0) + (COALESCE(preptime,".$this->boskasasure.") * interval '1 minute') targettime 
                    from product_trees 
                    where finish is null and materialtype in ('K') and COALESCE(carrierdrainer,'')<>'' 
                        and code=:code and number=:number";
                    $stmt_pt_k = $conn->prepare($sql_pt_k);
                    $stmt_pt_k->bindValue('code', $code);
                    $stmt_pt_k->bindValue('number', $number);
                    $stmt_pt_k->execute();
                    $records_pt_k = $stmt_pt_k->fetchAll();
                    if (count($records_pt_k) > 0) {
                        $rowcasept = $records_pt_k[0];
                        $drainer = $rowcasept["drainer"];
                        $carrierdrainer = $rowcasept["carrierdrainer"];
                        // $gorevtip=$sonrakioperasyon?'GRV009':($rowcasept["qualitycontrolrequire"]==true?'GRV005':($sonoperasyon?'GRV008':'GRV007'));
                        // $hedeflokasyon=$sonrakioperasyon?$this->urtmamb:($rowcasept["qualitycontrolrequire"]==true?$this->muayenealani:($yarimamul?$this->ymamulambari:($sonoperasyon?$this->mamulambari:$this->ymamulambari)));
                        $gorevtip = '';
                        $hedeflokasyon = '';
                        if ($sonrakioperasyon) {
                            $gorevtip = 'GRV009';
                            $hedeflokasyon = $this->urtmamb;
                        } else {
                            if ($rowcasept["qualitycontrolrequire"] == true && intval($rowdolukasa["isemri"]) === intval($rowdolukasa["toplamcikti"])) {
                                //2021-11-19 tarihli talep için eklendi
                                // son kasa muayene alanına
                                $gorevtip = 'GRV005';
                                $hedeflokasyon = $this->muayenealani;
                            } else {
                                if ($sonoperasyon) {
                                    $gorevtip = 'GRV008';
                                } else {
                                    $gorevtip = 'GRV007';
                                }
                                if ($yarimamul) {
                                    $hedeflokasyon = $this->ymamulambari;
                                } else {
                                    if ($sonoperasyon) {
                                        $hedeflokasyon = $this->mamulambari;
                                    } else {
                                        $hedeflokasyon = $this->ymamulambari;
                                    }
                                }
                            }
                        }
                        $sql_cm_k = "SELECT * 
                            from case_movements 
                            where actualfinishtime is null and tasktype=:tasktype
                                and locationsource=:locationsource and erprefnumber=:erprefnumber and code=:code and number=:number and casename=:casename";
                        $stmt_cm_k = $conn->prepare($sql_cm_k);
                        $stmt_cm_k->bindValue('tasktype', $gorevtip);
                        $stmt_cm_k->bindValue('locationsource', $client);
                        $stmt_cm_k->bindValue('erprefnumber', $erprefnumber);
                        $stmt_cm_k->bindValue('code', $code);
                        $stmt_cm_k->bindValue('number', $number);
                        $stmt_cm_k->bindValue('casename', $rowcasept["pack"]);
                        $stmt_cm_k->execute();
                        $records_cm_k = $stmt_cm_k->fetchAll();
                        if (count($records_cm_k) == 0) {
                            $arrdata = array(
                                "code" => $code
                                ,"stockcode" => $stockcode
                                ,"number" => $number
                                ,"casename" => $rowcasept["pack"]
                                ,"locationsource" => $client
                                ,"starttime" => $rowcasept["starttime"]
                                ,"targettime" => $rowcasept["targettime"]
                                ,"locationdestination" => $hedeflokasyon
                                ,"erprefnumber" => $erprefnumber
                                ,"client" => $client
                                ,"quantitynecessary" => intval($rowcasept["packcapacity"])
                                ,"carrierfeeder" => $rowcasept["carrierfeeder"]
                                ,"carrierdrainer" => $rowcasept["carrierdrainer"]
                            );
                            $this->hareketolustur($arrdata, $gorevtip, 4);
                        }
                        $sql_update_tc = "update task_cases set status='WAITFORDELIVERY' where movementdetail is not null and id=:id";
                        $stmt_update_tc = $conn->prepare($sql_update_tc);
                        $stmt_update_tc->bindValue('id', $rowdolukasa["id"]);
                        $stmt_update_tc->execute();
                    } else {
                        $this->errinsert("kasaoperasyonyok", "erprefnumber:$erprefnumber $code-$number ürün ağacı kasa kayıt yok");
                    }
                }
                //$this->resp['duration_10']=$this->microtime_diff($time_start_1,microtime());
                //taskcase tablosunda biten malzeme kasa kayıtları ve dolan boş kasa kayıtları kapatılıyor
                //$sql="update task_cases set status='CLOSED'
                //where movementdetail is not null and status not in ('DELIVERED','LABELDELETED')
                //and ((quantityremaining<=0 and casetype='I') or (quantityremaining>=packcapacity and casetype='O'))";
                //$stmt_tc_4 = $conn->prepare($sql);
                //$stmt_tc_4->execute();
                //hat başındaki girdi kasasının boş olanlarının taşınması
                $gorevtip = 'GRV011';
                $sql = "select tc.*,CURRENT_TIMESTAMP(0) starttime,current_timestamp(0) + (".$boshammaddekasasuresn." * interval '1 seconds') targettime 
                    from task_cases tc
                    ".($this->customer == 'ERMETAL' ? " join case_labels cl on cl.erprefnumber=tc.erprefnumber " : "")."
                    left join case_movements cm on cm.actualfinishtime is null 
                        and cm.tasktype='GRV011' and cm.locationsource=tc.client and cm.erprefnumber=tc.erprefnumber 
                        and cm.code=tc.opcode and cm.number=tc.opnumber and cm.casename=tc.casename and cm.stockcode=tc.stockcode
                    where tc.movementdetail is not null and tc.quantityremaining<=0 and tc.casetype='I'
                        and tc.status not in ('DELIVERED','LABELDELETED','SYSTEMCLOSE','CLOSE','CLOSED') ".($this->customer == 'ERMETAL' ? " and cl.status='OPEN' " : "")." 
                        and cm.id is null
                        and exists(
                            SELECT * 
                            from product_trees 
                            where finish is null and materialtype='H' and coalesce(carrierdrainer,'')<>'' 
                                and code=tc.opcode and number=tc.opnumber and stockcode=tc.stockcode
                        )";
                $stmt_tc_5 = $conn->prepare($sql);
                $stmt_tc_5->execute();
                foreach ($stmt_tc_5->fetchAll() as $row) {
                    $erprefnumber = $row["erprefnumber"];
                    $code = $row["opcode"];
                    $stockcode = $row["stockcode"];
                    $number = $row["opnumber"];
                    $casename = $row["casename"];
                    $locationsource = $row["client"];
                    //boş hammadde kasası götür görev oluşturulup oluşturulmayacağı kontrol ediliyor
                    $sql_pt_3 = "SELECT * 
                        from product_trees 
                        where finish is null and materialtype='H' and coalesce(carrierdrainer,'')<>''
                            and code=:code and number=:number and stockcode=:stockcode";
                    $stmt_pt_3 = $conn->prepare($sql_pt_3);
                    $stmt_pt_3->bindValue('code', $code);
                    $stmt_pt_3->bindValue('number', $number);
                    $stmt_pt_3->bindValue('stockcode', $stockcode);
                    $stmt_pt_3->execute();
                    $records_pt_3 = $stmt_pt_3->fetchAll();
                    if (count($records_pt_3) > 0) {
                        $row_pt_3 = $records_pt_3[0];
                        $drainer = $row_pt_3["drainer"];
                        $arrdata = array(
                                "code" => $code
                                ,"stockcode" => $stockcode
                                ,"number" => $number
                                ,"casename" => $casename
                                ,"locationsource" => $locationsource
                                ,"starttime" => $row["starttime"]
                                ,"targettime" => $row["targettime"]
                                ,"locationdestination" => $boshammaddekasalokasyon
                                ,"erprefnumber" => $erprefnumber
                                ,"client" => $locationsource
                                ,"quantitynecessary" => 0
                                ,"carrierfeeder" => $row_pt_3["carrierfeeder"]
                                ,"carrierdrainer" => $row_pt_3["carrierdrainer"]
                            );
                        $this->hareketolustur($arrdata, $gorevtip, 5);
                    }
                    $sql_update_tc = "update task_cases set status='WAITFORDELIVERY' where movementdetail is not null and id=:id";
                    $stmt_update_tc = $conn->prepare($sql_update_tc);
                    $stmt_update_tc->bindValue('id', $row["id"]);
                    $stmt_update_tc->execute();
                }
                //oturumu kapatıldıkdan sonra $kapalioturumisbosaltmasure dakika geçen taşıyıcının görevi varsa boşalt.
                if ($this->customer == 'ERMETAL') {
                    $sql_cm_2 = "SELECT carrier 
                    from case_movements 
                    where actualfinishtime is null and nullif(carrier,'') is not null and status not in ('DELIVERED','LABELDELETED','SYSTEMCLOSE')
                    GROUP BY carrier";
                    $stmt_cm_2 = $conn->prepare($sql_cm_2);
                    $stmt_cm_2->execute();
                    $_arr_session_close = [];
                    foreach ($stmt_cm_2->fetchAll() as $row) {
                        $sql_cs_1 = "SELECT carrier
                            ,round(extract(epoch from (CURRENT_TIMESTAMP::timestamp - COALESCE(finish,CURRENT_TIMESTAMP)::timestamp))) suresn
                        from carrier_sessions
                        where carrier=:carrier
                        order by cast(COALESCE(finish,CURRENT_TIMESTAMP)as varchar(19))::timestamp desc
                        limit 1";
                        $stmt_cs_1 = $conn->prepare($sql_cs_1);
                        $stmt_cs_1->bindValue('carrier', $row["carrier"]);
                        $stmt_cs_1->execute();
                        $records_cs_1 = $stmt_cs_1->fetchAll();
                        if (count($records_cs_1) > 0) {
                            $qoturumkapali = $records_cs_1[0];
                            $suresn = $qoturumkapali["suresn"];
                            if ($suresn >= $kapalioturumisbosaltmasure * 60) {
                                if (!(in_array($row['carrier'], $_arr_session_close))) {
                                    $_arr_session_close[] = $row['carrier'];
                                }
                                $sql_cs_2 = "update case_movement_details cmd
                                set canceltime=CURRENT_TIMESTAMP,description='Oturum kapatma zaman aşımı'
                                from case_movements cm 
                                where cmd.casemovementid=cm.id and cmd.carrier=cm.carrier and cmd.canceltime is null and cmd.donetime is null 
                                and cm.actualfinishtime is null and cm.status not in ('DELIVERED','LABELDELETED','SYSTEMCLOSE') and cmd.carrier=:carrier";
                                $stmt_cs_2 = $conn->prepare($sql_cs_2);
                                $stmt_cs_2->bindValue('carrier', $row["carrier"]);
                                $stmt_cs_2->execute();

                                $sql_cm_3 = "SELECT * from case_movements where actualfinishtime is null and status not in ('DELIVERED','LABELDELETED','SYSTEMCLOSE') and carrier=:carrier";
                                $stmt_cm_3 = $conn->prepare($sql_cm_3);
                                $stmt_cm_3->bindValue('carrier', $row["carrier"]);
                                $stmt_cm_3->execute();
                                foreach ($stmt_cm_3->fetchAll() as $rh) {
                                    $this->dosyasil($rh["filepath"], 3);
                                    $sql_cm_4 = "update case_movements set carrier=null,empcode=null,empname=null,assignmenttime=null,filepath=null,status='WAITFORASSIGNMENT' where id=:id";
                                    $stmt_cm_4 = $conn->prepare($sql_cm_4);
                                    $stmt_cm_4->bindValue('id', $rh["id"]);
                                    $stmt_cm_4->execute();
                                }
                                $sql_cs_3 = "update carrier_sessions set finish=CURRENT_TIMESTAMP where finish is null and carrier=:carrier";
                                $stmt_cs_3 = $conn->prepare($sql_cs_3);
                                $stmt_cs_3->bindValue('carrier', $row["carrier"]);
                                $stmt_cs_3->execute();
                            }
                        }
                    }
                }
                if ($this->customer == 'ERMETAL') {
                    foreach ($_arr_session_close as $item) {
                        $sql_c_1 = "select id from carriers where code=:code";
                        $stmt_c_1 = $conn->prepare($sql_c_1);
                        $stmt_c_1->bindValue('code', $item);
                        $stmt_c_1->execute();
                        $records_c_1 = $stmt_c_1->fetchAll();
                        if (count($records_c_1) > 0) {
                            $sql_cs_4 = "SELECT empcode from carrier_sessions where finish is null and carrier=:carrier";
                            $stmt_cs_4 = $conn->prepare($sql_cs_4);
                            $stmt_cs_4->bindValue('carrier', $item);
                            $stmt_cs_4->execute();
                            $records_cs_4 = $stmt_cs_4->fetchAll();
                            if (count($records_cs_4) > 0) {
                                $data = '{"material_prepare":true,"methodName":"oturumkapat","id":'.$records_c_1[0]["id"].',"sicil":'.$records_cs_4[0]["empcode"].',"carrier":"'.$item.'"}';
                                $this->dosyayaznodetrigger($data, 510);
                            }
                        }
                    }
                }
                //atama yapılmadan oluşturulan görevlerin dağtıma uygun olanları için gorevtip alanına göre bakılarak her girişte(30sn) 1 iş (bitishedef sıralı olarak) ataması yapılacak
                $sql_cm_not_assign = "SELECT * 
                from case_movements 
                where actualfinishtime is null and assignmenttime is null and carrier is null and status='WAITFORASSIGNMENT'
                order by locationdestination";
                $stmt_cm_not_assign = $conn->prepare($sql_cm_not_assign);
                $stmt_cm_not_assign->execute();
                $flag = false;
                foreach ($stmt_cm_not_assign->fetchAll() as $row) {
                    if ($flag) {
                        continue;
                    }
                    $gorevtip = $row['tasktype'];
                    $arr_feed = array('GRV001','GRV002','GRV004','GRV012');
                    if (in_array($gorevtip, $arr_feed)) {
                        $carriers = $row["carrierfeeder"];
                    } else {
                        $carriers = $row["carrierdrainer"];
                    }
                    $gorevli = $this->gorevlibul($carriers, 1);
                    if ($gorevli != null) {
                        $t = round(microtime(true) * 1000);
                        $empcode = null;
                        $empname = null;
                        $sql_cs_2 = "SELECT empcode,empname from carrier_sessions where finish is null and carrier=:carrier";
                        $stmt_cs_2 = $conn->prepare($sql_cs_2);
                        $stmt_cs_2->bindValue('carrier', $gorevli);
                        $stmt_cs_2->execute();
                        $records_cs_2 = $stmt_cs_2->fetchAll();
                        if (count($records_cs_2) > 0) {
                            $emp = $records_cs_2[0];
                            $empcode = $emp["empcode"];
                            $empname = $emp["empname"];
                        } else {
                            if (/*$row["carriertype"]=='P'&&*/$gorevli == $this->user_mh) {
                                $empcode = $this->user_mh;
                                $empname = $this->user_mh;
                            }
                        }
                        if (!(in_array($gorevli, $this->arrnodeinfo))) {
                            $this->arrnodeinfo[] = $gorevli;
                        }
                        $bilesen = str_replace("/", ";", ($row["tasktype"] == 'GRV001' || $row["tasktype"] == 'GRV011' ? $row["casename"] : $row["stockcode"]));
                        $exportFileName = $this->customer == 'ERMETAL' ? $this->exportpath."$bilesen#".$row["locationdestination"]."#$gorevli#$t.txt" : '';
                        $this->errinsert("gorevatama-ok", json_encode(array("id" => $row["id"],"carrier" => $gorevli,"empcode" => $empcode,"empname" => $empname)), 200);
                        $sql_cm_assign = "update case_movements
                                set carrier=:carrier,empcode=:empcode,empname=:empname,assignmenttime=current_timestamp,filepath=:filepath,status='WAITFORDELIVERY'
                                where id=:id";
                        $stmt_cm_assign = $conn->prepare($sql_cm_assign);
                        $stmt_cm_assign->bindValue('carrier', $gorevli);
                        $stmt_cm_assign->bindValue('empcode', $empcode);
                        $stmt_cm_assign->bindValue('empname', $empname);
                        $stmt_cm_assign->bindValue('filepath', $exportFileName);
                        $stmt_cm_assign->bindValue('id', $row["id"]);
                        $stmt_cm_assign->execute();
                        $sql_cmd_i_1 = "insert into case_movement_details (
                                casemovementid,carrier,assignmenttime,empcode,empname)
                                values (:casemovementid,:carrier,current_timestamp,:empcode,:empname)
                            ";
                        $stmt_cmd_i_1 = $conn->prepare($sql_cmd_i_1);
                        $stmt_cmd_i_1->bindValue('casemovementid', $row["id"]);
                        $stmt_cmd_i_1->bindValue('carrier', $gorevli);
                        $stmt_cmd_i_1->bindValue('empcode', $empcode);
                        $stmt_cmd_i_1->bindValue('empname', $empname);
                        $stmt_cmd_i_1->execute();
                        $this->dosyayaz($row["locationdestination"], $bilesen, $gorevli, $t, 1);
                        /*
                        görevliye yeni bir iş ataması yapılıyor, tasiyicibossure tablosunda açık kaydı var ise kapatılacak
                        */
                        $sql_11 = "update carrier_empty_sessions set finish=current_timestamp where finish is null and carrier=:carrier and empcode=:empcode";
                        $stmt_11 = $conn->prepare($sql_11);
                        $stmt_11->bindValue('carrier', $gorevli);
                        $stmt_11->bindValue('empcode', $empcode);
                        $stmt_11->execute();
                        $flag = true;
                    }
                }
                //$this->resp['duration_14']=$this->microtime_diff($time_start_1,microtime());
                //cihazda işin sonlandırılması
                /*
                $sql_task_finished_0="select x.*,y.startcpd from (
                    SELECT client,erprefnumber,min(starttime) starttime
                    from case_movements
                    where actualfinishtime is null and status in ('WAITFORASSIGNMENT','WAITFORDELIVERY')
                    group by client,erprefnumber)x
                left join (
                    SELECT cpd.client,cpd.erprefnumber,min(cpd.start)startcpd
                    from client_production_details cpd
                    join (
                        SELECT id,code,beginval,endval,concat(current_date,' ',cast(current_time as varchar(8))) zaman,CURRENT_TIMESTAMP(0) ts
                            ,cast(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) as date) \"day\"
                            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end
                        from job_rotations
                        where (finish is null or finish<now())
                            and (
                            (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)
                            or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )
                        )
                    )sv on sv.day=cpd.day and sv.code=cpd.jobrotation
                    where cpd.finish is null and cpd.type='c_p' and cpd.erprefnumber is not null
                    GROUP BY cpd.client,cpd.erprefnumber
                )y on y.client=x.client
                where x.starttime<y.startcpd-interval '30 minutes'";
                $stmt_task_finished_0 = $conn->prepare($sql_task_finished_0);
                $stmt_task_finished_0->execute();
                foreach ($stmt_task_finished_0->fetchAll() as $row_0) {
                    $sql_task_finished_1="
                        SELECT *
                        from case_movements
                        where actualfinishtime is null and status in ('WAITFORASSIGNMENT','WAITFORDELIVERY')
                            and client=:client and erprefnumber=:erprefnumber";
                    $stmt_task_finished_1 = $conn->prepare($sql_task_finished_1);
                    $stmt_task_finished_1->bindValue('client', $row_0["client"]);
                    $stmt_task_finished_1->bindValue('erprefnumber', $row_0["erprefnumber"]);
                    $stmt_task_finished_1->execute();
                    foreach ($stmt_task_finished_1->fetchAll() as $row_1) {
                        if($row_1['carrier']!=null&&$row_1['carrier']!=''){
                            $sql_tf_cmd_1="update case_movement_details
                                set canceltime=CURRENT_TIMESTAMP,description='İş emri bitişi-1'
                                where canceltime is null and donetime is null and casemovementid=:casemovementid and carrier=:carrier ";
                            $stmt_tf_cmd_1 = $conn->prepare($sql_tf_cmd_1);
                            $stmt_tf_cmd_1->bindValue('casemovementid', $row_1["id"]);
                            $stmt_tf_cmd_1->bindValue('carrier', $row_1["carrier"]);
                            $stmt_tf_cmd_1->execute();
                            $this->dosyasil($row_1["filepath"], 4);

                            $sql_tf_cm_c_1="SELECT count(*)sayi from case_movements where actualfinishtime is null and status='WAITFORDELIVERY' and carrier=:carrier";
                            $stmt_tf_cm_c_1 = $conn->prepare($sql_tf_cm_c_1);
                            $stmt_tf_cm_c_1->bindValue('carrier', $row_1["carrier"]);
                            $stmt_tf_cm_c_1->execute();
                            $record_tf_cm_c_1=$stmt_tf_cm_c_1->fetchAll();
                            if(intval($record_tf_cm_c_1[0]['sayi'])==0){
                                $sql_ces_i_1="insert into carrier_empty_sessions (carrier,empcode,empname,start)
                                    values (:carrier,:empcode,:empname,CURRENT_TIMESTAMP(0))";
                                $stmt_ces_i_1 = $conn->prepare($sql_ces_i_1);
                                $stmt_ces_i_1->bindValue('carrier', $row_1["carrier"]);
                                $stmt_ces_i_1->bindValue('empcode', $row_1["empcode"]);
                                $stmt_ces_i_1->bindValue('empname', $row_1["empname"]);
                                $stmt_ces_i_1->execute();
                            }
                            if (!(in_array($row_1['carrier'], $this->arrnodeinfo))) {
                                $this->arrnodeinfo[] = $row_1['carrier'];
                            }
                        }
                        $sql_tf_cm_1="update case_movements
                            set actualfinishtime=CURRENT_TIMESTAMP,status='CLOSED'
                            where id=:id";
                        $stmt_tf_cm_1 = $conn->prepare($sql_tf_cm_1);
                        $stmt_tf_cm_1->bindValue('id', $row_1["id"]);
                        $stmt_tf_cm_1->execute();
                    }
                }*/
                //iş bitiminde taşıma görevlerinin silinmesi
                $sql_task_finished = "SELECT cm.*
                    from task_lists tl 
                    join case_movements cm on cm.erprefnumber=tl.erprefnumber and cm.code=tl.opcode 
                        and cm.number=tl.opnumber and cm.status in ('WAITFORASSIGNMENT','WAITFORDELIVERY')
                        and cm.locationdestination=tl.client
                    where tl.finish>current_timestamp(0) - (10 * interval '1 minute') /*and tl.delete_user_id is null and tl.taskfromerp=true*/";
                $stmt_task_finished = $conn->prepare($sql_task_finished);
                $stmt_task_finished->execute();
                foreach ($stmt_task_finished->fetchAll() as $row) {
                    //$breaktimeseconds=0;
                    if ($row['carrier'] != null && $row['carrier'] != '') {
                        $sql_tf_cmd = "update case_movement_details 
                        set canceltime=CURRENT_TIMESTAMP,description='İş emri bitişi-2' 
                        where canceltime is null and donetime is null and casemovementid=:casemovementid and carrier=:carrier ";
                        $stmt_tf_cmd = $conn->prepare($sql_tf_cmd);
                        $stmt_tf_cmd->bindValue('casemovementid', $row["id"]);
                        $stmt_tf_cmd->bindValue('carrier', $row["carrier"]);
                        $stmt_tf_cmd->execute();
                        $this->dosyasil($row["filepath"], 4);

                        $sql_tf_cm_c = "SELECT count(*)sayi from case_movements where actualfinishtime is null and status='WAITFORDELIVERY' and carrier=:carrier";
                        $stmt_tf_cm_c = $conn->prepare($sql_tf_cm_c);
                        $stmt_tf_cm_c->bindValue('carrier', $row["carrier"]);
                        $stmt_tf_cm_c->execute();
                        $record_tf_cm_c = $stmt_tf_cm_c->fetchAll();
                        if (intval($record_tf_cm_c[0]['sayi']) == 0) {
                            $sql_sv = "SELECT id,code,beginval,endval
                                ,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) \"day\" 
                                ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                                ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end
                            from job_rotations 
                            where (finish is null or finish<now())
                                and (
                                (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)
                                or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )
                            )";
                            $stmt_sv = $conn->prepare($sql_sv);
                            $stmt_sv->execute();
                            $_day = null;
                            $_jr = null;
                            $record_sv = $stmt_sv->fetchAll();
                            if (count($record_sv) > 0) {
                                $_day = $record_sv[0]['day'];
                                $_jr = $record_sv[0]['code'];
                            }
                            $sql_ces_i = "insert into carrier_empty_sessions (carrier,empcode,empname,start,day,jobrotation) 
                            values (:carrier,:empcode,:empname,CURRENT_TIMESTAMP(0),:day,:jobrotation)";
                            $stmt_ces_i = $conn->prepare($sql_ces_i);
                            $stmt_ces_i->bindValue('carrier', $row["carrier"]);
                            $stmt_ces_i->bindValue('empcode', $row["empcode"]);
                            $stmt_ces_i->bindValue('empname', $row["empname"]);
                            $stmt_ces_i->bindValue('day', $_day);
                            $stmt_ces_i->bindValue('jobrotation', $_jr);
                            $stmt_ces_i->execute();
                        }
                        if (!(in_array($row['carrier'], $this->arrnodeinfo))) {
                            $this->arrnodeinfo[] = $row['carrier'];
                        }
                        //$sql_breaktime="select sum(COALESCE(extract(epoch from (cld_1.finish::timestamp - cld_1.start::timestamp)),0)) breaktimeseconds
                        // from (
                        //	SELECT case when cld.start>:assignmenttime then cld.start else :assignmenttime end as start
                        //		,coalesce(cld.finish,CURRENT_TIMESTAMP(0)) finish
                        //	from client_lost_details cld
                        //	where cld.type='l_c' and cld.losttype in ('CUMA PAYDOSU','ÇAY MOLASI','YEMEK MOLASI') and cld.client=:client
                        //	and (CURRENT_TIMESTAMP(0) between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0))
                        //		or :assignmenttime between cld.start and coalesce(cld.finish,CURRENT_TIMESTAMP(0))
                        //		or (:assignmenttime<=cld.start and CURRENT_TIMESTAMP(0)>=coalesce(cld.finish,CURRENT_TIMESTAMP(0)))
                        //	)
                        //)cld_1";
                        //$stmt_breaktime = $conn->prepare($sql_breaktime);
                        //$stmt_breaktime->bindValue('assignmenttime', $row["assignmenttime"]);
                        //$stmt_breaktime->bindValue('client', $row["client"]);
                        //$stmt_breaktime->execute();
                        //$record_breaktime=$stmt_breaktime->fetchAll();
                        //if(count($record_breaktime)>0){
                        //    $record_breaktime=$record_breaktime[0];
                        //    $breaktimeseconds=$record_breaktime["breaktimeseconds"];
                        //}
                    }
                    $sql_tf_cm = "update case_movements 
                    set carrier=null,empcode=null,empname=null,actualfinishtime=CURRENT_TIMESTAMP,status='CLOSED',carriertype='1'
                    where id=:id";
                    $stmt_tf_cm = $conn->prepare($sql_tf_cm);
                    $stmt_tf_cm->bindValue('id', $row["id"]);
                    //$stmt_tf_cm->bindValue('breaktimeseconds', $breaktimeseconds);
                    $stmt_tf_cm->execute();
                }
                //WAITFORMATERIAL tipli işlerin erpref numarası ile aynı işler silinmeli, aşağıdaki sorguya union ile ekle
                //kasa düzenleme sonrası silinmesi gereken iş var mı?
                $sql_kasa_duzenle_sonrasi_silinecekler = "SELECT cm.id,cm.carrier,cm.empcode,cm.empname,cm.filepath
                from case_movements cm
                left join task_lists tl on tl.erprefnumber=cm.erprefnumber and tl.opcode=cm.code and tl.opnumber=cm.number and tl.finish is null and tl.delete_user_id is null
                left join product_trees pt on pt.name=tl.opname and pt.materialtype='O' and pt.finish is null
                left join task_cases tc on tc.erprefnumber=cm.erprefnumber and tc.movementdetail is not null and tc.stockcode=cm.stockcode and ((tc.casetype='I' and tc.quantityremaining>0) or (tc.casetype='O' and tc.packcapacity>0 and tc.packcapacity-tc.quantityremaining>0))
                left join task_cases tc2 on tc2.erprefnumber=cm.erprefnumber and tc2.movementdetail is not null and tc2.stockcode=cm.stockcode and tc2.finish is not null and tc2.status='WAITFORDELIVERY'
                where cm.actualfinishtime is null and tc.status in ('WAIT','WORK') and cm.tasktype not in ('WAITFORMATERIAL') and tc2.id is null 
                and ((tc.casetype='I' and tc.preptime<floor(((tc.quantityremaining/tc.quantityused)*coalesce(pt.tpp,tl.tpp))/60) ) or (tc.casetype='O' and tc.preptime<floor((((tc.packcapacity-tc.quantityremaining)/tc.quantityused)*coalesce(pt.tpp,tl.tpp))/60)) )";
                $stmt_kasa_duzenle_sonrasi_silinecekler = $conn->prepare($sql_kasa_duzenle_sonrasi_silinecekler);
                $stmt_kasa_duzenle_sonrasi_silinecekler->execute();
                foreach ($stmt_kasa_duzenle_sonrasi_silinecekler->fetchAll() as $row) {
                    if ($row['carrier'] != null && $row['carrier'] != '') {
                        $sql_tf_cmd = "update case_movement_details 
                        set canceltime=CURRENT_TIMESTAMP,description='kasa düzenleme kasaya malzeme ekleme' 
                        where canceltime is null and donetime is null and casemovementid=:casemovementid and carrier=:carrier ";
                        $stmt_tf_cmd = $conn->prepare($sql_tf_cmd);
                        $stmt_tf_cmd->bindValue('casemovementid', $row["id"]);
                        $stmt_tf_cmd->bindValue('carrier', $row["carrier"]);
                        $stmt_tf_cmd->execute();
                        $this->dosyasil($row["filepath"], 4);

                        $sql_tf_cm_c = "SELECT count(*)sayi from case_movements where actualfinishtime is null and status='WAITFORDELIVERY' and carrier=:carrier";
                        $stmt_tf_cm_c = $conn->prepare($sql_tf_cm_c);
                        $stmt_tf_cm_c->bindValue('carrier', $row["carrier"]);
                        $stmt_tf_cm_c->execute();
                        $record_tf_cm_c = $stmt_tf_cm_c->fetchAll();
                        if (intval($record_tf_cm_c[0]['sayi']) == 0) {
                            $sql_ces_i = "insert into carrier_empty_sessions (carrier,empcode,empname,start) 
                            values (:carrier,:empcode,:empname,CURRENT_TIMESTAMP(0))";
                            $stmt_ces_i = $conn->prepare($sql_ces_i);
                            $stmt_ces_i->bindValue('carrier', $row["carrier"]);
                            $stmt_ces_i->bindValue('empcode', $row["empcode"]);
                            $stmt_ces_i->bindValue('empname', $row["empname"]);
                            $stmt_ces_i->execute();
                        }
                        if (!(in_array($row['carrier'], $this->arrnodeinfo))) {
                            $this->arrnodeinfo[] = $row['carrier'];
                        }
                    }
                    $sql_tf_cm = "update case_movements 
                    set actualfinishtime=CURRENT_TIMESTAMP,status='CLOSED-CASE-UPDATE',carriertype='2'
                    where id=:id";
                    $stmt_tf_cm = $conn->prepare($sql_tf_cm);
                    $stmt_tf_cm->bindValue('id', $row["id"]);
                    $stmt_tf_cm->execute();
                }

                //node client socket emit-tasimagorevilistele json dosya
                //yeni eklenen görevlerin cihazlara duyurulması
                foreach ($this->arrnodeinfo as $item) {
                    $sql_c_2 = "select id from carriers where code=:code";
                    $stmt_c_2 = $conn->prepare($sql_c_2);
                    $stmt_c_2->bindValue('code', $item);
                    $stmt_c_2->execute();
                    $records_c_2 = $stmt_c_2->fetchAll();
                    if (count($records_c_2) > 0) {
                        $data = '{"material_prepare":true,"methodName":"tasimagorevilistele","id":"'.$records_c_2[0]["id"].'","carrier":"'.$item.'"}';
                        $this->dosyayaznodetrigger($data, 512);
                    }
                }
                $sql = "delete from case_movements where status in ('CLOSED','CLOSED-CASE-UPDATE') and actualfinishtime-starttime<'00:00:05' /*and starttime<CURRENT_TIMESTAMP - INTERVAL '1 hour'*/";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $_d = $d->format("Y-m-d H:i:s.u");
                $sql_20 = "SELECT c.code client,coalesce(cast(max(cm.targettime) as varchar),'')targettime,c.ip,c.uuid,count(cm.*) gorevsayi
                from clients c
                left join case_movements cm on c.code=cm.client and cm.status='WAITFORDELIVERY' and cm.locationdestination=cm.client 
                where c.workflow not in ('El İşçiliği','Kataforez') and c.connected=true
                GROUP BY c.code,c.ip,c.uuid 
                order by c.code";
                $stmt_20 = $conn->prepare($sql_20);
                $stmt_20->execute();
                $records_20 = $stmt_20->fetchAll();
                if (count($records_20) > 0) {
                    foreach ($records_20 as $row_20) {
                        $_data = array("targettime" => $row_20["targettime"],"gorevsayi" => $row_20["gorevsayi"]);
                        // $conn->insert('_sync_messages', array('ip'=>($row_20["client"].'_server'),'uuid' => $row_20["uuid"], 'ack' => 'true','queued'=>0, 'type' => 'mh_case_movement_target_time', 'data' => json_encode($_data), '"createdAt"' => $_d, '"updatedAt"' => $_d));
                        if ($this->pc == "192.168.1.40") {
                            $conn->insert('__sync_messages', array('queuename' => ($row_20["client"].'_server'), 'priority' => 10, 'messagetype' => 'mh_case_movement_target_time', 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data),'info' => json_encode(array("tableName" => "")) ));
                        }
                    }
                }
            }
            $this->resp['duration_3'] = round($this->microtime_diff($time_start_1, microtime()), 2);
            $time_start_1 = microtime();
        } catch (Exception $e) {
            $this->resp['message'] = $e->getMessage();
            $this->errinsert("getDataMH-err", "message:".$e->getMessage(), 101);
        }
        if ($this->pc != "192.168.1.40" && $this->customer == 'ERMETAL') {
            try {
                if (is_bool($this->conn_prod) !== true) {
                    $type = get_resource_type($this->conn_prod);
                    $wait_until = time() + 3;
                    do {
                        odbc_close($this->conn_prod);
                    } while (get_resource_type($this->conn_prod) === $type && time() < $wait_until);
                    $this->conn_prod = null;
                }
            } catch (Exception $e) {
                $this->resp['errors'][] = $e->getMessage();
                return new JsonResponse($this->resp, $statusCode);
            }
            try {
                if (is_bool($this->conn_ozlm) !== true) {
                    $type = get_resource_type($this->conn_ozlm);
                    $wait_until = time() + 3;
                    do {
                        odbc_close($this->conn_ozlm);
                    } while (get_resource_type($this->conn_ozlm) === $type && time() < $wait_until);
                    $this->conn_ozlm = null;
                }
            } catch (Exception $e) {
                $this->resp['errors'][] = $e->getMessage();
                return new JsonResponse($this->resp, $statusCode);
            }
        }
        $time_end = microtime();
        $this->resp['duration'] = round($this->microtime_diff($time_start, $time_end), 2);
        return new JsonResponse($this->resp, $statusCode);
    }

    public function controlStationRequirements($station)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $boskasasure = $this->boskasasure;
        $boshammaddekasasuresn = $this->boshammaddekasasuresn;
        $boshammaddekasalokasyon = $this->boshammaddekasalokasyon;
        $kapalioturumisbosaltmasure = $this->kapalioturumisbosaltmasure;
        $bekleyen = false;

        $client = $station['code'];
        $clientInfo = $station["info"];
        if ($station['status'] == 'work') {
            $sql_tl0 = "select b.*,".($this->customer == 'ERMETAL' ? "cl.status" : "'OPEN'")." as status
                ,tl.productcount
                ,tl.productdonecount
                ,tl.tpp
                ,tl.productcount-tl.productdonecount kalan
            from (
                SELECT type,client,opcode,opnumber,opname,erprefnumber,tasklist,mould,mouldgroup 
                from client_production_details 
                where finish is null and type in ('c_p','c_p_confirm') and day>=current_date - interval '1' day and client=:client
                group by type,client,opcode,opnumber,opname,erprefnumber,tasklist,mould,mouldgroup
            )b
            ".($this->customer == 'ERMETAL' ? "join case_labels cl on cl.erprefnumber=b.erprefnumber and cl.status in('OPEN','DELETED')" : "")."
            join task_lists tl on tl.code=b.tasklist
            where tl.finish is null and tl.delete_user_id is null and tl.productcount>tl.productdonecount ".($this->customer == 'ERMETAL' ? "" : " and tl.taskfromerp=true ")."
            ";
            $stmt_1 = $conn->prepare($sql_tl0);
            $stmt_1->bindValue('client', $client);
            $stmt_1->execute();
            $records_1 = $stmt_1->fetchAll();
            if (count($records_1) > 0) {
                $station["moulds"] = array();
                $station["erprefnumber"] = '';
                //$tpp=intval($station["tpp"]);
                $isDeletedLabel = false;
                foreach ($records_1 as $rowtask) {
                    $tpp = intval($rowtask["tpp"]);//*count($records_1);
                    $productcount = intval($rowtask["productcount"]);
                    $productdonecount = intval($rowtask["productdonecount"]);
                    $kalanmiktar_is = intval($rowtask["kalan"]);
                    $kalansure_is = ($kalanmiktar_is * $tpp);
                    $erprefnumber = $rowtask["erprefnumber"];
                    $station["erprefnumber"] .= ($station["erprefnumber"] == "" ? "'".$erprefnumber."'" : ",'".$erprefnumber."'");
                    if (!(in_array($rowtask["mould"]."|".$rowtask["mouldgroup"], $station["moulds"]))) {
                        $station["moulds"][] = $rowtask["mould"]."|".$rowtask["mouldgroup"];
                    }

                    if ($rowtask["status"] == 'OPEN') {
                        $code = $rowtask["opcode"];
                        $number = $rowtask["opnumber"];
                        $name = $rowtask["opname"];

                        $bekleyen = false;
                        $ilkoperasyon = false;
                        $sonoperasyon = false;
                        $oncekioperasyon = false;
                        $sonrakioperasyon = false;

                        $sql_pt_0 = "SELECT pt.oporder
                            ,case when exists(SELECT id from product_trees where finish is null and code=pt.code and materialtype='O' and oporder<pt.oporder order by oporder desc limit 1) then 1 else 0 end op_pre
                            ,case when exists(SELECT id from product_trees where finish is null and code=pt.code and materialtype='O' and oporder>pt.oporder order by oporder asc limit 1) then 1 else 0 end op_after
                        from product_trees pt 
                        where pt.code=:code and pt.number=:number and pt.materialtype in ('O') and finish is null ".($this->customer == 'SAHINCE' ? " and pt.client=:client " : "")."
                        order by id limit 1";
                        $stmt_pt_0 = $conn->prepare($sql_pt_0);
                        $stmt_pt_0->bindValue('code', $code);
                        $stmt_pt_0->bindValue('number', $number);
                        if ($this->customer == 'SAHINCE') {
                            $stmt_pt_0->bindValue('client', $client);
                        }
                        $stmt_pt_0->execute();
                        $records_pt_0 = $stmt_pt_0->fetchAll();
                        if (count($records_pt_0) > 0) {
                            $row_pt_0 = $records_pt_0[0];
                            $ilkoperasyon = intval($row_pt_0["op_pre"]) == 0;
                            $sonoperasyon = intval($row_pt_0["op_after"]) == 0;
                            $oncekioperasyon = intval($row_pt_0["op_pre"]) == 1;
                            $sonrakioperasyon = intval($row_pt_0["op_after"]) == 1;
                            $bekleyen = $oncekioperasyon == false ? $this->bekleyenvarmi($erprefnumber, $code, $number, $row_pt_0["oporder"]) : false;
                        }
                        if (!$bekleyen) {
                            //operasyon için gerekli malzemeler sorgulanıyor
                            $sql_pt_r = "SELECT * 
                                ,COALESCE(preptime,10)*60 preptimeseconds
                                ,CURRENT_TIMESTAMP(0) starttime
                                ,CURRENT_TIMESTAMP(0) + (COALESCE(preptime,10) * interval '1 minute') targettime
                            from product_trees pt 
                            where pt.finish is null 
                                and COALESCE(pt.carrierfeeder,'')<>'' 
                                -- and COALESCE(pt.feeder,'K')<>'K' 
                                and pt.materialtype in ('H','K') 
                                and pt.code=:code
                                and pt.number=:number
                                ".($this->customer == 'SAHINCE' ? " and pt.isdefault=true " : "")."
                            order by pt.materialtype,pt.stockcode";
                            $stmt_pt_r = $conn->prepare($sql_pt_r);
                            $stmt_pt_r->bindValue('code', $code);
                            $stmt_pt_r->bindValue('number', $number);
                            $stmt_pt_r->execute();
                            $records_pt_r = $stmt_pt_r->fetchAll();
                            foreach ($records_pt_r as $row_pt_r) {
                                $stockcode = $row_pt_r["stockcode"];
                                //ürün gereklilikleri task_cases tablosundan kontrol ediliyor
                                if ($row_pt_r["materialtype"] === 'H') {
                                    $sql_tc_i = "SELECT tc.*
                                        ,ceil((case when tc.finish is null then tc.quantityremaining/tc.quantityused else 0 end))kalanadetaktifkasa
                                        ,case when ps.id is not null then '".$this->urtmamb."' else (case when tc.casetype='I' then '' else coalesce(cm.locationdestination,tc.client) end)end locationsource
                                        ,ps.stockcode ps_stockcode
                                    from task_cases tc
                                    left join case_movements cm on cm.id=tc.movementdetail
                                    left join production_storages ps on ps.casemovementidout=cm.id
                                    where tc.movementdetail is not null 
                                        and tc.status in ('WAIT','WORK')
                                        and tc.casetype='I' 
                                        and tc.client=:client 
                                        and tc.erprefnumber=:erprefnumber 
                                        and tc.opcode=:code 
                                        and tc.opnumber=:number 
                                        and tc.stockcode=:stockcode 
                                    order by tc.id asc
                                    limit 2";
                                    $stmt_tc = $conn->prepare($sql_tc_i);
                                    $stmt_tc->bindValue('client', $client);
                                    $stmt_tc->bindValue('erprefnumber', $erprefnumber);
                                    $stmt_tc->bindValue('code', $code);
                                    $stmt_tc->bindValue('number', $number);
                                    $stmt_tc->bindValue('stockcode', $stockcode);
                                    $stmt_tc->execute();
                                } else {
                                    $sql_tc_o = "SELECT tc.*
                                        ,ceil(case when finish is null then (tc.packcapacity-tc.quantityremaining)/tc.quantityused else tc.packcapacity end) kalanadetaktifkasa
                                    from task_cases tc
                                    where tc.movementdetail is not null 
                                        and tc.status in ('WAIT','WORK')
                                        and tc.casetype='O' 
                                        and tc.client=:client 
                                        and tc.erprefnumber=:erprefnumber 
                                        and tc.opcode=:code 
                                        and tc.opnumber=:number
                                    order by id asc
                                    limit 2";
                                    $stmt_tc = $conn->prepare($sql_tc_o);
                                    $stmt_tc->bindValue('client', $client);
                                    $stmt_tc->bindValue('erprefnumber', $erprefnumber);
                                    $stmt_tc->bindValue('code', $code);
                                    $stmt_tc->bindValue('number', $number);
                                    $stmt_tc->execute();
                                }
                                $records_tc = $stmt_tc->fetchAll();
                                $kalansure_kasa = 0;
                                $miktar_kasa = 0;
                                if (count($records_tc) > 0) {
                                    if (count($records_tc) == 1) {
                                        $rowhatbasikasa = $records_tc[0];
                                        $hazirliksure_kasa = $rowhatbasikasa["preptime"];
                                        $miktar_kasa = intval($rowhatbasikasa["kalanadetaktifkasa"]);
                                        //ihtiyaç miktarından fazla beseleme yapıldığı durumda bitiş tespiti yapılamadığından eklendi
                                        if ($miktar_kasa <= $kalanmiktar_is) {
                                            $kalansure_kasa = intval($miktar_kasa * $tpp);
                                        } else {
                                            $kalansure_kasa = intval($kalansure_is);
                                        }
                                        //hazırlık süresine bakılarak yeni kasa istenecek
                                        if (floor($kalansure_kasa / 60) <= intval($hazirliksure_kasa)) {
                                            //hazırlık süresinden daha kısa bir zaman var
                                            if ($kalanmiktar_is > $miktar_kasa) {
                                                //iş miktarı için hat başı kasa içi miktar yetersiz
                                                if ($rowhatbasikasa["casetype"] === 'I') {
                                                    $sql_cm = "SELECT * 
                                                    from case_movements 
                                                    where actualfinishtime is null 
                                                        and tasktype in ('GRV002','GRV004')
                                                        and locationdestination=:locationdestination 
                                                        and erprefnumber=:erprefnumber 
                                                        and code=:code 
                                                        and number=:number 
                                                        and stockcode=:stockcode";
                                                    $stmt_cm = $conn->prepare($sql_cm);
                                                    $stmt_cm->bindValue('locationdestination', $client);
                                                    $stmt_cm->bindValue('erprefnumber', $erprefnumber);
                                                    $stmt_cm->bindValue('code', $code);
                                                    $stmt_cm->bindValue('number', $number);
                                                    $stmt_cm->bindValue('stockcode', $stockcode);
                                                    $stmt_cm->execute();
                                                } else {
                                                    $sql_cm = "SELECT * 
                                                    from case_movements 
                                                    where actualfinishtime is null 
                                                        and tasktype in ('GRV001')
                                                        and locationdestination=:locationdestination 
                                                        and erprefnumber=:erprefnumber 
                                                        and code=:code 
                                                        and number=:number 
                                                        and casename=:casename";
                                                    $stmt_cm = $conn->prepare($sql_cm);
                                                    $stmt_cm->bindValue('locationdestination', $client);
                                                    $stmt_cm->bindValue('erprefnumber', $erprefnumber);
                                                    $stmt_cm->bindValue('code', $code);
                                                    $stmt_cm->bindValue('number', $number);
                                                    $stmt_cm->bindValue('casename', $row_pt_r["pack"]);
                                                    $stmt_cm->execute();
                                                }
                                                $records_cm = $stmt_cm->fetchAll();
                                                if (count($records_cm) == 0) {
                                                    if ($rowhatbasikasa["casetype"] === 'I') {
                                                        if ($rowhatbasikasa["locationsource"] == "") {
                                                            $adres = "";
                                                            $lot = "";
                                                            $kasano = "";
                                                            if ($this->customer == 'ERMETAL') {
                                                                if ($this->pc != "192.168.1.40") {
                                                                    $qry = "SELECT top 1 ffambh_kod 'ffambh_kod', ffambh_lot 'ffambh_lot', ffambh_kasa 'ffambh_kasa'
                                                                    FROM pub.ffambh_det 
                                                                    WHERE ffambh_domain='ERMETAL' and ffambh_part = '".$stockcode."' 
                                                                    order by ffambh_lot WITH(NOLOCK)";
                                                                    $data = $this->queryqad($this->conn_ozlm, $qry);
                                                                    if (count($data) > 0) {
                                                                        $data = $data[0];
                                                                        //if($dbg==1)mjd($data);
                                                                        $adres = $data["ffambh_kod"];
                                                                        $lot = $data["ffambh_lot"];
                                                                        $kasano = $data["ffambh_kasa"];
                                                                    }
                                                                } else {
                                                                    $adres = "YMID1";
                                                                    $lot = "U150706.2292";
                                                                }
                                                            }
                                                            $gorevtip = $row_pt_r["feeder"] == "P" ? "GRV004" : "GRV002";
                                                            $arrdata = array(
                                                                "code" => $code
                                                                ,"stockcode" => $stockcode
                                                                ,"number" => $number
                                                                ,"casename" => $row_pt_r["pack"]
                                                                ,"locationsource" => ($adres !== "" || $lot !== "" ? $adres."<br>".$lot : "")
                                                                ,"starttime" => $row_pt_r["starttime"]
                                                                ,"targettime" => $row_pt_r["targettime"]
                                                                ,"locationdestination" => $client
                                                                ,"erprefnumber" => $erprefnumber
                                                                ,"client" => $client
                                                                ,"quantitynecessary" => intval($row_pt_r["packcapacity"])
                                                                ,"quantityready" => (isset($this->stoklar[$stockcode]) ? $this->stoklar[$stockcode]["stok"] : null)
                                                                ,"carrierfeeder" => $row_pt_r["carrierfeeder"]
                                                                ,"carrierdrainer" => $row_pt_r["carrierdrainer"]
                                                                ,"casenumbersource" => $kasano
                                                            );
                                                            $this->hareketolustur($arrdata, $gorevtip, "CSR-1 ".floor($kalansure_kasa / 60)."-".intval($hazirliksure_kasa));
                                                        } else {
                                                            //önceki operasyon çıktısı üretimstok tablosunda var mı kontrol ediliyor
                                                            //üretim stok alanında olup,istasyon başında olmayan yarımamül için hareket kaydı olmayanlar seçiliyor
                                                            $gorevtip = "GRV012";
                                                            $arrdata = array(
                                                                "code" => $code
                                                                ,"stockcode" => $rowhatbasikasa["ps_stockcode"]
                                                                ,"number" => $number
                                                                ,"casename" => $rowhatbasikasa["casename"]
                                                                ,"locationsource" => $this->urtmamb
                                                                ,"locationdestination" => $client
                                                                ,"starttime" => $row_pt_r["starttime"]
                                                                ,"targettime" => $row_pt_r["targettime"]
                                                                ,"erprefnumber" => $erprefnumber
                                                                ,"client" => $client
                                                                ,"quantitynecessary" => intval($rowhatbasikasa["packcapacity"])
                                                            );
                                                            $this->hareketolustur($arrdata, $gorevtip, "CSR-ps-1");
                                                            $sql_ps_1 = "select * 
                                                            from production_storages
                                                            where casemovementidout is null and and finish is null 
                                                                and code:code 
                                                                and number=:number 
                                                                and stockcode=:stockcode 
                                                                and erprefnumber=:erprefnumber
                                                            order by casemovementidin limit 0,1
                                                            ";
                                                            $stmt_ps_1 = $conn->prepare($sql_ps_1);
                                                            $stmt_ps_1->bindValue('code', $code);
                                                            $stmt_ps_1->bindValue('number', $number);
                                                            $stmt_ps_1->bindValue('stockcode', $rowhatbasikasa["ps_stockcode"]);
                                                            $stmt_ps_1->bindValue('erprefnumber', $erprefnumber);
                                                            $stmt_ps_1->execute();
                                                            $records_ps_1 = $stmt_ps_1->fetchAll();
                                                            if (count($records_ps_1) > 0) {
                                                                $sql_ps_2 = "update production_storages
                                                                    set casemovementidout=:id_out
                                                                    ,update_user_id=1
                                                                    ,updated_at=current_timestamp()
                                                                    where id=:id";
                                                                $stmt_ps_2 = $conn->prepare($sql_ps_2);
                                                                $stmt_ps_2->bindValue('id_out', $lid);
                                                                $stmt_ps_2->bindValue('id', $records_ps_1[0]["id"]);
                                                                $stmt_ps_2->execute();
                                                            }
                                                        }
                                                    } else {
                                                        $gorevtip = "GRV001";
                                                        $arrdata = array(
                                                            "code" => $code
                                                            ,"stockcode" => $row_pt_r["stockname"]
                                                            ,"number" => $number
                                                            ,"casename" => $row_pt_r["pack"]
                                                            ,"locationsource" => $this->boshammaddekasalokasyon
                                                            ,"starttime" => $row_pt_r["starttime"]
                                                            ,"targettime" => $row_pt_r["targettime"]
                                                            ,"locationdestination" => $client
                                                            ,"client" => $client
                                                            ,"erprefnumber" => $erprefnumber
                                                            ,"quantitynecessary" => ($row_pt_r["packcapacity"])
                                                            ,"carrierfeeder" => $row_pt_r["carrierfeeder"]
                                                            ,"carrierdrainer" => $row_pt_r["carrierdrainer"]
                                                        );
                                                        $this->hareketolustur($arrdata, $gorevtip, "CSR-2 ".floor($kalansure_kasa / 60)."-".intval($hazirliksure_kasa));
                                                    }
                                                }
                                            } else {
                                                //hat başı kasa içi miktar işi bitirmek için yeterli
                                                //sıradaki iş kontrol edilebilir
                                                if ($station["controlnext"] === '' && $rowhatbasikasa["casetype"] === 'O') {
                                                    $station["controlnext"] = "operation-done";
                                                    $station["kalansure"] = $kalansure_is;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    //hat başında kasa yok, önceki iş yarım kesilmiş olabilir
                                    //getirme için görev olup olmadığı kontrol ediliyor
                                    if ($row_pt_r["materialtype"] === 'H') {
                                        $sql_cm = "SELECT * 
                                        from case_movements 
                                        where actualfinishtime is null 
                                            and tasktype in ('GRV002','GRV004')
                                            and locationdestination=:locationdestination 
                                            and erprefnumber=:erprefnumber 
                                            and code=:code 
                                            and number=:number 
                                            and stockcode=:stockcode";
                                        $stmt_cm = $conn->prepare($sql_cm);
                                        $stmt_cm->bindValue('locationdestination', $client);
                                        $stmt_cm->bindValue('erprefnumber', $erprefnumber);
                                        $stmt_cm->bindValue('code', $code);
                                        $stmt_cm->bindValue('number', $number);
                                        $stmt_cm->bindValue('stockcode', $stockcode);
                                        $stmt_cm->execute();
                                    } else {
                                        $sql_cm = "SELECT * 
                                        from case_movements 
                                        where actualfinishtime is null 
                                            and tasktype in ('GRV001')
                                            and locationdestination=:locationdestination 
                                            and erprefnumber=:erprefnumber 
                                            and code=:code 
                                            and number=:number 
                                            and casename=:casename";
                                        $stmt_cm = $conn->prepare($sql_cm);
                                        $stmt_cm->bindValue('locationdestination', $client);
                                        $stmt_cm->bindValue('erprefnumber', $erprefnumber);
                                        $stmt_cm->bindValue('code', $code);
                                        $stmt_cm->bindValue('number', $number);
                                        $stmt_cm->bindValue('casename', $row_pt_r["pack"]);
                                        $stmt_cm->execute();
                                    }
                                    $records_cm = $stmt_cm->fetchAll();
                                    if (count($records_cm) == 0) {
                                        if ($row_pt_r["materialtype"] === 'H') {
                                            $adres = "";
                                            $lot = "";
                                            $kasano = "";
                                            if ($this->customer == 'ERMETAL') {
                                                if ($this->pc != "192.168.1.40") {
                                                    $qry = "SELECT top 1 ffambh_kod 'ffambh_kod', ffambh_lot 'ffambh_lot', ffambh_kasa 'ffambh_kasa' 
                                                    FROM pub.ffambh_det 
                                                    WHERE ffambh_domain='ERMETAL' and ffambh_part = '".$stockcode."' 
                                                    order by ffambh_lot WITH(NOLOCK)";
                                                    $data = $this->queryqad($this->conn_ozlm, $qry);
                                                    if (count($data) > 0) {
                                                        $data = $data[0];
                                                        //if($dbg==1)mjd($data);
                                                        $adres = $data["ffambh_kod"];
                                                        $lot = $data["ffambh_lot"];
                                                        $kasano = $data["ffambh_kasa"];
                                                    }
                                                } else {
                                                    $adres = "YMID1";
                                                    $lot = "U150706.2292";
                                                }
                                            }
                                            $gorevtip = $row_pt_r["feeder"] == "P" ? "GRV004" : "GRV002";
                                            $arrdata = array(
                                                "code" => $code
                                                ,"stockcode" => $stockcode
                                                ,"number" => $number
                                                ,"casename" => $row_pt_r["pack"]
                                                ,"locationsource" => ($adres !== "" || $lot !== "" ? $adres."<br>".$lot : "")
                                                ,"starttime" => $row_pt_r["starttime"]
                                                ,"targettime" => $row_pt_r["targettime"]
                                                ,"locationdestination" => $client
                                                ,"erprefnumber" => $erprefnumber
                                                ,"client" => $client
                                                ,"quantitynecessary" => intval($row_pt_r["packcapacity"])
                                                ,"quantityready" => (isset($this->stoklar[$stockcode]) ? $this->stoklar[$stockcode]["stok"] : null)
                                                ,"carrierfeeder" => $row_pt_r["carrierfeeder"]
                                                ,"carrierdrainer" => $row_pt_r["carrierdrainer"]
                                                ,"casenumbersource" => $kasano
                                            );
                                            $this->hareketolustur($arrdata, $gorevtip, "CSR-3");
                                        } else {
                                            $gorevtip = "GRV001";
                                            $arrdata = array(
                                                "code" => $code
                                                ,"stockcode" => $row_pt_r["stockname"]
                                                ,"number" => $number
                                                ,"casename" => $row_pt_r["pack"]
                                                ,"locationsource" => $this->boshammaddekasalokasyon
                                                ,"starttime" => $row_pt_r["starttime"]
                                                ,"targettime" => $row_pt_r["targettime"]
                                                ,"locationdestination" => $client
                                                ,"client" => $client
                                                ,"erprefnumber" => $erprefnumber
                                                ,"quantitynecessary" => intval($row_pt_r["packcapacity"])
                                                ,"carrierfeeder" => $row_pt_r["carrierfeeder"]
                                                ,"carrierdrainer" => $row_pt_r["carrierdrainer"]
                                            );
                                            $this->hareketolustur($arrdata, $gorevtip, "CSR-4");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($rowtask["status"] == 'DELETED') {
                        $isDeletedLabel = true;
                        if ($kalansure_is <= 3600) {
                            if ($station["controlnext"] === '') {
                                $station["controlnext"] = "label-deleted";
                                $station["kalansure"] = $kalansure_is;
                            }
                        }
                    }
                }
            } else {
                $station['status'] = 'idle';
            }
        }
        if ($station['status'] == 'idle' && $station["controlnext"] === '') {
            $station["controlnext"] = "status-idle";
            $station["kalansure"] = 0;
            $station["erprefnumber"] = "";
        }
        if ($station["controlnext"] !== '') {
            $this->getDataNextJob($station);
        }
        //istasyonda seçili görev dışında, hedefi o istasyon olan görevler kontrol ediliyor
        if ($station['status'] == 'work' && $station["controlmethod"] == "single" && $station["controlnext"] !== "operation-done" && isset($station["erprefnumber"])) {
            //iş bitiminde taşıma görevlerinin silinmesi
            $sql_other_movements = "SELECT * from case_movements where status in ('WAITFORASSIGNMENT','WAITFORDELIVERY') and locationdestination=:client and erprefnumber not in (".$station["erprefnumber"].")";
            $stmt_other_movements = $conn->prepare($sql_other_movements);
            $stmt_other_movements->bindValue('client', $client);
            //$stmt_other_movements->bindValue('erprefnumber', $station["erprefnumber"]);
            $stmt_other_movements->execute();
            $records_other_movements = $stmt_other_movements->fetchAll();
            foreach ($records_other_movements as $item_other_movements) {
                if ($item_other_movements['carrier'] !== null && $item_other_movements['carrier'] !== '') {
                    if (!(in_array($item_other_movements['carrier'], $this->arrnodeinfo))) {
                        $this->arrnodeinfo[] = $item_other_movements['carrier'];
                    }
                    $sql_del_cmd = "delete from case_movement_details where casemovementid=:id";
                    $stmt_del_cmd = $conn->prepare($sql_del_cmd);
                    $stmt_del_cmd->bindValue('id', $item_other_movements['id']);
                    $stmt_del_cmd->execute();
                }
                $sql_del_cm = "delete from case_movements where id=:id";
                $stmt_del_cm = $conn->prepare($sql_del_cm);
                $stmt_del_cm->bindValue('id', $item_other_movements['id']);
                $stmt_del_cm->execute();
                $this->dosyasil($item_other_movements["filepath"], 2);
            }
        }
        //if($client==='03.06.0013'){
        $this->resp['clients'][] = $station;
        //}
    }

    public function getDataNextJob($station)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $client = $station['code'];
        $erprefnumber = $station["erprefnumber"];
        $controlmethod = $station["controlmethod"];
        $controlnext = $station["controlnext"];

        $sql_mould = "SELECT md.mould,md.mouldgroup,md.opname 
        from client_details cd 
        join mould_details md on md.mould=cd.code
        where cd.client=:client and cd.iotype=:iotype and cd.ioevent=:ioevent and cd.finish is null and md.finish is null
        order by cd.code,md.mouldgroup,md.opname";
        $stmt_mould = $conn->prepare($sql_mould);
        $stmt_mould->bindValue('client', $client);
        $stmt_mould->bindValue('iotype', 'I');
        $stmt_mould->bindValue('ioevent', 'input');
        $stmt_mould->execute();
        $cpc = $stmt_mould->fetchAll();

        $arrproductions = array();
        $moulds = array();
        if ($controlmethod === "multiple") {
            if (isset($station["moulds"]) && $station["moulds"] !== null) {
                foreach ($station["moulds"] as $item_mould) {
                    $moulds[] = $item_mould;
                }
            }
        }
        $w = ($erprefnumber !== "" && ($controlnext === 'operation-done' || $controlnext === 'label-deleted')) ? " and tl.erprefnumber not in ($erprefnumber) " : "";
        if ($this->customer == 'SAHINCE') {
            $w .= " and tl.taskfromerp=true ";
        }
        $sql = "SELECT tl.productcount,tl.erprefnumber,pt.*
            ,case when exists(SELECT id from product_trees where finish is null and code=pt.code and materialtype='O' and oporder<pt.oporder order by oporder desc limit 1) then 1 else 0 end op_pre
            ,case when exists(SELECT id from product_trees where finish is null and code=pt.code and materialtype='O' and oporder>pt.oporder order by oporder asc limit 1) then 1 else 0 end op_after
        from task_lists tl 
        ".($this->customer == 'ERMETAL' ? "join case_labels cl on cl.erprefnumber=tl.erprefnumber" : "")."
        join clients c on c.code=tl.client 
        join client_params cp on cp.client=c.code and cp.name='task_materialpreparation' and cp.value='true'
        join product_trees pt on pt.code=tl.opcode and pt.number=tl.opnumber and pt.materialtype in ('O')
        where tl.finish is null and tl.delete_user_id is null and tl.productcount>tl.productdonecount
            and tl.client=:client ".($this->customer == 'ERMETAL' ? " and cl.status='OPEN' " : "")." 
            and pt.finish is null  $w
        order by tl.client,tl.plannedstart,tl.deadline"; //and tl.plannedstart<CURRENT_TIMESTAMP+interval '60 minute' daha sonra tekrar eklenecek
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('client', $client);
        $stmt->execute();
        $records = $stmt->fetchAll();
        if (count($records) > 0) {
            foreach ($records as $row) {
                $b_mould_exists = false;
                //robotta aynı fixtürde yapılan diğer üretimin iş emri için görev oluşturmama kontrolü
                if (($controlmethod == "multiple" && ($controlnext === "label-deleted" || $station["controlnext"] === "status-idle")) || ($controlmethod == "single" && count($arrproductions) == 0)) {
                    $records_mould = array();
                    foreach ($cpc as $cap) {
                        if ($cap["opname"] === $row["name"]) {
                            $records_mould[] = $cap;
                        }
                    }
                    if (count($records_mould) > 0) {
                        $f_group_available = false;
                        foreach ($records_mould as $row_mould) {
                            if ($b_mould_exists == false) {
                                if ($controlmethod == "single") {
                                    //k1-a10 cihazında sıradaki işlerden 2 işe birden görev oluşması nedeniyle
                                    // $moulds[] = $row_mould["mould"]; yapılması gerekebilir
                                    if (count($moulds) == 0) {
                                        $moulds[] = $row_mould["mould"]."|".$row_mould["mouldgroup"];
                                        $b_mould_exists = true;
                                    } else {
                                        if ((in_array($row_mould["mould"]."|".$row_mould["mouldgroup"], $moulds))) {
                                            $b_mould_exists = true;
                                        }
                                    }
                                } else {
                                    $f_m_e = false;//mould_exists
                                    $f_mg_e = false;//mouldgroup_exists
                                    foreach ($moulds as $item_mould) {
                                        $arr_item_mould = explode('|', $item_mould);
                                        $_m = $arr_item_mould[0];
                                        $_mg = $arr_item_mould[1];
                                        if ($f_m_e == false) {
                                            if ($_m == $row_mould["mould"]) {
                                                $f_m_e = true;
                                            }
                                        }
                                        if ($f_m_e == true) {
                                            if ($f_mg_e == false) {
                                                if ($_mg == $row_mould["mouldgroup"]) {
                                                    $f_mg_e = true;
                                                }
                                            }
                                        }
                                    }
                                    if ($f_m_e == false) {
                                        $f_group_available = true;
                                        foreach ($records_mould as $r_m_2) {
                                            $moulds[] = $r_m_2["mould"]."|".$r_m_2["mouldgroup"];
                                        }
                                    } else {
                                        if ($f_mg_e == true) {
                                            $b_mould_exists = false;
                                            $f_group_available = true;
                                        }
                                    }
                                }
                            }
                        }
                        if ($controlmethod == "multiple") {
                            if ($f_group_available) {
                                $b_mould_exists = false;
                            } else {
                                $b_mould_exists = true;
                            }
                        }
                    }
                }
                if (($b_mould_exists == false && $controlmethod == "multiple") || ($controlmethod == "single" && count($arrproductions) == 0)) {
                    $code = $row["code"];
                    $number = $row["number"];
                    $opname = $row["name"];
                    $productcount = $row["productcount"];
                    $erprefnumber = $row["erprefnumber"];
                    $ilkoperasyon = intval($row["op_pre"]) == 0;
                    $sonoperasyon = intval($row["op_after"]) == 0;
                    $oncekioperasyon = intval($row["op_pre"]) == 1;
                    $sonrakioperasyon = intval($row["op_after"]) == 1;
                    $bekleyen = $oncekioperasyon == false ? $this->bekleyenvarmi($erprefnumber, $code, $number, $row["oporder"]) : false;
                    if (!$bekleyen) {
                        //operasyon için gerekli malzemeler sorgulanıyor
                        $sql_pt_r = "SELECT * 
                            ,COALESCE(preptime,10)*60 preptimeseconds
                            ,CURRENT_TIMESTAMP(0) starttime
                            ,CURRENT_TIMESTAMP(0) + (COALESCE(preptime,10) * interval '1 minute') targettime
                        from product_trees pt 
                        where pt.finish is null 
                            and COALESCE(pt.carrierfeeder,'')<>'' 
                            -- and COALESCE(pt.feeder,'K')<>'K' 
                            and pt.materialtype in ('H','K') 
                            and code=:code
                            and number=:number
                            ".($this->customer == 'SAHINCE' ? " and pt.isdefault=true " : "")."
                        order by pt.materialtype,pt.stockcode";
                        $stmt_pt_r = $conn->prepare($sql_pt_r);
                        $stmt_pt_r->bindValue('code', $code);
                        $stmt_pt_r->bindValue('number', $number);
                        $stmt_pt_r->execute();
                        $records_pt_r = $stmt_pt_r->fetchAll();
                        if (count($records_pt_r) > 0) {
                            foreach ($records_pt_r as $row_pt_r) {
                                $hazirliksuresi = $row_pt_r["preptimeseconds"];
                                $stockcode = $row_pt_r["stockcode"];
                                if ($station["kalansure"] <= $hazirliksuresi) {
                                    //ürün gereklilikleri task_cases tablosundan kontrol ediliyor
                                    if ($row_pt_r["materialtype"] === 'H') {
                                        $sql_tc_i = "SELECT *
                                            ,ceil((case when finish is null then quantityremaining else 0 end))kalanadetaktifkasa
                                        from task_cases 
                                        where movementdetail is not null 
                                            and status in ('WAIT','WORK')
                                            and casetype='I' 
                                            and client=:client 
                                            and erprefnumber=:erprefnumber 
                                            and opcode=:code 
                                            and opnumber=:number 
                                            and stockcode=:stockcode 
                                        order by id desc
                                        limit 1";
                                        $stmt_tc = $conn->prepare($sql_tc_i);
                                        $stmt_tc->bindValue('client', $client);
                                        $stmt_tc->bindValue('erprefnumber', $erprefnumber);
                                        $stmt_tc->bindValue('code', $code);
                                        $stmt_tc->bindValue('number', $number);
                                        $stmt_tc->bindValue('stockcode', $stockcode);
                                        $stmt_tc->execute();
                                    } else {
                                        $sql_tc_o = "SELECT tc.*
                                            ,ceil(case when finish is null then (tc.packcapacity-tc.quantityremaining)/tc.quantityused else tc.packcapacity end) kalanadetaktifkasa
                                        from task_cases tc
                                        where tc.movementdetail is not null 
                                            and tc.status in ('WAIT','WORK')
                                            and tc.casetype='O' 
                                            and tc.client=:client 
                                            and tc.erprefnumber=:erprefnumber 
                                            and tc.opcode=:code 
                                            and tc.opnumber=:number
                                        order by tc.quantityremaining asc
                                        limit 1";
                                        $stmt_tc = $conn->prepare($sql_tc_o);
                                        $stmt_tc->bindValue('client', $client);
                                        $stmt_tc->bindValue('erprefnumber', $erprefnumber);
                                        $stmt_tc->bindValue('code', $code);
                                        $stmt_tc->bindValue('number', $number);
                                        $stmt_tc->execute();
                                    }
                                    $records_tc = $stmt_tc->fetchAll();
                                    if (count($records_tc) == 0) {
                                        //hat başı kasa yok, getirme için hareket var mı?
                                        if ($row_pt_r["materialtype"] === 'H') {
                                            $sql_cm = "SELECT * 
                                            from case_movements 
                                            where actualfinishtime is null 
                                                and tasktype in ('GRV002','GRV004')
                                                and locationdestination=:locationdestination 
                                                and erprefnumber=:erprefnumber 
                                                and code=:code 
                                                and number=:number 
                                                and stockcode=:stockcode";
                                            $stmt_cm = $conn->prepare($sql_cm);
                                            $stmt_cm->bindValue('locationdestination', $client);
                                            $stmt_cm->bindValue('erprefnumber', $erprefnumber);
                                            $stmt_cm->bindValue('code', $code);
                                            $stmt_cm->bindValue('number', $number);
                                            $stmt_cm->bindValue('stockcode', $stockcode);
                                            $stmt_cm->execute();
                                        } else {
                                            $sql_cm = "SELECT * 
                                            from case_movements 
                                            where actualfinishtime is null 
                                                and tasktype in ('GRV001')
                                                and locationdestination=:locationdestination 
                                                and erprefnumber=:erprefnumber 
                                                and code=:code 
                                                and number=:number 
                                                and casename=:casename";
                                            $stmt_cm = $conn->prepare($sql_cm);
                                            $stmt_cm->bindValue('locationdestination', $client);
                                            $stmt_cm->bindValue('erprefnumber', $erprefnumber);
                                            $stmt_cm->bindValue('code', $code);
                                            $stmt_cm->bindValue('number', $number);
                                            $stmt_cm->bindValue('casename', $row_pt_r["pack"]);
                                            $stmt_cm->execute();
                                        }
                                        $records_cm = $stmt_cm->fetchAll();
                                        if (count($records_cm) == 0) {
                                            if ($row_pt_r["materialtype"] === 'H') {
                                                $adres = "";
                                                $lot = "";
                                                $kasano = "";
                                                if ($this->customer == 'ERMETAL') {
                                                    if ($this->pc != "192.168.1.40") {
                                                        $qry = "SELECT top 1 ffambh_kod 'ffambh_kod', ffambh_lot 'ffambh_lot', ffambh_kasa 'ffambh_kasa' 
                                                        FROM pub.ffambh_det 
                                                        WHERE ffambh_domain='ERMETAL' and ffambh_part = '".$stockcode."' 
                                                        order by ffambh_lot WITH(NOLOCK)";
                                                        $data = $this->queryqad($this->conn_ozlm, $qry);
                                                        if (count($data) > 0) {
                                                            $data = $data[0];
                                                            //if($dbg==1)mjd($data);
                                                            $adres = $data["ffambh_kod"];
                                                            $lot = $data["ffambh_lot"];
                                                            $kasano = $data["ffambh_kasa"];
                                                        }
                                                    } else {
                                                        $adres = "YMID1";
                                                        $lot = "U150706.2292";
                                                    }
                                                }
                                                $gorevtip = $row_pt_r["feeder"] == "P" ? "GRV004" : "GRV002";
                                                $arrdata = array(
                                                    "code" => $code
                                                    ,"stockcode" => $stockcode
                                                    ,"number" => $number
                                                    ,"casename" => $row_pt_r["pack"]
                                                    ,"locationsource" => ($adres !== "" || $lot !== "" ? $adres."<br>".$lot : "")
                                                    ,"starttime" => $row_pt_r["starttime"]
                                                    ,"targettime" => $row_pt_r["targettime"]
                                                    ,"locationdestination" => $client
                                                    ,"erprefnumber" => $erprefnumber
                                                    ,"client" => $client
                                                    ,"quantitynecessary" => intval($row_pt_r["packcapacity"])
                                                    ,"quantityready" => (isset($this->stoklar[$stockcode]) ? $this->stoklar[$stockcode]["stok"] : null)
                                                    ,"carrierfeeder" => $row_pt_r["carrierfeeder"]
                                                    ,"carrierdrainer" => $row_pt_r["carrierdrainer"]
                                                    ,"casenumbersource" => $kasano
                                                );
                                                $this->hareketolustur($arrdata, $gorevtip, "NJ-1");
                                            } else {
                                                $gorevtip = "GRV001";
                                                $arrdata = array(
                                                    "code" => $code
                                                    ,"stockcode" => $row_pt_r["stockname"]
                                                    ,"number" => $number
                                                    ,"casename" => $row_pt_r["pack"]
                                                    ,"locationsource" => $this->boshammaddekasalokasyon
                                                    ,"starttime" => $row_pt_r["starttime"]
                                                    ,"targettime" => $row_pt_r["targettime"]
                                                    ,"locationdestination" => $client
                                                    ,"client" => $client
                                                    ,"erprefnumber" => $erprefnumber
                                                    ,"quantitynecessary" => intval($row_pt_r["packcapacity"])
                                                    ,"carrierfeeder" => $row_pt_r["carrierfeeder"]
                                                    ,"carrierdrainer" => $row_pt_r["carrierdrainer"]
                                                );
                                                $this->hareketolustur($arrdata, $gorevtip, "NJ-2");
                                            }
                                        }
                                    }
                                }
                            }
                            if ($oncekioperasyon) {
                                $sql_onceki_ps = "SELECT pt.*
                                    ,c.feeder c_feeder,c.carrierfeeder c_carrierfeeder,c.carrierdrainer c_carrierdrainer
                                    ,COALESCE(pt.preptime,".$this->boskasasure.")*60 preptimeseconds,CURRENT_TIMESTAMP(0) starttime
                                    ,current_timestamp(0) + (COALESCE(pt.preptime,".$this->boskasasure.") * interval '1 minute') targettime 
                                from product_trees pt
                                join (
                                    SELECT *
                                    from product_trees pt1
                                    where pt1.finish is null 
                                        and pt1.materialtype='O'
                                        and pt1.code=:code 
                                        and pt1.oporder<:oporder
                                    order by pt1.oporder desc limit 1
                                )a on a.code=pt.code and a.number=pt.number
                                join (
                                    SELECT pt2.* 
                                    from product_trees pt2
                                    join (
                                        SELECT *
                                        from product_trees pt1
                                        where pt1.finish is null 
                                            and pt1.materialtype='O'
                                            and pt1.code=:code 
                                            and pt1.oporder=:oporder
                                        order by pt1.oporder desc limit 1
                                    )b on b.code=pt2.code and b.number=pt2.number
                                    where pt2.finish is null 
                                        and pt2.materialtype='Y'
                                )c on c.code=pt.code and c.stockcode=pt.stockname
                                join production_storages ps on ps.code=pt.code and ps.stockcode=pt.stockname
                                where pt.finish is null 
                                    and ps.finish is null 
                                    and pt.materialtype='K' 
                                    and pt.stockcode<>pt.stockname 
                                    and ps.erprefnumber=:erprefnumber
                                limit 1";//çıktı kasasının ambara götürülmeme koşulu pt.stockcode<>pt.stockname
                                $stmt_onceki_ps = $conn->prepare($sql_onceki_ps);
                                $stmt_onceki_ps->bindValue('code', $code);
                                $stmt_onceki_ps->bindValue('oporder', $row["oporder"]);
                                $stmt_onceki_ps->bindValue('erprefnumber', $erprefnumber);
                                $stmt_onceki_ps->execute();
                                $records_onceki_ps = $stmt_onceki_ps->fetchAll();
                                if (count($records_onceki_ps) > 0) {
                                    foreach ($records_onceki_ps as $row5) {
                                        //üretim stok alanında olup,istasyon başında olmayan yarımamül için hareket kaydı olup olmadığı kontrol ediliyor
                                        if ($station["kalansure"] <= $row5["preptimeseconds"]) {
                                            $sql_tc_onceki = "SELECT * 
                                            from task_cases 
                                            where movementdetail is not null 
                                                and casetype='I' 
                                                and status in ('WAIT','WORK')
                                                and erprefnumber=:erprefnumber 
                                                and opcode=:code 
                                                and opnumber=:number 
                                                and stockcode=:stockcode";
                                            $stmt_tc_onceki = $conn->prepare($sql_tc_onceki);
                                            $stmt_tc_onceki->bindValue('erprefnumber', $erprefnumber);
                                            $stmt_tc_onceki->bindValue('code', $code);
                                            $stmt_tc_onceki->bindValue('number', $number);
                                            $stmt_tc_onceki->bindValue('stockcode', $row5["stockname"]);
                                            $stmt_tc_onceki->execute();
                                            $records_tc_onceki = $stmt_tc_onceki->fetchAll();
                                            if (count($records_tc_onceki) == 0) {
                                                $sql_cm_onceki = "SELECT * 
                                                from case_movements 
                                                where actualfinishtime is null 
                                                    and locationsource=:locationsource 
                                                    and locationdestination=:locationdestination 
                                                    and erprefnumber=:erprefnumber 
                                                    and code=:code 
                                                    and number=:number 
                                                    and stockcode=:stockcode";
                                                $stmt_cm_onceki = $conn->prepare($sql_cm_onceki);
                                                $stmt_cm_onceki->bindValue('locationsource', $this->urtmamb);
                                                $stmt_cm_onceki->bindValue('locationdestination', $client);
                                                $stmt_cm_onceki->bindValue('erprefnumber', $erprefnumber);
                                                $stmt_cm_onceki->bindValue('code', $code);
                                                $stmt_cm_onceki->bindValue('number', $number);
                                                $stmt_cm_onceki->bindValue('stockcode', $row5["stockname"]);
                                                $stmt_cm_onceki->execute();
                                                $records_cm_onceki = $stmt_cm_onceki->fetchAll();
                                                if (count($records_cm_onceki) == 0) {
                                                    $gorevtip = "GRV012";
                                                    $arrdata = array(
                                                        "code" => $code
                                                        ,"stockcode" => $row5["stockname"]
                                                        ,"number" => $number
                                                        ,"casename" => $row5["pack"]
                                                        ,"locationsource" => $this->urtmamb
                                                        ,"starttime" => $row5["starttime"]
                                                        ,"targettime" => $row5["targettime"]
                                                        ,"locationdestination" => $client
                                                        ,"erprefnumber" => $erprefnumber
                                                        ,"client" => $client
                                                        ,"quantitynecessary" => intval($row5["packcapacity"])
                                                        ,"carrierfeeder" => $row5["c_carrierfeeder"]
                                                        ,"carrierdrainer" => $row5["c_carrierdrainer"]
                                                    );
                                                    $lid = $this->hareketolustur($arrdata, $gorevtip, "NJ-3");
                                                    $sql_ps_3 = "select * 
                                                    from production_storages
                                                    where casemovementidout is null 
                                                        and finish is null 
                                                        and code=:code 
                                                        and number=:number 
                                                        and stockcode=:stockcode 
                                                        and erprefnumber=:erprefnumber
                                                    order by casemovementidin asc limit 1
                                                    ";
                                                    $stmt_ps_3 = $conn->prepare($sql_ps_3);
                                                    $stmt_ps_3->bindValue('code', $code);
                                                    $stmt_ps_3->bindValue('number', $number);
                                                    $stmt_ps_3->bindValue('stockcode', $row5["stockname"]);
                                                    $stmt_ps_3->bindValue('erprefnumber', $erprefnumber);
                                                    $stmt_ps_3->execute();
                                                    $records_ps_3 = $stmt_ps_3->fetchAll();
                                                    if (count($records_ps_3) > 0) {
                                                        $sql_ps_4 = "update production_storages
                                                            set casemovementidout=:id_out
                                                            ,update_user_id=1
                                                            ,updated_at=current_timestamp()
                                                            where id=:id";
                                                        $stmt_ps_4 = $conn->prepare($sql_ps_4);
                                                        $stmt_ps_4->bindValue('id_out', $lid);
                                                        $stmt_ps_4->bindValue('id', $records_ps_3[0]["id"]);
                                                        $stmt_ps_4->execute();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $this->errinsert("uretimstokkayityok", "erprefnumber:$erprefnumber $code-$number production__storages tablosunda bu erprefnumber için ara stok bulunamadı.");
                                }
                            }
                            if (!(in_array($opname, $arrproductions))) {
                                $arrproductions[] = $opname;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @Route(path="/MaterialPrepare/CaseMovements/{id}",requirements={"id": "\d+"}, name="MaterialPrepare-CaseMovements", options={"expose"=true}, methods={"GET"})
     */
    public function getCaseMovementsAction(Request $request, $_locale, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql_c = "SELECT c.id,c.code,(SELECT finish from carrier_sessions cs where cs.carrier=c.code order by cs.start desc limit 1)finish,(SELECT empcode from carrier_sessions cs where cs.carrier=c.code order by cs.start desc limit 1)empcode from carriers c order by c.code";
        $stmt_c = $conn->prepare($sql_c);
        $stmt_c->execute();
        $sql_cm_2 = "SELECT cm.*,to_char(cm.starttime, 'HH24:MI') as cagrizamani 
        	,case when cm.tasktype='WAITFORMATERIAL' then 'ERP STOK YOK' else COALESCE(mtt.label,cm.tasktype) end islemtip
            ,case when cm.tasktype in ('GRV001','GRV011') then cm.casename else cm.stockcode end bilesen2 
            ,case when cm.targettime>current_timestamp then 
                TO_CHAR(( round(extract(epoch from (cm.targettime::timestamp - current_timestamp::timestamp))) || ' second')::interval, 'HH24:MI:SS') 
            else concat('-',TO_CHAR(( round(extract(epoch from (current_timestamp::timestamp - cm.targettime::timestamp))) || ' second')::interval, 'HH24:MI:SS'))
            end	kalansure 
            ,cr.id carrierid
        from case_movements cm 
        left join carriers cr on cr.code=cm.carrier 
        left join movement_task_types mtt on mtt.tasktype=cm.tasktype 
        where cm.actualfinishtime is null and cm.status in ('WAITFORASSIGNMENT','WAITFORDELIVERY','WAITFORMATERIAL') ".($id > 0 ? " and cr.id=:id " : "")."
        order by case when cm.carrier is null then 1 else 0 end,cm.targettime,cm.carrier,cm.locationdestination,cm.erprefnumber";
        $stmt_cm_2 = $conn->prepare($sql_cm_2);
        if (intval($id) > 0) {
            $stmt_cm_2->bindValue('id', $id);
        }
        $stmt_cm_2->execute();
        $respm = array(
            'title' => '',
            'success' => true,
            'carriers' => $stmt_c->fetchAll(),
            'data' => $stmt_cm_2->fetchAll(),
            'errno' => 'id:'.$id
        );
        return new JsonResponse($respm, 200);
    }

    /**
     * @Route(path="/MaterialPrepare/CaseMovementsLocation/{id}",requirements={"id": "\d+"}, name="MaterialPrepare-CaseMovementsLocation", options={"expose"=true}, methods={"GET"})
     */
    public function getCaseMovementsLocationAction(Request $request, $_locale, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $carriers = array("'MH03'","'PM02'","'PM03'","'DF03'","'RT03'");
        if ($id == 3) {
            $carriers = array("'MH03'","'PM02'","'PM03'","'DF03'","'RT03'");
        }
        if ($id == 1) {
            $carriers = array("'PM04'","'RT02'");
        }
        $sql_c = "SELECT c.id,c.code,(SELECT finish from carrier_sessions cs where cs.carrier=c.code order by cs.start desc limit 1)finish,(SELECT empcode from carrier_sessions cs where cs.carrier=c.code order by cs.start desc limit 1)empcode 
        from carriers c 
        where 1=1 and c.code in (".implode(',', $carriers).")
        order by c.code";
        $stmt_c = $conn->prepare($sql_c);
        //$stmt_c->bindValue('codes', array_values($carriers));
        $stmt_c->execute();
        $sql_cm_2 = "SELECT cm.*,to_char(cm.starttime, 'HH24:MI') as cagrizamani 
        	,case when cm.tasktype='WAITFORMATERIAL' then 'ERP STOK YOK' else COALESCE(mtt.label,cm.tasktype) end islemtip
            ,case when cm.tasktype in ('GRV001','GRV011') then cm.casename else cm.stockcode end bilesen2 
            ,case when cm.targettime>current_timestamp then 
                TO_CHAR(( round(extract(epoch from (cm.targettime::timestamp - current_timestamp::timestamp))) || ' second')::interval, 'HH24:MI:SS') 
            else concat('-',TO_CHAR(( round(extract(epoch from (current_timestamp::timestamp - cm.targettime::timestamp))) || ' second')::interval, 'HH24:MI:SS'))
            end	kalansure 
            ,cr.id carrierid
        from case_movements cm 
        left join carriers cr on cr.code=cm.carrier 
        left join movement_task_types mtt on mtt.tasktype=cm.tasktype 
        where cm.actualfinishtime is null and cm.status in ('WAITFORASSIGNMENT','WAITFORDELIVERY','WAITFORMATERIAL') and cm.carrier in (".implode(',', $carriers).")
        order by case when cm.carrier is null then 1 else 0 end,cm.carrier,cm.locationdestination,cm.erprefnumber";
        $stmt_cm_2 = $conn->prepare($sql_cm_2);
        //$stmt_cm_2->bindValue('codes', array_values($carriers));
        $stmt_cm_2->execute();
        $respml = array(
            'title' => '',
            'success' => true,
            'carriers' => $stmt_c->fetchAll(),
            'data' => $stmt_cm_2->fetchAll(),
            'errno' => 'id:'.$id
        );
        return new JsonResponse($respml, 200);
    }

    /**
     * @Route(path="/MaterialPrepare/ForkliftBekleme", name="MaterialPrepare-ForkliftBekleme", options={"expose"=true}, methods={"GET"})
     */
    public function getForkliftBeklemeAction(Request $request, $_locale)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //$sql_cm_2="SELECT * from client_lost_details where type='l_c' and losttype in ('FORKLİFT BEKLEME','KALIP BEKLEME (SETUP)','HURDA KASASI BEKLEME','BOŞ KASA BEKLEME','YARIMAMUL BEKLEME') and finish is null and day>CURRENT_DATE- interval '3 day' order by start";
        $sql_cm_2 = "SELECT min(id) id,client,day,jobrotation,losttype,start,string_agg( opname,',') opname 
        from client_lost_details 
        where type in ('l_c','l_c_t') and losttype in (SELECT losttype from lost_group_details where lostgroupcode='Lojistik Kayıpları') and finish is null and day>CURRENT_DATE- interval '3 day' 
        group by client,day,jobrotation,losttype,start order by start";
        $stmt_cm_2 = $conn->prepare($sql_cm_2);
        $stmt_cm_2->execute();
        $respfb = array(
            'title' => '',
            'success' => true,
            'data' => $stmt_cm_2->fetchAll()
        );
        return new JsonResponse($respfb, 200);
    }

    /**
     * @Route(path="/MaterialPrepare/WaitForMaterial", name="MaterialPrepare-WaitForMaterial", options={"expose"=true}, methods={"GET"})
     */
    public function getWaitForMaterialAction(Request $request, $_locale)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql_cm_2 = "SELECT cm.*,to_char(cm.starttime, 'HH24:MI') as cagrizamani 
        	,case when cm.tasktype='WAITFORMATERIAL' then 'ERP STOK YOK' else COALESCE(mtt.label,cm.tasktype) end islemtip
            ,case when cm.tasktype in ('GRV001','GRV011') then cm.casename else cm.stockcode end bilesen2 
            ,case when cm.targettime>current_timestamp then 
                TO_CHAR(( round(extract(epoch from (cm.targettime::timestamp - current_timestamp::timestamp))) || ' second')::interval, 'HH24:MI:SS') 
            else concat('-',TO_CHAR(( round(extract(epoch from (current_timestamp::timestamp - cm.targettime::timestamp))) || ' second')::interval, 'HH24:MI:SS'))
            end	kalansure 
            ,cr.id carrierid
        from case_movements cm 
        left join carriers cr on cr.code=cm.carrier 
        left join movement_task_types mtt on mtt.tasktype=cm.tasktype 
        where cm.actualfinishtime is null and cm.status in ('WAITFORMATERIAL') 
        order by case when cm.carrier is null then 1 else 0 end,cm.carrier,cm.locationdestination,cm.erprefnumber";
        $stmt_cm_2 = $conn->prepare($sql_cm_2);
        $stmt_cm_2->execute();
        $respwm = array(
            'title' => '',
            'success' => true,
            'data' => $stmt_cm_2->fetchAll()
        );
        return new JsonResponse($respwm, 200);
    }

    public function bekleyenvarmi($erprefnumber, $code, $number, $oporder)
    {
        $dbg = 0;
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        try {
            $sql_1 = "SELECT *
            from case_movements 
            where erprefnumber=:erprefnumber and actualfinishtime is null and tasktype='WAITFORMATERIAL'";
            $stmt_1 = $conn->prepare($sql_1);
            $stmt_1->bindValue('erprefnumber', $erprefnumber);
            $stmt_1->execute();
            $records_1 = $stmt_1->fetchAll();
            if (count($records_1) > 0) {
                return true;
            }
            $sql_2 = "SELECT pt.* 
            from product_trees pt
            join (
                SELECT *
                from product_trees pt1
                where pt1.finish is null 
                    and pt1.materialtype='O' 
                    and pt1.code=:code 
                    and pt1.oporder<:oporder
                order by pt1.oporder desc limit 1
            )a on a.code=pt.code and a.number=pt.number
            where pt.finish is null and pt.materialtype='K' and pt.stockcode<>pt.stockname";//çıktı kasasının ambara götürülmeme koşulu pt.stockcode<>pt.stockname
            $stmt_2 = $conn->prepare($sql_2);
            $stmt_2->bindValue('code', $code);
            $stmt_2->bindValue('oporder', $oporder);
            $stmt_2->execute();
            $records_2 = $stmt_2->fetchAll();
            foreach ($records_2 as $row) {
                $sql_3 = "SELECT *
                from production_storages 
                where finish is null and code=:code and erprefnumber=:erprefnumber and stockcode=:stockname";
                $stmt_3 = $conn->prepare($sql_3);
                $stmt_3->bindValue('code', $code);
                $stmt_3->bindValue('erprefnumber', $erprefnumber);
                $stmt_3->bindValue('stockname', $row["stockname"]);
                $stmt_3->execute();
                $records_3 = $stmt_3->fetchAll();
                if (count($records_3) == 0) {
                    if ($dbg == 1) {
                        echo "$erprefnumber-$code-$number-".$row["stockname"]." bekleyen var.\n";
                    }
                    return true;
                }
            }
            return false;
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->errinsert("bekleyenvarmi-err", "message:".$e->getMessage(), 101);
        }
    }

    public function dosyasil($path, $pos)
    {
        if ($this->pc === "172.16.1.142") {
            return;
        }
        if ($this->customer != 'ERMETAL') {
            return;
        }
        try {
            if ($path != "" && file_exists($path)) {
                $this->errinsert("f-d-ok-".$pos, "path:".$path, 202);
                unlink($path);
            } else {
                $this->errinsert("f-d-nf-".$pos, "path:".$path, 203);
            }
        } catch (Exception $e) {
            //echo("Dosya silme hatası! ".$e->getMessage()."\n");
            $this->errinsert("f-d-err-".$pos, "path:".$path." message:".$e->getMessage(), 204);
        }
    }

    public function dosyayaz($station, $bilesen, $tasiyicikod, $t, $pos)
    {
        if ($this->pc === "172.16.1.142") {
            return;
        }
        if ($this->customer != 'ERMETAL') {
            return;
        }
        try {
            $bilesen = str_replace("/", ";", $bilesen);
            if ($this->pc != "172.16.1.142" && $this->pc != "192.168.1.40") {
                $exportFileName = $this->exportpath."$bilesen#$station#$tasiyicikod#$t.txt";
            } else {
                $exportFileName = $this->importpathnode."_export/mh/$bilesen#$station#$tasiyicikod#$t.txt";
            }
            $exportFile = fopen($exportFileName, 'w');
            # Now UTF-8 - Add byte order mark
            fwrite($exportFile, pack("CCC", 0xef, 0xbb, 0xbf));
            fclose($exportFile);
            $this->errinsert("f-w-ok-".$pos, "path:".$exportFileName, 205);
        } catch (Exception $e) {
            $this->errinsert("f-w-err-".$pos, "station:".$station." Bileşen:".$bilesen." tasiyicikod:".$tasiyicikod." t:".$t." message:".$e->getMessage(), 206);
            //echo("Dosya yazma hatası! ".$e->getMessage()."\n");
        }
    }

    public function dosyayaznodetrigger($data, $pos)
    {
        if ($this->pc === "172.16.1.142") {
            return;
        }
        try {
            $t = round(microtime(true) * 1000);
            $exportFileName = '';
            //$exportFileName2='';
            if ($this->pc != "172.16.1.142" && $this->pc != "192.168.1.40") {
                $exportFileName = $this->importpathnode."_import\mh_tasimagorevilistele_$t.json";
                //$exportFileName2 = $this->importpathnode."_import_err\mh_tasimagorevilistele_$t.json";
            } else {
                $exportFileName = "\\\\localhost\\paylaşım\\mh\\mh_tasimagorevilistele_$t.json";
                $exportFileName = $this->importpathnode."_import\mh_tasimagorevilistele_$t.json";
            }
            if ($exportFileName !== '') {
                //echo($exportFileName);
                $exportFile = fopen($exportFileName, 'w');
                # Now UTF-8 - Add byte order mark
                //fwrite($exportFile, pack("CCC",0xef,0xbb,0xbf));
                fwrite($exportFile, $data);
                fclose($exportFile);
                //
                //$exportFileName = $this->importpathnode."_import_err\mh_tasimagorevilistele_$t.json";
                //$exportFile = fopen( $exportFileName, 'w' );
                //# Now UTF-8 - Add byte order mark
                ////fwrite($exportFile, pack("CCC",0xef,0xbb,0xbf));
                //fwrite($exportFile, $data);
                //fclose( $exportFile );
            }
            //debug için _err klasörüne yazma işlemi
            //if($exportFileName2!==''){
            //    $exportFile2 = fopen( $exportFileName2, 'w' );
            //    # Now UTF-8 - Add byte order mark
            //    //fwrite($exportFile, pack("CCC",0xef,0xbb,0xbf));
            //    fwrite($exportFile2, $data);
            //    fclose( $exportFile2 );
            //}
            $this->errinsert("f-w-ok", "path:".$exportFileName." data:".$data, $pos);
        } catch (Exception $e) {
            $this->errinsert("f-w-err-".$pos, "station:".$station." Bileşen:".$bilesen." tasiyicikod:".$tasiyicikod." t:".$t." message:".$e->getMessage(), 208);
            echo("dosyayaznodetrigger! ".$e->getMessage()."\n");
        }
    }

    public function errinsert($pos, $msg, $code = 100)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //$conn->beginTransaction();
        try {
            //$sql = "select * from _log_exception where path=:path and message=:message and time>current_timestamp - interval '1' hour";
            //$stmt = $conn->prepare($sql);
            //$stmt->bindValue('path', $pos);
            //$stmt->bindValue('message', $msg);
            //$stmt->execute();
            //$records = $stmt->fetchAll();
            //if(count($records)==0){
            $sql = "insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,current_timestamp)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('code', $code);
            $stmt->bindValue('file', '');
            $stmt->bindValue('line', 0);
            $stmt->bindValue('message', $msg);
            $stmt->bindValue('method', 'MH');
            $stmt->bindValue('path', $pos);
            $stmt->bindValue('sessionid', 0);
            $stmt->execute();
            //}
            //$conn->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return;
    }

    public function gorevlibul($carriers, $pos)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //ürün ağacından o işi yapabilecek taşıycı kodları geleceği için lokasyon kontrolü şimdilik devre dışı bırakılıyor, gerekmesi halinde açılacak
        //$sql="select * from (
        //    SELECT c.code,count(cm.id)sayi
        //    FROM carriers c
        //    join carrier_sessions cs on cs.carrier=c.code and cs.finish is null
        //    left join case_movements cm on cm.carrier=c.code and cm.actualfinishtime is null
        //    left join client_group_details cgd_cl on (cgd_cl.client=cm.locationdestination or cgd_cl.client=cm.locationsource) and cgd_cl.clientgroup='locationclient'
        //    left join client_group_details cgd_cr on cgd_cr.clientgroup='locationcarrier' and cgd_cl.clientgroupcode=cgd_cr.clientgroupcode
        //    where c.type=:type
        //    group by c.code)a
        //order by a.sayi desc limit 1";
        try {
            $name = null;
            //görev ataması sabit olarak mı yapılacak belirleniyor
            $arr_fixed_carrier = explode('|', $carriers);
            if (count($arr_fixed_carrier) > 1 && $this->customer == 'ERMETAL') {
                $fixed_carrier = array_shift($arr_fixed_carrier);
                $sql_session = "select * from carrier_sessions where finish is null and carrier=:carrier";
                $stmt_session = $conn->prepare($sql_session);
                $stmt_session->bindValue('carrier', $fixed_carrier);
                $stmt_session->execute();
                $records_session = $stmt_session->fetchAll();
                if (count($records_session) > 0) {
                    $name = $fixed_carrier;
                } else {
                    $carriers = implode(',', $arr_fixed_carrier);
                }
            }
            if ($name === null) {
                $where_carriers = $carriers == "" ? "" : " and c.code in ('".implode("','", explode(',', $carriers))."') ";
                if ($where_carriers == "") {
                    //echo($sql.'-name:'.$name.'-'.$pos.'<br>');
                    $this->errinsert($pos, "gorevlibul-carrier-empty", 103);
                } else {
                    //$sql="select * from (
                    //    SELECT c.code,count(cm.id)sayi
                    //    FROM carriers c
                    //    join carrier_sessions cs on cs.carrier=c.code and cs.finish is null
                    //    left join case_movements cm on cm.carrier=c.code and cm.actualfinishtime is null
                    //    where c.type=:type $where_carriers
                    //    group by c.code)a
                    //order by a.sayi asc limit 1";
                    $sql = "select * from (
                        SELECT c.code,count(cm.id)sayi
                        FROM carriers c
                        ".($this->customer == 'ERMETAL' ? " join carrier_sessions cs on cs.carrier=c.code and cs.finish is null " : "")."
                        left join case_movements cm on cm.carrier=c.code and cm.actualfinishtime is null and status='WAITFORDELIVERY'
                        where 1=1 $where_carriers
                        group by c.code)a
                    order by a.sayi asc limit 1";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $records = $stmt->fetchAll();
                    if (count($records) > 0) {
                        $q = $records[0];
                        $name = $q["code"];
                    }
                }
            }
            return $name;
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->errinsert("gorevlibul-err", "message:".$e->getMessage(), 101);
        }
    }

    public function hareketolustur($arrdata, $gorevtip, $position = "")
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        try {
            $time_start_10 = microtime();
            //mjd($arrdata);
            if (!isset($arrdata["status"])) {
                $arrdata["status"] = 'WAITFORASSIGNMENT';
            }
            if (!isset($arrdata["casenumbersource"])) {
                $arrdata["casenumbersource"] = '';
            }
            $arr_feed = array('GRV001','GRV002','GRV004','GRV012');
            if (in_array($gorevtip, $arr_feed)) {
                $carriers = $arrdata["carrierfeeder"];
                if ($arrdata["locationsource"] === "") {
                    $sql_cl = "select location from case_locations where stockcode=:stockcode order by id";
                    $stmt_cl = $conn->prepare($sql_cl);
                    $stmt_cl->bindValue('stockcode', $arrdata["stockcode"]);
                    $stmt_cl->execute();
                    $records_cl = $stmt_cl->fetchAll();
                    if (count($records_cl) > 0) {
                        $arrdata["locationsource"] = $records_cl[0]["location"];
                    }
                }
            } else {
                $carriers = $arrdata["carrierdrainer"];
            }
            $gorevli = $this->gorevlibul($carriers, 2);
            // if ($gorevli===null) {
            //     // 2021-07-07
            //     // atama yapılamayacak bir iş ise görev oluşturma
            //     return;
            // }
            $d = new \DateTime();
            $time = $d->format('Y-m-d H:i:s');
            $atama = ($gorevli != null ? $time : null);
            if ($gorevli != null) {
                $arrdata["status"] = 'WAITFORDELIVERY';
            }
            $arrdata["carrier"] = $gorevli;
            $arrdata["assignmenttime"] = $atama;
            $arrdata["tasktype"] = $gorevtip;
            $records_cc = [];
            //if($this->mamulambari===$arrdata["locationdestination"]){
            if ($gorevtip === "GRV005" || $gorevtip === "GRV008") {
                $sql_cc = "select * from case_calls where client=:client and erprefnumber=:erprefnumber and state=:state";
                $stmt_cc = $conn->prepare($sql_cc);
                $stmt_cc->bindValue('client', $arrdata["client"]);
                $stmt_cc->bindValue('erprefnumber', $arrdata["erprefnumber"]);
                $stmt_cc->bindValue('state', "BEKLİYOR");
                $stmt_cc->execute();
                $records_cc = $stmt_cc->fetchAll();
                if (count($records_cc) > 0) {
                    $arrdata["locationdestination"] = 'DEA';
                }
            }
            $lokasyonvaris = $arrdata["locationdestination"];
            $stockcode = $arrdata["stockcode"];

            $arrdata["position"] = $position;
            $arrdata["locationdestinationinfo"] = $arrdata["locationdestination"];
            $arrdata["locationsourceinfo"] = $arrdata["locationsource"];
            $sql_3 = "SELECT info from clients where code=:code";
            $stmt_3 = $conn->prepare($sql_3);
            $stmt_3->bindValue('code', $arrdata["locationdestination"]);
            $stmt_3->execute();
            $records_3 = $stmt_3->fetchAll();
            if (count($records_3) > 0) {
                $arrdata["locationdestinationinfo"] = $records_3[0]["info"];
            }
            $sql_4 = "select info from clients where code=:code";
            $stmt_4 = $conn->prepare($sql_4);
            $stmt_4->bindValue('code', $arrdata["locationsource"]);
            $stmt_4->execute();
            $records_4 = $stmt_4->fetchAll();
            if (count($records_4) > 0) {
                if ($records_4[0]["info"] != null && $records_4[0]["info"] != '') {
                    $arrdata["locationsourceinfo"] = $records_4[0]["info"];
                }
            }
            $t = round(microtime(true) * 1000);
            $stockcode = str_replace("/", ";", ($gorevtip == 'GRV001' || $gorevtip == 'GRV011' ? $arrdata["casename"] : $stockcode));
            $exportFileName = $this->exportpath."$stockcode#$lokasyonvaris#$gorevli#$t.txt";
            $arrdata["filepath"] = $this->customer == 'ERMETAL' ? $exportFileName : '';
            $sicil = '';
            $isim = '';
            $arrdata["empcode"] = '';
            $arrdata["empname"] = '';
            if ($gorevli != null && $this->customer == 'ERMETAL') {
                $sql_5 = "SELECT * from carrier_sessions where finish is null and carrier=:carrier";
                $stmt_5 = $conn->prepare($sql_5);
                $stmt_5->bindValue('carrier', $gorevli);
                $stmt_5->execute();
                $records_5 = $stmt_5->fetchAll();
                if (count($records_5) > 0) {
                    $q = $records_5[0];
                    $sicil = $q["empcode"];
                    $isim = $q["empname"];
                } else {
                    if ($gorevli == $this->user_mh) {
                        $sicil = $this->user_mh;
                        $isim = $this->user_mh;
                    }
                }
            }
            $arrdata["empcode"] = $sicil;
            $arrdata["empname"] = $isim;
            $sql_8 = "insert into case_movements (
                carrier,code,stockcode,number,casename
                ,locationsource,locationdestination,tasktype
                ,starttime,targettime,assignmenttime,status
                ,client,erprefnumber,quantitynecessary,quantityready
                ,locationsourceinfo,locationdestinationinfo,filepath
                ,position,carrierfeeder,carrierdrainer
                ,empcode,empname,casenumbersource)
            values (
                :carrier,:code,:stockcode,:number,:casename
                ,:locationsource,:locationdestination,:tasktype
                ,:starttime,:targettime,:assignmenttime,:status
                ,:client,:erprefnumber,:quantitynecessary,:quantityready
                ,:locationsourceinfo,:locationdestinationinfo,:filepath
                ,:position,:carrierfeeder,:carrierdrainer
                ,:empcode,:empname,:casenumbersource)";
            $stmt_8 = $conn->prepare($sql_8);
            $stmt_8->bindValue('carrier', $arrdata["carrier"]);
            $stmt_8->bindValue('code', $arrdata["code"]);
            $stmt_8->bindValue('stockcode', $arrdata["stockcode"]);
            $stmt_8->bindValue('number', $arrdata["number"]);
            $stmt_8->bindValue('casename', $arrdata["casename"]);

            $stmt_8->bindValue('locationsource', $arrdata["locationsource"]);
            $stmt_8->bindValue('locationdestination', $arrdata["locationdestination"]);
            $stmt_8->bindValue('tasktype', $arrdata["tasktype"]);

            $stmt_8->bindValue('starttime', $arrdata["starttime"]);
            $stmt_8->bindValue('targettime', $arrdata["targettime"]);
            $stmt_8->bindValue('assignmenttime', $arrdata["assignmenttime"]);
            $stmt_8->bindValue('status', $arrdata["status"]);

            $stmt_8->bindValue('client', $arrdata["client"]);
            $stmt_8->bindValue('erprefnumber', $arrdata["erprefnumber"]);
            $stmt_8->bindValue('quantitynecessary', intval($arrdata["quantitynecessary"]));
            if (isset($arrdata["quantityready"])) {
                $stmt_8->bindValue('quantityready', intval($arrdata["quantityready"]) < 0 ? 0 : intval($arrdata["quantityready"]));
            } else {
                $stmt_8->bindValue('quantityready', 0);
            }
            $stmt_8->bindValue('locationsourceinfo', $arrdata["locationsourceinfo"]);
            $stmt_8->bindValue('locationdestinationinfo', $arrdata["locationdestinationinfo"]);
            $stmt_8->bindValue('filepath', $arrdata["filepath"]);

            $stmt_8->bindValue('position', $arrdata["position"]);
            $stmt_8->bindValue('carrierfeeder', $arrdata["carrierfeeder"]);
            $stmt_8->bindValue('carrierdrainer', $arrdata["carrierdrainer"]);

            $stmt_8->bindValue('empcode', $arrdata["empcode"]);
            $stmt_8->bindValue('empname', $arrdata["empname"]);
            $stmt_8->bindValue('casenumbersource', $arrdata["casenumbersource"]);
            $stmt_8->execute();
            $lastId = $conn->lastInsertId();
            if (count($records_cc) > 0) {
                $sql_cc_u = "update case_calls 
                    set casemovementid=:casemovementid
                        ,starttime=:starttime
                        ,state=:state
                    where id=:id";
                $stmt_cc_u = $conn->prepare($sql_cc_u);
                $stmt_cc_u->bindValue('casemovementid', $lastId);
                $stmt_cc_u->bindValue('starttime', $arrdata["starttime"]);
                $stmt_cc_u->bindValue('state', 'TAŞINIYOR');
                $stmt_cc_u->bindValue('id', $records_cc[0]["id"]);
                $stmt_cc_u->execute();
            }
            if ($gorevli != null) {
                if (!(in_array($gorevli, $this->arrnodeinfo))) {
                    $this->arrnodeinfo[] = $gorevli;
                }
                $sql_9 = "insert into case_movement_details (
                    casemovementid,carrier,assignmenttime,empcode,empname)
                    values (:casemovementid,:carrier,:assignmenttime,:empcode,:empname)
                ";
                $stmt_9 = $conn->prepare($sql_9);
                $stmt_9->bindValue('casemovementid', $lastId);
                $stmt_9->bindValue('carrier', $arrdata["carrier"]);
                $stmt_9->bindValue('assignmenttime', $arrdata["assignmenttime"]);
                $stmt_9->bindValue('empcode', $arrdata["empcode"]);
                $stmt_9->bindValue('empname', $arrdata["empname"]);
                $stmt_9->execute();
                $this->dosyayaz($lokasyonvaris, $stockcode, $gorevli, $t, "2-".$position);
                //görevliye yeni bir iş ataması yapılıyor, tasiyicibossure tablosunda açık kaydı var ise kapatılacak
                $sql_10 = "update carrier_empty_sessions set finish=current_timestamp where finish is null and carrier=:carrier and empcode=:empcode";
                $stmt_10 = $conn->prepare($sql_10);
                $stmt_10->bindValue('carrier', $arrdata["carrier"]);
                $stmt_10->bindValue('empcode', $arrdata["empcode"]);
                $stmt_10->execute();
            }
            $this->errinsert("hareketolustur-ok", json_encode($arrdata), 200);
            return $lastId;
        } catch (\OutOfMemoryException $ome) {
            echo $ome->getMessage();
            $this->errinsert("hareketolustur-err", "message:".$ome->getMessage(), 101);
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->errinsert("hareketolustur-err", "message:".$e->getMessage(), 101);
        }
    }

    public function mhstokkontrol2()
    {
        $d = new \DateTime();
        $time = $d->format('Y-m-d H:i:s');
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        try {
            $t = microtime(true);
            $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
            $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));

            $_d = $d->format("Y-m-d H:i:s.u");
            $ts_1 = microtime();
            $sql = "
            select s1.*,cast(s1.quantity*s1.productcount as int) gerekenmiktar,cast(s1.quantity*s1.packcapacity as int) gerekenmiktarmin
            from 
            (	select tl1.id,tl1.plannedstart,tl1.deadline,tl1.erprefnumber,tl1.client,tl1.opcode,tl1.opnumber,tl1.opname,tl1.productcount
                    ,(SELECT max(packcapacity)packcapacity from product_trees where finish is null and materialtype='K' and name=tl1.opname)packcapacity,a.info
                    ,pt.stockcode,pt.quantity,pt.pack
                from task_lists tl1 
                join (SELECT tl.client,tl.erprefnumber,tl.opname,max(tl.productdonecount)productdonecount,c.info,min(tl.plannedstart)plannedstart
                    from task_lists tl
                    join clients c on c.code=tl.client
                    join client_params cp on cp.client=c.code and cp.name='task_materialpreparation' and cp.value='true'
                    ".($this->customer == 'ERMETAL' ? " join case_labels cl on cl.erprefnumber=tl.erprefnumber " : "")."
                    where tl.finish is null and tl.delete_user_id is null and tl.productcount>tl.productdonecount and cl.status='OPEN' and tl.plannedstart>CURRENT_DATE - interval '30 day'
                    group by tl.client,tl.erprefnumber,tl.opname,c.info )a on a.client=tl1.client and a.erprefnumber=tl1.erprefnumber and a.opname=tl1.opname and a.productdonecount=tl1.productdonecount
                join product_trees pt on pt.finish is null and pt.materialtype in ('H') and pt.stockcode not ilike 'st%' and pt.name=tl1.opname
                where tl1.finish is null and tl1.delete_user_id is null and tl1.productcount>tl1.productdonecount
                    and exists(
                        SELECT pt_h.* 
                        from product_trees pt_h 
                        where pt_h.finish is null and pt_h.materialtype in ('H') and pt_h.name=tl1.opname)
                    and exists(
                        SELECT pt_k.* 
                        from product_trees pt_k 
                        where pt_k.finish is null and pt_k.materialtype in ('K') and pt_k.name=tl1.opname)
                    and not exists (
                        SELECT * 
                        from product_trees pt1 
                        join product_trees pt2 on pt2.code=pt1.code and pt2.materialtype=pt1.materialtype and pt2.finish is null and pt2.oporder<pt1.oporder 
                        where pt1.name=tl1.opname and pt1.finish is null and pt1.materialtype='O')
                    and not exists (
                        SELECT 'kh' tip,id 
                        from case_movements 
                        where code=tl1.opcode and number=tl1.opnumber and erprefnumber=tl1.erprefnumber and actualfinishtime is null and tasktype='WAITFORMATERIAL')
            )s1
            order by s1.stockcode,s1.plannedstart,s1.deadline,s1.id
            ";
            $stmt_tl = $conn->prepare($sql);
            $stmt_tl->execute();
            $tmp_arr_stockcode = array();
            $records_tl = $stmt_tl->fetchAll();
            foreach ($records_tl as $row) {
                $stockcode = $row["stockcode"];
                $gerekenmiktar = $row["gerekenmiktar"];
                $gerekenmiktarmin = $row["gerekenmiktarmin"];
                if (!in_array($stockcode, $tmp_arr_stockcode)) {
                    $tmp_arr_stockcode[] = $stockcode;
                }
                if (!isset($this->stoklar[$stockcode])) {
                    $this->stoklar[$stockcode] = array(
                        "toplamgerekenmiktar" => $gerekenmiktar
                        ,"toplamgerekenmiktarmin" => $gerekenmiktarmin
                        ,"gerekenmiktarmin" => $gerekenmiktarmin
                        ,"serbestmiktar" => 0
                        ,"stok" => 0
                        ,"erpmiktar" => 0
                        ,"hatbasigirdistok" => 0
                        ,"farkliiskalanmiktar" => 0
                        ,"details" => array(
                            $row
                        )
                        //,"data"=>array(array(
                        //    "code"=>$opcode
                        //    ,"stockcode"=>$stockcode
                        //    ,"number"=>$opnumber
                        //    ,"casename"=>$rowstok["pack"]
                        //    ,"starttime"=>$time
                        //    ,"locationdestination"=>$client
                        //    ,"tasktype"=>'WAITFORMATERIAL'
                        //    ,"status"=>'WAITFORMATERIAL'
                        //    ,"client"=>$client
                        //    ,"erprefnumber"=>$erprefnumber
                        //    ,"quantitynecessary"=>0
                        //    ,"quantitynecessarymin"=>$gerekenmiktarmin
                        //    ,"quantityready"=>(($qadquantity-$hatbasigirdistok-$farkliiskalanmiktar)>0?($qadquantity-$hatbasigirdistok-$farkliiskalanmiktar):0)
                        //))
                    );
                } else {
                    $this->stoklar[$stockcode]["toplamgerekenmiktar"] += $gerekenmiktar;
                    $this->stoklar[$stockcode]["toplamgerekenmiktarmin"] += $gerekenmiktarmin;
                    if ($this->stoklar[$stockcode]["gerekenmiktarmin"] > $gerekenmiktarmin) {
                        $this->stoklar[$stockcode]["gerekenmiktarmin"] = $gerekenmiktarmin;
                    }
                    $this->stoklar[$stockcode]["details"][] = $row;
                }
            }
            $this->resp['d_1'] = round($this->microtime_diff($ts_1, microtime()), 2);
            $ts_1 = microtime();
            $qry = "SELECT in_part 'in_part',cast(sum(in_qty_avail) as int) 'in_qty_avail' 
            FROM pub.in_mstr 
            WHERE in_domain = 'ERMETAL' and in_part in ('".implode($tmp_arr_stockcode, '\',\'')."') 
            group by in_part 
            WITH(NOLOCK)";
            $data = $this->queryqad($this->conn_prod, $qry, 1);
            foreach ($data as $row_erp) {
                if (isset($this->stoklar[$row_erp["in_part"]])) {
                    $this->stoklar[$row_erp["in_part"]]["erpmiktar"] = intval($row_erp["in_qty_avail"]);
                    $this->stoklar[$row_erp["in_part"]]["serbestmiktar"] = intval($row_erp["in_qty_avail"]);
                    $this->stoklar[$row_erp["in_part"]]["stok"] = intval($row_erp["in_qty_avail"]);
                }
            }
            $this->resp['d_2'] = round($this->microtime_diff($ts_1, microtime()), 2);
            $ts_1 = microtime();
            /*foreach($records_tl as $row_tl){
                foreach($row_tl["details"] as $row_tl_det){
                    $sql_tc="SELECT erprefnumber,stockcode
                    from task_cases
                    where movementdetail is not null and casetype='I' and status in ('WAIT','WORK')
                        and stockcode=:stockcode and erprefnumber<>:erprefnumber
                        and start>CURRENT_TIMESTAMP(0)-interval '3 day'";
                    $stmt_tc = $conn->prepare($sql_tc);
                    $stmt_tc->bindValue('stockcode', $row_tl_det["stockcode"]);
                    $stmt_tc->bindValue('erprefnumber', $row_tl_det["erprefnumber"]);
                    $stmt_tc->execute();
                    $records_tc = $stmt_tc->fetchAll();
                    foreach($records_tc as $rowb){
                        if($this->pc!="192.168.1.40"){
                            $qry_tc="SELECT wod_nbr, wod_lot, wod_part, wod_qty_req, wod_qty_iss, (wod_qty_req - wod_qty_iss) miktar
                            FROM pub.wod_det
                            WHERE wod_domain = 'ERMETAL' and cast((wod_qty_req - wod_qty_iss) as int)>0 AND wod_lot = '".$rowb["erprefnumber"]."' AND wod_part = '".$rowb["stockcode"]."'
                            WITH(NOLOCK)";
                            $data_tc = $this->queryqad($this->conn_prod,$qry_tc);
                            if(count($data_tc)>0){
                                foreach($data_tc as $rowquantity){
                                    if(isset($this->stoklar[$row_erp_tc["wod_part"]])){
                                        $this->stoklar[$row_erp_tc["wod_part"]]["farkliiskalanmiktar"]+=intval($rowquantity["MIKTAR"]);
                                        $this->stoklar[$row_erp_tc["wod_part"]]["serbestmiktar"]-=intval($rowquantity["MIKTAR"]);
                                        $this->stoklar[$row_erp_tc["wod_part"]]["stok"]-=intval($rowquantity["MIKTAR"]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->resp['d_3']=round($this->microtime_diff($ts_1,microtime()),2);
            $ts_1 = microtime();*/
            $sql_tc = "SELECT erprefnumber,stockcode 
            from task_cases 
            where movementdetail is not null and casetype='I' and status in ('WAIT','WORK') 
                and start>CURRENT_TIMESTAMP(0)-interval '3 day'
                and stockcode in ('".implode($tmp_arr_stockcode, '\',\'')."') 
            order by client";
            $stmt_tc = $conn->prepare($sql_tc);
            $stmt_tc->execute();
            $str = '(';
            $records_tc = $stmt_tc->fetchAll();
            $this->resp['d_3'] = round($this->microtime_diff($ts_1, microtime()), 2);
            $ts_1 = microtime();
            foreach ($records_tc as $row_tc) {
                if ($str === '(') {
                    $str .= " (wod_lot = '".$row_tc["erprefnumber"]."' AND wod_part = '".$row_tc["stockcode"]."')";
                } else {
                    $str .= " or (wod_lot = '".$row_tc["erprefnumber"]."' AND wod_part = '".$row_tc["stockcode"]."')";
                }
            }
            $str .= ' )';
            $this->resp['d_4'] = round($this->microtime_diff($ts_1, microtime()), 2);
            $ts_1 = microtime();
            $qry_tc = "SELECT wod_nbr, wod_lot, wod_part 'wod_part', wod_qty_req, wod_qty_iss, cast((wod_qty_req - wod_qty_iss) as int) 'miktar'
            FROM pub.wod_det 
            WHERE wod_domain = 'ERMETAL' and cast((wod_qty_req - wod_qty_iss) as int)>0 AND ".($str !== '()' ? $str : '')." 
            WITH(NOLOCK)";
            $data_tc = $this->queryqad($this->conn_prod, $qry_tc);
            foreach ($data_tc as $row_erp_tc) {
                if (isset($this->stoklar[$row_erp_tc["wod_part"]])) {
                    $this->stoklar[$row_erp_tc["wod_part"]]["farkliiskalanmiktar"] += intval($row_erp_tc["miktar"]);
                    $this->stoklar[$row_erp_tc["wod_part"]]["serbestmiktar"] -= intval($row_erp_tc["miktar"]);
                    $this->stoklar[$row_erp_tc["wod_part"]]["stok"] -= intval($row_erp_tc["miktar"]);
                }
            }
            $this->resp['d_5'] = round($this->microtime_diff($ts_1, microtime()), 2);
            $ts_1 = microtime();
            $this->resp['stoklar'] = $this->stoklar;
            return $resp;
        } catch (Error $err) {
            echo $err->getMessage();
            $this->errinsert("mhstokkontrol-err", "message:".$err->getMessage(), 101);
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->errinsert("mhstokkontrol-err", "message:".$e->getMessage(), 102);
        }
    }

    public function mhstokkontrol()
    {
        $d = new \DateTime();
        $time = $d->format('Y-m-d H:i:s');
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        try {
            $t = microtime(true);
            $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
            $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));

            $_d = $d->format("Y-m-d H:i:s.u");
            $resp = array('erp' => array());
            $ts_1 = microtime();

            //$sql_6="SELECT stockcode,coalesce(sum(packcapacity),0)as miktar
            //    from task_cases
            //    where movementdetail is not null and casetype='I' and status in ('WAIT','WORK')
            //        and start>CURRENT_TIMESTAMP(0)-interval '3 day'
            //    GROUP BY stockcode";
            //$stmt_6 = $conn->prepare($sql_6);
            //$stmt_6->execute();
            //$records_6 = $stmt_6->fetchAll();
            //temp tablo oluşturma
            $sql_c_t = "
            CREATE TABLE IF NOT EXISTS task_cases_tmp (
                id serial PRIMARY KEY,
                client varchar(50) NOT NULL,
                opname varchar(60) NOT NULL,
                stockcode varchar(50) NOT NULL,
                erprefnumber varchar(50) NOT NULL
            )
            ";
            $stmt = $conn->prepare($sql_c_t);
            $stmt->execute();

            $sql_d_t = "delete from task_cases_tmp";
            $stmt = $conn->prepare($sql_d_t);
            $stmt->execute();

            $sql_i_t = "
            insert into task_cases_tmp (client,opname,stockcode,erprefnumber)
            select b.client,b.opname,b.stockcode,b.erprefnumber from (
                SELECT tc.*
                    ,(SELECT max(packcapacity)packcapacity from product_trees where finish is null and materialtype='K' and name=tc.opname)packcapacityptree 
                    ,tl.productcount-tl.productdonecount remainproduction,tl.id tlid
                from task_cases tc
                join task_lists tl on tl.erprefnumber=tc.erprefnumber and tl.finish is null and tl.delete_user_id is null
                where tc.movementdetail is not null and tc.casetype='I' and tc.status in ('WAIT','WORK') 
                    and tc.start>CURRENT_TIMESTAMP(0)-interval '3 day'
            )b
            where b.packcapacityptree<b.remainproduction
            ";
            $stmt = $conn->prepare($sql_i_t);
            $stmt->execute();

            $sql = "select * from (
                select tl1.*,a.info
                    ,(SELECT max(packcapacity)packcapacity from product_trees where finish is null and materialtype='K' and name=tl1.opname)packcapacity
                    ,tl1.productcount-tl1.productdonecount remainproduction
                from task_lists tl1 
                join (SELECT tl.client,tl.erprefnumber,tl.opname,max(tl.productdonecount)productdonecount,c.info,min(tl.plannedstart)plannedstart
                    from task_lists tl
                    join clients c on c.code=tl.client
                    join client_params cp on cp.client=c.code and cp.name='task_materialpreparation' and cp.value='true'
                    ".($this->customer == 'ERMETAL' ? " join case_labels cl on cl.erprefnumber=tl.erprefnumber " : "")."
                    where tl.finish is null and tl.delete_user_id is null and tl.productcount>tl.productdonecount and cl.status='OPEN' 
                    group by tl.client,tl.erprefnumber,tl.opname,c.info )a on a.client=tl1.client and a.erprefnumber=tl1.erprefnumber and a.opname=tl1.opname and a.productdonecount=tl1.productdonecount
                where tl1.finish is null and tl1.delete_user_id is null and tl1.productcount>tl1.productdonecount
                    and exists(
                        SELECT pt_h.* 
                        from product_trees pt_h 
                        where pt_h.finish is null and pt_h.materialtype in ('H') and pt_h.name=tl1.opname)
                    and not exists (
                        SELECT * 
                        from product_trees pt1 
                        join product_trees pt2 on pt2.code=pt1.code and pt2.materialtype=pt1.materialtype and pt2.finish is null and pt2.oporder<pt1.oporder 
                        where pt1.name=tl1.opname and pt1.finish is null and pt1.materialtype='O')
                    /*and exists(
                        SELECT pt_k.* 
                        from product_trees pt_k 
                        where pt_k.finish is null and pt_k.materialtype in ('K') and pt_k.name=tl1.opname)
                      and not exists (
                        SELECT 'kh' tip,id 
                        from case_movements 
                        where code=tl1.opcode and number=tl1.opnumber and erprefnumber=tl1.erprefnumber and actualfinishtime is null and tasktype='WAITFORMATERIAL')*/
            )b
            where b.packcapacity<b.remainproduction
            order by b.client,b.plannedstart,b.deadline,b.id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            //$this->resp['records']=count($records);
            //$qadquerycount=0;
            foreach ($records as $row) {
                $ts_2 = microtime();
                $erprefnumber = $row["erprefnumber"];
                $client = $row["client"];
                $opcode = $row["opcode"];
                $opnumber = $row["opnumber"];
                $opname = $row["opname"];
                $productcount = $row["productcount"];
                $remainproduction = $row["remainproduction"];
                $packcapacity = $row["packcapacity"];
                $sql_4 = "SELECT * from product_trees where finish is null and materialtype in ('H') and stockcode not ilike 'st%' and name=:name ";
                $stmt_4 = $conn->prepare($sql_4);
                $stmt_4->bindValue('name', $opname);
                $stmt_4->execute();
                $records_4 = $stmt_4->fetchAll();
                foreach ($records_4 as $rowstok) {
                    $qadquantity = 0;
                    $stockcode = $rowstok["stockcode"];
                    $gerekenmiktar = intval($rowstok["quantity"]) * intval($productcount);
                    //$gerekenmiktar=intval($rowstok["quantity"])*intval($remainproduction);
                    $gerekenmiktarmin = intval($rowstok["quantity"]) * intval($packcapacity);
                    if (!isset($this->stoklar[$stockcode])) {
                        if ($this->pc != "192.168.1.40") {
                            //$qadquerycount++;
                            $qry = "SELECT in_qty_avail 'in_qty_avail' 
                            FROM pub.in_mstr 
                            WHERE in_domain = 'ERMETAL' AND in_part = '$stockcode' WITH(NOLOCK)";
                            $data = $this->queryqad($this->conn_prod, $qry, 1);
                            if (count($data) > 0) {
                                foreach ($data as $rowquantity) {
                                    $qadquantity += intval($rowquantity["in_qty_avail"]);
                                }
                            } else {
                                $qadquantity = 1000000;
                            }
                        } else {
                            $qadquantity = 10000000;
                        }
                    } else {
                        $item = $this->stoklar[$stockcode];
                        $qadquantity = intval($item["qadquantity"]);
                    }
                    //sahaya verilmiş ama henüz tesellümü yapılmadığından stok düşümü yapılmamış malzeme toplamı qad stok miktarından düşülecek
                    //taskcase tablosunda bu bileşeni kullanan açık işlerin erprefnumber için iş emri kalan miktarları alınacak
                    //qad miktarı-qad iş emri kalan miktar-tesellüm edilmemiş miktar
                    $farkliiskalanmiktar = 0;
                    //$sql_5="
                    //select * from (
                    //    SELECT tc.*
                    //        ,(SELECT max(packcapacity)packcapacity from product_trees where finish is null and materialtype='K' and name=tc.opname)packcapacityptree
                    //        ,tl.productcount-tl.productdonecount remainproduction,tl.id tlid
                    //    from task_cases tc
                    //    join task_lists tl on tl.erprefnumber=tc.erprefnumber and tl.finish is null and tl.delete_user_id is null
                    //    where tc.movementdetail is not null and tc.casetype='I' and tc.status in ('WAIT','WORK')
                    //        and tc.stockcode=:stockcode and tc.erprefnumber<>:erprefnumber
                    //        and tc.start>CURRENT_TIMESTAMP(0)-interval '3 day'
                    //)b
                    //where b.packcapacityptree<b.remainproduction
                    //";
                    $sql_5 = "select * from task_cases_tmp where stockcode=:stockcode and erprefnumber<>:erprefnumber";
                    $stmt_5 = $conn->prepare($sql_5);
                    $stmt_5->bindValue('stockcode', $stockcode);
                    $stmt_5->bindValue('erprefnumber', $erprefnumber);
                    $stmt_5->execute();
                    $records_5 = $stmt_5->fetchAll();
                    foreach ($records_5 as $rowb) {
                        if ($this->pc != "192.168.1.40") {
                            //$qadquerycount++;
                            $qry = "SELECT wod_nbr, wod_lot, wod_part, wod_qty_req, wod_qty_iss, (wod_qty_req - wod_qty_iss) 'miktar'
                            FROM pub.wod_det 
                            WHERE wod_domain = 'ERMETAL' AND wod_lot = '".$rowb["erprefnumber"]."' AND wod_part = '$stockcode' WITH(NOLOCK)";
                            $data = $this->queryqad($this->conn_prod, $qry);
                            if (count($data) > 0) {
                                foreach ($data as $rowquantity) {
                                    $farkliiskalanmiktar += intval($rowquantity["miktar"]);
                                }
                            } else {
                                $farkliiskalanmiktar = 0;
                            }
                        } else {
                            $qadquantity = 10000;
                        }
                    }
                    $hatbasigirdistok = 0;
                    //$sql_6="SELECT coalesce(sum(packcapacity),0)as miktar
                    //from task_cases
                    //where movementdetail is not null and casetype='I' and status in ('WAIT','WORK')
                    //    and stockcode=:stockcode and start>CURRENT_TIMESTAMP(0)-interval '3 day'";
                    //$stmt_6 = $conn->prepare($sql_6);
                    //$stmt_6->bindValue('stockcode', $stockcode);
                    //$stmt_6->execute();
                    //$records_6 = $stmt_6->fetchAll();
                    //if($records_6&&count($records_6)>0){
                    //    $hatbasigirdistok=$records_6[0]["miktar"];
                    //}
                    //foreach($records_6 as $row_6){
                    //    if($row_6["stockcode"]==$stockcode){
                    //        $hatbasigirdistok=$row_6["miktar"];
                    //    }
                    //}
                    if (!isset($this->stoklar[$stockcode])) {
                        $this->stoklar[$stockcode] = array(
                            "gerekenmiktar" => $gerekenmiktar
                            ,"gerekenmiktarmin" => $gerekenmiktarmin
                            ,"stok" => $qadquantity - $hatbasigirdistok - $farkliiskalanmiktar
                            ,"qadquantity" => $qadquantity
                            ,"hatbasigirdistok" => $hatbasigirdistok
                            ,"farkliiskalanmiktar" => $farkliiskalanmiktar
                            ,"data" => array(array(
                                "code" => $opcode
                                ,"stockcode" => $stockcode
                                ,"number" => $opnumber
                                ,"casename" => $rowstok["pack"]
                                ,"starttime" => $time
                                ,"locationdestination" => $client
                                ,"tasktype" => 'WAITFORMATERIAL'
                                ,"status" => 'WAITFORMATERIAL'
                                ,"client" => $client
                                ,"erprefnumber" => $erprefnumber
                                ,"quantitynecessary" => 0
                                ,"quantitynecessarymin" => intval($gerekenmiktarmin)
                                ,"quantityready" => (($qadquantity - $hatbasigirdistok - $farkliiskalanmiktar) > 0 ? ($qadquantity - $hatbasigirdistok - $farkliiskalanmiktar) : 0)
                            ))
                        );
                    } else {
                        $item = $this->stoklar[$stockcode];
                        $item["gerekenmiktar"] += $gerekenmiktar;
                        $item["gerekenmiktarmin"] += $gerekenmiktarmin;
                        $item["data"][] = array(
                            "code" => $opcode
                            ,"stockcode" => $stockcode
                            ,"number" => $opnumber
                            ,"casename" => $rowstok["pack"]
                            ,"starttime" => $time
                            ,"locationdestination" => $client
                            ,"tasktype" => 'WAITFORMATERIAL'
                            ,"status" => 'WAITFORMATERIAL'
                            ,"client" => $client
                            ,"erprefnumber" => $erprefnumber
                            ,"quantitynecessary" => 0
                            ,"quantitynecessarymin" => intval($gerekenmiktarmin)
                            ,"quantityready" => (($qadquantity - $hatbasigirdistok - $farkliiskalanmiktar) > 0 ? ($qadquantity - $hatbasigirdistok - $farkliiskalanmiktar) : 0)
                        );
                        $this->stoklar[$stockcode] = $item;
                    }
                }
                $t1 = round($this->microtime_diff($ts_2, microtime()), 2);
                //$this->resp['erp'][]=array('erprefnumber'=>$erprefnumber,'t1'=>$t1);
            }

            $d1 = round($this->microtime_diff($ts_1, microtime()), 2);
            //$this->resp['qadquerycount']=$qadquerycount;
            $this->resp['d_1'] = $d1;
            $ts_1 = microtime();
            $_x_ = [];
            //$this->stoklar içinde olup, beklemeye gerek olmayanlar kapatılıyor
            foreach ($this->stoklar as $key => $row) {
                $stok = intval($row["stok"]);
                if (intval($row["gerekenmiktarmin"]) > $stok) {
                    foreach ($row["data"] as $item) {
                        if ($stok >= intval($item["quantitynecessarymin"])) {
                            $stok = $stok - intval($item["quantitynecessarymin"]);
                        } else {
                            $_x_[] = array("key" => $key,"type" => "2","data" => $row);
                            $sql_7 = "SELECT * 
                            from case_movements 
                            where actualfinishtime is null 
                                and stockcode=:stockcode 
                                and erprefnumber=:erprefnumber";
                            //and tasktype='WAITFORMATERIAL'
                            //and status='WAITFORMATERIAL'
                            $stmt_7 = $conn->prepare($sql_7);
                            $stmt_7->bindValue('stockcode', $item["stockcode"]);
                            $stmt_7->bindValue('erprefnumber', $item["erprefnumber"]);
                            $stmt_7->execute();
                            $records_7 = $stmt_7->fetchAll();
                            if (count($records_7) == 0) {
                                $this->errinsert("stokbulunamadi", "Stok Bulunamadı. erpref:".$item["erprefnumber"]." Bileşen:".$item["stockcode"]." Stok:".$item["stok"]." ERP Stok Miktarı:".$item["quantityready"]." Gereken Miktar:".$item["quantitynecessarymin"]." Farklıis:".$item["farkliiskalanmiktar"]);
                                //unset($item["quantitynecessarymin"]);
                                $sql_8 = "insert into case_movements (code,stockcode,number,casename,tasktype,starttime,status,client,erprefnumber,quantitynecessary,position,quantityready)
                                values (:code,:stockcode,:number,:casename,:tasktype,:starttime,:status,:client,:erprefnumber,:quantitynecessary,:pos,:quantityready)";
                                $stmt_8 = $conn->prepare($sql_8);
                                $stmt_8->bindValue('code', $item["code"]);
                                $stmt_8->bindValue('stockcode', $item["stockcode"]);
                                $stmt_8->bindValue('number', $item["number"]);
                                $stmt_8->bindValue('casename', $item["casename"]);
                                $stmt_8->bindValue('tasktype', $item["tasktype"]);
                                $stmt_8->bindValue('starttime', $item["starttime"]);
                                $stmt_8->bindValue('status', $item["status"]);
                                $stmt_8->bindValue('client', $item["client"]);
                                $stmt_8->bindValue('erprefnumber', $item["erprefnumber"]);
                                $stmt_8->bindValue('quantitynecessary', intval($item["quantitynecessarymin"]));
                                $stmt_8->bindValue('quantityready', $stok);
                                $stmt_8->bindValue('pos', 1);
                                $stmt_8->execute();
                            }
                            $sql_7_2 = "SELECT * 
                            from case_movements 
                            where actualfinishtime is null 
                                and status in ('WAITFORDELIVERY','WAITFORASSIGNMENT') 
                                and stockcode=:stockcode 
                                and erprefnumber=:erprefnumber";
                            $stmt_7_2 = $conn->prepare($sql_7_2);
                            $stmt_7_2->bindValue('stockcode', $item["stockcode"]);
                            $stmt_7_2->bindValue('erprefnumber', $item["erprefnumber"]);
                            $stmt_7_2->execute();
                            $records_7_2 = $stmt_7_2->fetchAll();
                            if (count($records_7_2) > 0) {
                                foreach ($records_7_2 as $row_7_2) {
                                    $sql_7_3 = "update case_movements 
                                    set tasktype=:tasktype
                                    ,status=:status 
                                    ,carrier=null
                                    ,locationdestination=null
                                    ,targettime=null
                                    ,assignmenttime=null
                                    ,empcode=null
                                    ,empname=null
                                    ,locationdestinationinfo=null
                                    ,filepath=null
                                    ,carrierfeeder=null
                                    ,carrierdrainer=null
                                    where actualfinishtime is null and id=:id";
                                    $stmt_7_3 = $conn->prepare($sql_7_3);
                                    $stmt_7_3->bindValue('tasktype', 'WAITFORMATERIAL');
                                    $stmt_7_3->bindValue('status', 'WAITFORMATERIAL');
                                    $stmt_7_3->bindValue('id', $row_7_2["id"]);
                                    $stmt_7_3->execute();

                                    $sql_7_4 = "update case_movement_details 
                                    set canceltime=current_timestamp,description=:description 
                                    where canceltime is null and donetime is null and casemovementid=:casemovementid";
                                    $stmt_7_4 = $conn->prepare($sql_7_4);
                                    $stmt_7_4->bindValue('description', 'QAD Stok Düşürme');
                                    $stmt_7_4->bindValue('casemovementid', $row_7_2["id"]);
                                    $stmt_7_4->execute();
                                    if ($row_7_2["status"] === 'WAITFORDELIVERY') {
                                        if (!(in_array($row_7_2['carrier'], $this->arrnodeinfo))) {
                                            $this->arrnodeinfo[] = $row_7_2['carrier'];
                                        }
                                        $this->dosyasil($row_7_2["filepath"], 8);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    foreach ($row["data"] as $item) {
                        $sql_9 = "SELECT cm.*,c.ip,c.uuid
                        from case_movements cm 
                        join clients c on c.code=cm.client
                        where cm.actualfinishtime is null and cm.tasktype='WAITFORMATERIAL' and cm.status='WAITFORMATERIAL' and cm.stockcode=:stockcode and cm.erprefnumber=:erprefnumber";
                        $stmt_9 = $conn->prepare($sql_9);
                        $stmt_9->bindValue('stockcode', $item["stockcode"]);
                        $stmt_9->bindValue('erprefnumber', $item["erprefnumber"]);
                        $stmt_9->execute();
                        $records_9 = $stmt_9->fetchAll();
                        if (count($records_9) > 0) {
                            foreach ($records_9 as $row_9) {
                                $_pos = 'P_1-stok:'.$stok.'-gerekenmiktarmin:'.$row["gerekenmiktarmin"];
                                $sql_10 = "update case_movements 
                                set actualfinishtime=current_timestamp,status='PROCURED',position=:position 
                                where actualfinishtime is null and status='WAITFORMATERIAL' and stockcode=:stockcode and erprefnumber=:erprefnumber";
                                $stmt_10 = $conn->prepare($sql_10);
                                $stmt_10->bindValue('stockcode', $item["stockcode"]);
                                $stmt_10->bindValue('erprefnumber', $item["erprefnumber"]);
                                $stmt_10->bindValue('position', $_pos);
                                $stmt_10->execute();
                                $_data = array("stockcode" => $item["stockcode"],"erprefnumber" => $item["erprefnumber"]);
                                // $conn->insert('_sync_messages', array('ip'=>($row_9["client"].'_server'),'uuid' => $row_9["uuid"], 'ack' => 'true','queued'=>0, 'type' => 'mh_waitmaterial_clear', 'data' => json_encode($_data), '"createdAt"' => $_d, '"updatedAt"' => $_d));
                                if ($this->pc == "192.168.1.40") {
                                    $conn->insert('__sync_messages', array('queuename' => ($row_9["client"].'_server'), 'priority' => 10, 'messagetype' => 'mh_waitmaterial_clear', 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data),'info' => json_encode(array("tableName" => "")) ));
                                }
                            }
                        }
                    }
                }
            }
            //$this->stoklar içinde olmayıp, beklemeye gerek olmayanlar kapatılıyor
            $sql_11 = "SELECT cm.*,c.ip,c.uuid
            from case_movements cm 
            join clients c on c.code=cm.client
            where cm.actualfinishtime is null and cm.tasktype='WAITFORMATERIAL' and cm.status='WAITFORMATERIAL'";
            $stmt_11 = $conn->prepare($sql_11);
            $stmt_11->execute();
            $records_11 = $stmt_11->fetchAll();
            if (count($records_11) > 0) {
                foreach ($records_11 as $row_11) {
                    $qadquantity = 0;
                    if ($this->pc != "192.168.1.40") {
                        $qry = "SELECT in_qty_avail 'in_qty_avail' 
                        FROM pub.in_mstr 
                        WHERE in_domain = 'ERMETAL' AND in_part = '".$row_11["stockcode"]."' WITH(NOLOCK)";
                        $data = $this->queryqad($this->conn_prod, $qry, 2);
                        if (count($data) > 0) {
                            foreach ($data as $rowquantity) {
                                $qadquantity = $qadquantity + intval($rowquantity["in_qty_avail"]);
                            }
                        } else {
                            $qadquantity = 0;
                        }
                    } else {
                        $qadquantity = 10000;
                    }
                    if (intval($qadquantity) > intval($row_11["quantitynecessary"])) {
                        $_pos = 'P_2-stok:'.$qadquantity.'-quantitynecessary:'.intval($row_11["quantitynecessary"]);
                        $sql_110 = "update case_movements set actualfinishtime=current_timestamp,status='PROCURED',position=:position where actualfinishtime is null and status='WAITFORMATERIAL' and stockcode=:stockcode and erprefnumber=:erprefnumber";
                        $stmt_110 = $conn->prepare($sql_110);
                        $stmt_110->bindValue('stockcode', $row_11["stockcode"]);
                        $stmt_110->bindValue('erprefnumber', $row_11["erprefnumber"]);
                        $stmt_110->bindValue('position', $_pos);
                        $stmt_110->execute();
                        $_data = array("stockcode" => $row_11["stockcode"],"erprefnumber" => $row_11["erprefnumber"]);
                        // $conn->insert('_sync_messages', array('ip'=>($row_11["client"].'_server'),'uuid' => $row_11["uuid"], 'ack' => 'true','queued'=>0, 'type' => 'mh_waitmaterial_clear', 'data' => json_encode($_data), '"createdAt"' => $_d, '"updatedAt"' => $_d));
                        if ($this->pc == "192.168.1.40") {
                            $conn->insert('__sync_messages', array('queuename' => ($row_11["client"].'_server'), 'priority' => 10, 'messagetype' => 'mh_waitmaterial_clear', 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data),'info' => json_encode(array("tableName" => "")) ));
                        }
                    }
                }
            }
            //bekleyen tipte olup iş bitmiş olanlar temizleniyor
            $sql_11_1 = "SELECT cm.*,c.ip,c.uuid
            from case_movements cm 
            join clients c on c.code=cm.client
			join task_lists tl on tl.erprefnumber=cm.erprefnumber and tl.opcode=cm.code and tl.finish is not null and tl.delete_user_id is null
            where cm.actualfinishtime is null and cm.tasktype='WAITFORMATERIAL' and cm.status='WAITFORMATERIAL'";
            $stmt_11_1 = $conn->prepare($sql_11_1);
            $stmt_11_1->execute();
            $records_11_1 = $stmt_11_1->fetchAll();
            if (count($records_11_1) > 0) {
                foreach ($records_11_1 as $row_11_1) {
                    $_pos = 'P_3';
                    $sql_111 = "update case_movements set actualfinishtime=current_timestamp,status='PROCURED',position=:position where actualfinishtime is null and status='WAITFORMATERIAL' and stockcode=:stockcode and erprefnumber=:erprefnumber";
                    $stmt_111 = $conn->prepare($sql_111);
                    $stmt_111->bindValue('stockcode', $row_11_1["stockcode"]);
                    $stmt_111->bindValue('erprefnumber', $row_11_1["erprefnumber"]);
                    $stmt_111->bindValue('position', $_pos);
                    $stmt_111->execute();
                    $_data = array("stockcode" => $row_11_1["stockcode"],"erprefnumber" => $row_11_1["erprefnumber"]);
                    // $conn->insert('_sync_messages', array('ip'=>($row_11_1["client"].'_server'),'uuid' => $row_11_1["uuid"], 'ack' => 'true','queued'=>0, 'type' => 'mh_waitmaterial_clear', 'data' => json_encode($_data), '"createdAt"' => $_d, '"updatedAt"' => $_d));
                    if ($this->pc == "192.168.1.40") {
                        $conn->insert('__sync_messages', array('queuename' => ($row_11_1["client"].'_server'), 'priority' => 10, 'messagetype' => 'mh_waitmaterial_clear', 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data),'info' => json_encode(array("tableName" => "")) ));
                    }
                }
            }
            $sql_12 = "SELECT cm.*,c.ip,c.uuid
            from case_movements cm 
            join clients c on c.code=cm.client
            where cm.actualfinishtime is null and cm.tasktype='WAITFORMATERIAL' and cm.status='WAITFORMATERIAL'";
            $stmt_12 = $conn->prepare($sql_12);
            $stmt_12->execute();
            $records_12 = $stmt_12->fetchAll();
            if (count($records_12) > 0) {
                foreach ($records_12 as $row_12) {
                    $_data = array("stockcode" => $row_12["stockcode"],"erprefnumber" => $row_12["erprefnumber"]);
                    // $conn->insert('_sync_messages', array('ip'=>($row_12["client"].'_server'),'uuid' => $row_12["uuid"], 'ack' => 'true','queued'=>0, 'type' => 'mh_waitmaterial_set', 'data' => json_encode($_data), '"createdAt"' => $_d, '"updatedAt"' => $_d));
                    if ($this->pc == "192.168.1.40") {
                        $conn->insert('__sync_messages', array('queuename' => ($row_12["client"].'_server'), 'priority' => 10, 'messagetype' => 'mh_waitmaterial_set', 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data),'info' => json_encode(array("tableName" => "")) ));
                    }
                }
            }

            $d2 = round($this->microtime_diff($ts_1, microtime()), 2);
            $this->resp['d_2'] = $d2;
            //if(isset($this->stoklar['01.13268AB'])){
            //    $resp['stoklar']=$this->stoklar['01.13268AB'];
            //}
            $this->resp['stoklar'] = $this->stoklar;
            $this->resp['eksikler'] = $_x_;
            return $resp;
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->errinsert("mhstokkontrol-err", "message:".$e->getMessage(), 101);
        }
    }

    public function microtime_diff($start, $end = null)
    {
        if (!$end) {
            $end = microtime();
        }
        list($start_usec, $start_sec) = explode(" ", $start);
        list($end_usec, $end_sec) = explode(" ", $end);
        $diff_sec = intval($end_sec) - intval($start_sec);
        $diff_usec = floatval($end_usec) - floatval($start_usec);
        return floatval($diff_sec) + $diff_usec;
    }

    public function queryqad($con, $query, $pos = 0)
    {
        //echo $query."\n";
        $data = array();
        try {
            if ($con) {
                try {
                    $result = odbc_exec($con, $query);
                    if ($result) {
                        while (odbc_fetch_row($result)) {
                            $tmp = array();
                            for ($i = 1;$i <= odbc_num_fields($result);$i++) {
                                if (odbc_field_name($result, $i)) {
                                    $tmp[odbc_field_name($result, $i)] = odbc_result($result, $i);
                                }
                            }
                            $data[] = $tmp;
                        }
                        odbc_free_result($result);
                    } else {
                        $this->resp['errors'][] = 'result yok-'.$query;
                    }
                } catch (Exception $e) {
                    //$this->resp['errors'][]="catch";
                    $this->resp['errors'][] = $e->getMessage();
                    $this->errinsert("queryqad-err", "query:".$query." message:".$e->getMessage(), 201);
                }
            } else {
                $this->resp['errors'][] = "odbc bağlantı yok";
            }
            if (count($data) == 0) {
                if (strpos($query, 'SELECT in_qty_avail') === false && strpos($query, 'SELECT wod_nbr, wod_lot,') === false) {
                    $this->resp['errors'][] = "1-RC=0 ".$query."<br>";
                    //$this->errinsert("queryqad-rc=0","query:".$query." message:qad dönen kayıt yok.",201);
                }
            } else {
                if (strpos($query, 'SELECT in_qty_avail') === false && strpos($query, 'SELECT wod_nbr, wod_lot,') === false) {
                    $this->resp['errors'][] = "2-RC=".count($data)." ".$query."<br>";
                }
            }
            return $data;
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->errinsert("queryqad-err", "message:".$e->getMessage(), 901);
        }
        return $data;
    }

    /**
     * @Route(path="/MaterialPrepare", name="MaterialPrepare-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $r = new \StdClass();
            $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->findBy(array('name' => 'materialPrepare','value' => 'Otomatik' ));
            $r->status = ($entity ? 'OPEN' : 'CLOSE');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0, //$this instanceof BaseAuditControllerInterface,
                'data' => array('MaterialPrepare' => array("totalProperty" => 1,"records" => array($r))),
                'extras' => array()
            );

            return $this->render('Modules/MaterialPrepare.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/MaterialPrepare/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="MaterialPrepare-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $data = $this->clearLookup(json_decode($request->getContent()));
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->findBy(array('name' => 'materialPrepare','value' => ($data->status == 'OPEN' ? 'Hayır' : 'Otomatik') ));
        $cbu = $this->checkBeforeUpdate($request, $id, $entity, -1);
        if ($cbu === true) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                if ($data->status == 'CLOSE') {
                    $sql_1 = "update case_movement_details set canceltime=current_timestamp,description=:description where canceltime is null and donetime is null";
                    $stmt_1 = $conn->prepare($sql_1);
                    $stmt_1->bindValue('description', 'Sistemin Kapatılması');
                    $stmt_1->execute();

                    $sql_2 = "update case_movements set status=:status where actualfinishtime is null and status in ('WAITFORASSIGNMENT','WAITFORDELIVERY')";
                    $stmt_2 = $conn->prepare($sql_2);
                    $stmt_2->bindValue('status', 'SYSTEMCLOSE');
                    $stmt_2->execute();
                    if ($this->customer == 'ERMETAL') {
                        $sql_3 = "update case_labels set status=:statusnew,synced=1 where status=:statusold";
                        $stmt_3 = $conn->prepare($sql_3);
                        $stmt_3->bindValue('statusnew', 'SYSTEMCLOSE');
                        $stmt_3->bindValue('statusold', 'OPEN');
                        $stmt_3->execute();
                    }

                    $sql_tc = "select * from task_cases where movementdetail is not null and status in ('WAIT','WORK','WAITFORDELIVERY')";
                    $stmt_tc = $conn->prepare($sql_tc);
                    $stmt_tc->execute();
                    $records_tc = $stmt_tc->fetchAll();
                    foreach ($records_tc as $row) {
                        /* @var $repo TaskCase */
                        $repo = $this->getDoctrine()
                            ->getRepository('App:TaskCase');
                        $entity_1 = $repo->find($row["id"]);
                        if ($entity_1 !== null) {
                            $entity_1->setStatus('SYSTEMCLOSE');
                            $entity_1->setFinish(new \DateTime());
                            $entity_1->setFinishtype('Sistemin Kapatılması');
                            $em->persist($entity_1);
                            $em->flush();
                            $em->clear();
                        }
                    }

                    //$sql_4="update task_cases set status=:status,finish=current_timestamp,finishtype=:finishtype where movementdetail is not null and status in ('WAIT','WORK','WAITFORDELIVERY')";
                    //$stmt_4 = $conn->prepare($sql_4);
                    //$stmt_4->bindValue('status', 'SYSTEMCLOSE');
                    //$stmt_4->bindValue('finishtype', 'Sistemin Kapatılması');
                    //$stmt_4->execute();

                    //export dizin içi temizlenecek
                    if ($this->pc != "172.16.1.142" && $this->pc != "192.168.1.40") {
                        $exportpath = $this->exportpath;
                    } else {
                        $exportpath = "//localhost/paylaşım/mh/";
                    }
                    $files = array_diff(scandir($exportpath), array('..', '.'));
                    foreach ($files as $file) {
                        if (is_file($exportpath.$file)) {
                            unlink($exportpath.$file);
                        }
                    }
                    $sql_5 = "select id,code from carriers";
                    $stmt_5 = $conn->prepare($sql_5);
                    $stmt_5->execute();
                    foreach ($stmt_5->fetchAll() as $row) {
                        $data = '{"material_prepare":true,"methodName":"tasimagorevilistele","id":"'.$row["id"].'","carrier":"'.$row["code"].'"}';
                        $this->dosyayaznodetrigger($data, 513);
                    }
                }

                $sql_update = "update client_params set value=:valuenew where name=:name";
                $stmt_update = $conn->prepare($sql_update);
                $stmt_update->bindValue('valuenew', ($data->status == 'OPEN' ? 'Otomatik' : 'Hayır'));
                $stmt_update->bindValue('name', 'materialPrepare');
                $stmt_update->execute();
                //_sync_messages insert
                $t = microtime(true);
                $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
                $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));

                $_d = $d->format("Y-m-d H:i:s.u");
                $clients = $conn->fetchAll('SELECT * FROM clients where "isactive"=? ', array(true));
                foreach ($clients as $row) {
                    $_data = array("tableName" => "client_params","procType" => "update","data" => array("client" => $row["code"], "name" => "materialPrepare","value" => ($data->status == 'OPEN' ? 'Otomatik' : 'Hayır')));
                    $__data = json_encode($_data);
                    // $conn->insert('_sync_messages', array('ip'=>(($row["code"]!==null&&$row["code"]!=='' ? $row["code"] : $row["ip"]).'_server'),'uuid' => $row["uuid"], 'ack' => 'true','queued'=>0, 'type' => 'sync_message', 'data' => $__data, '"createdAt"' => $_d, '"updatedAt"' => $_d));
                    if ($this->pc == "192.168.1.40") {
                        $conn->insert('__sync_messages', array('queuename' => ($row["code"].'_server'), 'priority' => 10, 'messagetype' => 'sync_message', 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data["data"]),'info' => json_encode(array("tableName" => "client_params")) ));
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            return $this->msgSuccess();
        } else {
            $message = ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans(($data->status == 'OPEN' ? 'MaterialPrepare.errAlreadyOpen' : 'MaterialPrepare.errAlreadyClose'), array(), 'MaterialPrepare');
            return $this->msgError($message);
        }
    }
}
