<?php

namespace App\Controller;

use App\Entity\Overtime;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OvertimeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:Overtime';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/Overtime/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Overtime-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new Overtime();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('o.clients,o.')
                ->from('App:Overtime', 'o')
                ->where('o.deleteuserId is null')
                ->orderBy('o.id', 'ASC');
        $queries['Overtime'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT o.* 
                    ,case when strpos(days,'Monday')>0 then 1 else 0 end \"Monday\"
                    ,case when strpos(days,'Tuesday')>0 then 1 else 0 end \"Tuesday\"
                    ,case when strpos(days,'Wednesday')>0 then 1 else 0 end \"Wednesday\"
                    ,case when strpos(days,'Thursday')>0 then 1 else 0 end \"Thursday\"
                    ,case when strpos(days,'Friday')>0 then 1 else 0 end \"Friday\"
                    ,case when strpos(days,'Saturday')>0 then 1 else 0 end \"Saturday\"
                    ,case when strpos(days,'Sunday')>0 then 1 else 0 end \"Sunday\"
                FROM Overtimes o 
                ORDER BY o.id ASC";
        $queries['Overtime'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/Overtime/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Overtime-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $content = $request->getContent();
        $this->_requestData = json_decode($request->getContent());
        if(isset($this->_requestData->startday)){
            $this->_requestData->finishday=$this->_requestData->startday;
        }
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Overtime/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Overtime-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $content = $request->getContent();
        $this->_requestData = json_decode($request->getContent());
        if(isset($this->_requestData->startday)){
            $this->_requestData->finishday=$this->_requestData->startday;
        }
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Overtime", name="Overtime-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/Overtime.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Overtime/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Overtime-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Overtime', $id);

            return $this->render('Modules/Overtime.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Overtime/{id}", requirements={"id": "\d+"}, name="Overtime-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Overtime', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Overtime/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="Overtime-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
