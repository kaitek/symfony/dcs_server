<?php
/*
 * kayıt girişi toplu yapılacak
 * silme için gridde her kaydın yanındaki check seçilerek toplu silme yapılacak
 *
 */

namespace App\Controller;

use App\Entity\ClientLostDetail;
use App\Entity\ClientProductionDetail;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientProductionDetailController extends DcsBaseController implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:ClientProductionDetail';
    public $dayCount = 60;

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType = self::QUERY_TYPE_SQL;
        if ($_SERVER['HTTP_HOST'] == '10.10.0.10') {
            $this->dayCount = 20;
        }
    }

    /**
     * @Route(path="/ClientProductionDetail/{pg}/{lm}/{id}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+"}, name="ClientProductionDetail-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v = 0)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        if ($cbd === true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $user = $this->getUser();
            $userId = $user->getId();
            //$records=$this->getDoctrine()
            //    ->getRepository(self::ENTITY)
            //    ->findBy(array('client' => $entity->getClient(), 'day' => $entity->getDay(), 'jobrotation' => $entity->getJobrotation(), 'start' => $entity->getStart(), 'finish' => $entity->getFinish()));
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                //foreach($records as $row){
                //    $row->setDeleteuserId($userId);
                //    $em->persist($row);
                //    $em->flush();
                //    $em->remove($row);
                //    $em->flush();
                //}
                //if($entity->getType()=='c_p_out'||$entity->getType()=='e_p_out'){
                //    //ClientLostDetail tablosunda başlangıcı silinen kaydın bitişi ile aynı olan kayıtlar var ise bitiş zamanını kendisinden önceki kayıtlara güncelle, bu kaydı sil
                //    $finish=null;
                //    //silinecek kayıtlar bulunuyor
                //    $entitylost = $this->getDoctrine()
                //        ->getRepository('App:ClientLostDetail')
                //        ->findBy(array('client' => $entity->getClient(), 'day' => $entity->getDay(), 'jobrotation' => $entity->getJobrotation(), 'start' => $entity->getFinish()));
                //    if($entitylost!=null&&count($entitylost)>0){
                //        foreach($entitylost as $row){
                //            $row->setDeleteuserId($userId);
                //            if($finish==null){
                //                $finish=$row->getFinish();
                //            }
                //            $em->persist($row);
                //            $em->flush();
                //            $em->remove($row);
                //            $em->flush();
                //        }
                //    }
                //    //önceki kaydın bitiş zamanı ileri alınıyor
                //    $entitylost = $this->getDoctrine()
                //        ->getRepository('App:ClientLostDetail')
                //        ->findBy(array('client' => $entity->getClient(), 'day' => $entity->getDay(), 'jobrotation' => $entity->getJobrotation(), 'finish' => $entity->getStart()));
                //    if($entitylost!=null&&count($entitylost)>0){
                //        foreach($entitylost as $row){
                //            $row->setFinish($finish);
                //            $em->persist($row);
                //            $em->flush();
                //        }
                //    }
                //}
                //şimdilik silinen kayıtlar için audit olmayacak
                $sql = "delete from client_production_details where finish is not null and client=:client and day=:day and start>=:start and finish<=:finish ";
                if ($content->record->tip == 'Operatör') {
                    $sql .= " and type in ('e_p','e_p_confirm','e_p_out') ";
                }
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $content->record->client);
                $stmt->bindValue('day', $content->record->day);
                $stmt->bindValue('start', $content->record->start);
                $stmt->bindValue('finish', $content->record->finish);
                $stmt->execute();
                $conn->commit();
                //$day=$entity->getDay();
                if ($content->calculate) {
                    $this->efficiencyCalculate($content->record->day);
                }
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
            //}
        } else {
            return $cbd;
        }
    }

    public function getNewEntity()
    {
        return new ClientProductionDetail();
    }

    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT 0 del,min(cpd.id)id,cpd.client,cpd.day,cpd.jobrotation,min(cpd.tip)tip
            ,cpd.start,cpd.finish,cpd.suredk,cpd.start_d,cpd.finish_d,cpd.start_t,cpd.finish_t
            ,string_agg(distinct cpd.employee,'|')employee,string_agg(distinct cpd.opname,'|')opname
            ,string_agg(distinct cpd.employees,'|')employees,string_agg(distinct cpd.opnames,'|')opnames
            ,max(cpd.productioncurrent)productioncurrent,max(cpd.taskfinishdescription)taskfinishdescription
            ,max(cpd.version) \"version\" 
        FROM (
            select cpd.id,cpd.client,cpd.day,cpd.jobrotation
                ,case when cpd.type in ('c_p','c_p_confirm','c_p_out') then 'İstasyon' else 'Operatör' end tip
                ,case when cpd.employee is not null then concat(e.name) else null end employee
                ,case when cpd.opname is not null then concat(cpd.opname) else null end opname
                ,case when cpd.employee is not null then concat(cpd.employee,'~',e.name) else null end employees
                ,case when cpd.opname is not null then concat(cpd.erprefnumber,'~',cpd.opname,'~',cpd.productioncurrent) else null end opnames
                ,cpd.start,cpd.finish
                ,ceil(cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suredk
                ,cast(cpd.start as varchar(10)) start_d,cast(cpd.finish as varchar(10)) finish_d
                ,right(cast(cpd.start as varchar(19)),8)start_t,right(cast(cpd.finish as varchar(19)),8)finish_t
                ,case when cpd.type in ('c_p','c_p_confirm','e_p','e_p_confirm') then 1 else 0 end issystem
                ,cpd.productioncurrent,cpd.version,cpd.taskfinishdescription
            from client_production_details cpd
            left join employees e on e.code=cpd.employee
            where cpd.type in ('c_p','c_p_confirm','c_p_out','e_p','e_p_confirm','e_p_out') and cpd.finish is not null and e.finish is null 
            and cpd.day>CURRENT_DATE - interval '".$this->dayCount." days'
        )cpd
        where 1=1 @@where@@
        GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish,cpd.suredk,cpd.start_d,cpd.finish_d,cpd.start_t,cpd.finish_t
        ORDER BY cpd.day DESC,cpd.start desc";
        $queries['ClientProductionDetail'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('jre.id,jre.day,jre.week,jre.jobrotation'
                . ',jre.jobrotationteam,jre.employee,jre.isovertime'
                . ',jre.beginval,jre.endval,jre.worktime,jre.breaktime'
                . ',jre.version')
                ->from('App:ClientProductionDetail', 'jre')
                ->where('jre.deleteuserId is null')
                ->orderBy('jre.day', 'DESC');
        $queries['ClientProductionDetail'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ClientProductionDetail/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientProductionDetail-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba = $this->checkBeforeAdd($request);
        if ($cba === true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $d_s = new \DateTime($content->start_d." ".$content->start_t);
            $d_f = new \DateTime($content->finish_d." ".$content->finish_t);
            if ($d_s >= $d_f) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ClientProductionDetail.errdatestartgreaterthanfinish', array(), 'ClientProductionDetail'));
            }
            if (count($content->productions) == 0) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ClientProductionDetail.errnotfoundproductions', array(), 'ClientProductionDetail'));
            }
            if ($content->employees == '') {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ClientProductionDetail.errnotfoundemployees', array(), 'ClientProductionDetail'));
            }
            $content->start = $content->start_d." ".$content->start_t;
            $content->finish = $content->finish_d." ".$content->finish_t;
            $start = strtotime($content->start_d." ".$content->start_t);
            $end   = strtotime($content->finish_d." ".$content->finish_t);
            $gap  = $end - $start;
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                if ($content->tip == 'İstasyon') {
                    //seçili her iş için birer adet c_p_out
                    foreach ($content->productions as $production) {
                        $arr_code_number = explode('-', $production->opname);
                        if (count($arr_code_number) > 1) {
                            $production->opnumber = $arr_code_number[count($arr_code_number) - 1];
                            array_pop($arr_code_number);
                            $production->opcode = implode('-', $arr_code_number);
                        } else {
                            $production->opnumber = 0;
                            $production->opcode = $production->opname;
                        }
                        if ($production->opnumber === '') {
                            $production->opnumber = 0;
                        }
                        $sql_cp = "insert into client_production_details 
                            (type,client,day,jobrotation,tasklist,time
                            ,opcode,opnumber,opname,leafmask,leafmaskcurrent
                            ,production,productioncurrent,erprefnumber
                            ,gap,start,finish,record_id
                            ,create_user_id,created_at,update_user_id,updated_at
                            ,version,isexported,opsayi,refsayi,taskfinishdescription)
                        values (:type,:client,:day,:jobrotation,:tasklist,CURRENT_TIMESTAMP(0)
                            ,:opcode,:opnumber,:opname,'01','01'
                            ,:production,:productioncurrent,:erprefnumber
                            ,:gap,:start,:finish,:record_id
                            ,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0)
                            ,:version,false,:opsayi,:refsayi,:taskfinishdescription)
                        ";
                        $stmt_cp = $conn->prepare($sql_cp);
                        $stmt_cp->bindValue('type', 'c_p_out');
                        $stmt_cp->bindValue('client', $content->client);
                        $stmt_cp->bindValue('day', $content->day);
                        $stmt_cp->bindValue('jobrotation', $content->jobrotation);
                        $stmt_cp->bindValue('tasklist', '');
                        $stmt_cp->bindValue('opcode', $production->opcode);
                        $stmt_cp->bindValue('opnumber', $production->opnumber);
                        $stmt_cp->bindValue('opname', $production->opname);
                        $stmt_cp->bindValue('production', $production->productioncurrent);
                        $stmt_cp->bindValue('productioncurrent', $production->productioncurrent);
                        $stmt_cp->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_cp->bindValue('gap', $gap);
                        $stmt_cp->bindValue('start', $content->start);
                        $stmt_cp->bindValue('finish', $content->finish);
                        $stmt_cp->bindValue('record_id', $this->gen_uuid());
                        $stmt_cp->bindValue('create_user_id', $userId);
                        $stmt_cp->bindValue('update_user_id', $userId);
                        $stmt_cp->bindValue('version', 1);
                        $stmt_cp->bindValue('opsayi', count($content->employees));
                        $stmt_cp->bindValue('refsayi', count($content->productions));
                        $stmt_cp->bindValue('taskfinishdescription', $content->taskfinishdescription);
                        $stmt_cp->execute();
                    }
                }
                //seçili her sicil için birer adet e_p_out
                foreach ($content->employees as $employee) {
                    foreach ($content->productions as $production) {
                        $arr_code_number = explode('-', $production->opname);
                        if (count($arr_code_number) > 1) {
                            $production->opnumber = $arr_code_number[count($arr_code_number) - 1];
                            array_pop($arr_code_number);
                            $production->opcode = implode('-', $arr_code_number);
                        } else {
                            $production->opnumber = 0;
                            $production->opcode = $production->opname;
                        }
                        if ($production->opnumber === '') {
                            $production->opnumber = 0;
                        }
                        $sql_ep = "insert into client_production_details 
                            (type,client,day,jobrotation,tasklist,time
                            ,opcode,opnumber,opname,leafmask,leafmaskcurrent
                            ,production,productioncurrent,employee,erprefnumber
                            ,gap,start,finish,record_id
                            ,create_user_id,created_at,update_user_id,updated_at
                            ,version,isexported,opsayi,refsayi,taskfinishdescription)
                        values (:type,:client,:day,:jobrotation,:tasklist,CURRENT_TIMESTAMP(0)
                            ,:opcode,:opnumber,:opname,'01','01'
                            ,:production,:productioncurrent,:employee,:erprefnumber
                            ,:gap,:start,:finish,:record_id
                            ,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0)
                            ,:version,false,:opsayi,:refsayi,:taskfinishdescription)
                        ";
                        $stmt_ep = $conn->prepare($sql_ep);
                        $stmt_ep->bindValue('type', 'e_p_out');
                        $stmt_ep->bindValue('client', $content->client);
                        $stmt_ep->bindValue('day', $content->day);
                        $stmt_ep->bindValue('jobrotation', $content->jobrotation);
                        $stmt_ep->bindValue('tasklist', '');
                        $stmt_ep->bindValue('opcode', $production->opcode);
                        $stmt_ep->bindValue('opnumber', $production->opnumber);
                        $stmt_ep->bindValue('opname', $production->opname);
                        $stmt_ep->bindValue('production', $production->productioncurrent);
                        $stmt_ep->bindValue('productioncurrent', $production->productioncurrent);
                        $stmt_ep->bindValue('employee', $employee->code);
                        $stmt_ep->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_ep->bindValue('gap', $gap);
                        $stmt_ep->bindValue('start', $content->start);
                        $stmt_ep->bindValue('finish', $content->finish);
                        $stmt_ep->bindValue('record_id', $this->gen_uuid());
                        $stmt_ep->bindValue('create_user_id', $userId);
                        $stmt_ep->bindValue('update_user_id', $userId);
                        $stmt_ep->bindValue('version', 1);
                        $stmt_ep->bindValue('opsayi', count($content->employees));
                        $stmt_ep->bindValue('refsayi', count($content->productions));
                        $stmt_ep->bindValue('taskfinishdescription', $content->taskfinishdescription);
                        $stmt_ep->execute();
                    }
                }
                $conn->commit();
                $sql = "SELECT * from client_production_details where type not like 'c_s%' and client=:client and day=:day and start=:start and finish=:finish order by start";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $content->client);
                $stmt->bindValue('day', $content->day);
                $stmt->bindValue('start', $content->start);
                $stmt->bindValue('finish', $content->finish);
                $stmt->execute();
                $records = $stmt->fetchAll();
                foreach ($records as $row) {
                    $sql_cld = "SELECT COALESCE(sum(DATE_PART('day', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 24*3600 
                    	+ DATE_PART('hour', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 3600  
                    	+ DATE_PART('minute', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)*60  
                    	+ DATE_PART('second', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)),0) suresn  
                     from client_lost_details cld 
                     where type=:type and client=:client and day=:day and jobrotation=:jobrotation and losttype not in ('YETKİNLİK GÖZLEM') 
                    	and ( start between :start and :finish  
                    	or finish between :start and :finish 
                    	or (start<:start and finish>:finish) 
                    )";
                    $stmt_cld = $conn->prepare($sql_cld);
                    $stmt_cld->bindValue('type', 'l_c');
                    $stmt_cld->bindValue('client', $row['client']);
                    $stmt_cld->bindValue('day', $row['day']);
                    $stmt_cld->bindValue('jobrotation', $row['jobrotation']);
                    $stmt_cld->bindValue('start', $row['start']);
                    $stmt_cld->bindValue('finish', $row['finish']);
                    $stmt_cld->execute();
                    $records_cld = $stmt_cld->fetchAll();
                    $sql_upd = "update client_production_details set losttime=:losttime where id=:id";
                    $stmt_upd = $conn->prepare($sql_upd);
                    $stmt_upd->bindValue('losttime', $records_cld[0]['suresn']);
                    $stmt_upd->bindValue('id', $row['id']);
                    $stmt_upd->execute();
                }
                if ($content->calculate) {
                    $this->efficiencyCalculate($content->day);
                }
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
            //}
        } else {
            return $cba;
        }
        //return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ClientProductionDetail/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientProductionDetail-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbu = $this->checkBeforeUpdate($request, $id, $entity, $v);
        if ($cbu === true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $d_s = new \DateTime($content->start_d." ".$content->start_t);
            $d_f = new \DateTime($content->finish_d." ".$content->finish_t);
            if ($d_s >= $d_f) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ClientProductionDetail.errdatestartgreaterthanfinish', array(), 'ClientProductionDetail'));
            }
            $content->start = $content->start_d." ".$content->start_t;
            $content->finish = $content->finish_d." ".$content->finish_t;
            $start = strtotime($content->start_d." ".$content->start_t);
            $end   = strtotime($content->finish_d." ".$content->finish_t);
            $gap  = $end - $start;
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $sql_pre = "SELECT cpd1.* 
                from client_production_details cpd0 
                join client_production_details cpd1 on 
                    cpd1.client=cpd0.client and cpd1.day=cpd0.day and cpd1.jobrotation=cpd0.jobrotation 
                    and cpd1.start=cpd0.start and cpd1.finish=cpd0.finish and cpd1.type=cpd0.type
                where cpd0.id=:id";
                $stmt_pre = $conn->prepare($sql_pre);
                $stmt_pre->bindValue('id', $content->id);
                $stmt_pre->execute();
                $records_pre = $stmt_pre->fetchAll();
                foreach ($records_pre as $row_pre) {
                    foreach ($content->productions as $production) {
                        if ($production->opname === $row_pre['opname']) {
                            $production->mould = $row_pre['mould'];
                            $production->mouldgroup = $row_pre['mouldgroup'];
                            $production->calculatedtpp = $row_pre['calculatedtpp'];
                            $production->tasklist = $row_pre['tasklist'];
                        }
                    }
                }

                //şimdilik silinen kayıtlar için audit olmayacak
                $sql = "delete from client_production_details where finish is not null and id in (SELECT cpd1.id
                    from client_production_details cpd0 
                    join client_production_details cpd1 on 
                        cpd1.client=cpd0.client and cpd1.day=cpd0.day and cpd1.jobrotation=cpd0.jobrotation 
                        and cpd1.start=cpd0.start and cpd1.finish=cpd0.finish and cpd1.type=cpd0.type
                    where cpd0.id=:id) ";
                if ($content->tip == 'Operatör') {
                    $sql .= " and type in ('e_p','e_p_confirm','e_p_out') ";
                }
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('id', $content->id);
                //$stmt->bindValue('start', $content->start);
                //$stmt->bindValue('finish', $content->finish);
                $stmt->execute();
                //yenileri oluştur
                if ($content->tip == 'İstasyon') {
                    //seçili her iş için birer adet c_p_out
                    foreach ($content->productions as $production) {
                        $arr_code_number = explode('-', $production->opname);
                        if (count($arr_code_number) > 1) {
                            $production->opnumber = $arr_code_number[count($arr_code_number) - 1];
                            array_pop($arr_code_number);
                            $production->opcode = implode('-', $arr_code_number);
                        } else {
                            $production->opnumber = 0;
                            $production->opcode = $production->opname;
                        }
                        if ($production->opnumber === '') {
                            $production->opnumber = 0;
                        }
                        $sql_cp = "insert into client_production_details 
                            (type,client,day,jobrotation,time
                            ,opcode,opnumber,opname,leafmask,leafmaskcurrent
                            ,production,productioncurrent,erprefnumber
                            ,gap,start,finish,record_id
                            ,mould,mouldgroup,calculatedtpp,tasklist
                            ,create_user_id,created_at,update_user_id,updated_at
                            ,version,isexported,opsayi,refsayi,taskfinishdescription)
                        values (:type,:client,:day,:jobrotation,CURRENT_TIMESTAMP(0)
                            ,:opcode,:opnumber,:opname,'01','01'
                            ,:production,:productioncurrent,:erprefnumber
                            ,:gap,:start,:finish,:record_id
                            ,:mould,:mouldgroup,:calculatedtpp,:tasklist
                            ,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0)
                            ,:version,false,:opsayi,:refsayi,:taskfinishdescription)
                        ";
                        $stmt_cp = $conn->prepare($sql_cp);
                        $stmt_cp->bindValue('type', 'c_p_out');
                        $stmt_cp->bindValue('client', $content->client);
                        $stmt_cp->bindValue('day', $content->day);
                        $stmt_cp->bindValue('jobrotation', $content->jobrotation);
                        $stmt_cp->bindValue('opcode', $production->opcode);
                        $stmt_cp->bindValue('opnumber', $production->opnumber);
                        $stmt_cp->bindValue('opname', $production->opname);
                        $stmt_cp->bindValue('production', $production->productioncurrent);
                        $stmt_cp->bindValue('productioncurrent', $production->productioncurrent);
                        $stmt_cp->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_cp->bindValue('gap', $gap);
                        $stmt_cp->bindValue('start', $content->start);
                        $stmt_cp->bindValue('finish', $content->finish);
                        $stmt_cp->bindValue('record_id', $this->gen_uuid());
                        $stmt_cp->bindValue('mould', $production->mould);
                        $stmt_cp->bindValue('mouldgroup', $production->mouldgroup);
                        $stmt_cp->bindValue('calculatedtpp', $production->calculatedtpp);
                        $stmt_cp->bindValue('tasklist', ($production->tasklist !== null && $production->tasklist !== 'null' ? $production->tasklist : ''));
                        $stmt_cp->bindValue('create_user_id', $userId);
                        $stmt_cp->bindValue('update_user_id', $userId);
                        $stmt_cp->bindValue('version', 1);
                        $stmt_cp->bindValue('opsayi', count($content->employees));
                        $stmt_cp->bindValue('refsayi', count($content->productions));
                        $stmt_cp->bindValue('taskfinishdescription', $content->taskfinishdescription);
                        $stmt_cp->execute();
                    }
                }
                //seçili her sicil için birer adet e_p_out
                foreach ($content->employees as $employee) {
                    foreach ($content->productions as $production) {
                        $arr_code_number = explode('-', $production->opname);
                        if (count($arr_code_number) > 1) {
                            $production->opnumber = $arr_code_number[count($arr_code_number) - 1];
                            array_pop($arr_code_number);
                            $production->opcode = implode('-', $arr_code_number);
                        } else {
                            $production->opnumber = 0;
                            $production->opcode = $production->opname;
                        }
                        if ($production->opnumber === '') {
                            $production->opnumber = 0;
                        }
                        $sql_ep = "insert into client_production_details 
                            (type,client,day,jobrotation,time
                            ,opcode,opnumber,opname,leafmask,leafmaskcurrent
                            ,production,productioncurrent,employee,erprefnumber
                            ,gap,start,finish,record_id
                            ,mould,mouldgroup,calculatedtpp,tasklist
                            ,create_user_id,created_at,update_user_id,updated_at
                            ,version,isexported,opsayi,refsayi,taskfinishdescription)
                        values (:type,:client,:day,:jobrotation,CURRENT_TIMESTAMP(0)
                            ,:opcode,:opnumber,:opname,'01','01'
                            ,:production,:productioncurrent,:employee,:erprefnumber
                            ,:gap,:start,:finish,:record_id
                            ,:mould,:mouldgroup,:calculatedtpp,:tasklist
                            ,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0)
                            ,:version,false,:opsayi,:refsayi,:taskfinishdescription)
                        ";
                        $stmt_ep = $conn->prepare($sql_ep);
                        $stmt_ep->bindValue('type', 'e_p_out');
                        $stmt_ep->bindValue('client', $content->client);
                        $stmt_ep->bindValue('day', $content->day);
                        $stmt_ep->bindValue('jobrotation', $content->jobrotation);
                        $stmt_ep->bindValue('opcode', $production->opcode);
                        $stmt_ep->bindValue('opnumber', $production->opnumber);
                        $stmt_ep->bindValue('opname', $production->opname);
                        $stmt_ep->bindValue('production', $production->productioncurrent);
                        $stmt_ep->bindValue('productioncurrent', $production->productioncurrent);
                        $stmt_ep->bindValue('employee', $employee->code);
                        $stmt_ep->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_ep->bindValue('gap', $gap);
                        $stmt_ep->bindValue('start', $content->start);
                        $stmt_ep->bindValue('finish', $content->finish);
                        $stmt_ep->bindValue('record_id', $this->gen_uuid());
                        $stmt_ep->bindValue('mould', $production->mould);
                        $stmt_ep->bindValue('mouldgroup', $production->mouldgroup);
                        $stmt_ep->bindValue('calculatedtpp', $production->calculatedtpp);
                        $stmt_ep->bindValue('tasklist', ($production->tasklist !== null && $production->tasklist !== 'null' ? $production->tasklist : ''));
                        $stmt_ep->bindValue('create_user_id', $userId);
                        $stmt_ep->bindValue('update_user_id', $userId);
                        $stmt_ep->bindValue('version', 1);
                        $stmt_ep->bindValue('opsayi', count($content->employees));
                        $stmt_ep->bindValue('refsayi', count($content->productions));
                        $stmt_ep->bindValue('taskfinishdescription', $content->taskfinishdescription);
                        $stmt_ep->execute();
                    }
                }
                $conn->commit();
                $sql = "SELECT * from client_production_details where type not like 'c_s%' and client=:client and day=:day and start=:start and finish=:finish order by start";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $content->client);
                $stmt->bindValue('day', $content->day);
                $stmt->bindValue('start', $content->start);
                $stmt->bindValue('finish', $content->finish);
                $stmt->execute();
                $records = $stmt->fetchAll();
                foreach ($records as $row) {
                    $sql_cld = "SELECT COALESCE(sum(DATE_PART('day', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 24*3600 
                    	+ DATE_PART('hour', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 3600  
                    	+ DATE_PART('minute', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)*60  
                    	+ DATE_PART('second', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)),0) suresn  
                     from client_lost_details cld 
                     where type=:type and client=:client and day=:day and jobrotation=:jobrotation and losttype not in ('YETKİNLİK GÖZLEM') 
                    	and ( start between :start and :finish  
                    	or finish between :start and :finish 
                    	or (start<:start and finish>:finish) 
                    )";
                    $stmt_cld = $conn->prepare($sql_cld);
                    $stmt_cld->bindValue('type', 'l_c');
                    $stmt_cld->bindValue('client', $row['client']);
                    $stmt_cld->bindValue('day', $row['day']);
                    $stmt_cld->bindValue('jobrotation', $row['jobrotation']);
                    $stmt_cld->bindValue('start', $row['start']);
                    $stmt_cld->bindValue('finish', $row['finish']);
                    $stmt_cld->execute();
                    $records_cld = $stmt_cld->fetchAll();
                    $sql_upd = "update client_production_details set losttime=:losttime where id=:id";
                    $stmt_upd = $conn->prepare($sql_upd);
                    $stmt_upd->bindValue('losttime', $records_cld[0]['suresn']);
                    $stmt_upd->bindValue('id', $row['id']);
                    $stmt_upd->execute();
                }
                if ($content->calculate) {
                    $this->efficiencyCalculate($content->day);
                }
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
            //}
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/ClientProductionDetail", name="ClientProductionDetail-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY, 0);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients', 'code', 'code', '', "  ");//and uuid is not null
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees', 'code', "concat(code,'-',name)", '', ' and finish is null ');
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['employees'] = json_decode($employees->getContent())->records;
            return $this->render('Modules/ClientProductionDetail.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientProductionDetail/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ClientProductionDetail-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ClientProductionDetail', $id);

            return $this->render('Modules/ClientProductionDetail.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientProductionDetail/{id}", requirements={"id": "\d+"}, name="ClientProductionDetail-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ClientProductionDetail', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientProductionDetail/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientProductionDetail-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientProductionDetail/allrecordslost/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientProductionDetail-getallrecords-lost", options={"expose"=true}, methods={"GET"})
     */
    public function getAllRecordsLostAction(Request $request, $_locale, $pg, $lm)
    {
        //$cbg = $this->checkBeforeGet($request);
        //if ($cbg === true) {
        $records = array();
        $_sql = "SELECT 0 del,cld.id,cld.client,cld.day,cld.jobrotation
                        ,cld.start,cld.finish,cld.losttype,cld.version
                    FROM client_lost_details cld
                    WHERE cld.type in ('l_c') and cld.finish is not null and cld.delete_user_id is null and finish-start>'00:01:00' @@where@@
                    ORDER BY cld.day DESC,cld.start desc";
        $records['ClientProductionDetail'] = $this->getAllRecordsSQL($scope, $request, $_sql, $pg, $lm);

        return new JsonResponse($records);
        //} else {
        //    return $cbg;
        //}
    }

    /**
     * @Route(path="/ClientProductionDetail/checkTaskNewOrVeryOld", name="ClientProductionDetail-checkTaskNewOrVeryOld", options={"expose"=true}, methods={"POST"})
     */
    public function checkTaskNewOrVeryOldAction(Request $request, $_locale)
    {
        $parameters = json_decode($request->getContent());
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //where opname in ('".implode("','",$parameters->opname)."')
        $sql = "SELECT id,cast(extract(epoch from (CURRENT_TIMESTAMP-start::timestamp)) as integer) > 90*24*3600 sonuretim
        from client_production_details 
        where opname = :opname and day>CURRENT_DATE - interval '4 months'
        order by start desc limit 1";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('opname', $parameters->opname);
        $stmt->execute();
        $records = $stmt->fetchAll();
        return new JsonResponse(array("totalProperty" => count($records),"records" => $records));
    }
}
