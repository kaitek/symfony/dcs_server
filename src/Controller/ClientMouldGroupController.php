<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\ClientMouldGroup;
use App\Entity\ClientMouldDetail;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientMouldGroupController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:Client';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/ClientMouldGroup/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientMouldGroup-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entityM = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        $cbd = $this->checkBeforeDelete($request, $id, $entityM, $v);
        if ($cbd === true) {
            $user = $this->getUser();
            $userId = $user->getId();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $client=$entityM->getCode();
                //grup kayıtları tespit ediliyor
                $qb_mouldg = $em->createQueryBuilder();
                $qb_mouldg = $qb_mouldg->select('cmg.id,cmg.code')
                    ->from('App:ClientMouldGroup', 'cmg')
                    ->where('cmg.client=:client')
                    ->setParameters(array('client' => $client));
                $tlg=$qb_mouldg->getQuery()->getArrayResult();
                if(count($tlg)!==0) {
                    foreach($tlg as $rowg) {
                        //grubun detay kayıtları siliniyor
                        $qb_mouldd = $em->createQueryBuilder();
                        $qb_mouldd = $qb_mouldd->select('cmd.id')
                            ->from('App:ClientMouldDetail', 'cmd')
                            ->where('cmd.client=:client and cmd.clientmouldgroup=:clientmouldgroup')
                            ->setParameters(array('client' => $client, 'clientmouldgroup' => $rowg['code']));
                        $tld=$qb_mouldd->getQuery()->getArrayResult();
                        if(count($tld)!==0) {
                            foreach($tld as $rowd) {
                                $entity_md_del = $this->getDoctrine()
                                    ->getRepository('App:ClientMouldDetail')
                                    ->find($rowd['id']);
                                $entity_md_del->setDeleteuserId($userId);
                                $em->persist($entity_md_del);
                                $em->flush();
                                $em->remove($entity_md_del);
                                $em->flush();
                            }
                        }
                        $entity_mg_del = $this->getDoctrine()
                            ->getRepository('App:ClientMouldGroup')
                            ->find($rowg['id']);
                        $entity_mg_del->setDeleteuserId($userId);
                        $em->persist($entity_mg_del);
                        $em->flush();
                        $em->remove($entity_mg_del);
                        $em->flush();
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();

                // Foreign key violation mesajı mı?
                $msg = $e->getMessage();

                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbd;
        }
    }

    /**
     * @Route(path="/ClientMouldGroup/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientMouldGroup-getComboValues-special", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesSpecial(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = '')
    {
        if($request->query->get('options')!=='') {
            $uri = strtolower($request->getBaseUrl());
            $options=json_decode($request->query->get('options'));
            if($options->where!=='') {
                if($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149') {
                    $_where=" and code ilike '%".$val."%' and finish is null ";
                } else {
                    $sql="SELECT string_agg(code,''',''')code from client_details where client='".$options->where."' and iotype='I' and finish is null";
                    /** @var EntityManager $em */
                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $records = $stmt->fetchAll();
                    if(count($records) == 0) {
                        return $this->msgError("Belirtilen kriterlere uygun kayıt bulunamadı.");
                    }
                    $_where=" and mould in ('".$records[0]["code"]."') and finish is null ";
                }
            }
        }
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val, $_where);
    }

    public function getNewEntity()
    {
        return new Client();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('c.id,c.code,c.code client'
                .',c.version')
                ->from('App:Client', 'c')
                ->where('c.deleteuserId is null')
                ->orderBy('c.code', 'ASC');
        $queries['ClientMouldGroup'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ClientMouldGroup/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientMouldGroup-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba=$this->checkBeforeAdd($request);
        if($cba===true) {
            $entityC = new Client();
            $this->setEntityWithRequest($entityC, $request);
            $validator = ($this->_container==null ? $this->container : $this->_container)->get('validator');
            $client=$this->_requestData->client;
            $time=new \DateTime();
            $user = $this->getUser();
            $userId = $user->getId();
            $groups=$this->_requestData->groups;
            foreach($groups as $group) {
                $entityCMG = new ClientMouldGroup();
                $entityCMG->setCode($group->code);
                $entityCMG->setClient($client);
                $entityCMG->setProductionmultiplier($group->productionmultiplier);
                $entityCMG->setIntervalmultiplier($group->intervalmultiplier);
                $entityCMG->setSetup($group->setup);
                $entityCMG->setCycletime($group->cycletime);
                $entityCMG->setLotcount($group->lotcount);
                $entityCMG->setOperatorcount($group->operatorcount);
                $entityCMG->setIsdefault($group->isdefault);
                $entityCMG->setStart($time);
                $entityCMG->setCreateuserId($userId);
                $errors = $this->getValidateMessage($validator->validate($entityCMG));
                if ($errors!==false) {
                    return $errors;
                }
                $details=$group->_details;
                foreach($details as $detail) {
                    $entityCMD = new ClientMouldDetail();
                    $entityCMD->setClient($client);
                    $entityCMD->setClientmouldgroup($group->code);
                    $entityCMD->setMouldgroup($detail->mouldgroup);
                    $entityCMD->setStart($time);
                    $entityCMD->setCreateuserId($userId);
                    $errors = $this->getValidateMessage($validator->validate($entityCMD));
                    if ($errors!==false) {
                        return $errors;
                    }
                }
            }
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $qb_mould = $em->createQueryBuilder();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                foreach($groups as $group) {
                    $entityCMG = new ClientMouldGroup();
                    $qb_mouldg = $em->createQueryBuilder();
                    $qb_mouldg = $qb_mouldg->select('cmg.id')
                        ->from('App:ClientMouldGroup', 'cmg')
                        ->where('cmg.code=:code and cmg.client=:client')
                        ->setParameters(array('code' => $group->code, 'client' => $client));
                    $tlg=$qb_mouldg->getQuery()->getArrayResult();
                    if(count($tlg)!==0) {
                        throw new \Error(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientMouldGroup.errRecordGroup', array(), 'ClientMouldGroup'));
                    }
                    $entityCMG->setCode($group->code);
                    $entityCMG->setClient($client);
                    $entityCMG->setProductionmultiplier($group->productionmultiplier);
                    $entityCMG->setIntervalmultiplier($group->intervalmultiplier);
                    $entityCMG->setSetup($group->setup);
                    $entityCMG->setCycletime($group->cycletime);
                    $entityCMG->setLotcount($group->lotcount);
                    $entityCMG->setOperatorcount($group->operatorcount);
                    $entityCMG->setIsdefault($group->isdefault);
                    $entityCMG->setStart($time);
                    $entityCMG->setCreateuserId($userId);
                    $errors = $this->getValidateMessage($validator->validate($entityCMG));
                    $em->persist($entityCMG);
                    $em->flush();
                    $details=$group->_details;
                    foreach($details as $detail) {
                        $entityCMD = new ClientMouldDetail();
                        $qb_mouldd = $em->createQueryBuilder();
                        $qb_mouldd = $qb_mouldd->select('cmd.id')
                            ->from('App:ClientMouldDetail', 'cmd')
                            ->where('cmd.client=:client and cmd.clientmouldgroup=:clientmouldgroup and cmd.mouldgroup=:mouldgroup')
                            ->setParameters(array('client' => $client, 'clientmouldgroup' => $group->code,'mouldgroup'=>$detail->mouldgroup));
                        $tld=$qb_mouldd->getQuery()->getArrayResult();
                        if(count($tld)!==0) {
                            throw new \Error(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientMouldGroup.errRecordDetail', array(), 'ClientMouldGroup'));
                        }
                        $entityCMD->setClient($client);
                        $entityCMD->setClientmouldgroup($group->code);
                        $entityCMD->setMouldgroup($detail->mouldgroup);
                        $entityCMD->setStart($time);
                        $entityCMD->setCreateuserId($userId);
                        $em->persist($entityCMD);
                        $em->flush();
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            } catch (\Error $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }

        } else {
            return $cba;
        }
    }

    /**
     * @Route(path="/ClientMouldGroup/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientMouldGroup-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entityC = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbu=$this->checkBeforeUpdate($request, $id, $entityC, $v);
        if($cbu===true) {
            $this->setEntityWithRequest($entityC, $request);
            $user = $this->getUser();
            $userId = $user->getId();
            $validator = ($this->_container==null ? $this->container : $this->_container)->get('validator');
            $client=$this->_requestData->client;
            $time=new \DateTime();
            $groups=$this->_requestData->groups;
            foreach($groups as $group) {
                $entityCMG = new ClientMouldGroup();
                $entityCMG->setCode($group->code);
                $entityCMG->setClient($client);
                $entityCMG->setProductionmultiplier($group->productionmultiplier);
                $entityCMG->setIntervalmultiplier($group->intervalmultiplier);
                $entityCMG->setSetup($group->setup);
                $entityCMG->setCycletime($group->cycletime);
                $entityCMG->setLotcount($group->lotcount);
                $entityCMG->setOperatorcount($group->operatorcount);
                $entityCMG->setIsdefault($group->isdefault);
                $entityCMG->setStart($time);
                $entityCMG->setCreateuserId($userId);
                $errors = $this->getValidateMessage($validator->validate($entityCMG));
                if ($errors!==false) {
                    return $errors;
                }
                $details=$group->_details;
                foreach($details as $detail) {
                    $entityCMD = new ClientMouldDetail();
                    $entityCMD->setClient($client);
                    $entityCMD->setClientmouldgroup($group->code);
                    $entityCMD->setMouldgroup($detail->mouldgroup);
                    $entityCMD->setStart($time);
                    $entityCMD->setCreateuserId($userId);
                    $errors = $this->getValidateMessage($validator->validate($entityCMD));
                    if ($errors!==false) {
                        return $errors;
                    }
                }
            }
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $arr_g=[];
                foreach($groups as $group) {
                    $arr_g[]=$group->code;
                    $qb_mouldg = $em->createQueryBuilder();
                    if(isset($group->id)&&$group->id!==''&&$group->id!==null) {
                        $entityCMG = $this->getDoctrine()
                            ->getRepository('App:ClientMouldGroup')
                            ->find($group->id);
                        $entityCMG->setUpdateuserId($userId);
                        $qb_mouldg = $qb_mouldg->select('cmg.id')
                            ->from('App:ClientMouldGroup', 'cmg')
                            ->where('cmg.code=:code and cmg.client=:client and cmg.id<>:id')
                            ->setParameters(array('code' => $group->code, 'client' => $client,'id'=>$group->id));
                    } else {
                        $entityCMG = new ClientMouldGroup();
                        $entityCMG->setCreateuserId($userId);
                        $entityCMG->setUpdateuserId($userId);
                        $qb_mouldg = $qb_mouldg->select('cmg.id')
                            ->from('App:ClientMouldGroup', 'cmg')
                            ->where('cmg.code=:code and cmg.client=:client')
                            ->setParameters(array('code' => $group->code, 'client' => $client));
                    }
                    $tlg=$qb_mouldg->getQuery()->getArrayResult();
                    if(count($tlg)!==0) {
                        throw new \Error(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientMouldGroup.errRecordGroup', array(), 'ClientMouldGroup'));
                    }
                    $entityCMG->setCode($group->code);
                    $entityCMG->setClient($client);
                    $entityCMG->setProductionmultiplier($group->productionmultiplier);
                    $entityCMG->setIntervalmultiplier($group->intervalmultiplier);
                    $entityCMG->setSetup($group->setup);
                    $entityCMG->setCycletime($group->cycletime);
                    $entityCMG->setLotcount($group->lotcount);
                    $entityCMG->setOperatorcount($group->operatorcount);
                    $entityCMG->setIsdefault($group->isdefault);
                    $entityCMG->setStart($time);
                    $errors = $this->getValidateMessage($validator->validate($entityCMG));
                    $em->persist($entityCMG);
                    $em->flush();
                    $details=$group->_details;
                    $arr_d=[];
                    foreach($details as $detail) {
                        $arr_d[]=$detail->mouldgroup;
                        $qb_mouldd = $em->createQueryBuilder();
                        if(isset($detail->id)&&$detail->id!==''&&$detail->id!==null) {
                            $entityCMD = $this->getDoctrine()
                                ->getRepository('App:ClientMouldDetail')
                                ->find($detail->id);
                            $entityCMD->setUpdateuserId($userId);
                            $qb_mouldd = $qb_mouldd->select('cmd.id')
                                ->from('App:ClientMouldDetail', 'cmd')
                                ->where('cmd.client=:client and cmd.clientmouldgroup=:clientmouldgroup and cmd.mouldgroup=:mouldgroup and cmd.id<>:id')
                                ->setParameters(array('client' => $client, 'clientmouldgroup' => $group->code,'mouldgroup'=>$detail->mouldgroup,'id'=>$detail->id));
                        } else {
                            $entityCMD = new ClientMouldDetail();
                            $entityCMD->setCreateuserId($userId);
                            $entityCMD->setUpdateuserId($userId);
                            $qb_mouldd = $qb_mouldd->select('cmd.id')
                                ->from('App:ClientMouldDetail', 'cmd')
                                ->where('cmd.client=:client and cmd.clientmouldgroup=:clientmouldgroup and cmd.mouldgroup=:mouldgroup')
                                ->setParameters(array('client' => $client, 'clientmouldgroup' => $group->code,'mouldgroup'=>$detail->mouldgroup));
                        }
                        $tld=$qb_mouldd->getQuery()->getArrayResult();
                        if(count($tld)!==0) {
                            throw new \Error(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientMouldGroup.errRecordDetail', array(), 'ClientMouldGroup'));
                        }
                        $entityCMD->setClient($client);
                        $entityCMD->setClientmouldgroup($group->code);
                        $entityCMD->setMouldgroup($detail->mouldgroup);
                        $entityCMD->setStart($time);
                        $em->persist($entityCMD);
                        $em->flush();
                    }
                    //silinen detay kayıtları tespit ediliyor
                    $qb_mouldd = $em->createQueryBuilder();
                    $qb_mouldd = $qb_mouldd->select('cmd.id')
                        ->from('App:ClientMouldDetail', 'cmd')
                        ->where('cmd.client=:client and cmd.clientmouldgroup=:clientmouldgroup and cmd.mouldgroup not in (:mouldgroup)')
                        ->setParameters(array('client' => $client, 'clientmouldgroup' => $group->code,'mouldgroup'=>$arr_d));
                    $tld=$qb_mouldd->getQuery()->getArrayResult();
                    if(count($tld)!==0) {
                        foreach($tld as $rowd) {
                            $entity_md_del = $this->getDoctrine()
                                ->getRepository('App:ClientMouldDetail')
                                ->find($rowd['id']);
                            $entity_md_del->setDeleteuserId($userId);
                            $em->persist($entity_md_del);
                            $em->flush();
                            $em->remove($entity_md_del);
                            $em->flush();
                        }
                    }
                }
                //silinen grup kayıtları tespit ediliyor
                $qb_mouldg = $em->createQueryBuilder();
                $qb_mouldg = $qb_mouldg->select('cmg.id,cmg.code')
                    ->from('App:ClientMouldGroup', 'cmg')
                    ->where('cmg.code not in (:code) and cmg.client=:client')
                    ->setParameters(array('code' => $arr_g, 'client' => $client));
                $tlg=$qb_mouldg->getQuery()->getArrayResult();
                if(count($tlg)!==0) {
                    foreach($tlg as $rowg) {
                        //grubun detay kayıtları siliniyor
                        $qb_mouldd = $em->createQueryBuilder();
                        $qb_mouldd = $qb_mouldd->select('cmd.id')
                            ->from('App:ClientMouldDetail', 'cmd')
                            ->where('cmd.client=:client and cmd.clientmouldgroup=:clientmouldgroup')
                            ->setParameters(array('client' => $client, 'clientmouldgroup' => $rowg['code']));
                        $tld=$qb_mouldd->getQuery()->getArrayResult();
                        if(count($tld)!==0) {
                            foreach($tld as $rowd) {
                                $entity_md_del = $this->getDoctrine()
                                    ->getRepository('App:ClientMouldDetail')
                                    ->find($rowd['id']);
                                $entity_md_del->setDeleteuserId($userId);
                                $em->persist($entity_md_del);
                                $em->flush();
                                $em->remove($entity_md_del);
                                $em->flush();
                            }
                        }
                        $entity_mg_del = $this->getDoctrine()
                            ->getRepository('App:ClientMouldGroup')
                            ->find($rowg['id']);
                        $entity_mg_del->setDeleteuserId($userId);
                        $em->persist($entity_mg_del);
                        $em->flush();
                        $em->remove($entity_mg_del);
                        $em->flush();
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            } catch (\Error $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }

        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/ClientMouldGroup", name="ClientMouldGroup-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/ClientMouldGroup.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientMouldGroup/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ClientMouldGroup-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ClientMouldGroup', $id);

            return $this->render('Modules/ClientMouldGroup.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientMouldGroup/{id}", requirements={"id": "\d+"}, name="ClientMouldGroup-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Client', $id);
            if($records["ClientMouldGroup"]["totalProperty"]>0) {
                $records["ClientMouldGroup"]["records"][0]["_groups"]=[];
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                /** @var QueryBuilder $qb */
                $qb = $em->createQueryBuilder();
                $qb = $qb->select('cmg.id,cmg.code,cmg.client,cmg.productionmultiplier'
                    .',cmg.intervalmultiplier,cmg.setup,cmg.cycletime'
                    .',cmg.lotcount,cmg.operatorcount,cmg.isdefault,cmg.start,cmg.finish,cmg.version')
                    ->from('App:ClientMouldGroup', 'cmg')
                    ->where('cmg.deleteuserId is null and cmg.finish is null ')
                    ->andWhere('cmg.client=:client')
                    ->orderBy('cmg.code', 'ASC');
                $qb=$qb->getQuery();
                $qb->setParameter('client', $records["ClientMouldGroup"]["records"][0]["client"]);
                $groups=$qb->getArrayResult();
                foreach($groups as $group) {
                    /** @var QueryBuilder $qbd */
                    $qbd = $em->createQueryBuilder();
                    $qbd = $qbd->select('cmd.id,cmd.client,cmd.clientmouldgroup,cmd.mouldgroup
                            ,cmd.start,cmd.finish'
                        . ',cmd.version')
                        ->from('App:ClientMouldDetail', 'cmd')
                        ->where('cmd.deleteuserId is null and cmd.finish is null')
                        ->andWhere('cmd.client=:client')
                        ->andWhere('cmd.clientmouldgroup=:clientmouldgroup')
                        ->orderBy('cmd.mouldgroup', 'ASC');
                    $qbd=$qbd->getQuery();
                    $qbd->setParameter('client', $group["client"]);
                    $qbd->setParameter('clientmouldgroup', $group["code"]);
                    $details=$qbd->getArrayResult();
                    $group['_details']=$details;
                    $records["ClientMouldGroup"]["records"][0]["_groups"][]=$group;
                }
            }
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientMouldGroup/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientMouldGroup-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
