<?php
/*
 * kayıt girişi toplu yapılacak
 * silme için gridde her kaydın yanındaki check seçilerek toplu silme yapılacak
 *
 */

namespace App\Controller;

use App\Entity\ClientLostDetail;
use App\Entity\TaskRejectDetail;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TaskRejectDetailController extends DcsBaseController implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:TaskRejectDetail';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/TaskRejectDetail/{pg}/{lm}/{id}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+"}, name="TaskRejectDetail-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v=0)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        if ($cbd === true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $user = $this->getUser();
            $userId = $user->getId();
            //$records=$this->getDoctrine()
            //    ->getRepository(self::ENTITY)
            //    ->findBy(array('client' => $entity->getClient(), 'day' => $entity->getDay(), 'jobrotation' => $entity->getJobrotation(), 'start' => $entity->getStart(), 'finish' => $entity->getFinish()));
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                //şimdilik silinen kayıtlar için audit olmayacak
                $sql="delete from task_reject_details 
                where client=:client and day=:day and jobrotation=:jobrotation 
                    and opname=:opname and time=:time and rejecttype=:rejecttype and quantityscrap=:quantityscrap";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $content->record->client);
                $stmt->bindValue('day', $content->record->day);
                $stmt->bindValue('jobrotation', $content->record->jobrotation);
                $stmt->bindValue('opname', $content->record->opname);
                $stmt->bindValue('time', $content->record->time);
                $stmt->bindValue('rejecttype', $content->record->rejecttype);
                $stmt->bindValue('quantityscrap', $content->record->quantityscrap);
                $stmt->execute();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
        //}
        } else {
            return $cbd;
        }
    }

    public function getNewEntity()
    {
        return new TaskRejectDetail();
    }

    public function getSqlStr()
    {
        $queries = array();
        $_sql="SELECT 0 del,min(trd.id)id,trd.client,trd.day,trd.jobrotation
            ,trd.time,trd.time_d,trd.time_t,trd.description,trd.rejecttype
            ,trd.employee,trd.opname,trd.empname
            ,string_agg(distinct trd.rejects,'|')rejects,string_agg(distinct trd.opnames,'|')opnames
            ,max(trd.quantityreject)quantityreject
            ,max(trd.quantityretouch)quantityretouch
            ,max(trd.quantityscrap)quantityscrap
            ,max(trd.version) \"version\" 
        FROM (
            select trd.id,trd.client,trd.day,trd.jobrotation,trd.time,trd.description,trd.rejecttype
                ,trd.employee,trd.opname,e.name empname
                ,concat(trd.rejecttype,'~',trd.quantityscrap,'~',trd.description) rejects
                ,concat(trd.erprefnumber,'~',trd.opname,'~',e.name,'~','~') opnames
                ,cast(trd.time as varchar(10)) time_d,right(cast(trd.time as varchar(19)),8)time_t
                ,trd.quantityreject,trd.quantityretouch,trd.quantityscrap,trd.version
            from task_reject_details trd
            left join employees e on e.code=trd.employee
            where trd.type in ('e_r') and trd.day>CURRENT_DATE - interval '40 days'
        )trd
        where 1=1 @@where@@
        GROUP BY trd.client,trd.day,trd.jobrotation,trd.time,trd.time_d,trd.time_t,trd.description,trd.rejecttype,trd.employee,trd.opname,trd.empname
        ORDER BY trd.day DESC,trd.time desc";
        $queries['TaskRejectDetail'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    public function getQBQuery()
    {
        $queries = array();
        //    /** @var EntityManager $em */
        //    $em = $this->getDoctrine()->getManager();
        //    $qb = $em->createQueryBuilder();
        //    $qb = $qb->select('jre.id,jre.day,jre.week,jre.jobrotation'
        //            . ',jre.jobrotationteam,jre.employee,jre.isovertime'
        //            . ',jre.beginval,jre.endval,jre.worktime,jre.breaktime'
        //            . ',jre.version')
        //            ->from('App:TaskRejectDetail', 'jre')
        //            ->where('jre.deleteuserId is null')
        //            ->orderBy('jre.day', 'DESC');
        //    $queries['TaskRejectDetail'] = array('qb' => $qb, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/TaskRejectDetail/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskRejectDetail-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba=$this->checkBeforeAdd($request);
        if ($cba===true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            //$d_t=new \DateTime($content->time_d." ".$content->time_t);
            //if ($d_s>=$d_f) {
            //    return $this->msgError(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskRejectDetail.errdatestartgreaterthanfinish', array(), 'TaskRejectDetail'));
            //}
            //if ($content->employees=='') {
            //    return $this->msgError(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskRejectDetail.errnotfoundemployees', array(), 'TaskRejectDetail'));
            //}
            if (count($content->rejects)==0) {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('TaskRejectDetail.errnotfoundrejects', array(), 'TaskRejectDetail'));
            }
            $content->time=$content->time_d." ".$content->time_t;
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $production=$content->production;
                foreach ($content->rejects as $reject) {
                    $sql_tr="insert into task_reject_details 
                        (type,client,day,jobrotation
                        ,rejecttype,materialunit,tasklist,time
                        ,mould,mouldgroup,opcode,opnumber,opname,opdescription
                        ,quantityreject,quantityretouch,quantityscrap
                        ,employee,erprefnumber,description
                        ,create_user_id,created_at,update_user_id,updated_at
                        ,version,isexported)
                    values (:type,:client,:day,:jobrotation
                        ,:rejecttype,:materialunit,:tasklist,:time
                        ,:mould,:mouldgroup,:opcode,:opnumber,:opname,:opdescription
                        ,:quantityreject,:quantityretouch,:quantityscrap
                        ,:employee,:erprefnumber,:description
                        ,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0)
                        ,:version,false)
                    ";
                    $stmt_tr = $conn->prepare($sql_tr);
                    $stmt_tr->bindValue('type', 't_r');
                    $stmt_tr->bindValue('client', $content->client);
                    $stmt_tr->bindValue('day', $content->day);
                    $stmt_tr->bindValue('jobrotation', $content->jobrotation);

                    $stmt_tr->bindValue('rejecttype', $reject->rejecttype);
                    $stmt_tr->bindValue('materialunit', 'Adet');
                    $stmt_tr->bindValue('tasklist', $production->tasklist);
                    $stmt_tr->bindValue('time', $content->time);

                    $stmt_tr->bindValue('mould', $production->mould);
                    $stmt_tr->bindValue('mouldgroup', $production->mouldgroup);
                    $stmt_tr->bindValue('opcode', $production->opcode);
                    $stmt_tr->bindValue('opnumber', $production->opnumber);
                    $stmt_tr->bindValue('opname', $production->opname);
                    $stmt_tr->bindValue('opdescription', $production->opdescription);

                    $stmt_tr->bindValue('quantityreject', $reject->quantity);
                    $stmt_tr->bindValue('quantityretouch', 0);
                    $stmt_tr->bindValue('quantityscrap', $reject->quantity);

                    $stmt_tr->bindValue('employee', null);
                    $stmt_tr->bindValue('erprefnumber', $production->erprefnumber);
                    $stmt_tr->bindValue('description', $reject->description);

                    $stmt_tr->bindValue('create_user_id', $userId);
                    $stmt_tr->bindValue('update_user_id', $userId);
                    $stmt_tr->bindValue('version', 1);
                    $stmt_tr->execute();

                    $sql_er="insert into task_reject_details 
                        (type,client,day,jobrotation
                        ,rejecttype,materialunit,tasklist,time
                        ,mould,mouldgroup,opcode,opnumber,opname,opdescription
                        ,quantityreject,quantityretouch,quantityscrap
                        ,employee,erprefnumber,description
                        ,create_user_id,created_at,update_user_id,updated_at
                        ,version,isexported)
                    values (:type,:client,:day,:jobrotation
                        ,:rejecttype,:materialunit,:tasklist,:time
                        ,:mould,:mouldgroup,:opcode,:opnumber,:opname,:opdescription
                        ,:quantityreject,:quantityretouch,:quantityscrap
                        ,:employee,:erprefnumber,:description
                        ,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0)
                        ,:version,false)
                    ";
                    $stmt_tr = $conn->prepare($sql_tr);
                    $stmt_tr->bindValue('type', 'e_r');
                    $stmt_tr->bindValue('client', $content->client);
                    $stmt_tr->bindValue('day', $content->day);
                    $stmt_tr->bindValue('jobrotation', $content->jobrotation);

                    $stmt_tr->bindValue('rejecttype', $reject->rejecttype);
                    $stmt_tr->bindValue('materialunit', 'Adet');
                    $stmt_tr->bindValue('tasklist', $production->tasklist);
                    $stmt_tr->bindValue('time', $content->time);

                    $stmt_tr->bindValue('mould', $production->mould);
                    $stmt_tr->bindValue('mouldgroup', $production->mouldgroup);
                    $stmt_tr->bindValue('opcode', $production->opcode);
                    $stmt_tr->bindValue('opnumber', $production->opnumber);
                    $stmt_tr->bindValue('opname', $production->opname);
                    $stmt_tr->bindValue('opdescription', $production->opdescription);

                    $stmt_tr->bindValue('quantityreject', $reject->quantity);
                    $stmt_tr->bindValue('quantityretouch', 0);
                    $stmt_tr->bindValue('quantityscrap', $reject->quantity);

                    $stmt_tr->bindValue('employee', $production->employee);
                    $stmt_tr->bindValue('erprefnumber', $production->erprefnumber);
                    $stmt_tr->bindValue('description', $reject->description);

                    $stmt_tr->bindValue('create_user_id', $userId);
                    $stmt_tr->bindValue('update_user_id', $userId);
                    $stmt_tr->bindValue('version', 1);
                    $stmt_tr->execute();
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
        //}
        } else {
            return $cba;
        }
        //return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/TaskRejectDetail/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskRejectDetail-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        //if ($entity->getCreateUserId()===0&&($this->_container==null ? $this->container : $this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')!==true) {
        //    return $this->msgError(
        //        ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
        //        401
        //    );
        //}
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
        if ($cbu===true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            if (count($content->rejects)==0) {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('TaskRejectDetail.errnotfoundrejects', array(), 'TaskRejectDetail'));
            }
            $content->time=$content->time_d." ".$content->time_t;
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $production=$content->production;
                $rejectold=$content->rejectsold[0];
                foreach ($content->rejects as $reject) {
                    $sql_up="update task_reject_details 
                    set rejecttype=:rejecttypenew
                        ,quantityreject=:quantityrejectnew
                        ,quantityretouch=:quantityretouchnew
                        ,quantityscrap=:quantityscrapnew
                        ,description=:descriptionnew
                    where client=:client and day=:day and jobrotation=:jobrotation 
                        and opname=:opname and time=:time and rejecttype=:rejecttype and quantityscrap=:quantityscrap";
                    $stmt_up = $conn->prepare($sql_up);
                    $stmt_up->bindValue('rejecttypenew', $reject->rejecttype);
                    $stmt_up->bindValue('quantityrejectnew', $reject->quantity);
                    $stmt_up->bindValue('quantityretouchnew', 0);
                    $stmt_up->bindValue('quantityscrapnew', $reject->quantity);
                    $stmt_up->bindValue('descriptionnew', $reject->description);

                    $stmt_up->bindValue('client', $content->client);
                    $stmt_up->bindValue('day', $content->day);
                    $stmt_up->bindValue('jobrotation', $content->jobrotation);

                    $stmt_up->bindValue('opname', $production->opname);
                    $stmt_up->bindValue('time', $content->time);
                    $stmt_up->bindValue('rejecttype', $rejectold->rejecttype);
                    $stmt_up->bindValue('quantityscrap', $rejectold->quantity);

                    $stmt_up->execute();
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
        //}
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/TaskRejectDetail", name="TaskRejectDetail-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY, 0);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            //$employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees', 'code', 'name');
            $rejects = $this->getComboValues($request, $_locale, 1, 1000, 'reject_types', 'code', 'code');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['job_rotations']=json_decode($job_rotations->getContent())->records;
            //$data['extras']['employees']=json_decode($employees->getContent())->records;
            $data['extras']['rejects']=json_decode($rejects->getContent())->records;
            return $this->render('Modules/TaskRejectDetail.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskRejectDetail/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="TaskRejectDetail-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'TaskRejectDetail', $id);

            return $this->render('Modules/TaskRejectDetail.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskRejectDetail/{id}", requirements={"id": "\d+"}, name="TaskRejectDetail-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'TaskRejectDetail', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskRejectDetail/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="TaskRejectDetail-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskRejectDetail/allrecordsproduction/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="TaskRejectDetail-getallrecords-production", options={"expose"=true}, methods={"GET"})
     */
    public function getAllRecordsProductionAction(Request $request, $_locale, $pg, $lm)
    {
        //$cbg = $this->checkBeforeGet($request);
        //if ($cbg === true) {
        //$records = array();
        $_sql = "SELECT cpd.*,e.name empname
                    FROM client_production_details cpd
                    left join employees e on e.code=cpd.employee
                    WHERE cpd.type in ('e_p') and cpd.finish is not null and cpd.delete_user_id is null @@where@@
                    ORDER BY cpd.start asc";
        //$records['ClientProductionDetail'] = $this->getAllRecordsSQL($this, $request, $_sql, $pg, $lm);

        //return new JsonResponse($records);
        return new JsonResponse($this->getAllRecordsSQL($this, $request, $_sql, $pg, $lm));
        //} else {
        //    return $cbg;
        //}
    }
}
