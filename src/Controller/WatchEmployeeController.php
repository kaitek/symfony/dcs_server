<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WatchEmployeeController extends ControllerBase implements BaseAuditControllerInterface
{
    public const ENTITY = 'App:Employee';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/WatchEmployee/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="WatchEmployee-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        //operatörü boşaltma yapılacak
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
        if ($cbu===true) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $entity->setClient(null);
                $entity->setTasklist(null);
                $entity->setDay(null);
                $entity->setJobrotation(null);
                $entity->setStrlastseen(date("Y-m-d H:i:s", time()));
                $em->persist($entity);
                $em->flush();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/WatchEmployee", name="WatchEmployee-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $_rpp = ($this->_container==null ? $this->container : $this->_container)->getParameter('kaitek_framework.recordsperpage');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => $this->prepData(1, 10000),
                'extras' => array()
            );
            return $this->render('Modules/WatchEmployee.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/WatchEmployee/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="WatchEmployee-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->prepData($pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    private function prepData($pg, $lm)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="SELECT id,code,beginval,endval ,current_date,current_time
            ,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) \"day\" 
            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start 
            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end 
        from job_rotations 	
        where (finish is null or finish<now())	
            and (	
            (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
            or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
        )";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $current_job_rotation=$stmt->fetchAll();
        if (count($current_job_rotation)>0) {
            $cjr=$current_job_rotation[0];
            $sql="select d.*,d.worktime-d.work_lost timeprod,0 \"version\"
            from (
                SELECT e.id,e.code,e.name,e.client
                    ,CURRENT_TIMESTAMP(0)-:jrstart sure
                    ,(SELECT sum(worktime)worktime from (SELECT COALESCE(finish,CURRENT_TIMESTAMP(0))-start worktime,start,finish from client_production_details where type in ('e_p','e_p_confirm') and day=:day and jobrotation=:jobrotation and employee=e.code GROUP BY start,finish)a)worktime
                    ,COALESCE(l.work_lost,'00:00:00')work_lost,COALESCE(l.out_lost,'00:00:00')out_lost
                    ,case when e.client is null then 1 else 0 end isout
                    ,case when e.client is not null then case when exists(SELECT * from clients where code=e.client and workflow='El İşçiliği') then '#ffd687' else '' end else '#ffb6d8' end \"rowColor\"
                    ,case when e.client is null then (SELECT client from client_production_details where type in ('e_p','e_p_confirm') and day=:day and jobrotation=:jobrotation and employee=e.code order by start desc limit 1) else e.client end lastclient
                    ,(SELECT opname from client_production_details where type in ('e_p','e_p_confirm') and day=:day and jobrotation=:jobrotation and employee=e.code order by start desc limit 1) lastprod
                    ,(SELECT COALESCE(finish,start) from client_production_details where type in ('e_p','e_p_confirm') and day=:day and jobrotation=:jobrotation and employee=e.code order by start desc limit 1) lasttime
                FROM employees e 
                left join (select c.employee,sum(c.work_lost)work_lost,sum(c.out_lost)out_lost
                    from (
                        select b.employee,sum(case when b.is_work_lost=1 then b.sure else '00:00:00' end) work_lost,sum(case when b.is_work_lost=0 then b.sure else '00:00:00' end) out_lost
                        from (
                            SELECT COALESCE(cld.finish,CURRENT_TIMESTAMP(0))-cld.start sure,cld.employee
                                ,case when exists(select id from client_lost_details where type='l_e_t' and day=cld.day and jobrotation=cld.jobrotation and employee=cld.employee and start=cld.start) then 1 else 0 end is_work_lost
                            from client_lost_details cld where cld.type='l_e' and cld.day=:day and cld.jobrotation=:jobrotation
                        )b
                        group by b.employee,b.is_work_lost
                    )c
                    group by c.employee
                )l on l.employee=e.code
                where 1=1
                    and (e.client is not null or code in (
                        SELECT employee from client_lost_details where employee is not null and type in ('l_e','l_e_t') and day=:day and jobrotation=:jobrotation GROUP BY employee
                        union all
                        SELECT employee from client_production_details where employee is not null and type in ('e_p','e_p_confirm') and day=:day and jobrotation=:jobrotation GROUP BY employee
                        union all
        				SELECT employee from job_rotation_employees where day=:day and jobrotation=:jobrotation
                    )) 
                    and e.finish is null 
            )d
            ORDER BY case when d.worktime is not null then 0 else 1 end,case when d.client is null then 0 else 1 end desc,d.worktime-d.work_lost desc,d.name ASC";//--and e.isoperator=true
            $lsql=$sql." OFFSET :ofs LIMIT :lim";
            $offset = ($pg-1)*$lm;
            $stmt = $conn->prepare($lsql);
            $stmt->bindValue('jrstart', $cjr['jr_start']);
            $stmt->bindValue('day', $cjr['day']);
            $stmt->bindValue('jobrotation', $cjr['code']);
            $stmt->bindValue("ofs", $offset);
            $stmt->bindValue("lim", $lm);
            $stmt->execute();
            $records=$stmt->fetchAll();

            $stmt_c = $conn->prepare($sql);
            $stmt_c->bindValue('jrstart', $cjr['jr_start']);
            $stmt_c->bindValue('day', $cjr['day']);
            $stmt_c->bindValue('jobrotation', $cjr['code']);
            $stmt_c->execute();
            $records=$stmt_c->fetchAll();
            $count=count($records);
        } else {
            $records=array();
            $count=0;
        }

        return array('WatchEmployee'=>array('totalProperty'=>$count,"records"=>$records));
    }
}
