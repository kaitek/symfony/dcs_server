<?php
namespace App\Controller;

use App\Entity\ClientParam;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientParamController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:ClientParam';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }
    
    /**
     * @Route(path="/ClientParam/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientParam-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError("Kayıt Silinemez.");
    }

    public function getNewEntity()
    {
        return new ClientParam();
    }
    
    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('c.id,c.recordId,c.client,c.name,c.value'
                .',c.version')
                ->from('App:ClientParam', 'c')
                ->where('c.deleteuserId is null')
                ->orderBy('c.client', 'ASC');
        $queries['ClientParam'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }
    
    /**
     * @Route(path="/ClientParam/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientParam-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError("Kayıt Eklenemez.");
    }
    
    /**
     * @Route(path="/ClientParam/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientParam-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError("Kayıt Değiştirilemez.");
    }
    
    /**
     * @Route(path="/ClientParam", name="ClientParam-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/ClientParam.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientParam/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ClientParam-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ClientParam', $id);

            return $this->render('Modules/ClientParam.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientParam/{id}", requirements={"id": "\d+"}, name="ClientParam-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ClientParam', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
    
    /**
     * @Route(path="/ClientParam/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientParam-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}

