<?php

namespace App\Controller;

use App\Entity\TaskCaseControl;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TaskCaseControlController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:TaskCaseControl';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/TaskCaseControl/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskCaseControl-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
             ->getRepository(self::ENTITY)
             ->find($id);
        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @ Route(path="/TaskCaseControl/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskCaseControl-getComboValues", options={"expose"=true}, methods={"GET"})
     */
    //public function getComboValuesTaskCaseControl(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = ''){
    //    return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val," and finish is null ");
    //}

    public function getNewEntity()
    {
        return new TaskCaseControl();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('tcc')
                ->from('App:TaskCaseControl', 'tcc')
                ->where('tcc.deleteuserId is null')
                ->orderBy('tcc.opcode', 'ASC')
                ->addOrderBy('tcc.quantityremaining', 'ASC');
        $queries['TaskCaseControl'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @ Route(path="/TaskCaseControl/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskCaseControl-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @ Route(path="/TaskCaseControl/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskCaseControl-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/TaskCaseControl", name="TaskCaseControl-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            //$clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            //$data['data']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/TaskCaseControl.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @ Route(path="/TaskCaseControl/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="TaskCaseControl-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'TaskCaseControl', $id);

            return $this->render('Modules/TaskCaseControl.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @ Route(path="/TaskCaseControl/{id}", requirements={"id": "\d+"}, name="TaskCaseControl-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'TaskCaseControl', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskCaseControl/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="TaskCaseControl-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
