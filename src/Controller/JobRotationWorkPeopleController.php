<?php
/*
 * kayıt girişi toplu yapılacak
 * silme için gridde her kaydın yanındaki check seçilerek toplu silme yapılacak
 *  
 */
namespace App\Controller;

use App\Entity\JobRotationWorkPeople;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobRotationWorkPeopleController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:JobRotationWorkPeople';
    
    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/JobRotationWorkPeople/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="JobRotationWorkPeople-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new JobRotationWorkPeople();
    }
    
    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT 0 del,jrwp.id,jrwp.day,jrwp.jobrotation "
                . " ,jrwp.jobrotationteam,jrwp.employee "
                . " ,jrwp.beginval,jrwp.endval "
                . " ,e.code,e.name "
                . " ,jrwp.version "
                . " FROM job_rotation_work_peoples jrwp "
                . " LEFT JOIN employees e on e.code=jrwp.employee"
                . " WHERE 1=1 and e.finish is null @@where@@ "
                . " ORDER BY jrwp.day DESC";
        $queries['JobRotationWorkPeople'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('jrwp.id,jrwp.day,jrwp.jobrotation'
                . ',jrwp.jobrotationteam,jrwp.employee'
                . ',jrwp.beginval,jrwp.endval'
                . ',jrwp.version')
                ->from('App:JobRotationWorkPeople', 'jrwp')
                ->where('jrwp.deleteuserId is null')
                ->orderBy('jrwp.day', 'DESC');
        $queries['JobRotationWorkPeople'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }
    
    /**
     * @Route(path="/JobRotationWorkPeople/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="JobRotationWorkPeople-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba=$this->checkBeforeAdd($request);
        if($cba===true){
            $this->_requestData = json_decode($request->getContent());
            if($this->_requestData == null){
                $this->_requestData = $request->request->all();
            }
            $this->_requestData = $this->clearLookup($this->_requestData);
            $day=new \DateTime($this->_requestData->day);
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $sql_delete="delete from job_rotation_work_peoples where day=:day and jobrotation=:jobrotation";
                $stmt_delete = $conn->prepare($sql_delete);
                $stmt_delete->bindValue('day', $this->_requestData->day);
                $stmt_delete->bindValue('jobrotation', $this->_requestData->jobrotation);
                $stmt_delete->execute();
                foreach(explode(',',$this->_requestData->employees) as $employee){
                    $validator = ($this->_container==null?$this->container:$this->_container)->get('validator');
                    $entity = $this->getNewEntity();
                    $entity->setDay($day);
                    $entity->setJobrotation($this->_requestData->jobrotation);
                    $entity->setJobrotationteam($this->_requestData->jobrotationteam);
                    $entity->setEmployee($employee);
                    $entity->setBeginval($this->_requestData->beginval.":00");
                    $entity->setEndval($this->_requestData->endval.":59");
                    $entity->setCreateuserId($userId);
                    $entity->setUpdateuserId($userId);
                    $errors = $this->getValidateMessage($validator->validate($entity));
                    if ($errors!==false)
                        return $errors;
                    $em->persist($entity);
                    $em->flush();
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        }else{
            return $cba;
        }
        //return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/JobRotationWorkPeople/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="JobRotationWorkPeople-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/JobRotationWorkPeople", name="JobRotationWorkPeople-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $data['extras']['job_rotation_teams']=json_decode($job_rotation_teams->getContent())->records;
            $data['extras']['job_rotations']=json_decode($job_rotations->getContent())->records;
            return $this->render('Modules/JobRotationWorkPeople.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/JobRotationWorkPeople/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="JobRotationWorkPeople-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'JobRotationWorkPeople', $id);

            return $this->render('Modules/JobRotationWorkPeople.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/JobRotationWorkPeople/{id}", requirements={"id": "\d+"}, name="JobRotationWorkPeople-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'JobRotationWorkPeople', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/JobRotationWorkPeople/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="JobRotationWorkPeople-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}

