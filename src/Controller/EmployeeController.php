<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Entity\OperationAuthorization;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmployeeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:Employee';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/Employee/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Employee-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Employee/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="Employee-getComboValues", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesEmployee(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = ''){
        $_where=" and finish is null ";
        if($request->query->get('options')!==''){
            $options=json_decode($request->query->get('options'));
            if(isset($options->isquality)&&$options->isquality===true){
                $_where=" and isquality=true and finish is null ";
            }
        }
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val, $_where);
    }

    public function getNewEntity()
    {
        return new Employee();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('e.id,e.code,e.name,e.secret,e.isexpert,e.isoperator,e.ismaintenance,e.ismould,e.isquality,e.jobrotationteam,e.start,e.finish,e.version,e.teamleader')
                ->from(self::ENTITY, 'e')
                ->where(' e.deleteuserId is null')
                ->orderBy('e.name', 'ASC');
        $queries['Employee'] = array('qb' => $qb, 'getAll' => true);

        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('jrt.id, jrt.code')
        //    ->from('App:JobRotationTeam', 'jrt')
        //    ->where('jrt.finish is null')
        //    ->orderBy('jrt.code');
        //$queries['JobRotationTeam'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/Employee/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Employee-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        //$customer=($this->_container==null?$this->container:$this->_container)->getParameter('mh_customer');
        $this->_requestData = json_decode($request->getContent());
        if($this->_requestData == null){
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb_task = $em->createQueryBuilder();
        $qb_task = $qb_task->select('e.id')
            ->from('App:Employee', 'e')
            ->where('e.code=:code and e.finish is null')
            ->setParameters(array('code' => $this->_requestData->code));
        $tl=$qb_task->getQuery()->getArrayResult();
        if(count($tl)===0){
            //if($customer=='ERMETAL'){
            //    $ret=$this->recordAdd($request, $_locale, $pg, $lm);
            //    if($ret->getStatuscode()==200&&$this->_requestData->isoperator){
            //        $conn = $em->getConnection();
            //        $conn->beginTransaction();
            //        try {
            //            $user = $this->getUser();
            //            $userId = $user->getId();
            //            $sql="SELECT name from product_trees where finish is null and materialtype='O' order by name";
            //            $stmt = $conn->prepare($sql);
            //            $stmt->execute();
            //            foreach ($stmt->fetchAll() as $row){
            //                $entity_oa = new OperationAuthorization();
            //                $entity_oa->setCreateuserId($userId);
            //                $entity_oa->setUpdateuserId($userId);
            //                $entity_oa->setEmployee($this->_requestData->code);
            //                $entity_oa->setOpname($row["name"]);
            //                $entity_oa->setL1(1);
            //                $entity_oa->setL2(0);
            //                $entity_oa->setL3(0);
            //                $entity_oa->setL4(0);
            //                $entity_oa->setL5(0);
            //                $entity_oa->setTime(new \Datetime());
            //            
            //                $em->persist($entity_oa);
            //                $em->flush();
            //                $em->clear();
            //            }
            //            $conn->commit();
            //        } catch (\Exception $e) {
            //            // Rollback the failed transaction attempt
            //            $conn->rollback();
            //            //throw $e;
            //            return $this->msgError($e->getMessage());
            //        }
            //    }
            //    return $ret;
            //}else{
            //    return $this->recordAdd($request, $_locale, $pg, $lm);
            //}
            return $this->recordAdd($request, $_locale, $pg, $lm);
        }else{
            return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Employee.errRecordAdd', array(), 'Employee') );
        }
    }

    /**
     * @Route(path="/Employee/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Employee-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v){
        if(!isset($this->_requestData))
            $this->_requestData = json_decode($request->getContent());
        $customer=($this->_container==null?$this->container:$this->_container)->getParameter('mh_customer');
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);
        if($entity->getSecret()===null||($entity->getSecret()===''&&$customer!=='SAHINCE')){
            $v=substr(hash('sha256', $entity->getCode().time().rand(100,10000)),0,10);
            $this->_requestData->secret=$v;
        }
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Employee", name="Employee-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $team_leaders = $this->getComboValues($request, $_locale, 1, 100, 'team_leaders');
            $data['extras']['job_rotation_teams']=json_decode($job_rotation_teams->getContent())->records;
            $data['extras']['team_leaders']=json_decode($team_leaders->getContent())->records;
            return $this->render('Modules/Employee.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Employee/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Employee-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Employee', $id);

            return $this->render('Modules/Employee.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Employee/{id}", requirements={"id": "\d+"}, name="Employee-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Employee', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Employee/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Employee-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/m2m/Employee", name="Employee-update-all-records", options={"expose"=true}, methods={"PUT"})
     */
    public function putImportAction(Request $request, $_locale) {
        $id=0;
        $request->attributes->set('_isDCSService', true);
        //$content = json_decode($request->getContent());
        function random15() {
            $number = "";
            for($i=0; $i<15; $i++) {
              $min = ($i == 0) ? 1:0;
              $number .= mt_rand($min,9);
            }
            return $number;
        };
          
        /* @var $entity Employee */
        $employees = $this->getDoctrine()
                ->getRepository('App:Employee')
                //->findOneBy(array('code'=>'4962'));
                ->findAll();
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $conn->beginTransaction();
        try {
            foreach($employees as $employee)
            {
                $employee->setUpdateuserId(1);
                if($employee->getSecret()==null||$employee->getSecret()==''){
                    //$employee->setSecret(strtoupper(bin2hex(random_bytes(10))));//base64_encode(random_bytes(10))//14 chars
                    $employee->setSecret(random15());
                }else{
                    $employee->setSecret($employee->getSecret());//base64_encode(random_bytes(10))//14 chars
                }
                $em->flush();
            }
            $conn->commit();
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            $conn->rollback();
            //throw $e;
            return $this->msgError($e->getMessage());
        }
        
        return $this->msgSuccess();
    }

}
