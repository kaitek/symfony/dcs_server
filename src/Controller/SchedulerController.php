<?php

namespace App\Controller;

use App\Entity\TaskList;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SchedulerController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:TaskList';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/Scheduler/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Scheduler-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError(
            ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    public function getNewEntity()
    {
        return new TaskList();
    }

    public function getQBQuery()
    {
        //$queries = array();
        ///** @var EntityManager $em */
        //$em = $this->getDoctrine()->getManager();
        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('t')
        //    ->from(self::ENTITY, 't')
        //    ->where('t.finish is null and t.productdonecount<t.productcount')
        //    ->orderBy('t.client', 'ASC')
        //    ->addOrderBy('t.plannedstart', 'ASC')
        //    ->addOrderBy('t.opname', 'ASC');
        //$queries['Scheduler'] = array('qb' => $qb, 'getAll' => true);


        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('p.id, p.name, p.description')
        //    ->from('App:ProductTree', 'p')
        //    ->where('p.materialtype = :materialtype')
        //    ->setParameter('materialtype', 'O')
        //    ->orderBy('p.name');
        //$queries['ProductTree'] = array('qb' => $qb, 'getAll' => true);
        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('c.id, c.code')
        //    ->from('App:Client', 'c')
        //    ->orderBy('c.code');
        //$queries['Client'] = array('qb' => $qb, 'getAll' => true);

        //return $queries;
        return array();
    }

    public function getSqlStr(Request $request)
    {
        $uri = strtolower($request->getBaseUrl());
        $queries = array();
        if(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')) {
            $_sql = "SELECT c.code as id,c.code title,concat('[',']') children FROM clients c where c.workflow not in ('El İşçiliği','Gölge Cihaz') ORDER BY c.ganttorder";
        } else {
            //$_sql = "SELECT c.code as id,c.code title,concat('[',(SELECT string_agg(concat('{\"title\":\"',cd.code,'\",\"','id\":\"',cd.code,'\"}'),',' order by cd.code) from client_details cd where cd.iotype='I' and cd.ioevent='input' and cd.finish is null and cd.client=c.code GROUP BY cd.client ),']') children FROM clients c where c.workflow<>'El İşçiliği' ORDER BY c.code";
            $_sql = "SELECT c.code as id,c.code title,concat('[','{\"title\":\"',c.code,'\",\"','id\":\"',c.code,'\"}',']') children FROM clients c where c.workflow not in ('El İşçiliği','Gölge Cihaz') ORDER BY c.code";
        }
        $queries['Client'] = array('sql' => $_sql, 'getAll' => true);
        if (($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')) {
            $sql_moulds="SELECT md.mould,md.mouldgroup,md.opcode,md.opnumber,pt.name opname,md.leafmask,md.leafmaskcurrent
                ,mg.productionmultiplier,mg.intervalmultiplier,mg.setup,mg.cycletime,m.client1,m.client2,m.client3,m.client4,m.client5
            FROM mould_details md
            join mould_groups mg on mg.mould=md.mould and mg.code=md.mouldgroup 
            join moulds m on m.code=md.mould
            join product_trees pt on pt.code=md.opcode and md.opname=pt.name and (pt.finish is null or pt.finish>CURRENT_DATE) and pt.materialtype='O'
            where md.finish is null and mg.finish is null and m.finish is null
            ORDER BY md.mould,md.mouldgroup,md.opname";
        } else {
            $sql_moulds="SELECT * FROM (
                select '1'x,md.mould,md.mouldgroup,md.opcode,md.opnumber,md.opname,md.leafmask,md.leafmaskcurrent
                    ,mg.productionmultiplier,mg.setup,mg.cycletime,cd.client
                    ,round((coalesce(mg.productionmultiplier,100)*mg.cycletime)/100,2)tpp 
                from mould_details md
                join mould_groups mg on mg.mould=md.mould and mg.code=md.mouldgroup 
                join client_details cd on cd.code=md.mould and cd.iotype='I' and cd.finish is null
                where md.finish is null and mg.finish is null and mg.code not in (select cmd.mouldgroup from client_mould_details cmd)
                union all
                select '2'x,cmd.clientmouldgroup mould,cmd.clientmouldgroup mouldgroup,md.opcode,md.opnumber,md.opname,md.leafmask,md.leafmaskcurrent
                    ,cmg.productionmultiplier,cmg.setup,cmg.cycletime,cmd.client
                    ,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime)/100,2)tpp 
                from client_mould_details cmd  
                join mould_details md on md.mouldgroup=cmd.mouldgroup 
                join client_details cd on cd.iotype='I' and cd.finish is null and cd.code=md.mould and cd.client=cmd.client 
                join client_mould_groups cmg on cmg.client=cmd.client and cmg.code=cmd.clientmouldgroup
                where md.finish is null and cd.finish is null and cmd.finish is null )m1
            ORDER BY m1.mould,m1.mouldgroup,m1.opname";
        }
        $queries['Mould'] = array('sql' => $sql_moulds, 'getAll' => true);
        if(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')) {
            $sql_task="SELECT 
                ti.id,pt.id ptid,pt.description,ti.opname title
                ,COALESCE((SELECT m.description from mould_details md join moulds m on m.code=md.mould where m.finish is null and md.finish is null and md.opname=ti.opcode limit 1),ti.opcode)title2
                ,cast(round(ti.productcount*pt.tpp*1000) as integer) duration,ti.productcount _production
                ,cast(pt.tpp as integer)tpp
                /*,COALESCE((SELECT concat(m.client1
                    ,case when m.client2 is not null then concat(',',m.client2) else '' end
                    ,case when m.client3 is not null then concat(',',m.client3) else '' end
                    ,case when m.client4 is not null then concat(',',m.client4) else '' end
                    ,case when m.client5 is not null then concat(',',m.client5) else '' end)
                from mould_details md 
                join moulds m on m.code=md.mould
                where m.finish is null and md.finish is null and md.opname=ti.opcode limit 1),'') \"targetResources\"*/
                ,ti.erprefnumber,coalesce(tl.plannedstart,ti.deadline) \"start\",'' \"end\"
                ,case when tl.plannedstart is not null and tl.resourceid is not null then true else false end scheduled
                ,coalesce(tl.resourceid,tl.client) \"resourceId\",ti.id importid,COALESCE(tl.id,0) listid,c.code client
            FROM task_imports ti 
            join product_trees pt on pt.code=ti.opcode and pt.name=ti.opname and (pt.finish is null or pt.finish>CURRENT_DATE) and pt.materialtype='O' and COALESCE(pt.number,0)=COALESCE(ti.opnumber,0)
            left join task_lists tl on tl.erprefnumber=ti.erprefnumber and tl.opcode=ti.opcode and tl.opname=ti.opname
            left join clients c on c.code=tl.client and c.workflow not in ('El İşçiliği','Gölge Cihaz')
            where tl.finish is null and tl.deleted_at is null
                and (tl.plannedstart is null or (tl.plannedstart is not null and coalesce(tl.plannedstart,ti.deadline)>'2023-02-03'))
                and (coalesce(tl.resourceid,tl.client) is null or coalesce(tl.resourceid,tl.client) in (select code from clients))
                -- and c.workflow not in ('El İşçiliği','Gölge Cihaz')
            ORDER BY ti.client,pt.description,coalesce(tl.plannedstart,ti.deadline)";
        } else {
            //$sql_task="SELECT
            //    ti.id,concat(ti.opcode,'-',ti.opnumber) title,cast(round(ti.productcount*pt.tpp*1000) as integer) duration,ti.productcount _production
            //    ,cast(pt.tpp as integer)tpp
            //    ,COALESCE((SELECT string_agg(distinct mould,',' order by mould) mould from mould_details where finish is null and opname=concat(ti.opcode,'-',ti.opnumber) GROUP BY opname),ti.client) \"targetResources\"
            //    ,ti.erprefnumber,coalesce(tl.plannedstart,ti.deadline) \"start\"
            //    ,case when tl.plannedstart is not null then true else false end scheduled
            //    ,ti.id importid,COALESCE(tl.id,0) tlid
            //    ,c.code \"resourceId\"
            //    ,(select distinct(md0.mould) from mould_details md0 join client_details cd0 on cd0.code=md0.mould where md0.finish is null and md0.opname=concat(ti.opcode,'-',ti.opnumber) and cd0.client=tl.client and cd0.iotype='I') \"resourceId2\"
            //FROM task_imports ti
            //left join product_trees pt on pt.code=ti.opcode and pt.number=ti.opnumber and pt.finish is null and pt.materialtype='O'
            //left join task_lists tl on tl.client=ti.client and tl.erprefnumber=ti.erprefnumber and tl.opcode=ti.opcode and tl.opnumber=ti.opnumber
            //left join clients c on c.code=tl.client
            //where tl.finish is null and tl.deleted_at is null and c.id is not null
            //    and (tl.plannedstart is null or (tl.plannedstart is not null and coalesce(tl.plannedstart,ti.deadline)>'2019-02-10'))
            //    and ti.deadline between CURRENT_DATE-interval '1 days' and CURRENT_DATE+interval '6 days'
            //ORDER BY ti.client,coalesce(tl.plannedstart,ti.deadline)";
            $sql_task="SELECT 
                ti.id,concat(ti.opcode,'-',ti.opnumber) title,cast(round(ti.productcount*ti.tpp*1000) as integer) duration,ti.productcount _production
                ,cast(ti.tpp as integer)tpp,ti.opcode,ti.opnumber,concat(ti.opcode,'-',ti.opnumber) opname
                ,c.code \"targetResources\",coalesce(pt.description,'') opdescription
                ,ti.erprefnumber,coalesce(tl.plannedstart,ti.deadline) \"start\"
                ,case when tl.plannedstart is not null then true else false end scheduled
                ,ti.id importid,COALESCE(tl.id,0) tlid,c.code \"resourceId\",c.code client,'' \"end\"
            FROM task_imports ti 
            left join product_trees pt on pt.code=ti.opcode and pt.number=ti.opnumber and pt.finish is null and pt.materialtype='O'
            left join task_lists tl on tl.client=ti.client and tl.erprefnumber=ti.erprefnumber and tl.opcode=ti.opcode and tl.opnumber=ti.opnumber
            left join clients c on c.code=ti.client and c.workflow<>'Gölge Cihaz'
            where tl.finish is null and tl.deleted_at is null and c.id is not null
                and (tl.plannedstart is null or (tl.plannedstart is not null and coalesce(tl.plannedstart,ti.deadline)>'2019-02-10'))
                and ti.deadline between CURRENT_DATE-interval '1 days' and CURRENT_DATE+interval '6 days' 
            ORDER BY ti.client,coalesce(tl.plannedstart,ti.deadline)";
        }

        $queries['Scheduler'] = array('sql' => $sql_task, 'getAll' => true);

        $sql_ps="SELECT * FROM offtimes where delete_user_id is null and (finish is null or finish>CURRENT_TIMESTAMP(0))  ORDER BY id";
        $queries['plannedStops'] = array('sql' => $sql_ps, 'getAll' => true);

        $sql_ovt="SELECT * FROM overtimes where delete_user_id is null and (finish is null or finish>CURRENT_TIMESTAMP(0)) ORDER BY id";
        $queries['overtimes'] = array('sql' => $sql_ovt, 'getAll' => true);
        return $queries;
    }

    ///**
    // * @ Route(path="/Scheduler/getReport", name="Scheduler-getReport", options={"expose"=true}, methods={"POST"})
    // */
    //public function getReportAction(Request $request, $_locale){
    //    /** @var EntityManager $em */
    //    $em = $this->getDoctrine()->getManager();
    //    $conn = $em->getConnection();
    //    $sql='SELECT client "İstasyon",erprefnumber "ERP Ref",opname "Operasyon",productcount "Miktar",plannedstart "Pl. Başlangıç",tpp "Birim Süre"
    //    from task_lists
    //    where finish is null and taskfromerp=true and plannedstart is not null and productdonecount<productcount
    //    order by client,plannedstart';
    //    $stmt = $conn->prepare($sql);
    //    $stmt->execute();
    //    $records = $stmt->fetchAll();
    //    if(count($records) == 0){
    //        return $this->msgError( "Belirtilen kriterlere uygun kayıt bulunamadı." );
    //    }
    //    $name="gorev_listesi_".date("Y-m-d_H:i:s");
    //    $objPHPExcel = new Spreadsheet();
    //    $objPHPExcel->getProperties()
    //        ->setCreator("Kaitek - Reports")
    //        ->setLastModifiedBy("Kaitek - Reports")
    //        ->setTitle("Kaitek - Reports")
    //        ->setSubject("Kaitek - Reports")
    //        ->setDescription($name)
    //        ->setKeywords('Verimot')
    //        ->setCategory('-');
    //    $objPHPExcel->setActiveSheetIndex(0);
    //    $col = 1;
    //    $row = 2;
    //    $sheet = $objPHPExcel->getActiveSheet();
    //    $_strcol = Coordinate::stringFromColumnIndex($col++);
    //    $sheet->setCellValue($_strcol.$row, 'Sıra');
    //    $sheet->getColumnDimension($_strcol)->setAutoSize(true);
    //    $sheet->getStyle($_strcol.$row)->getFont()->setBold(true);
    //    $first = true;
    //    $_lc = 0;
    //    foreach($records as $item=>$kayit){
    //        if($first){
    //            foreach($kayit as $key=>$val){
    //                $_strcol = Coordinate::stringFromColumnIndex($col++);
    //                $sheet->setCellValue($_strcol.$row, strip_tags($key)/*strtoupper($this->replace_tr(strip_tags($key)))*/);
    //                $sheet->getColumnDimension($_strcol)->setAutoSize(true);
    //                $sheet->getStyle($_strcol.$row)->getFont()->setBold(true);
    //            }
    //            $row++;
    //            $first = false;
    //            $_lc = $col;
    //            /*
    //            Başlık Alanı
    //            */
    //            $sheet->mergeCells("A1:".$_strcol."1");
    //            $sheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
    //            $sheet->setCellValue("A1", $title);
    //            $sheet->getStyle("A1")->getFont()->setBold(true);
    //        }
    //        $col = 2;
    //        $sheet->setCellValue("A".$row, $row-2);
    //        foreach($kayit as $title=>$val){
    //            $_strcol = Coordinate::stringFromColumnIndex($col++);
    //            $pos = strpos($val,"|");
    //            if($pos == 1){
    //                $val = substr($val, 2);
    //                $sheet->setCellValueExplicit($_strcol.$row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/,DataType::TYPE_STRING);
    //            }else{
    //                $sheet->setCellValue($_strcol.$row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/);
    //            }
    //        }
    //        $row++;
    //    }
    //    $col = 1;
    //    foreach($kayit as $title=>$val){
    //        $_strcol = Coordinate::stringFromColumnIndex($col++);
    //        $sheet->getColumnDimension($_strcol)->setAutoSize(true);
    //    }
    //    //$sheet->setTitle('Sayfa-1');
    //    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    //    $objPHPExcel->setActiveSheetIndex(0);
    //    // create the writer
    //    $objWriter = new Xlsx($objPHPExcel);
    //    //$objWriter->setOffice2003Compatibility(true);
    //    //$objWriter->setIncludeCharts(TRUE);
    //    $response = new StreamedResponse(
    //        function () use ($objWriter) {
    //            $objWriter->save('php://output');
    //        },
    //        200,
    //        array()
    //    );
    //    // adding headers
    //    $dispositionHeader = $response->headers->makeDisposition(
    //        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
    //        $name.'.xlsx'
    //    );
    //    $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
    //    $response->headers->set('Pragma', 'public');
    //    $response->headers->set('Cache-Control', 'maxage=1');
    //    $response->headers->set('Content-Disposition', $dispositionHeader);
    //    return $response;
    //
    //}

    /**
     * @Route(path="/Scheduler/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Scheduler-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba=$this->checkBeforeAdd($request);
        if($cba===true) {
            if(!isset($this->_requestData)) {
                $content = $request->getContent();
                $this->_requestData = json_decode($request->getContent());
                if($this->_requestData == null) {
                    $this->_requestData = $request->request->all();
                }
            }
            $uri = strtolower($request->getBaseUrl());
            $user = $this->getUser();
            $userId = $user->getId();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $dirty=false;
                $parentClient='';
                $parentPlannedStart='';
                $parentUUID=$this->gen_uuid();
                $found='';
                foreach($this->_requestData->data as $row) {
                    $row->type='quantity';
                    $arr_find=array('erprefnumber' => $row->erprefnumber, 'opname'=>$row->title);
                    if(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')) {
                        $arr=explode('-', $row->title);
                        $opnumber=$arr[(count($arr)-1)];
                        $opcode=implode('-', array_splice($arr, 0, count($arr)-1));
                        $row->client=$row->resourceId;
                        $row->opnumber=$opnumber;
                        $row->opcode=$opcode;
                        $row->opname=$row->title;
                        $row->opdescription=$row->description;
                        // $arr_find=array('erprefnumber' => $row->erprefnumber, 'opname'=>$row->opname);
                    }
                    $add=false;
                    $found='';
                    $records =$this->getDoctrine()
                        ->getRepository(self::ENTITY)
                        ->findBy($arr_find);
                    if(count($records)>0) {//kayıt var, başlamamış ise bilgileri değiştirilecek
                        $found='var';
                        if($records[0]->getStart()==null) {
                            $found.=' baslamamis';
                            $entity=$records[0];
                            if($row->scheduled==false||$entity->getClient()!==$row->client) {
                                $found.=' client farkli';
                                if($entity->getClient()!==$row->client) {
                                    $add=true;
                                }
                                $entity->setDeleteuserId($userId);
                                $em->persist($entity);
                                $em->flush();
                                $em->remove($entity);
                                $em->flush();
                            } elseif ($entity->getProductcount()!==$row->_production
                                ||$entity->getStart()!==$row->start
                            ) {
                                $entity->setProductcount($row->_production);
                                $entity->setPlannedstart(new \DateTime($row->start));
                                $em->persist($entity);
                                //aynı zamanda başlamak için planlı başka işler var mı?
                                if(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')) {
                                    $arr_find_other=array('client'=>$row->client, 'plannedstart'=>$entity->getPlannedstart());
                                    $records_other =$this->getDoctrine()
                                        ->getRepository(self::ENTITY)
                                        ->findBy($arr_find_other);
                                    if(count($records_other)>0) {
                                        foreach($records_other as $ro) {
                                            if($ro->getErprefnumber()!==$row->erprefnumber) {
                                                $ro->setPlannedstart(new \DateTime($row->start));
                                                $em->persist($ro);
                                                $em->flush();
                                            }
                                        }
                                    }
                                }
                                $dirty=true;
                            }
                        }
                    } else {//kayıt yok, yeni görev kaydı oluştur
                        $add=true;
                    }
                    if($add) {
                        if($parentClient!==$row->client||$parentPlannedStart!==$row->start) {
                            if($parentClient!=='') {
                                $parentUUID=$this->gen_uuid();
                            }
                            $parentClient=$row->client;
                            $parentPlannedStart=$row->start;
                        }
                        $entity_new=$this->getNewEntity();
                        $entity_new->setCreateuserId($userId);
                        $entity_new->setCode($row->erprefnumber.$row->opnumber);
                        $entity_new->setErprefnumber($row->erprefnumber);
                        $entity_new->setOpcode($row->opcode);
                        $entity_new->setOpnumber($row->opnumber);
                        $entity_new->setOpname($row->opname);
                        $entity_new->setOpdescription($row->opdescription);
                        $entity_new->setProductcount($row->_production);
                        $entity_new->setDeadline(new \DateTime($row->start));
                        $entity_new->setPlannedstart(new \DateTime($row->start));
                        $entity_new->setPlannedfinish(new \DateTime($row->finish));
                        $entity_new->setClient($row->client);
                        $entity_new->setResourceid($row->resourceId);
                        $entity_new->setType($row->type);
                        $entity_new->setTpp($row->tpp);
                        $entity_new->setIdx($parentUUID);
                        //$entity_new->setStart($entity->getStart());
                        $entity_new->setTaskfromerp(true);
                        //$entity_new->setProductdonecount($entity->getProductdonecount());
                        //$entity_new->setScrappart($entity->getScrappart());
                        $em->persist($entity_new);
                        $dirty=true;
                    }
                    if($dirty) {
                        $em->flush();
                        $em->clear();
                    } else {
                        $em->clear();
                    }
                }
                $conn->commit();
                //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                //    return $this->showAllAction($request, $_locale, $page, $limit);
                //} else {
                //    return $this->msgSuccess();
                //}
                return $this->msgSuccess($found);
            } catch (\Error $err) {
                $conn->rollback();
                $mtitle='Hata';
                $resp = array(
                    'title' => $mtitle,
                    'success' => false,
                    'message' => $err->getMessage()." \n",
                    'file' => $err->getfile()." Line:".$err->getLine()
                );
                $statusCode=400;
                return new JsonResponse($resp, $statusCode);
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                $mtitle='Hata';
                $resp = array(
                    'title' => $mtitle,
                    'success' => false,
                    'message' => $e->getMessage()." \n",
                    'file' => $e->getfile()." Line:".$e->getLine()
                );
                $statusCode=400;
                return new JsonResponse($resp, $statusCode);
            }
        } else {
            return $cba;
        }
    }

    /**
     * @Route(path="/Scheduler/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Scheduler-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        //$entity = $this->getDoctrine()
        //    ->getRepository(self::ENTITY)
        //    ->find($id);
        //return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
        return $this->msgError(
            ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    /**
     * @Route(path="/Scheduler", name="Scheduler-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY, 9999);

            return $this->render('Modules/Scheduler.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Scheduler/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Scheduler-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Scheduler', $id);

            return $this->render('Modules/Scheduler.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Scheduler/{id}", requirements={"id": "\d+"}, name="Scheduler-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Scheduler', $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Scheduler/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Scheduler-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, 9999);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Scheduler/getTasks/{period}/{dayscount}", name="Scheduler-getTasks", options={"expose"=true}, methods={"POST"})
     */
    public function getTasksAction(Request $request, $_locale, $period, $dayscount)
    {
        //$period=10;//dakika cinsinden parça büyüklüğü
        //$dayscount=3;//gün sayısı
        $statusCode=200;
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $sql="SELECT cd.client ,string_agg(distinct cd.code, '|' order by cd.code) codes,count(cd.code)ekipmansayisi 
        from client_details cd 
        join clients c on c.code=cd.client
        where cd.iotype='I' and cd.ioevent='input' and cd.finish is null and c.workflow not in ('El İşçiliği','Kataforez','Gölge Cihaz') 
            --and cd.client like '03.08.0024%' 
        GROUP BY cd.client
        order by cd.client";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $client_details=$stmt->fetchAll();
        $result["client_details"]=$client_details;

        $sql="select d.client,d.mould,d.mouldgroup,d.opname
            ,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime)/100,2)tpp 
            ,d.opcount
        from ( 
            SELECT cmd.client,cmd.clientmouldgroup 
                ,string_agg(distinct md.mould,'|' ORDER BY md.mould)mould 
                ,string_agg(distinct md.mouldgroup,'|' ORDER BY md.mouldgroup)mouldgroup 
                ,string_agg(md.opname,'|' order by md.opname)opname
                ,count(md.opname)opcount
            from client_mould_details cmd  
            join mould_details md on md.mouldgroup=cmd.mouldgroup 
            join client_details cd on cd.iotype='I' and cd.code=md.mould 
            where md.finish is null and cd.finish is null and cmd.finish is null
            GROUP BY cmd.client,cmd.clientmouldgroup
        )d 
        join client_mould_groups cmg on cmg.client=d.client and cmg.code=d.clientmouldgroup 
        where cmg.finish is null
        order by d.client asc,d.opcount desc,d.mould asc,d.mouldgroup asc,d.opname asc";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $client_mould_details=array();
        foreach($stmt->fetchAll() as $row) {
            if(!isset($client_mould_details[$row['client']])) {
                $client_mould_details[$row['client']]=array();
            }
            $client_mould_details[$row['client']][]=$row;
        }
        $result["client_mould_details"]=$client_mould_details;

        $sql="SELECT md.mould,md.mouldgroup,md.opcode,md.opnumber,md.opname,md.leafmask,md.leafmaskcurrent
            ,mg.productionmultiplier,mg.intervalmultiplier,mg.setup,mg.cycletime
        FROM mould_details md
        join mould_groups mg on mg.mould=md.mould and mg.code=md.mouldgroup 
        where md.finish is null and mg.finish is null
        ORDER BY md.mould,md.mouldgroup,md.opname";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $mould_details=array();
        foreach($stmt->fetchAll() as $row) {
            if(!isset($mould_details[$row['mould']])) {
                $mould_details[$row['mould']]=array();
            }
            $mould_details[$row['mould']][]=$row;
        }
        $result["mould_details"]=$mould_details;

        $sql="SELECT id,clients,losttype,name,repeattype,days,startday,starttime,finishday,finishtime,clearonovertime
        from offtimes 
        where (finish is null or finish>CURRENT_TIMESTAMP(0)) ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $offtimes=$stmt->fetchAll();
        $result["offtimes"]=$offtimes;

        $sql="SELECT id,clients,name,repeattype,days,startday,starttime,finishday,finishtime 
        from overtimes 
        where (finish is null or finish>CURRENT_TIMESTAMP(0)) ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $overtimes=$stmt->fetchAll();
        $result["overtimes"]=$overtimes;

        $sql="select * 
        from (	
            SELECT concat(ti.erprefnumber,ti.opnumber) opid,concat(ti.opcode,'-',ti.opnumber) opname
                ,coalesce((SELECT concat(ti.erprefnumber,pt_pre.number)
                    from product_trees pt_pre
                    join task_imports ti_pre on concat(ti_pre.opcode,'-',ti_pre.opnumber)=pt_pre.name and ti_pre.erprefnumber=tl.erprefnumber
                    where pt_pre.finish is null and pt_pre.materialtype='O' and pt_pre.code=pt.code and pt_pre.oporder<pt.oporder 
                    order by pt_pre.oporder desc limit 1) ,'0') onkosul
                ,ti.erprefnumber,cast(ti.deadline-interval '2 day' as varchar(10)) baslamatarihi,ti.deadline termintarihi
                ,pt.tpp,ti.productcount,cast(ceil((pt.tpp*ti.productcount)/60) as integer)sure,ti.client
                ,case when cpd.client is not null or ti.erprefnumber in ('72825604','72737597','72826723') then 'Devam Eden' 
                        when tl.finish is not null then 'Tamamlanan' else '0' end sabitlenmis
                ,coalesce((select string_agg(aaa.mould,'|')mould 
                    from 
                        (SELECT md.mould
                        from client_details cd
                        join mould_details md on md.mould=cd.code
                        where cd.client=ti.client and cd.iotype='I' and md.opname=concat(ti.opcode,'-',ti.opnumber) and cd.finish is null and md.finish is null
                        group by md.mould
                        )
                    aaa),ti.client) clientdetail
            from task_imports ti 
            join product_trees pt on pt.code=ti.opcode and pt.number=ti.opnumber and pt.materialtype in ('O') and pt.finish is null
            left join task_lists tl on ti.erprefnumber=tl.erprefnumber and ti.opcode=tl.opcode and ti.opnumber=tl.opnumber
            left join (SELECT client,erprefnumber from client_production_details where type in ('c_p','c_p_confirm') and finish is null group by client,erprefnumber)cpd on cpd.client=ti.client and cpd.erprefnumber=ti.erprefnumber
            where ti.deadline>current_date- interval '5 day'
        )tlx
        --where tlx.client like '03.08.0024%' 
        order by case when tlx.sabitlenmis='Devam Eden' then 1 else 2 end,tlx.baslamatarihi,tlx.erprefnumber,tlx.onkosul ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $tasks=$stmt->fetchAll();
        $result["tasks"]=$tasks;

        $result["period"]=$period;
        $result["dayscount"]=$dayscount;
        $sql_start="SELECT 
            concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
        from job_rotations 
        where (finish is null or finish<now())
        order by beginval desc limit 1";
        $stmt_start = $conn->prepare($sql_start);
        $stmt_start->bindValue('t1', $tasks[0]["baslamatarihi"]);
        $stmt_start->execute();
        $records_start = $stmt_start->fetchAll();
        $result["firsttime"]=$records_start[0]["jr_start"];
        return new JsonResponse($result, $statusCode);
    }

    /**
     * @Route(path="/Scheduler/getTasksServer/{period}/{dayscount}/{opcount}", name="Scheduler-getTasksServer", options={"expose"=true}, methods={"POST"})
     */
    public function getTasksServerAction(Request $request, $_locale, $period, $dayscount, $opcount)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('AJAX request expected.');
        }

        $time_start = microtime();
        $records=array();
        function microtime_diff($start, $end = null)
        {
            if (!$end) {
                $end = microtime();
            }
            list($start_usec, $start_sec) = explode(" ", $start);
            list($end_usec, $end_sec) = explode(" ", $end);
            $diff_sec = intval($end_sec) - intval($start_sec);
            $diff_usec = floatval($end_usec) - floatval($start_usec);
            return number_format((float)floatval($diff_sec) + $diff_usec, 2, ',', '');
        }
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $arr_data=array();
        $sql="SELECT cd.client ,string_agg(distinct cd.code, '|' order by cd.code) codes,count(distinct cd.code)ekipmansayisi 
        from client_details cd 
        join clients c on c.code=cd.client
        where cd.iotype='I' and cd.ioevent='input' and cd.finish is null and c.workflow not in ('El İşçiliği','Kataforez','Gölge Cihaz')
            --and cd.client like '03.06.00%' 
        GROUP BY cd.client
        order by cd.client";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $client_details=$stmt->fetchAll();
        $records["client_details"]=$client_details;
        $sql="select d.client,d.mould,d.mouldgroup,d.opname
            ,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime)/100,2)tpp 
            ,d.opcount
        from ( 
            SELECT cmd.client,cmd.clientmouldgroup 
                ,string_agg(distinct md.mould,'|' ORDER BY md.mould)mould 
                ,string_agg(distinct md.mouldgroup,'|' ORDER BY md.mouldgroup)mouldgroup 
                ,string_agg(md.opname,'|' order by md.opname)opname
                ,count(md.opname)opcount
            from client_mould_details cmd  
            join mould_details md on md.mouldgroup=cmd.mouldgroup 
            join client_details cd on cd.iotype='I' and cd.code=md.mould 
            where md.finish is null and cd.finish is null and cmd.finish is null
            GROUP BY cmd.client,cmd.clientmouldgroup
        )d 
        join client_mould_groups cmg on cmg.client=d.client and cmg.code=d.clientmouldgroup 
        where cmg.finish is null
        order by d.client asc,d.opcount desc,d.mould asc,d.mouldgroup asc,d.opname asc";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $client_mould_details=array();
        foreach($stmt->fetchAll() as $row) {
            if(!isset($client_mould_details[$row['client']])) {
                $client_mould_details[$row['client']]=array();
            }
            $client_mould_details[$row['client']][]=$row;
        }

        //$sql="SELECT md.mould,md.mouldgroup,md.opcode,md.opnumber,md.opname,md.leafmask,md.leafmaskcurrent
        //    ,mg.productionmultiplier,mg.intervalmultiplier,mg.setup,mg.cycletime
        //FROM mould_details md
        //join mould_groups mg on mg.mould=md.mould and mg.code=md.mouldgroup
        //where md.finish is null and mg.finish is null
        //ORDER BY md.mould,md.mouldgroup,md.opname";
        //$stmt = $conn->prepare($sql);
        //$stmt->execute();
        //$mould_details=array();
        //foreach($stmt->fetchAll() as $row){
        //    if(!isset($mould_details[$row['mould']])){
        //        $mould_details[$row['mould']]=array();
        //    }
        //    $mould_details[$row['mould']][]=$row;
        //}

        $sql="select *
            ,coalesce((select string_agg(aaa.mould,'|')mould 
                from 
                (SELECT md.mould
                    from client_details cd
                    join mould_details md on md.mould=cd.code
                    where cd.client=tlx.client and cd.iotype='I' and md.opname=tlx.opname and cd.finish is null and md.finish is null
                    group by md.mould
                )
                aaa),tlx.client) clientdetail
        from (	
            SELECT ti.id,concat(ti.erprefnumber,ti.opnumber) opid,concat(ti.opcode,'-',ti.opnumber) opname
                ,coalesce((SELECT concat(ti.erprefnumber,pt_pre.number)
                    from product_trees pt_pre
                    join task_imports ti_pre on concat(ti_pre.opcode,'-',ti_pre.opnumber)=pt_pre.name and ti_pre.erprefnumber=ti.erprefnumber
                    where pt_pre.finish is null and pt_pre.materialtype='O' and pt_pre.code=pt.code and COALESCE(pt_pre.oporder,pt_pre.number)<COALESCE(pt.oporder,pt.number) 
                    order by COALESCE(pt_pre.oporder,pt_pre.number) desc limit 1) ,'0') onkosul
                ,ti.erprefnumber,cast(ti.deadline-interval '2 day' as varchar(10)) baslamatarihi,ti.deadline termintarihi
                ,pt.tpp,ti.productcount,cast(ceil((pt.tpp*ti.productcount)/60) as integer)sure,ti.client clients
                ,case when POSITION('|' in ti.client)>0 then true else false end is_alternate_client_available
                ,case when cpd.client is not null then 'Devam Eden' 
                    when tl.finish is not null then 'Tamamlanan' else '0' end sabitlenmis
                ,COALESCE(pt.oporder,pt.number) pt_order
                ,unnest(string_to_array(ti.client, '|')) as client
            from task_imports ti 
            join product_trees pt on pt.code=ti.opcode and pt.number=ti.opnumber and pt.materialtype in ('O') and pt.finish is null
            left join task_lists tl on ti.erprefnumber=tl.erprefnumber and ti.opcode=tl.opcode and ti.opnumber=tl.opnumber
            left join (SELECT client,erprefnumber from client_production_details where type in ('c_p','c_p_confirm') and finish is null group by client,erprefnumber)cpd on cpd.client=ti.client and cpd.erprefnumber=ti.erprefnumber
            where ti.deadline>current_date- interval '5 day' 
                --and ti.erprefnumber in ('72745577','72817558','72545949')
        )tlx
        -- where tlx.client like '03.06.00%' 
        order by case when tlx.sabitlenmis='Devam Eden' then 1 else 2 end,tlx.baslamatarihi,tlx.erprefnumber,tlx.onkosul,tlx.pt_order,tlx.id 
        ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $tasks=$stmt->fetchAll();
        if(count($tasks)>0) {
            $sql_start="SELECT 
                concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
            from job_rotations 
            where (finish is null or finish<now())
            order by beginval desc limit 1";
            $stmt_start = $conn->prepare($sql_start);
            $stmt_start->bindValue('t1', $tasks[0]["baslamatarihi"]);
            $stmt_start->execute();
            $records_start = $stmt_start->fetchAll();
            $firsttime=$records_start[0]["jr_start"];

            $sql="SELECT id,clients,losttype,name,repeattype,days,startday,starttime,finishday,finishtime,clearonovertime
                ,REPLACE(starttime, ':', '')starttime2,REPLACE(finishtime, ':', '')finishtime2,start,finish
            from offtimes 
            where (finish is null or finish>:finish) ";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('finish', $firsttime);
            $stmt->execute();
            $offtimes=$stmt->fetchAll();

            $sql="SELECT id,clients,name,repeattype,days,startday,starttime,finishday,finishtime
                ,REPLACE(starttime, ':', '')starttime2,REPLACE(finishtime, ':', '')finishtime2,start,finish 
            from overtimes 
            where (finish is null or finish>:finish) ";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('finish', $firsttime);
            $stmt->execute();
            $overtimes=$stmt->fetchAll();
        }

        $arr_data["_timeline"]=array("days"=>array(),"hours"=>array(),"opcount"=>array());
        foreach($client_details as $row_cd) {
            $str_client=implode("", explode(".", $row_cd["client"]));
            if(!isset($arr_data[$str_client])) {
                $arr_data[$str_client]=array();
            }
            $arr_data[$str_client]["title"]=$row_cd["client"];
            $arr_data[$str_client]["_timeline"]=array();
            $arr_data[$str_client]["equipments"]=array();
            $arr_codes=explode("|", $row_cd["codes"]);
            foreach($arr_codes as $row_cd_codes) {
                $str_code=implode("", explode(".", $row_cd_codes));
                $arr_data[$str_client]["equipments"][$str_code]=array("title"=>$row_cd_codes,"_timeline"=>array());
            }
        }
        if (count($tasks)>0) {
            $x=new \DateTime($firsttime);
            for ($i=0; $i <($dayscount*24*60/$period) ; $i++) {
                $str_index=$x->format('YmdHi');
                $str_day=$x->format('Ymd');
                $str_readable_day=$x->format('l');
                $str_day_short=$x->format('d-m-y');
                $str_time5=$x->format('H:i');
                $str_time4=$x->format('Hi');
                if (!isset($arr_data["_timeline"]["days"][$str_day])) {
                    $arr_data["_timeline"]["days"][$str_day]=array("id"=>$str_day,"title"=>$str_day_short,"cs"=>1 );
                } else {
                    $arr_data["_timeline"]["days"][$str_day]["cs"]=intval($arr_data["_timeline"]["days"][$str_day]["cs"])+1;
                }
                $arr_data["_timeline"]["hours"][$str_index]=array("id"=>$str_index,"title"=>$str_time5);
                $arr_data["_timeline"]["opcount"][$str_index]=array("id"=>$str_index,"title"=>0);

                $last_client=false;
                foreach ($client_details as $row_cd) {
                    $str_client=implode("", explode(".", $row_cd["client"]));
                    $arr_codes=explode("|", $row_cd["codes"]);
                    $_k=0;
                    $_cls="free";
                    //if($str_index=='201907160500'){
                    //    $_cls="work";
                    //}
                    $_title='';
                    if ($last_client!==$str_client) {
                        foreach ($offtimes as &$row_offtime) {
                            if (strpos($row_offtime["clients"], $row_cd["client"])!==false
                                &&(
                                    ($row_offtime["repeattype"]!=='day' && implode("", explode("-", $row_offtime["startday"]))===$str_day)
                                    || ($row_offtime["repeattype"]==='day' && strpos($row_offtime["days"], $str_readable_day)!==false)
                                )
                            ) {
                                $vi_start=intval($row_offtime["starttime2"]);
                                $vi_finish=intval($row_offtime["finishtime2"]);
                                if ($row_offtime["clearonovertime"]==true
                                    &&(count($overtimes)>0)
                                    &&($row_offtime["finish"]==null||($row_offtime["finish"]!=null&&$row_offtime["finish"]>$firsttime))
                                ) {
                                    for ($j=0;$j<count($overtimes);$j++) {
                                        $item_over=$overtimes[$j];
                                        if (strpos($item_over["clients"], $row_cd["client"])!==false&&implode("", explode("-", $item_over["startday"]))===$str_day) {
                                            $vi_start_over=intval($item_over["starttime2"]);
                                            $vi_finish_over=intval($item_over["finishtime2"]);
                                            if ($vi_start>=$vi_start_over) {
                                                //offtime öncesinden başlayan bir fazla mesai
                                                if ($vi_finish>$vi_finish_over) {
                                                    //offtime başlangıcını ötele
                                                    $row_offtime["starttime"]=$item_over["finishtime"];
                                                    $row_offtime["starttime2"]=$item_over["finishtime2"];
                                                } elseif ($vi_finish<=$vi_finish_over) {
                                                    //offtime kaydını sil
                                                    $row_offtime["modified"]=false;
                                                }
                                            } else {
                                                if ($vi_finish>$vi_finish_over) {
                                                    //parçala
                                                    $_x=$row_offtime;
                                                    $row_offtime["finishtime"]=$item_over["starttime"];
                                                    $row_offtime["finishtime2"]=$item_over["starttime2"];
                                                    $_x["starttime"]=$item_over["finishtime"];
                                                    $_x["starttime2"]=$item_over["finishtime2"];
                                                    $offtimes[]=$_x;
                                                } else {
                                                    //offtime bitişini geri çek
                                                    $row_offtime["finishtime"]=$item_over["starttime"];
                                                    $row_offtime["finishtime2"]=$item_over["starttime2"];
                                                }
                                            }
                                            if (intval($str_time4)>=$vi_start && intval($str_time4)<$vi_finish) {
                                                $_cls='offtime';
                                                $_title=$row_offtime["losttype"].' '.$row_offtime["starttime"].'-'.$row_offtime["finishtime"];
                                            }
                                        }
                                    }
                                } else {
                                    if (intval($str_time4)>=$vi_start && intval($str_time4)<$vi_finish) {
                                        $_cls='offtime';
                                        $_title=$row_offtime["losttype"].' '.$row_offtime["starttime"].'-'.$row_offtime["finishtime"];
                                    }
                                }
                            }
                        }
                    }
                    foreach ($arr_codes as $row_cd_codes) {
                        $str_code=implode("", explode(".", $row_cd_codes));
                        $arr_data[$str_client]["equipments"][$str_code]["_timeline"][]=array("id"=>$str_index,"cls"=>" ".$str_client.'_'.$str_index.' '.$_cls,"title"=>$_title );
                        if ($_k==0) {
                            $arr_data[$str_client]["_timeline"][]=array("id"=>$str_index,"cls"=>" ".$str_client.'_'.$str_index.' '.$_cls,"title"=>$_title );
                        }
                        $_k++;
                    }
                    $last_client=$str_client;
                }

                $x->setTimestamp($x->getTimestamp()+((($period/60)*3600)));
            }
            $records["firsttime"]=$firsttime;
            $records["offtimes"]=$offtimes;
        }
        $records["dayscount"]=$dayscount;
        $records["period"]=$period;
        function task_preprocess(&$tasks, $client_mould_details)
        {
            $i=0;
            foreach($tasks as &$task) {
                if(!isset($task["groupedtask"])) {
                    $arr_task_group=array();
                    if(isset($client_mould_details[$task["client"]])) {
                        $arr_task_group[]=$i;
                        $_cmd_=$client_mould_details[$task["client"]];
                        for($j=0;$j<count($_cmd_);$j++) {
                            $_row_cmd=$_cmd_[$j];
                            $_row_arr_opname=explode("|", $_row_cmd["opname"]);
                            $_flag_opname=true;
                            for($k=0;$k<count($_row_arr_opname);$k++) {
                                $_item_opname=$_row_arr_opname[$k];
                                if($_item_opname==$task["opname"]) {
                                    continue;
                                }
                                $_task_found=false;
                                for($l=0;$l<count($tasks);$l++) {
                                    $_item_task=$tasks[$l];
                                    if(isset($_item_task["groupedtask"])
                                        ||$_task_found!==false
                                        ||$_item_task["opname"]!==$_item_opname
                                        ||$_item_task["client"]!==$task["client"]
                                        ||$_item_task["termintarihi"]!==$task["termintarihi"]
                                        ||$_item_task["productcount"]!==$task["productcount"]
                                    ) {
                                        continue;
                                    } else {
                                        $arr_task_group[]=$l;
                                        $_task_found=true;
                                        break;
                                    }
                                }
                                if(!$_task_found) {
                                    $_flag_opname=false;
                                }
                            }
                            if($_flag_opname) {
                                for($m=0;$m<count($arr_task_group);$m++) {
                                    $idx=$arr_task_group[$m];
                                    $tasks[$idx]["groupedtask"]=$arr_task_group[0];
                                    $tasks[$idx]["tpp"]=$_row_cmd["tpp"];
                                    $tasks[$idx]["sure"]=ceil((intval($_row_cmd["tpp"])*$task["productcount"])/60);
                                }
                                break;
                            }
                        }
                    }
                }
                $i++;
            }
            return;
        }
        task_preprocess($tasks, $client_mould_details);
        $records["tasks"]=array();
        function control_pre_task(&$tasks, $opid)
        {
            for($i=0;$i<count($tasks);$i++) {
                if($tasks[$i]["opid"]==$opid&&isset($tasks[$i]["__start__"])) {
                    return $tasks[$i];
                }
            }
            return false;
        };
        function equipment_render(&$records, &$arr_data, &$task, $str_client, $str_code, $colspan, $boxcount)
        {
            $boxes_equipment=&$arr_data[$str_client]["equipments"][$str_code]["_timeline"];
            if(isset($boxes_equipment)&&count($boxes_equipment)>0) {
                $str_attr=$task["erprefnumber"].' '.$task["opname"].' '.$task["productcount"].' ('.$boxcount.')';
                $first_index=-1;
                for($i=0;$i<count($boxes_equipment);$i++) {
                    $el_rem_idx=$boxes_equipment[$i];
                    $pos_start = strpos($el_rem_idx["cls"], $task["__start__"]);
                    if($pos_start!==false) {
                        $first_index=$i;
                        break;
                    }
                }
                if($first_index>-1) {
                    $el_equipment=&$boxes_equipment[$first_index];
                    $pos_free = strpos($el_equipment["cls"], "free");
                    if($pos_free!==false) {
                        $el_equipment["title"]=$str_attr;
                        $el_equipment["cls"]=str_replace("free", "work", $el_equipment["cls"]);
                        array_splice($boxes_equipment, $first_index+1, $colspan-1);
                        $el_equipment["colSpan"]=$colspan;
                        $records["tasks"][]=array("event"=>"equipment_render_add","erprefnumber"=>$task["erprefnumber"],"rowcls"=>"client_".$str_client."_".$str_code,"boxcls"=>"client_hour_".$str_client."_".$el_equipment["id"],"title"=>$el_equipment["title"],"colspan"=>$el_equipment["colSpan"]);
                    } else {
                        $el_equipment["title"].='new_line'. $task["erprefnumber"].' '.$task["opname"].' '.$task["productcount"].' ('.$boxcount.')';
                        $records["tasks"][]=array("event"=>"equipment_render_edit","erprefnumber"=>$task["erprefnumber"],"rowcls"=>"client_".$str_client."_".$str_code,"boxcls"=>"client_hour_".$str_client."_".$el_equipment["id"],"title"=>$el_equipment["title"]);
                    }
                    //console.log(str_client,str_code,task.erprefnumber,task.opname);
                    $task["done"]=true;
                    //task_counter++;
                }
            }
            return;
        }

        function task_render(&$records, &$arr_data, &$tasks, &$task, $str_client, &$boxes, $opcount, $period, $index)
        {
            $boxcount=ceil($task["sure"]/$period);
            $counter=0;
            $j;
            for($j=$index;$j<count($boxes);$j++) {
                if($counter<$boxcount) {
                    $elc=&$boxes[$j];
                    $__start__=$elc["id"];
                    $pos_offtime = strpos($elc["cls"], "offtime");
                    if($pos_offtime===false) {
                        $el_opcount=&$arr_data["_timeline"]["opcount"][$elc["id"]];
                        $pos_work = strpos($elc["cls"], "work");
                        if($pos_work===false&&intval($el_opcount["title"])<$opcount) {
                            if($counter<$boxcount) {
                                ++$counter;
                                if(!isset($task["__start__"])) {
                                    $task["__start__"]=$__start__;
                                }
                                $el_opcount["title"]=intval($el_opcount["title"])+1;
                                $elc["cls"]=str_replace("free", "work", $elc["cls"]);
                            }
                        } else {
                            break;
                        }
                    } else {
                        if($j==$index) {
                            break;
                        }
                    }
                } else {
                    array_splice($boxes, $index+1, ($j-$index)-1);
                    $boxes[$index]["colSpan"]=$j-$index;
                    if(!isset($task["groupedtask"])) {
                        $arr_clientdetail=explode("|", $task["clientdetail"]);
                        for($cj=0;$cj<count($arr_clientdetail);$cj++) {
                            $tcd=$arr_clientdetail[$cj];
                            $str_code=implode("", explode(".", $tcd));
                            equipment_render($records, $arr_data, $task, $str_client, $str_code, $j-$index, $boxcount);
                        }
                    } else {
                        for($i=0;$i<count($tasks);$i++) {
                            $_row_task=&$tasks[$i];
                            if(isset($_row_task["groupedtask"])&&$_row_task["groupedtask"]===$task["groupedtask"]) {
                                $_row_task["__start__"]=$task["__start__"];
                                $arr_clientdetail=explode("|", $_row_task["clientdetail"]);
                                for($cj=0;$cj<count($arr_clientdetail);$cj++) {
                                    $tcd=$arr_clientdetail[$cj];
                                    $str_code=implode("", explode(".", $tcd));
                                    equipment_render($records, $arr_data, $_row_task, $str_client, $str_code, $j-$index, $boxcount);
                                }
                            }
                        }
                    }
                    return;
                }
            }
            if($counter>0) {
                //yarıda kalan varsa temizle
                unset($task["__start__"]);
                for($i=$index;$i<$j;$i++) {
                    $el=&$boxes[$i];
                    $pos_work = strpos($el["cls"], "work");
                    if($pos_work!==false) {
                        $el_opcount=&$arr_data["_timeline"]["opcount"][$el["id"]];
                        $el_opcount["title"]=intval($el_opcount["title"])-1;
                        $el["cls"]=str_replace("work", "free", $elc["cls"]);
                        //?$arr_data[$str_client]["_timeline"][$i]["title"]='';
                    }
                }
            }
            if($j<count($boxes)) {
                return task_render($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $j+1);
            }
        }

        function task_placer(&$records, &$arr_data, &$tasks, $task, $str_client, $boxes, $opcount, $period, $index, $_idx_)
        {
            $boxcount=ceil($task["sure"]/$period);
            $counter=0;
            $j;
            for($j=$index;$j<count($boxes);$j++) {
                if($counter<$boxcount) {
                    $elc=$boxes[$j];
                    $__start__=$elc["id"];
                    $pos_offtime = strpos($elc["cls"], "offtime");
                    if($pos_offtime===false) {
                        $el_opcount=&$arr_data["_timeline"]["opcount"][$elc["id"]];
                        $pos_work = strpos($elc["cls"], "work");
                        if($pos_work===false&&intval($el_opcount["title"])<$opcount) {
                            if($counter<$boxcount) {
                                ++$counter;
                                if(!isset($task["__start__"])) {
                                    $task["__start__"]=$__start__;
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        if($j==$index) {
                            break;
                        }
                    }
                } else {
                    return array("client"=>$str_client,"__start__"=>$task["__start__"],"index"=>$index,"_idx_"=>$_idx_);
                }
            }
            if($j<count($boxes)) {
                return task_placer($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $j+1, $_idx_);
            }
        }

        function render_tasks(&$records, &$arr_data, &$tasks, $opcount, $period)
        {
            $tmp_alternate_client_tasks=array();
            $alternate_task_id=0;
            $_idx_=0;
            foreach($tasks as &$task) {
                if(count($tmp_alternate_client_tasks)>0&&(($task["is_alternate_client_available"]===false)||($task["is_alternate_client_available"]===true&&$task["id"]!==$alternate_task_id))) {
                    $_task=&$tasks[$tmp_alternate_client_tasks[0]["_idx_"]];
                    $_str_client=$tmp_alternate_client_tasks[0]["client"];
                    $_boxes=&$arr_data[$_str_client]["_timeline"];
                    $_idx=$tmp_alternate_client_tasks[0]["index"];
                    task_render($records, $arr_data, $tasks, $_task, $_str_client, $_boxes, $opcount, $period, $_idx);
                    //görevi en uygun yere yerleştir
                    $alternate_task_id=0;
                    $tmp_alternate_client_tasks=array();
                }
                if(!isset($task["__start__"])) {
                    $idx=0;
                    $pre_task=false;
                    if($task["onkosul"]!=='0') {
                        $pre_task=control_pre_task($tasks, $task["onkosul"]);
                    }
                    //@todo: birden fazla istasyonda yapılabilme durumunda, hangisinde daha erkene yerleştirilebiliyor ise ona yerleşim yapılmalı
                    $str_client=implode("", explode(".", $task["client"]));
                    $arr_clientdetail=explode("|", $task["clientdetail"]);
                    $task["sure"]=$task["sure"]/count($arr_clientdetail);
                    if(isset($arr_data[$str_client])) {
                        $boxes=&$arr_data[$str_client]["_timeline"];
                        if (count($boxes)>0) {
                            if($pre_task!==false) {
                                for($j=0;$j<count($boxes);$j++) {
                                    $el=$boxes[$j];
                                    $__start__=$el["id"];
                                    if($pre_task["__start__"]===$__start__) {
                                        $idx=$j;
                                        break;
                                    }
                                }
                            }
                            if($task["is_alternate_client_available"]===true) {
                                if($alternate_task_id===0) {
                                    $alternate_task_id=$task["id"];
                                }
                                $tmp_arr=task_placer($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $idx, $_idx_);
                                if(count($tmp_alternate_client_tasks)>0) {
                                    if($tmp_alternate_client_tasks[0]["__start__"]>$tmp_arr["__start__"]) {
                                        $tmp_alternate_client_tasks[0]=$tmp_arr;
                                    }
                                } else {
                                    $tmp_alternate_client_tasks[]=$tmp_arr;
                                }
                            } else {
                                task_render($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $idx);
                            }
                        }
                    }
                }
                $_idx_++;
            }
            if(count($tmp_alternate_client_tasks)>0) {
                //görevi en uygun yere yerleştir
                $alternate_task_id=0;
                $tmp_alternate_client_tasks=array();
            }
        }
        render_tasks($records, $arr_data, $tasks, $opcount, $period);
        $records["render_opcount"]=$arr_data["_timeline"]["opcount"];

        $time_end = microtime();
        $duration=microtime_diff($time_start, $time_end);
        $records["duration"]=$duration;
        //$records["_arr_data"]=$arr_data;
        //$records["_tasks"]=$tasks;
        //echo json_encode(array("event"=>"done","duration"=>$duration,"data"=>$arr_data,"tasks"=>$tasks));

        return new JsonResponse(array("totalProperty"=>count($records),"records"=>$records));
    }

    /**
     * @Route(path="/Scheduler/getTasksStream/{period}/{dayscount}/{opcount}", name="Scheduler-getTasksStream", options={"expose"=true}, methods={"POST"})
     */
    public function getTasksStreamAction(Request $request, $_locale, $period, $dayscount, $opcount)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('AJAX request expected.');
        }
        ob_end_clean();
        ob_implicit_flush();
        $response = new StreamedResponse();
        $response->setCallback(function () use ($period, $dayscount, $opcount) {
            $time_start = microtime();
            function microtime_diff($start, $end = null)
            {
                if (!$end) {
                    $end = microtime();
                }
                list($start_usec, $start_sec) = explode(" ", $start);
                list($end_usec, $end_sec) = explode(" ", $end);
                $diff_sec = intval($end_sec) - intval($start_sec);
                $diff_usec = floatval($end_usec) - floatval($start_usec);
                return number_format((float)floatval($diff_sec) + $diff_usec, 2, ',', '');
            }
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $arr_data=array();
            $sql="SELECT cd.client ,string_agg(distinct cd.code, '|' order by cd.code) codes,count(distinct cd.code)ekipmansayisi 
            from client_details cd 
            join clients c on c.code=cd.client
            where cd.iotype='I' and cd.ioevent='input' and cd.finish is null and c.workflow not in ('El İşçiliği','Kataforez','Gölge Cihaz')
                --and cd.client like '03.08.0024%' 
            GROUP BY cd.client
            order by cd.client";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $client_details=$stmt->fetchAll();
            echo json_encode(array("event"=>"render_clients","data"=>$client_details));
            usleep(100000);
            $sql="select d.client,d.mould,d.mouldgroup,d.opname
                ,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime)/100,2)tpp 
                ,d.opcount
            from ( 
                SELECT cmd.client,cmd.clientmouldgroup 
                    ,string_agg(distinct md.mould,'|' ORDER BY md.mould)mould 
                    ,string_agg(distinct md.mouldgroup,'|' ORDER BY md.mouldgroup)mouldgroup 
                    ,string_agg(md.opname,'|' order by md.opname)opname
                    ,count(md.opname)opcount
                from client_mould_details cmd  
                join mould_details md on md.mouldgroup=cmd.mouldgroup 
                join client_details cd on cd.iotype='I' and cd.code=md.mould 
                where md.finish is null and cd.finish is null and cmd.finish is null
                GROUP BY cmd.client,cmd.clientmouldgroup
            )d 
            join client_mould_groups cmg on cmg.client=d.client and cmg.code=d.clientmouldgroup 
            where cmg.finish is null
            order by d.client asc,d.opcount desc,d.mould asc,d.mouldgroup asc,d.opname asc";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $client_mould_details=array();
            foreach($stmt->fetchAll() as $row) {
                if(!isset($client_mould_details[$row['client']])) {
                    $client_mould_details[$row['client']]=array();
                }
                $client_mould_details[$row['client']][]=$row;
            }

            $sql="SELECT md.mould,md.mouldgroup,md.opcode,md.opnumber,md.opname,md.leafmask,md.leafmaskcurrent
                ,mg.productionmultiplier,mg.intervalmultiplier,mg.setup,mg.cycletime
            FROM mould_details md
            join mould_groups mg on mg.mould=md.mould and mg.code=md.mouldgroup 
            where md.finish is null and mg.finish is null
            ORDER BY md.mould,md.mouldgroup,md.opname";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $mould_details=array();
            foreach($stmt->fetchAll() as $row) {
                if(!isset($mould_details[$row['mould']])) {
                    $mould_details[$row['mould']]=array();
                }
                $mould_details[$row['mould']][]=$row;
            }

            $sql="SELECT id,clients,losttype,name,repeattype,days,startday,starttime,finishday,finishtime,clearonovertime
                ,REPLACE(starttime, ':', '')starttime2,REPLACE(finishtime, ':', '')finishtime2
            from offtimes 
            where (finish is null or finish>CURRENT_TIMESTAMP(0)) ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $offtimes=$stmt->fetchAll();

            $sql="SELECT id,clients,name,repeattype,days,startday,starttime,finishday,finishtime 
            from overtimes 
            where (finish is null or finish>CURRENT_TIMESTAMP(0)) ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $overtimes=$stmt->fetchAll();

            $sql="select * 
            from (	
                SELECT concat(ti.erprefnumber,ti.opnumber) opid,concat(ti.opcode,'-',ti.opnumber) opname
                    ,coalesce((SELECT concat(ti.erprefnumber,pt_pre.number)
                        from product_trees pt_pre
                        join task_imports ti_pre on concat(ti_pre.opcode,'-',ti_pre.opnumber)=pt_pre.name and ti_pre.erprefnumber=tl.erprefnumber
                        where pt_pre.finish is null and pt_pre.materialtype='O' and pt_pre.code=pt.code and pt_pre.oporder<pt.oporder 
                        order by pt_pre.oporder desc limit 1) ,'0') onkosul
                    ,ti.erprefnumber,cast(ti.deadline-interval '2 day' as varchar(10)) baslamatarihi,ti.deadline termintarihi
                    ,pt.tpp,ti.productcount,cast(ceil((pt.tpp*ti.productcount)/60) as integer)sure,ti.client
                    ,case when cpd.client is not null or ti.erprefnumber in ('72825604','72737597','72826723') then 'Devam Eden' 
                            when tl.finish is not null then 'Tamamlanan' else '0' end sabitlenmis
                    ,coalesce((select string_agg(aaa.mould,'|')mould 
                        from 
                            (SELECT md.mould
                            from client_details cd
                            join mould_details md on md.mould=cd.code
                            where cd.client=ti.client and cd.iotype='I' and md.opname=concat(ti.opcode,'-',ti.opnumber) and cd.finish is null and md.finish is null
                            group by md.mould
                            )
                        aaa),ti.client) clientdetail
                from task_imports ti 
                join product_trees pt on pt.code=ti.opcode and pt.number=ti.opnumber and pt.materialtype in ('O') and pt.finish is null
                left join task_lists tl on ti.erprefnumber=tl.erprefnumber and ti.opcode=tl.opcode and ti.opnumber=tl.opnumber
                left join (SELECT client,erprefnumber from client_production_details where type in ('c_p','c_p_confirm') and finish is null group by client,erprefnumber)cpd on cpd.client=ti.client and cpd.erprefnumber=ti.erprefnumber
                where ti.deadline>current_date- interval '5 day'
            )tlx
            --where tlx.client like '03.08.0024%' 
            order by case when tlx.sabitlenmis='Devam Eden' then 1 else 2 end,tlx.baslamatarihi,tlx.erprefnumber,tlx.onkosul ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $tasks=$stmt->fetchAll();

            $sql_start="SELECT 
                concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
            from job_rotations 
            where (finish is null or finish<now())
            order by beginval desc limit 1";
            $stmt_start = $conn->prepare($sql_start);
            $stmt_start->bindValue('t1', $tasks[0]["baslamatarihi"]);
            $stmt_start->execute();
            $records_start = $stmt_start->fetchAll();
            $firsttime=$records_start[0]["jr_start"];

            $arr_data["_timeline"]=array("days"=>array(),"hours"=>array(),"opcount"=>array());
            foreach($client_details as $row_cd) {
                $str_client=implode("", explode(".", $row_cd["client"]));
                if(!isset($arr_data[$str_client])) {
                    $arr_data[$str_client]=array();
                }
                $arr_data[$str_client]["title"]=$row_cd["client"];
                $arr_data[$str_client]["_timeline"]=array();
                $arr_data[$str_client]["equipments"]=array();
                $arr_codes=explode("|", $row_cd["codes"]);
                foreach($arr_codes as $row_cd_codes) {
                    $str_code=implode("", explode(".", $row_cd_codes));
                    $arr_data[$str_client]["equipments"][$str_code]=array("title"=>$row_cd_codes,"_timeline"=>array());
                }
            }
            //$arr_tmp=array("days"=>array(),"hours"=>array(),"opcount"=>array());
            //$last_day=false;
            $x=new \DateTime($firsttime);
            for ($i=0; $i <($dayscount*24*60/$period) ; $i++) {
                $str_index=$x->format('YmdHi');
                $str_day=$x->format('Ymd');
                $str_readable_day=$x->format('l');
                $str_day_short=$x->format('d-m-y');
                $str_time5=$x->format('H:i');
                $str_time4=$x->format('Hi');
                //if($last_day!==false){
                //    if($last_day!==$str_day){
                //        echo json_encode(array("event"=>"render_timeline","data"=>$arr_tmp));
                //        flush();
                //        usleep(50000);
                //        $arr_tmp=array("days"=>array(),"hours"=>array(),"opcount"=>array());
                //        $last_day=$str_day;
                //    }
                //}else{
                //    $last_day=$str_day;
                //}
                if(!isset($arr_data["_timeline"]["days"][$str_day])) {
                    $arr_data["_timeline"]["days"][$str_day]=array("id"=>$str_day,"title"=>$str_day_short,"cs"=>1 );
                    //$arr_tmp["days"][$str_day]=array("id"=>$str_day,"title"=>$str_day_short,"cs"=>1 );
                } else {
                    $arr_data["_timeline"]["days"][$str_day]["cs"]=intval($arr_data["_timeline"]["days"][$str_day]["cs"])+1;
                    //$arr_tmp["days"][$str_day]["cs"]=intval($arr_data["_timeline"]["days"][$str_day]["cs"]);
                }
                $arr_data["_timeline"]["hours"][$str_index]=array("id"=>$str_index,"title"=>$str_time5);
                $arr_data["_timeline"]["opcount"][$str_index]=array("id"=>$str_index,"title"=>0);

                //$arr_tmp["hours"][$str_index]=array("id"=>$str_index,"title"=>$str_time5);
                //$arr_tmp["opcount"][$str_index]=array("id"=>$str_index,"title"=>0);
                $last_client=false;
                foreach($client_details as $row_cd) {
                    $str_client=implode("", explode(".", $row_cd["client"]));
                    $arr_codes=explode("|", $row_cd["codes"]);
                    $_k=0;
                    $_cls='free';
                    $_title='';
                    if($last_client!==$str_client) {
                        foreach($offtimes as $row_offtime) {
                            if(strpos($row_offtime["clients"], $row_cd["client"])!==false
                            &&(($row_offtime["repeattype"]!=='day') || ($row_offtime["repeattype"]==='day' && strpos($row_offtime["days"], $str_readable_day)!==false))
                            ) {
                                $vi_start=intval($row_offtime["starttime2"]);
                                $vi_finish=intval($row_offtime["finishtime2"]);
                                if(intval($str_time4)>=$vi_start && intval($str_time4)<$vi_finish) {
                                    $_cls='offtime';
                                    $_title=$row_offtime["losttype"].' '.$row_offtime["starttime"].'-'.$row_offtime["finishtime"];
                                }
                            }
                        }
                    }
                    foreach($arr_codes as $row_cd_codes) {
                        $str_code=implode("", explode(".", $row_cd_codes));
                        $arr_data[$str_client]["equipments"][$str_code]["_timeline"][]=array("id"=>$str_day.$str_time4,"cls"=>" ".$str_client.'_'.$str_day.$str_time4.' '.$_cls,"title"=>$_title );
                        if($_k==0) {
                            $arr_data[$str_client]["_timeline"][]=array("id"=>$str_day.$str_time4,"cls"=>" ".$str_client.'_'.$str_day.$str_time4.' '.$_cls,"title"=>$_title );
                        }
                        $_k++;
                    }
                    $last_client=$str_client;
                }

                $x->setTimestamp($x->getTimestamp()+((($period/60)*3600)));
            }
            //echo json_encode(array("event"=>"render_timeline","data"=>$arr_tmp));
            //flush();
            //usleep(50000);
            //echo json_encode(array("event"=>"render_timeline","data"=>$arr_data["_timeline"]));
            //flush();
            //usleep(100000);
            //$boxes=array();
            /*
            foreach($arr_data as $key=>$row){
                if($key!=="_timeline"){
                    $boxes=array();
                    $equipments=array();
                    foreach($row["equipments"] as $key_e=>$row_e){
                        $equipments[]=$key_e;
                    }
                    $boxes[$key]=array("title"=>$row["title"],"client"=>$key,"_timeline"=>$row["_timeline"],"equipments"=>implode("|", $equipments));
                    echo json_encode(array("event"=>"render_boxes","data"=>$boxes));
                    flush();
                    usleep(500000);
                }
            }
            */
            //echo json_encode(array("event"=>"render_boxes","data"=>$boxes));
            //flush();
            echo json_encode(array("event"=>"render_boxes_2","data"=>array("firsttime"=>$firsttime,"dayscount"=>$dayscount,"period"=>$period,"offtimes"=>$offtimes)));
            usleep(500000);
            function task_preprocess(&$tasks, $client_mould_details)
            {
                $i=0;
                foreach($tasks as &$task) {
                    if(!isset($task["groupedtask"])) {
                        $arr_task_group=array();
                        if(isset($client_mould_details[$task["client"]])) {
                            $arr_task_group[]=$i;
                            $_cmd_=$client_mould_details[$task["client"]];
                            for($j=0;$j<count($_cmd_);$j++) {
                                $_row_cmd=$_cmd_[$j];
                                $_row_arr_opname=explode("|", $_row_cmd["opname"]);
                                $_flag_opname=true;
                                for($k=0;$k<count($_row_arr_opname);$k++) {
                                    $_item_opname=$_row_arr_opname[$k];
                                    if($_item_opname==$task["opname"]) {
                                        continue;
                                    }
                                    $_task_found=false;
                                    for($l=0;$l<count($tasks);$l++) {
                                        $_item_task=$tasks[$l];
                                        if(isset($_item_task["groupedtask"])
                                            ||$_task_found!==false
                                            ||$_item_task["opname"]!==$_item_opname
                                            ||$_item_task["client"]!==$task["client"]
                                            ||$_item_task["termintarihi"]!==$task["termintarihi"]
                                            ||$_item_task["productcount"]!==$task["productcount"]
                                        ) {
                                            continue;
                                        } else {
                                            $arr_task_group[]=$l;
                                            $_task_found=true;
                                            break;
                                        }
                                    }
                                    if(!$_task_found) {
                                        $_flag_opname=false;
                                    }
                                }
                                if($_flag_opname) {
                                    for($m=0;$m<count($arr_task_group);$m++) {
                                        $idx=$arr_task_group[$m];
                                        $tasks[$idx]["groupedtask"]=$arr_task_group[0];
                                        $tasks[$idx]["tpp"]=$_row_cmd["tpp"];
                                        $tasks[$idx]["sure"]=ceil((intval($_row_cmd["tpp"])*$task["productcount"])/60);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    $i++;
                }
                return;
            }
            task_preprocess($tasks, $client_mould_details);

            function equipment_render(&$arr_data, &$task, $str_client, $str_code, $colspan, $boxcount)
            {
                $boxes_equipment=&$arr_data[$str_client]["equipments"][$str_code]["_timeline"];
                if(count($boxes_equipment)>0) {
                    $str_attr=$task["erprefnumber"].' '.$task["opname"].' '.$task["productcount"].' ('.$boxcount.')';
                    $first_index=-1;
                    for($i=0;$i<count($boxes_equipment);$i++) {
                        $el_rem_idx=$boxes_equipment[$i];
                        $pos_start = strpos($el_rem_idx["cls"], $task["__start__"]);
                        if($pos_start!==false) {
                            $first_index=$i;
                            break;
                        }
                    }
                    if($first_index>-1) {
                        $el_equipment=&$boxes_equipment[$first_index];
                        $pos_free = strpos($el_equipment["cls"], "free");
                        if($pos_free!==false) {
                            $el_equipment["title"]=$str_attr;
                            $el_equipment["cls"]=str_replace("free", "work", $el_equipment["cls"]);
                            array_splice($boxes_equipment, $first_index+1, $colspan-1);
                            $el_equipment["colSpan"]=$colspan;
                            echo json_encode(array("event"=>"equipment_render_add","rowcls"=>"client_".$str_client."_".$str_code,"boxcls"=>"client_hour_".$str_client."_".$el_equipment["id"],"title"=>$el_equipment["title"],"colspan"=>$el_equipment["colSpan"]));
                            usleep(400000);
                        } else {
                            $el_equipment["title"].='new_line'. $task["erprefnumber"].' '.$task["opname"].' '.$task["productcount"].' ('.$boxcount.')';
                            echo json_encode(array("event"=>"equipment_render_edit","rowcls"=>"client_".$str_client."_".$str_code,"boxcls"=>"client_hour_".$str_client."_".$el_equipment["id"],"title"=>$el_equipment["title"]));
                            usleep(400000);
                        }
                        //console.log(str_client,str_code,task.erprefnumber,task.opname);
                        $task["done"]=true;
                        //task_counter++;
                    }
                }
                return;
            }

            function task_render(&$arr_data, &$tasks, &$task, $str_client, &$boxes, $opcount, $period, $index)
            {
                $boxcount=ceil($task["sure"]/$period);
                $counter=0;
                $j;
                for($j=$index;$j<count($boxes);$j++) {
                    if($counter<$boxcount) {
                        $elc=&$boxes[$j];
                        $__start__=$elc["id"];
                        $pos_offtime = strpos($elc["cls"], "offtime");
                        if($pos_offtime===false) {
                            $el_opcount=&$arr_data["_timeline"]["opcount"][$elc["id"]];
                            $pos_work = strpos($elc["cls"], "work");
                            if($pos_work===false&&intval($el_opcount["title"])<$opcount) {
                                if($counter<$boxcount) {
                                    ++$counter;
                                    if(!isset($task["__start__"])) {
                                        $task["__start__"]=$__start__;
                                    }
                                    $el_opcount["title"]=intval($el_opcount["title"])+1;
                                    $elc["cls"]=str_replace("free", "work", $elc["cls"]);
                                }
                            } else {
                                break;
                            }
                        } else {
                            if($j==$index) {
                                break;
                            }
                        }
                    } else {
                        array_splice($boxes, $index+1, ($j-$index)-1);
                        $boxes[$index]["colSpan"]=$j-$index;
                        if(!isset($task["groupedtask"])) {
                            $arr_clientdetail=explode("|", $task["clientdetail"]);
                            for($cj=0;$cj<count($arr_clientdetail);$cj++) {
                                $tcd=$arr_clientdetail[$cj];
                                $str_code=implode("", explode(".", $tcd));
                                equipment_render($arr_data, $task, $str_client, $str_code, $j-$index, $boxcount);
                                usleep(300000);
                            }
                        } else {
                            for($i=0;$i<count($tasks);$i++) {
                                $_row_task=&$tasks[$i];
                                if(isset($_row_task["groupedtask"])&&$_row_task["groupedtask"]===$task["groupedtask"]) {
                                    $_row_task["__start__"]=$task["__start__"];
                                    $arr_clientdetail=explode("|", $_row_task["clientdetail"]);
                                    for($cj=0;$cj<count($arr_clientdetail);$cj++) {
                                        $tcd=$arr_clientdetail[$cj];
                                        $str_code=implode("", explode(".", $tcd));
                                        equipment_render($arr_data, $_row_task, $str_client, $str_code, $j-$index, $boxcount);
                                        usleep(400000);
                                    }
                                }
                            }
                        }
                        return;
                    }
                }
                if($counter>0) {
                    //yarıda kalan varsa temizle
                    unset($task["__start__"]);
                    for($i=$index;$i<$j;$i++) {
                        $el=&$boxes[$i];
                        $pos_work = strpos($el["cls"], "work");
                        if($pos_work!==false) {
                            $el_opcount=&$arr_data["_timeline"]["opcount"][$el["id"]];
                            $el_opcount["title"]=intval($el_opcount["title"])-1;
                            $el["cls"]=str_replace("work", "free", $elc["cls"]);
                            //?$arr_data[$str_client]["_timeline"][$i]["title"]='';
                        }
                    }
                }
                if($j<count($boxes)) {
                    usleep(50000);
                    return task_render($arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $j+1);
                }
            }

            function render_tasks(&$arr_data, &$tasks, $opcount, $period)
            {
                foreach($tasks as &$task) {
                    if(!isset($task["__start__"])) {
                        $idx=0;
                        $pre_task=false;
                        if($task["onkosul"]!=='0') {
                            $pre_task=control_pre_task($task["onkosul"]);
                        }
                        //@todo: birden fazla istasyonda yapılabilme durumunda, hangisinde daha erkene yerleştirilebiliyor ise ona yerleşim yapılmalı
                        $str_client=implode("", explode(".", $task["client"]));
                        $arr_clientdetail=explode("|", $task["clientdetail"]);
                        $task["sure"]=$task["sure"]/count($arr_clientdetail);
                        if(isset($arr_data[$str_client])) {
                            $boxes=&$arr_data[$str_client]["_timeline"];
                            if (count($boxes)>0) {
                                if($pre_task!==false) {
                                    for($j=0;$j<count($boxes);$j++) {
                                        $el=$boxes[$j];
                                        $__start__=$el["id"];
                                        if($pre_task["__start__"]===$__start__) {
                                            $idx=$j;
                                            break;
                                        }
                                    }
                                }
                                task_render($arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $idx);
                            }
                        }
                    }
                }
            }
            render_tasks($arr_data, $tasks, $opcount, $period);

            echo json_encode(array("event"=>"render_opcount","data"=>$arr_data["_timeline"]["opcount"]));

            $time_end = microtime();
            $duration=microtime_diff($time_start, $time_end);
            echo json_encode(array("event"=>"done","duration"=>$duration,"data"=>$arr_data,"tasks"=>$tasks));
        });

        return $response;
    }

    /**
     * @Route(path="/Scheduler/getTaskPlanned/{period}/{dayscount}/{opcount}", name="Scheduler-getTaskPlanned", options={"expose"=true}, methods={"POST"})
     */
    public function getTaskPlanned(Request $request, $_locale, $period, $dayscount, $opcount)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('AJAX request expected.');
        }

        $time_start = microtime();
        $records=array();
        function microtime_diff($start, $end = null)
        {
            if (!$end) {
                $end = microtime();
            }
            list($start_usec, $start_sec) = explode(" ", $start);
            list($end_usec, $end_sec) = explode(" ", $end);
            $diff_sec = intval($end_sec) - intval($start_sec);
            $diff_usec = floatval($end_usec) - floatval($start_usec);
            return number_format((float)floatval($diff_sec) + $diff_usec, 2, ',', '');
        }
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $arr_data=array();
        $sql="SELECT cd.client ,string_agg(distinct cd.code, '|' order by cd.code) codes,count(distinct cd.code)ekipmansayisi 
        from client_details cd 
        join clients c on c.code=cd.client
        where cd.iotype='I' and cd.ioevent='input' and cd.finish is null and c.workflow not in ('El İşçiliği','Kataforez','Gölge Cihaz')
            -- and cd.client like '03.08.0018%' 
        GROUP BY cd.client
        order by cd.client";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $client_details=$stmt->fetchAll();
        $records["client_details"]=$client_details;
        $sql="select d.client,d.mould,d.mouldgroup,d.opname
            ,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime)/100,2)tpp 
            ,d.opcount
        from ( 
            SELECT cmd.client,cmd.clientmouldgroup 
                ,string_agg(distinct md.mould,'|' ORDER BY md.mould)mould 
                ,string_agg(distinct md.mouldgroup,'|' ORDER BY md.mouldgroup)mouldgroup 
                ,string_agg(md.opname,'|' order by md.opname)opname
                ,count(md.opname)opcount
            from client_mould_details cmd  
            join mould_details md on md.mouldgroup=cmd.mouldgroup 
            join client_details cd on cd.iotype='I' and cd.code=md.mould 
            where md.finish is null and cd.finish is null and cmd.finish is null
            GROUP BY cmd.client,cmd.clientmouldgroup
        )d 
        join client_mould_groups cmg on cmg.client=d.client and cmg.code=d.clientmouldgroup 
        where cmg.finish is null
        order by d.client asc,d.opcount desc,d.mould asc,d.mouldgroup asc,d.opname asc";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $client_mould_details=array();
        foreach($stmt->fetchAll() as $row) {
            if(!isset($client_mould_details[$row['client']])) {
                $client_mould_details[$row['client']]=array();
            }
            $client_mould_details[$row['client']][]=$row;
        }
        $sql="SELECT concat(erprefnumber,opnumber) opid,opname,0 onkosul,erprefnumber,productcount,deadline,client,tpp,start,plannedstart,plannedfinish 
            ,ceil(cast(extract(epoch from (plannedfinish::timestamp-plannedstart::timestamp)) as integer)/60) sure
            ,client as clients,false is_alternate_client_available,'1' as sabitlenmis 
            ,coalesce((select string_agg(aaa.mould,'|')mould 
                from 
                (SELECT md.mould
                    from client_details cd
                    join mould_details md on md.mould=cd.code
                    where cd.client=tl.client and cd.iotype='I' and md.opname=tl.opname and cd.finish is null and md.finish is null
                    group by md.mould
                )
                aaa),tl.client) clientdetail
        from task_lists tl
        where plannedfinish is not null and delete_user_id is null and finish is null and plannedstart<>plannedfinish--and client='03.06.0026'
        order by plannedstart";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $tasks=$stmt->fetchAll();
        if(count($tasks)>0) {
            $sql_start="SELECT 
                concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
            from job_rotations 
            where (finish is null or finish<now())
            order by beginval desc limit 1";
            $stmt_start = $conn->prepare($sql_start);
            $stmt_start->bindValue('t1', $tasks[0]["plannedstart"]);
            $stmt_start->execute();
            $records_start = $stmt_start->fetchAll();
            $firsttime=$records_start[0]["jr_start"];

            $sql="SELECT id,clients,losttype,name,repeattype,days,startday,starttime,finishday,finishtime,clearonovertime
                ,REPLACE(starttime, ':', '')starttime2,REPLACE(finishtime, ':', '')finishtime2,start,finish
            from offtimes 
            where (finish is null or finish>:finish) ";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('finish', $firsttime);
            $stmt->execute();
            $offtimes=$stmt->fetchAll();

            $sql="SELECT id,clients,name,repeattype,days,startday,starttime,finishday,finishtime
                ,REPLACE(starttime, ':', '')starttime2,REPLACE(finishtime, ':', '')finishtime2,start,finish 
            from overtimes 
            where (finish is null or finish>:finish) ";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('finish', $firsttime);
            $stmt->execute();
            $overtimes=$stmt->fetchAll();
        }

        $arr_data["_timeline"]=array("days"=>array(),"hours"=>array(),"opcount"=>array());
        foreach($client_details as $row_cd) {
            $str_client=implode("", explode(".", $row_cd["client"]));
            if(!isset($arr_data[$str_client])) {
                $arr_data[$str_client]=array();
            }
            $arr_data[$str_client]["title"]=$row_cd["client"];
            $arr_data[$str_client]["_timeline"]=array();
            $arr_data[$str_client]["equipments"]=array();
            $arr_codes=explode("|", $row_cd["codes"]);
            foreach($arr_codes as $row_cd_codes) {
                $str_code=implode("", explode(".", $row_cd_codes));
                $arr_data[$str_client]["equipments"][$str_code]=array("title"=>$row_cd_codes,"_timeline"=>array());
            }
        }
        if (count($tasks)>0) {
            $x=new \DateTime($firsttime);
            for ($i=0; $i <($dayscount*24*60/$period) ; $i++) {
                $str_index=$x->format('YmdHi');
                $str_day=$x->format('Ymd');
                $str_readable_day=$x->format('l');
                $str_day_short=$x->format('d-m-y');
                $str_time5=$x->format('H:i');
                $str_time4=$x->format('Hi');
                if (!isset($arr_data["_timeline"]["days"][$str_day])) {
                    $arr_data["_timeline"]["days"][$str_day]=array("id"=>$str_day,"title"=>$str_day_short,"cs"=>1 );
                } else {
                    $arr_data["_timeline"]["days"][$str_day]["cs"]=intval($arr_data["_timeline"]["days"][$str_day]["cs"])+1;
                }
                $arr_data["_timeline"]["hours"][$str_index]=array("id"=>$str_index,"title"=>$str_time5);
                $arr_data["_timeline"]["opcount"][$str_index]=array("id"=>$str_index,"title"=>0);

                $last_client=false;
                foreach ($client_details as $row_cd) {
                    $str_client=implode("", explode(".", $row_cd["client"]));
                    $arr_codes=explode("|", $row_cd["codes"]);
                    $_k=0;
                    $_cls="free";
                    //if($str_index=='201907160500'){
                    //    $_cls="work";
                    //}
                    $_title='';
                    if ($last_client!==$str_client) {
                        foreach ($offtimes as &$row_offtime) {
                            if (strpos($row_offtime["clients"], $row_cd["client"])!==false
                        &&(($row_offtime["repeattype"]!=='day' && implode("", explode("-", $row_offtime["startday"]))===$str_day) || ($row_offtime["repeattype"]==='day' && strpos($row_offtime["days"], $str_readable_day)!==false))
                            ) {
                                $vi_start=intval($row_offtime["starttime2"]);
                                $vi_finish=intval($row_offtime["finishtime2"]);
                                if ($row_offtime["clearonovertime"]==true
                                &&(count($overtimes)>0)
                                &&($row_offtime["finish"]==null||($row_offtime["finish"]!=null&&$row_offtime["finish"]>$firsttime))) {
                                    for ($j=0;$j<count($overtimes);$j++) {
                                        $item_over=$overtimes[$j];
                                        if (strpos($item_over["clients"], $row_cd["client"])!==false&&implode("", explode("-", $item_over["startday"]))===$str_day) {
                                            $vi_start_over=intval($item_over["starttime2"]);
                                            $vi_finish_over=intval($item_over["finishtime2"]);
                                            if ($vi_start>=$vi_start_over) {
                                                //offtime öncesinden başlayan bir fazla mesai
                                                if ($vi_finish>$vi_finish_over) {
                                                    //offtime başlangıcını ötele
                                                    $row_offtime["starttime"]=$item_over["finishtime"];
                                                    $row_offtime["starttime2"]=$item_over["finishtime2"];
                                                } elseif ($vi_finish<=$vi_finish_over) {
                                                    //offtime kaydını sil
                                                    $row_offtime["modified"]=false;
                                                }
                                            } else {
                                                if ($vi_finish>$vi_finish_over) {
                                                    //parçala
                                                    $_x=$row_offtime;
                                                    $row_offtime["finishtime"]=$item_over["starttime"];
                                                    $row_offtime["finishtime2"]=$item_over["starttime2"];
                                                    $_x["starttime"]=$item_over["finishtime"];
                                                    $_x["starttime2"]=$item_over["finishtime2"];
                                                    $offtimes[]=$_x;
                                                } else {
                                                    //offtime bitişini geri çek
                                                    $row_offtime["finishtime"]=$item_over["starttime"];
                                                    $row_offtime["finishtime2"]=$item_over["starttime2"];
                                                }
                                            }
                                            if (intval($str_time4)>=$vi_start && intval($str_time4)<$vi_finish) {
                                                $_cls='offtime';
                                                $_title=$row_offtime["losttype"].' '.$row_offtime["starttime"].'-'.$row_offtime["finishtime"];
                                            }
                                        }
                                    }
                                } else {
                                    if (intval($str_time4)>=$vi_start && intval($str_time4)<$vi_finish) {
                                        $_cls='offtime';
                                        $_title=$row_offtime["losttype"].' '.$row_offtime["starttime"].'-'.$row_offtime["finishtime"];
                                    }
                                }
                            }
                        }
                    }
                    foreach ($arr_codes as $row_cd_codes) {
                        $str_code=implode("", explode(".", $row_cd_codes));
                        $arr_data[$str_client]["equipments"][$str_code]["_timeline"][]=array("id"=>$str_index,"cls"=>" ".$str_client.'_'.$str_index.' '.$_cls,"title"=>$_title );
                        if ($_k==0) {
                            $arr_data[$str_client]["_timeline"][]=array("id"=>$str_index,"cls"=>" ".$str_client.'_'.$str_index.' '.$_cls,"title"=>$_title );
                        }
                        $_k++;
                    }
                    $last_client=$str_client;
                }

                $x->setTimestamp($x->getTimestamp()+((($period/60)*3600)));
            }
            $records["firsttime"]=$firsttime;
            $records["offtimes"]=$offtimes;
        }
        $records["dayscount"]=$dayscount;
        $records["period"]=$period;
        function task_preprocess(&$tasks, $client_mould_details)
        {
            $i=0;
            foreach($tasks as &$task) {
                if(!isset($task["groupedtask"])) {
                    $arr_task_group=array();
                    if(isset($client_mould_details[$task["client"]])) {
                        $arr_task_group[]=$i;
                        $_cmd_=$client_mould_details[$task["client"]];
                        for($j=0;$j<count($_cmd_);$j++) {
                            $_row_cmd=$_cmd_[$j];
                            $_row_arr_opname=explode("|", $_row_cmd["opname"]);
                            $_flag_opname=true;
                            for($k=0;$k<count($_row_arr_opname);$k++) {
                                $_item_opname=$_row_arr_opname[$k];
                                if($_item_opname==$task["opname"]) {
                                    continue;
                                }
                                $_task_found=false;
                                for($l=0;$l<count($tasks);$l++) {
                                    $_item_task=$tasks[$l];
                                    if(isset($_item_task["groupedtask"])
                                        ||$_task_found!==false
                                        ||$_item_task["opname"]!==$_item_opname
                                        ||$_item_task["client"]!==$task["client"]
                                        ||$_item_task["termintarihi"]!==$task["termintarihi"]
                                        ||$_item_task["productcount"]!==$task["productcount"]
                                    ) {
                                        continue;
                                    } else {
                                        $arr_task_group[]=$l;
                                        $_task_found=true;
                                        break;
                                    }
                                }
                                if(!$_task_found) {
                                    $_flag_opname=false;
                                }
                            }
                            if($_flag_opname) {
                                for($m=0;$m<count($arr_task_group);$m++) {
                                    $idx=$arr_task_group[$m];
                                    $tasks[$idx]["groupedtask"]=$arr_task_group[0];
                                    $tasks[$idx]["tpp"]=$_row_cmd["tpp"];
                                    $tasks[$idx]["sure"]=ceil((intval($_row_cmd["tpp"])*$task["productcount"])/60);
                                }
                                break;
                            }
                        }
                    }
                }
                $i++;
            }
            return;
        }
        task_preprocess($tasks, $client_mould_details);
        $records["tasks"]=array();
        function control_pre_task(&$tasks, $opid)
        {
            for($i=0;$i<count($tasks);$i++) {
                if($tasks[$i]["opid"]==$opid&&isset($tasks[$i]["__start__"])) {
                    return $tasks[$i];
                }
            }
            return false;
        };
        function equipment_render(&$records, &$arr_data, &$task, $str_client, $str_code, $colspan, $boxcount)
        {
            if($task["sabitlenmis"]==='1'&&$task["plannedfinish"]!==''&&$task["plannedfinish"]!==null) {
                $colspan=$boxcount;
                if(!isset($task["__start__"])) {
                    $strstart=implode("", explode(" ", implode("", explode("-", implode("", explode(":", $task["plannedstart"]))))));
                    $task["__start__"]=$strstart;
                }
            }
            $boxes_equipment=&$arr_data[$str_client]["equipments"][$str_code]["_timeline"];
            if(isset($boxes_equipment)&&count($boxes_equipment)>0) {
                $str_attr=$task["erprefnumber"].' '.$task["opname"].' '.$task["productcount"].' ('.$boxcount.')';
                $first_index=-1;
                for($i=0;$i<count($boxes_equipment);$i++) {
                    $el_rem_idx=$boxes_equipment[$i];
                    $pos_start = strpos($el_rem_idx["cls"], $task["__start__"]);
                    if($pos_start!==false) {
                        $first_index=$i;
                        break;
                    }
                }
                if($first_index>-1) {
                    $el_equipment=&$boxes_equipment[$first_index];
                    if($task["sabitlenmis"]==='1'&&$task["plannedfinish"]!==''&&$task["plannedfinish"]!==null) {
                        $pos_free = true;
                    } else {
                        $pos_free = strpos($el_equipment["cls"], "free");
                    }
                    if($pos_free!==false) {
                        $el_equipment["title"]=$str_attr;
                        $el_equipment["cls"]=str_replace("free", "work", $el_equipment["cls"]);
                        array_splice($boxes_equipment, $first_index+1, $colspan-1);
                        $el_equipment["colSpan"]=$colspan;
                        $records["tasks"][]=array("event"=>"equipment_render_add","erprefnumber"=>$task["erprefnumber"],"rowcls"=>"client_".$str_client."_".$str_code,"boxcls"=>"client_hour_".$str_client."_".$el_equipment["id"],"title"=>$el_equipment["title"],"colspan"=>$el_equipment["colSpan"]);
                    } else {
                        $el_equipment["title"].='new_line'. $task["erprefnumber"].' '.$task["opname"].' '.$task["productcount"].' ('.$boxcount.')';
                        $records["tasks"][]=array("event"=>"equipment_render_edit","erprefnumber"=>$task["erprefnumber"],"rowcls"=>"client_".$str_client."_".$str_code,"boxcls"=>"client_hour_".$str_client."_".$el_equipment["id"],"title"=>$el_equipment["title"]);
                    }
                    //console.log(str_client,str_code,task.erprefnumber,task.opname);
                    $task["done"]=true;
                    //task_counter++;
                }
            }
            return;
        }

        function task_render(&$records, &$arr_data, &$tasks, &$task, $str_client, &$boxes, $opcount, $period, $index)
        {
            $boxcount=ceil($task["sure"]/$period);
            $counter=0;
            $j;
            for($j=$index;$j<count($boxes);$j++) {
                if($counter<$boxcount) {
                    $elc=&$boxes[$j];
                    $__start__=$elc["id"];
                    $strstart=implode("", explode(" ", implode("", explode("-", implode("", explode(":", $task["plannedstart"]))))));
                    if($task["sabitlenmis"]==='1'&&$task["plannedfinish"]!==''&&$task["plannedfinish"]!==null) {
                        if(($__start__."00")<$strstart) {
                            continue;
                        } else {
                            if($counter===0) {
                                $index=$j;
                            }
                        }
                    }
                    if($task["sabitlenmis"]==='1'&&$task["plannedfinish"]!==''&&$task["plannedfinish"]!==null) {
                        $pos_offtime=false;
                    } else {
                        $pos_offtime = strpos($elc["cls"], "offtime");
                    }
                    if($pos_offtime===false) {
                        $el_opcount=&$arr_data["_timeline"]["opcount"][$elc["id"]];
                        $pos_work = strpos($elc["cls"], "work");
                        if($pos_work===false&&intval($el_opcount["title"])<$opcount) {
                            if($counter<$boxcount) {
                                ++$counter;
                                if(!isset($task["__start__"])) {
                                    $task["__start__"]=$__start__;
                                }
                                $el_opcount["title"]=intval($el_opcount["title"])+1;
                                $elc["cls"]=str_replace("free", "work", $elc["cls"]);
                                if ($task["sabitlenmis"]==='1'&&$task["plannedfinish"]!==''&&$task["plannedfinish"]!==null) {
                                    $elc["cls"]=str_replace("offtime", "work", $elc["cls"]);
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        if($j==$index) {
                            break;
                        }
                    }
                } else {
                    array_splice($boxes, $index+1, ($j-$index)-1);
                    if($task["sabitlenmis"]==='1'&&$task["plannedfinish"]!==''&&$task["plannedfinish"]!==null) {
                        $boxes[$index]["colSpan"]=$boxcount;
                    } else {
                        $boxes[$index]["colSpan"]=$j-$index;
                    }
                    if(!isset($task["groupedtask"])) {
                        $arr_clientdetail=explode("|", $task["clientdetail"]);
                        for($cj=0;$cj<count($arr_clientdetail);$cj++) {
                            $tcd=$arr_clientdetail[$cj];
                            $str_code=implode("", explode(".", $tcd));
                            equipment_render($records, $arr_data, $task, $str_client, $str_code, $j-$index, $boxcount);
                        }
                    } else {
                        for($i=0;$i<count($tasks);$i++) {
                            $_row_task=&$tasks[$i];
                            if(isset($_row_task["groupedtask"])&&$_row_task["groupedtask"]===$task["groupedtask"]) {
                                $_row_task["__start__"]=$task["__start__"];
                                $arr_clientdetail=explode("|", $_row_task["clientdetail"]);
                                for($cj=0;$cj<count($arr_clientdetail);$cj++) {
                                    $tcd=$arr_clientdetail[$cj];
                                    $str_code=implode("", explode(".", $tcd));
                                    equipment_render($records, $arr_data, $_row_task, $str_client, $str_code, $j-$index, $boxcount);
                                }
                            }
                        }
                    }
                    return;
                }
            }
            if($counter>0) {
                //yarıda kalan varsa temizle
                unset($task["__start__"]);
                for($i=$index;$i<$j;$i++) {
                    $el=&$boxes[$i];
                    $pos_work = strpos($el["cls"], "work");
                    if($pos_work!==false) {
                        $el_opcount=&$arr_data["_timeline"]["opcount"][$el["id"]];
                        $el_opcount["title"]=intval($el_opcount["title"])-1;
                        $el["cls"]=str_replace("work", "free", $elc["cls"]);
                        //?$arr_data[$str_client]["_timeline"][$i]["title"]='';
                    }
                }
            }
            if($j<count($boxes)) {
                return task_render($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $j+1);
            }
        }

        function task_placer(&$records, &$arr_data, &$tasks, $task, $str_client, $boxes, $opcount, $period, $index, $_idx_)
        {
            $boxcount=ceil($task["sure"]/$period);
            $counter=0;
            $j;
            for($j=$index;$j<count($boxes);$j++) {
                if($counter<$boxcount) {
                    $elc=$boxes[$j];
                    $__start__=$elc["id"];
                    $pos_offtime = strpos($elc["cls"], "offtime");
                    if($pos_offtime===false) {
                        $el_opcount=&$arr_data["_timeline"]["opcount"][$elc["id"]];
                        $pos_work = strpos($elc["cls"], "work");
                        if($pos_work===false&&intval($el_opcount["title"])<$opcount) {
                            if($counter<$boxcount) {
                                ++$counter;
                                if(!isset($task["__start__"])) {
                                    $task["__start__"]=$__start__;
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        if($j==$index) {
                            break;
                        }
                    }
                } else {
                    return array("client"=>$str_client,"__start__"=>$task["__start__"],"index"=>$index,"_idx_"=>$_idx_);
                }
            }
            if($j<count($boxes)) {
                return task_placer($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $j+1, $_idx_);
            }
        }

        function render_tasks(&$records, &$arr_data, &$tasks, $opcount, $period)
        {
            $tmp_alternate_client_tasks=array();
            $alternate_task_id=0;
            $_idx_=0;
            foreach($tasks as &$task) {
                if(count($tmp_alternate_client_tasks)>0&&(($task["is_alternate_client_available"]===false)||($task["is_alternate_client_available"]===true&&$task["id"]!==$alternate_task_id))) {
                    $_task=&$tasks[$tmp_alternate_client_tasks[0]["_idx_"]];
                    $_str_client=$tmp_alternate_client_tasks[0]["client"];
                    $_boxes=&$arr_data[$_str_client]["_timeline"];
                    $_idx=$tmp_alternate_client_tasks[0]["index"];
                    task_render($records, $arr_data, $tasks, $_task, $_str_client, $_boxes, $opcount, $period, $_idx);
                    //görevi en uygun yere yerleştir
                    $alternate_task_id=0;
                    $tmp_alternate_client_tasks=array();
                }
                if(!isset($task["__start__"])) {
                    $idx=0;
                    $pre_task=false;
                    if($task["onkosul"]!=='0'&&$task["onkosul"]!==0) {
                        $pre_task=control_pre_task($tasks, $task["onkosul"]);
                    }
                    //@todo: birden fazla istasyonda yapılabilme durumunda, hangisinde daha erkene yerleştirilebiliyor ise ona yerleşim yapılmalı
                    $str_client=implode("", explode(".", $task["client"]));
                    $arr_clientdetail=explode("|", $task["clientdetail"]);
                    $task["sure"]=$task["sure"]/count($arr_clientdetail);
                    if(isset($arr_data[$str_client])) {
                        $boxes=&$arr_data[$str_client]["_timeline"];
                        if (count($boxes)>0) {
                            if($pre_task!==false) {
                                for($j=0;$j<count($boxes);$j++) {
                                    $el=$boxes[$j];
                                    $__start__=$el["id"];
                                    if($pre_task["__start__"]===$__start__) {
                                        $idx=$j;
                                        break;
                                    }
                                }
                            }
                            if($task["is_alternate_client_available"]===true) {
                                if($alternate_task_id===0) {
                                    $alternate_task_id=$task["id"];
                                }
                                $tmp_arr=task_placer($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $idx, $_idx_);
                                if(count($tmp_alternate_client_tasks)>0) {
                                    if($tmp_alternate_client_tasks[0]["__start__"]>$tmp_arr["__start__"]) {
                                        $tmp_alternate_client_tasks[0]=$tmp_arr;
                                    }
                                } else {
                                    $tmp_alternate_client_tasks[]=$tmp_arr;
                                }
                            } else {
                                task_render($records, $arr_data, $tasks, $task, $str_client, $boxes, $opcount, $period, $idx);
                            }
                        }
                    }
                }
                $_idx_++;
            }
            if(count($tmp_alternate_client_tasks)>0) {
                //görevi en uygun yere yerleştir
                $alternate_task_id=0;
                $tmp_alternate_client_tasks=array();
            }
        }
        render_tasks($records, $arr_data, $tasks, $opcount, $period);
        $records["render_opcount"]=$arr_data["_timeline"]["opcount"];

        $time_end = microtime();
        $duration=microtime_diff($time_start, $time_end);
        $records["duration"]=$duration;
        //$records["_arr_data"]=$arr_data;
        //$records["_tasks"]=$tasks;
        //echo json_encode(array("event"=>"done","duration"=>$duration,"data"=>$arr_data,"tasks"=>$tasks));

        return new JsonResponse(array("totalProperty"=>count($records),"records"=>$records));
    }
}
