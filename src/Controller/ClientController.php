<?php

namespace App\Controller;

use App\Entity\Client;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:Client';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/Client/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Client-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        //$cbd=true;
        if ($cbd === true) {
            return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
            /*$user = $this->getUser();
            $userId = $user->getId();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $qb_clients = $em->createQueryBuilder();
                $qb_clients = $qb_clients->select('c.id')
                    ->from('App:Client', 'c')
                    ->where('c.recordId=:recordId')
                    ->setParameters(array( 'recordId' => $entity->getRecordId()));
                    //->where('c.code=:code or c.uuid=:uuid or c.ip=:ip or c.recordId=:recordId')
                    //->setParameters(array('code' => $entity->getCode(), 'uuid' => $entity->getUuid(), 'ip' => $entity->getIp(), 'recordId' => $entity->getRecordId()));
                $clients=$qb_clients->getQuery()->getArrayResult();
                if(count($clients)!==0){
                    foreach($clients as $rowd){
                        $entity_c_del = $this->getDoctrine()
                            ->getRepository('App:Client')
                            ->find($rowd['id']);
                        $entity_c_del->setDeleteuserId($userId);
                        $em->persist($entity_c_del);
                        $em->flush();
                        $em->remove($entity_c_del);
                        $em->flush();
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                $conn->rollback();

                // Foreign key violation mesajı mı?
                $msg = $e->getMessage();
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }*/
        } else {
            return $cbd;
        }
        //return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new Client();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('c.id,c.code,c.description,c.uuid,c.isactive'
                .',c.ganttorder,c.clientweight,c.connected,c.status,c.statustime'
                .',c.info,c.watchorder,c.materialpreparation,c.version'
                .',c.ip,c.pincode,c.workflow,c.computername')
                ->from('App:Client', 'c')
                ->where('c.deleteuserId is null')
                ->orderBy('c.code', 'ASC');
        $queries['Client'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT c.* 
                FROM clients c 
                where  c.workflow<>'Gölge Cihaz'
                ORDER BY c.code ASC";
        $queries['Client'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/Client/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Client-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    /**
     * @Route(path="/Client/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Client-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    /**
     * @Route(path="/Client", name="Client-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/Client.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Client/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Client-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Client', $id);

            return $this->render('Modules/Client.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Client/{id}", requirements={"id": "\d+"}, name="Client-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Client', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Client/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="Client-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Client/api/ar/{client}/{event}", name="Client-api-ar", options={"expose"=true}, methods={"GET"})
     */
    public function apiAction(Request $request, $_locale, $client, $event)
    {
        //https://192.168.1.40/kaynak3/tr/Client/api/ar/03.06.0002/holo_start
        //https://192.168.1.40/kaynak3/tr/Client/api/ar/03.06.0002/holo_finish
        //hololens takip başlangıç-bitiş için eklendi
        $importpathnode=($this->_container==null?$this->container:$this->_container)->getParameter('verimotRT_path');
        $t=round(microtime(true)*1000);
        $data='{"api":true,"event":"'.$event.'","client":"'.$client.'"}';
        $exportFileName = $importpathnode."_import\api_call_$t.json";
        $exportFile = fopen( $exportFileName, 'w' );
        fwrite($exportFile, $data); 
        fclose( $exportFile );
        $mtitle = ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.success_message_title', array(), 'KaitekFrameworkBundle');
        $message = ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.success_message', array(), 'KaitekFrameworkBundle');
        $d2=new \DateTime();
        $d2f=$d2->format("Y-m-d H:i:s");
        $content = array(
            'title' => $mtitle,
            'success' => true,
            'message' => $message,
            'time' => $d2f,
            'hostname' => $_SERVER['HTTP_HOST'],
            'port' => 5672,
            'username' => 'admin',
            'password' => 'q32wxcx',
            'exchange' => 'hololens',
            'queue' => $client,
            'file' => ''
        );
        return new JsonResponse($content, 200);
    }
}
