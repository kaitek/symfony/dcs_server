<?php

namespace App\Controller;

use App\Entity\TaskCase;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CaseDeliverController extends ControllerBase implements BaseAuditControllerInterface
{
    public const ENTITY = 'App:CaseDeliver';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/CaseDeliver/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="CaseDeliver-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return;
    }

    /**
     * @Route(path="/CaseDeliver", name="CaseDeliver-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $_rpp = ($this->_container==null ? $this->container : $this->_container)->getParameter('kaitek_framework.recordsperpage');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => $this->prepData(1, 10000),
                'extras' => array()
            );

            return $this->render('Modules/CaseDeliver.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/CaseDeliver/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="CaseDeliver-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->prepData($pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    private function prepData($pg, $lm)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="SELECT erprefnumber,opname,client,cast(packcapacity as int) packcapacity,casename 
        from task_cases 
        where movementdetail is null and casenumber='-' 
        order by erprefnumber,opname,start,id";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records=$stmt->fetchAll();
        return array('CaseDeliver'=>array('totalProperty'=>count($records),"records"=>$records));
    }

}
