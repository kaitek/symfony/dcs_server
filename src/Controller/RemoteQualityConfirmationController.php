<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RemoteQualityConfirmationController extends ControllerBase implements BaseAuditControllerInterface
{
    public const ENTITY = 'App:ClientLostDetail';
    public $fieldname_opname;
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->fieldname_opname='opname';
        $this->_queryType=self::QUERY_TYPE_SQL;
    }
    public function getQBQuery()
    {
        return array();
    }
    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT * FROM (select min(cld.id)id,min(cld.version)\"version\",cld.client,cld.day,cld.jobrotation
            ,cld.start,cld.start,cast(CURRENT_TIMESTAMP-cld.start as varchar(8)) duration,string_agg(cld.erprefnumber, ',' order by cld.erprefnumber)erprefnumber
            ,string_agg(cld.".$this->fieldname_opname.", ',' order by cld.".$this->fieldname_opname.") opname
        from client_lost_details cld
        join (SELECT client from client_params where name in ('process_remoteQualityConfirmation','remoteQualityConfirmation') and value='true' group by client) cp on cp.client=cld.client
        where cld.finish is null and cld.type='l_c_t' and cld.day>CURRENT_DATE - interval '3 day' and cld.losttype='KALİTE ONAY' @@where@@
        group  by cld.client,cld.day,cld.jobrotation,cld.start
        order by cld.start)cld";
        $queries['RemoteQualityConfirmation'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }
    /**
     * @Route(path="/RemoteQualityConfirmation/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="RemoteQualityConfirmation-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $uri = strtolower($request->getBaseUrl());
        if (($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')) {
            $this->fieldname_opname='opdescription';
        }
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
        if ($cbu===true) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $data = json_decode($request->getContent());
                $description=(isset($data->qualityrejectiontype) ? ($data->qualityrejectiontype.' ') : '').(isset($data->description) ? $data->description : '');
                $_data=array("employee"=>$data->employee,"type"=>(isset($data->qualityrejectiontype) ? ($data->qualityrejectiontype) : ''),"description"=>substr($description, 0, 250));
                //$t = microtime(true);
                //$micro = sprintf("%06d",($t - floor($t)) * 1000000);
                //$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
                $d = new \DateTime();
                $_d=$d->format("Y-m-d H:i:s.u");
                //$conn->insert('_sync_messages', array('ip'=>($data->client.'_server'),'uuid' => null, 'ack' => 'true','queued'=>0, 'type' => 'remote_quality_'.($data->type=='Onay' ? 'confirm' : 'rejection'), 'data' => json_encode($_data), '"createdAt"' => $_d, '"updatedAt"' => $_d));
                //if ($_SERVER['HTTP_HOST']=="192.168.1.10"||$_SERVER['HTTP_HOST']=="192.168.1.40"||$_SERVER['HTTP_HOST']=="127.0.0.1") {
                $conn->insert('__sync_messages', array('queuename'=>($data->client.'_server'), 'priority'=>999, 'messagetype' => 'remote_quality_'.($data->type=='Onay' ? 'confirm' : 'rejection'), 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data),'info'=>json_encode(array("tableName"=>"")) ));
                //}
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/RemoteQualityConfirmation", name="RemoteQualityConfirmation-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $quality_rejection_types = $this->getComboValues($request, $_locale, 1, 1000, 'quality_rejection_types');
            $employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees', 'code', "concat(code,'-',name)", '', ' and isquality=true and finish is null ');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['job_rotations']=json_decode($job_rotations->getContent())->records;
            $data['extras']['quality_rejection_types']=json_decode($quality_rejection_types->getContent())->records;
            $data['extras']['employees']=json_decode($employees->getContent())->records;
            return $this->render('Modules/RemoteQualityConfirmation.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/RemoteQualityConfirmation/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="RemoteQualityConfirmation-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
