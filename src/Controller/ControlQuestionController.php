<?php

namespace App\Controller;

use App\Entity\ControlQuestion;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControlQuestionController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:ControlQuestion';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/ControlQuestion/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ControlQuestion-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
             ->getRepository(self::ENTITY)
             ->find($id);
        //return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        if ($cbd === true) {
            $tmpPath=$entity->getPath();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user = $this->getUser();
            $userId = $user->getId();
            $entity->setDeleteuserId($userId);
            $em->persist($entity);
            $em->flush();
            $em->remove($entity);
            $em->flush();
            $dir_documents=$this->getParameter('documents_base_directory');
            if ($tmpPath!=='') {
                $this->delete_files($dir_documents."Controlquestions/".$tmpPath);
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbd;
        }
    }

    /**
     * @Route(path="/ControlQuestion/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="ControlQuestion-getComboValues", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesControlQuestion(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = '')
    {
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val, " and finish is null ");
    }

    public function getNewEntity()
    {
        return new ControlQuestion();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('cq')
                ->from('App:ControlQuestion', 'cq')
                ->where('cq.deleteuserId is null')
                ->orderBy('cq.type', 'ASC')
                ->addOrderBy('cq.opname', 'ASC')
                ->addOrderBy('cq.question', 'ASC');
        $queries['ControlQuestion'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ControlQuestion/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ControlQuestion-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ControlQuestion/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ControlQuestion-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ControlQuestion", name="ControlQuestion-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['data']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/ControlQuestion.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ControlQuestion/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ControlQuestion-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ControlQuestion', $id);

            return $this->render('Modules/ControlQuestion.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ControlQuestion/{id}", requirements={"id": "\d+"}, name="ControlQuestion-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ControlQuestion', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ControlQuestion/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ControlQuestion-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ControlQuestion/upload", name="ControlQuestion-upload", options={"expose"=true}, methods={"POST"})
     */
    public function uploadAction(Request $request, $_locale)
    {
        $arr_files = array();
        $formData = $request->get("formData");
        if ($formData!=="") {
            $formData=json_decode($formData);
        }
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //if($formData->opname!==$formData->opcode){
        $arr_opname=explode('-', $formData->opname);
        if (count($arr_opname)>1) {
            $formData->opnumber=array_pop($arr_opname);
            $formData->opcode=implode('-', $arr_opname);
            //}else{
        //    return $this->msgError("Operasyon adı girilmemiş");
        }
        //}
        $dir_documents=$this->getParameter('documents_base_directory');
        if (!is_dir($dir_documents."Controlquestions/".$formData->type)) {
            mkdir($dir_documents."Controlquestions/".$formData->type, 0777, true);
        }
        if (isset($formData->opcode)) {
            if (!is_dir($dir_documents."Controlquestions/".$formData->type."/".$formData->opcode)) {
                mkdir($dir_documents."Controlquestions/".$formData->type."/".$formData->opcode, 0777, true);
            }
            if (!is_dir($dir_documents."Controlquestions/".$formData->type."/".$formData->opcode."/".$formData->opname)) {
                mkdir($dir_documents."Controlquestions/".$formData->type."/".$formData->opcode."/".$formData->opname, 0777, true);
            }
        }
        $user = $this->getUser();
        $userId = $user->getId();
        //if(!isset($formData->lastupdate)||$formData->lastupdate===''||$formData->lastupdate===null){
        //    $formData->lastupdate=date("Y-m-d H:i:s");
        //}
        //if(!isset($formData->currentversion)||$formData->currentversion===''||$formData->currentversion===null){
        //    $formData->currentversion=1;
        //}
        //if(!isset($formData->description)||$formData->description===''||$formData->description===null){
        //    $formData->description='';
        //}
        if (isset($formData->opcode)) {
            $file = $request->files->get('file1');
            if (empty($file)) {
                if (!isset($formData->id)) {
                    return $this->msgError("Yüklenecek dosya seçilmemiş");
                }
            } else {
                $file=$file[0];
                $filename = $file->getClientOriginalName();

                $path_destination=$dir_documents."Controlquestions/".$formData->type."/".$formData->opcode."/".$formData->opname."/";
                $formData->path=$formData->type."/".$formData->opcode."/".$formData->opname."/".$filename;
                $file->move($path_destination, $filename);
            }
        }
        $conn->beginTransaction();
        try {
            //güncelleme yapılıyor ise, eski kaydı bul, resim dosyasını sil, tablodaki kaydı güncelle
            if (isset($formData->id)&&$formData->id!=='') {
                $repo = $this->getDoctrine()->getRepository(self::ENTITY);
                $entity=$repo->findOneBy(array('id'=>$formData->id));
                if ($entity!==null) {
                    $prefile=$entity->getPath();
                    if ($prefile!==null&&$formData->path!==$prefile&&$formData->path!=='_isFile') {
                        unlink($dir_documents."Controlquestions/".$prefile);
                    }
                }
                if ($formData->path==='_isFile') {
                    $formData->path=null;
                }
                if (!empty($file)) {
                    $entity->setPath($formData->path);
                } else {
                    $formData->path=$entity->getPath();
                    if ($formData->path==='_isFile') {
                        $formData->path=null;
                    }
                }
                $entity->setUpdateuserId($userId);
                $entity->setOpname($formData->opname);
                $entity->setQuestion($formData->question);
                $entity->setAnswertype($formData->answertype);
                $entity->setValuerequire($formData->valuerequire);
                $entity->setValuemin($formData->valuemin);
                $entity->setValuemax($formData->valuemax);
                $entity->setDocumentnumber($formData->documentnumber);
                $entity->setClients($formData->clients);
                $entity->setPath($formData->path);
                $em->persist($entity);
                $em->flush();
                $em->clear();
            } else {
                if ($formData->path==='_isFile') {
                    $formData->path=null;
                }
                //yeni kayıt ise doğrudan kayıt aç
                $entity = new ControlQuestion();
                $entity->setCreateuserId($userId);
                //$entity->setUpdateuserId($userId);
                $entity->setType($formData->type);
                $entity->setOpname($formData->opname);
                $entity->setQuestion($formData->question);
                $entity->setAnswertype($formData->answertype);
                $entity->setValuerequire($formData->valuerequire);
                $entity->setValuemin($formData->valuemin);
                $entity->setValuemax($formData->valuemax);
                $entity->setDocumentnumber($formData->documentnumber);
                $entity->setClients($formData->clients);
                $entity->setPath($formData->path);
                $em->persist($entity);
                $em->flush();
                $formData->id = $entity->getId();
                $em->clear();
            }
            $conn->commit();
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            $conn->rollback();
            //throw $e;
            return $this->msgError($e->getMessage());
        }
        return new JsonResponse($formData, 200);
    }
}
