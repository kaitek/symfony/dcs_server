<?php

namespace App\Controller;

use App\Entity\Unit;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UnitController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:Unit';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/Unit/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Unit-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new Unit();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('u.id,u.code,u.version')
                ->from('App:Unit', 'u')
                ->where('u.deleteuserId is null')
                ->orderBy('u.code', 'ASC');
        $queries['Unit'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/Unit/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Unit-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Unit/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Unit-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Unit", name="Unit-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/Unit.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Unit/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Unit-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Unit', $id);

            return $this->render('Modules/Unit.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Unit/{id}", requirements={"id": "\d+"}, name="Unit-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Unit', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Unit/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="Unit-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
