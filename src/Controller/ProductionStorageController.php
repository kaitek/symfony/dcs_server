<?php
namespace App\Controller;

use App\Entity\ProductionStorage;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProductionStorageController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:ProductionStorage';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/ProductionStorage/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ProductionStorage-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity() {
        return new ProductionStorage();
    }

    public function getQBQuery() {
        
        return array();
    }
    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT ps.*
            ,cast(start as varchar(10)) sdate,substr(cast(start as varchar(20)), 12, 5) stime
            ,cast(finish as varchar(10)) fdate,substr(cast(finish as varchar(20)), 12, 5) ftime
        FROM production_storages ps 
        WHERE 1=1 @@where@@ 
        ORDER BY ps.finish,ps.start,ps.erprefnumber,ps.code,ps.number,ps.stockcode";
        $queries['ProductionStorage'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/ProductionStorage/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ProductionStorage-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
        //$content = $request->getContent();
        //$this->_requestData = json_decode($request->getContent());
        //if($this->_requestData->sdate==null||$this->_requestData->sdate==''||$this->_requestData->stime==null||$this->_requestData->stime==''){
        //    $this->_requestData->start=$this->_requestData->sdate.' '.$this->_requestData->stime.':00';
        //    unset($this->_requestData->sdate);
        //    unset($this->_requestData->stime);
        //}
        //if($this->_requestData->fdate==null||$this->_requestData->fdate==''||$this->_requestData->ftime==null||$this->_requestData->ftime==''){
        //    $this->_requestData->finish=$this->_requestData->fdate.' '.$this->_requestData->ftime.':00';
        //    unset($this->_requestData->fdate);
        //    unset($this->_requestData->ftime);
        //}
        //if(isset($this->_requestData->startday)){
        //    $this->_requestData->finishday=$this->_requestData->startday;
        //}
        //return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ProductionStorage/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ProductionStorage-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $content = $request->getContent();
        $this->_requestData = json_decode($request->getContent());
        if($this->_requestData->sdate!==null&&$this->_requestData->sdate!==''&&$this->_requestData->stime!==null&&$this->_requestData->stime!==''){
            $this->_requestData->start=$this->_requestData->sdate.' '.$this->_requestData->stime.':00';
            unset($this->_requestData->sdate);
            unset($this->_requestData->stime);
        }
        if($this->_requestData->fdate!==null&&$this->_requestData->fdate!==''&&$this->_requestData->ftime!==null&&$this->_requestData->ftime!==''){
            $this->_requestData->finish=$this->_requestData->fdate.' '.$this->_requestData->ftime.':00';
            unset($this->_requestData->fdate);
            unset($this->_requestData->ftime);
        }
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ProductionStorage", name="ProductionStorage-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/ProductionStorage.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ProductionStorage/{id}", requirements={"id": "\d+"}, name="ProductionStorage-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ProductionStorage', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ProductionStorage/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ProductionStorage-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    function errinsert($pos,$msg,$code=100){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //$conn->beginTransaction();
        try{
            //$sql = "select * from _log_exception where path=:path and message=:message and time>current_timestamp - interval '1' hour";
            //$stmt = $conn->prepare($sql);
            //$stmt->bindValue('path', $pos);
            //$stmt->bindValue('message', $msg);
            //$stmt->execute();
            //$records = $stmt->fetchAll();
            //if(count($records)==0){
            $sql= "insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,current_timestamp)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('code', $code);
            $stmt->bindValue('file', '');
            $stmt->bindValue('line', 0);
            $stmt->bindValue('message', $msg);
            $stmt->bindValue('method', 'MH');
            $stmt->bindValue('path', $pos);
            $stmt->bindValue('sessionid', 0);
            $stmt->execute();
            //}
            //$conn->commit();
        }catch (Exception $e) {
            echo $e->getMessage();
        }
        return;
    }

}