<?php
/*
 * kayıt girişi toplu yapılacak
 * silme için gridde her kaydın yanındaki check seçilerek toplu silme yapılacak
 *
 */

namespace App\Controller;

use App\Entity\JobRotationEmployee;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobRotationEmployeeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:JobRotationEmployee';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/JobRotationEmployee/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="JobRotationEmployee-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new JobRotationEmployee();
    }

    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT 0 del,jre.id,jre.day,jre.week,jre.jobrotation "
                . " ,jre.jobrotationteam,jre.employee,jre.isovertime "
                . " ,jre.beginval,jre.endval,jre.worktime,jre.breaktime "
                . " ,e.code,e.name "
                . " ,jre.version "
                . " FROM job_rotation_employees jre "
                . " LEFT JOIN employees e on e.code=jre.employee"
                . " WHERE 1=1 and e.finish is null and jre.day>CURRENT_DATE - interval '180 days' @@where@@ "
                . " ORDER BY jre.day DESC";
        $queries['JobRotationEmployee'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('jre.id,jre.day,jre.week,jre.jobrotation'
                . ',jre.jobrotationteam,jre.employee,jre.isovertime'
                . ',jre.beginval,jre.endval,jre.worktime,jre.breaktime'
                . ',jre.version')
                ->from('App:JobRotationEmployee', 'jre')
                ->where('jre.deleteuserId is null')
                ->orderBy('jre.day', 'DESC');
        $queries['JobRotationEmployee'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/JobRotationEmployee/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="JobRotationEmployee-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba=$this->checkBeforeAdd($request);
        if($cba===true) {
            $this->_requestData = json_decode($request->getContent());
            if($this->_requestData == null) {
                $this->_requestData = $request->request->all();
            }
            $this->_requestData = $this->clearLookup($this->_requestData);
            $d_s=new \DateTime($this->_requestData->dateStart);
            $d_e=new \DateTime($this->_requestData->dateEnd);
            $interval = new \DateInterval('P1D');
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                while($d_s<=$d_e) {
                    foreach(explode(',', $this->_requestData->employees) as $employee) {
                        $validator = ($this->_container==null ? $this->container : $this->_container)->get('validator');
                        $entity = $this->getNewEntity();
                        $entity->setDay($d_s);
                        $entity->setWeek($this->_requestData->week);
                        $entity->setJobrotation($this->_requestData->jobrotation);
                        $entity->setJobrotationteam($this->_requestData->jobrotationteam);
                        $entity->setEmployee($employee);
                        $entity->setIsovertime($this->is_true($this->_requestData->isovertime));
                        $entity->setBeginval($this->_requestData->beginval.":00");
                        $entity->setEndval($this->_requestData->endval.":59");
                        $entity->setWorktime($this->_requestData->worktime);
                        $entity->setBreaktime($this->_requestData->breaktime);
                        $entity->setCreateuserId($userId);
                        $entity->setUpdateuserId($userId);
                        $errors = $this->getValidateMessage($validator->validate($entity));
                        if ($errors!==false) {
                            return $errors;
                        }
                        $sql="select * from job_rotation_employees where day=:day and jobrotation=:jobrotation and employee=:employee";
                        $stmt = $conn->prepare($sql);
                        $stmt->bindValue('day', $d_s->format('Y-m-d'));
                        $stmt->bindValue('jobrotation', $this->_requestData->jobrotation);
                        $stmt->bindValue('employee', $employee);
                        $stmt->execute();
                        $records = $stmt->fetchAll();
                        if(count($records)>0) {
                            throw new \Error($employee.' sicili için '.$d_s->format('Y-m-d').' günü '.$this->_requestData->jobrotation.' vardiyasına tanım daha önce yapılmış.<br>İşlem yapılamıyor');
                        }
                        $em->persist($entity);
                        $em->flush();
                    }
                    $d_s->add($interval);
                }
                $conn->commit();
            } catch (\Error $err) {
                $conn->rollback();
                return $this->msgError($err->getMessage());
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cba;
        }
        //return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/JobRotationEmployee/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="JobRotationEmployee-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/JobRotationEmployee", name="JobRotationEmployee-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $data['extras']['job_rotation_teams']=json_decode($job_rotation_teams->getContent())->records;
            $data['extras']['job_rotations']=json_decode($job_rotations->getContent())->records;
            return $this->render('Modules/JobRotationEmployee.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/JobRotationEmployee/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="JobRotationEmployee-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'JobRotationEmployee', $id);

            return $this->render('Modules/JobRotationEmployee.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/JobRotationEmployee/{id}", requirements={"id": "\d+"}, name="JobRotationEmployee-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'JobRotationEmployee', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/JobRotationEmployee/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="JobRotationEmployee-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
