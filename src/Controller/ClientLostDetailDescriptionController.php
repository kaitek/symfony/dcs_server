<?php
/*
 * kayıt girişi toplu yapılacak
 * silme için gridde her kaydın yanındaki check seçilerek toplu silme yapılacak
 *
 */

namespace App\Controller;

use App\Entity\ClientLostDetail;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientLostDetailDescriptionController extends DcsBaseController implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:ClientLostDetail';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/ClientLostDetailDescription/{pg}/{lm}/{id}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+"}, name="ClientLostDetailDescription-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v=0)
    {
        return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientLostDetailDescription.errdeleterecord', array(), 'ClientLostDetailDescription'));
    }

    public function getNewEntity()
    {
        return new ClientLostDetail();
    }

    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT 0 del,min(cld.id)id,cld.client,cld.day,cld.jobrotation,min(cld.tip)tip
            ,cld.start,cld.finish,cld.suredk,cld.start_d,cld.finish_d,cld.start_t,cld.finish_t,cld.losttype
            ,string_agg(distinct cld.employee,'|')employee,string_agg(distinct cld.opname,'|')opname
            ,string_agg(distinct cld.employees,'|')employees,string_agg(distinct cld.opnames,'|')opnames
            ,max(cld.descriptionlost)descriptionlost
            ,max(cld.version)\"version\"
        FROM (
            select cld.id,cld.client,cld.type,cld.day,cld.jobrotation
                ,case when cld.type in ('l_c','l_c_out') then 'İstasyon' else 'Operatör' end tip
                ,case when cld.employee is not null then concat(e.name) else null end employee
                ,case when cld.opname is not null then concat(cld.opname) else null end opname
                ,case when cld.employee is not null then concat(cld.employee,'~',e.name) else null end employees
                ,case when cld.opname is not null then concat(cld.erprefnumber,'~',cld.opname) else null end opnames
                ,cld.start,cld.finish
                ,ceil(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60)suredk
                ,cast(cld.start as varchar(10)) start_d,cast(cld.finish as varchar(10)) finish_d
                ,right(cast(cld.start as varchar(19)),8)start_t,right(cast(cld.finish as varchar(19)),8)finish_t
                ,case when cld.type='l_e' then 1 else 0 end issystem
                ,cld.losttype,coalesce(cld.descriptionlost,'')descriptionlost,cld.version
            from client_lost_details cld
            left join employees e on e.code=cld.employee
            where cld.type in ('l_c','l_c_out','l_e','l_e_out','l_c_t') and cld.finish is not null and e.finish is null 
        )cld
        where 1=1 @@where@@
        GROUP BY cld.client,cld.day,cld.jobrotation,cld.start,cld.finish,cld.suredk,cld.start_d,cld.finish_d,cld.start_t,cld.finish_t,cld.losttype
        ORDER BY cld.day DESC,cld.suredk desc";
        $queries['ClientLostDetailDescription'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('jre.id,jre.day,jre.week,jre.jobrotation'
                . ',jre.jobrotationteam,jre.employee,jre.isovertime'
                . ',jre.beginval,jre.endval,jre.worktime,jre.breaktime'
                . ',jre.version')
                ->from('App:ClientLostDetail', 'jre')
                ->where('jre.deleteuserId is null')
                ->orderBy('jre.day', 'DESC');
        $queries['ClientLostDetail'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ClientLostDetailDescription/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientLostDetailDescription-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientLostDetailDescription.errnewrecord', array(), 'ClientLostDetailDescription'));
    }

    /**
     * @Route(path="/ClientLostDetailDescription/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientLostDetailDescription-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v=0)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        if ($entity==null||!$entity) {
            return $this->msgError(
                ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.not_found', array(), 'KaitekFrameworkBundle'),
                401
            );
        }
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
        if ($cbu===true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $d_s=new \DateTime($content->start_d." ".$content->start_t);
            $d_f=new \DateTime($content->finish_d." ".$content->finish_t);
            if ($d_s>=$d_f) {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientLostDetail.errdatestartgreaterthanfinish', array(), 'ClientLostDetail'));
            }
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                //eski kayıtları güncelle
                $sql="update client_lost_details set descriptionlost=:descriptionlost where client=:client and start=:start and finish=:finish and losttype=:losttype ";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('descriptionlost', $content->descriptionlost);
                $stmt->bindValue('client', $content->client);
                $stmt->bindValue('start', $content->start);
                $stmt->bindValue('finish', $content->finish);
                $stmt->bindValue('losttype', $content->losttype);
                $stmt->execute();
                $records = $stmt->fetchAll();
                $conn->commit();
                // $this->efficiencyCalculate($content->day);
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
            //}
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/ClientLostDetailDescription", name="ClientLostDetailDescription-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY, 0);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $lost_types = $this->getComboValues($request, $_locale, 1, 1000, 'lost_types');
            $employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees', 'code', "concat(code,'-',name)", '', ' and finish is null ');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['job_rotations']=json_decode($job_rotations->getContent())->records;
            $data['extras']['lost_types']=json_decode($lost_types->getContent())->records;
            $data['extras']['employees']=json_decode($employees->getContent())->records;
            return $this->render('Modules/ClientLostDetailDescription.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientLostDetailDescription/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ClientLostDetailDescription-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ClientLostDetail', $id);

            return $this->render('Modules/ClientLostDetailDescription.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientLostDetailDescription/{id}", requirements={"id": "\d+"}, name="ClientLostDetailDescription-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ClientLostDetailDescription', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientLostDetailDescription/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientLostDetailDescription-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
