<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WatchController extends ControllerBase
{
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/Watch/getClientData", name="Watch-getClientData", options={"expose"=true}, methods={"POST"})
     */
    public function getClientDataAction(Request $request, $_locale)
    {
        $customer=($this->_container==null ? $this->container : $this->_container)->getParameter('mh_customer');
        $statusCode=200;
        $result = array("groups"=>array());
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="SELECT cgd_d.clientgroupcode,c.*
            ,case when exists (select id from client_params cp where cp.client=c.code and cp.name='materialPrepare' and cp.value='Otomatik') then true else false end cp_materialpreparation 
            ,case when exists (SELECT cpd.id 
                from client_production_details cpd 
                left join documents d on d.opname=replace(cpd.opname,'/','_')
                where cpd.day=CURRENT_DATE and cpd.finish is null and cpd.type='c_p' and d.id is not null and cpd.client=c.code) then false else ".(($customer=='ERMETAL'||$customer=='OSKIM') ? "false" : "true")." end sop_not_exists
        from client_group_details cgd_g
        join client_group_details cgd_d on cgd_g.client=cgd_d.clientgroupcode and cgd_d.clientgroup='watch'
        join clients c on c.code=cgd_d.client and c.uuid is not null and c.workflow<>'Gölge Cihaz'
        where cgd_g.clientgroup='watch_order'
        order by cgd_g.clientgroupcode,cgd_d.id";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records=$stmt->fetchAll();
        if (count($records)>0) {
            foreach ($records as $row) {
                if (!isset($result["groups"][$row["clientgroupcode"]])) {
                    $result["groups"][$row["clientgroupcode"]]=array("title"=>$row["clientgroupcode"],"items"=>array());
                }
                if (!isset($result["groups"][$row["clientgroupcode"]]["items"][$row["code"]])) {
                    $result["groups"][$row["clientgroupcode"]]["items"][$row["code"]]=$row;
                }
            }
        } else {
        }

        return new JsonResponse($result, $statusCode);
    }

    /**
     * @Route(path="/Watch/getWorkshopValues", name="Watch-getWorkshopValues", options={"expose"=true}, methods={"POST"})
     */
    public function getWorkshopValuesAction(Request $request, $_locale)
    {
        $statusCode=200;
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="select jr.code
            ,case when jr.beginval>jr.endval and (cast(CURRENT_TIME as varchar(5))>=jr.beginval or cast(CURRENT_TIME as varchar(5))<=jr.endval)
                then cast(CURRENT_DATE + INTERVAL '1 day' as varchar(10)) 
                else cast(current_date as varchar(10)) end as day
        from (
            SELECT * from job_rotations jr where jr.finish is null and beginval<endval and cast(CURRENT_TIME as varchar(5)) between beginval and endval
            union all
            SELECT * from job_rotations jr where jr.finish is null and beginval>endval and (cast(CURRENT_TIME as varchar(5))>beginval or cast(CURRENT_TIME as varchar(5))<=endval)
        )jr limit 1";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records=$stmt->fetchAll();
        if (count($records)>0) {
            $planlanan=0;
            $calisan=0;
            $bosta=0;
            $sql_emp_work="SELECT count(*)sayi from employees where day is not null";
            $stmt_emp_work = $conn->prepare($sql_emp_work);
            $stmt_emp_work->execute();
            $records_emp_work=$stmt_emp_work->fetchAll();
            $sql2="SELECT case when e.client is null then 0 else 1 end isworking,count(*)sayi
            from job_rotation_employees jre 
            left join employees e on e.code=jre.employee
            where jre.day=:day and jre.jobrotation=:jobrotation and e.finish is null
            group by case when e.client is null then 0 else 1 end";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->bindValue('day', $records[0]["day"]);
            $stmt2->bindValue('jobrotation', $records[0]["code"]);
            $stmt2->execute();
            $records2=$stmt2->fetchAll();
            if (count($records2)>0) {
                foreach ($records2 as $row) {
                    if ($row["isworking"]==0) {
                        $bosta=$row["sayi"];
                    } else {
                        $calisan=$row["sayi"];
                    }
                }
                if ($calisan<$records_emp_work[0]["sayi"]) {
                    $calisan=$records_emp_work[0]["sayi"];
                }
                $planlanan=$calisan+$bosta;
                $result['workshop'] = array(
                    'planlanan'=>$planlanan,
                    'calisan'=>$calisan,
                    'bosta'=>$bosta
                );
            }
            $sql3="SELECT cpd.client,cpd.erprefnumber,cl.status 
            from client_production_details cpd
            left join case_labels cl on cl.erprefnumber=cpd.erprefnumber
            where cpd.type='c_p' and cpd.day=:day and cpd.jobrotation=:jobrotation and cpd.finish is null and cl.status not in ('OPEN')";
            $stmt3 = $conn->prepare($sql3);
            $stmt3->bindValue('day', $records[0]["day"]);
            $stmt3->bindValue('jobrotation', $records[0]["code"]);
            $stmt3->execute();
            $records3=$stmt3->fetchAll();
            if (count($records3)>0) {
                $result['case_labels'] = $records3;
            }
            $sql_ca="SELECT x.client,x.erprefnumber,concat(x.client,'|',x.erprefnumber)pcheck,ca1.answer 
            from (
                SELECT cpd.client,cpd.erprefnumber,max(ca.id) caid
                from client_production_details cpd
                join control_answers ca on ca.type='SETUP-AYAR' and ca.client=cpd.client 
                    and ca.erprefnumber=cpd.erprefnumber and ca.question like '%POKA YOKE%' 
                where cpd.type='c_p' and cpd.day=:day and cpd.jobrotation=:jobrotation and cpd.finish is null 
                group by cpd.client,cpd.erprefnumber
            )x
            join control_answers ca1 on ca1.id=x.caid";
            $stmt_ca = $conn->prepare($sql_ca);
            $stmt_ca->bindValue('day', $records[0]["day"]);
            $stmt_ca->bindValue('jobrotation', $records[0]["code"]);
            $stmt_ca->execute();
            $records_ca=$stmt_ca->fetchAll();
            if (count($records_ca)>0) {
                $result['poka_yoke'] = $records_ca;
            }
        }
        return new JsonResponse($result, $statusCode);
    }

    /**
     * @Route(path="/Watch/getSetups", name="Watch-getSetups", options={"expose"=true}, methods={"GET"})
     */
    public function getSetupsAction(Request $request, $_locale)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql_cm_2="select lists.client,lists.opname,lists.erprefnumber,lists.plannedstart,works.estimatedfinish 
            ,case when works.estimatedfinish is null and lists.plannedstart<CURRENT_TIMESTAMP then CURRENT_TIMESTAMP-lists.plannedstart else null end lagtime
            ,case when works.estimatedfinish is null and lists.plannedstart<CURRENT_TIMESTAMP then 'red' when works.estimatedfinish is not null and lists.plannedstart<CURRENT_TIMESTAMP then 'yellow' else '' end rowclass
        from (
            SELECT tl.client,string_agg(tl.opname,'|' ORDER BY tl.id)opname
                ,string_agg(tl.erprefnumber,'|' ORDER BY tl.id)erprefnumber,tl.plannedstart
            from task_lists tl
            where finish is null and start is null 
                and not exists(SELECT * from client_production_details cpd where cpd.type='c_p' and cpd.day>CURRENT_DATE-interval '30 days' and cpd.erprefnumber=tl.erprefnumber and cpd.opname=tl.opname)
                and tl.plannedstart<=CURRENT_TIMESTAMP+interval '30 minutes' and tl.plannedstart>CURRENT_TIMESTAMP-interval '24 hours'
            group by tl.client,tl.plannedstart)lists
        join (
            select cpd_w.client,cast(CURRENT_TIMESTAMP(0) + cast(tl_w.tpp*(tl_w.productcount-tl_w.productdonecount) as int)*interval '1' second as timestamp) estimatedfinish
            from task_lists tl_w
            join(
                SELECT cpd2.client,cpd2.day,cpd2.jobrotation,max(cpd2.tasklist) tasklist
                from client_production_details cpd2 
                join (
                    SELECT id,code,beginval,endval ,current_date,current_time
                        ,cast(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) as date) \"day\" 
                        ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start 
                        ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp +interval '1 second' jr_end 
                from job_rotations 	
                where (finish is null or finish<now())	
                    and (	
                        (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
                        or 
                        (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
                    )
                )sv on sv.day=cpd2.day
            where cpd2.type='c_p' and cpd2.finish is null
            GROUP BY cpd2.client,cpd2.day,cpd2.jobrotation)cpd_w on cpd_w.tasklist=tl_w.code
        )works on works.client=lists.client
        order by lists.plannedstart";
        $stmt_cm_2 = $conn->prepare($sql_cm_2);
        //if(intval($id)>0){
        //    $stmt_cm_2->bindValue('id', $id);
        //}
        $stmt_cm_2->execute();
        $respm = array(
            'title' => '',
            'success' => true,
            'data' => $stmt_cm_2->fetchAll()
        );
        return new JsonResponse($respm, 200);
    }
}
