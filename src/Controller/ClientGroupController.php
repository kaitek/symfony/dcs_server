<?php

namespace App\Controller;

use App\Entity\ClientGroup;
#use App\Entity\ClientGroupDetail;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientGroupController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:ClientGroup';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/ClientGroup/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientGroup-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new ClientGroup();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('cg.id,cg.code,cg.groupcode,cg.version')
                ->from('App:ClientGroup', 'cg')
                ->where('cg.deleteuserId is null')
                ->orderBy('cg.code', 'ASC')
                ->addOrderBy('cg.groupcode', 'ASC');
        $queries['ClientGroup'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ClientGroup/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientGroup-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ClientGroup/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientGroup-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ClientGroup", name="ClientGroup-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/ClientGroup.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientGroup/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ClientGroup-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ClientGroup', $id);

            return $this->render('Modules/ClientGroup.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientGroup/{id}", requirements={"id": "\d+"}, name="ClientGroup-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records=array();
            $records['ClientGroup']= $this->getRecordById($this, $request, 'ClientGroup', $id);
            $code=$records['ClientGroup']['ClientGroup']['records'][0]['code'];
            $clientgroupcode=$records['ClientGroup']['ClientGroup']['records'][0]['groupcode'];
            $em = $this->getDoctrine()->getManager();
            $qb = $em->createQueryBuilder();
            $qb1 = $qb->select('cgd.id,cgd.clientgroup,cgd.clientgroupcode,cgd.client,cgd.version')
                ->from('App:ClientGroupDetail', 'cgd')
                ->where('cgd.deleteuserId is null')
                ->orderBy('cgd.clientgroup', 'ASC')
                ->addOrderBy('cgd.clientgroupcode', 'ASC');            
            $records['ClientGroupDetail']=$this->getRecordByIdDQLQB($this,$request,$qb1,array(array('cgd.clientgroup'=>$code),array('cgd.clientgroupcode'=>$clientgroupcode)));
            //$records = $this->getRecordByFieldName($this, $request, 'clientgroup', array(array('cg.clientgroup'=>$code)));
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientGroup/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientGroup-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
