<?php

namespace App\Controller;

use App\Entity\TaskPlan;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class TaskPlanController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:TaskPlan';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);

        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/TaskPlan/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskPlan-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new TaskPlan();
    }

    public function getQBQuery()
    {
        return array();
        //$queries = array();
        ///** @var EntityManager $em */
        //$em = $this->getDoctrine()->getManager();
        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('tp')
        //        ->from(self::ENTITY, 'tp')
        //        ->where(" tp.idx='' or (tp.idx<>'' and tp.status='P') or tp.issendbyforce=true " )
        //        ->orderBy('tp.opname', 'ASC');
        //$queries['TaskPlan'] = array('qb' => $qb, 'getAll' => true);

        //return $queries;
    }

    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT tp.* 
            FROM task_plans tp 
            WHERE tp.idx='' /*or (tp.idx<>'' and tp.status='P')*/ or tp.issendbyforce=true @@where@@ 
            ORDER BY case when tp.idx='' then 0 else 1 end,tp.client,tp.opname,tp.deadline ASC";
        $queries['TaskPlan'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/TaskPlan/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskPlan-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        //$customer=($this->_container==null?$this->container:$this->_container)->getParameter('mh_customer');
        $this->_requestData = json_decode($request->getContent());
        if($this->_requestData == null){
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb_task = $em->createQueryBuilder();
        $qb_task = $qb_task->select('e.id')
            ->from('App:TaskPlan', 'e')
            ->where('e.code=:code and e.finish is null')
            ->setParameters(array('code' => $this->_requestData->code));
        $tl=$qb_task->getQuery()->getArrayResult();
        if(count($tl)===0){
            return $this->recordAdd($request, $_locale, $pg, $lm);
        }else{
            return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskPlan.errRecordAdd', array(), 'TaskPlan') );
        }
    }

    /**
     * @Route(path="/TaskPlan/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskPlan-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v){
        if(!isset($this->_requestData))
            $this->_requestData = json_decode($request->getContent());
        $customer=($this->_container==null?$this->container:$this->_container)->getParameter('mh_customer');
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);
        if($entity->getSecret()===null||($entity->getSecret()===''&&$customer!=='SAHINCE')){
            $v=substr(hash('sha256', $entity->getCode().time().rand(100,10000)),0,10);
            $this->_requestData->secret=$v;
        }
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/TaskPlan", name="TaskPlan-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/TaskPlan.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskPlan/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="TaskPlan-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'TaskPlan', $id);

            return $this->render('Modules/TaskPlan.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskPlan/{id}", requirements={"id": "\d+"}, name="TaskPlan-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'TaskPlan', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskPlan/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskPlan-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskPlan/updateIdx/{pg}/{lm}/", requirements={"pg": "\d+","lm": "\d+"}, name="TaskPlan-updateIdx", options={"expose"=true}, methods={"PUT"})
     */
    public function updateIdxAction(Request $request, $_locale, $pg, $lm){
        $content = json_decode($request->getContent());
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        foreach($content->recordIds1 as $row){
            $sql="update task_plans set idx=:idx6,issendbyforce=true where id=:id";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('idx6', $this->gen_uuid());
            $stmt->bindValue('id', $row);
            $stmt->execute();
        }
        //foreach($content->recordIds0 as $row){
        //    $sql="update task_plans set idx='',issendbyforce=null where id=:id";
        //    $stmt = $conn->prepare($sql);
        //    $stmt->bindValue('id', $row);
        //    $stmt->execute();
        //}
        return $this->msgSuccess();
        //$records = $this->getAllRecords($this, $request, $pg, $lm);
        //return new JsonResponse($records);
    }

    /**
     * @Route(path="/TaskPlan/getPlanEmployee", name="TaskPlan-getPlanEmployee", options={"expose"=true}, methods={"POST"})
     */
    public function getPlanEmployeeAction(Request $request, $_locale)
    {
        $parameters=json_decode($request->getContent());
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="select tpe_all.employee,tpe_all.name,tpe_all.client,tpe_all.opname,tpe_all.erprefnumber,tpe_all.plannedstart,tpe_all.plannedfinish,works.estimatedfinish
            ,case when works.estimatedfinish>tpe_all.plannedstart then 'danger' else '' end rowclass
        from (
            SELECT tpe.employee,e.name,tpe.client
                ,string_agg(tpe.opname,'|' ORDER BY tpe.id) opname
                ,string_agg(tpe.erprefnumber,'|' ORDER BY tpe.id) erprefnumber
                ,tpe.plannedstart,tpe.plannedfinish
            from task_plan_employees tpe 
            join (
                SELECT id,code,beginval,endval ,current_date,current_time
                ,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) \"day\" 
                ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start 
                ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp +interval '1 second' jr_end 
            from job_rotations 	
            where (finish is null or finish<now())	
                and (	
                    (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
                    or 
                    (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
                )
            )sv on tpe.plannedstart>=sv.jr_start and tpe.plannedfinish<=sv.jr_end
            join employees e on e.code=tpe.employee
            where 1=1 and tpe.employee=:employee
            GROUP BY tpe.employee,e.name,tpe.client,tpe.plannedstart,tpe.plannedfinish
        )tpe_all
        join (
            select cpd_w.client,cast(CURRENT_TIMESTAMP(0) + cast(tl_w.tpp*(tl_w.productcount-tl_w.productdonecount) as int)*interval '1' second as timestamp) estimatedfinish
            from task_lists tl_w
            join(
                SELECT cpd2.client,cpd2.day,cpd2.jobrotation,max(cpd2.tasklist) tasklist
                from client_production_details cpd2 
                join (
                    SELECT id,code,beginval,endval ,current_date,current_time
                        ,cast(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) as date) \"day\" 
                        ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start 
                        ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp +interval '1 second' jr_end 
                from job_rotations 	
                where (finish is null or finish<now())	
                    and (	
                        (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
                        or 
                        (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
                    )
                )sv on sv.day=cpd2.day
            where cpd2.type='c_p' and cpd2.finish is null
            GROUP BY cpd2.client,cpd2.day,cpd2.jobrotation)cpd_w on cpd_w.tasklist=tl_w.code
        )works on works.client=tpe_all.client";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('employee', $parameters->employee);
        $stmt->execute();
        $records=$stmt->fetchAll();
        return new JsonResponse(array("totalProperty"=>count($records),"records"=>$records));
    }

}
