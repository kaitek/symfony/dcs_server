<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\CaseCall;

class QualityCaseCallController extends ControllerBase implements BaseAuditControllerInterface
{
    public const ENTITY = 'App:CaseCall';
    public $fieldname_opname;
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->fieldname_opname='opname';
        $this->_queryType=self::QUERY_TYPE_SQL;
    }
    public function getQBQuery()
    {

        return array();
    }
    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT coalesce(cc.id,cast(cpd.erprefnumber as int))id,coalesce(cc.version,1)\"version\",cpd.client,cpd.day,cpd.jobrotation,cpd.erprefnumber,cpd.opname,coalesce(cc.state,'')state
        FROM client_production_details cpd 
        JOIN (
            select id,code,beginval,endval,concat(current_date,' ',cast(current_time as varchar(8))) zaman,CURRENT_TIMESTAMP(0) ts
                ,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as date) \"day\"
                ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end
            from job_rotations 
            where (finish is null or finish<now())
                and (
                (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)
                or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )
            )
        )jr on jr.day=cpd.day and jr.code=cpd.jobrotation
        left join case_calls cc on cc.client=cpd.client and cc.day=cpd.day and cc.jobrotation=cpd.jobrotation and cc.erprefnumber=cpd.erprefnumber and cc.opname=cpd.opname and cc.actualfinishtime is null
        WHERE cpd.type='c_p' and cpd.finish is null 
            and exists(select * from task_cases where finish is null and casetype='O' and movementdetail is not null and erprefnumber=cpd.erprefnumber and opname=cpd.opname)
            @@where@@
        GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.erprefnumber,cpd.opname,cc.id,cc.version,cc.state
        ORDER BY case when coalesce(cc.state,'')='' then 1 else 0 end,cpd.opname";
        $queries['QualityCaseCall'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }
    /**
     * @Route(path="/QualityCaseCall/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="QualityCaseCall-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $uri = strtolower($request->getBaseUrl());
        if(($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149')) {
            $this->fieldname_opname='opdescription';
        }
        $user = $this->getUser();
        $userId = $user->getId();
        $content=json_decode($request->getContent());
        if($content->id===intval($content->erprefnumber)) {
            $id=null;
            $cba=$this->checkBeforeAdd($request);
            if($cba===true) {
                $entity = new CaseCall();
                $entity->setCreateuserId($userId);
                $entity->setClient($content->client);
                $entity->setDay(new \DateTime($content->day));
                $entity->setJobrotation($content->jobrotation);
                $entity->setErprefnumber($content->erprefnumber);
                $entity->setOpname($content->opname);
                $entity->setState("BEKLİYOR");
                $entity->setCalltime(new \Datetime());

                $validator = ($this->_container==null ? $this->container : $this->_container)->get('validator');
                $errors = $this->getValidateMessage($validator->validate($entity));
                if ($errors!==false) {
                    return $errors;
                }

                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();
                $conn->beginTransaction();
                try {
                    $em->persist($entity);
                    $em->flush();
                    $conn->commit();
                } catch (\Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
                if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                    return $this->showAllAction($request, $_locale, $pg, $lm);
                } else {
                    return $this->msgSuccess();
                }
            } else {
                return $cba;
            }
        } else {
            return $this->msgSuccess();
        }
    }

    /**
     * @Route(path="/QualityCaseCall", name="QualityCaseCall-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY, 10000);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            //$quality_rejection_types = $this->getComboValues($request, $_locale, 1, 1000, 'quality_rejection_types');
            //$employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees','code','name','',' and isquality=true ');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['job_rotations']=json_decode($job_rotations->getContent())->records;
            //$data['extras']['quality_rejection_types']=json_decode($quality_rejection_types->getContent())->records;
            //$data['extras']['employees']=json_decode($employees->getContent())->records;
            return $this->render('Modules/QualityCaseCall.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/QualityCaseCall/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="QualityCaseCall-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

}
