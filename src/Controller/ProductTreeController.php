<?php

namespace App\Controller;

use App\Entity\ProductTree;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProductTreeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:ProductTree';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/ProductTree/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ProductTree-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ProductTree/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="ProductTree-getComboValues-special", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesSpecial(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = '')
    {
        if($request->query->get('options')!=='') {
            $options=json_decode($request->query->get('options'));
            if($options->where==='M') {
                $_where=" and materialtype in ('M','YM') /*and parent is null*/ and finish is null ";
            }
            if($options->where==='O') {
                $_where=" and materialtype in ('O') and parent is not null and finish is null ";
            }
        }
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val, $_where);
    }

    public function getNewEntity()
    {
        return new ProductTree();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('pt.id,pt.parent,pt.code,pt.number,pt.description'
                . ',pt.materialtype,pt.name,pt.stockcode,pt.stockname,pt.tpp'
                . ',pt.quantity,pt.unit_quantity,pt.opcounter,pt.pack,pt.client'
                . ',pt.packcapacity,pt.unit_packcapacity,pt.oporder,pt.preptime'
                . ',pt.qualitycontrolrequire,pt.task,pt.feeder,pt.drainer'
                . ',pt.carrierfeeder,pt.carrierdrainer'
                . ',pt.goodsplanner,pt.productionmultiplier,pt.intervalmultiplier'
                . ',pt.locationsource,pt.locationdestination'
                . ',pt.empcount,pt.isdefault,pt.notificationtime,pt.lotcount'
                . ',pt.delivery,pt.start,pt.finish,pt.version')
                ->from('App:ProductTree', 'pt')
                ->where('pt.deleteuserId is null and pt.finish is null')
                ->orderBy('pt.code', 'ASC')
                ->addOrderBy('pt.number', 'ASC')
                ->addOrderBy('pt.materialtype', 'ASC')
                ->addOrderBy('pt.stockcode', 'ASC');
        $queries['ProductTree'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ProductTree/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ProductTree-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $content = $request->getContent();
        $this->_requestData = json_decode($request->getContent());
        if(!isset($this->_requestData->materialtype)||(isset($this->_requestData->materialtype)&&($this->_requestData->materialtype===''||$this->_requestData->materialtype===null))) {
            return $this->msgError(
                ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ProductTree.errMaterialTypeIsNotNull', array(), 'ProductTree'),
                401
            );
        }
        if($this->_requestData->materialtype=='O') {
            if(!isset($this->_requestData->number)||(isset($this->_requestData->number)&&($this->_requestData->number===''||$this->_requestData->number===null))) {
                return $this->msgError(
                    ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ProductTree.errOpnumberIsNotNull', array(), 'ProductTree'),
                    401
                );
            }
        }
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ProductTree/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ProductTree-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $content = $request->getContent();
        $this->_requestData = json_decode($request->getContent());
        if(!isset($this->_requestData->materialtype)||(isset($this->_requestData->materialtype)&&($this->_requestData->materialtype===''||$this->_requestData->materialtype===null))) {
            return $this->msgError(
                ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ProductTree.errMaterialTypeIsNotNull', array(), 'ProductTree'),
                401
            );
        }
        if($this->_requestData->materialtype=='O') {
            if(!isset($this->_requestData->number)||(isset($this->_requestData->number)&&($this->_requestData->number===''||$this->_requestData->number===null))) {
                return $this->msgError(
                    ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ProductTree.errOpnumberIsNotNull', array(), 'ProductTree'),
                    401
                );
            }
        }
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/ProductTree", name="ProductTree-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $material_types = $this->getComboValues($request, $_locale, 1, 100, 'material_types', 'value', 'concat(value,\'-\',code)', '', ' and value not in (\'S\',\'Y\') ');
            $material_units = $this->getComboValues($request, $_locale, 1, 100, 'material_units');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['material_types']=json_decode($material_types->getContent())->records;
            $data['extras']['material_units']=json_decode($material_units->getContent())->records;
            return $this->render('Modules/ProductTree.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ProductTree/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ProductTree-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ProductTree', $id);

            return $this->render('Modules/ProductTree.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ProductTree/{id}", requirements={"id": "\d+"}, name="ProductTree-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ProductTree', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ProductTree/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ProductTree-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ProductTree/updatedelivery", name="ProductTree-updatedelivery", options={"expose"=true}, methods={"GET"})
     */
    public function updateDeliveryAction(Request $request, $_locale)
    {
        //$userId=1;
        //$em = $this->getDoctrine()->getManager();
        //$conn = $em->getConnection();
        ///* @var $repo ProductTree */
        //$repo = $this->getDoctrine()->getRepository('App:ProductTree');
        //$sql_pt='SELECT record_id,name,delivery from product_trees where deleted_at is null and delivery=false and materialtype=:materialtype and "name" in (SELECT opno from "Kaynak")';
        //$stmt_pt = $conn->prepare($sql_pt);
        //$stmt_pt->bindValue('materialtype', 'O');
        //$stmt_pt->execute();
        //$records_pt = $stmt_pt->fetchAll();
        //if(count($records_pt)>0){
        //    for ($i=0;$i<count($records_pt);$i++) {
        //        $row=$records_pt[$i];
        //        $entity=$repo->findOneBy(array('recordId'=>$row['record_id']));
        //        if ($entity!==null){
        //            if (is_callable(array($entity, "setUpdateuserId"))) {
        //                $entity->setUpdateuserId($userId);
        //            }
        //            $entity->setDelivery(true);
        //            $em->persist($entity);
        //            $em->flush();
        //            $em->clear();
        //        }
        //    }
        //}
        return $this->msgSuccess('ok');
    }
}
