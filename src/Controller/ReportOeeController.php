<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Chart\Axis;
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\Chart\Layout;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Collection\CellsFactory;
use PhpOffice\PhpSpreadsheet\Settings;

//use lyquidity\xbrl_validate\PhpOffice\PhpSpreadsheet\Groups;
//use lyquidity\xbrl_validate\PhpOffice\PhpSpreadsheet\Group;
//use lyquidity\xbrl_validate\PhpOffice\PhpSpreadsheet\Spreadsheet;

class ReportOeeController extends DcsBaseController
{
    public $fieldname_opname;
    public $prod_concat_type;
    public $summary_type;
    public $uri;
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->fieldname_opname = 'opname';
        $this->prod_concat_type = 'all';
        $this->summary_type = 'all';//all
    }

    /**
     * @Route(path="/ReportOee/calculateWeb", name="ReportOee-calculate-web", options={"expose"=true}, methods={"POST"})
     */
    public function calculateAction(Request $request, $_locale)
    {
        $this->uri = strtolower($request->getBaseUrl());
        $uri = $this->uri;
        $_data = json_decode($request->getContent());
        return $this->getCalculateOeeClientsAction($request, $_locale);
    }

    /**
     * @Route(path="/m2m/GetOeeClients", name="ReportOee-getOeeClients", options={"expose"=true}, methods={"POST"})
     */
    public function getOeeClientsAction(Request $request, $_locale)
    {
        $this->uri = strtolower($request->getBaseUrl());
        if (($this->uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
            $this->fieldname_opname = 'opdescription';
        }
        $request->attributes->set('_isDCSService', true);
        $_data = $request->request->all();
        if (count($_data) === 0) {
            $_data = json_decode($request->getContent());
        } else {
            $_data = json_decode(json_encode($_data, JSON_FORCE_OBJECT));
        }
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        if ($_data->group === '') {
            $sqlC = "select c.code
            from clients c
            where c.code<>'' and c.workflow<>'Gölge Cihaz' and c.uuid is null
            order by c.code";
            $stmtC = $conn->prepare($sqlC);
        } else {
            $sqlC = "select c.code
            from clients c
            join client_group_details cgd on cgd.client=c.code and cgd.clientgroup='oeemail' and cgd.clientgroupcode=:clientgroupcode
            where c.code<>'' and c.workflow<>'Gölge Cihaz' and c.uuid is null 
            order by c.code";
            $stmtC = $conn->prepare($sqlC);
            $stmtC->bindValue('clientgroupcode', $_data->group);
        }
        $stmtC->execute();
        $recordsC = $stmtC->fetchAll();
        $clients = '';
        foreach ($recordsC as $rowC => $dataC) {
            $clients = $clients . ($clients === '' ? '' : ',') . $dataC["code"];
        }
        return $this->oee_sablon_1(0, 'total', true, false, $_data->day, $_data->day, $clients, null, null);
    }

    /**
     * @Route(path="/ReportOee/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ReportOee-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $this->uri = strtolower($request->getBaseUrl());
        $uri = $this->uri;
        $_data = json_decode($request->getContent());
        $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
        $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
        $_shortlost = $_data->shortlost;
        $_viewoption = $_data->viewoption ? $_data->viewoption : 'total';
        $_isdetail = $_viewoption == 'separatepage' ? false : $_data->isdetail;
        $_islostdetail = $_data->islostdetail;
        $_s = $_data->start;
        $_f = $_data->finish;
        $_cl = $_data->clients;
        $date1 = date_create($_s);
        $date2 = date_create($_f);
        $diff = date_diff($date1, $date2);
        if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
            $this->fieldname_opname = 'opdescription';
        }
        if ($_SERVER['HTTP_HOST'] == '192.168.1.40' || $_SERVER['HTTP_HOST'] == '172.16.1.149') {
            $this->summary_type = 'oldstyle';
        }
        if (($_SERVER['HTTP_HOST'] == '10.10.0.10')) {
            $this->prod_concat_type = 'opname';
        }
        if (($diff->m > 1 || ($diff->m == 1 && $diff->d > 2))
            && count(explode(',', $_cl)) > 1
        ) {
            $_isdetail = false;
        }
        if ($_s == null || $_f == null || $_cl == null || $_shortlost === null) {
            return $this->msgError(
                ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                401
            );
        }
        if ($_viewoption === 'total') {
            return $this->oee_sablon_1($_shortlost, $_viewoption, $_isdetail, $_islostdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam);
        }

        if ($_viewoption === 'list') {
            return $this->oee_sablon_2($_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam);
        }
    }

    private function oee_lost_row_1(&$objLostRecordSheet, $r, $row)
    {
        $objLostRecordSheet->setCellValue("A$r", $row["day"]);
        $objLostRecordSheet->setCellValue("B$r", $row["jobrotation"]);
        $objLostRecordSheet->setCellValue("C$r", $row["client"]);
        $objLostRecordSheet->setCellValue("D$r", $row["task"]);
        $objLostRecordSheet->setCellValue("E$r", $row["start"]);
        $objLostRecordSheet->setCellValue("F$r", $row["finish"]);
        $objLostRecordSheet->setCellValue("G$r", $row["suredk"]);
        $objLostRecordSheet->setCellValue("H$r", $row["suresaat"]);
        $objLostRecordSheet->setCellValue("I$r", $row["losttype"]);
        $objLostRecordSheet->setCellValue("J$r", $row["lostgroupcode"]);
        $objLostRecordSheet->setCellValue("K$r", $row["lostgroup"]);
        $objLostRecordSheet->setCellValue("L$r", $row["info"]);
        $objLostRecordSheet->setCellValue("M$r", $row["opsayi"]);
        $objLostRecordSheet->setCellValue("N$r", $row["descriptionlost"]);
        $objLostRecordSheet->setCellValue("O$r", $row["ftdescription"]);
        $objLostRecordSheet->setCellValue("P$r", $row["faultdevice"]);
        //if (($_SERVER['HTTP_HOST']=='10.10.0.10')) {
        $objLostRecordSheet->setCellValue("Q$r", $row["employee"]);
        $objLostRecordSheet->setCellValue("R$r", $row["faulttype"]);
        $objLostRecordSheet->setCellValue("S$r", $row["clientgroupcode"]);
        //}
        //$objLostRecordSheet->setCellValue("P$r", $row["sourceclient"]);
        //$objLostRecordSheet->setCellValue("Q$r", $row["sourcelosttype"]);
        //$objLostRecordSheet->setCellValue("R$r", $row["sourcedescriptionlost"]);
        $objLostRecordSheet->getStyle("A$r:R$r")->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                'rgb' => ($row["info"] == "" ? ($row["lostgroup"] == 'OEE-uretimekapali' ? "ded5ce" : ($row["lostgroup"] == 'OEE-planlidurus' ? "9ebb87" : ($row["lostgroup"] == 'OEE-kullanilabilirlik' ? "bddbe6" : "f7e5b1"))) : "FF8080")
            )
        ));
    }

    private function oee_lost_1(&$objPHPExcel, &$_toplamsureler, $_shortlost, $_viewoption, $_isdetail, $_islostdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = "
        select * from (
            " . $this->getSqlLost($_shortlost, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $this->fieldname_opname) . "
        )b
        order by b.start
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        if ($_isdetail) {
            //kayıp listesi sayfası
            $objPHPExcel->createSheet();
            $objLostRecordSheet = $objPHPExcel->setActiveSheetIndex(1);
            $objLostRecordSheet->setTitle("Kayıp Listesi");
            $objLostRecordSheet->setCellValue('A1', 'Tarih');
            $objLostRecordSheet->setCellValue('B1', "Vardiya");
            $objLostRecordSheet->setCellValue('C1', "İstasyon Adı");
            $objLostRecordSheet->setCellValue('D1', "Görev");
            $objLostRecordSheet->setCellValue('E1', "Başlangıç");
            $objLostRecordSheet->setCellValue('F1', "Bitiş");
            $objLostRecordSheet->setCellValue('G1', "Süre Dk");
            $objLostRecordSheet->setCellValue('H1', "Süre Saat");
            $objLostRecordSheet->setCellValue('I1', "Kayıp Tipi");
            $objLostRecordSheet->setCellValue('J1', "Grup");
            $objLostRecordSheet->setCellValue('K1', "OEE Grubu");
            $objLostRecordSheet->setCellValue('L1', "Bilgi");
            $objLostRecordSheet->setCellValue('M1', "Operatör Sayısı");
            $objLostRecordSheet->setCellValue('N1', "Kayıp Açıklaması");
            $objLostRecordSheet->setCellValue('O1', "Arıza Tipi Açıklaması");
            $objLostRecordSheet->setCellValue('P1', "Ekipman");
            //if (($_SERVER['HTTP_HOST']=='10.10.0.10')) {
            $objLostRecordSheet->setCellValue('Q1', "Operatörler");
            $objLostRecordSheet->setCellValue('R1', "Arıza Tipi");
            $objLostRecordSheet->setCellValue('S1', "İstasyon Grup");
            //}
            //$objLostRecordSheet->setCellValue('P1', "Kaynak İstasyon");
            //$objLostRecordSheet->setCellValue('Q1', "Kaynak İstasyon Kayıp Kodu");
            //$objLostRecordSheet->setCellValue('R1', "Kaynak İstasyon Kayıp Açıklaması");
            $objLostRecordSheet->getStyle('A1:S1')->getFont()->setBold(true);
            $r = 2;
        }
        foreach ($records as $item => $row) {
            if ($_isdetail) {
                $this->oee_lost_row_1($objLostRecordSheet, $r, $row);
            }
            $main_item = null;
            if ($row['lostgroup'] != null && $row['lostgroup'] != '') {
                switch ($row['lostgroup']) {
                    case 'OEE-performans':
                        $main_item = 'P_C_S';
                        break;
                    case 'OEE-kullanilabilirlik':
                        $main_item = 'I_S';
                        break;
                    case 'OEE-planlidurus':
                        $main_item = 'C_N_S';
                        break;
                    case 'OEE-uretimekapali':
                        $main_item = 'C_S';
                        break;
                    case 'OEE-kalitesizlik':
                        $main_item = 'E_C_S';
                        break;
                }
                $lgc = $this->replace_tr($row['lostgroupcode']);

                if ($row["info"] == "KISA DURUS") {
                    $_toplamsureler[$main_item]['data']["KISA DURUS"]["val"] += $row["suresn"];
                } else {
                    //if(!isset($_toplamsureler[$row["lostgroup"]]["data"][$row["kayiptipi"]])){
                    //    $_toplamsureler[$row["lostgroup"]]["data"][$row["kayiptipi"]]=0;
                    //}
                    if ($lgc !== null && $lgc !== '') {
                        $_toplamsureler[$main_item]['data'][$lgc]["title"] = $row['lostgroupcode'];
                        $_toplamsureler[$main_item]['data'][$lgc]["val"] += $row["suresn"];
                    }
                }
            }
            if ($_isdetail) {
                $r++;
                //    if($row["losttype"]=='SETUP-AYAR'&&$row["suresn"]<60){
                //        $r--;
                //    }
            }
        }
        if ($_isdetail) {
            for ($j = 1;$j < 20;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objLostRecordSheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        foreach ($_toplamsureler as $row => $item) {
            if (isset($_toplamsureler[$row]["data"]) && count($_toplamsureler[$row]["data"]) > 0) {
                foreach ($_toplamsureler[$row]["data"] as $_r => $_i) {
                    $_toplamsureler[$row]["data"][$_r]["val"] = round($_i["val"] / 3600, $ondalikhane);
                    $_toplamsureler[$row]["toplam"] += round($_i["val"] / 3600, $ondalikhane);
                }
            }
        }
        if ($_islostdetail) {
            return $records;
        } else {
            return [];
        }
    }

    private function oee_prod_row_1(&$objProdRecordSheet, $r, $row)
    {
        $objProdRecordSheet->setCellValue("A$r", $row["day"]);
        $objProdRecordSheet->setCellValue("B$r", $row["jobrotation"]);
        $objProdRecordSheet->setCellValue("C$r", $row["client"]);
        $objProdRecordSheet->setCellValue("D$r", $row["task"]);
        $objProdRecordSheet->setCellValue("E$r", $row["erprefnumber"]);
        $objProdRecordSheet->setCellValue("F$r", $row["start"]);
        $objProdRecordSheet->setCellValue("G$r", $row["finish"]);
        $objProdRecordSheet->setCellValue("H$r", $row["suresn"]);
        $objProdRecordSheet->setCellValue("I$r", $row["production"]);
        $objProdRecordSheet->setCellValue("J$r", $row["productioncurrent"]);
        $objProdRecordSheet->setCellValue("K$r", $row["calculatedtpp"]);
        $objProdRecordSheet->setCellValue("L$r", $row["proctime"]);
        $objProdRecordSheet->setCellValue("M$r", $row["proctimecurrent"]);
        $objProdRecordSheet->setCellValue("N$r", $row["losttime"]);
        $objProdRecordSheet->setCellValue("O$r", $row["slowdown"]);
        $objProdRecordSheet->setCellValue("P$r", $row["clientgroupcode"]);
        //$objProdRecordSheet->setCellValue("P$r", $row["operasyonsayisi"]);
        //$objProdRecordSheet->setCellValue("Q$r", $row["kalipsayisi"]);
    }

    private function oee_prod_1(&$objPHPExcel, &$_toplamsureler, &$uretim, &$hizkaybi, $_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = $this->getSqlProd($this->uri, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $this->fieldname_opname, $this->prod_concat_type);
        //
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        if ($_isdetail) {
            //üretim listesi sayfası
            $objPHPExcel->createSheet();
            $objProdRecordSheet = $objPHPExcel->setActiveSheetIndex(2);
            $objProdRecordSheet->setTitle("Üretim Listesi");
            $objProdRecordSheet->setCellValue('A1', 'Tarih');
            $objProdRecordSheet->setCellValue('B1', "Vardiya");
            $objProdRecordSheet->setCellValue('C1', "İstasyon Adı");
            $objProdRecordSheet->setCellValue('D1', "Görev");
            $objProdRecordSheet->setCellValue('E1', "Erp Ref");
            $objProdRecordSheet->setCellValue('F1', "Başlangıç");
            $objProdRecordSheet->setCellValue('G1', "Bitiş");
            $objProdRecordSheet->setCellValue('H1', "Süre(sn)");
            $objProdRecordSheet->setCellValue('I1', "Üretilen-1");
            $objProdRecordSheet->setCellValue('J1', "Üretilen-2");
            $objProdRecordSheet->setCellValue('K1', "Birim Süre(sn)");
            $objProdRecordSheet->setCellValue('L1', "Üretim Süre-1(sn)");
            $objProdRecordSheet->setCellValue('M1', "Üretim Süre-2(sn)");
            $objProdRecordSheet->setCellValue('N1', "Kayıp Süre(sn)");
            $objProdRecordSheet->setCellValue('O1', "Yavaşlatma Süre(sn)");
            $objProdRecordSheet->setCellValue('P1', "İstasyon Grup");
            $objProdRecordSheet->getStyle('A1:P1')->getFont()->setBold(true);
            $r = 2;
        }
        foreach ($records as $item => $row) {
            if ($_isdetail) {
                $this->oee_prod_row_1($objProdRecordSheet, $r, $row);
                $r++;
            }
            $uretim += $row["proctimecurrent"];
            $hizkaybi += $row["slowdown"];
        }
        if ($_isdetail) {
            for ($j = 1;$j < 21;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objProdRecordSheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
    }

    private function oee_reject_row_1(&$objScrapRecordSheet, $r, $row)
    {
        $objScrapRecordSheet->setCellValue("A$r", $row["day"]);
        $objScrapRecordSheet->setCellValue("B$r", $row["jobrotation"]);
        $objScrapRecordSheet->setCellValue("C$r", $row["time"]);
        $objScrapRecordSheet->setCellValue("D$r", $row["client"]);
        $objScrapRecordSheet->setCellValue("E$r", $row["erprefnumber"]);
        $objScrapRecordSheet->setCellValue("F$r", $row["opname"]);
        $objScrapRecordSheet->setCellValue("G$r", $row["rejecttype"]);
        $objScrapRecordSheet->setCellValue("H$r", $row["quantityreject"]);
        $objScrapRecordSheet->setCellValue("I$r", $row["tpp"]);
        $objScrapRecordSheet->setCellValue("J$r", $row["iskartasure"]);
        $objScrapRecordSheet->setCellValue("K$r", $row["importfilename"]);
        $objScrapRecordSheet->setCellValue("L$r", $row["clientgroupcode"]);
    }

    private function oee_reject_1(&$objPHPExcel, &$_toplamsureler, &$iskarta, $_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = $this->getSqlReject($_s, $_f, $_cl, $_jobrotation, $_jobrotationteam);

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        if ($_isdetail) {
            //ıskarta listesi sayfası
            $objPHPExcel->createSheet();
            $objScrapRecordSheet = $objPHPExcel->setActiveSheetIndex(3);
            $objScrapRecordSheet->setTitle("Iskarta Listesi");
            $objScrapRecordSheet->setCellValue('A1', 'Tarih');
            $objScrapRecordSheet->setCellValue('B1', "Vardiya");
            $objScrapRecordSheet->setCellValue('C1', "Zaman");
            $objScrapRecordSheet->setCellValue('D1', "İstasyon Adı");
            $objScrapRecordSheet->setCellValue('E1', "Erp Ref");
            $objScrapRecordSheet->setCellValue('F1', "Operasyon");
            $objScrapRecordSheet->setCellValue('G1', "Iskarta Tipi");
            $objScrapRecordSheet->setCellValue('H1', "Miktar");
            $objScrapRecordSheet->setCellValue('I1', "Birim Süre(sn)");
            $objScrapRecordSheet->setCellValue('J1', "Toplam Süre(sn)");
            $objScrapRecordSheet->setCellValue('K1', "Tespit Edilen İstasyon");
            $objScrapRecordSheet->setCellValue('L1', "İstasyon Grup");
            $objScrapRecordSheet->getStyle('A1:L1')->getFont()->setBold(true);
            $r = 2;
        }
        foreach ($records as $item => $row) {
            if ($_isdetail) {
                $this->oee_reject_row_1($objScrapRecordSheet, $r, $row);
                $r++;
            }
            $iskarta += $row["iskartasure"];
        }
        if ($_isdetail) {
            for ($j = 1;$j < 15;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objScrapRecordSheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        $_toplamsureler['E_C_S']['data']['iskarta']["val"] = round($iskarta / 3600, $ondalikhane);
        $_toplamsureler['E_C_S']['toplam'] += round($iskarta / 3600, $ondalikhane);
    }

    private function oee_missing_clients_row_1(&$objMissingClientRecordSheet, $r, $row)
    {
        $objMissingClientRecordSheet->setCellValue("A$r", $row["day"]);
        $objMissingClientRecordSheet->setCellValue("B$r", $row["jobrotation"]);
        $objMissingClientRecordSheet->setCellValue("C$r", $row["client"]);
        $objMissingClientRecordSheet->setCellValue("D$r", $row["jr_start"]);
        $objMissingClientRecordSheet->setCellValue("E$r", $row["jr_end"]);
    }

    private function oee_missing_clients_1(&$objPHPExcel, &$_toplamsureler, &$iskarta, $_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = $this->getSqlMissingClients($_s, $_f, $_cl, $_jobrotation, $_jobrotationteam);

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        if ($_isdetail) {
            //ıskarta listesi sayfası
            $objPHPExcel->createSheet();
            $objMissingClientRecordSheet = $objPHPExcel->setActiveSheetIndex(4);
            $objMissingClientRecordSheet->setTitle("Veri Olmayan İstasyonlar");
            $objMissingClientRecordSheet->setCellValue('A1', 'Tarih');
            $objMissingClientRecordSheet->setCellValue('B1', "Vardiya");
            $objMissingClientRecordSheet->setCellValue('C1', "İstasyon Adı");
            $objMissingClientRecordSheet->setCellValue('D1', "Başlangıç");
            $objMissingClientRecordSheet->setCellValue('E1', "Bitiş");
            $objMissingClientRecordSheet->getStyle('A1:E1')->getFont()->setBold(true);
            $r = 2;
        }
        foreach ($records as $item => $row) {
            if ($_isdetail) {
                $this->oee_missing_clients_row_1($objMissingClientRecordSheet, $r, $row);
                $r++;
            }
        }
        if ($_isdetail) {
            for ($j = 1;$j < 15;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objMissingClientRecordSheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
    }

    //$_viewoption==='total'
    public function oee_sablon_1($_shortlost, $_viewoption, $_isdetail, $_islostdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam)
    {
        $name = "OEE_" . date("Y-m-d_H:i:s");
        $_arr_bold = array(
            'font'  => array(
                'bold'  => true
            ));
        $_arr_color_red = array(
            'font'  => array(
                'color' => array('rgb' => 'FF0000')
            ));
        $_arr_border = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => Border::BORDER_THIN
                )
            )
        );
        $ondalikhane = 4;
        $ondalikhanegoster = 4;
        $totalOEECount = 6;
        $startColOEE = 1;
        $detailRowCount = 3;
        $startRowOEE = 4;
        $_toplamsureler = array(
            "C_K_S" => array("toplam" => 0,"header" => "ÇKS")
            ,"C_S" => array("toplam" => 0,"header" => "ÇS","title" => "Üretime Kapalı Zaman","data" => array())
            ,"C_N_S" => array("toplam" => 0,"header" => "ÇNS","title" => "Planlı Duruşlar","data" => array())
            ,"I_S" => array("toplam" => 0,"header" => "İS","title" => "Kullanılabilirlik","data" => array())
            ,"P_C_S" => array("toplam" => 0,"header" => "PÇS","title" => "Performans","data" => array())
            ,"E_C_S" => array("toplam" => 0,"header" => "EÇS","title" => "Kalitesiz Üretim","data" => array("iskarta" => array("title" => 'Iskarta',"val" => 0)))
        );
        $uri = $this->uri;
        if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
            $ondalikhane = 1;
            $ondalikhanegoster = 1;
        }
        $sql_oee_grup = "select lostgroup,lostgroupcode
            from lost_group_details 
            where lostgroup like 'OEE%' and delete_user_id is null
            GROUP BY lostgroup,lostgroupcode
            order by case 
                when lostgroup='OEE-performans' then 1 
                when lostgroup='OEE-kullanilabilirlik' then 2 
                when lostgroup='OEE-planlidurus' then 3 
                when lostgroup='OEE-uretimekapali' then 4 
                else 5 end,lostgroupcode";

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql_oee_grup);
        $stmt->execute();
        $records = $stmt->fetchAll();
        foreach ($records as $row => $data) {
            $val = $this->replace_tr($data['lostgroupcode']);
            switch ($data['lostgroup']) {
                case 'OEE-performans':
                    $_toplamsureler['P_C_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-kullanilabilirlik':
                    $_toplamsureler['I_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-planlidurus':
                    $_toplamsureler['C_N_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-uretimekapali':
                    $_toplamsureler['C_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
                case 'OEE-kalitesizlik':
                    $_toplamsureler['E_C_S']['data'][$val] = array("title" => $data['lostgroupcode'],"val" => 0);
                    break;
            }
        }
        $_toplamsureler['P_C_S']['data']['HIZ KAYBI'] = array("title" => "Hız Kaybı","val" => 0);
        $_toplamsureler['P_C_S']['data']['KISA DURUS'] = array("title" => "KISA DURUŞ","val" => 0);
        $_toplamsureler['I_S']['data']['KISA DURUS'] = array("title" => "KISA DURUŞ","val" => 0);
        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getProperties()
            ->setCreator("Kaitek - Reports")
            ->setLastModifiedBy("Kaitek - Reports")
            ->setTitle("Kaitek - Reports")
            ->setSubject("Kaitek - Reports")
            ->setDescription($name)
            ->setKeywords('Verimot')
            ->setCategory('-');
        $objMainSheet = $objPHPExcel->getActiveSheet();
        //lost
        $recordsLost = $this->oee_lost_1($objPHPExcel, $_toplamsureler, $_shortlost, $_viewoption, $_isdetail, $_islostdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane);
        //prod
        $uretim = 0;
        $hizkaybi = 0;
        $this->oee_prod_1($objPHPExcel, $_toplamsureler, $uretim, $hizkaybi, $_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane);
        //reject
        $iskarta = 0;//round($qi["sure"]/3600,$ondalikhane);
        $this->oee_reject_1($objPHPExcel, $_toplamsureler, $iskarta, $_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane);

        $this->oee_missing_clients_1($objPHPExcel, $_toplamsureler, $iskarta, $_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $ondalikhane);

        if ($_jobrotationteam === null) {
            $date1 = new \DateTime($_s);
            $date2 = new \DateTime($_f);
            $gun = $date2->diff($date1)->format("%a");
            $sure = 24;
            if ($_jobrotation !== null) {
                $arr_sure = explode('-', preg_replace('/:/', '', $_jobrotation));
                $arr_sure[0] = intval($arr_sure[0]);
                $arr_sure[1] = intval($arr_sure[1]);
                if ($arr_sure[0] > $arr_sure[1]) {
                    $arr_sure[1] += 2400;
                }
                $sure = round(($arr_sure[1] - $arr_sure[0]) / 100);
            }
            $_cks = round(count(explode(',', $_cl)) * (intval($gun) + 1) * $sure, $ondalikhane);
        } else {
            $sql_sure = "select sum(aa.suredk)/60 sure 
            from (
                SELECT (jre.worktime+jre.breaktime) suredk
                from job_rotation_employees jre
                join employees e on e.code=jre.employee
                where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam,jre.worktime,jre.breaktime)aa";
            $stmt_sure = $conn->prepare($sql_sure);
            $stmt_sure->execute();
            $records_sure = $stmt_sure->fetchAll();
            $_cks = round(count(explode(',', $_cl)) * $records_sure[0]['sure'], $ondalikhane);
        }
        $_toplamsureler['C_K_S']['toplam'] = $_cks;
        $_cs = $_cks - $_toplamsureler["C_S"]["toplam"];
        $_cns = $_cs - $_toplamsureler["C_N_S"]["toplam"];
        $_is = $_cns - $_toplamsureler["I_S"]["toplam"];
        //hız kaybı hesaplanması
        //$hiz_kaybi=$uretim>0?($_is-$_toplamsureler["P_C_S"]["toplam"]-round($uretim/3600,$ondalikhane)):0;
        //$hiz_kaybi=$hiz_kaybi<0?0:$hiz_kaybi;
        $_toplamsureler['P_C_S']['data']['HIZ KAYBI']["val"] = round($hizkaybi / 3600, $ondalikhane);
        $_toplamsureler['P_C_S']['toplam'] += round($hizkaybi / 3600, $ondalikhane);

        $_pcs = $_is - $_toplamsureler["P_C_S"]["toplam"];
        $_ecs = $_pcs - $_toplamsureler['E_C_S']['toplam'];
        $_toplamsureler["_cks"] = $_cks;
        $_toplamsureler["_cs"] = $_cs;
        $_toplamsureler["_cns"] = $_cns;
        $_toplamsureler["_is"] = $_is;
        $_toplamsureler["_pcs"] = $_pcs;
        $_toplamsureler["_ecs"] = $_ecs;
        $_toplamsureler["kayiptoplam_ur_kul"] = $_toplamsureler["I_S"]["toplam"];
        $_toplamsureler["kayiptoplam_ur_per"] = $_toplamsureler["P_C_S"]["toplam"];
        $_toplamsureler["kayiptoplam_ur_kal"] = $_toplamsureler['E_C_S']['toplam'];
        $_toplamsureler["kayiptoplam_fab_kul"] = $_toplamsureler["kayiptoplam_ur_kul"] + $_toplamsureler["C_N_S"]["toplam"];
        $_toplamsureler["kayiptoplam_fab_per"] = $_toplamsureler["P_C_S"]["toplam"];
        $_toplamsureler["kayiptoplam_fab_kal"] = $_toplamsureler['E_C_S']['toplam'];
        $_toplamsureler["kayiptoplam_gen_kul"] = $_toplamsureler["kayiptoplam_fab_kul"] + $_toplamsureler["C_S"]["toplam"];
        $_toplamsureler["kayiptoplam_gen_per"] = $_toplamsureler["P_C_S"]["toplam"];
        $_toplamsureler["kayiptoplam_gen_kal"] = $_toplamsureler['E_C_S']['toplam'];
        $_toplamsureler["oee_ur"] = round(100 * ($_cns > 0 ? ($_ecs / $_cns) : 1), $ondalikhane);
        $_toplamsureler["oee_fab"] = round(100 * ($_cs > 0 ? ($_ecs / $_cs) : 1), $ondalikhane);
        $_toplamsureler["oee_gen"] = round(100 * ($_cks > 0 ? ($_ecs / $_cks) : 1), $ondalikhane);

        $_toplamsureler["oee_carpan_kul_ur"] = round((100 * ($_toplamsureler["_cns"] > 0 ? ($_toplamsureler["_is"] / $_toplamsureler["_cns"]) : 1)), $ondalikhanegoster);
        $_toplamsureler["oee_carpan_kul_fab"] = round((100 * ($_toplamsureler["_cs"] > 0 ? ($_toplamsureler["_is"] / $_toplamsureler["_cs"]) : 1)), $ondalikhanegoster);
        $_toplamsureler["oee_carpan_kul_gen"] = round((100 * ($_toplamsureler["_cks"] > 0 ? ($_toplamsureler["_is"] / $_toplamsureler["_cks"]) : 1)), $ondalikhanegoster);

        $_toplamsureler["oee_carpan_per_ur"] = round((100 * ($_toplamsureler["_is"] > 0 ? ($_toplamsureler["_pcs"] / $_toplamsureler["_is"]) : 1)), $ondalikhanegoster);
        $_toplamsureler["oee_carpan_per_fab"] = round((100 * ($_toplamsureler["_is"] > 0 ? ($_toplamsureler["_pcs"] / $_toplamsureler["_is"]) : 1)), $ondalikhanegoster);
        $_toplamsureler["oee_carpan_per_gen"] = round((100 * ($_toplamsureler["_is"] > 0 ? ($_toplamsureler["_pcs"] / $_toplamsureler["_is"]) : 1)), $ondalikhanegoster);

        $_toplamsureler["oee_carpan_kal_ur"] = round((100 * ($_toplamsureler["_pcs"] > 0 ? ($_toplamsureler["_ecs"] / $_toplamsureler["_pcs"]) : 1)), $ondalikhanegoster);
        $_toplamsureler["oee_carpan_kal_fab"] = round((100 * ($_toplamsureler["_pcs"] > 0 ? ($_toplamsureler["_ecs"] / $_toplamsureler["_pcs"]) : 1)), $ondalikhanegoster);
        $_toplamsureler["oee_carpan_kal_gen"] = round((100 * ($_toplamsureler["_pcs"] > 0 ? ($_toplamsureler["_ecs"] / $_toplamsureler["_pcs"]) : 1)), $ondalikhanegoster);

        $_toplamsureler["oee_kayip_kul_ur"] = round((100 - $_toplamsureler["oee_carpan_kul_ur"]), $ondalikhanegoster);
        $_toplamsureler["oee_kayip_kul_fab"] = round((100 - $_toplamsureler["oee_carpan_kul_fab"]), $ondalikhanegoster);
        $_toplamsureler["oee_kayip_kul_gen"] = round((100 - $_toplamsureler["oee_carpan_kul_gen"]), $ondalikhanegoster);

        $_toplamsureler["oee_kayip_per_ur"] = round((100 - $_toplamsureler["oee_carpan_per_ur"]), $ondalikhanegoster);
        $_toplamsureler["oee_kayip_per_fab"] = round((100 - $_toplamsureler["oee_carpan_per_fab"]), $ondalikhanegoster);
        $_toplamsureler["oee_kayip_per_gen"] = round((100 - $_toplamsureler["oee_carpan_per_gen"]), $ondalikhanegoster);

        $_toplamsureler["oee_kayip_kal_ur"] = round((100 - $_toplamsureler["oee_carpan_kal_ur"]), $ondalikhanegoster);
        $_toplamsureler["oee_kayip_kal_fab"] = round((100 - $_toplamsureler["oee_carpan_kal_fab"]), $ondalikhanegoster);
        $_toplamsureler["oee_kayip_kal_gen"] = round((100 - $_toplamsureler["oee_carpan_kal_gen"]), $ondalikhanegoster);
        if ($this->summary_type == 'all') {
            $_toplamsureler["oee_kayip_ur"] = $_toplamsureler["oee_kayip_kul_ur"] + $_toplamsureler["oee_kayip_per_ur"] + $_toplamsureler["oee_kayip_kal_ur"];
            $_toplamsureler["oee_kayip_fab"] = $_toplamsureler["oee_kayip_kul_fab"] + $_toplamsureler["oee_kayip_per_fab"] + $_toplamsureler["oee_kayip_kal_fab"];
            $_toplamsureler["oee_kayip_gen"] = $_toplamsureler["oee_kayip_kul_gen"] + $_toplamsureler["oee_kayip_per_gen"] + $_toplamsureler["oee_kayip_kal_gen"];
        } else {
            $_toplamsureler["oee_kayip_ur"] = 100 - $_toplamsureler["oee_ur"];
            $_toplamsureler["oee_kayip_fab"] = 100 - $_toplamsureler["oee_fab"];
            $_toplamsureler["oee_kayip_gen"] = 100 - $_toplamsureler["oee_gen"];
        }
        $_toplamsureler["kayiptoplam_ur"] = $_toplamsureler["kayiptoplam_ur_kul"] + $_toplamsureler["kayiptoplam_ur_per"] + $_toplamsureler["kayiptoplam_ur_kal"];
        $_toplamsureler["kayiptoplam_fab"] = $_toplamsureler["kayiptoplam_fab_kul"] + $_toplamsureler["kayiptoplam_fab_per"] + $_toplamsureler["kayiptoplam_fab_kal"];
        $_toplamsureler["kayiptoplam_gen"] = $_toplamsureler["kayiptoplam_gen_kul"] + $_toplamsureler["kayiptoplam_gen_per"] + $_toplamsureler["kayiptoplam_gen_kal"];


        $objPHPExcel->setActiveSheetIndex(0);
        $objMainSheet->getColumnDimension('A')->setWidth(1);
        ///////////////////////////////////////////
        //ecs detail section
        $i = 4;
        $ecs_detail_col = '';
        foreach ($_toplamsureler['E_C_S']['data'] as $row => $item) {
            $_strcol = Coordinate::stringFromColumnIndex($i++);
            if ($ecs_detail_col == '') {
                $ecs_detail_col = $_strcol;
            }
            $curRow = (($totalOEECount - 1) * $detailRowCount) + $startRowOEE + 1;
            $objMainSheet->setCellValue($_strcol . $curRow, $item["title"]);
            $objMainSheet->setCellValue($_strcol . ($curRow + 1), round($item["val"], $ondalikhanegoster));
            $objMainSheet->getStyle($_strcol . ($curRow + 1))->applyFromArray($_arr_color_red);
        }
        $ecs_finish_col = $_strcol;
        $ecs_finish_colIndex = $i;
        $objMainSheet->mergeCells($ecs_detail_col . ($curRow - 1) . ":" . $ecs_finish_col . ($curRow - 1));
        $objMainSheet->setCellValue($ecs_detail_col . ($curRow - 1), $_toplamsureler['E_C_S']['title']);
        $objMainSheet->getStyle($ecs_detail_col . $curRow . ":" . $ecs_finish_col . ($curRow + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ffff99"
            )
        ));
        //ecs data section
        $_strcol = Coordinate::stringFromColumnIndex(3);
        $curRow = (($totalOEECount - 1) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $_strcol . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), round($_toplamsureler["_ecs"], $ondalikhanegoster));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "b2d44a"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        //ecs header section
        $_strcol = Coordinate::stringFromColumnIndex(2);
        $curRow = (($totalOEECount - 1) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $_strcol . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), $_toplamsureler['E_C_S']['header']);
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "b2d44a"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        $objMainSheet->getStyle($_strcol . ($curRow) . ":" . $ecs_finish_col . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_border);
        ///////////////////////////////////////////
        //pcs detail section
        $i = $ecs_finish_colIndex;
        $pcs_detail_col = '';
        foreach ($_toplamsureler['P_C_S']['data'] as $row => $item) {
            $_strcol = Coordinate::stringFromColumnIndex($i++);
            if ($pcs_detail_col == '') {
                $pcs_detail_col = $_strcol;
            }
            $curRow = (($totalOEECount - 2) * $detailRowCount) + $startRowOEE + 1;
            $objMainSheet->setCellValue($_strcol . $curRow, $item["title"]);
            $objMainSheet->setCellValue($_strcol . ($curRow + 1), round($item["val"], $ondalikhanegoster));
            $objMainSheet->getStyle($_strcol . ($curRow + 1))->applyFromArray($_arr_color_red);
        }
        $pcs_finish_col = $_strcol;
        $pcs_finish_colIndex = $i;
        $objMainSheet->mergeCells($pcs_detail_col . ($curRow - 1) . ":" . $pcs_finish_col . ($curRow - 1));
        $objMainSheet->setCellValue($pcs_detail_col . ($curRow - 1), $_toplamsureler['P_C_S']['title']);
        $objMainSheet->getStyle($pcs_detail_col . $curRow . ":" . $pcs_finish_col . ($curRow + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ffff99"
            )
        ));
        //pcs data section
        $_strcol = Coordinate::stringFromColumnIndex(3);
        $curRow = (($totalOEECount - 2) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $ecs_finish_col . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), round($_toplamsureler["_pcs"], $ondalikhanegoster));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "f7e5b1"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        //pcs header section
        $_strcol = Coordinate::stringFromColumnIndex(2);
        $curRow = (($totalOEECount - 2) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $_strcol . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), $_toplamsureler['P_C_S']['header']);
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "f7e5b1"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        $objMainSheet->getStyle($_strcol . ($curRow) . ":" . $pcs_finish_col . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_border);
        ///////////////////////////////////////////
        //is detail section
        $i = $pcs_finish_colIndex;
        $is_detail_col = '';
        foreach ($_toplamsureler['I_S']['data'] as $row => $item) {
            $_strcol = Coordinate::stringFromColumnIndex($i++);
            if ($is_detail_col == '') {
                $is_detail_col = $_strcol;
            }
            $curRow = (($totalOEECount - 3) * $detailRowCount) + $startRowOEE + 1;
            $objMainSheet->setCellValue($_strcol . $curRow, $item["title"]);
            $objMainSheet->setCellValue($_strcol . ($curRow + 1), round($item["val"], $ondalikhanegoster));
            $objMainSheet->getStyle($_strcol . ($curRow + 1))->applyFromArray($_arr_color_red);
        }
        $is_finish_col = $_strcol;
        $is_finish_colIndex = $i;
        $objMainSheet->mergeCells($is_detail_col . ($curRow - 1) . ":" . $is_finish_col . ($curRow - 1));
        $objMainSheet->setCellValue($is_detail_col . ($curRow - 1), $_toplamsureler['I_S']['title']);
        $objMainSheet->getStyle($is_detail_col . $curRow . ":" . $is_finish_col . ($curRow + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ffff99"
            )
        ));
        //is data section
        $_strcol = Coordinate::stringFromColumnIndex(3);
        $curRow = (($totalOEECount - 3) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $pcs_finish_col . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), round($_toplamsureler["_is"], $ondalikhanegoster));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "bddbe6"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        //is header section
        $_strcol = Coordinate::stringFromColumnIndex(2);
        $curRow = (($totalOEECount - 3) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $_strcol . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), $_toplamsureler['I_S']['header']);
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "bddbe6"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        $objMainSheet->getStyle($_strcol . ($curRow) . ":" . $is_finish_col . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_border);
        ///////////////////////////////////////////
        //cns detail section
        $i = $is_finish_colIndex;
        $cns_detail_col = '';
        foreach ($_toplamsureler['C_N_S']['data'] as $row => $item) {
            $_strcol = Coordinate::stringFromColumnIndex($i++);
            if ($cns_detail_col == '') {
                $cns_detail_col = $_strcol;
            }
            $curRow = (($totalOEECount - 4) * $detailRowCount) + $startRowOEE + 1;
            $objMainSheet->setCellValue($_strcol . $curRow, $item["title"]);
            $objMainSheet->setCellValue($_strcol . ($curRow + 1), round($item["val"], $ondalikhanegoster));
            $objMainSheet->getStyle($_strcol . ($curRow + 1))->applyFromArray($_arr_color_red);
        }
        $cns_finish_col = $_strcol;
        $cns_finish_colIndex = $i;
        $objMainSheet->mergeCells($cns_detail_col . ($curRow - 1) . ":" . $cns_finish_col . ($curRow - 1));
        $objMainSheet->setCellValue($cns_detail_col . ($curRow - 1), $_toplamsureler['C_N_S']['title']);
        $objMainSheet->getStyle($cns_detail_col . $curRow . ":" . $cns_finish_col . ($curRow + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "92D050"
            )
        ));
        //cns data section
        $_strcol = Coordinate::stringFromColumnIndex(3);
        $curRow = (($totalOEECount - 4) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $is_finish_col . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), round($_toplamsureler["_cns"], $ondalikhanegoster));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "9ebb87"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        //cns header section
        $_strcol = Coordinate::stringFromColumnIndex(2);
        $curRow = (($totalOEECount - 4) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $_strcol . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), $_toplamsureler['C_N_S']['header']);
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "9ebb87"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        $objMainSheet->getStyle($_strcol . ($curRow) . ":" . $cns_finish_col . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_border);
        ///////////////////////////////////////////
        //cs detail section
        $i = $cns_finish_colIndex;
        $cs_detail_col = '';
        foreach ($_toplamsureler['C_S']['data'] as $row => $item) {
            $_strcol = Coordinate::stringFromColumnIndex($i++);
            if ($cs_detail_col == '') {
                $cs_detail_col = $_strcol;
            }
            $curRow = (($totalOEECount - 5) * $detailRowCount) + $startRowOEE + 1;
            $objMainSheet->setCellValue($_strcol . $curRow, $item["title"]);
            $objMainSheet->setCellValue($_strcol . ($curRow + 1), round($item["val"], $ondalikhanegoster));
            $objMainSheet->getStyle($_strcol . ($curRow + 1))->applyFromArray($_arr_color_red);
        }
        $cs_finish_col = $_strcol;
        $cs_finish_colIndex = $i;
        $objMainSheet->mergeCells($cs_detail_col . ($curRow - 1) . ":" . $cs_finish_col . ($curRow - 1));
        $objMainSheet->setCellValue($cs_detail_col . ($curRow - 1), $_toplamsureler['C_S']['title']);
        $objMainSheet->getStyle($cs_detail_col . $curRow . ":" . $cs_finish_col . ($curRow + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ffc000"
            )
        ));
        //cs data section
        $_strcol = Coordinate::stringFromColumnIndex(3);
        $curRow = (($totalOEECount - 5) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $cns_finish_col . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), round($_toplamsureler["_cs"], $ondalikhanegoster));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ded5ce"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        //cs header section
        $_strcol = Coordinate::stringFromColumnIndex(2);
        $curRow = (($totalOEECount - 5) * $detailRowCount) + $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $_strcol . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), $_toplamsureler['C_S']['header']);
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ded5ce"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        $objMainSheet->getStyle($_strcol . ($curRow) . ":" . $cs_finish_col . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_border);
        ///////////////////////////////////////////
        //cks data section
        $i = $cs_finish_colIndex;
        $_strcol = Coordinate::stringFromColumnIndex(3);
        $curRow = $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $cs_finish_col . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), round($_toplamsureler["_cks"], $ondalikhanegoster));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "A5C9E2"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        //cks header section
        $_strcol = Coordinate::stringFromColumnIndex(2);
        $curRow = $startRowOEE;
        $objMainSheet->mergeCells($_strcol . ($curRow) . ":" . $_strcol . ($curRow + ($detailRowCount - 1)));
        $objMainSheet->setCellValue($_strcol . ($curRow), $_toplamsureler['C_K_S']['header']);
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "A5C9E2"
            )
        ));
        $objMainSheet->getStyle($_strcol . $curRow . ":" . $_strcol . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_bold);
        $objMainSheet->getStyle($_strcol . ($curRow) . ":" . $cs_finish_col . ($curRow + ($detailRowCount - 1)))->applyFromArray($_arr_border);
        //title
        $objMainSheet->getStyle('A1:' . $cs_finish_col . '1')->getFont()->setBold(true);
        $objMainSheet->mergeCells('A1:' . $cs_finish_col . '1');
        $objMainSheet->getStyle('A1:' . $cs_finish_col . '1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $objMainSheet->setCellValue('A1', "OEE ANALİZİ");
        $objMainSheet->setTitle("OEE ANALİZİ");
        $objMainSheet->mergeCells("B3:D3");
        $objMainSheet->setCellValue("B3", $_s . "-" . $_f);
        $objMainSheet->getStyle("B4:" . $cs_finish_col . "21")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

        $objMainSheet->mergeCells("F3:" . $cs_finish_col . "3");
        $objMainSheet->setCellValue('F3', $_cl);//seçili istasyonlar

        ////////////////////
        //oee
        $objMainSheet->getStyle("J22:M" . ($this->summary_type == 'all' ? "34" : "27") . "")->applyFromArray($_arr_border);
        $objMainSheet->mergeCells("J22:J23");
        $objMainSheet->getStyle("J22")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
        $objMainSheet->setCellValue("J22", "OEE");
        $objMainSheet->getStyle("J22")->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "c6d9f1"
            )
        ));

        $objMainSheet->setCellValue("K22", "ÜRETİM MÜD.");
        $objMainSheet->setCellValue("L22", "FABRİKA MÜD.");
        $objMainSheet->setCellValue("M22", "GENEL MÜD.");
        $objMainSheet->setCellValue("K23", round($_toplamsureler["oee_ur"], $ondalikhanegoster));
        $objMainSheet->setCellValue("L23", round($_toplamsureler["oee_fab"], $ondalikhanegoster));
        $objMainSheet->setCellValue("M23", round($_toplamsureler["oee_gen"], $ondalikhanegoster));

        $objMainSheet->getStyle("K22:K23")->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ffff00"
            )
        ));
        $objMainSheet->getStyle("L22:L23")->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "92D050"
            )
        ));
        $objMainSheet->getStyle("M22:M23")->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                    'rgb' => "ffc000"
            )
        ));
        if ($this->summary_type == 'all') {
            $objMainSheet->getStyle("J24:J26")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "c6d9f1"
                )
            ));
            $objMainSheet->getStyle("K24:K26")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffff00"
                )
            ));
            $objMainSheet->getStyle("L24:L26")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "92D050"
                )
            ));
            $objMainSheet->getStyle("M24:M26")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffc000"
                )
            ));
            $objMainSheet->setCellValue("J24", "Kullanabilirlik");
            $objMainSheet->setCellValue("J25", "Performans");
            $objMainSheet->setCellValue("J26", "Kalite");

            $objMainSheet->mergeCells("J27:M27");
            $objMainSheet->getStyle('J27')->applyFromArray($_arr_color_red);
            $objMainSheet->getStyle("J27")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objMainSheet->setCellValue("J27", "OEE KAYIPLARI %");
            $objMainSheet->getStyle("J27")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ccc1da"
                )
            ));
            $objMainSheet->getStyle("J28:J30")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "c6d9f1"
                )
            ));
            $objMainSheet->setCellValue("J28", "Kullanabilirlik");
            $objMainSheet->setCellValue("J29", "Performans");
            $objMainSheet->setCellValue("J30", "Kalite");

            $objMainSheet->getStyle("K28:K30")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffff00"
                )
            ));
            $objMainSheet->getStyle("L28:L30")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "92D050"
                )
            ));

            $objMainSheet->getStyle("M28:M30")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffc000"
                )
            ));

            $objMainSheet->mergeCells("J31:M31");
            $objMainSheet->getStyle('J31')->applyFromArray($_arr_color_red);
            $objMainSheet->getStyle("J31")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objMainSheet->setCellValue("J31", "OEE KAYIPLARI SAAT");
            $objMainSheet->getStyle("J31")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ccc1da"
                )
            ));
            $objMainSheet->getStyle("J32:J34")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "c6d9f1"
                )
            ));
            $objMainSheet->setCellValue("J32", "Kullanabilirlik");
            $objMainSheet->setCellValue("J33", "Performans");
            $objMainSheet->setCellValue("J34", "Kalite");

            $objMainSheet->getStyle("K32:K34")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffff00"
                )
            ));
            $objMainSheet->getStyle("L32:L34")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "92D050"
                )
            ));
            $objMainSheet->getStyle("M32:M34")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffc000"
                )
            ));
        } else {
            $objMainSheet->mergeCells("J24:M24");
            $objMainSheet->getStyle('J24')->applyFromArray($_arr_color_red);
            $objMainSheet->getStyle("J24")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objMainSheet->setCellValue("J24", "OEE KAYIPLARI %");
            $objMainSheet->getStyle("J24")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ccc1da"
                )
            ));

            $objMainSheet->getStyle("J25:J27")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "c6d9f1"
                )
            ));
            $objMainSheet->setCellValue("J25", "Kullanabilirlik");
            $objMainSheet->setCellValue("J26", "Performans");
            $objMainSheet->setCellValue("J27", "Kalite");

            $objMainSheet->getStyle("K25:K27")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffff00"
                )
            ));
            $objMainSheet->getStyle("L25:L27")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "92D050"
                )
            ));

            $objMainSheet->getStyle("M25:M27")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "ffc000"
                )
            ));
        }
        //*************************************************************** */
        //özet tablo
        $idx_summary_table = $this->summary_type == 'all' ? 35 : 28;
        $objMainSheet->mergeCells("F$idx_summary_table:I$idx_summary_table");
        $objMainSheet->setCellValue("F$idx_summary_table", "OEE ANALİZİ ÖZET TABLO");
        $objMainSheet->setCellValue("J$idx_summary_table", "Kayıp (saat)");
        $objMainSheet->setCellValue("K$idx_summary_table", "Kayıp %");
        $objMainSheet->setCellValue("L$idx_summary_table", "Kayıp %");
        $objMainSheet->setCellValue("M$idx_summary_table", "Kayıp %");

        $y_ur_kul = 0;
        $y_ur_per = 0;
        $y_ur_kal = 0;
        $y_fab_kul = 0;
        $y_fab_per = 0;
        $y_fab_kal = 0;
        $y_gen_kul = 0;
        $y_gen_per = 0;
        $y_gen_kal = 0;

        $i = $idx_summary_table + 1;
        foreach ($_toplamsureler['C_S']['data'] as $row => $item) {
            if ($this->summary_type == 'all') {
                $val_gen = $_toplamsureler["kayiptoplam_gen_kul"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen_kul"]) * $_toplamsureler["oee_kayip_kul_gen"]) : 0;
            } else {
                $val_gen = $_toplamsureler["kayiptoplam_gen"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen"]) * $_toplamsureler["oee_kayip_gen"]) : 0;
            }
            $y_gen_kul += $val_gen;
            $objMainSheet->mergeCells("H$i:I$i");
            $objMainSheet->setCellValue("H$i", $item["title"]);
            $objMainSheet->setCellValue("J$i", round($item["val"], $ondalikhanegoster));
            $objMainSheet->setCellValue("K$i", '-');
            $objMainSheet->setCellValue("L$i", '-');
            $objMainSheet->setCellValue("M$i", round($val_gen, $ondalikhanegoster));
            $objMainSheet->getStyle("H$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffc000"
                )
            ));
            $objMainSheet->getStyle("M$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffc000"
                )
            ));
            $i++;
        }

        foreach ($_toplamsureler['C_N_S']['data'] as $row => $item) {
            if ($this->summary_type == 'all') {
                $val_fab = $_toplamsureler["kayiptoplam_fab_kul"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab_kul"]) * $_toplamsureler["oee_kayip_kul_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen_kul"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen_kul"]) * $_toplamsureler["oee_kayip_kul_gen"]) : 0;
            } else {
                $val_fab = $_toplamsureler["kayiptoplam_fab"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab"]) * $_toplamsureler["oee_kayip_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen"]) * $_toplamsureler["oee_kayip_gen"]) : 0;
            }
            $y_fab_kul += $val_fab;
            $y_gen_kul += $val_gen;
            $objMainSheet->mergeCells("H$i:I$i");
            $objMainSheet->setCellValue("H$i", $item["title"]);
            $objMainSheet->setCellValue("J$i", round($item["val"], $ondalikhanegoster));
            $objMainSheet->setCellValue("K$i", '-');
            $objMainSheet->setCellValue("L$i", round($val_fab, $ondalikhanegoster));
            $objMainSheet->setCellValue("M$i", round($val_gen, $ondalikhanegoster));
            $objMainSheet->getStyle("H$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                        'rgb' => "92D050"
                )
            ));
            $objMainSheet->getStyle("L$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "92D050"
                )
            ));
            $objMainSheet->getStyle("M$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffc000"
                )
            ));
            $i++;
        }

        foreach ($_toplamsureler['I_S']['data'] as $row => $item) {
            if ($this->summary_type == 'all') {
                $val_ur = $_toplamsureler["kayiptoplam_ur_kul"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur_kul"]) * $_toplamsureler["oee_kayip_kul_ur"]) : 0;
                $val_fab = $_toplamsureler["kayiptoplam_fab_kul"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab_kul"]) * $_toplamsureler["oee_kayip_kul_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen_kul"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen_kul"]) * $_toplamsureler["oee_kayip_kul_gen"]) : 0;
            } else {
                $val_ur = $_toplamsureler["kayiptoplam_ur"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur"]) * $_toplamsureler["oee_kayip_ur"]) : 0;
                $val_fab = $_toplamsureler["kayiptoplam_fab"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab"]) * $_toplamsureler["oee_kayip_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen"]) * $_toplamsureler["oee_kayip_gen"]) : 0;
            }
            $y_ur_kul += $val_ur;
            $y_fab_kul += $val_fab;
            $y_gen_kul += $val_gen;
            $objMainSheet->mergeCells("H$i:I$i");
            $objMainSheet->setCellValue("H$i", $item["title"]);
            $objMainSheet->setCellValue("J$i", round($item["val"], $ondalikhanegoster));
            $objMainSheet->setCellValue("K$i", round($val_ur, $ondalikhanegoster));
            $objMainSheet->setCellValue("L$i", round($val_fab, $ondalikhanegoster));
            $objMainSheet->setCellValue("M$i", round($val_gen, $ondalikhanegoster));
            $objMainSheet->getStyle("H$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffff99"
                )
            ));
            $objMainSheet->getStyle("K$i:M$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffff99"
                )
            ));
            $i++;
        }
        $objMainSheet->mergeCells("F" . ($idx_summary_table + 1) . ":G" . ($i - 1));
        $objMainSheet->getStyle("F" . ($idx_summary_table + 1) . "")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
        $objMainSheet->setCellValue("F" . ($idx_summary_table + 1) . "", "KULLANABİLİRLİK");
        $pi = $i;
        foreach ($_toplamsureler['P_C_S']['data'] as $row => $item) {
            if ($this->summary_type == 'all') {
                $val_ur = $_toplamsureler["kayiptoplam_ur_per"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur_per"]) * $_toplamsureler["oee_kayip_per_ur"]) : 0;
                $val_fab = $_toplamsureler["kayiptoplam_fab_per"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab_per"]) * $_toplamsureler["oee_kayip_per_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen_per"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen_per"]) * $_toplamsureler["oee_kayip_per_gen"]) : 0;
            } else {
                $val_ur = $_toplamsureler["kayiptoplam_ur"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur"]) * $_toplamsureler["oee_kayip_ur"]) : 0;
                $val_fab = $_toplamsureler["kayiptoplam_fab"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab"]) * $_toplamsureler["oee_kayip_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen"]) * $_toplamsureler["oee_kayip_gen"]) : 0;
            }
            $y_ur_per += $val_ur;
            $y_fab_per += $val_fab;
            $y_gen_per += $val_gen;
            $objMainSheet->mergeCells("H$i:I$i");
            $objMainSheet->setCellValue("H$i", $item["title"]);
            $objMainSheet->setCellValue("J$i", round($item["val"], $ondalikhanegoster));
            $objMainSheet->setCellValue("K$i", round($val_ur, $ondalikhanegoster));
            $objMainSheet->setCellValue("L$i", round($val_fab, $ondalikhanegoster));
            $objMainSheet->setCellValue("M$i", round($val_gen, $ondalikhanegoster));
            $objMainSheet->getStyle("H$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffff99"
                )
            ));
            $objMainSheet->getStyle("K$i:M$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffff99"
                )
            ));
            $i++;
        }
        $objMainSheet->mergeCells("F$pi:G" . ($i - 1));
        $objMainSheet->getStyle("F$pi")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
        $objMainSheet->setCellValue("F$pi", "PERFORMANS");

        $ki = $i;
        foreach ($_toplamsureler['E_C_S']['data'] as $row => $item) {
            if ($this->summary_type == 'all') {
                $val_ur = $_toplamsureler["kayiptoplam_ur_kal"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur_kal"]) * $_toplamsureler["oee_kayip_kal_ur"]) : 0;
                $val_fab = $_toplamsureler["kayiptoplam_fab_kal"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab_kal"]) * $_toplamsureler["oee_kayip_kal_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen_kal"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen_kal"]) * $_toplamsureler["oee_kayip_kal_gen"]) : 0;
            } else {
                $val_ur = $_toplamsureler["kayiptoplam_ur"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur"]) * $_toplamsureler["oee_kayip_ur"]) : 0;
                $val_fab = $_toplamsureler["kayiptoplam_fab"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_fab"]) * $_toplamsureler["oee_kayip_fab"]) : 0;
                $val_gen = $_toplamsureler["kayiptoplam_gen"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_gen"]) * $_toplamsureler["oee_kayip_gen"]) : 0;
            }
            $y_ur_kal += $val_ur;
            $y_fab_kal += $val_fab;
            $y_gen_kal += $val_gen;
            $objMainSheet->mergeCells("H$i:I$i");
            $objMainSheet->setCellValue("H$i", $item["title"]);
            $objMainSheet->setCellValue("J$i", round($item["val"], $ondalikhanegoster));
            $objMainSheet->setCellValue("K$i", round($val_ur, $ondalikhanegoster));
            $objMainSheet->setCellValue("L$i", round($val_fab, $ondalikhanegoster));
            $objMainSheet->setCellValue("M$i", round($val_gen, $ondalikhanegoster));
            $objMainSheet->getStyle("H$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffff99"
                )
            ));
            $objMainSheet->getStyle("K$i:M$i")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffff99"
                )
            ));
            $i++;
        }
        $objMainSheet->mergeCells("F$ki:G" . ($i - 1));
        $objMainSheet->getStyle("F$ki")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
        $objMainSheet->setCellValue("F$ki", "KALİTESİZLİK");

        $objMainSheet->mergeCells("H$i:I" . ($i + 1));
        $objMainSheet->getStyle("H$i")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
        $objMainSheet->setCellValue("H$i", "Toplam Kayıp");
        $objMainSheet->getStyle("H$i")->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                'rgb' => "ffff99"
            )
        ));

        $objMainSheet->setCellValue("J$i", "%");
        $objMainSheet->setCellValue("J" . ($i + 1), "Saat");
        $objMainSheet->setCellValue("K$i", round($_toplamsureler["oee_kayip_ur"], $ondalikhanegoster));
        $objMainSheet->setCellValue("K" . ($i + 1), round($_toplamsureler["kayiptoplam_ur"], $ondalikhanegoster));
        $objMainSheet->getStyle("K$i:K" . ($i + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                'rgb' => "ffff00"
            )
        ));

        $objMainSheet->setCellValue("L$i", round($_toplamsureler["oee_kayip_fab"], $ondalikhanegoster));
        $objMainSheet->setCellValue("L" . ($i + 1), round($_toplamsureler["kayiptoplam_fab"], $ondalikhanegoster));
        $objMainSheet->getStyle("L$i:L" . ($i + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                'rgb' => "92D050"
            )
        ));

        $objMainSheet->setCellValue("M$i", round($_toplamsureler["oee_kayip_gen"], $ondalikhanegoster));
        $objMainSheet->setCellValue("M" . ($i + 1), round($_toplamsureler["kayiptoplam_gen"], $ondalikhanegoster));
        $objMainSheet->getStyle("M$i:M" . ($i + 1))->getFill()->applyFromArray(array(
            'fillType' => Fill::FILL_SOLID,
            'startColor' => array(
                'rgb' => "ffc000"
            )
        ));

        $objMainSheet->getStyle("F22:M" . ($i + 1))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
        $objMainSheet->getStyle("F$idx_summary_table:M" . ($i - 1))->applyFromArray($_arr_border);
        $objMainSheet->getStyle("H$i:M" . ($i + 1))->applyFromArray($_arr_border);
        if ($this->summary_type == 'all') {
            //oee kayıp % çarpan değerleri
            $objMainSheet->setCellValue("K24", $_toplamsureler["oee_carpan_kul_ur"]);
            $objMainSheet->setCellValue("L24", $_toplamsureler["oee_carpan_kul_fab"]);
            $objMainSheet->setCellValue("M24", $_toplamsureler["oee_carpan_kul_gen"]);

            $objMainSheet->setCellValue("K25", $_toplamsureler["oee_carpan_per_ur"]);
            $objMainSheet->setCellValue("L25", $_toplamsureler["oee_carpan_per_fab"]);
            $objMainSheet->setCellValue("M25", $_toplamsureler["oee_carpan_per_gen"]);

            $objMainSheet->setCellValue("K26", $_toplamsureler["oee_carpan_kal_ur"]);
            $objMainSheet->setCellValue("L26", $_toplamsureler["oee_carpan_kal_fab"]);
            $objMainSheet->setCellValue("M26", $_toplamsureler["oee_carpan_kal_gen"]);

            //oee kayıp % toplamları
            $objMainSheet->setCellValue("K28", round($_toplamsureler["oee_kayip_kul_ur"], $ondalikhanegoster));
            $objMainSheet->setCellValue("L28", round($_toplamsureler["oee_kayip_kul_fab"], $ondalikhanegoster));
            $objMainSheet->setCellValue("M28", round($_toplamsureler["oee_kayip_kul_gen"], $ondalikhanegoster));

            $objMainSheet->setCellValue("K29", round($_toplamsureler["oee_kayip_per_ur"], $ondalikhanegoster));
            $objMainSheet->setCellValue("L29", round($_toplamsureler["oee_kayip_per_fab"], $ondalikhanegoster));
            $objMainSheet->setCellValue("M29", round($_toplamsureler["oee_kayip_per_gen"], $ondalikhanegoster));

            $objMainSheet->setCellValue("K30", round($_toplamsureler["oee_kayip_kal_ur"], $ondalikhanegoster));
            $objMainSheet->setCellValue("L30", round($_toplamsureler["oee_kayip_kal_fab"], $ondalikhanegoster));
            $objMainSheet->setCellValue("M30", round($_toplamsureler["oee_kayip_kal_gen"], $ondalikhanegoster));

            //oee kayıp süre toplamları
            $objMainSheet->setCellValue("K32", round($_toplamsureler["kayiptoplam_ur_kul"], $ondalikhanegoster));
            $objMainSheet->setCellValue("L32", round($_toplamsureler["kayiptoplam_fab_kul"], $ondalikhanegoster));
            $objMainSheet->setCellValue("M32", round($_toplamsureler["kayiptoplam_gen_kul"], $ondalikhanegoster));

            $objMainSheet->setCellValue("K33", round($_toplamsureler["kayiptoplam_ur_per"], $ondalikhanegoster));
            $objMainSheet->setCellValue("L33", round($_toplamsureler["kayiptoplam_fab_per"], $ondalikhanegoster));
            $objMainSheet->setCellValue("M33", round($_toplamsureler["kayiptoplam_gen_per"], $ondalikhanegoster));

            $objMainSheet->setCellValue("K34", round($_toplamsureler["kayiptoplam_ur_kal"], $ondalikhanegoster));
            $objMainSheet->setCellValue("L34", round($_toplamsureler["kayiptoplam_fab_kal"], $ondalikhanegoster));
            $objMainSheet->setCellValue("M34", round($_toplamsureler["kayiptoplam_gen_kal"], $ondalikhanegoster));
        } else {
            //oee kayıp % toplamları
            $objMainSheet->setCellValue("K25", round($y_ur_kul, $ondalikhanegoster));
            $objMainSheet->setCellValue("L25", round($y_fab_kul, $ondalikhanegoster));
            $objMainSheet->setCellValue("M25", round($y_gen_kul, $ondalikhanegoster));

            $objMainSheet->setCellValue("K26", round($y_ur_per, $ondalikhanegoster));
            $objMainSheet->setCellValue("L26", round($y_fab_per, $ondalikhanegoster));
            $objMainSheet->setCellValue("M26", round($y_gen_per, $ondalikhanegoster));

            $objMainSheet->setCellValue("K27", round($y_ur_kal, $ondalikhanegoster));
            $objMainSheet->setCellValue("L27", round($y_fab_kal, $ondalikhanegoster));
            $objMainSheet->setCellValue("M27", round($y_gen_kal, $ondalikhanegoster));
        }
        for ($j = 2;$j <= $cs_finish_colIndex;$j++) {
            $_strcol = Coordinate::stringFromColumnIndex($j);
            $objMainSheet->getColumnDimension($_strcol)->setAutoSize(true);
        }
        $objWriter = false;
        if ($_islostdetail && count($recordsLost) > 0 && ($_SERVER['HTTP_HOST'] == '192.168.1.10' || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
            //özet sekmesi başlangıç
            $objPHPExcel->createSheet();
            $objSheetOeeOzet = $objPHPExcel->setActiveSheetIndex($_isdetail ? 5 : 1);
            $objSheetOeeOzet->getColumnDimension('B')->setVisible(false);
            $ws_name = "OEE_Ozet";
            $objSheetOeeOzet->setTitle($ws_name);
            $objSheetOeeOzet->setCellValue("A2", "OEE");
            $objSheetOeeOzet->setCellValue("A3", "Kul");
            $objSheetOeeOzet->setCellValue("A4", "Per");
            $objSheetOeeOzet->setCellValue("A5", "Kal");

            $objSheetOeeOzet->getStyle("A2:A5")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "c6d9f1"
                )
            ));

            $objSheetOeeOzet->getStyle("C1:C5")->getFill()->applyFromArray(array(
                'fillType' => Fill::FILL_SOLID,
                'startColor' => array(
                    'rgb' => "ffff00"
                )
            ));
            $objSheetOeeOzet->getStyle("A1:C5")->applyFromArray($_arr_border);

            $objSheetOeeOzet->setCellValue("C1", "ÜRETİM MÜD.");
            //$objSheetOeeOzet->setCellValue("D1", "FABRİKA MÜD.");
            //$objSheetOeeOzet->setCellValue("E1", "GENEL MÜD.");
            $objSheetOeeOzet->getColumnDimension('C')->setAutoSize(true);
            $objSheetOeeOzet->setCellValue("C2", round($_toplamsureler["oee_ur"], $ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("D2", round($_toplamsureler["oee_fab"],$ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("E2", round($_toplamsureler["oee_gen"],$ondalikhanegoster));

            $objSheetOeeOzet->setCellValue("C3", round($y_ur_kul, $ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("D3", round($y_fab_kul, $ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("E3", round($y_gen_kul, $ondalikhanegoster));

            $objSheetOeeOzet->setCellValue("C4", round($y_ur_per, $ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("D4", round($y_fab_per, $ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("E4", round($y_gen_per, $ondalikhanegoster));

            $objSheetOeeOzet->setCellValue("C5", round($y_ur_kal, $ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("D5", round($y_fab_kal, $ondalikhanegoster));
            //$objSheetOeeOzet->setCellValue("E5", round($y_gen_kal, $ondalikhanegoster));
            $idx_summary_table_2 = 2;
            $objSheetOeeOzet->mergeCells("F$idx_summary_table_2:G$idx_summary_table_2");
            $objSheetOeeOzet->setCellValue("F$idx_summary_table_2", "OEE ANALİZİ ÖZET TABLO");
            $objSheetOeeOzet->setCellValue("H$idx_summary_table_2", "Kayıp (saat)");
            $objSheetOeeOzet->setCellValue("I$idx_summary_table_2", "Kayıp %");

            $i = $idx_summary_table_2 + 1;
            foreach ($_toplamsureler['I_S']['data'] as $row => $item) {
                if ($this->summary_type == 'all') {
                    $val_ur = $_toplamsureler["kayiptoplam_ur_kul"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur_kul"]) * $_toplamsureler["oee_kayip_kul_ur"]) : 0;
                } else {
                    $val_ur = $_toplamsureler["kayiptoplam_ur"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur"]) * $_toplamsureler["oee_kayip_ur"]) : 0;
                }
                $objSheetOeeOzet->setCellValue("G$i", $item["title"]);
                $objSheetOeeOzet->setCellValue("H$i", round($item["val"], $ondalikhanegoster));
                $objSheetOeeOzet->setCellValue("I$i", round($val_ur, $ondalikhanegoster));
                $objSheetOeeOzet->getStyle("G$i")->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                        'rgb' => "ffff99"
                    )
                ));
                $objSheetOeeOzet->getStyle("H$i:I$i")->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                        'rgb' => "ffff99"
                    )
                ));
                $i++;
            }
            $objSheetOeeOzet->mergeCells("F" . ($idx_summary_table_2 + 1) . ":F" . ($i - 1));
            $objSheetOeeOzet->getStyle("F" . ($idx_summary_table_2 + 1) . "")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objSheetOeeOzet->setCellValue("F" . ($idx_summary_table_2 + 1) . "", "KULLANABİLİRLİK");

            $pi = $i;
            foreach ($_toplamsureler['P_C_S']['data'] as $row => $item) {
                if ($this->summary_type == 'all') {
                    $val_ur = $_toplamsureler["kayiptoplam_ur_per"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur_per"]) * $_toplamsureler["oee_kayip_per_ur"]) : 0;
                } else {
                    $val_ur = $_toplamsureler["kayiptoplam_ur"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur"]) * $_toplamsureler["oee_kayip_ur"]) : 0;
                }
                $objSheetOeeOzet->setCellValue("G$i", $item["title"]);
                $objSheetOeeOzet->setCellValue("H$i", round($item["val"], $ondalikhanegoster));
                $objSheetOeeOzet->setCellValue("I$i", round($val_ur, $ondalikhanegoster));
                $objSheetOeeOzet->getStyle("G$i")->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                        'rgb' => "ffff99"
                    )
                ));
                $objSheetOeeOzet->getStyle("H$i:I$i")->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                        'rgb' => "ffff99"
                    )
                ));
                $i++;
            }
            $objSheetOeeOzet->mergeCells("F$pi:F" . ($i - 1));
            $objSheetOeeOzet->getStyle("F$pi")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objSheetOeeOzet->setCellValue("F$pi", "PERFORMANS");

            $ki = $i;
            foreach ($_toplamsureler['E_C_S']['data'] as $row => $item) {
                if ($this->summary_type == 'all') {
                    $val_ur = $_toplamsureler["kayiptoplam_ur_kal"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur_kal"]) * $_toplamsureler["oee_kayip_kal_ur"]) : 0;
                } else {
                    $val_ur = $_toplamsureler["kayiptoplam_ur"] > 0 ? (($item["val"] / $_toplamsureler["kayiptoplam_ur"]) * $_toplamsureler["oee_kayip_ur"]) : 0;
                }
                $objSheetOeeOzet->setCellValue("G$i", $item["title"]);
                $objSheetOeeOzet->setCellValue("H$i", round($item["val"], $ondalikhanegoster));
                $objSheetOeeOzet->setCellValue("I$i", round($val_ur, $ondalikhanegoster));
                $objSheetOeeOzet->getStyle("G$i")->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                        'rgb' => "ffff99"
                    )
                ));
                $objSheetOeeOzet->getStyle("H$i:I$i")->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                        'rgb' => "ffff99"
                    )
                ));
                $i++;
            }
            $objSheetOeeOzet->mergeCells("F$ki:F" . ($i - 1));
            $objSheetOeeOzet->getStyle("F$ki")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objSheetOeeOzet->setCellValue("F$ki", "KALİTESİZLİK");

            $ozet_tablo_son_satir = $i - 1;
            $objSheetOeeOzet->getStyle("F$idx_summary_table_2:I" . $ozet_tablo_son_satir)->applyFromArray($_arr_border);
            for ($j = 6;$j <= 12;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objSheetOeeOzet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            //yığın grafik başlangıç
            $arrDataSeriesLabels = array(
                new DataSeriesValues('String', $ws_name . '!$A$2:$A$2', null, 1),
                new DataSeriesValues('String', $ws_name . '!$A$3:$A$3', null, 1),
                new DataSeriesValues('String', $ws_name . '!$A$4:$A$4', null, 1),
                new DataSeriesValues('String', $ws_name . '!$A$5:$A$5', null, 1),
            );
            $arrCategorysDataSeries = array(
                new DataSeriesValues('String', $ws_name . '!$A$1', null, 1),
            );
            $arrDataSeriesValues = array(
                new DataSeriesValues('Number', $ws_name . '!$B$2:$D$2', null, 3),
                new DataSeriesValues('Number', $ws_name . '!$B$3:$D$3', null, 3),
                new DataSeriesValues('Number', $ws_name . '!$B$4:$D$4', null, 3),
                new DataSeriesValues('Number', $ws_name . '!$B$5:$D$5', null, 3),
            );
            $objSeries = new DataSeries(
                DataSeries::TYPE_BARCHART,// plotType
                DataSeries::GROUPING_PERCENT_STACKED,// plotGrouping
                range(0, count($arrDataSeriesValues) - 1),// plotOrder
                $arrDataSeriesLabels,// plotLabel
                $arrCategorysDataSeries,// plotCategory
                $arrDataSeriesValues// plotValues
            );

            $objSeries->setPlotDirection(DataSeries::DIRECTION_COL);

            $layout = new Layout();
            $layout->setShowVal(true);

            $objPlotArea = new PlotArea($layout, array($objSeries));
            $objLegend = new Legend(Legend::POSITION_BOTTOM, null, false);
            // $objTitle = new Title('OEE Kayıp Dağılımı');

            $xAxisLabel = new Title('OEE Kayıp Dağılımı');
            $xaxis = new Axis();
            $xaxis->setAxisOptionsProperties('none', null, null, null, null, null, 0, 0, null, null);

            $objChart = new Chart(
                'chart_oee_ozet',// name
                null,// title
                $objLegend,// legend
                $objPlotArea,// plotArea
                true,// plotVisibleOnly
                DataSeries::EMPTY_AS_GAP,// displayBlanksAs
                $xAxisLabel,//xAxisLabel
                null,//yAxisLabel
                null,//yAxis
                $xaxis,//xAxis
            );

            $objChart->setTopLeftPosition('A8');
            $objChart->setBottomRightPosition('E32');
            $objSheetOeeOzet->addChart($objChart);
            //yığın grafik bitiş

            //bar grafik başlangıcı

            $dataSeriesLabels = [
                new DataSeriesValues('String', $ws_name . '!$H$2:$H$2', null, 1), // 'kayıp saat'
                new DataSeriesValues('String', $ws_name . '!$I$2:$I$2', null, 1), // 'kayıp %'
            ];
            $xAxisTickValues = [
                new DataSeriesValues('String', $ws_name . '!$F$3:$G$' . $ozet_tablo_son_satir, null, $ozet_tablo_son_satir - 2),
            ];
            $dataSeriesValues = [
                new DataSeriesValues('Number', $ws_name . '!$H$3:$H$' . $ozet_tablo_son_satir, null, $ozet_tablo_son_satir - 2),
                new DataSeriesValues('Number', $ws_name . '!$I$3:$I$' . $ozet_tablo_son_satir, null, $ozet_tablo_son_satir - 2),
            ];
            $series = new DataSeries(
                DataSeries::TYPE_BARCHART, // plotType
                DataSeries::GROUPING_CLUSTERED, // plotGrouping
                range(0, count($dataSeriesValues) - 1), // plotOrder
                $dataSeriesLabels, // plotLabel
                $xAxisTickValues, // plotCategory
                $dataSeriesValues // plotValues
            );
            $series->setPlotDirection(DataSeries::DIRECTION_COL);
            $plotArea = new PlotArea(null, [$series]);
            $legend = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title = new Title('Kayıpların Dağılımı');
            // $xAxisLabel = new Title('Financial Period');
            // $yAxisLabel = new Title('Kayıp (saat)');

            $chart = new Chart(
                'chart_kayip_dagilim', // name
                $title, // title
                $legend, // legend
                $plotArea, // plotArea
                true, // plotVisibleOnly
                DataSeries::EMPTY_AS_GAP, // displayBlanksAs
                null, // xAxisLabel
                null  // yAxisLabel
            );
            $chart->setTopLeftPosition('K2');
            $chart->setBottomRightPosition('Z40');
            $objSheetOeeOzet->addChart($chart);
            //bar grafik bitiş

            //özet sekmesi bitiş
            /*
                        //kayıp detay sekmesi başlangıç
                        $networks = array(
                            array(	'data' => $recordsLost,
                            'args' => array(
                                "Kayip_Detay",
                                2 + count($recordsLost) + 1 + 3, 2,
                                new Groups(array( 'losttype', 'task' )),
                                new Groups(),
                                new Groups(array( 'suredk' ))
                                )
                            ),
                            array(	'data' => $recordsLost,
                            'args' => array(
                                "Kisa_Durus_Detay",
                                2 + count($recordsLost) + 1 + 3, 2,
                                new Groups(array( 'info', 'losttype', 'task' )),
                                new Groups(),
                                new Groups(array( 'suredk' ))
                                )
                            )
                        );
                        $range1=false;
                        $range2=false;
                        foreach ($networks as $index => $network) {
                            $range = $objPHPExcel->addData($recordsLost, $network['args'][0]);
                            if ($range1===false) {
                                $range1=$range;
                            }
                            if ($range2===false) {
                                $range2=$range;
                            }
                            $objPHPExcel->addNewPivotTable($recordsLost, $range, ...$network['args']);
                        }

                        $objSheetKayipDetay = $objPHPExcel->setActiveSheetIndex($_isdetail ? 5 : 2);
                        $boundary = Coordinate::rangeBoundaries($range1);
                        //$leftCol = $boundary[0][0];
                        //$topRow = $boundary[0][1];
                        //$dilimler = explode(":", $range);
                        //$first_data_row=preg_replace( '/[^0-9]/', '', $dilimler[0] ) ;
                        //$last_data_row=preg_replace( '/[^0-9]/', '', $dilimler[0] ) ;
                        $first_data_row=intval($boundary[0][1]);
                        $last_data_row=intval($boundary[1][1]);
                        $table_header_row=$last_data_row+3;
                        $data_start_row=$table_header_row+2;
                        // $objSheetKayipDetay->setCellValue("A1", $first_data_row);
                        // $objSheetKayipDetay->setCellValue("B1", $last_data_row);
                        $objSheetKayipDetay->setCellValue("B$table_header_row", "Kayıp Tipi");
                        $objSheetKayipDetay->setCellValue("C$table_header_row", "Görev");
                        $objSheetKayipDetay->setCellValue("D$table_header_row", "Süre (dk)");
                        $objSheetKayipDetay->getColumnDimension('B')->setAutoSize(true);
                        $objSheetKayipDetay->getColumnDimension('C')->setAutoSize(true);
                        $objSheetKayipDetay->getColumnDimension('D')->setAutoSize(true);
                        //$objSheetKayipDetay->getColumnDimension('E')->setAutoSize(true);
                        //kayıp detay grafik başlangıcı

                        $ws_name="Kayip_Detay";
                        $dataSeriesLabels = [
                            new DataSeriesValues('String', $ws_name.'!$D$'.$table_header_row.':$D$'.$table_header_row, null, 1), // 'süre dk'
                        ];
                        $xAxisTickValues = [
                            new DataSeriesValues('String', $ws_name.'!$C$'.$data_start_row.':$C$'.($data_start_row+30), null, 30),
                        ];
                        $dataSeriesValues = [
                            new DataSeriesValues('Number', $ws_name.'!$D$'.$data_start_row.':$D$'.($data_start_row+30), null, 30),
                        ];
                        $series = new DataSeries(
                            DataSeries::TYPE_BARCHART, // plotType
                            DataSeries::GROUPING_CLUSTERED, // plotGrouping
                            range(0, count($dataSeriesValues) - 1), // plotOrder
                            $dataSeriesLabels, // plotLabel
                            $xAxisTickValues, // plotCategory
                            $dataSeriesValues        // plotValues
                        );
                        $series->setPlotDirection(DataSeries::DIRECTION_COL);
                        $plotArea = new PlotArea(null, [$series]);
                        $legend = new Legend(Legend::POSITION_BOTTOM, null, false);

                        $title = new Title('Referans Bazında Kayıp Detay Analizi');
                        // $xAxisLabel = new Title('Financial Period');
                        // $yAxisLabel = new Title('Kayıp (saat)');

                        $chart = new Chart(
                            'chart_kayip_detay', // name
                            $title, // title
                            $legend, // legend
                            $plotArea, // plotArea
                            true, // plotVisibleOnly
                            DataSeries::EMPTY_AS_GAP, // displayBlanksAs
                            null, // xAxisLabel
                            null  // yAxisLabel
                        );
                        $chart->setTopLeftPosition('F'.($last_data_row+2));
                        $chart->setBottomRightPosition('Z'.($last_data_row+42));

                        $objSheetKayipDetay->addChart($chart);

                        //kayıp detay grafik bitişi

                        //kayıp detay sekmesi bitiş
                        //kısa duruş detay sekmesi başlangıç
                        $objSheetKisaDurusDetay = $objPHPExcel->setActiveSheetIndex($_isdetail ? 6 : 3);
                        $boundary = Coordinate::rangeBoundaries($range2);
                        $first_data_row=intval($boundary[0][1]);
                        $last_data_row=intval($boundary[1][1]);
                        $table_header_row=$last_data_row+3;
                        $data_start_row=$table_header_row+2;
                        // $objSheetKisaDurusDetay->setCellValue("A1", $first_data_row);
                        // $objSheetKisaDurusDetay->setCellValue("B1", $last_data_row);
                        $objSheetKisaDurusDetay->setCellValue("B$table_header_row", "Bilgi");
                        $objSheetKisaDurusDetay->setCellValue("C$table_header_row", "Kayıp Tipi");
                        $objSheetKisaDurusDetay->setCellValue("D$table_header_row", "Görev");
                        $objSheetKisaDurusDetay->setCellValue("E$table_header_row", "Süre (dk)");
                        $objSheetKisaDurusDetay->getColumnDimension('B')->setAutoSize(true);
                        $objSheetKisaDurusDetay->getColumnDimension('C')->setAutoSize(true);
                        $objSheetKisaDurusDetay->getColumnDimension('D')->setAutoSize(true);
                        $objSheetKisaDurusDetay->getColumnDimension('E')->setAutoSize(true);
                        //kısa duruş detay sekmesi bitiş
                        */
            $objPHPExcel->setActiveSheetIndex(0);
            //$objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
            $objWriter = new Xlsx($objPHPExcel);
        } else {
            $objPHPExcel->setActiveSheetIndex(0);
            $objWriter = new Xlsx($objPHPExcel);
            //$objWriter = IOFactory::createWriter( $objPHPExcel, 'Xlsx' );
        }
        // $objPHPExcel->setActiveSheetIndex(0);
        //$objWriter->setOffice2003Compatibility(true);
        $objWriter->setIncludeCharts(true);
        $response = new StreamedResponse(
            function () use ($objWriter) {
                $objWriter->save('php://output');
            },
            200,
            array()
        );
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $name . '.xlsx'
        );
        //$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Type', 'application/download');
        $response->headers->set('Expires', '0');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->sendHeaders();
        //$objPHPExcel->disconnectWorksheets();
        //unset($objPHPExcel);
        return $response;
    }

    //$_viewoption==='list'
    public function oee_sablon_2($_shortlost, $_viewoption, $_isdetail, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam)
    {
        $name = "OEE_" . date("Y-m-d_H:i:s");
        $_arr_bold = array(
            'font'  => array(
                'bold'  => true
            ));
        $_arr_color_red = array(
            'font'  => array(
                'color' => array('rgb' => 'FF0000')
            ));
        $_arr_border = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => Border::BORDER_THIN
                )
            )
        );
        $ondalikhane = 4;
        $ondalikhanegoster = 4;
        $uri = $this->uri;
        if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
            $ondalikhane = 1;
            $ondalikhanegoster = 1;
        }
        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getProperties()
            ->setCreator("Kaitek - Reports")
            ->setLastModifiedBy("Kaitek - Reports")
            ->setTitle("Kaitek - Reports")
            ->setSubject("Kaitek - Reports")
            ->setDescription($name)
            ->setKeywords('Verimot')
            ->setCategory('-');
        $objMainSheet = $objPHPExcel->getActiveSheet();
        $objMainSheet->mergeCells("A2:C2");
        $objMainSheet->setCellValue("A2", $_s . "-" . $_f);
        $r_h0 = 3;
        $r_h1 = 4;
        $r_h2 = 5;
        $r = 6;
        $header = false;
        $clients = $this->oeeCalculate($_shortlost, $_s, $_f, $_cl, $_jobrotation, $_jobrotationteam, $this->fieldname_opname, $this->prod_concat_type, $this->uri);
        foreach ($clients as $row => $client) {
            $idx_col = 1;

            if (!$header) {
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "İstasyon");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), $clients[$row]["C_K_S"]["title"]);
                $cs_col_first = false;
                foreach ($clients[$row]["C_S"]["data"] as $cs_row => $cs_item) {
                    $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                    if (!$cs_col_first) {
                        $cs_col_first = $_strcol;
                    }
                    $objMainSheet->setCellValue($_strcol . ($r_h2), $cs_item["title"]);
                }
                $objMainSheet->getStyle($cs_col_first . $r_h2 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffc000"
                    )
                ));
                $objMainSheet->mergeCells($cs_col_first . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($cs_col_first . ($r_h1), $clients[$row]["C_S"]["title"]);

                $cns_col_first = false;
                foreach ($clients[$row]["C_N_S"]["data"] as $cs_row => $cs_item) {
                    $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                    if (!$cns_col_first) {
                        $cns_col_first = $_strcol;
                    }
                    $objMainSheet->setCellValue($_strcol . ($r_h2), $cs_item["title"]);
                }
                $objMainSheet->getStyle($cns_col_first . $r_h2 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "92D050"
                    )
                ));
                $objMainSheet->mergeCells($cns_col_first . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($cns_col_first . ($r_h1), $clients[$row]["C_N_S"]["title"]);

                $is_col_first = false;
                foreach ($clients[$row]["I_S"]["data"] as $cs_row => $cs_item) {
                    $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                    if (!$is_col_first) {
                        $is_col_first = $_strcol;
                    }
                    $objMainSheet->setCellValue($_strcol . ($r_h2), $cs_item["title"]);
                }
                $objMainSheet->getStyle($is_col_first . $r_h2 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffff99"
                    )
                ));
                $objMainSheet->mergeCells($is_col_first . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($is_col_first . ($r_h1), $clients[$row]["I_S"]["title"]);

                $pcs_col_first = false;
                foreach ($clients[$row]["P_C_S"]["data"] as $cs_row => $cs_item) {
                    $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                    if (!$pcs_col_first) {
                        $pcs_col_first = $_strcol;
                    }
                    $objMainSheet->setCellValue($_strcol . ($r_h2), $cs_item["title"]);
                }
                $objMainSheet->getStyle($pcs_col_first . $r_h2 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffff99"
                    )
                ));
                $objMainSheet->mergeCells($pcs_col_first . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($pcs_col_first . ($r_h1), $clients[$row]["P_C_S"]["title"]);

                $ecs_col_first = false;
                foreach ($clients[$row]["E_C_S"]["data"] as $cs_row => $cs_item) {
                    $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                    if (!$ecs_col_first) {
                        $ecs_col_first = $_strcol;
                    }
                    $objMainSheet->setCellValue($_strcol . ($r_h2), $cs_item["title"]);
                }
                $objMainSheet->getStyle($ecs_col_first . $r_h2 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffff99"
                    )
                ));
                $objMainSheet->mergeCells($ecs_col_first . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($ecs_col_first . ($r_h1), $clients[$row]["E_C_S"]["title"]);
                //oee kayıp saat
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $header_kayip_saat = $_strcol;
                $header_kayip_saat_ur = $_strcol;
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kullanabilirlik");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Performans");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kalite");
                $objMainSheet->mergeCells($header_kayip_saat_ur . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($header_kayip_saat_ur . $r_h1, 'Üretim Müdürü');
                $objMainSheet->getStyle($header_kayip_saat_ur . $r_h1 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffff00"
                    )
                ));

                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $header_kayip_saat_fab = $_strcol;
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kullanabilirlik");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Performans");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kalite");
                $objMainSheet->mergeCells($header_kayip_saat_fab . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($header_kayip_saat_fab . $r_h1, 'Fabrika Müdürü');
                $objMainSheet->getStyle($header_kayip_saat_fab . $r_h1 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "92D050"
                    )
                ));

                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $header_kayip_saat_gen = $_strcol;
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kullanabilirlik");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Performans");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kalite");
                $objMainSheet->mergeCells($header_kayip_saat_gen . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($header_kayip_saat_gen . $r_h1, 'Genel Müdür');
                $objMainSheet->getStyle($header_kayip_saat_gen . $r_h1 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffc000"
                    )
                ));

                $objMainSheet->mergeCells($header_kayip_saat . $r_h0 . ":" . $_strcol . $r_h0);
                $objMainSheet->setCellValue($header_kayip_saat . $r_h0, 'OEE KAYIPLARI SAAT');

                //oee çarpanları
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $header_yuzde_carpan = $_strcol;
                $header_yuzde_carpan_ur = $_strcol;
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kullanabilirlik");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Performans");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kalite");
                $objMainSheet->mergeCells($header_yuzde_carpan_ur . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($header_yuzde_carpan_ur . $r_h1, 'Üretim Müdürü');
                $objMainSheet->getStyle($header_yuzde_carpan_ur . $r_h1 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffff00"
                    )
                ));

                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $header_yuzde_carpan_fab = $_strcol;
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kullanabilirlik");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Performans");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kalite");
                $objMainSheet->mergeCells($header_yuzde_carpan_fab . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($header_yuzde_carpan_fab . $r_h1, 'Fabrika Müdürü');
                $objMainSheet->getStyle($header_yuzde_carpan_fab . $r_h1 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "92D050"
                    )
                ));

                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $header_yuzde_carpan_gen = $_strcol;
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kullanabilirlik");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Performans");
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r_h2), "Kalite");
                $objMainSheet->mergeCells($header_yuzde_carpan_gen . $r_h1 . ":" . $_strcol . $r_h1);
                $objMainSheet->setCellValue($header_yuzde_carpan_gen . $r_h1, 'Genel Müdür');
                $objMainSheet->getStyle($header_yuzde_carpan_gen . $r_h1 . ":" . $_strcol . ($r_h2))->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffc000"
                    )
                ));

                $objMainSheet->mergeCells($header_yuzde_carpan . $r_h0 . ":" . $_strcol . $r_h0);
                $objMainSheet->setCellValue($header_yuzde_carpan . $r_h0, 'OEE KAYIPLARI %');

                //oee
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $header_oee = $_strcol;
                $objMainSheet->mergeCells($_strcol . $r_h1 . ":" . $_strcol . $r_h2);
                $objMainSheet->setCellValue($_strcol . $r_h1, 'Üretim Müdürü');
                $objMainSheet->getStyle($_strcol . $r_h1)->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffff00"
                    )
                ));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->mergeCells($_strcol . $r_h1 . ":" . $_strcol . $r_h2);
                $objMainSheet->setCellValue($_strcol . $r_h1, 'Fabrika Müdürü');
                $objMainSheet->getStyle($_strcol . $r_h1)->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "92D050"
                    )
                ));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->mergeCells($_strcol . $r_h1 . ":" . $_strcol . $r_h2);
                $objMainSheet->setCellValue($_strcol . $r_h1, 'Genel Müdür');
                $objMainSheet->getStyle($_strcol . $r_h1)->getFill()->applyFromArray(array(
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => array(
                            'rgb' => "ffc000"
                    )
                ));

                $objMainSheet->mergeCells($header_oee . $r_h0 . ":" . $_strcol . $r_h0);
                $objMainSheet->setCellValue($header_oee . $r_h0, 'OEE');

                $objMainSheet->getStyle('A3:' . $_strcol . $r_h2)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objMainSheet->getStyle("A1:" . $_strcol . $r_h2)->applyFromArray($_arr_bold);
                $objMainSheet->getStyle("A" . $r_h2 . ":" . $_strcol . $r_h2)->applyFromArray($_arr_border);
                $objMainSheet->getStyle("C" . $r_h1 . ":" . $_strcol . $r_h2)->applyFromArray($_arr_border);
                $objMainSheet->getStyle($header_kayip_saat . $r_h0 . ":" . $_strcol . $r_h2)->applyFromArray($_arr_border);
                $header = true;
                $idx_col = 1;
            }

            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $row);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["C_K_S"]["toplam"]);
            foreach ($clients[$row]["C_S"]["data"] as $cs_row => $cs_item) {
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r), $cs_item["val"]);
            }
            foreach ($clients[$row]["C_N_S"]["data"] as $cns_row => $cns_item) {
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r), $cns_item["val"]);
            }
            foreach ($clients[$row]["I_S"]["data"] as $is_row => $is_item) {
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r), $is_item["val"]);
            }
            foreach ($clients[$row]["P_C_S"]["data"] as $pcs_row => $pcs_item) {
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r), $pcs_item["val"]);
            }
            foreach ($clients[$row]["E_C_S"]["data"] as $ecs_row => $ecs_item) {
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                $objMainSheet->setCellValue($_strcol . ($r), $ecs_item["val"]);
            }
            //oee kayıp saat
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_ur_kul"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_ur_per"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_ur_kal"]);

            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_fab_kul"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_fab_per"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_fab_kal"]);

            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_gen_kul"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_gen_per"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["kayiptoplam_gen_kal"]);
            //oee kayıp
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            if ($clients[$row]["kayiptoplam_ur"] > 0) {
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kul_ur"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_ur_kul"] / $clients[$row]["kayiptoplam_ur"]) * (100 - $clients[$row]["oee_ur"]), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_per_ur"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_ur_per"] / $clients[$row]["kayiptoplam_ur"]) * (100 - $clients[$row]["oee_ur"]), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kal_ur"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_ur_kal"] / $clients[$row]["kayiptoplam_ur"]) * (100 - $clients[$row]["oee_ur"]), $ondalikhanegoster));
            } else {
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_per_ur"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kal_ur"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
            }
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            if ($clients[$row]["kayiptoplam_fab"] > 0) {
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kul_fab"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_fab_kul"] / $clients[$row]["kayiptoplam_fab"]) * (100 - $clients[$row]["oee_fab"]), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_per_fab"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_fab_per"] / $clients[$row]["kayiptoplam_fab"]) * (100 - $clients[$row]["oee_fab"]), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kal_fab"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_fab_kal"] / $clients[$row]["kayiptoplam_fab"]) * (100 - $clients[$row]["oee_fab"]), $ondalikhanegoster));
            } else {
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_per_fab"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kal_fab"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
            }
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            if ($clients[$row]["kayiptoplam_gen"] > 0) {
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kul_gen"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_gen_kul"] / $clients[$row]["kayiptoplam_gen"]) * (100 - $clients[$row]["oee_gen"]), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_per_gen"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_gen_per"] / $clients[$row]["kayiptoplam_gen"]) * (100 - $clients[$row]["oee_gen"]), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kal_gen"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(($clients[$row]["kayiptoplam_gen_kal"] / $clients[$row]["kayiptoplam_gen"]) * (100 - $clients[$row]["oee_gen"]), $ondalikhanegoster));
            } else {
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_per_gen"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
                //$objMainSheet->setCellValue($_strcol.($r), $clients[$row]["oee_carpan_kal_gen"]);
                $objMainSheet->setCellValue($_strcol . ($r), round(0, $ondalikhanegoster));
            }
            //oee
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["oee_ur"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["oee_fab"]);
            $_strcol = Coordinate::stringFromColumnIndex($idx_col++);
            $objMainSheet->setCellValue($_strcol . ($r), $clients[$row]["oee_gen"]);
            $r++;
        }
        $objMainSheet->setTitle("OEE ANALİZİ");
        for ($j = 1;$j <= $idx_col;$j++) {
            $_strcol = Coordinate::stringFromColumnIndex($j);
            $objMainSheet->getColumnDimension($_strcol)->setAutoSize(true);
        }

        $objWriter = new Xlsx($objPHPExcel);
        //$objWriter->setOffice2003Compatibility(true);
        //$objWriter->setIncludeCharts(TRUE);
        $response = new StreamedResponse(
            function () use ($objWriter) {
                $objWriter->save('php://output');
            },
            200,
            array()
        );
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $name . '.xlsx'
        );
        //$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Type', 'application/download');
        $response->headers->set('Expires', '0');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->sendHeaders();
        //$objPHPExcel->disconnectWorksheets();
        //unset($objPHPExcel);
        return $response;
    }

    /**
     * @Route(path="/ReportOee", name="ReportOee-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            //json_decode($clients->getContent())->records
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0, //$this instanceof BaseAuditControllerInterface,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
            return $this->render('Reports/Oee.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ReportOee/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ReportOee-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/m2m/CalculateOee", name="ReportOee-calculate", options={"expose"=true}, methods={"POST"})
     */
    public function getCalculateOeeAction(Request $request, $_locale)
    {
        $request->attributes->set('_isDCSService', true);
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sqlClients = "SELECT code from clients where workflow not in ('El İşçiliği','Kataforez','CNC','Gölge Cihaz') order by code";
        $stmt = $conn->prepare($sqlClients);
        $stmt->execute();
        $recordsClients = $stmt->fetchAll();

        $sqlJR = "SELECT id,code,beginval,endval,concat(current_date,' ',cast(current_time as varchar(8))) zaman,CURRENT_TIMESTAMP(0) ts
            ,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) \"day\" 
            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start
            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end
        from job_rotations 
        where (finish is null or finish<now())
            and (
            (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)
            or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )
        )";
        $stmtJR = $conn->prepare($sqlJR);
        $stmtJR->execute();
        $recordsJR = $stmtJR->fetchAll();
        $recordJR = $recordsJR[0];

        foreach ($recordsClients as $rowc => $datac) {
            $_cl = $datac["code"];
            $records = $this->oeeCalculate(10, $recordJR["day"], $recordJR["day"], $_cl, $recordJR["code"], null, $this->fieldname_opname, $this->prod_concat_type, $this->uri);
            //$records=$this->oeeCalculate(10, $recordJR["day"], $recordJR["day"], $_cl, null, null, $this->fieldname_opname, $this->prod_concat_type, $this->uri);
            $fBelowZero = false;
            foreach ($records as $row => $data) {
                $sqlfind = "SELECT * FROM client_oee_details where client=:client and day=:day and jobrotation=:jobrotation";
                $stmtfind = $conn->prepare($sqlfind);
                $stmtfind->bindValue('client', $_cl);
                $stmtfind->bindValue('day', $recordJR["day"]);
                $stmtfind->bindValue('jobrotation', $recordJR["code"]);
                $stmtfind->execute();
                $recordsfind = $stmtfind->fetchAll();
                //if(intval($data["oee_ur"])>0){
                if (count($recordsfind) === 0) {
                    $sqlInsert = "insert into client_oee_details (client,day,jobrotation,kul,per,kal,oee) values (:client,:day,:jobrotation,:kul,:per,:kal,:oee)";
                    $stmtInsert = $conn->prepare($sqlInsert);
                    $stmtInsert->bindValue('client', $_cl);
                    $stmtInsert->bindValue('day', $recordJR["day"]);
                    $stmtInsert->bindValue('jobrotation', $recordJR["code"]);
                    $stmtInsert->bindValue('kul', ($data["oee_carpan_kul_ur"] > 0 ? round($data["oee_carpan_kul_ur"], 4) : 0));
                    $stmtInsert->bindValue('per', ($data["oee_carpan_per_ur"] > 0 ? round($data["oee_carpan_per_ur"], 4) : 0));
                    $stmtInsert->bindValue('kal', ($data["oee_carpan_kal_ur"] > 0 ? round($data["oee_carpan_kal_ur"], 4) : 0));
                    $stmtInsert->bindValue('oee', ($data["oee_ur"] > 0 ? round($data["oee_ur"], 4) : 0));
                    $stmtInsert->execute();
                } else {
                    $sqlUpdate = "update client_oee_details 
                        set kul=:kul,per=:per,kal=:kal,oee=:oee
                        where client=:client and day=:day and jobrotation=:jobrotation";
                    $stmtUpdate = $conn->prepare($sqlUpdate);
                    $stmtUpdate->bindValue('client', $_cl);
                    $stmtUpdate->bindValue('day', $recordJR["day"]);
                    $stmtUpdate->bindValue('jobrotation', $recordJR["code"]);
                    $stmtUpdate->bindValue('kul', ($data["oee_carpan_kul_ur"] > 0 ? round($data["oee_carpan_kul_ur"], 4) : 0));
                    $stmtUpdate->bindValue('per', ($data["oee_carpan_per_ur"] > 0 ? round($data["oee_carpan_per_ur"], 4) : 0));
                    $stmtUpdate->bindValue('kal', ($data["oee_carpan_kal_ur"] > 0 ? round($data["oee_carpan_kal_ur"], 4) : 0));
                    $stmtUpdate->bindValue('oee', ($data["oee_ur"] > 0 ? round($data["oee_ur"], 4) : 0));
                    $stmtUpdate->execute();
                }
                //}
                if ($fBelowZero !== true) {
                    if (intval($data["oee_ur"]) < 0) {
                        $fBelowZero = true;
                    }
                }
            }
            if ($fBelowZero) {//|| $_cl=='03.06.0006'
                $sql = "insert into _log_exception (code,file,line,message,method,path,sessionid,time) 
                values (200,'ReportOeeController',0,:message,'CalculateOee',:path,'',CURRENT_TIMESTAMP(0))";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('message', json_encode($data));
                $stmt->bindValue('path', $_cl);
                $stmt->execute();
            }
        }

        $sql = "select client,day,jobrotation,round(floor(kul*100)/100,2) kul,round(floor(per*100)/100,2) per,round(floor(kal*100)/100,2) kal,round(floor(oee*100)/100,2) oee 
        from client_oee_details 
        where day=:day and jobrotation=:jobrotation 
        order by client";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('day', $recordJR["day"]);
        $stmt->bindValue('jobrotation', $recordJR["code"]);
        $stmt->execute();
        $records = $stmt->fetchAll();

        return new JsonResponse(array("totalProperty" => count($records),"records" => $records));
    }

    /**
     * @Route(path="/m2m/CalculateOeeClients", name="ReportOee-calculateclients", options={"expose"=true}, methods={"POST"})
     */
    public function getCalculateOeeClientsAction(Request $request, $_locale)
    {
        $request->attributes->set('_isDCSService', true);
        $_data = $request->request->all();
        if (count($_data) === 0) {
            $_data = json_decode($request->getContent());
        } else {
            $_data = json_decode(json_encode($_data, JSON_FORCE_OBJECT));
        }
        if ($_data->day == 'undefined' || $_data->day == null || $_data->day == '') {
            return $this->msgError('Gün boş olamaz.');
        }
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $records = [];
        $sqlJR = "select c.code client,jrs.day,jrs.code jobrotation,jrs.jr_start \"start\",jrs.jr_end \"finish\"
        from (
            SELECT id,code,beginval,endval
                ,'" . $_data->day . "' \"day\" 
                ,concat(cast(case when beginval>endval then cast('" . $_data->day . "' as date) - INTERVAL '1 day' else '" . $_data->day . "' end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                ,concat(cast(case when beginval>endval then cast('" . $_data->day . "' as date) else '" . $_data->day . "' end as varchar(10)),' ',endval,':59')::timestamp jr_end
            from job_rotations jr
            where (finish is null)
        )jrs
        join clients c on c.code<>'' and c.workflow<>'Gölge Cihaz' and c.uuid is null
        order by jrs.jr_start,c.code";
        $stmtJR = $conn->prepare($sqlJR);
        $stmtJR->execute();
        $recordsJR = $stmtJR->fetchAll();
        $logtime = date('Y-m-d H:i:s');
        //$rspdata = array();
        foreach ($recordsJR as $rowJR => $dataJR) {
            $records = $this->oeeCalculate(0, $dataJR["day"], $dataJR["day"], $dataJR["client"], $dataJR["jobrotation"], null, $this->fieldname_opname, $this->prod_concat_type, $this->uri);
            if (count($records) > 0) {
                foreach ($records as $row => $data) {
                    $sqlOEE = "select * from client_oee_details where client=:client and day=:day and jobrotation=:jobrotation";
                    $stmtOEE = $conn->prepare($sqlOEE);
                    $stmtOEE->bindValue('client', $dataJR["client"]);
                    $stmtOEE->bindValue('day', $dataJR["day"]);
                    $stmtOEE->bindValue('jobrotation', $dataJR["jobrotation"]);
                    $stmtOEE->execute();
                    //if ($dataJR["client"] === 'RK004') {
                    //    $rspdata[] = $data;
                    //}
                    $recordsOEE = $stmtOEE->fetchAll();
                    if (count($recordsOEE) === 0) {
                        $conn->insert('client_oee_details', array(
                            'client' => $dataJR["client"], 'day' => $dataJR["day"], 'jobrotation' => $dataJR["jobrotation"]
                            , 'kul' => ($data["oee_carpan_kul_ur"] > 0 ? round($data["oee_carpan_kul_ur"], 4) : 0)
                            , 'per' => ($data["oee_carpan_per_ur"] > 0 ? round($data["oee_carpan_per_ur"], 4) : 0)
                            , 'kal' => ($data["oee_carpan_kal_ur"] > 0 ? round($data["oee_carpan_kal_ur"], 4) : 0)
                            , 'oee' => ($data["oee_ur"] > 0 ? round($data["oee_ur"], 4) : 0)
                            , 'start' => $dataJR["start"], 'finish' => $dataJR["finish"]
                            , 'c_k_s' => json_encode($data["C_K_S"], JSON_UNESCAPED_UNICODE)
                            , 'c_s' => json_encode($data["C_S"], JSON_UNESCAPED_UNICODE)
                            , 'c_n_s' => json_encode($data["C_N_S"], JSON_UNESCAPED_UNICODE)
                            , 'i_s' => json_encode($data["I_S"], JSON_UNESCAPED_UNICODE)
                            , 'p_c_s' => json_encode($data["P_C_S"], JSON_UNESCAPED_UNICODE)
                            , 'e_c_s' => json_encode($data["E_C_S"], JSON_UNESCAPED_UNICODE)
                            , 'kayipsaat' => json_encode(array("uretim" => array("kul" => $data["kayiptoplam_ur_kul"],"per" => $data["kayiptoplam_ur_per"],"kal" => $data["kayiptoplam_ur_kal"]),"fabrika" => array("kul" => $data["kayiptoplam_fab_kul"],"per" => $data["kayiptoplam_fab_per"],"kal" => $data["kayiptoplam_fab_kal"]),"genel" => array("kul" => $data["kayiptoplam_gen_kul"],"per" => $data["kayiptoplam_gen_per"],"kal" => $data["kayiptoplam_gen_kal"])), JSON_UNESCAPED_UNICODE)
                            , 'kayipyuzde' => 0//json_encode(array("uretim"=>array("kul"=>0,"per"=>0,"kal"=>0),"fabrika"=>array("kul"=>,"per"=>,"kal"=>),"genel"=>array("kul"=>,"per"=>,"kal"=>)))
                            , 'oeeyuzde' => json_encode(array("uretim" => array("kul" => $data["oee_carpan_kul_ur"],"per" => $data["oee_carpan_per_ur"],"kal" => $data["oee_carpan_kal_ur"]),"fabrika" => array("kul" => $data["oee_carpan_kul_fab"],"per" => $data["oee_carpan_per_fab"],"kal" => $data["oee_carpan_kal_fab"]),"genel" => array("kul" => $data["oee_carpan_kul_gen"],"per" => $data["oee_carpan_per_gen"],"kal" => $data["oee_carpan_kal_gen"])), JSON_UNESCAPED_UNICODE)
                            , 'oeedeger' => json_encode(array("uretim" => $data["oee_ur"],"fabrika" => $data["oee_fab"],"genel" => $data["oee_gen"]), JSON_UNESCAPED_UNICODE)
                            , 'created_at' => $logtime, 'updated_at' => $logtime
                        ));
                    } else {
                        $conn->update('client_oee_details', array(
                            'kul' => ($data["oee_carpan_kul_ur"] > 0 ? round($data["oee_carpan_kul_ur"], 4) : 0)
                            , 'per' => ($data["oee_carpan_per_ur"] > 0 ? round($data["oee_carpan_per_ur"], 4) : 0)
                            , 'kal' => ($data["oee_carpan_kal_ur"] > 0 ? round($data["oee_carpan_kal_ur"], 4) : 0)
                            , 'oee' => ($data["oee_ur"] > 0 ? round($data["oee_ur"], 4) : 0)
                            , 'start' => $dataJR["start"], 'finish' => $dataJR["finish"]
                            , 'c_k_s' => json_encode($data["C_K_S"], JSON_UNESCAPED_UNICODE)
                            , 'c_s' => json_encode($data["C_S"], JSON_UNESCAPED_UNICODE)
                            , 'c_n_s' => json_encode($data["C_N_S"], JSON_UNESCAPED_UNICODE)
                            , 'i_s' => json_encode($data["I_S"], JSON_UNESCAPED_UNICODE)
                            , 'p_c_s' => json_encode($data["P_C_S"], JSON_UNESCAPED_UNICODE)
                            , 'e_c_s' => json_encode($data["E_C_S"], JSON_UNESCAPED_UNICODE)
                            , 'kayipsaat' => json_encode(array("uretim" => array("kul" => $data["kayiptoplam_ur_kul"],"per" => $data["kayiptoplam_ur_per"],"kal" => $data["kayiptoplam_ur_kal"]),"fabrika" => array("kul" => $data["kayiptoplam_fab_kul"],"per" => $data["kayiptoplam_fab_per"],"kal" => $data["kayiptoplam_fab_kal"]),"genel" => array("kul" => $data["kayiptoplam_gen_kul"],"per" => $data["kayiptoplam_gen_per"],"kal" => $data["kayiptoplam_gen_kal"])), JSON_UNESCAPED_UNICODE)
                            , 'kayipyuzde' => 0//json_encode(array("uretim"=>array("kul"=>0,"per"=>0,"kal"=>0),"fabrika"=>array("kul"=>,"per"=>,"kal"=>),"genel"=>array("kul"=>,"per"=>,"kal"=>)))
                            , 'oeeyuzde' => json_encode(array("uretim" => array("kul" => $data["oee_carpan_kul_ur"],"per" => $data["oee_carpan_per_ur"],"kal" => $data["oee_carpan_kal_ur"]),"fabrika" => array("kul" => $data["oee_carpan_kul_fab"],"per" => $data["oee_carpan_per_fab"],"kal" => $data["oee_carpan_kal_fab"]),"genel" => array("kul" => $data["oee_carpan_kul_gen"],"per" => $data["oee_carpan_per_gen"],"kal" => $data["oee_carpan_kal_gen"])), JSON_UNESCAPED_UNICODE)
                            , 'oeedeger' => json_encode(array("uretim" => $data["oee_ur"],"fabrika" => $data["oee_fab"],"genel" => $data["oee_gen"]), JSON_UNESCAPED_UNICODE)
                            , 'created_at' => $recordsOEE[0]["created_at"], 'updated_at' => $logtime
                            //, 'data' => $data
                            ), array('id' => $recordsOEE[0]["id"]));
                    }
                }
            }
        }
        //return new JsonResponse(array("totalProperty" => count($records),"rspdata" => $rspdata));
        //return new JsonResponse(array("totalProperty" => count($records),"records" => $records,"recordsJR" => $recordsJR));
        return $this->msgSuccess();
    }
}
