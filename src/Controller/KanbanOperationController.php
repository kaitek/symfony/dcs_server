<?php

namespace App\Controller;

use App\Entity\KanbanOperation;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class KanbanOperationController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:KanbanOperation';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/KanbanOperation/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="KanbanOperation-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new KanbanOperation();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb = $qb->select('ko')
            ->from(self::ENTITY, 'ko')
            ->orderBy('ko.client', 'ASC')
            ->addOrderBy('ko.product', 'ASC');
        $queries['KanbanOperation'] = array('qb' => $qb, 'getAll' => true);

        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('p.id, p.name, p.description')
        //    ->from('App:ProductTree', 'p')
        //    ->where('p.materialtype = :materialtype')
        //    ->setParameter('materialtype', 'M')
        //    ->orderBy('p.name');
        //$queries['ProductTree'] = array('qb' => $qb, 'getAll' => true);

        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('c.id, c.code')
        //    ->from('App:Client', 'c')
        //    ->orderBy('c.code');
        //$queries['Client'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
    * @Route(path="/KanbanOperation/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="KanbanOperation-add", options={"expose"=true}, methods={"POST"})

    */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $this->_requestData = json_decode($request->getContent());
        if($this->_requestData == null) {
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);
        if($_SERVER['HTTP_HOST']=='192.168.1.40'||$_SERVER['HTTP_HOST']=='172.16.1.149') {
            $this->_requestData->boxcount=0;
            $this->_requestData->minboxcount=0;
            $this->_requestData->maxboxcount=0;
            $this->_requestData->packaging=0;
            $this->_requestData->hourlydemand=0;
        }
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb = $qb->select('ko.id, ko.client, ko.product')
            ->from('App:KanbanOperation', 'ko')
            ->where('ko.client = :client')
            ->andWhere('ko.product = :product')
            ->setParameters(array('client' => $this->_requestData->client, 'product' => $this->_requestData->product));
        $KanbanOperation = $qb->getQuery()->getArrayResult();
        if(count($KanbanOperation) == 0) {
            return $this->recordAdd($request, $_locale, $pg, $lm);
        } else {
            $mtitle = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('KanbanOperation.message_title', array(), 'KanbanOperation');
            $message = ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('KanbanOperation.record_exists', array(), 'KanbanOperation');
            $content = array(
                'title' => $mtitle,
                'success' => false,
                'message' => $message
            );
            return new JsonResponse($content, 401);
        }
    }

    /**
     * @Route(path="/KanbanOperation/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="KanbanOperation-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/KanbanOperation", name="KanbanOperation-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/KanbanOperation.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/KanbanOperation/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="KanbanOperation-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'KanbanOperation', $id);

            return $this->render('Modules/KanbanOperation.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/KanbanOperation/{id}", requirements={"id": "\d+"}, name="KanbanOperation-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'KanbanOperation', $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/KanbanOperation/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="KanbanOperation-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/m2m/KanbanOperation", name="KanbanOperation-import", options={"expose"=true}, methods={"PUT"})
     */
    public function putImportAction(Request $request, $_locale)
    {
        $id=0;
        $request->attributes->set('_isDCSService', true);
        $content = json_decode($request->getContent());
        /* @var $entity KanbanOperation */
        $entity = $this->getDoctrine()
                ->getRepository('App:KanbanOperation')
                ->findOneBy(array(/*'client' => $content->client,*/ 'product' => $content->product));
        if($entity!==null) {
            $id=$entity->getId();
            if($entity->getCurrentboxcount()!==$content->currentboxcount||$entity->getPackaging()!==$content->packaging) {
                return $this->recordEdit($request, $entity, $id, null, $_locale, null, null);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $this->msgError("Kayıt bulunamadı!Ürün:".$content->product."");
        }
    }

    /**
     * @Route(path="/KanbanOperation/AllProducts", name="KanbanOperation-GetAllProducts", options={"expose"=true}, methods={"GET"})
     */
    public function allProducts(Request $request, $_locale)
    {
        $sql="SELECT client,product,boxcount,minboxcount,maxboxcount,currentboxcount,packaging,hourlydemand
        from kanban_operations 
        order by client,product ";
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        $tmp=array();
        foreach($records as $row) {
            if(!isset($tmp[$row["client"]])) {
                $tmp[$row["client"]]=array("client"=>$row["client"],"data"=>array());
            }
            $tmp[$row["client"]]["data"][]=$row;
        }
        $resp = array(
            'title' => '',
            'success' => true,
            'data' => $tmp
        );
        return new JsonResponse($resp, 200);
    }
}
