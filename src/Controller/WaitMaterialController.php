<?php
namespace App\Controller;

use App\Entity\CaseMovement;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WaitMaterialController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:CaseMovement';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/WaitMaterial/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="WaitMaterial-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    public function getNewEntity() {
        return new WaitMaterial();
    }

    public function getQBQuery() {
        
        return array();
    }
    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT cm.*
            ,pt.goodsplanner
        FROM case_movements cm 
        join product_trees pt on pt.code=cm.code and cm.number=pt.number and cm.stockcode=pt.stockcode and pt.finish is null and pt.materialtype='H'
        WHERE cm.actualfinishtime is null and cm.tasktype='WAITFORMATERIAL' and cm.status='WAITFORMATERIAL' @@where@@ 
        ORDER BY cm.number,cm.stockcode";
        $queries['WaitMaterial'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/WaitMaterial/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="WaitMaterial-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    /**
     * @Route(path="/WaitMaterial/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="WaitMaterial-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbu=$this->checkBeforeUpdate($request,$id,$entity,-1);
        if($cbu===true){
            $content = json_decode($request->getContent());
            if($content->fdate==null||$content->fdate==''||$content->ftime==null||$content->ftime==''){
                return $this->msgError("Bitiş zamanı girmediniz.");
            }
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $entity->setEstimatewaitfinishtime(new \DateTime($content->fdate.' '.$content->fdate.':00'));
                $em->persist($entity);
                $em->flush();
                $em->clear();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        }else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/WaitMaterial", name="WaitMaterial-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/WaitMaterial.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/WaitMaterial/{id}", requirements={"id": "\d+"}, name="WaitMaterial-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'WaitMaterial', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/WaitMaterial/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="WaitMaterial-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    function errinsert($pos,$msg,$code=100){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //$conn->beginTransaction();
        try{
            //$sql = "select * from _log_exception where path=:path and message=:message and time>current_timestamp - interval '1' hour";
            //$stmt = $conn->prepare($sql);
            //$stmt->bindValue('path', $pos);
            //$stmt->bindValue('message', $msg);
            //$stmt->execute();
            //$records = $stmt->fetchAll();
            //if(count($records)==0){
            $sql= "insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,current_timestamp)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('code', $code);
            $stmt->bindValue('file', '');
            $stmt->bindValue('line', 0);
            $stmt->bindValue('message', $msg);
            $stmt->bindValue('method', 'MH');
            $stmt->bindValue('path', $pos);
            $stmt->bindValue('sessionid', 0);
            $stmt->execute();
            //}
            //$conn->commit();
        }catch (Exception $e) {
            echo $e->getMessage();
        }
        return;
    }

}