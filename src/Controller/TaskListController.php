<?php

namespace App\Controller;

use App\Entity\TaskList;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

//u s e \PhpOffice\PhpSpreadsheet\Spreadsheet;
//use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use \PhpOffice\PhpSpreadsheet\Cell\Cell;
//use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;
//use \PhpOffice\PhpSpreadsheet\Cell\DataType;
//use \PhpOffice\PhpSpreadsheet\IOFactory;
//use \PhpOffice\PhpSpreadsheet\Style\Border;
//use \PhpOffice\PhpSpreadsheet\Style\Alignment;
//use \PhpOffice\PhpSpreadsheet\Style\Fill;
//use \PhpOffice\PhpSpreadsheet\Collection\CellsFactory;
//use \PhpOffice\PhpSpreadsheet\Settings;
class TaskListController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:TaskList';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/TaskList/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskList-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        if ($cbd === true) {
            if ($entity->getStart()==null) {
                return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
            } else {
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                $qb_cpd = $em->createQueryBuilder();
                $qb_cpd = $qb_cpd->select('cpd.id')
                    ->from('App:ClientProductionDetail', 'cpd')
                    ->where('cpd.finish is null and cpd.type<>\'c_s\' and cpd.erprefnumber=:erprefnumber')
                    ->setParameters(array('erprefnumber' => $entity->getErprefnumber()));
                $cpd=$qb_cpd->getQuery()->getArrayResult();
                if (count($cpd)===0) {
                    $qb_cld = $em->createQueryBuilder();
                    $qb_cld = $qb_cld->select('cld.id')
                        ->from('App:ClientLostDetail', 'cld')
                        ->where('cld.finish is null and cld.type=\'l_c_t\' and cld.erprefnumber=:erprefnumber')
                        ->setParameters(array('erprefnumber' => $entity->getErprefnumber()));
                    $cld=$qb_cld->getQuery()->getArrayResult();
                    if (count($cld)===0) {
                        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
                    } else {
                        return $this->msgError('İş emrine başlandığı için kayıt silinemiyor!');
                    }
                } else {
                    return $this->msgError('İş emrine başlandığı için kayıt silinemiyor!');
                }
            }
        } else {
            return $cbd;
        }
    }

    public function getNewEntity()
    {
        return new TaskList();
    }

    public function getQBQuery()
    {
        return array();
        //$queries = array();
        ///** @var EntityManager $em */
        //$em = $this->getDoctrine()->getManager();
        //$qb = $em->createQueryBuilder();
        //$qb = $qb->select('t')
        //    ->from(self::ENTITY, 't')
        //    ->where('t.finish is null and t.productdonecount<t.productcount')
        //    ->orderBy('t.client', 'ASC')
        //    ->addOrderBy('t.plannedstart', 'ASC')
        //    ->addOrderBy('t.opname', 'ASC');
        //$queries['TaskList'] = array('qb' => $qb, 'getAll' => true);
        ////$qb = $em->createQueryBuilder();
        ////$qb = $qb->select('p.id, p.name, p.description')
        ////    ->from('App:ProductTree', 'p')
        ////    ->where('p.materialtype = :materialtype')
        ////    ->setParameter('materialtype', 'O')
        ////    ->orderBy('p.name');
        ////$queries['ProductTree'] = array('qb' => $qb, 'getAll' => true);
        ////$qb = $em->createQueryBuilder();
        ////$qb = $qb->select('c.id, c.code')
        ////    ->from('App:Client', 'c')
        ////    ->orderBy('c.code');
        ////$queries['Client'] = array('qb' => $qb, 'getAll' => true);
        //return $queries;
    }
    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT t.* ,concat(t.opname,'-',t.opdescription) tdescription 
                FROM task_lists t 
                WHERE t.finish is null /*and t.productdonecount<t.productcount*/ and t.deleted_at is null @@where@@ 
                ORDER BY t.client,t.plannedstart,t.opname ASC";
        if($_SERVER['HTTP_HOST']==='10.0.0.101'||$_SERVER['HTTP_HOST']==='10.0.0.237') {
            $_sql = "SELECT t.* ,concat(t.opname,'-',t.opdescription) tdescription 
            FROM task_lists t 
            WHERE t.finish is null and t.productdonecount<t.productcount and t.deleted_at is null @@where@@ 
            ORDER BY t.client,t.plannedstart,t.opname ASC";
        }
        $queries['TaskList'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    ///**
    // * @ Route(path="/TaskList/getReport", name="TaskList-getReport", options={"expose"=true}, methods={"POST"})
    // */
    //public function getReportAction(Request $request, $_locale){
    //    /** @var EntityManager $em */
    //    $em = $this->getDoctrine()->getManager();
    //    $conn = $em->getConnection();
    //    $sql='SELECT client "İstasyon",erprefnumber "ERP Ref",opname "Operasyon",productcount "Miktar",plannedstart "Pl. Başlangıç",tpp "Birim Süre"
    //    from task_lists
    //    where finish is null and taskfromerp=true and plannedstart is not null and productdonecount<productcount and deleted_at is null
    //    order by client,plannedstart';
    //    $stmt = $conn->prepare($sql);
    //    $stmt->execute();
    //    $records = $stmt->fetchAll();
    //    if(count($records) == 0){
    //        return $this->msgError( "Belirtilen kriterlere uygun kayıt bulunamadı." );
    //    }
    //    $name="gorev_listesi_".date("Y-m-d_H:i:s");
    //    $objPHPExcel = new Spreadsheet();
    //    $objPHPExcel->getProperties()
    //        ->setCreator("Kaitek - Reports")
    //        ->setLastModifiedBy("Kaitek - Reports")
    //        ->setTitle("Kaitek - Reports")
    //        ->setSubject("Kaitek - Reports")
    //        ->setDescription($name)
    //        ->setKeywords('Verimot')
    //        ->setCategory('-');
    //    $objPHPExcel->setActiveSheetIndex(0);
    //    $col = 1;
    //    $row = 2;
    //    $sheet = $objPHPExcel->getActiveSheet();
    //    $_strcol = Coordinate::stringFromColumnIndex($col++);
    //    $sheet->setCellValue($_strcol.$row, 'Sıra');
    //    $sheet->getColumnDimension($_strcol)->setAutoSize(true);
    //    $sheet->getStyle($_strcol.$row)->getFont()->setBold(true);
    //    $first = true;
    //    $_lc = 0;
    //    foreach($records as $item=>$kayit){
    //        if($first){
    //            foreach($kayit as $key=>$val){
    //                $_strcol = Coordinate::stringFromColumnIndex($col++);
    //                $sheet->setCellValue($_strcol.$row, strip_tags($key)/*strtoupper($this->replace_tr(strip_tags($key)))*/);
    //                $sheet->getColumnDimension($_strcol)->setAutoSize(true);
    //                $sheet->getStyle($_strcol.$row)->getFont()->setBold(true);
    //            }
    //            $row++;
    //            $first = false;
    //            $_lc = $col;
    //            /*
    //            Başlık Alanı
    //            */
    //            $sheet->mergeCells("A1:".$_strcol."1");
    //            $sheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
    //            $sheet->setCellValue("A1", $title);
    //            $sheet->getStyle("A1")->getFont()->setBold(true);
    //        }
    //        $col = 2;
    //        $sheet->setCellValue("A".$row, $row-2);
    //        foreach($kayit as $title=>$val){
    //            $_strcol = Coordinate::stringFromColumnIndex($col++);
    //            $pos = strpos($val,"|");
    //            if($pos == 1){
    //                $val = substr($val, 2);
    //                $sheet->setCellValueExplicit($_strcol.$row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/,DataType::TYPE_STRING);
    //            }else{
    //                $sheet->setCellValue($_strcol.$row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/);
    //            }
    //        }
    //        $row++;
    //    }
    //    $col = 1;
    //    foreach($kayit as $title=>$val){
    //        $_strcol = Coordinate::stringFromColumnIndex($col++);
    //        $sheet->getColumnDimension($_strcol)->setAutoSize(true);
    //    }
    //    //$sheet->setTitle('Sayfa-1');
    //    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    //    $objPHPExcel->setActiveSheetIndex(0);
    //    // create the writer
    //    $objWriter = new Xlsx($objPHPExcel);
    //    //$objWriter->setOffice2003Compatibility(true);
    //    //$objWriter->setIncludeCharts(TRUE);
    //    $response = new StreamedResponse(
    //        function () use ($objWriter) {
    //            $objWriter->save('php://output');
    //        },
    //        200,
    //        array()
    //    );
    //    // adding headers
    //    $dispositionHeader = $response->headers->makeDisposition(
    //        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
    //        $name.'.xlsx'
    //    );
    //    $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
    //    $response->headers->set('Pragma', 'public');
    //    $response->headers->set('Cache-Control', 'maxage=1');
    //    $response->headers->set('Content-Disposition', $dispositionHeader);
    //    return $response;
    //
    //}

    /**
     * @Route(path="/TaskList/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskList-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $this->_requestData = json_decode($request->getContent());
        if ($this->_requestData == null) {
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb_task = $em->createQueryBuilder();
        $qb_task = $qb_task->select('t.id')
            ->from('App:TaskList', 't')
            ->where('t.opname=:opname and t.erprefnumber=:erprefnumber')
            ->setParameters(array('opname' => $this->_requestData->opname, 'erprefnumber' => $this->_requestData->erprefnumber));
        $tl=$qb_task->getQuery()->getArrayResult();
        if (count($tl)===0) {
            $qb = $em->createQueryBuilder();
            if ($_SERVER['HTTP_HOST']=='10.10.0.10'&&($_SERVER['BASE']=='/kaynak'||$_SERVER['BASE']=='/pres'||$_SERVER['BASE']=='/punta')) {
                $qb = $qb->select('p.id, p.code, p.name, p.description, p.tpp, p.number')
                ->from('App:ProductTree', 'p')
                ->where('p.name = :name')
                ->andWhere('p.materialtype = :materialtype')
                ->andWhere('p.client = :client')
                ->andWhere('p.finish is null')
                ->setParameters(array('name' => $this->_requestData->opname, 'materialtype' => 'O', 'client'=>$this->_requestData->client));
            } else {
                $qb = $qb->select('p.id, p.code, p.name, p.description, p.tpp, p.number')
                ->from('App:ProductTree', 'p')
                ->where('p.name = :name')
                ->andWhere('p.materialtype = :materialtype')
                ->andWhere('p.finish is null')
                ->setParameters(array('name' => $this->_requestData->opname, 'materialtype' => 'O'));
            }
            $productTree = $qb->getQuery()->getArrayResult();
            //if ($_SERVER['HTTP_HOST']=='10.10.0.10'&&($_SERVER['BASE']=='/kaynak'||$_SERVER['BASE']=='/pres'||$_SERVER['BASE']=='/punta')&&count($productTree) == 0) {
            //    return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('TaskList.errRecordAddOperation', array(), 'TaskList'));
            //}
            if (count($productTree) >= 1) {
                $productTree = $productTree[0];
                $this->_requestData->opcode = $productTree['code'];
                $this->_requestData->opname = $productTree['name'];
                $this->_requestData->opnumber = $productTree['number'];
                $this->_requestData->opdescription = $productTree['description'];
                $this->_requestData->tpp = $productTree['tpp'];
            }
            $this->_requestData->code = $this->_requestData->client . $this->_requestData->opcode . $this->_requestData->opnumber . $this->_requestData->erprefnumber . time();
            $this->_requestData->type = 'quantity';

            return $this->recordAdd($request, $_locale, $pg, $lm);
        } else {
            return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('TaskList.errRecordAdd', array(), 'TaskList'));
        }
    }

    /**
     * @Route(path="/TaskList/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskList-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $this->_requestData = json_decode($request->getContent());
        if ($this->_requestData == null) {
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb_task = $em->createQueryBuilder();
        $qb_task = $qb_task->select('t.id')
            ->from('App:TaskList', 't')
            ->where('t.opname=:opname and t.erprefnumber=:erprefnumber')
            ->setParameters(array('opname' => $this->_requestData->opname, 'erprefnumber' => $this->_requestData->erprefnumber));
        $tl=$qb_task->getQuery()->getArrayResult();
        if (count($tl)!==0) {
            $qb = $em->createQueryBuilder();
            if ($_SERVER['HTTP_HOST']=='10.10.0.10'&&($_SERVER['BASE']=='/kaynak'||$_SERVER['BASE']=='/pres'||$_SERVER['BASE']=='/punta')) {
                $qb = $qb->select('p.id, p.code, p.name, p.description, p.tpp, p.number')
                ->from('App:ProductTree', 'p')
                ->where('p.name = :name')
                ->andWhere('p.materialtype = :materialtype')
                ->andWhere('p.client = :client')
                ->andWhere('p.finish is null')
                ->setParameters(array('name' => $this->_requestData->opname, 'materialtype' => 'O', 'client'=>$this->_requestData->client));
                $productTree = $qb->getQuery()->getArrayResult();
                //if ($_SERVER['HTTP_HOST']=='10.10.0.10'&&($_SERVER['BASE']=='/kaynak'||$_SERVER['BASE']=='/pres'||$_SERVER['BASE']=='/punta')&&count($productTree) == 0) {
                //    return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('TaskList.errRecordAddOperation', array(), 'TaskList'));
                //}
            }
            $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
            return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
        } else {
            return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('TaskList.errRecordEdit', array(), 'TaskList'));
        }
    }

    /**
     * @Route(path="/TaskList", name="TaskList-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/TaskList.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskList/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="TaskList-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'TaskList', $id);

            return $this->render('Modules/TaskList.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskList/{id}", requirements={"id": "\d+"}, name="TaskList-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'TaskList', $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskList/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskList-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/m2m/TaskList", name="TaskList-update-all-records", options={"expose"=true}, methods={"PUT"})
     */
    public function putImportAction(Request $request, $_locale)
    {
        $userId=1;
        $request->attributes->set('_isDCSService', true);
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $qb = $em->createQueryBuilder()->select('t')
            ->from(self::ENTITY, 't')
            ->where("t.finish is null and t.deadline<'2019-03-04'");
        $records=$qb->getQuery()->getArrayResult();
        $conn->beginTransaction();
        try {
            $i=0;
            foreach ($records as $task) {
                $entity = $this->getDoctrine()->getRepository('App:TaskList')->findOneBy(array("id"=>$task["id"]));
                $entity->setUpdateuserId($userId);
                $entity->setFinish(new \DateTime());
                $em->persist($entity);
                $em->flush();
                if (($i++ % 20) === 0) {
                    $em->clear();
                }
            }
            $em->clear();
            $conn->commit();
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            $conn->rollback();
            //throw $e;
            return $this->msgError($e->getMessage());
        }

        return $this->msgSuccess("count:".count($records));
    }

    /**
     * @Route(path="/m2m/TaskListDescription", name="TaskList-description", options={"expose"=true}, methods={"POST"})
     */
    public function getTasklistDescriptionAction(Request $request, $_locale)
    {
        //markalama tezgahları için hp009 için m003,hp012 için m004 olacak şekilde dönüş yapılacak
        $request->attributes->set('_isDCSService', true);
        $client=$request->get('client');
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="select tl.opdescription,tl.erprefnumber,tl.code
        from (
            SELECT type,client,opcode,opnumber,opname,erprefnumber,tasklist,mould,mouldgroup 
            from client_production_details 
            where finish is null and type in ('c_p','c_p_confirm') and day>=current_date - interval '10' day and client=:client
            group by type,client,opcode,opnumber,opname,erprefnumber,tasklist,mould,mouldgroup
        )b
        join task_lists tl on tl.code=b.tasklist
        where tl.finish is null and tl.taskfromerp=true and tl.opdescription like '\\\\%' and tl.opdescription like '%.ezd'";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('client', $client);
        $stmt->execute();
        $records=$stmt->fetchAll();
        foreach($records as $record) {
            if($client==='HP009'){
                $pos3=strpos($record["opdescription"],"M003");
                $pos4=strpos($record["opdescription"],"M004");
                if($pos3===false&&$pos4!==false){
                    $record["opdescription"]=implode("M003", explode("M004", $record["opdescription"]));
                }
            }
            if($client==='HP012'){
                $pos3=strpos($record["opdescription"],"M003");
                $pos4=strpos($record["opdescription"],"M004");
                if($pos3!==false&&$pos4===false){
                    $record["opdescription"]=implode("M004", explode("M003", $record["opdescription"]));
                }
            }
        }
        return new JsonResponse(array("totalProperty"=>count($records),"records"=>$records));
    }
}
