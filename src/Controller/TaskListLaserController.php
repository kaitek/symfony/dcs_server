<?php

namespace App\Controller;

use App\Entity\TaskListLaser;
use App\Entity\TaskListLaserDetail;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TaskListLaserController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:TaskListLaser';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/TaskListLaser/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskListLaser-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entityM = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        if($entityM->getStart()!==null){
            $em = $this->getDoctrine()->getManager();
            $qb_TaskListLaserd = $em->createQueryBuilder();
            $qb_TaskListLaserd = $qb_TaskListLaserd->select('md.id')
                ->from('App:TaskListLaserDetail', 'md')
                ->where('md.tasklistlaser=:tasklistlaser and md.productdonecount>0')
                ->setParameters(array('tasklistlaser' => $entityM->getCode()));
            $tld=$qb_TaskListLaserd->getQuery()->getArrayResult();
            if (count($tld)!==0) {
                return $this->msgError(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskListLaser.errTaskStartedDel', array(), 'TaskListLaser'));
            }
        }
        $cbd = $this->checkBeforeDelete($request, $id, $entityM, $v);
        if ($cbd === true) {
            $user = $this->getUser();
            $userId = $user->getId();
            $entityM->setDeleteuserId($userId);
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $em->persist($entityM);
                $em->flush();
                $em->remove($entityM);
                $em->flush();
                //detay kayıtları tespit ediliyor
                $qb_TaskListLaserd = $em->createQueryBuilder();
                $qb_TaskListLaserd = $qb_TaskListLaserd->select('md.id')
                    ->from('App:TaskListLaserDetail', 'md')
                    ->where('md.tasklistlaser=:tasklistlaser')
                    ->setParameters(array('tasklistlaser' => $entityM->getCode()));
                $tld=$qb_TaskListLaserd->getQuery()->getArrayResult();
                if(count($tld)!==0){
                    foreach($tld as $rowd){
                        $entity_md_del = $this->getDoctrine()
                            ->getRepository('App:TaskListLaserDetail')
                            ->find($rowd['id']);
                        $entity_md_del->setDeleteuserId($userId);
                        $em->persist($entity_md_del);
                        $em->flush();
                        $em->remove($entity_md_del);
                        $em->flush();
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();

                // Foreign key violation mesajı mı?
                $msg = $e->getMessage();

                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbd;
        }
    }

    public function getNewEntity()
    {
        return new TaskListLaser();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('tll.id,tll.code,tll.description,tll.production,tll.plannedstart'
                .',tll.client1,tll.client2,tll.client3,tll.client4,tll.client5'
                .',tll.start,tll.finish,tll.version')
                ->from('App:TaskListLaser', 'tll')
                ->where('tll.deleteuserId is null and tll.finish is null')
                ->orderBy('tll.plannedstart', 'ASC');
        $queries['TaskListLaser'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/TaskListLaser/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskListLaser-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba=$this->checkBeforeAdd($request);
        if($cba===true){
            $entityM = new TaskListLaser();
            $this->setEntityWithRequest($entityM, $request);
            $user = $this->getUser();
            $userId = $user->getId();
            $entityM->setCode($this->gen_uuid());
            $validator = ($this->_container==null?$this->container:$this->_container)->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entityM));
            if ($errors!==false)
                return $errors;
            $details=$this->_requestData->details;
            foreach($details as $detail){
                $arr=explode('-',$detail->opname);
                $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                $entityMD = new TaskListLaserDetail();
                $entityMD->setTasklistlaser($entityM->getCode());
                $entityMD->setOpcode($opcode);
                $entityMD->setOpnumber($opnumber);
                $entityMD->setOpname($detail->opname);
                $entityMD->setErprefnumber($detail->erprefnumber);
                $entityMD->setLeafmask($detail->leafmask);
                $entityMD->setProductcount($detail->leafmask*$entityM->getProduction());
                $entityMD->setCreateuserId($userId);
                $errors = $this->getValidateMessage($validator->validate($entityMD));
                if ($errors!==false)
                    return $errors;
            }
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            //$qb_TaskListLaser = $em->createQueryBuilder();
            //$qb_TaskListLaser = $qb_TaskListLaser->select('tll.id')
            //    ->from('App:TaskListLaser', 'm')
            //    ->where('tll.code=:code or tll.serialnumber=:serialnumber')
            //    ->setParameters(array('code' => $this->_requestData->code, 'serialnumber' => $this->_requestData->serialnumber));
            //$tl=$qb_TaskListLaser->getQuery()->getArrayResult();
            //if(count($tl)===0){
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $em->persist($entityM);
                $em->flush();
                foreach($details as $detail){
                    $arr=explode('-',$detail->opname);
                    $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                    $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                    $entityMD = new TaskListLaserDetail();
                    $qb_TaskListLaserd = $em->createQueryBuilder();
                    $qb_TaskListLaserd = $qb_TaskListLaserd->select('md.id')
                        ->from('App:TaskListLaserDetail', 'md')
                        ->where('md.tasklistlaser=:tasklistlaser and md.opname=:opname')
                        ->setParameters(array('tasklistlaser' => $entityM->getCode(), 'opname'=>$detail->opname));
                    $tld=$qb_TaskListLaserd->getQuery()->getArrayResult();
                    if(count($tld)!==0){
                        throw new \Error(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskListLaser.errRecordDetail', array(), 'TaskListLaser'));
                    }
                    $entityMD->setTasklistlaser($entityM->getCode());
                    $entityMD->setOpcode($opcode);
                    $entityMD->setOpnumber($opnumber);
                    $entityMD->setOpname($detail->opname);
                    $entityMD->setErprefnumber($detail->erprefnumber);
                    $entityMD->setLeafmask($detail->leafmask);
                    $entityMD->setProductcount($detail->leafmask*$entityM->getProduction());
                    $entityMD->setCreateuserId($userId);
                    $em->persist($entityMD);
                    $em->flush();
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            } catch (\Error $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
            //}else{
            //    return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskListLaser.errRecordAdd', array(), 'TaskListLaser') );
            //}
        } else {
            return $cba;
        }
    }

    /**
     * @Route(path="/TaskListLaser/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskListLaser-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entityM = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        //if($entityM->getStart()!==null){
        //    return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskListLaser.errTaskStartedPut', array(), 'TaskListLaser') );
        //}
        $cbu=$this->checkBeforeUpdate($request,$id,$entityM,$v);
        if($cbu===true){
            $this->setEntityWithRequest($entityM, $request);
            $user = $this->getUser();
            $userId = $user->getId();
            $validator = ($this->_container==null?$this->container:$this->_container)->get('validator');
            $errors = $this->getValidateMessage($validator->validate($entityM));
            if ($errors!==false)
                return $errors;
            $details=$this->_requestData->details;
            foreach($details as $detail){
                $arr=explode('-',$detail->opname);
                $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                $entityMD = new TaskListLaserDetail();
                $entityMD->setTasklistlaser($detail->tasklistlaser!==''?$detail->tasklistlaser:$entityM->getCode());
                $entityMD->setOpcode($opcode);
                $entityMD->setOpnumber($opnumber);
                $entityMD->setOpname($detail->opname);
                $entityMD->setErprefnumber($detail->erprefnumber);
                $entityMD->setLeafmask($detail->leafmask);
                $entityMD->setProductcount($detail->leafmask*$entityM->getProduction());
                $entityMD->setCreateuserId($userId);
                $errors = $this->getValidateMessage($validator->validate($entityMD));
                if ($errors!==false)
                    return $errors;
            }
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            //$qb_TaskListLaser = $em->createQueryBuilder();
            //$qb_TaskListLaser = $qb_TaskListLaser->select('tll.id')
            //    ->from('App:TaskListLaser', 'm')
            //    ->where('(tll.code=:code or tll.serialnumber=:serialnumber) and tll.id<>:id')
            //    ->setParameters(array('code' => $this->_requestData->code, 'serialnumber' => $this->_requestData->serialnumber,'id'=>$id));
            //$tl=$qb_TaskListLaser->getQuery()->getArrayResult();
            //if(count($tl)===0){
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $em->persist($entityM);
                $em->flush();
                $arr_d=[];
                foreach($details as $detail){
                    $arr_d[]=$detail->opname;
                    $arr=explode('-',$detail->opname);
                    $opcode=count($arr)>1?implode('-',array_slice($arr,0,count($arr)-1)):$detail->opname;
                    $opnumber=count($arr)>1?$arr[count($arr)-1]:0;
                    $qb_TaskListLaserd = $em->createQueryBuilder();
                    if($detail->id!==''&&$detail->id!==null){
                        $entityMD = $this->getDoctrine()
                            ->getRepository('App:TaskListLaserDetail')
                            ->find($detail->id);
                        $qb_TaskListLaserd = $qb_TaskListLaserd->select('md.id')
                            ->from('App:TaskListLaserDetail', 'md')
                            ->where('md.tasklistlaser=:tasklistlaser and md.opname=:opname and md.id<>:id')
                            ->setParameters(array('tasklistlaser' => $entityM->getCode(), 'opname'=>$detail->opname,'id'=>$detail->id));
                    }else{
                        $entityMD = new TaskListLaserDetail();
                        $qb_TaskListLaserd = $qb_TaskListLaserd->select('md.id')
                            ->from('App:TaskListLaserDetail', 'md')
                            ->where('md.tasklistlaser=:tasklistlaser and md.opname=:opname')
                            ->setParameters(array('tasklistlaser' => $entityM->getCode(), 'opname'=>$detail->opname));
                    }
                    $tld=$qb_TaskListLaserd->getQuery()->getArrayResult();
                    if(count($tld)!==0){
                        throw new \Error(($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskListLaser.errRecordDetail', array(), 'TaskListLaser'));
                    }
                    $entityMD->setTasklistlaser($detail->tasklistlaser!==''?$detail->tasklistlaser:$entityM->getCode());
                    $entityMD->setOpcode($opcode);
                    $entityMD->setOpnumber($opnumber);
                    $entityMD->setOpname($detail->opname);
                    $entityMD->setErprefnumber($detail->erprefnumber);
                    $entityMD->setLeafmask($detail->leafmask);
                    $entityMD->setProductcount($detail->leafmask*$entityM->getProduction());
                    $em->persist($entityMD);
                    $em->flush();
                }
                //silinen detay kayıtları tespit ediliyor
                $qb_TaskListLaserd = $em->createQueryBuilder();
                $qb_TaskListLaserd = $qb_TaskListLaserd->select('md.id')
                    ->from('App:TaskListLaserDetail', 'md')
                    ->where(count($details)>0?'md.tasklistlaser=:tasklistlaser and md.opname not in (:opname)':'md.tasklistlaser=:tasklistlaser')
                    ->setParameters(count($details)>0?array('tasklistlaser' => $entityM->getCode(), 'opname'=>$arr_d):array('tasklistlaser' => $entityM->getCode()));
                $tld=$qb_TaskListLaserd->getQuery()->getArrayResult();
                if(count($tld)!==0){
                    foreach($tld as $rowd){
                        $entity_md_del = $this->getDoctrine()
                            ->getRepository('App:TaskListLaserDetail')
                            ->find($rowd['id']);
                        $entity_md_del->setDeleteuserId($userId);
                        $em->persist($entity_md_del);
                        $em->flush();
                        $em->remove($entity_md_del);
                        $em->flush();
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            } catch (\Error $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
            //}else{
            //    return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('TaskListLaser.errRecordAdd', array(), 'TaskListLaser') );
            //}
        }else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/TaskListLaser", name="TaskListLaser-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/TaskListLaser.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskListLaser/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="TaskListLaser-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'TaskListLaser', $id);

            return $this->render('Modules/TaskListLaser.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskListLaser/{id}", requirements={"id": "\d+"}, name="TaskListLaser-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'TaskListLaser', $id);
            if($records["TaskListLaser"]["totalProperty"]>0){
                $records["TaskListLaser"]["records"][0]["_details"]=[];
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                /** @var QueryBuilder $qb */
                $qb = $em->createQueryBuilder();
                $qb = $qb->select('md.id,md.tasklistlaser,md.opcode,md.opnumber'
                    .',md.opname,md.erprefnumber,md.leafmask,md.productcount'
                    .',md.version')
                    ->from('App:TaskListLaserDetail', 'md')
                    ->where('md.deleteuserId is null ')
                    ->andWhere('md.tasklistlaser=:TaskListLaser')
                    ->orderBy('md.opname', 'ASC');
                $qb=$qb->getQuery();
                $qb->setParameter('TaskListLaser', $records["TaskListLaser"]["records"][0]["code"]);
                $details=$qb->getArrayResult();
                $records["TaskListLaser"]["records"][0]["_details"]=$details;
            }
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskListLaser/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="TaskListLaser-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
