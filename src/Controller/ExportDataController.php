<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
//use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExportDataController extends ControllerBase
{
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/ExportData/getMachineStops/{day}", name="ExportData-getMachineStops", options={"expose"=true}, methods={"GET"})
     */
    public function getMachineStopsAction(Request $request, $_locale, $day)
    {
        $statusCode = 200;
        $result = array();
        if($day > '20201001') {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $sql = "SELECT cld.client as istasyon,cgd.clientgroupcode istasyongrubu,string_agg(distinct cld.opname, ',')gorev
                ,string_agg(distinct cld.employee,',')sicil,string_agg(distinct e.name,',')personel
                ,cld.losttype as durusSebep,cld.record_id
                /*,string_agg(cld.erprefnumber, ',')erprefnumber*/
                ,cld.start as baslangic,cld.finish as bitis
                ,floor(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suredk
            from client_lost_details cld 
            left join employees e on e.code=cld.employee
            left join client_group_details cgd on cgd.client=cld.client and cgd.clientgroup='locationclient'
            where cld.type='l_e_t' and COALESCE(cld.faulttype,'')='' and cld.losttype not in ('SETUP-AYAR') and cld.day=:day 
                and COALESCE(cld.losttype,'')<>'' and cld.losttype<>'ÜRETİM'
            group by cld.type,cld.client,cld.day,cld.losttype,cld.start,cld.finish,cgd.clientgroupcode
            order by cld.start";// and e.finish is null
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $day);
            $stmt->execute();
            $records = $stmt->fetchAll();
        } else {
            $records = [];
        }

        return new JsonResponse($records, $statusCode);
        //$response = new JsonResponse($records,$statusCode);
        //$response->headers->set('Content-Type', 'application/json; charset=utf-8');
        //$response->setCharset('UTF-8');
        //$response->sendHeaders();
        //return $response;
    }

    /**
     * @Route(path="/ExportData/getSetups/{day}", name="ExportData-getSetups", options={"expose"=true}, methods={"GET"})
     */
    public function getSetupsAction(Request $request, $_locale, $day)
    {
        $statusCode = 200;
        $result = array();
        if($day > '20201001') {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $sql = "SELECT cld.client as istasyon,cgd.clientgroupcode istasyongrubu,string_agg(distinct cld.opname, ',')gorev
                ,string_agg(distinct cld.employee,',')sicil,string_agg(distinct e.name,',')usta
                ,cld.start as basla,cld.finish as bitir,cld.record_id
                ,floor(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suredk
            from client_lost_details cld 
            left join employees e on e.code=cld.employee
            left join client_group_details cgd on cgd.client=cld.client and cgd.clientgroup='locationclient'
            where cld.type='l_e_t' and cld.faulttype is null and cld.losttype in ('SETUP-AYAR') and e.isexpert=true and cld.day=:day
                and COALESCE(cld.losttype,'')<>'' and cld.losttype<>'ÜRETİM'
            group by cld.type,cld.client,cld.day,cld.losttype,cld.start,cld.finish,cgd.clientgroupcode
            order by cld.start";// and e.finish is null
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $day);
            $stmt->execute();
            $records = $stmt->fetchAll();
        } else {
            $records = [];
        }
        return new JsonResponse($records, $statusCode);
    }

    /**
     * @Route(path="/ExportData/getFailures/{day}", name="ExportData-getFailures", options={"expose"=true}, methods={"GET"})
     */
    public function getFailuresAction(Request $request, $_locale, $day)
    {
        $statusCode = 200;
        $result = array();
        if($day > '20201001') {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $sql = "SELECT cld.client as istasyon,cgd.clientgroupcode istasyongrubu,ft.description as arizakodu
                ,case cld.faultgroupcode when 'ARIZA MAKİNE MEKANİK' then 'MEKANIK' when 'ARIZA MAKİNE ELEKTRİK' then 'ELEKTRIK' else 'URETIM' end arizatip
                ,cld.start as baslangic,cld.finish as bitis,cld.finish-cld.start toplamarizasure,cld.record_id
            from client_lost_details cld 
            left join fault_types ft on ft.code=cld.faulttype
            left join client_group_details cgd on cgd.client=cld.client and cgd.clientgroup='locationclient'
            where cld.type='l_c' and COALESCE(cld.faulttype,'')<>'' and cld.faultgroupcode not in ('ARIZA KALIP-APARAT') and cld.day=:day
                and COALESCE(cld.losttype,'')<>'' and cld.losttype<>'ÜRETİM'
            order by cld.start";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $day);
            $stmt->execute();
            $records = $stmt->fetchAll();
        } else {
            $records = [];
        }
        return new JsonResponse($records, $statusCode);
    }

    /**
     * @Route(path="/ExportData/getPatternFailures/{day}", name="ExportData-getPatternFailures", options={"expose"=true}, methods={"GET"})
     */
    public function getPatternFailuresAction(Request $request, $_locale, $day)
    {
        $statusCode = 200;
        $result = array();
        if($day > '20201001') {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $sql = "SELECT cld.client as istasyon,cgd.clientgroupcode istasyongrubu,ft.description as arizakodu
                ,'KALIP' arizatip,cld.record_id
                ,cld.start as baslangic,cld.finish as bitis,cld.finish-cld.start toplamarizasure
            from client_lost_details cld 
            left join fault_types ft on ft.code=cld.faulttype
            left join client_group_details cgd on cgd.client=cld.client and cgd.clientgroup='locationclient'
            where cld.type='l_c' and COALESCE(cld.faulttype,'')<>'' and cld.faultgroupcode in ('ARIZA KALIP-APARAT') and cld.day=:day
                and COALESCE(cld.losttype,'')<>'' and cld.losttype<>'ÜRETİM'
            order by cld.start";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $day);
            $stmt->execute();
            $records = $stmt->fetchAll();
        } else {
            $records = [];
        }
        return new JsonResponse($result, $statusCode);
    }

    /**
     * @Route(path="/ExportData/getReworks/{day}", name="ExportData-getReworks", options={"expose"=true}, methods={"GET"})
     */
    public function getReworksAction(Request $request, $_locale, $day)
    {
        $statusCode = 200;
        $result = array();
        if($day > '20201001') {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $sql = "SELECT cpd.opcode as operasyon,cpd.opnumber as opno
                ,r.reworktype as tip,cpd.erprefnumber 
                ,cpd.start as baslama,cpd.finish as bitis,e.name as personel
                ,cpd.productioncurrent as yapilan,cpd.finish-cpd.start as sure,cpd.record_id
                ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
            from client_production_details cpd
            join clients c on c.code=cpd.client and c.workflow='El İşçiliği'
            left join reworks r on r.code=cpd.tasklist
            left join employees e on e.code=cpd.employee
            where cpd.type='e_p' and cpd.mould is null and cpd.day=:day
            order by cpd.start";// and e.finish is null
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $day);
            $stmt->execute();
            $records = $stmt->fetchAll();
        } else {
            $records = [];
        }
        return new JsonResponse($records, $statusCode);
    }

    /**
     * @Route(path="/ExportData/getLossOfSpeed/{day}", name="ExportData-getLossOfSpeed", options={"expose"=true}, methods={"GET"})
     */
    public function getLossOfSpeedAction(Request $request, $_locale, $day)
    {
        $statusCode = 200;
        $result = array();
        if($day > '20201001') {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $uri = strtolower($request->getBaseUrl());
            $bolum = ($uri == '/kaynak2' ? 'Kaynak 2' : ($uri == '/kaynak1' ? 'Kaynak 1' : 'cgd.clientgroupcode'));
            $sql = "
            select * 
            from (
                select cpd.client,cgd.clientgroupcode,cpd.day,cpd.jobrotation
                    ,round(sum(cpd.productioncurrent)/case when max(cpd.ispres)=1 then cpd.operasyonsayisi else 1 end)productioncurrent
                    ,cpd.start,cpd.finish,round(cpd.suresn/cpd.operasyonsayisi) as donetimecurrent,string_agg(distinct cpd.opname,',')task
                    ,string_agg(distinct cpd.erprefnumber,',')erprefnumber
                    ,round(sum(cpd.proctimecurrent)/case when max(cpd.ispres)=1 then cpd.operasyonsayisi else 1 end)proctimecurrent
                    ,cpd.calculatedtpp,(cpd.suresn/cpd.operasyonsayisi)-cpd.losttimex-round(sum(cpd.proctimecurrent)/case when max(cpd.ispres)=1 then cpd.operasyonsayisi else 1 end)losttime
                    ,round(cast((((cpd.suresn/cpd.operasyonsayisi)-cpd.losttimex-round(sum(cpd.proctimecurrent)/case when max(cpd.ispres)=1 then cpd.operasyonsayisi else 1 end))/3600) as numeric),2) losttimehour
                    ,".$bolum." as section
                from (	
                    SELECT a.*,(SELECT sum(
                            cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                        )losttimex
                        from client_lost_details cld where type='l_c' and day=a.day and client=a.client and start>=a.start and finish<=a.finish and losttype not in ('YETKİNLİK GÖZLEM'))/a.operasyonsayisi losttimex
                        ,case when exists(select c.workflow from clients c where c.code=a.client and c.workflow='Değişken Üretim Aparatlı') then 1 else 0 end ispres
                    from (
                        SELECT cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent
                            ,cpd.start,cpd.finish,cpd.calculatedtpp,cpd.opname
                            ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            ,cpd.production*cpd.calculatedtpp proctime,cpd.productioncurrent*cpd.calculatedtpp proctimecurrent,cpd.erprefnumber
                            ,coalesce(cpd.opsayi,0)opsayi
                            ,coalesce(nullif((
                                select count(*)
                                from client_production_details 
                                where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type
                            ),0),1)operasyonsayisi
                        from client_production_details cpd 
                        where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>0 and cpd.day=:day
                    )a
                )cpd
                left join client_group_details cgd on cgd.client=cpd.client and cgd.clientgroup='locationclient'
                where cpd.proctimecurrent<cpd.suresn
                GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.opname,cpd.start,cpd.finish,cpd.suresn,cpd.losttimex,cpd.calculatedtpp,cgd.clientgroupcode,cpd.operasyonsayisi
                )b
            where b.losttime>0
            order by b.start
            ";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $day);
            $stmt->execute();
            $records = $stmt->fetchAll();
        } else {
            $records = [];
        }
        return new JsonResponse($records, $statusCode);
    }

    /**
     * @Route(path="/ExportData/getLosts/{day}", name="ExportData-getLosts", options={"expose"=true}, methods={"GET"})
     */
    public function getLostsAction(Request $request, $_locale, $day)
    {
        $statusCode = 200;
        $result = array();
        if($day > '20201001') {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $uri = strtolower($request->getBaseUrl());
            $bolum = ($uri == '/kaynak2' ? 'Kaynak 2' : ($uri == '/kaynak1' ? 'Kaynak 1' : 'Pres'));
            //$sql="select aa.*,((aa.sure*aa.operatorsayisi)/aa.operasyonsayisi) kayipsuredk //20211108 doğan tekvim talebi ile kaldırıldı
            $sql = "select aa.*,((aa.sure)/coalesce(nullif(aa.operasyonsayisi,0),1)) kayipsuredk
            from (
                SELECT cld.client istasyon,COALESCE(cgd.clientgroupcode,'".$bolum."') bolum,cld.day gun,cld.jobrotation vardiya,cld.losttype kayiptipi
                    ,cld.mould kalip,cld.mouldgroup kalipgrup,cld.opcode operasyonkodu,cld.opdescription operasyonaciklamasi
                    ,cld.start baslangic,cld.finish bitis
                    ,count(*) operatorsayisi
                    ,coalesce(nullif((
                        select count(*)
                        from client_lost_details 
                        where client=cld.client and day=cld.day and jobrotation=cld.jobrotation and losttype=cld.losttype 
                            /*and mould=cld.mould and mouldgroup=cld.mouldgroup and opdescription=cld.opdescription*/ 
                            and start=cld.start and finish=cld.finish and type=cld.type
                    ),0),1)/count(*) operasyonsayisi
                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 sure
                from client_lost_details cld
                left join client_group_details cgd on cgd.client=cld.client and cgd.clientgroup='locationclient'
                where cld.type='l_e_t' and cld.day=:day and COALESCE(cld.losttype,'')<>'' and cld.losttype<>'ÜRETİM'
                group by cld.client,cgd.clientgroupcode,cld.day,cld.jobrotation,cld.losttype,cld.mould,cld.mouldgroup,cld.opcode,cld.opdescription,cld.start,cld.finish,cld.type
            )aa
            order by aa.istasyon,aa.baslangic";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $day);
            $stmt->execute();
            $records = $stmt->fetchAll();
        } else {
            $records = [];
        }
        return new JsonResponse($records, $statusCode);
    }

    /**
     * @Route(path="/ExportData/getProductTrees", name="ExportData-getProductTrees", options={"expose"=true}, methods={"GET"})
     */
    public function getProductTreesAction(Request $request, $_locale)
    {
        $statusCode = 200;
        $result = array();
        //if($day>'20201001') {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = "SELECT p.kalem,max(p.tezgah) as tezgah,p.mamul,p.operasyon,p.yarımamul,
                p.operasyonacıklama,p.bilesen,p.kasatipi,
                p.sevkaracno,p.kasaicimiktar,p.materyeltipi,p.miktar
            FROM(
                SELECT DISTINCT 
                    product_trees.id AS kalem,REPLACE(client_details.client, '.', '') AS tezgah,
                    product_trees.code AS mamul,
                    product_trees.number as operasyon,
                    product_trees.stockcode as yarımamul,
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE
                        (product_trees.description,'ı','i'),'ğ','g'),'ç','c'),'ş','s'),'ü','u'),'ö','o'),
                        'I','İ'),'Ğ','G'),'Ç','C'),'Ş','S'),'Ü','U'),'Ö','O') AS operasyonacıklama,
                    product_trees.stockname as bilesen,
                    product_trees.pack AS kasatipi,
                    product_trees.carrierfeeder AS sevkaracno,
                    product_trees.packcapacity as kasaicimiktar,
                    product_trees.materialtype AS materyeltipi,
                    product_trees.quantity as miktar
                FROM product_trees
                left JOIN mould_details ON product_trees.name=mould_details.opname
                left JOIN client_details ON mould_details.mould=client_details.code
                where product_trees.materialtype IN('H','K')
                ORDER BY product_trees.code ASC
            ) AS p 
            GROUP BY p.mamul,p.operasyon,p.yarımamul,p.operasyonacıklama,p.bilesen,p.kasatipi,p.sevkaracno,p.kasaicimiktar,p.materyeltipi,p.kalem,p.miktar
            ORDER BY p.kalem ASC";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        //} else {
        //    $records=[];
        //}
        return new JsonResponse($records, $statusCode);
    }
}
