<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AndroidWatchController extends ControllerBase
{
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/AndroidWatch/RefreshData/{pc}", requirements={"pc": "\d+"}, name="AndroidWatch-RefreshData", options={"expose"=true}, methods={"POST"})
     */
    public function getRefreshDataAction(Request $request, $_locale, $pc)
    {
        //$arrNewApp=array(1,2,3,4,5,7);
        $uri = strtolower($request->getBaseUrl());
        $statusCode = 200;
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $sql = "SELECT id,code,beginval,endval ,current_date,current_time
            ,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) \"day\" 
            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start 
            ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end 
        from job_rotations 	
        where (finish is null or finish<now())	
            and (
            (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
            or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
        )";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        $jr_day = $records[0]['day'];
        $jr_code = $records[0]['code'];

        if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') || $_SERVER['HTTP_HOST'] == '192.168.1.10') {
            $sql_c = "SELECT c.* 
            from clients c
            join client_group_details cgd on cgd.client=c.code and cgd.clientgroup='androidwatch' 
            where c.workflow<>'Gölge Cihaz' and cgd.clientgroupcode='screen-$pc'";
            $stmt_c = $conn->prepare($sql_c);
            $stmt_c->execute();
            $client = $stmt_c->fetchAll();
            if ($client[0]['uuid'] !== null) {
                $status = $client[0]['status'];
                $arr_status = explode('|', $status);
                $lost = $arr_status[0];
                $arr_lost = explode(':', $lost);
                $work = $arr_status[1];
                $arr_work = explode(':', $work);
                $wfp = $arr_status[3];
            } else {
                $sql_lost = "SELECT *
                from client_lost_details 
                where client=:client and day=:day and jobrotation=:jobrotation and finish is null and type in ('l_c','l_c_t') ";
                $stmt_lost = $conn->prepare($sql_lost);
                $stmt_lost->bindValue('client', $client[0]['code']);
                $stmt_lost->bindValue('day', $jr_day);
                $stmt_lost->bindValue('jobrotation', $jr_code);
                $stmt_lost->execute();
                $records_lost = $stmt_lost->fetchAll();
                if (count($records_lost) > 0) {
                    $arr_lost = array("lost",$records_lost[0]["losttype"]);
                } else {
                    $arr_lost = array("lost","-");
                }
                $sql_work = "SELECT *
                from client_production_details 
                where client=:client and day=:day and jobrotation=:jobrotation and finish is null and type in ('c_p','c_p_confirm','c_p_unconfirm') and opdescription is not null";
                $stmt_work = $conn->prepare($sql_work);
                $stmt_work->bindValue('client', $client[0]['code']);
                $stmt_work->bindValue('day', $jr_day);
                $stmt_work->bindValue('jobrotation', $jr_code);
                $stmt_work->execute();
                $records_work = $stmt_work->fetchAll();
                if (count($records_work) > 0) {
                    $arr_work = array("work","OP");
                } else {
                    $arr_work = array("work","-");
                }
                $wfp = "wfp:-";
                if ($arr_lost[1] == 'SETUP-AYAR' || $arr_lost[1] == 'KALIP SÖKME') {
                    $wfp = 'wfp:SETUP';
                }
                if ($arr_lost[1] == 'KALİTE ONAY') {
                    $wfp = 'wfp:KALİTE';
                }
            }
            $sql_op = "
            select aa.opdescription,aa.opname,aa.mouldgroup,round(mg.cycletime,2) cycletime from (
                SELECT opdescription,opname,mouldgroup 
                from client_lost_details 
                where client=:client and finish is null and type in ('l_c','l_c_t') and opdescription is not null and day=:day and jobrotation=:jobrotation
                union all
                SELECT opdescription,opname,mouldgroup 
                from client_production_details 
                where client=:client and finish is null and type in ('c_p','c_p_confirm','c_p_unconfirm') and opdescription is not null and day=:day and jobrotation=:jobrotation
            )aa
            left join mould_groups mg on mg.code=aa.mouldgroup
            GROUP BY aa.opdescription,aa.opname,aa.mouldgroup,mg.cycletime
            order by aa.opname";
            $stmt_op = $conn->prepare($sql_op);
            $stmt_op->bindValue('client', $client[0]['code']);
            $stmt_op->bindValue('day', $jr_day);
            $stmt_op->bindValue('jobrotation', $jr_code);
            $stmt_op->execute();
            $records_op = $stmt_op->fetchAll();
            if ($wfp === 'wfp:SETUP' && ($arr_lost[1] == 'SETUP-AYAR' || $arr_lost[1] == 'KALİTE BEKLEME' || $arr_lost[1] == 'KALİTE ONAY')) {
                $sql_m = "SELECT mg.* 
                from moulds m
                join mould_groups mg on mg.mould=m.code
                where mg.code=:code";
                $stmt_m = $conn->prepare($sql_m);
                $stmt_m->bindValue('code', $records_op[0]['mouldgroup']);
                $stmt_m->execute();
                $records_m = $stmt_m->fetchAll();
                $renk = 0;
                $sure = 0;
                $surestr = '';
                if (count($records_m) > 0) {
                    $setup = $records_m[0]['setup'];
                    $sql_cld = "SELECT y.sure
                        ,concat(case when y.sure>0 then '' else '-' end
                        ,to_char((case when y.sure>0 then y.sure else y.sure*-1 end || ' second')::interval, 'HH24:MI:SS'))surestr0
                        ,case when y.sure>0 then y.sure else y.sure*-1 end surestr
                        ,case when y.sure<300 and y.sure>0 then 2 when y.sure<0 then 1 else 3 end as renk
                    from (
                        SELECT round(".$setup."-(select COALESCE(sum(x.sure),0)sure from (
                            SELECT extract(epoch from (COALESCE(finish,now())::timestamp - start::timestamp)) sure
                            from client_lost_details 
                            where type='l_c' and losttype in ('ÇAY MOLASI','YEMEK MOLASI','ARIZA KALIP-APARAT','ARIZA MAKİNE ELEKTRİK','ARIZA MAKİNE MEKANİK') 
                            and client=cld.client and start>=cld.start and COALESCE(finish,now())<=CURRENT_TIMESTAMP
                        )x)-extract(epoch from (now()::timestamp - cld.start::timestamp))) sure
                        from client_lost_details cld 
                        where cld.type='l_c_t' and cld.losttype='SETUP-AYAR' and cld.client=:client and cld.day=:day and cld.jobrotation=:jobrotation and cld.mouldgroup=:mouldgroup 
                        order by cld.start limit 1
                    )y
                    ";
                    $stmt_cld = $conn->prepare($sql_cld);
                    $stmt_cld->bindValue('client', $client[0]['code']);
                    $stmt_cld->bindValue('day', $jr_day);
                    $stmt_cld->bindValue('jobrotation', $jr_code);
                    $stmt_cld->bindValue('mouldgroup', $records_m[0]['code']);
                    $stmt_cld->execute();
                    $records_cld = $stmt_cld->fetchAll();
                    if (count($records_cld) > 0) {
                        $sure = $records_cld[0]['sure'];
                        $surestr = $records_cld[0]['surestr'];
                        $renk = $records_cld[0]['renk'];
                    }
                }
                $result['0'] = false;
                $result['1'] = false;
                $result['2'] = array(array("name" => $client[0]['code'],"description" => $records_op[0]['opdescription'],"durum" => 'AYAR YAPILIYOR',"renk" => $renk,"surestr" => $surestr,"sure" => $sure));
            } else {
                $sql_oee = "SELECT cast(kul as integer)kul,cast(per as integer)per,cast(kal as integer)kal,cast(oee as integer)oee from client_oee_details where client=:client order by id desc limit 1";
                $stmt_oee = $conn->prepare($sql_oee);
                $stmt_oee->bindValue('client', $client[0]['code']);
                $stmt_oee->execute();
                $records_oee = $stmt_oee->fetchAll();
                $_kul = 0;
                $_per = 0;
                $_kal = 0;
                $_oee = 0;
                if (count($records_oee) > 0) {
                    $_kul = $records_oee[0]['kul'];
                    $_per = $records_oee[0]['per'];
                    $_kal = $records_oee[0]['kal'];
                    $_oee = $records_oee[0]['oee'];
                }
                //if (strpos($arrNewApp, $pc) !== false) {
                $sql_tpp2 = "SELECT cast((case when sum(production)>0 then sum(suresn)/sum(production) else 0 end) as decimal(20,2))tpp2 from (
                    SELECT production ,gap suresn
                    from client_production_signals 
                    where client=:client and type in ('c_s') and day=:day and jobrotation=:jobrotation /*and opdescription like '%01.12164AC%' */
                    order by time desc limit 10)cpd";
                //} else {
                //    $sql_tpp2="SELECT cast((case when sum(production)>0 then sum(suresn)/sum(production) else 0 end) as decimal(20,2))tpp2 from (
                //        SELECT production
                //        ,case when extract(epoch from (CURRENT_TIMESTAMP(0)::timestamp - start::timestamp))>600 then 600
                //        else extract(epoch from (CURRENT_TIMESTAMP(0)::timestamp - start::timestamp)) end suresn
                //        from client_production_details
                //        where client=:client and type in ('c_s') and day=:day and jobrotation=:jobrotation /*and opdescription like '%01.12164AC%' */
                //        order by start desc limit 2)cpd";
                //}
                $stmt_tpp2 = $conn->prepare($sql_tpp2);
                $stmt_tpp2->bindValue('client', $client[0]['code']);
                $stmt_tpp2->bindValue('day', $jr_day);
                $stmt_tpp2->bindValue('jobrotation', $jr_code);
                $stmt_tpp2->execute();
                $records_tpp2 = $stmt_tpp2->fetchAll();
                $result['0'] = array(
                    array(
                        "title" => $client[0]['code']
                        ,"islem" => ($arr_lost[1] !== '-' ? ($arr_lost[1] == '' ? 'DURUYOR' : $arr_lost[1]) : ($arr_work[1] == 'OP' ? 'ÇALIŞIYOR' : ''))
                        ,"uretim" => 0
                        ,"planlanan" => 0
                        ,"refno" => count($records_op) > 0 ? $records_op[0]['opdescription'] : ''
                        ,"production" => 0
                        ,"verim" => 0
                        ,"renk" => 1
                        ,"amir" => ''
                        ,"surat" => ($_oee < 75 ? 1 : (($_oee >= 75 && $_oee < 85) ? 2 : 3))
                        ,"hedef" => (count($records_op) > 0 ? $records_op[0]['cycletime'] : '0.00')
                        ,"gercek" => (count($records_tpp2) > 0 ? $records_tpp2[0]['tpp2'] : '0.00')
                        ,"kul" => $_kul
                        ,"per" => $_per
                        ,"kal" => $_kal
                        ,"oee" => $_oee
                        ,"renk_kul" => ($_kul < 75 ? 1 : (($_kul >= 75 && $_kul < 85) ? 2 : 3))
                        ,"renk_per" => ($_per < 75 ? 1 : (($_per >= 75 && $_per < 85) ? 2 : 3))
                        ,"renk_kal" => ($_kal < 75 ? 1 : (($_kal >= 75 && $_kal < 85) ? 2 : 3))
                        ,"renk_oee" => ($_oee < 75 ? 1 : (($_oee >= 75 && $_oee < 85) ? 2 : 3))
                    )
                );
                $sql_e = "SELECT concat('<ul>',string_agg(concat('<li>',upper(name),'</li>'),'' order by name),'</ul>') personel
                from employees 
                where client=:client";
                $stmt_e = $conn->prepare($sql_e);
                $stmt_e->bindValue('client', $client[0]['code']);
                $stmt_e->execute();
                $records_e = $stmt_e->fetchAll();
                $result['1'] = $records_e;
                $result['2'] = false;
            }
        } else {
            $sql  = "SELECT c.id,c.code istasyon
                ,case split_part(split_part(c.status,'|',1),':',2) when '-' then 'ÇALIŞIYOR' when '' then 'DURUYOR' else split_part(split_part(c.status,'|',1),':',2) end surec
                ,case split_part(split_part(c.status,'|',1),':',2) when 'İŞ YOK' then '' else o.operasyon end referans
                ,(SELECT left(string_agg(concat(left(split_part(name,' ',1),1),'.',split_part(name,' ',2)),','),20) personel from employees e where e.finish is null and e.client=c.code )personel
                ,case split_part(split_part(c.status,'|',1),':',2) when '-' then '#e9e9e9' when '' then '#febeb9' when 'İŞ YOK' then '#a0A8a3' when 'GÖREVDE KİMSE YOK' then '#a0A8a3' else '#febeb9' end color
                ,COALESCE(cod.oee,0) verim
            from clients c
            join client_group_details cgd on cgd.client=c.code
            left join (select client,left(string_agg(distinct operasyon,','),20)operasyon 
                from (
                    SELECT client,COALESCE(mouldgroup,opcode) operasyon from client_lost_details where finish is null and type='l_c_t' and jobrotation=:jobrotation and day=:day
                    union all
                    select client,COALESCE(mouldgroup,opcode) operasyon from client_production_details where finish is null and type in ('c_p','c_p_confirm') and jobrotation=:jobrotation and day=:day
                )a
                group by client
            )o on o.client=c.code
            left join client_oee_details cod on cod.client=c.code and cod.day=:day and cod.jobrotation=:jobrotation
            where cgd.clientgroup='androidwatch' and c.workflow<>'Gölge Cihaz' and cgd.clientgroupcode='screen-".$pc."'
            order by c.watchorder,cgd.id";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $jr_day);
            $stmt->bindValue('jobrotation', $jr_code);
            $stmt->execute();
            $result['0'] = $stmt->fetchAll();

            $sql = "SELECT count(*)sayi from employees where finish is null and client is not null";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $calisan = $records[0]['sayi'];
            $sql = "SELECT count(*)sayi from job_rotation_employees where day=:day and jobrotation=:jobrotation";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $jr_day);
            $stmt->bindValue('jobrotation', $jr_code);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $planlanan = $records[0]['sayi'];
            $bosta = ($planlanan - $calisan > 0 ? $planlanan - $calisan : 0);
            $bospersonel = '';
            $i = 0;
            $oee = 0;
            foreach ($result['0'] as $row => $item) {
                $i++;
                $oee += $item['verim'];
            }
            $hatverim = round($oee / $i, 2);
            $sql = "SELECT round(sum(cod.oee)/count(*),2)oee
            from client_group_details cgd
            join client_oee_details cod on cod.client=cgd.client and cod.day=:day and cod.jobrotation=:jobrotation";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $jr_day);
            $stmt->bindValue('jobrotation', $jr_code);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $atolyeverim = $records[0]['oee'];
            $result['1'] = array(
                'planlanan' => $planlanan,
                'calisan' => $calisan,
                'bosta' => $bosta,
                'bospersonel' => $bospersonel ? $bospersonel : '',
                'atolyeverim' => $atolyeverim,
                'hatverim' => $hatverim
            );
        }
        return new JsonResponse($result, $statusCode);
    }
}
