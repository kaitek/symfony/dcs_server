<?php

namespace App\Controller;

use App\Entity\CaseLabel;
use App\Entity\TaskCase;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DateTime;

class CaseLabelController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:CaseLabel';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/CaseLabel/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="CaseLabel-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError(
            ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    public function getNewEntity()
    {
        return new CaseLabel();
    }

    public function getQBQuery()
    {
        return array();
    }
    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT cl.id,cl.erprefnumber,cl.time,cl.status,cl.canceldescription "
                ." ,case when ti.id is not null then concat(ti.opcode,'-',ti.opnumber) when tl.id is not null then concat(tl.opcode,'-',tl.opnumber) else '' end opname "
                . " FROM case_labels cl "
                . " left join task_imports ti on ti.erprefnumber=cl.erprefnumber "
                . " left join task_lists tl on tl.erprefnumber=cl.erprefnumber "
                . " WHERE cl.status='OPEN' and cl.canceltime is null @@where@@ "
                . " ORDER BY cl.erprefnumber DESC";
        $queries['CaseLabel'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/CaseLabel/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="CaseLabel-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError(
            ($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    /**
     * @Route(path="/CaseLabel/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="CaseLabel-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        //if(($this->_container==null?$this->container:$this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')!==true){
        //    return $this->msgError(
        //        ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
        //        401
        //    );
        //}
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, -1);
        if ($cbu===true) {
            $str= $request->getContent();
            $str2=str_replace('\t', ' ', str_replace('\r', ' ', str_replace('\n', ' ', $str)));
            //$str3=addslashes($str);
            $content = json_decode($str2);
            $arrnodeinfo=array();
            $user = $this->getUser();
            $this->errinsert("erprefiptal", "erprefnumber:".$content->erprefnumber);
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $sqlc="select * from clients where uuid is null and workflow<>'Gölge Cihaz'";
                $stmtc = $conn->prepare($sqlc);
                $stmtc->execute();
                $recordsc = $stmtc->fetchAll();
                if (count($recordsc)>0) {
                    //$sqli="insert into __sync_messages (queuename,messagetype,priority,status,time,data,info) values ('','material_prepare',1,'material_prepare',CURRENT_TIMESTAMP(0),$6,$7)";
                    $time = microtime(true);
                    $micro = sprintf("%06d", ($time - floor($time)) * 1000000);
                    $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $time));

                    $_d=$d->format("Y-m-d H:i:s.u");
                    $jdata='{"messagetype":"material_prepare","material_prepare":true,"methodName":"etiketsil"'.
                        ',"cancelusername":"'.$user->getUsername().'"'.
                        ',"canceldescription":"'.(($content->canceldescription!==''&&$content->canceldescription!==null) ? $content->canceldescription : 'Erp Ref İptal').'"'.
                        ',"erprefnumber":"'.$content->erprefnumber.'"}';
                    $conn->insert('__sync_messages', array('queuename'=>'', 'priority'=>1, 'messagetype' => 'material_prepare', 'status' => 'material_prepare', 'time' => $_d
                    , 'data' => $jdata,'info'=>"{}" ));
                } else {
                    $sql_1="update case_labels 
                    set canceldescription=:canceldescription,canceltime=CURRENT_TIMESTAMP(0),cancelusername=:cancelusername,status=:status,synced=1
                        ,updated_at=CURRENT_TIMESTAMP(0)
                    where status='OPEN' and erprefnumber=:erprefnumber";
                    $stmt_1 = $conn->prepare($sql_1);
                    $stmt_1->bindValue('canceldescription', $content->canceldescription);
                    $stmt_1->bindValue('cancelusername', $user->getUsername());
                    $stmt_1->bindValue('status', 'DELETED');
                    $stmt_1->bindValue('erprefnumber', $content->erprefnumber);
                    $stmt_1->execute();

                    //$sql_2="select * from task_cases where movementdetail is not null and status in ('WAIT','WORK') and erprefnumber=:erprefnumber";
                    //$stmt_2 = $conn->prepare($sql_2);
                    //$stmt_2->bindValue('erprefnumber', $content->erprefnumber);
                    //$stmt_2->execute();
                    //$records_2 = $stmt_2->fetchAll();
                    //foreach ($records_2 as $row) {
                    //    /* @var $repo TaskCase */
                    //    $repo = $this->getDoctrine()
                    //        ->getRepository('App:TaskCase');
                    //    $entity_1=$repo->find($row["id"]);
                    //    if($entity_1!==null){
                    //        //if (is_callable(array($entity_1, "setStatus"))) {
                    //        $entity_1->setStatus('LABELDELETED');
                    //        //$entity_1->setRecordId($row['record_id']);
                    //        $em->persist($entity_1);
                    //        $em->flush();
                    //        $em->clear();
                    //        //}
                    //    }
                    //}
                    $sql_2="update task_cases
                    set status=:status 
                    where movementdetail is not null and status in ('WAIT','WORK') and erprefnumber=:erprefnumber
                    ";
                    $stmt_2 = $conn->prepare($sql_2);
                    $stmt_2->bindValue('status', 'LABELDELETED');
                    $stmt_2->bindValue('erprefnumber', $content->erprefnumber);
                    $stmt_2->execute();

                    $sql_3="SELECT cmd.* 
                    from case_movements cm 
                    join case_movement_details cmd on cmd.casemovementid=cm.id 
                    where cm.actualfinishtime is null and cmd.donetime is null and cmd.canceltime is null and cm.erprefnumber=:erprefnumber";
                    $stmt_3 = $conn->prepare($sql_3);
                    $stmt_3->bindValue('erprefnumber', $content->erprefnumber);
                    $stmt_3->execute();
                    $records_3 = $stmt_3->fetchAll();
                    foreach ($records_3 as $row) {
                        $sql_4="update case_movement_details set canceltime=CURRENT_TIMESTAMP(0),description=:description where id=:id";
                        $stmt_4 = $conn->prepare($sql_4);
                        $stmt_4->bindValue('description', 'Erp Ref İptal');
                        $stmt_4->bindValue('id', $row["id"]);
                        $stmt_4->execute();
                    }

                    $sql_5="update production_storages set finish=CURRENT_TIMESTAMP(0) where erprefnumber=:erprefnumber";
                    $stmt_5 = $conn->prepare($sql_5);
                    $stmt_5->bindValue('erprefnumber', $content->erprefnumber);
                    $stmt_5->execute();

                    $sql_6="select *
                    from case_movements
                    where actualfinishtime is null and erprefnumber=:erprefnumber";
                    $stmt_6 = $conn->prepare($sql_6);
                    $stmt_6->bindValue('erprefnumber', $content->erprefnumber);
                    $stmt_6->execute();
                    $records_6 = $stmt_6->fetchAll();
                    foreach ($records_6 as $row) {
                        if (!(in_array($row["carrier"], $arrnodeinfo))) {
                            $arrnodeinfo[] = $row["carrier"];
                        }
                        $sql_7="update case_movements set carrier=null,empcode=null,empname=null,status=:status,actualfinishtime=CURRENT_TIMESTAMP(0) where id=:id";
                        $stmt_7 = $conn->prepare($sql_7);
                        $stmt_7->bindValue('status', 'LABELDELETED');
                        $stmt_7->bindValue('id', $row["id"]);
                        $stmt_7->execute();
                        $this->dosyasil($row["filepath"], 10);
                    }
                    foreach ($arrnodeinfo as $item) {
                        $sql_c_2="select id from carriers where code=:code";
                        $stmt_c_2 = $conn->prepare($sql_c_2);
                        $stmt_c_2->bindValue('code', $item);
                        $stmt_c_2->execute();
                        $records_c_2=$stmt_c_2->fetchAll();
                        if (count($records_c_2)>0) {
                            $data='{"material_prepare":true,"methodName":"tasimagorevilistele","id":"'.$records_c_2[0]["id"].'"}';
                            $this->dosyayaznodetrigger($data, 512);
                        }
                    }
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/CaseLabel", name="CaseLabel-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/CaseLabel.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/CaseLabel/{id}", requirements={"id": "\d+"}, name="CaseLabel-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'CaseLabel', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/CaseLabel/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="CaseLabel-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    public function dosyasil($path, $pos)
    {
        try {
            if ($path!=""&&file_exists($path)) {
                $this->errinsert("f-d-ok-".$pos, "path:".$path, 202);
                unlink($path);
            } else {
                $this->errinsert("f-d-nf-".$pos, "path:".$path, 203);
            }
        } catch (Exception $e) {
            //echo("Dosya silme hatası! ".$e->getMessage()."\n");
            $this->errinsert("f-d-err-".$pos, "path:".$path." message:".$e->getMessage(), 204);
        }
    }

    public function dosyayaznodetrigger($data, $pos)
    {
        try {
            $importpathnode=($this->_container==null ? $this->container : $this->_container)->getParameter('mh_importpathnode');
            $pc=($_SERVER['HTTP_HOST']);
            $t=round(microtime(true)*1000);
            if ($pc!="192.168.1.10"&&$pc!="192.168.1.40") {
                $exportFileName = "\\\\".$pc."\\".$importpathnode."\\mh_tasimagorevilistele_$t.json";
            } else {
                $exportFileName = "\\\\localhost\\paylaşım\\mh_tasimagorevilistele_$t.json";
            }
            $exportFile = fopen($exportFileName, 'w');
            # Now UTF-8 - Add byte order mark
            //fwrite($exportFile, pack("CCC",0xef,0xbb,0xbf));
            fwrite($exportFile, $data);
            fclose($exportFile);
            $this->errinsert("f-w-ok", "path:".$exportFileName." data:".$data, $pos);
        } catch (Exception $e) {
            echo("dosyayaznodetrigger! ".$e->getMessage()."\n");
        }
    }

    public function errinsert($pos, $msg, $code=100)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //$conn->beginTransaction();
        try {
            //$sql = "select * from _log_exception where path=:path and message=:message and time>current_timestamp - interval '1' hour";
            //$stmt = $conn->prepare($sql);
            //$stmt->bindValue('path', $pos);
            //$stmt->bindValue('message', $msg);
            //$stmt->execute();
            //$records = $stmt->fetchAll();
            //if(count($records)==0){
            $sql= "insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,current_timestamp)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('code', $code);
            $stmt->bindValue('file', '');
            $stmt->bindValue('line', 0);
            $stmt->bindValue('message', $msg);
            $stmt->bindValue('method', 'MH');
            $stmt->bindValue('path', $pos);
            $stmt->bindValue('sessionid', 0);
            $stmt->execute();
            //}
            //$conn->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return;
    }
}
