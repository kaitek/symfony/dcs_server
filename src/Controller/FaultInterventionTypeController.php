<?php

namespace App\Controller;

use App\Entity\FaultInterventionType;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FaultInterventionTypeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:FaultInterventionType';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    /**
     * @Route(path="/FaultInterventionType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="FaultInterventionType-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/FaultInterventionType/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="FaultInterventionType-getComboValues", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesFaultInterventionType(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = '')
    {
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val, "  ");
    }

    public function getNewEntity()
    {
        return new FaultInterventionType();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('fit.id,fit.code,fit.description,fit.version')
                ->from('App:FaultInterventionType', 'fit')
                ->where('fit.deleteuserId is null')
                ->orderBy('fit.code', 'ASC');
        $queries['FaultInterventionType'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/FaultInterventionType/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="FaultInterventionType-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/FaultInterventionType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="FaultInterventionType-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/FaultInterventionType", name="FaultInterventionType-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/FaultInterventionType.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultInterventionType/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="FaultInterventionType-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'FaultInterventionType', $id);

            return $this->render('Modules/FaultInterventionType.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultInterventionType/{id}", requirements={"id": "\d+"}, name="FaultInterventionType-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'FaultInterventionType', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultInterventionType/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="FaultInterventionType-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
