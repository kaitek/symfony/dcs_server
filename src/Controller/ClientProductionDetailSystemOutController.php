<?php
/*
 * kayıt girişi toplu yapılacak
 * silme için gridde her kaydın yanındaki check seçilerek toplu silme yapılacak
 *
 */

namespace App\Controller;

use App\Entity\ClientProductionDetail;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientProductionDetailSystemOutController extends DcsBaseController implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:ClientProductionDetail';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/ClientProductionDetailSystemOut/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientProductionDetailSystemOut-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new ClientProductionDetail();
    }

    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT * FROM (
            select 0 del,0 sec,cpd.id,cpd.client,cpd.day,cpd.jobrotation
                ,cpd.employee,e.name employee_lookup_
                ,cpd.start,cpd.finish
                ,cast(cpd.start as varchar(10)) start_d,cast(cpd.finish as varchar(10)) finish_d
                ,right(cast(cpd.start as varchar(19)),8)start_t,right(cast(cpd.finish as varchar(19)),8)finish_t
                ,case when cpd.type='c_p_unconfirm' then 'İstasyon' else 'Operatör' end tip
                ,cpd.opname,cpd.productioncurrent,cpd.version
            from client_production_details cpd
            left join employees e on e.code=cpd.employee
            where cpd.type in ('c_p_unconfirm','e_p_unconfirm') and cpd.finish is not null and e.finish is null 
        )cpd
        where 1=1 @@where@@
        ORDER BY cpd.day DESC,cpd.start desc";
        $queries['ClientProductionDetailSystemOut'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('jre.id,jre.day,jre.week,jre.jobrotation'
                . ',jre.jobrotationteam,jre.employee,jre.isovertime'
                . ',jre.beginval,jre.endval,jre.worktime,jre.breaktime'
                . ',jre.version')
                ->from('App:ClientProductionDetail', 'jre')
                ->where('jre.deleteuserId is null')
                ->orderBy('jre.day', 'DESC');
        $queries['ClientProductionDetail'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ClientProductionDetailSystemOut/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientProductionDetailSystemOut-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgSuccess();
    }

    /**
     * @Route(path="/ClientProductionDetailSystemOut/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientProductionDetailSystemOut-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbu=$this->checkBeforeUpdate($request, $id, $entity, $v);
        if ($cbu===true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $content->production=$content->productioncurrent;
            $content->start=$content->start_d." ".$content->start_t;
            $content->finish=$content->finish_d." ".$content->finish_t;
            $arr_code_number=explode('-', $content->opname);
            $content->opnumber=$arr_code_number[count($arr_code_number)-1];
            array_pop($arr_code_number);
            $content->opcode=implode('-', $arr_code_number);
            $d_s=new \DateTime($content->start);
            $d_f=new \DateTime($content->finish);
            if ($d_s>=$d_f) {
                return $this->msgError(($this->_container==null ? $this->container : $this->_container)->get('translator')->trans('ClientProductionDetail.errdatestartgreaterthanfinish', array(), 'ClientProductionDetail'));
            }
            $user = $this->getUser();
            $userId = $user->getId();
            //$record=$this->getDoctrine()
            //    ->getRepository(self::ENTITY)
            //    ->find($content->id);
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                //$record->setType(($record->getType()=='c_p_unconfirm'?'c_p':'e_p'));
                //if($record->getType()=='e_p_unconfirm'){
                //    $record->setEmployee($content->employee);
                //}
                //$record->setOpcode($content->opcode);
                //$record->setOpnumber($content->opnumber);
                //$record->setOpname($content->opname);
                //$record->setProduction($content->production);
                //$record->setProductioncurrent($content->productioncurrent);
                //$em->persist($record);
                //$em->flush();
                $sql="update client_production_details 
                    set type=:type
                        ,opcode=:opcode
                        ,opnumber=:opnumber
                        ,opname=:opname
                        ,production=:production
                        ,productioncurrent=:productioncurrent 
                        ,update_user_id=:update_user_id
                        ".($content->tip=='Operatör' ? ",employee=:employee " : "")."
                    where id=:id";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('type', ($content->tip=='Operatör' ? 'e_p_confirm' : 'c_p_confirm'));
                $stmt->bindValue('opcode', $content->opcode);
                $stmt->bindValue('opnumber', $content->opnumber);
                $stmt->bindValue('opname', $content->opname);
                $stmt->bindValue('production', $content->production);
                $stmt->bindValue('productioncurrent', $content->productioncurrent);
                $stmt->bindValue('update_user_id', $userId);
                if ($content->tip=='Operatör') {
                    $stmt->bindValue('employee', $content->employee);
                }
                $stmt->bindValue('id', $content->id);
                $stmt->execute();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
        //}
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/ClientProductionDetailSystemOut", name="ClientProductionDetailSystemOut-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees', 'code', "concat(code,'-',name)", '', ' and finish is null ');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['job_rotations']=json_decode($job_rotations->getContent())->records;
            $data['extras']['employees']=json_decode($employees->getContent())->records;
            return $this->render('Modules/ClientProductionDetailSystemOut.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientProductionDetailSystemOut/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ClientProductionDetailSystemOut-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ClientProductionDetailSystemOut', $id);

            return $this->render('Modules/ClientProductionDetailSystemOut.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientProductionDetailSystemOut/{id}", requirements={"id": "\d+"}, name="ClientProductionDetailSystemOut-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ClientProductionDetailSystemOut', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientProductionDetailSystemOut/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientProductionDetailSystemOut-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
