<?php
namespace App\Controller;

use App\Entity\TaskCase;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TaskCaseController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:TaskCase';
    var $uri;
    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/TaskCase/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskCase-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    public function getNewEntity() {
        return new TaskCase();
    }

    public function getQBQuery() {
        
        return array();
    }
    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT tc.id,tc.erprefnumber,substring(cast(tc.start as varchar),6,14) \"start\",substring(cast(tc.finish as varchar),6,14)finish,concat(carrier,'-',empcode) carrier,tc.opname,tc.stockcode "
                . "     ,tc.stockcode,cm.locationsource,tc.casename,tc.casetype"
                . "     ,cast(tc.packcapacity as integer)packcapacity,cast(tc.quantityremaining as integer)quantityremaining "
                . "     ,case when tc.finish is null then 0 else 1 end isclosed "
                . " FROM task_cases tc "
                . " join case_movements cm on cm.id=tc.movementdetail "
                . " WHERE 1=1 @@where@@ "
                . " ORDER BY tc.erprefnumber DESC,tc.opname ASC,tc.casetype ASC,tc.stockcode ASC,tc.id";
        if(($_SERVER['HTTP_HOST']=='10.10.0.10'||$_SERVER['HTTP_HOST']=='192.168.1.40')){
            $_sql = "SELECT tc.id,tc.erprefnumber,substring(cast(tc.start as varchar),6,14) \"start\",substring(cast(tc.finish as varchar),6,14)finish
                    ,'' carrier,tc.opname,tc.stockcode,tc.client,tc.status,tc.casenumber
                    ,tc.stockcode,'' locationsource,tc.casename,tc.casetype
                    ,cast(tc.packcapacity as integer)packcapacity,cast(tc.quantityremaining as integer)quantityremaining
                    ,case when tc.finish is null then 0 else 1 end isclosed
                FROM task_cases tc
                WHERE 1=1 and tc.status='TCC' @@where@@
                ORDER BY tc.erprefnumber DESC,tc.opname ASC,tc.casetype ASC,tc.stockcode ASC,tc.id";
        }
        $queries['TaskCase'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/TaskCase/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskCase-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    /**
     * @Route(path="/TaskCase/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="TaskCase-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        //return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
        $cbu=$this->checkBeforeUpdate($request,$id,$entity,-1);
        if($cbu===true){
            $content = json_decode($request->getContent());
            if($content->quantityremaining>$content->packcapacity){
                return $this->msgError("Kasa miktarından fazla kalan miktar girilemez.");
            }
            $arrnodeinfo=array();
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $entity->setDeliverytime(null);
                $entity->setFinish(null);
                $entity->setFinishtype(null);
                if (!($_SERVER['HTTP_HOST']=='10.10.0.10'||$_SERVER['HTTP_HOST']=='192.168.1.40')) {
                    $entity->setStatus('WORK');
                }
                $entity->setPackcapacity($content->packcapacity);
                $entity->setQuantityremaining($content->quantityremaining);
                $em->persist($entity);
                $em->flush();
                $em->clear();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        }else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/TaskCase", name="TaskCase-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            return $this->render('Modules/TaskCase.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskCase/{id}", requirements={"id": "\d+"}, name="TaskCase-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'TaskCase', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/TaskCase/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="TaskCase-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $this->uri = strtolower($request->getBaseUrl());
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    function errinsert($pos,$msg,$code=100){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //$conn->beginTransaction();
        try{
            //$sql = "select * from _log_exception where path=:path and message=:message and time>current_timestamp - interval '1' hour";
            //$stmt = $conn->prepare($sql);
            //$stmt->bindValue('path', $pos);
            //$stmt->bindValue('message', $msg);
            //$stmt->execute();
            //$records = $stmt->fetchAll();
            //if(count($records)==0){
            $sql= "insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,current_timestamp)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('code', $code);
            $stmt->bindValue('file', '');
            $stmt->bindValue('line', 0);
            $stmt->bindValue('message', $msg);
            $stmt->bindValue('method', 'MH');
            $stmt->bindValue('path', $pos);
            $stmt->bindValue('sessionid', 0);
            $stmt->execute();
            //}
            //$conn->commit();
        }catch (Exception $e) {
            echo $e->getMessage();
        }
        return;
    }

}