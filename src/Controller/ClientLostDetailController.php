<?php
/*
 * kayıt girişi toplu yapılacak
 * silme için gridde her kaydın yanındaki check seçilerek toplu silme yapılacak
 *
 */

namespace App\Controller;

use App\Entity\ClientLostDetail;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientLostDetailController extends DcsBaseController implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:ClientLostDetail';
    public $resp = false;
    public $dayCount = 60;

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType = self::QUERY_TYPE_SQL;
        if ($_SERVER['HTTP_HOST'] == '10.10.0.10') {
            $this->dayCount = 20;
        }
    }

    /**
     * @Route(path="/ClientLostDetail/{pg}/{lm}/{id}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+"}, name="ClientLostDetail-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v = 0)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        if ($cbd === true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $this->dataExporter($content->record, 'DELETE');
                //şimdilik silinen kayıtlar için audit olmayacak
                $sql = "delete from client_lost_details where finish is not null and client=:client and day=:day and start>=:start and finish<=:finish ";
                if ($content->record->tip == 'Operatör') {
                    $sql .= " and type in ('l_e','l_e_t','l_e_out') ";
                }
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $content->record->client);
                $stmt->bindValue('day', $content->record->day);
                $stmt->bindValue('start', $content->record->start);
                $stmt->bindValue('finish', $content->record->finish);
                $stmt->execute();
                $conn->commit();
                if ($content->calculate) {
                    $this->efficiencyCalculate($content->record->day);
                }
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
            //}
        } else {
            return $cbd;
        }
    }

    public function getNewEntity()
    {
        return new ClientLostDetail();
    }

    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT 0 del,min(cld.id)id,cld.client,cld.day,cld.jobrotation,min(cld.tip)tip
            ,cld.start,cld.finish,cld.suredk,cld.start_d,cld.finish_d,cld.start_t,cld.finish_t,cld.losttype
            ,string_agg(distinct cld.employee,'|')employee,string_agg(distinct cld.opname,'|')opname
            ,string_agg(distinct cld.employees,'|')employees,string_agg(distinct cld.opnames,'|')opnames
            ,max(cld.descriptionlost)descriptionlost
            ,max(cld.version)\"version\"
        FROM (
            select cld.id,cld.client,cld.type,cld.day,cld.jobrotation
                ,case when cld.type in ('l_c','l_c_out') then 'İstasyon' else 'Operatör' end tip
                ,case when cld.employee is not null then concat(e.name) else null end employee
                ,case when cld.opname is not null then concat(cld.opname) else null end opname
                ,case when cld.employee is not null then concat(cld.employee,'~',e.name) else null end employees
                ,case when cld.opname is not null then concat(cld.erprefnumber,'~',cld.opname) else null end opnames
                ,cld.start,cld.finish
                ,ceil(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60)suredk
                ,cast(cld.start as varchar(10)) start_d,cast(cld.finish as varchar(10)) finish_d
                ,right(cast(cld.start as varchar(19)),8)start_t,right(cast(cld.finish as varchar(19)),8)finish_t
                ,case when cld.type='l_e' then 1 else 0 end issystem
                ,cld.losttype,coalesce(cld.descriptionlost,'')descriptionlost,cld.version
            from client_lost_details cld
            left join employees e on e.code=cld.employee
            where cld.type in ('l_c','l_c_out','l_e','l_e_out','l_c_t') and cld.finish is not null and e.finish is null 
                and cld.day>CURRENT_DATE - interval '".$this->dayCount." days'
        )cld
        where 1=1 @@where@@
        GROUP BY cld.client,cld.day,cld.jobrotation,cld.start,cld.finish,cld.suredk,cld.start_d,cld.finish_d,cld.start_t,cld.finish_t,cld.losttype
        ORDER BY cld.day DESC,cld.suredk desc";
        $queries['ClientLostDetail'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('jre.id,jre.day,jre.week,jre.jobrotation'
                . ',jre.jobrotationteam,jre.employee,jre.isovertime'
                . ',jre.beginval,jre.endval,jre.worktime,jre.breaktime'
                . ',jre.version')
                ->from('App:ClientLostDetail', 'jre')
                ->where('jre.deleteuserId is null')
                ->orderBy('jre.day', 'DESC');
        $queries['ClientLostDetail'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/ClientLostDetail/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="ClientLostDetail-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $cba = $this->checkBeforeAdd($request);
        if ($cba === true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            $content->start = $content->start_d." ".$content->start_t;
            $content->finish = $content->finish_d." ".$content->finish_t;
            if ($content->client === null) {
                $content->client = '';
            }
            $d_s = new \DateTime($content->start);
            $d_f = new \DateTime($content->finish);
            if ($d_s >= $d_f) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ClientLostDetail.errdatestartgreaterthanfinish', array(), 'ClientLostDetail'));
            }
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $time_start = microtime();
                $this->resp = array("t1" => 0,"t2" => 0,"t3" => 0);
                if ($content->tip == 'İstasyon') {
                    $sql_lc = "insert into client_lost_details 
                        (type,client,day,jobrotation,losttype,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported,opsayi,refsayi)
                    values (:type,:client,:day,:jobrotation,:losttype,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false,:opsayi,:refsayi)
                    ";
                    $stmt_lc = $conn->prepare($sql_lc);
                    $stmt_lc->bindValue('type', 'l_c');
                    $stmt_lc->bindValue('client', $content->client);
                    $stmt_lc->bindValue('day', $content->day);
                    $stmt_lc->bindValue('jobrotation', $content->jobrotation);
                    $stmt_lc->bindValue('losttype', $content->losttype);
                    $stmt_lc->bindValue('start', $content->start);
                    $stmt_lc->bindValue('finish', $content->finish);
                    $stmt_lc->bindValue('record_id', $this->gen_uuid());
                    $stmt_lc->bindValue('create_user_id', $userId);
                    $stmt_lc->bindValue('update_user_id', $userId);
                    $stmt_lc->bindValue('version', 1);
                    $stmt_lc->bindValue('opsayi', count($content->employees));
                    $stmt_lc->bindValue('refsayi', count($content->productions));
                    $stmt_lc->execute();
                }
                foreach ($content->employees as $employee) {
                    $sql_le = "insert into client_lost_details 
                        (type,client,day,jobrotation,losttype,descriptionlost,employee,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported)
                    values (:type,:client,:day,:jobrotation,:losttype,:descriptionlost,:employee,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false)
                    ";
                    $stmt_le = $conn->prepare($sql_le);
                    $stmt_le->bindValue('type', 'l_e');
                    $stmt_le->bindValue('client', $content->client);
                    $stmt_le->bindValue('day', $content->day);
                    $stmt_le->bindValue('jobrotation', $content->jobrotation);
                    $stmt_le->bindValue('losttype', $content->losttype);
                    $stmt_le->bindValue('descriptionlost', $content->descriptionlost);
                    $stmt_le->bindValue('employee', $employee->code);
                    $stmt_le->bindValue('start', $content->start);
                    $stmt_le->bindValue('finish', $content->finish);
                    $stmt_le->bindValue('record_id', $this->gen_uuid());
                    $stmt_le->bindValue('create_user_id', $userId);
                    $stmt_le->bindValue('update_user_id', $userId);
                    $stmt_le->bindValue('version', 1);
                    $stmt_le->execute();
                }
                $this->resp['t1'] = round($this->microtime_diff($time_start, microtime()), 2);
                $time_start = microtime();
                foreach ($content->productions as $production) {
                    $arr_code_number = explode('-', $production->opname);
                    if (count($arr_code_number) > 1) {
                        $production->opnumber = $arr_code_number[count($arr_code_number) - 1];
                        array_pop($arr_code_number);
                        $production->opcode = implode('-', $arr_code_number);
                    } else {
                        $production->opnumber = 0;
                        $production->opcode = $production->opname;
                    }
                    if ($production->opnumber === '') {
                        $production->opnumber = 0;
                    }
                    if ($content->tip == 'İstasyon') {
                        $sql_lct = "insert into client_lost_details 
                            (type,client,day,jobrotation,losttype,descriptionlost,opcode,opnumber,opname,erprefnumber,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported)
                        values (:type,:client,:day,:jobrotation,:losttype,:descriptionlost,:opcode,:opnumber,:opname,:erprefnumber,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false)
                        ";
                        $stmt_lct = $conn->prepare($sql_lct);
                        $stmt_lct->bindValue('type', 'l_c_t');
                        $stmt_lct->bindValue('client', $content->client);
                        $stmt_lct->bindValue('day', $content->day);
                        $stmt_lct->bindValue('jobrotation', $content->jobrotation);
                        $stmt_lct->bindValue('losttype', $content->losttype);
                        $stmt_lct->bindValue('descriptionlost', $content->descriptionlost);
                        $stmt_lct->bindValue('opcode', $production->opcode);
                        $stmt_lct->bindValue('opnumber', $production->opnumber);
                        $stmt_lct->bindValue('opname', $production->opname);
                        $stmt_lct->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_lct->bindValue('start', $content->start);
                        $stmt_lct->bindValue('finish', $content->finish);
                        $stmt_lct->bindValue('record_id', $this->gen_uuid());
                        $stmt_lct->bindValue('create_user_id', $userId);
                        $stmt_lct->bindValue('update_user_id', $userId);
                        $stmt_lct->bindValue('version', 1);
                        $stmt_lct->execute();
                    }
                    foreach ($content->employees as $employee) {
                        $sql_let = "insert into client_lost_details 
                            (type,client,day,jobrotation,losttype,descriptionlost,opcode,opnumber,opname,employee,erprefnumber,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported)
                        values (:type,:client,:day,:jobrotation,:losttype,:descriptionlost,:opcode,:opnumber,:opname,:employee,:erprefnumber,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false)
                        ";
                        $stmt_let = $conn->prepare($sql_let);
                        $stmt_let->bindValue('type', 'l_e_t');
                        $stmt_let->bindValue('client', $content->client);
                        $stmt_let->bindValue('day', $content->day);
                        $stmt_let->bindValue('jobrotation', $content->jobrotation);
                        $stmt_let->bindValue('losttype', $content->losttype);
                        $stmt_let->bindValue('descriptionlost', $content->descriptionlost);
                        $stmt_let->bindValue('opcode', $production->opcode);
                        $stmt_let->bindValue('opnumber', $production->opnumber);
                        $stmt_let->bindValue('opname', $production->opname);
                        $stmt_let->bindValue('employee', $employee->code);
                        $stmt_let->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_let->bindValue('start', $content->start);
                        $stmt_let->bindValue('finish', $content->finish);
                        $stmt_let->bindValue('record_id', $this->gen_uuid());
                        $stmt_let->bindValue('create_user_id', $userId);
                        $stmt_let->bindValue('update_user_id', $userId);
                        $stmt_let->bindValue('version', 1);
                        $stmt_let->execute();
                    }
                }
                $conn->commit();
                if ($content->client !== '') {
                    $sql = "SELECT * from client_production_details where type not like 'c_s%' and client=:client and day=:day and jobrotation=:jobrotation and start<=:start and finish>=:finish order by start";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('client', $content->client);
                    $stmt->bindValue('day', $content->day);
                    $stmt->bindValue('jobrotation', $content->jobrotation);
                    $stmt->bindValue('start', $content->start);
                    $stmt->bindValue('finish', $content->finish);
                    $stmt->execute();
                    $records = $stmt->fetchAll();
                    foreach ($records as $row) {
                        $sql_cld = "SELECT COALESCE(sum(DATE_PART('day', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 24*3600 
                            + DATE_PART('hour', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 3600  
                            + DATE_PART('minute', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)*60  
                            + DATE_PART('second', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)),0) suresn  
                         from client_lost_details cld 
                         where type=:type and client=:client and day=:day and jobrotation=:jobrotation and losttype not in ('YETKİNLİK GÖZLEM') 
                            and ( start between :start and :finish  
                            or finish between :start and :finish 
                            or (start<:start and finish>:finish) 
                        )";
                        $stmt_cld = $conn->prepare($sql_cld);
                        $stmt_cld->bindValue('type', 'l_c');
                        $stmt_cld->bindValue('client', $row['client']);
                        $stmt_cld->bindValue('day', $row['day']);
                        $stmt_cld->bindValue('jobrotation', $row['jobrotation']);
                        $stmt_cld->bindValue('start', $row['start']);
                        $stmt_cld->bindValue('finish', $row['finish']);
                        $stmt_cld->execute();
                        $records_cld = $stmt_cld->fetchAll();
                        $sql_upd = "update client_production_details set losttime=:losttime where id=:id";
                        $stmt_upd = $conn->prepare($sql_upd);
                        $stmt_upd->bindValue('losttime', $records_cld[0]['suresn']);
                        $stmt_upd->bindValue('id', $row['id']);
                        $stmt_upd->execute();
                    }
                }
                $this->resp['t2'] = round($this->microtime_diff($time_start, microtime()), 2);
                $time_start = microtime();
                $this->dataExporter($content, 'POST');
                $this->resp['t3'] = round($this->microtime_diff($time_start, microtime()), 2);
                $time_start = microtime();
                if ($content->calculate) {
                    $this->efficiencyCalculate($content->day);
                }
                $this->resp['t4'] = round($this->microtime_diff($time_start, microtime()), 2);
                $time_start = microtime();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
            //}
        } else {
            return $cba;
        }
    }

    /**
     * @Route(path="/ClientLostDetail/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="ClientLostDetail-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v = 0)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        if ($entity == null || !$entity) {
            return $this->msgError(
                ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.not_found', array(), 'KaitekFrameworkBundle'),
                401
            );
        }
        $cbu = $this->checkBeforeUpdate($request, $id, $entity, $v);
        if ($cbu === true) {
            $content = json_decode($request->getContent());
            if ($content == null) {
                $content = $request->request->all();
            }
            $content = $this->clearLookup($content);
            if ($content->client === null) {
                $content->client = '';
            }
            $d_s = new \DateTime($content->start_d." ".$content->start_t);
            $d_f = new \DateTime($content->finish_d." ".$content->finish_t);
            if ($d_s >= $d_f) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ClientLostDetail.errdatestartgreaterthanfinish', array(), 'ClientLostDetail'));
            }
            $user = $this->getUser();
            $userId = $user->getId();
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                //eski kaydı sil
                $sql = "select id,type from client_lost_details where finish is not null and client=:client and day=:day and start>=:start and finish<=:finish ";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $content->client);
                $stmt->bindValue('day', $content->day);
                $stmt->bindValue('start', $content->start);
                $stmt->bindValue('finish', $content->finish);
                $stmt->execute();
                $records = $stmt->fetchAll();
                $dcs_export_data = ($this->_container == null ? $this->container : $this->_container)->getParameter('dcs_export_data');
                $dcs_export_path = ($this->_container == null ? $this->container : $this->_container)->getParameter('dcs_export_path');
                $b_e_lost = $dcs_export_data == 'E_ALL' || strpos($dcs_export_data, 'E_LOST') > -1;
                if ($b_e_lost) {
                    foreach ($records as $row) {
                        $result = array("id" => $row["id"],"type" => $row["type"]);
                        $exportFileName = $dcs_export_path.$row["id"]."-del-".$row["type"].".json";
                        $exportFile = fopen($exportFileName, 'w');
                        //fwrite($exportFile, pack("CCC",0xef,0xbb,0xbf));
                        $str = json_encode($result, JSON_UNESCAPED_UNICODE);
                        fwrite($exportFile, $str);
                        fclose($exportFile);
                    }
                }
                $sql = "delete from client_lost_details where finish is not null and client=:client and day=:day and start>=:start and finish<=:finish ";
                //if($content->tip=='Operatör'){
                //    $sql.=" and type in ('l_e','l_e_t','l_e_out') ";
                //}
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $content->client);
                $stmt->bindValue('day', $content->day);
                $stmt->bindValue('start', $content->start);
                $stmt->bindValue('finish', $content->finish);
                $stmt->execute();
                //yenileri oluştur
                $content->start = $content->start_d." ".$content->start_t;
                $content->finish = $content->finish_d." ".$content->finish_t;
                if ($content->tip == 'İstasyon') {
                    $sql_lc = "insert into client_lost_details 
                        (type,client,day,jobrotation,losttype,descriptionlost,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported,opsayi,refsayi)
                    values (:type,:client,:day,:jobrotation,:losttype,:descriptionlost,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false,:opsayi,:refsayi)
                    ";
                    $stmt_lc = $conn->prepare($sql_lc);
                    $stmt_lc->bindValue('type', 'l_c');
                    $stmt_lc->bindValue('client', $content->client);
                    $stmt_lc->bindValue('day', $content->day);
                    $stmt_lc->bindValue('jobrotation', $content->jobrotation);
                    $stmt_lc->bindValue('losttype', $content->losttype);
                    $stmt_lc->bindValue('descriptionlost', $content->descriptionlost);
                    $stmt_lc->bindValue('start', $content->start);
                    $stmt_lc->bindValue('finish', $content->finish);
                    $stmt_lc->bindValue('record_id', $this->gen_uuid());
                    $stmt_lc->bindValue('create_user_id', $userId);
                    $stmt_lc->bindValue('update_user_id', $userId);
                    $stmt_lc->bindValue('version', 1);
                    $stmt_lc->bindValue('opsayi', count($content->employees));
                    $stmt_lc->bindValue('refsayi', count($content->productions));
                    $stmt_lc->execute();
                }
                foreach ($content->employees as $employee) {
                    $sql_le = "insert into client_lost_details 
                        (type,client,day,jobrotation,losttype,descriptionlost,employee,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported)
                    values (:type,:client,:day,:jobrotation,:losttype,:descriptionlost,:employee,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false)
                    ";
                    $stmt_le = $conn->prepare($sql_le);
                    $stmt_le->bindValue('type', 'l_e');
                    $stmt_le->bindValue('client', $content->client);
                    $stmt_le->bindValue('day', $content->day);
                    $stmt_le->bindValue('jobrotation', $content->jobrotation);
                    $stmt_le->bindValue('descriptionlost', $content->descriptionlost);
                    $stmt_le->bindValue('losttype', $content->losttype);
                    $stmt_le->bindValue('employee', $employee->code);
                    $stmt_le->bindValue('start', $content->start);
                    $stmt_le->bindValue('finish', $content->finish);
                    $stmt_le->bindValue('record_id', $this->gen_uuid());
                    $stmt_le->bindValue('create_user_id', $userId);
                    $stmt_le->bindValue('update_user_id', $userId);
                    $stmt_le->bindValue('version', 1);
                    $stmt_le->execute();
                }
                foreach ($content->productions as $production) {
                    $arr_code_number = explode('-', $production->opname);
                    if (count($arr_code_number) > 1) {
                        $production->opnumber = $arr_code_number[count($arr_code_number) - 1];
                        array_pop($arr_code_number);
                        $production->opcode = implode('-', $arr_code_number);
                    } else {
                        $production->opnumber = 0;
                        $production->opcode = $production->opname;
                    }
                    if ($production->opnumber === '') {
                        $production->opnumber = 0;
                    }
                    if ($content->tip == 'İstasyon') {
                        $sql_lct = "insert into client_lost_details 
                            (type,client,day,jobrotation,losttype,descriptionlost,opcode,opnumber,opname,erprefnumber,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported)
                        values (:type,:client,:day,:jobrotation,:losttype,:descriptionlost,:opcode,:opnumber,:opname,:erprefnumber,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false)
                        ";
                        $stmt_lct = $conn->prepare($sql_lct);
                        $stmt_lct->bindValue('type', 'l_c_t');
                        $stmt_lct->bindValue('client', $content->client);
                        $stmt_lct->bindValue('day', $content->day);
                        $stmt_lct->bindValue('jobrotation', $content->jobrotation);
                        $stmt_lct->bindValue('losttype', $content->losttype);
                        $stmt_lct->bindValue('descriptionlost', $content->descriptionlost);
                        $stmt_lct->bindValue('opcode', $production->opcode);
                        $stmt_lct->bindValue('opnumber', $production->opnumber);
                        $stmt_lct->bindValue('opname', $production->opname);
                        $stmt_lct->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_lct->bindValue('start', $content->start);
                        $stmt_lct->bindValue('finish', $content->finish);
                        $stmt_lct->bindValue('record_id', $this->gen_uuid());
                        $stmt_lct->bindValue('create_user_id', $userId);
                        $stmt_lct->bindValue('update_user_id', $userId);
                        $stmt_lct->bindValue('version', 1);
                        $stmt_lct->execute();
                    }
                    foreach ($content->employees as $employee) {
                        $sql_let = "insert into client_lost_details 
                            (type,client,day,jobrotation,losttype,descriptionlost,opcode,opnumber,opname,employee,erprefnumber,start,finish,record_id,create_user_id,created_at,update_user_id,updated_at,version,isexported)
                        values (:type,:client,:day,:jobrotation,:losttype,:descriptionlost,:opcode,:opnumber,:opname,:employee,:erprefnumber,:start,:finish,:record_id,:create_user_id,CURRENT_TIMESTAMP(0),:update_user_id,CURRENT_TIMESTAMP(0),:version,false)
                        ";
                        $stmt_let = $conn->prepare($sql_let);
                        $stmt_let->bindValue('type', 'l_e_t');
                        $stmt_let->bindValue('client', $content->client);
                        $stmt_let->bindValue('day', $content->day);
                        $stmt_let->bindValue('jobrotation', $content->jobrotation);
                        $stmt_let->bindValue('losttype', $content->losttype);
                        $stmt_let->bindValue('descriptionlost', $content->descriptionlost);
                        $stmt_let->bindValue('opcode', $production->opcode);
                        $stmt_let->bindValue('opnumber', $production->opnumber);
                        $stmt_let->bindValue('opname', $production->opname);
                        $stmt_let->bindValue('employee', $employee->code);
                        $stmt_let->bindValue('erprefnumber', $production->erprefnumber);
                        $stmt_let->bindValue('start', $content->start);
                        $stmt_let->bindValue('finish', $content->finish);
                        $stmt_let->bindValue('record_id', $this->gen_uuid());
                        $stmt_let->bindValue('create_user_id', $userId);
                        $stmt_let->bindValue('update_user_id', $userId);
                        $stmt_let->bindValue('version', 1);
                        $stmt_let->execute();
                    }
                }
                $conn->commit();
                if ($content->client !== '') {
                    $sql = "SELECT * from client_production_details where type not like 'c_s%' and client=:client and day=:day and start<=:start and finish>=:finish order by start";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('client', $content->client);
                    $stmt->bindValue('day', $content->day);
                    $stmt->bindValue('start', $content->start);
                    $stmt->bindValue('finish', $content->finish);
                    $stmt->execute();
                    $records = $stmt->fetchAll();
                    foreach ($records as $row) {
                        $sql_cld = "SELECT COALESCE(sum(DATE_PART('day', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 24*3600 
                            + DATE_PART('hour', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp) * 3600  
                            + DATE_PART('minute', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)*60  
                            + DATE_PART('second', coalesce((case when cld.finish>:finish then :finish else cld.finish end),(case when cld.start<:start then :start else cld.start end))::timestamp - (case when cld.start<:start then :start else cld.start end)::timestamp)),0) suresn  
                        from client_lost_details cld 
                        where type=:type and client=:client and day=:day and jobrotation=:jobrotation and losttype not in ('YETKİNLİK GÖZLEM') 
                            and ( start between :start and :finish  
                            or finish between :start and :finish 
                            or (start<:start and finish>:finish) 
                        )";
                        $stmt_cld = $conn->prepare($sql_cld);
                        $stmt_cld->bindValue('type', 'l_c');
                        $stmt_cld->bindValue('client', $row['client']);
                        $stmt_cld->bindValue('day', $row['day']);
                        $stmt_cld->bindValue('jobrotation', $row['jobrotation']);
                        $stmt_cld->bindValue('start', $row['start']);
                        $stmt_cld->bindValue('finish', $row['finish']);
                        $stmt_cld->execute();
                        $records_cld = $stmt_cld->fetchAll();
                        $sql_upd = "update client_production_details set losttime=:losttime where id=:id";
                        $stmt_upd = $conn->prepare($sql_upd);
                        $stmt_upd->bindValue('losttime', $records_cld[0]['suresn']);
                        $stmt_upd->bindValue('id', $row['id']);
                        $stmt_upd->execute();
                    }
                }
                $this->dataExporter($content, 'PUT');
                if ($content->calculate) {
                    $this->efficiencyCalculate($content->day);
                }
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            //if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
            //    return $this->showAllAction($request, $_locale, $pg, $lm);
            //} else {
            return $this->msgSuccess();
            //}
        } else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/ClientLostDetail/sourcelosttype/{id}", requirements={"id": "\d+"}, name="ClientLostDetail-update-sourcelosttype", options={"expose"=true}, methods={"PUT"})
     */
    public function putSourcelosttypeAction(Request $request, $_locale, $id)
    {
        $content = json_decode($request->getContent());
        if ($content == null) {
            $content = $request->request->all();
        }
        // ilgili kuyruğa kayıp kapatma için veri yazılıyor
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $conn->beginTransaction();
        try {
            $time = microtime(true);
            $micro = sprintf("%06d", ($time - floor($time)) * 1000000);
            $d = new \DateTime(date('Y-m-d H:i:s.'.$micro, $time));

            $_d = $d->format("Y-m-d H:i:s.u");
            $sql = "insert into __sync_messages (queuename,messagetype,priority,status,time,data,info)
            values (:queuename,:messagetype,:priority,:status,:time,:data,:info)";
            //    $sql="update client_lost_details set sourcelosttype=:sourcelosttype where id=:id ";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('queuename', ($content->client.'_server'));
            $stmt->bindValue('messagetype', 'close_lost_fb');
            $stmt->bindValue('priority', 1);
            $stmt->bindValue('status', 'wait_for_send');
            $stmt->bindValue('time', $_d);
            $stmt->bindValue('data', json_encode($content));
            $stmt->bindValue('info', json_encode(array("tableName" => "client_lost_details")));
            $stmt->execute();
            $conn->commit();
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            $conn->rollback();
            //throw $e;
            return $this->msgError($e->getMessage());
        }
        return $this->msgSuccess();
    }

    /**
     * @Route(path="/ClientLostDetail", name="ClientLostDetail-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY, 0);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients', 'code', 'code', '', "  ");//and uuid is not null
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $lost_types = $this->getComboValues($request, $_locale, 1, 1000, 'lost_types');
            $employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees', 'code', "concat(code,'-',name)", '', ' and finish is null ');
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['lost_types'] = json_decode($lost_types->getContent())->records;
            $data['extras']['employees'] = json_decode($employees->getContent())->records;
            return $this->render('Modules/ClientLostDetail.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientLostDetail/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="ClientLostDetail-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'ClientLostDetail', $id);

            return $this->render('Modules/ClientLostDetail.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientLostDetail/{id}", requirements={"id": "\d+"}, name="ClientLostDetail-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'ClientLostDetail', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/ClientLostDetail/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="ClientLostDetail-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $time_start = microtime();
            $records = $this->getAllRecords($this, $request, $pg, $lm);
            $this->resp['t5'] = round($this->microtime_diff($time_start, microtime()), 2);
            $time_start = microtime();
            $records["resp"] = $this->resp;
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    private function dataExporter($content, $type)
    {
        $dcs_export_data = ($this->_container == null ? $this->container : $this->_container)->getParameter('dcs_export_data');
        $b_e_lost = $dcs_export_data == 'E_ALL' || strpos($dcs_export_data, 'E_LOST') > -1;
        if ($dcs_export_data === 'E_NONE' || $b_e_lost == false) {
            return;
        }
        $records = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->findBy(array('client' => $content->client, 'day' => new \DateTime($content->day), 'jobrotation' => $content->jobrotation, 'start' => new \DateTime($content->start), 'finish' => new \DateTime($content->finish)));
        try {
            $dcs_export_path = ($this->_container == null ? $this->container : $this->_container)->getParameter('dcs_export_path');
            foreach ($records as $row) {
                if ($b_e_lost) {
                    $result = array();
                    $className = get_class($row);
                    $uow = $this->getDoctrine()->getManager()->getUnitOfWork();
                    $entityPersister = $uow->getEntityPersister($className);
                    $classMetadata = $entityPersister->getClassMetadata();
                    foreach ($uow->getOriginalEntityData($row) as $field => $value) {
                        if (isset($classMetadata->columnNames[$field])) {
                            $columnName = $classMetadata->columnNames[$field];
                            $v_field = $classMetadata->fieldMappings[$field];
                            $v_type = $v_field['type'];
                            if ($v_type == 'date' && $value != null) {
                                $value = $value->format('Y-m-d');
                            }
                            if ($v_type == 'datetime' && $value != null) {
                                $value = $value->format('Y-m-d H:i:s');
                            }
                            $result[$columnName] = $value;
                        }
                    }
                    if ($content->type == 'l_c_t') {
                        $em = $this->getDoctrine()->getManager();
                        $conn = $em->getConnection();
                        $conn->beginTransaction();
                        $sql_emp_count = "select count(*) sayi from(select count(*) from client_lost_details where client=:client and start=:start and type='l_e' GROUP BY opname)a";
                        $stmt = $conn->prepare($sql_emp_count);
                        $stmt->bindValue('client', $content->client);
                        $stmt->bindValue('start', $content->start);
                        $stmt->execute();
                        $records = $stmt->fetchAll();
                        if (count($records) > 0) {
                            $result['empcount'] = $records[0]['sayi'];
                        }
                    }
                    $result['losttype'] = $content->losttype;
                    $result['losttypechanged'] = true;
                    $result['proctype'] = ($type == 'PUT' ? 'update' : ($type == 'POST' ? 'insert' : 'delete'));
                    $exportFileName = $dcs_export_path.$row->getId()."-".$row->getType().".json";
                    $exportFile = fopen($exportFileName, 'w');
                    //fwrite($exportFile, pack("CCC",0xef,0xbb,0xbf));
                    $str = json_encode($result, JSON_UNESCAPED_UNICODE);
                    fwrite($exportFile, $str);
                    fclose($exportFile);
                }
            }
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            //throw $e;
            return $this->msgError($e->getMessage());
        }
        return;
    }

    public function microtime_diff($start, $end = null)
    {
        if (!$end) {
            $end = microtime();
        }
        list($start_usec, $start_sec) = explode(" ", $start);
        list($end_usec, $end_sec) = explode(" ", $end);
        $diff_sec = intval($end_sec) - intval($start_sec);
        $diff_usec = floatval($end_usec) - floatval($start_usec);
        return floatval($diff_sec) + $diff_usec;
    }
}
