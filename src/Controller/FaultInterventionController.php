<?php

namespace App\Controller;

use App\Entity\FaultIntervention;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FaultInterventionController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:FaultIntervention';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/FaultIntervention/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="FaultIntervention-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->find($id);

        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    public function getNewEntity()
    {
        return new FaultIntervention();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('fi.clients,fi.')
                ->from('App:FaultIntervention', 'fi')
                ->where('fi.deleteuserId is null')
                ->orderBy('fi.id', 'ASC');
        $queries['FaultIntervention'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT fi.* 
        FROM fault_interventions fi 
        ORDER BY fi.id ASC";
        $queries['FaultIntervention'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/FaultIntervention/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="FaultIntervention-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $content = $request->getContent();
        $this->_requestData = json_decode($request->getContent());
        if (isset($this->_requestData->startday)) {
            $this->_requestData->finishday=$this->_requestData->startday;
        }
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/FaultIntervention/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="FaultIntervention-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $content = $request->getContent();
        $this->_requestData = json_decode($request->getContent());
        if (isset($this->_requestData->startday)) {
            $this->_requestData->finishday=$this->_requestData->startday;
        }
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/FaultIntervention", name="FaultIntervention-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $fault_intervention_types = $this->getComboValues($request, $_locale, 1, 1000, 'fault_intervention_types');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['fault_intervention_types']=json_decode($fault_intervention_types->getContent())->records;
            return $this->render('Modules/FaultIntervention.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultIntervention/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="FaultIntervention-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'FaultIntervention', $id);

            return $this->render('Modules/FaultIntervention.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultIntervention/{id}", requirements={"id": "\d+"}, name="FaultIntervention-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'FaultIntervention', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/FaultIntervention/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="FaultIntervention-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
