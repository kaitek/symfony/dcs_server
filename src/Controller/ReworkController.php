<?php

namespace App\Controller;

use App\Entity\Rework;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReworkController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:Rework';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/Rework/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Rework-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v){
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
        //if($entity->getStart()==null){
        //    return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
        //}else{
        //    /** @var EntityManager $em */
        //    $em = $this->getDoctrine()->getManager();
        //    $qb_cpd = $em->createQueryBuilder();
        //    $qb_cpd = $qb_cpd->select('cpd.id')
        //        ->from('App:ClientProductionDetail', 'cpd')
        //        ->where('cpd.finish is null and cpd.type<>\'c_s\' and cpd.erprefnumber=:erprefnumber')
        //        ->setParameters(array('erprefnumber' => $entity->getErprefnumber()));
        //    $cpd=$qb_cpd->getQuery()->getArrayResult();
        //    if(count($cpd)===0){
        //        $qb_cld = $em->createQueryBuilder();
        //        $qb_cld = $qb_cld->select('cld.id')
        //            ->from('App:ClientLostDetail', 'cld')
        //            ->where('cld.finish is null and cld.type=\'l_c_t\' and cld.erprefnumber=:erprefnumber')
        //            ->setParameters(array('erprefnumber' => $entity->getErprefnumber()));
        //        $cld=$qb_cld->getQuery()->getArrayResult();
        //        if(count($cld)===0){
        //            return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
        //        }else{
        //            return $this->msgError('İş emrine başlandığı için kayıt silinemiyor!');
        //        }
        //    }else{
        //        return $this->msgError('İş emrine başlandığı için kayıt silinemiyor!');
        //    }
        //}
    }
   
    /**
     * @Route(path="/Rework/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="Rework-getComboValues-client", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesClient(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = ''){
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val," and workflow='El İşçiliği' ");
    }

    public function getNewEntity()
    {
        return new Rework();
    }

    public function getQBQuery(){
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb = $qb->select('r')
            ->from(self::ENTITY, 'r')
            ->where('r.finish is null')
            ->orderBy('r.islisted', 'DESC')
            ->addOrderBy('r.client', 'ASC')
            ->addOrderBy('r.listorder', 'ASC')
            ->addOrderBy('r.opname', 'ASC');
        $queries['Rework'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/Rework/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Rework-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm){
        $this->_requestData = json_decode($request->getContent());
        if($this->_requestData == null){
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb_task = $em->createQueryBuilder();
        $qb_task = $qb_task->select('r.id')
            ->from('App:Rework', 'r')
            ->where('r.finish is null and r.client=:client and r.opname=:opname and r.reworktype=:reworktype')
            ->setParameters(array('client' => $this->_requestData->client,'opname' => $this->_requestData->opname, 'reworktype' => $this->_requestData->reworktype));
        $tl=$qb_task->getQuery()->getArrayResult();
        if(count($tl)===0){
            $qb = $em->createQueryBuilder();
            $qb = $qb->select('p.id, p.code, p.name, p.description, p.tpp, p.number')
                ->from('App:ProductTree', 'p')
                ->where('p.name = :name')
                ->andWhere('p.materialtype = :materialtype')
                ->setParameters(array('name' => $this->_requestData->opname, 'materialtype' => 'O'));
            $productTree = $qb->getQuery()->getArrayResult();
            if(count($productTree) == 1){
                $productTree = $productTree[0];
                $this->_requestData->opcode = $productTree['code'];
                $this->_requestData->opname = $productTree['name'];
                $this->_requestData->opnumber = $productTree['number'];
                //$this->_requestData->opdescription = $productTree['description'];
                //$this->_requestData->tpp = $productTree['tpp'];
            }
            $this->_requestData->code = $this->_requestData->reworktype . time();
            //$this->_requestData->type = 'quantity';
    
            return $this->recordAdd($request, $_locale, $pg, $lm);
        }else{
            return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('Rework.errRecordAdd', array(), 'Rework') );
        }
    }

    /**
     * @Route(path="/Rework/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Rework-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v) {
        $this->_requestData = json_decode($request->getContent());
        if($this->_requestData == null){
            $this->_requestData = $request->request->all();
        }
        $this->_requestData = $this->clearLookup($this->_requestData);
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('p.id, p.code, p.name, p.description, p.tpp, p.number')
            ->from('App:ProductTree', 'p')
            ->where('p.name = :name')
            ->andWhere('p.materialtype = :materialtype')
            ->setParameters(array('name' => $this->_requestData->opname, 'materialtype' => 'O'));
        $productTree = $qb->getQuery()->getArrayResult();
        if(count($productTree) == 1){
            $productTree = $productTree[0];
            $this->_requestData->opcode = $productTree['code'];
            $this->_requestData->opname = $productTree['name'];
            $this->_requestData->opnumber = $productTree['number'];
        }
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/Rework", name="Rework-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $rework_types = $this->getComboValues($request, $_locale, 1, 1000, 'rework_types');
            $data['extras']['clients']=json_decode($clients->getContent())->records;
            $data['extras']['rework_types']=json_decode($rework_types->getContent())->records;
            return $this->render('Modules/Rework.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Rework/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Rework-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Rework', $id);

            return $this->render('Modules/Rework.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Rework/{id}", requirements={"id": "\d+"}, name="Rework-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Rework', $id);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Rework/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Rework-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm){
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
