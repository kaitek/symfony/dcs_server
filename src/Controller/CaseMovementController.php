<?php
namespace App\Controller;

use App\Entity\CaseMovement;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CaseMovementController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:CaseMovement';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/CaseMovement/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="CaseMovement-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    public function getNewEntity() {
        return new CaseMovement();
    }

    public function getQBQuery() {
        
        return array();
    }
    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT cm.*
            ,cast(starttime as varchar(10)) sdate,substr(cast(starttime as varchar(20)), 12, 5) stime
            ,cast(assignmenttime as varchar(10)) adate,substr(cast(assignmenttime as varchar(20)), 12, 5) atime
            ,cast(targettime as varchar(10)) tdate,substr(cast(targettime as varchar(20)), 12, 5) ttime
            ,case when (actualfinishtime-starttime)>(targettime-starttime) then 
	            round(cast(EXTRACT(hour from (actualfinishtime-starttime))*60*60+ EXTRACT(minutes from (actualfinishtime-starttime))*60+ EXTRACT(seconds from (actualfinishtime-starttime)) as int)
                -cast(EXTRACT(hour from (targettime-starttime))*60*60+ EXTRACT(minutes from (targettime-starttime))*60+ EXTRACT(seconds from (targettime-starttime)) as int))/60 
            else 0 end yapilmadk
            ,case when (actualfinishtime-assignmenttime)>(targettime-starttime) then 
	            round(cast(EXTRACT(hour from (actualfinishtime-assignmenttime))*60*60+ EXTRACT(minutes from (actualfinishtime-assignmenttime))*60+ EXTRACT(seconds from (actualfinishtime-assignmenttime)) as int)
                -cast(EXTRACT(hour from (targettime-starttime))*60*60+ EXTRACT(minutes from (targettime-starttime))*60+ EXTRACT(seconds from (targettime-starttime)) as int))/60 
            else 0 end yapilmasicildk
            ,round(cm.breaktimeseconds/60)molasuresi
        FROM case_movements cm 
        WHERE 1=1 @@where@@ 
        ORDER BY cm.starttime DESC,cm.code ASC,cm.number ASC,cm.stockcode ASC,cm.id";
        $queries['CaseMovement'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/CaseMovement/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="CaseMovement-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgError(
            ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
            401
        );
    }

    /**
     * @Route(path="/CaseMovement/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="CaseMovement-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        //return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
        $cbu=$this->checkBeforeUpdate($request,$id,$entity,-1);
        if($cbu===true){
            $content = json_decode($request->getContent());
            if($content->sdate==null||$content->sdate==''||$content->stime==null||$content->stime==''){
                return $this->msgError("Başlangıç zamanı girmediniz.");
            }
            if($content->adate==null||$content->adate==''||$content->atime==null||$content->atime==''){
                return $this->msgError("Atama zamanı girmediniz.");
            }
            //$user = $this->getUser();
            //$userId = $user->getId();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $entity->setStarttime(new \DateTime($content->sdate.' '.$content->stime.':00'));
                $entity->setAssignmenttime(new \DateTime($content->adate.' '.$content->atime.':00'));
                $entity->setTargettime(new \DateTime($content->tdate.' '.$content->ttime.':00'));
                $em->persist($entity);
                $em->flush();
                $em->clear();
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            if(method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true){
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        }else {
            return $cbu;
        }
    }

    /**
     * @Route(path="/CaseMovement", name="CaseMovement-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/CaseMovement.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/CaseMovement/{id}", requirements={"id": "\d+"}, name="CaseMovement-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'CaseMovement', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/CaseMovement/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="CaseMovement-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    function errinsert($pos,$msg,$code=100){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        //$conn->beginTransaction();
        try{
            //$sql = "select * from _log_exception where path=:path and message=:message and time>current_timestamp - interval '1' hour";
            //$stmt = $conn->prepare($sql);
            //$stmt->bindValue('path', $pos);
            //$stmt->bindValue('message', $msg);
            //$stmt->execute();
            //$records = $stmt->fetchAll();
            //if(count($records)==0){
            $sql= "insert into _log_exception (code,file,line,message,method,path,sessionid,time) values (:code,:file,:line,:message,:method,:path,:sessionid,current_timestamp)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('code', $code);
            $stmt->bindValue('file', '');
            $stmt->bindValue('line', 0);
            $stmt->bindValue('message', $msg);
            $stmt->bindValue('method', 'MH');
            $stmt->bindValue('path', $pos);
            $stmt->bindValue('sessionid', 0);
            $stmt->execute();
            //}
            //$conn->commit();
        }catch (Exception $e) {
            echo $e->getMessage();
        }
        return;
    }

    /**
     * @Route(path="/CaseMovement/getClientMovements", name="CaseMovement-getClientMovements", options={"expose"=true}, methods={"POST"})
     */
    public function getClientMovementsAction(Request $request, $_locale)
    {
        $parameters=json_decode($request->getContent());
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="SELECT carrier,code,stockcode,casename,starttime,targettime,erprefnumber,empcode,empname 
        from case_movements 
        where status='WAITFORDELIVERY' and client=:client
        order by starttime";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('client', $parameters->client);
        $stmt->execute();
        $records=$stmt->fetchAll();
        return new JsonResponse(array("totalProperty"=>count($records),"records"=>$records));
    }

}