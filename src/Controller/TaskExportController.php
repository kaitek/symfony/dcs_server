<?php

namespace App\Controller;

use App\Entity\TaskList;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class TaskExportController extends ControllerBase
{
    public const ENTITY = 'App:TaskList';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);

        $this->_queryType = self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/TaskExport/export", requirements={}, name="TaskExport-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale)
    {
        try {
            $uri = strtolower($request->getBaseUrl());
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $tasks = array();
            //if($uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149'){
            $sql_c = "SELECT c.code,c.workflow
                ,case when EXISTS(
                    SELECT *
                    from (SELECT cd.* 
                        from client_details cd
                        where cd.client=c.code and cd.iotype='I' and cd.finish is null) cd
                    join client_mould_groups cmg on cmg.client=cd.client 
                    join client_mould_details cmd on cd.client=cmd.client and cmg.code=cmd.clientmouldgroup
                    join mould_groups mg on mg.code=cmd.mouldgroup 
                    where /*cmg.isdefault=true and*/ cmd.client=c.code
                ) then true else false end cmg
            from clients c 
            where c.workflow in ('Sabit Üretim Aparatlı','Değişken Üretim Aparatlı','Üretim Aparatsız') 
            -- and c.code='1400T'
            order by c.code";
            //}else{
            //    $sql_c="SELECT c.code,c.workflow
            //        ,case when EXISTS(
            //            SELECT *
            //            from (SELECT cd.*
            //                from client_details cd
            //                where cd.client=c.code and cd.iotype='I' and cd.finish is null) cd
            //            join mould_details md on md.mould=cd.code
            //            join mould_groups mg on md.mouldgroup=mg.code and md.mould=mg.mould
            //            join client_mould_details cmd on mg.code=cmd.mouldgroup and cd.client=cmd.client
            //            join client_mould_groups cmg on cmg.client=cmd.client and cmg.code=cmd.clientmouldgroup
            //            where /*cmg.isdefault=true and*/ cmd.client=c.code
            //        ) then true else false end cmg
            //    from clients c
            //    where c.workflow in ('Sabit Üretim Aparatlı','Değişken Üretim Aparatlı','Üretim Aparatsız')
            //    -- and c.code='03.08.0021'
            //    order by c.code";
            //}
            $stmt_c = $conn->prepare($sql_c);
            $stmt_c->execute();
            $clients = $stmt_c->fetchAll();
            //çalışan işler alınıyor
            $sql_task_working = "SELECT tp.idx,tp.code,tp.client,tp.opcode,tp.opnumber,tp.opname,tp.erprefnumber,tp.productcount,tp.deadline,tp.tpp
                ,tp.clientmouldgroup,tp.mould,tp.mouldgroup,tp.operatorcount,tp.isdefault,tp.mask,tp.status,tp.setup,tp.cyclecount,tp.productdonecount
                ,tp.plannedstart,tp.plannedfinish,cpd.client,cpd.client cpdclient 
            from client_production_details cpd 
            join task_plans tp on tp.client=cpd.client and tp.opname=cpd.opname and tp.erprefnumber=cpd.erprefnumber
            join (SELECT id,code,beginval,endval,concat(current_date,' ',cast(current_time as varchar(8))) zaman,CURRENT_TIMESTAMP(0) ts
                    ,cast(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)) as date) \"day\" 
                    ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL '1 day' else current_date end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                    ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL '1 day' else current_date end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                    and (
                    (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)
                    or (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )
            )) sv on cpd.day=sv.day and cpd.jobrotation=sv.code
            where cpd.type='c_p' and cpd.finish is null
            group by tp.idx,tp.code,tp.client,tp.opcode,tp.opnumber,tp.opname,tp.erprefnumber,tp.productcount,tp.deadline,tp.tpp
                ,tp.clientmouldgroup,tp.mould,tp.mouldgroup,tp.operatorcount,tp.isdefault,tp.mask,tp.status,tp.setup,tp.cyclecount,tp.productdonecount
                ,tp.plannedstart,tp.plannedfinish,cpd.client ";
            $stmt_task_working = $conn->prepare($sql_task_working);
            $stmt_task_working->execute();
            $tasks_working = $stmt_task_working->fetchAll();

            //elle gönderim sağlanan işler alınıyor
            $sql_sendforce = "SELECT * from task_plans where issendbyforce=true";
            $stmt_sendforce = $conn->prepare($sql_sendforce);
            $stmt_sendforce->execute();
            $tasks_sendforce = $stmt_sendforce->fetchAll();

            $sql = "truncate table task_plans";
            //$sql="delete from task_plans where coalesce(idx,'')=''";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            //task_plans import kısmına alındı
            //$sql="truncate table task_plan_employees";
            //$stmt = $conn->prepare($sql);
            //$stmt->execute();

            foreach ($tasks_working as $working_task) {
                $sql = "insert into task_plans (idx,code,client,opcode,opnumber,opname,erprefnumber,productcount,deadline,tpp
                    ,clientmouldgroup,mould,mouldgroup,operatorcount,isdefault,mask,status,setup,cyclecount,productdonecount
                    ,plannedstart,plannedfinish) 
                    values (:idx1,:code,:client,:opcode,:opnumber,:opname,:erprefnumber,:productcount,:deadline,:tpp
                    ,:clientmouldgroup,:mould,:mouldgroup,:operatorcount,:isdefault,:mask,:status,:setup,:cyclecount,:productdonecount
                    ,:plannedstart,:plannedfinish)";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('idx1', $working_task["idx"]);
                $stmt->bindValue('code', $working_task["code"]);
                $stmt->bindValue('client', $working_task["cpdclient"]);
                $stmt->bindValue('opcode', $working_task["opcode"]);
                $stmt->bindValue('opnumber', $working_task["opnumber"]);
                $stmt->bindValue('opname', $working_task["opname"]);
                $stmt->bindValue('erprefnumber', $working_task["erprefnumber"]);
                $stmt->bindValue('productcount', $working_task["productcount"]);
                $stmt->bindValue('deadline', $working_task["deadline"]);
                $stmt->bindValue('tpp', $working_task["tpp"]);
                $stmt->bindValue('clientmouldgroup', $working_task["clientmouldgroup"]);
                $stmt->bindValue('mould', $working_task["mould"]);
                $stmt->bindValue('mouldgroup', $working_task["mouldgroup"]);
                $stmt->bindValue('operatorcount', $working_task["operatorcount"]);
                $stmt->bindValue('isdefault', $working_task["isdefault"] == "true" ? $working_task["isdefault"] : "false");
                $stmt->bindValue('mask', $working_task["mask"]);
                $stmt->bindValue('status', "Devam Eden");
                $stmt->bindValue('setup', $working_task["setup"]);
                $stmt->bindValue('cyclecount', $working_task["cyclecount"]);
                $stmt->bindValue('productdonecount', $working_task["productdonecount"]);
                $stmt->bindValue('plannedstart', $working_task["plannedstart"]);
                $stmt->bindValue('plannedfinish', $working_task["plannedfinish"]);
                $stmt->execute();
            }

            foreach ($clients as $client) {
                $tl = $this->getClientTaskLists($uri, $client["code"], $client["workflow"], $client["cmg"]);
                foreach ($tl as $row) {
                    if ($client["cmg"] === true) {
                        if (isset($row["mould"]) && $row["mould"] !== '') {
                            $tasks[] = $row;
                        }
                    } else {
                        $tasks[] = $row;
                    }
                }
            }
            //birleştirilmiş işler yazılıyor
            foreach ($tasks as $task) {
                //$sql_select="select * from task_plans where idx=:idx and code=:code";
                //$stmt_select = $conn->prepare($sql_select);
                //$stmt_select->bindValue('idx', $task["idx"]);
                //$stmt_select->bindValue('code', $task["code"]);
                //$stmt_select->execute();
                //$record_task_plan=$stmt_select->fetchAll();
                //if(count($record_task_plan)===0){
                $sql = "insert into task_plans (idx,code,client,opcode,opnumber,opname,erprefnumber,productcount,deadline,tpp
                    ,clientmouldgroup,mould,mouldgroup,operatorcount,isdefault,mask,status,setup,cyclecount,productdonecount) 
                    values (:idx1,:code,:client,:opcode,:opnumber,:opname,:erprefnumber,:productcount,:deadline,:tpp
                    ,:clientmouldgroup,:mould,:mouldgroup,:operatorcount,:isdefault,:mask,:status,:setup,:cyclecount,:productdonecount)";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('idx1', $task["idx"]);
                $stmt->bindValue('code', $task["code"]);
                $stmt->bindValue('client', $task["client"]);
                $stmt->bindValue('opcode', $task["opcode"]);
                $stmt->bindValue('opnumber', $task["opnumber"]);
                $stmt->bindValue('opname', $task["opname"]);
                $stmt->bindValue('erprefnumber', $task["erprefnumber"]);
                $stmt->bindValue('productcount', $task["productcount"]);
                $stmt->bindValue('deadline', $task["deadline"]);
                $stmt->bindValue('tpp', $task["tpp"]);
                $stmt->bindValue('clientmouldgroup', $task["clientmouldgroup"]);
                $stmt->bindValue('mould', $task["mould"]);
                $stmt->bindValue('mouldgroup', $task["mouldgroup"]);
                $stmt->bindValue('operatorcount', $task["operatorcount"]);
                $stmt->bindValue('isdefault', $task["isdefault"] == "true" ? $task["isdefault"] : "false");
                $stmt->bindValue('mask', $task["mask"]);
                $stmt->bindValue('status', $task["status"]);
                $stmt->bindValue('setup', $task["setup"]);
                $stmt->bindValue('cyclecount', $task["cyclecount"]);
                $stmt->bindValue('productdonecount', $task["productdonecount"]);
                $stmt->execute();
                //}else{
                //    $sql_u="update task_plans set status=:status where idx=:idx and code=:code";
                //    $stmt_u = $conn->prepare($sql_u);
                //    $stmt_u->bindValue('idx', $task["idx"]);
                //    $stmt_u->bindValue('code', $task["code"]);
                //    $stmt_u->bindValue('status', $task["status"]);
                //    $stmt_u->execute();
                //}
            }
            //elle gönderilenler güncelleniyor
            if (count($tasks_sendforce) > 0) {
                foreach ($tasks_sendforce as $task_sendforce) {
                    $sql_u = "update task_plans 
                    set idx=:idx2,issendbyforce=true 
                    where issendbyforce is null and code=:code and client=:client and opname=:opname
                        and coalesce(idx,'')=''
                    ";
                    $stmt_u = $conn->prepare($sql_u);
                    $stmt_u->bindValue('idx2', $task_sendforce["idx"]);
                    $stmt_u->bindValue('code', $task_sendforce["code"]);
                    $stmt_u->bindValue('client', $task_sendforce["client"]);
                    $stmt_u->bindValue('opname', $task_sendforce["opname"]);
                    $stmt_u->execute();
                }
            }

            //biten işlerin bilgileri yazılıyor
            $sql_tl = "select * 
            from (
                SELECT '-' idx,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber,tl.productcount
                    ,tl.productdonecount,tl.deadline,tl.tpp,'Tamamlanan' status,0 cyclecount,0 setup 
                from task_lists tl
                join (SELECT erprefnumber from task_plans where idx<>'' GROUP BY erprefnumber)tp on tl.erprefnumber=tp.erprefnumber 
                where tl.code not in (SELECT code from task_plans where idx<>'') and tl.productdonecount>=tl.productcount
                union ALL
                SELECT '-' idx,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber,tl.productcount
                    ,tl.productdonecount,tl.deadline,tl.tpp,'Tamamlanan' status,0 cyclecount,0 setup 
                from task_lists tl
                where tl.productdonecount>=tl.productcount and finish is not null and finish>CURRENT_TIMESTAMP(0)-interval '20 days'
            )tl
            GROUP BY tl.idx,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber,tl.productcount
                ,tl.productdonecount,tl.deadline,tl.tpp,tl.status,tl.cyclecount,tl.setup";
            $stmt_tl = $conn->prepare($sql_tl);
            $stmt_tl->execute();
            $tasks_tl = $stmt_tl->fetchAll();
            foreach ($tasks_tl as $task) {
                $sql = "insert into task_plans (idx,code,client,opcode,opnumber,opname,erprefnumber,productcount,deadline,tpp
                ,clientmouldgroup,mould,mouldgroup,operatorcount,isdefault,mask,status,setup,cyclecount,productdonecount) 
                values (:idx3,:code,:client,:opcode,:opnumber,:opname,:erprefnumber,:productcount,:deadline,:tpp
                ,:clientmouldgroup,:mould,:mouldgroup,:operatorcount,:isdefault,:mask,:status,:setup,:cyclecount,:productdonecount)";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('idx3', $task["idx"]);
                $stmt->bindValue('code', $task["code"]);
                $stmt->bindValue('client', $task["client"]);
                $stmt->bindValue('opcode', $task["opcode"]);
                $stmt->bindValue('opnumber', $task["opnumber"]);
                $stmt->bindValue('opname', $task["opname"]);
                $stmt->bindValue('erprefnumber', $task["erprefnumber"]);
                $stmt->bindValue('productcount', $task["productcount"]);
                $stmt->bindValue('deadline', $task["deadline"]);
                $stmt->bindValue('tpp', $task["tpp"]);
                $stmt->bindValue('clientmouldgroup', $task["clientmouldgroup"]);
                $stmt->bindValue('mould', $task["mould"]);
                $stmt->bindValue('mouldgroup', $task["mouldgroup"]);
                $stmt->bindValue('operatorcount', $task["operatorcount"]);
                $stmt->bindValue('isdefault', $task["isdefault"] == "true" ? $task["isdefault"] : "false");
                $stmt->bindValue('mask', $task["mask"]);
                $stmt->bindValue('status', $task["status"]);
                $stmt->bindValue('setup', $task["setup"]);
                $stmt->bindValue('cyclecount', $task["cyclecount"]);
                $stmt->bindValue('productdonecount', $task["productdonecount"]);
                $stmt->execute();
            }
            //birleşmemiş işlerin birim süreleri kalıp tablosundan alınıp yazılıyor
            $sql_tpp = "SELECT md.opname,mg.cycletime
            from mould_details md 
            join (SELECT tp.opname from task_plans tp where tp.idx='' GROUP BY tp.opname) tp on tp.opname=md.opname
            join mould_groups mg on mg.code=md.mouldgroup
            where md.mouldgroup in (
                SELECT md.mouldgroup
                from mould_details md
                GROUP BY md.mouldgroup
                having count(*)=1)
            ";
            $stmt_tpp = $conn->prepare($sql_tpp);
            $stmt_tpp->execute();
            $records_tpp = $stmt_tpp->fetchAll();
            foreach ($records_tpp as $record_tpp) {
                $sql_u = "update task_plans set tpp=:tpp where idx='' and opname=:opname";
                $stmt_u = $conn->prepare($sql_u);
                $stmt_u->bindValue('tpp', $record_tpp["cycletime"]);
                $stmt_u->bindValue('opname', $record_tpp["opname"]);
                $stmt_u->execute();
            }
            $sql_status = "update task_plans
            set status='R'
            where idx in (
                select idx 
                from (
                    SELECT idx,status
                    from task_plans 
                    where coalesce(idx,'') not in ('','-') and status in ('P','R')
                    GROUP BY idx,status)x
                GROUP BY x.idx
                having count(*)>1)";
            $stmt_status = $conn->prepare($sql_status);
            $stmt_status->execute();
            return new JsonResponse(array("totalProperty" => 1,"records" => $tasks));
        } catch (\Exception $e) {
            return $this->msgError($e->getMessage());
        }
    }

    private function getClientTaskLists($uri, $client, $workflow, $cmg)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        if ($cmg === true) {
            if ($workflow === 'Değişken Üretim Aparatlı'/*$uri=='/pres'&&$_SERVER['HTTP_HOST']=='172.16.1.149'*/) {
                $sql_cpc = "select * from (
                    SELECT cmd.client,cmd.clientmouldgroup,md.mould,cmd.mouldgroup,cmg.operatorcount
                        ,coalesce(cmg.isdefault,false)isdefault,md.opcode,md.opnumber,md.opname
                        ,coalesce(nullif(cast(md.leafmaskcurrent as int),'0'),'1')mask,cmg.setup
                        ,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime)/100,2)cycletime
                        ,(select count(*) from client_mould_details where client=cmd.client and clientmouldgroup=cmd.clientmouldgroup)forder
                    from (SELECT cd.* 
                        from client_details cd
                        where cd.client=:client and cd.iotype='I' and cd.finish is null) cd
                    join client_mould_groups cmg on cmg.client=cd.client 
                    join client_mould_details cmd on cd.client=cmd.client and cmg.code=cmd.clientmouldgroup
                    join mould_groups mg on mg.code=cmd.mouldgroup 
                    join mould_details md on md.mouldgroup=mg.code
                    where /*cmg.isdefault=true and*/ cmd.client=:client
                )aaa
                order by aaa.client asc,case when aaa.isdefault=true then 1 else 2 end asc
                    ,aaa.forder desc,aaa.clientmouldgroup asc,aaa.mould asc,aaa.mouldgroup asc";
            } else {
                $sql_cpc = "select * from (
                    SELECT cmd.client,cmd.clientmouldgroup,md.mould,cmd.mouldgroup,cmg.operatorcount
                        ,coalesce(cmg.isdefault,false)isdefault,md.opcode,md.opnumber,md.opname
                        ,coalesce(nullif(cast(md.leafmaskcurrent as int),'0'),'1')mask,cmg.setup
                        ,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime/(case when md.mould='03.23.0545' or md.mould='03.23.0546' then 2 else 1 end))/100,2)cycletime
                        ,(select count(*) from client_mould_details where client=cmd.client and clientmouldgroup=cmd.clientmouldgroup)forder
                    from (SELECT cd.* 
                        from client_details cd
                        where cd.client=:client and cd.iotype='I' and cd.finish is null) cd
                    join mould_details md on md.mould=cd.code 
                    join mould_groups mg on md.mouldgroup=mg.code and md.mould=mg.mould 
                    join client_mould_details cmd on mg.code=cmd.mouldgroup and cd.client=cmd.client
                    join client_mould_groups cmg on cmg.client=cmd.client and cmg.code=cmd.clientmouldgroup
                    where /*cmg.isdefault=true and*/ cmd.client=:client
                )aaa
                order by aaa.client asc,case when aaa.isdefault=true then 1 else 2 end asc
                    ,aaa.forder desc,aaa.clientmouldgroup asc,aaa.mould asc,aaa.mouldgroup asc";
            }

            $stmt_cpc = $conn->prepare($sql_cpc);
            $stmt_cpc->bindValue('client', $client);
            $stmt_cpc->execute();
            $cpc = $stmt_cpc->fetchAll();
        } else {
            if ($workflow !== 'Üretim Aparatsız') {
                $sql_cpc = "SELECT md.mould,md.mouldgroup,md.opcode,md.opnumber,md.opname
                    ,coalesce(nullif(cast(md.leafmaskcurrent as int),'0'),'1')mask,mg.setup
                    ,round((coalesce(mg.productionmultiplier,100)*mg.cycletime)/100,2)cycletime,1 operatorcount
                from (SELECT cd.* 
                    from client_details cd
                    where cd.client=:client and cd.iotype='I' and cd.ioevent='input' and cd.finish is null) cd
                join mould_details md on md.mould=cd.code 
                join mould_groups mg on md.mouldgroup=mg.code and md.mould=mg.mould 
                where cd.client=:client
                order by cd.client asc,(select count(*) from mould_details where mould=md.mould and mouldgroup=md.mouldgroup) desc,md.mould asc,md.mouldgroup asc";
                $stmt_cpc = $conn->prepare($sql_cpc);
                $stmt_cpc->bindValue('client', $client);
                $stmt_cpc->execute();
                $cpc = $stmt_cpc->fetchAll();
            }
        }
        if (($_SERVER['HTTP_HOST'] == '10.10.0.10'/*||$_SERVER['SSL_SERVER_S_DN_CN']=='192.168.1.40'*/)) {
            $sql_tl = "SELECT coalesce(tl0.idx,'') idx,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber,tl.productcount,coalesce(tl0.productdonecount,0)productdonecount,tl.deadline,tl.tpp
                ,case when cpd.client is not null then 'Devam Eden' 
                    when tl0.finish is not null then 'Tamamlanan' else '' end status
                ,0 cyclecount,0 setup
            from task_imports tl 
            left join task_lists tl0 on tl0.erprefnumber=tl.erprefnumber and tl0.client=tl.client and tl0.opname=concat(tl.opcode,'-',tl.opnumber) and tl0.code=tl.code
            left join task_plans tp on tp.code=tl.code
            left join (SELECT client,erprefnumber,opname 
                from client_production_details 
                where type in ('c_p','c_p_confirm') and finish is null and day>current_date - interval '3' day 
                group by client,erprefnumber,opname) cpd on cpd.client=tl.client and cpd.erprefnumber=tl.erprefnumber and cpd.opname=tl.opname
            where /*tl.erprefnumber in (select erprefnumber from task_lists where finish is null and taskfromerp=true and deadline>current_date-interval '20 day')
            and */coalesce(tl0.idx,'')='' and coalesce(tp.idx,'')='' and tl.client=:client
            order by tl.deadline,tl.erprefnumber,tl.opname";
        } else {
            //    $sql_tl="SELECT '' idx,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber,tl.productcount,tl.productdonecount,tl.deadline,tl.tpp
            //        ,case when cpd.client is not null then 'Devam Eden'
            //            when tl.finish is not null then 'Tamamlanan' else '' end status
            //        ,0 cyclecount,0 setup
            //    from task_lists tl
            //    left join (SELECT client,erprefnumber,opname
            //        from client_production_details
            //        where type in ('c_p','c_p_confirm') and finish is null and day>current_date - interval '3' day
            //        group by client,erprefnumber,opname) cpd on cpd.client=tl.client and cpd.erprefnumber=tl.erprefnumber and cpd.opname=tl.opname
            //    where tl.erprefnumber in (select erprefnumber from task_lists where finish is null and taskfromerp=true and deadline>current_date-interval '20 day')
            //    and tl.client=:client
            //    order by tl.deadline,tl.erprefnumber,tl.opname";
            /*and tl0.code=tl.code*/ //kısmı planda gruplu işler tek seçilsin açıldıktan sonra açılacak
            //$sql_tl="SELECT case when cpd.client is not null then coalesce(tl0.idx,'') else '' end idx
            //    ,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber
            //    ,tl.productcount,coalesce(tl0.productdonecount,0)productdonecount,tl.deadline,tl.tpp
            //    ,case when cpd.client is not null then 'Devam Eden'
            //        when tl0.finish is not null then 'Tamamlanan' else tl.status end status
            //    ,0 cyclecount,0 setup
            //from task_imports tl
            //left join task_lists tl0 on tl0.erprefnumber=tl.erprefnumber and tl0.client=tl.client and tl0.opname=concat(tl.opcode,'-',tl.opnumber) and tl0.code=tl.code
            //left join task_plans tp on tp.erprefnumber=tl.erprefnumber and tp.opname=tl.opname and tp.client=tl.client
            //left join (SELECT client,erprefnumber,opname
            //    from client_production_details
            //    where type in ('c_p','c_p_confirm') and finish is null and day>current_date - interval '3' day
            //    group by client,erprefnumber,opname) cpd on cpd.client=tl.client and cpd.erprefnumber=tl.erprefnumber and cpd.opname=tl.opname
            //where /*tl.erprefnumber in (select erprefnumber from task_lists where finish is null and taskfromerp=true and deadline>current_date-interval '20 day')
            //and coalesce(tl0.idx,'')='' and coalesce(tp.idx,'')='' and*/ tp.id is null and tl.client=:client
            //order by tl.deadline,tl.erprefnumber,tl.opname";
            $sql_tl = "SELECT case when cpd.client is not null then coalesce(tl0.idx,'') else '' end idx
                ,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber
                ,tl.productcount,coalesce(tl0.productdonecount,0)productdonecount,tl.deadline,tl.tpp
                ,case when cpd.client is not null then 'Devam Eden' 
                    when tl0.finish is not null then 'Tamamlanan' else tl.status end status
                ,0 cyclecount,0 setup
            from task_imports tl 
            left join task_lists tl0 on tl0.erprefnumber=tl.erprefnumber and tl0.opname=concat(tl.opcode,'-',tl.opnumber) and tl0.code=tl.code
            left join task_plans tp on tp.erprefnumber=tl.erprefnumber and tp.opname=tl.opname and tp.client=tl.client 
            left join (SELECT client,erprefnumber,opname 
                from client_production_details 
                where type in ('c_p','c_p_confirm') and finish is null and day>current_date - interval '3' day 
                group by client,erprefnumber,opname) cpd on cpd.client=tl.client and cpd.erprefnumber=tl.erprefnumber and cpd.opname=tl.opname
            where /*tl.erprefnumber in (select erprefnumber from task_lists where finish is null and taskfromerp=true and deadline>current_date-interval '20 day')
            and coalesce(tl0.idx,'')='' and coalesce(tp.idx,'')='' and*/ tp.id is null and tl.client=:client
            order by tl.deadline,tl.erprefnumber,tl.opname";
        }
        //if($_SERVER['SSL_SERVER_S_DN_CN']=='192.168.1.40'){
        //    $cpc=$content;
        //}
        $stmt_tl = $conn->prepare($sql_tl);
        $stmt_tl->bindValue('client', $client);
        $stmt_tl->execute();
        $tasks = $stmt_tl->fetchAll();
        if ($workflow === 'Üretim Aparatsız') {
            $i = 0;
            foreach ($tasks as $task) {
                $tasks[$i]["idx"] = $this->gen_uuid();
                $tasks[$i]["operatorcount"] = 1;
                $tasks[$i]["mask"] = 1;
                $i++;
            }
            return $tasks;
        }
        $proccessed_task = $tasks;

        $arr_idx = array();
        foreach ($tasks as $task) {
            if ($task["idx"] === '') {
                $arr_cmg = array();
                $mg = '';
                foreach ($cpc as $cap) {
                    if ($cap["opname"] === $task["opname"]) {
                        if ($cmg === true) {
                            $mg = $cap["clientmouldgroup"];
                        } else {
                            $mg = $cap["mouldgroup"];
                        }
                        $task["cyclecount"] = ceil(intval($task["productcount"]) / intval($cap["mask"]));
                        //break;//eskiden bulduğu gibi çıkması için eklenmişti
                        //2020-01-20 olabilecek tüm varyasyonlar kontrol edilecek
                        if (!in_array($mg, $arr_cmg)) {
                            $arr_cmg[] = $mg;
                            $mg = '';
                        }
                    }
                }
                if (count($arr_cmg) > 0) {
                    foreach ($arr_cmg as $mg) {
                        $tmp_cpc = array();
                        $idx = $this->gen_uuid();
                        foreach ($cpc as $cap) {
                            if ($cmg === true) {
                                if ($cap["clientmouldgroup"] === $mg) {
                                    $tmp_cpc[] = $cap;
                                }
                            } else {
                                if ($cap["mouldgroup"] === $mg) {
                                    $tmp_cpc[] = $cap;
                                }
                            }
                        }
                        foreach ($tmp_cpc as $row_cpc) {
                            foreach ($proccessed_task as $task_p) {
                                if ($task_p["idx"] === '' && $task_p["opname"] === $row_cpc["opname"]
                                    // &&($task["deadline"]===$task_p["deadline"]) //Salih emek talebi ile tarih kısıtı kaldırıldı
                                ) {
                                    $cyclecount = ceil(intval($task_p["productcount"]) / intval($row_cpc["mask"]));
                                    //2022-02-16 pres tarafı için miktar kısıtı kaldırıldı @SalihEmek
                                    if ($task["cyclecount"] === $cyclecount || ($uri === '/pres' && $_SERVER['HTTP_HOST'] === '172.16.1.149')) {
                                        $arr_idx[] = array("id" => $task_p["id"],"code" => $task_p["code"],"cyclecount" => $cyclecount,"cycletime" => $row_cpc["cycletime"],"idx" => $idx,"cpc" => $row_cpc);
                                        break;
                                    }
                                }
                            }
                        }
                        if (count($arr_idx) === 0 || count($arr_idx) !== (count($tmp_cpc))) {
                            $arr_idx = array();
                        } else {
                            while (count($arr_idx) > 0) {
                                $row = array_shift($arr_idx);
                                $i = 0;
                                foreach ($proccessed_task as $pt) {
                                    if ($pt["idx"] === '' && $pt["code"] === $row["code"]) {
                                        $proccessed_task[$i]["idx"] = $row["idx"];
                                        if ($cmg === true) {
                                            $proccessed_task[$i]["clientmouldgroup"] = $row["cpc"]["clientmouldgroup"];
                                        } else {
                                            $proccessed_task[$i]["clientmouldgroup"] = '';
                                        }
                                        $proccessed_task[$i]["mould"] = $row["cpc"]["mould"];
                                        $proccessed_task[$i]["mouldgroup"] = $row["cpc"]["mouldgroup"];
                                        $proccessed_task[$i]["operatorcount"] = $row["cpc"]["operatorcount"];
                                        $proccessed_task[$i]["isdefault"] = $row["cpc"]["isdefault"];
                                        $proccessed_task[$i]["setup"] = $row["cpc"]["setup"];
                                        $proccessed_task[$i]["mask"] = $row["cpc"]["mask"];
                                        $proccessed_task[$i]["cyclecount"] = $row["cyclecount"];
                                        $proccessed_task[$i]["tpp"] = $row["cycletime"];
                                        break;
                                    }
                                    $i++;
                                }
                            }
                            $arr_cmg = array();
                            break;
                        }
                    }
                }
            }
        }
        return $proccessed_task;
    }

    /**
     * @ Route(path="/TaskExport", name="TaskExport-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0, //$this instanceof BaseAuditControllerInterface,
                'data' => array(),//array('clients'=>json_decode($clients->getContent())->records),
                'extras' => array()
            );
            return $this->render('Modules/TaskExport.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @ Route(path="/TaskExport/all/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="TaskExport-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/m2m/TaskExportData", name="TaskExportData-import", options={"expose"=true}, methods={"PUT"})
     */
    public function putImportDataAction(Request $request, $_locale)
    {
        $content = json_decode($request->getContent());
        $tmp = array();
        foreach ($content as $row) {
            $tmp[] = array("client" => $row->client
            ,"clientmouldgroup" => $row->clientmouldgroup
            ,"mould" => $row->mould
            ,"mouldgroup" => $row->mouldgroup
            ,"operatorcount" => $row->operatorcount
            ,"isdefault" => $row->isdefault
            ,"opcode" => $row->opcode
            ,"opnumber" => $row->opnumber
            ,"opname" => $row->opname
            ,"mask" => $row->mask
            ,"setup" => $row->setup
            ,"cycletime" => $row->cycletime
            ,"forder" => $row->forder
            );
        }
        $tasks = array();
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $tl = $this->getClientTaskLists('K425', 'Sabit Üretim Aparatlı', true, $tmp);
        foreach ($tl as $row) {
            $tasks[] = $row;
        }
        $sql = "truncate table task_plans";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        foreach ($tasks as $task) {
            $sql = "insert into task_plans (idx,code,client,opcode,opnumber,opname,erprefnumber,productcount,deadline,tpp
            ,clientmouldgroup,mould,mouldgroup,operatorcount,isdefault,mask,status,setup,cyclecount,productdonecount) 
            values (:idx4,:code,:client,:opcode,:opnumber,:opname,:erprefnumber,:productcount,:deadline,:tpp
            ,:clientmouldgroup,:mould,:mouldgroup,:operatorcount,:isdefault,:mask,:status,:setup,:cyclecount,:productdonecount)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('idx4', $task["idx"]);
            $stmt->bindValue('code', $task["code"]);
            $stmt->bindValue('client', $task["client"]);
            $stmt->bindValue('opcode', $task["opcode"]);
            $stmt->bindValue('opnumber', $task["opnumber"]);
            $stmt->bindValue('opname', $task["opname"]);
            $stmt->bindValue('erprefnumber', $task["erprefnumber"]);
            $stmt->bindValue('productcount', $task["productcount"]);
            $stmt->bindValue('deadline', $task["deadline"]);
            $stmt->bindValue('tpp', $task["tpp"]);
            $stmt->bindValue('clientmouldgroup', $task["clientmouldgroup"]);
            $stmt->bindValue('mould', $task["mould"]);
            $stmt->bindValue('mouldgroup', $task["mouldgroup"]);
            $stmt->bindValue('operatorcount', $task["operatorcount"]);
            $stmt->bindValue('isdefault', $task["isdefault"] == "true" ? $task["isdefault"] : "false");
            $stmt->bindValue('mask', $task["mask"]);
            $stmt->bindValue('status', $task["status"]);
            $stmt->bindValue('setup', $task["setup"]);
            $stmt->bindValue('cyclecount', $task["cyclecount"]);
            $stmt->bindValue('productdonecount', $task["productdonecount"]);
            $stmt->execute();
        }
        $sql_tl = "SELECT '-' idx,tl.code,tl.client,tl.opcode,tl.opnumber,tl.opname,tl.erprefnumber,tl.productcount,tl.productdonecount,tl.deadline,tl.tpp
            ,'Tamamlanan' status
            ,0 cyclecount,0 setup 
        from task_lists tl
        join (SELECT erprefnumber from task_plans where idx<>'' GROUP BY erprefnumber)tp on tl.erprefnumber=tp.erprefnumber 
        where tl.code not in (SELECT code from task_plans where idx<>'')";
        $stmt_tl = $conn->prepare($sql_tl);
        $stmt_tl->execute();
        $tasks_tl = $stmt_tl->fetchAll();
        foreach ($tasks_tl as $task) {
            $sql = "insert into task_plans (idx,code,client,opcode,opnumber,opname,erprefnumber,productcount,deadline,tpp
            ,clientmouldgroup,mould,mouldgroup,operatorcount,isdefault,mask,status,setup,cyclecount,productdonecount) 
            values (:idx5,:code,:client,:opcode,:opnumber,:opname,:erprefnumber,:productcount,:deadline,:tpp
            ,:clientmouldgroup,:mould,:mouldgroup,:operatorcount,:isdefault,:mask,:status,:setup,:cyclecount,:productdonecount)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('idx5', $task["idx"]);
            $stmt->bindValue('code', $task["code"]);
            $stmt->bindValue('client', $task["client"]);
            $stmt->bindValue('opcode', $task["opcode"]);
            $stmt->bindValue('opnumber', $task["opnumber"]);
            $stmt->bindValue('opname', $task["opname"]);
            $stmt->bindValue('erprefnumber', $task["erprefnumber"]);
            $stmt->bindValue('productcount', $task["productcount"]);
            $stmt->bindValue('deadline', $task["deadline"]);
            $stmt->bindValue('tpp', $task["tpp"]);
            $stmt->bindValue('clientmouldgroup', $task["clientmouldgroup"]);
            $stmt->bindValue('mould', $task["mould"]);
            $stmt->bindValue('mouldgroup', $task["mouldgroup"]);
            $stmt->bindValue('operatorcount', $task["operatorcount"]);
            $stmt->bindValue('isdefault', $task["isdefault"] == "true" ? $task["isdefault"] : "false");
            $stmt->bindValue('mask', $task["mask"]);
            $stmt->bindValue('status', $task["status"]);
            $stmt->bindValue('setup', $task["setup"]);
            $stmt->bindValue('cyclecount', $task["cyclecount"]);
            $stmt->bindValue('productdonecount', $task["productdonecount"]);
            $stmt->execute();
        }
        return new JsonResponse($tasks);
    }
}
