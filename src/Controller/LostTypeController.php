<?php
namespace App\Controller;

use App\Entity\LostType;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class LostTypeController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    CONST ENTITY = 'App:LostType';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
    }

    /**
     * @Route(path="/LostType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="LostType-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
       $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
            if($entity->getCreateUserId()===0&&($this->_container==null?$this->container:$this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')!==true){
                return $this->msgError(
                    ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
                    401
                );
            }
        return $this->recordDelete($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/LostType/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="LostType-getComboValues", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesLostType(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val='', $where = ''){
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val," and finish is null ");
    }

    public function getNewEntity() {
        return new LostType();
    }

    public function getQBQuery() {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('lt.id,lt.code,lt.description'
                //. ',lt.isemployeerequired,lt.isexpertemployeerequired,lt.isoperationnumberrequired'
                . ',lt.time,lt.closenoemployee,lt.isopenallports'
                . ',lt.ioevent,lt.islisted,lt.isclient'
                . ',lt.start,lt.finish,lt.listorder,lt.isemployee,lt.isshortlost,lt.issourcelookup'
                . ',lt.authorizeloststart, lt.authorizelostfinish, lt.isoperationnumberrequired'
                . ',lt.extcode,lt.version,lt.isdescriptionrequired')
                ->from('App:LostType', 'lt')
                ->where('lt.deleteuserId is null')
                //->orderBy('lt.listorder', 'ASC')
                ->addOrderBy('lt.code', 'ASC');
        $queries['LostType'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }

    /**
     * @Route(path="/LostType/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="LostType-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->recordAdd($request, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/LostType/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="LostType-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);
        if($entity->getCreateUserId()===0&&($this->_container==null?$this->container:$this->_container)->get('security.authorization_checker')->isGranted('ROLE_ADMIN')!==true){
            return $this->msgError(
                ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('err.main.process_authorize', array(), 'KaitekFrameworkBundle'),
                401
            );
        }
        return $this->recordEdit($request, $entity, $id, $v, $_locale, $pg, $lm);
    }

    /**
     * @Route(path="/LostType", name="LostType-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);
            $io_events = $this->getComboValues($request, $_locale, 1, 100, 'io_events','code','code','',' and iotype=\'O\' ');
            $data['extras']['io_events']=json_decode($io_events->getContent())->records;
            return $this->render('Modules/LostType.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/LostType/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="LostType-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField) {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'LostType', $id);

            return $this->render('Modules/LostType.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/LostType/{id}", requirements={"id": "\d+"}, name="LostType-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'LostType', $id);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/LostType/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="LostType-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

}