<?php

namespace App\Controller;

use App\Entity\Document;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BasePagingControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentController extends ControllerBase implements BasePagingControllerInterface, BaseAuditControllerInterface
{
    public const ENTITY = 'App:Document';

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    /**
     * @Route(path="/Document/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Document-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        $entity = $this->getDoctrine()
            ->getRepository(self::ENTITY)
            ->find($id);

        $cbd = $this->checkBeforeDelete($request, $id, $entity, $v);
        if ($cbd === true) {
            $tmpType=$entity->getType();
            $tmpOpcode=$entity->getOpcode();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user = $this->getUser();
            $userId = $user->getId();
            $entities = $this->getDoctrine()
                ->getRepository(self::ENTITY)
                ->findBy(array("type"=>$entity->getType(),"opcode"=>$entity->getOpcode()));
            foreach ($entities as $entity) {
                $entity->setDeleteuserId($userId);
                $em->persist($entity);
                $em->flush();
                $em->remove($entity);
                $em->flush();
            }
            $dir_documents=$this->getParameter('documents_base_directory');
            if ($tmpType!==''&&$tmpOpcode!=='') {
                $this->delete_files($dir_documents.$tmpType."/".$tmpOpcode);
            }
            if (method_exists($this, 'showAllAction') && $request->attributes->get('_isDCSService') !== true) {
                return $this->showAllAction($request, $_locale, $pg, $lm);
            } else {
                return $this->msgSuccess();
            }
        } else {
            return $cbd;
        }
    }

    /**
     * @Route(path="/Document/deleteChild", name="Document-child-del", options={"expose"=true}, methods={"DELETE"})
     */
    public function deleteChildAction(Request $request, $_locale)
    {
        $content=json_decode($request->getContent());
        $_data=json_decode($content->data);
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        if ($content->item==='groups') {
            if (isset($_data->type)&&isset($_data->opcode)&&isset($_data->opname)) {
                $user = $this->getUser();
                $userId = $user->getId();
                $entities = $this->getDoctrine()
                    ->getRepository(self::ENTITY)
                    ->findBy(array("opname"=>$_data->opname));
                foreach ($entities as $entity) {
                    $entity->setDeleteuserId($userId);
                    $em->persist($entity);
                    $em->flush();
                    $em->remove($entity);
                    $em->flush();
                }
                $dir_documents=$this->getParameter('documents_base_directory');
                if ($_data->type!==''&&$_data->opcode!==''&&$_data->opname!=='') {
                    $this->delete_files($dir_documents.$_data->type."/".$_data->opcode."/".$_data->opname."/");
                }
            }
        } else {
            //details
            if (isset($_data->id)&&isset($_data->path)) {
                $entity = $this->getDoctrine()
                    ->getRepository(self::ENTITY)
                    ->find($_data->id);
                $user = $this->getUser();
                $userId = $user->getId();
                $entity->setDeleteuserId($userId);
                $em->persist($entity);
                $em->flush();
                $em->remove($entity);
                $em->flush();
                $dir_documents=$this->getParameter('documents_base_directory');
                $this->delete_files($dir_documents.$_data->path);
            }
        }
        return $this->msgSuccess();
    }

    public function getNewEntity()
    {
        return new Document();
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('d.id,d.type,d.opcode'
            .',d.version')
            ->from('App:Document', 'd')
            ->where('d.deleteuserId is null')
            ->orderBy('d.opcode', 'ASC');
        $queries['Document'] = array('qb' => $qb, 'getAll' => true);

        return $queries;
    }
    public function getSqlStr()
    {
        $queries = array();
        $_sql = "SELECT min(Document.id)id,Document.type,Document.opcode,min(Document.version) \"version\" 
        FROM documents Document 
        WHERE 1=1 @@where@@  
        GROUP  BY Document.type,Document.opcode 
        order by Document.type,Document.opcode";
        $queries['Document'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/Document/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Document-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        return $this->msgSuccess();
    }

    /**
     * @Route(path="/Document/{pg}/{lm}/{id}/{v}", requirements={"pg": "\d+","lm": "\d+", "id": "\d+","v": "\d+"}, name="Document-update", options={"expose"=true}, methods={"PUT"})
     */
    public function putAction(Request $request, $_locale, $pg, $lm, $id, $v)
    {
        return $this->msgSuccess();
    }

    /**
     * @Route(path="/Document", name="Document-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $data = $this->getBackendData($request, $_locale, self::ENTITY);

            return $this->render('Modules/Document.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Document/edit/{id}/{focusField}", requirements={"id": "\d+"}, defaults={"focusField" = false}, name="Document-open-record", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModuleWithRecord(Request $request, $_locale, $id, $focusField)
    {
        $cbg = $this->checkBeforeGet($request);
        //$cbg=true;
        if ($cbg === true) {
            $data = $this->getBackendDataById($request, $_locale, self::ENTITY, 'Document', $id);

            return $this->render('Modules/Document.html.twig', $data);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Document/{id}", requirements={"id": "\d+"}, name="Document-show", options={"expose"=true}, methods={"GET"})
     */
    public function showAction(Request $request, $_locale, $id)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getRecordById($this, $request, 'Document', $id);
            if ($records["Document"]["totalProperty"]>0) {
                $records["Document"]["records"][0]["_groups"]=[];
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();
                $sql="SELECT type,opcode,opname 
                from documents 
                where type=:type and opcode=:opcode 
                GROUP BY type,opcode,opname 
                order by type,opcode,opname";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('type', $records["Document"]["records"][0]["type"]);
                $stmt->bindValue('opcode', $records["Document"]["records"][0]["opcode"]);
                $stmt->execute();
                $groups = $stmt->fetchAll();
                foreach ($groups as $group) {
                    $sql_d="SELECT * 
                    from documents 
                    where type=:type and opname=:opname 
                    order by type,opname,path";
                    $stmt_d = $conn->prepare($sql_d);
                    $stmt_d->bindValue('type', $group["type"]);
                    $stmt_d->bindValue('opname', $group["opname"]);
                    $stmt_d->execute();
                    $details = $stmt_d->fetchAll();
                    $group['_details']=$details;
                    $records["Document"]["records"][0]["_groups"][]=$group;
                }
            }
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Document/{type}/{opcode}", name="Document-load", options={"expose"=true}, methods={"POST"})
     */
    public function getDocumentAction(Request $request, $_locale, $type, $opcode)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $sql="select min(id)id from documents where type=:type and opcode=:opcode";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('type', $type);
            $stmt->bindValue('opcode', $opcode);
            $stmt->execute();
            $records = $stmt->fetchAll();
            if (count($records)>0&&$records[0]["id"]!==null) {
                return $this->showAction($request, $_locale, $records[0]["id"]);
            }
            return new JsonResponse(array("totalProperty"=>0,"record"=>array()), 200);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Document/all/{pg}/{lm}", defaults={"pg": 1, "lm": 10}, requirements={"pg": "\d+","lm": "\d+"}, name="Document-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/Document/upload", name="Document-upload", options={"expose"=true}, methods={"POST"})
     */
    public function uploadAction(Request $request, $_locale)
    {
        $arr_files = array();
        $formData = $request->get("formData");
        if ($formData!=="") {
            $formData=json_decode($formData);
        }
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $dir_documents=$this->getParameter('documents_base_directory');
        if (!is_dir($dir_documents.$formData->type."/".$formData->opcode)) {
            mkdir($dir_documents.$formData->type."/".$formData->opcode, 0777, true);
        }
        if (!is_dir($dir_documents.$formData->type."/".$formData->opcode."/".$formData->opname)) {
            mkdir($dir_documents.$formData->type."/".$formData->opcode."/".$formData->opname, 0777, true);
        }
        $user = $this->getUser();
        $userId = $user->getId();
        if ($formData->opname!==$formData->opcode) {
            $arr_opname=explode('-', $formData->opname);
            if (count($arr_opname)>1) {
                $formData->opnumber=array_pop($arr_opname);
            }
        }
        if (!isset($formData->lastupdate)||$formData->lastupdate===''||$formData->lastupdate===null) {
            $formData->lastupdate=date("Y-m-d H:i:s");
        }
        if (!isset($formData->currentversion)||$formData->currentversion===''||$formData->currentversion===null) {
            $formData->currentversion=1;
        }
        if (!isset($formData->description)||$formData->description===''||$formData->description===null) {
            $formData->description='';
        }

        $file = $request->files->get('file1');
        if (empty($file)) {
            if (!isset($formData->id)) {
                return $this->msgError("Yüklenecek dosya seçilmemiş");
            }
        } else {
            $file=$file[0];
            $filename = $file->getClientOriginalName();

            $path_destination=$dir_documents.$formData->type."/".$formData->opcode."/".$formData->opname."/";
            $formData->path=$formData->type."/".$formData->opcode."/".$formData->opname."/".$filename;
            $file->move($path_destination, $filename);
        }
        $conn->beginTransaction();
        try {
            //güncelleme yapılıyor ise, eski kaydı bul, resim dosyasını sil, tablodaki kaydı güncelle
            if (isset($formData->id)&&$formData->id!=='') {
                $repo = $this->getDoctrine()->getRepository(self::ENTITY);
                $entity=$repo->findOneBy(array('id'=>$formData->id));
                if ($entity!==null) {
                    $prefile=$entity->getPath();
                    if ($prefile!==null&&$formData->path!==$prefile&&$formData->path!=='_isFile') {
                        unlink($dir_documents.$prefile);
                    }
                }
                if (!empty($file)) {
                    $entity->setPath($formData->path);
                } else {
                    $formData->path=$entity->getPath();
                }
                $entity->setUpdateuserId($userId);
                $entity->setCurrentversion($formData->currentversion);
                $entity->setDescription($formData->description);
                $entity->setIslot($this->is_true($formData->islot));
                $entity->setLastupdate(new \DateTime($formData->lastupdate));
                $em->persist($entity);
                $em->flush();
                $em->clear();
            } else {
                //yeni kayıt ise doğrudan kayıt aç
                $entity = new Document();
                $entity->setCreateuserId($userId);
                //$entity->setUpdateuserId($userId);
                $entity->setType($formData->type);
                $entity->setPath($formData->path);
                $entity->setOpcode($formData->opcode);
                $entity->setOpnumber($formData->opnumber);
                $entity->setOpname($formData->opname);
                $entity->setCurrentversion($formData->currentversion);
                $entity->setDescription($formData->description);
                $entity->setIslot($this->is_true($formData->islot));
                $entity->setLastupdate(new \DateTime($formData->lastupdate));
                $em->persist($entity);
                $em->flush();
                $formData->id = $entity->getId();
                $em->clear();
            }
            $conn->commit();
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            $conn->rollback();
            //throw $e;
            return $this->msgError($e->getMessage());
        }
        return new JsonResponse($formData, 200);
    }

    /**
     * @Route(path="/m2m/Document/upload", name="Document-upload-m2m", options={"expose"=true}, methods={"POST"})
     */
    public function uploadFileAction(Request $request, $_locale)
    {
        //markalama ekran görüntüsü upload
        $arr_files = array();
        $formData = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $dir_documents=$this->getParameter('documents_base_directory');
        if (!is_dir($dir_documents.$formData['fileKey']."/".$formData['client'])) {
            mkdir($dir_documents.$formData['fileKey']."/".$formData['client'], 0777, true);
        }
        $file = $request->files->get('file');
        if (empty($file)) {
            if (!isset($formData->id)) {
                return $this->msgError("Yüklenecek dosya seçilmemiş");
            }
        } else {
            //$file=$file[0];
            $filename = $file->getClientOriginalName();

            $path_destination=$dir_documents.$formData['fileKey']."/".$formData['client']."/";
            //$formData->path=$formData['fileKey']."/".$formData['client']."/".$filename;
            $file->move($path_destination, $filename);
        }
        return $this->msgSuccess();
    }

    /**
     * @Route(path="/Document/readFolder", name="Document-readFolder", options={"expose"=true}, methods={"GET"})
     */
    public function readFolder(Request $request, $_locale)
    {
        return;
        $dir_old=$this->getParameter('documents_old_directory');
        $dir_documents=$this->getParameter('documents_base_directory');
        $hideName = array('.','..','.DS_Store','Thumbs.db','index.php');
        $arr_files = array();
        $opcode='';
        $opnumber=0;
        $opname='';
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $conn->beginTransaction();
        try {
            $folders = scandir($dir_old);
            foreach ($folders as $foldername) {
                if (!in_array($foldername, $hideName)) {
                    if ($foldername!=='docs') {
                        $opcode=$foldername;
                        if (!is_dir($dir_documents."SOP/".$opcode)) {
                            mkdir($dir_documents."SOP/".$opcode, 0777, true);
                        }
                        $folders_l1 = scandir($dir_old.$opcode);
                        foreach ($folders_l1 as $opname) {
                            if (!in_array($opname, $hideName)) {
                                if (!is_dir($dir_documents."SOP/".$opcode."/".$opname)) {
                                    mkdir($dir_documents."SOP/".$opcode."/".$opname, 0777, true);
                                }
                                if ($opname!==$opcode) {
                                    $arr_opname=explode('-', $opname);
                                    if (count($arr_opname)>1) {
                                        $opnumber=array_pop($arr_opname);
                                    }
                                }
                                $files = scandir($dir_old.$opcode.'/'.$opname);
                                foreach ($files as $filename) {
                                    if (!in_array($filename, $hideName)) {
                                        $path="SOP/".$opcode."/".$opname."/".$filename;
                                        $sql="SELECT * from documents where opname=:opname and path=:path";
                                        $stmt = $conn->prepare($sql);
                                        $stmt->bindValue('opname', $opname);
                                        $stmt->bindValue('path', $path);
                                        $stmt->execute();
                                        $records = $stmt->fetchAll();
                                        if (count($records)==0) {
                                            copy($dir_old.$opcode."/".$opname."/".$filename, $dir_documents.$path);
                                            $sql_i="insert into documents (type,path,opcode,opnumber,opname,currentversion,description,islot,create_user_id,created_at,update_user_id,updated_at,lastupdate) 
                                            values (:type,:path,:opcode,:opnumber,:opname,:currentversion,'',false,2,now(),2,now(),now())";
                                            $stmt_i = $conn->prepare($sql_i);
                                            $stmt_i->bindValue('type', 'SOP');
                                            $stmt_i->bindValue('path', $path);
                                            $stmt_i->bindValue('opcode', $opcode);
                                            $stmt_i->bindValue('opnumber', $opnumber);
                                            $stmt_i->bindValue('opname', $opname);
                                            $stmt_i->bindValue('currentversion', 1);
                                            $stmt_i->execute();
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (is_dir($dir_old."docs/MSDS")) {
                            $folders_l1 = scandir($dir_old.'docs/MSDS');
                            foreach ($folders_l1 as $foldername) {
                                if (!in_array($foldername, $hideName)) {
                                    $opcode=$foldername;
                                    $opname=$foldername;
                                    if (!is_dir($dir_documents."MSDS/".$foldername)) {
                                        mkdir($dir_documents."MSDS/".$foldername, 0777, true);
                                    }
                                    $files = scandir($dir_old."docs/MSDS/".$foldername);
                                    foreach ($files as $filename) {
                                        if (!in_array($filename, $hideName)) {
                                            $path="MSDS/".$foldername."/".$filename;
                                            $sql="SELECT * from documents where opname=:opname and path=:path";
                                            $stmt = $conn->prepare($sql);
                                            $stmt->bindValue('opname', $opname);
                                            $stmt->bindValue('path', $path);
                                            $stmt->execute();
                                            $records = $stmt->fetchAll();
                                            if (count($records)==0) {
                                                copy($dir_old."docs/MSDS/".$foldername."/".$filename, $dir_documents.$path);
                                                $sql_i="insert into documents (type,path,opcode,opnumber,opname,currentversion,description,islot,create_user_id,created_at,update_user_id,updated_at,lastupdate) 
                                                values (:type,:path,:opcode,:opnumber,:opname,:currentversion,'',false,2,now(),2,now(),now())";
                                                $stmt_i = $conn->prepare($sql_i);
                                                $stmt_i->bindValue('type', 'MSDS');
                                                $stmt_i->bindValue('path', $path);
                                                $stmt_i->bindValue('opcode', $opcode);
                                                $stmt_i->bindValue('opnumber', $opnumber);
                                                $stmt_i->bindValue('opname', $opname);
                                                $stmt_i->bindValue('currentversion', 1);
                                                $stmt_i->execute();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $em->flush();
            $em->clear();
            $conn->commit();
        } catch (\Exception $e) {
            // Rollback the failed transaction attempt
            $conn->rollback();
            $resp['success']=false;
            $resp['message']=$e->getMessage();
            $resp['arr_err']=json_encode($_arr_err);
            $statusCode=400;
        }

        return new JsonResponse($arr_files, 200);
    }
}
