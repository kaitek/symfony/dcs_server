<?php

namespace App\Controller;

use App\Entity\ActivityControlQuestion;
use App\Entity\ControlQuestion;
use App\Entity\Document;
use App\Entity\Employee;
use App\Entity\JobRotationTeam;
use App\Entity\KanbanOperation;
use App\Entity\Mould;
use App\Entity\MouldDetail;
use App\Entity\MouldGroup;
use App\Entity\Penetration;
use App\Entity\ProductTree;
use App\Entity\RejectType;
use App\Entity\Stock;
use App\Entity\TaskCaseControl;
use App\Entity\TaskImport;
use App\Entity\TaskList;
use App\Entity\TaskListLaser;
use App\Entity\TaskListLaserDetail;
use App\Entity\TaskRejectDetail;
use Doctrine\ORM\EntityManager;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseAuditControllerInterface;
use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportDataController extends ControllerBase implements BaseAuditControllerInterface
{
    private $_table = null;

    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
    }

    public function getNewEntity()
    {
        switch ($this->_table) {
            case 'ActivityControlQuestion':
                return new ActivityControlQuestion();
                break;
            case 'ControlQuestion':
                return new ControlQuestion();
                break;
            case 'Document':
                return new Document();
                break;
            case 'Employee':
                return new Employee();
                break;
            case 'JobRotationTeam':
                return new JobRotationTeam();
                break;
            case 'Mould':
                return new Mould();
                break;
            case 'MouldDetail':
                return new MouldDetail();
                break;
            case 'MouldGroup':
                return new MouldGroup();
                break;
            case 'Penetration':
                return new Penetration();
                break;
            case 'ProductTree':
                return new ProductTree();
                break;
            case 'RejectType':
                return new RejectType();
                break;
            case 'Stock':
                return new Stock();
                break;
            case 'TaskCaseControl':
                return new TaskCaseControl();
                break;
            case 'TaskImport':
                return new TaskImport();
                break;
            case 'TaskList':
                return new TaskList();
                break;
            case 'TaskListLaser':
                return new TaskListLaser();
                break;
            case 'TaskRejectDetail':
                return new TaskRejectDetail();
                break;
        }
    }

    /**
     * @Route(path="/m2m/ImportData", name="ImportData-import", options={"expose"=true}, methods={"PUT"})
     */
    public function putImportDataAction(Request $request, $_locale)
    {
        $uri = strtolower($request->getBaseUrl());
        $time_start = microtime(true);
        $batchSize = 20;
        $statusCode = 200;
        $userId = 1;
        $request->attributes->set('_isDCSService', true);
        $content = json_decode($request->getContent());
        $mtitle = ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.success_message_title', array(), 'KaitekFrameworkBundle');
        $message = 'OK';
        //$path = $content->file;
        $resp = array(
            'title' => $mtitle,
            'success' => true,
            'message' => $message/*,
            'file' => $e->getfile()."-".$e->getLine()*/
        );
        $b_persist = true;
        $table = $content->table;
        $customer = $content->customer;
        $_arr_err = [];
        if ((isset($content->data) && count($content->data) === 0) && (isset($content->deleted) && count($content->deleted) === 0)) {
            $resp['success'] = false;
            $resp['message'] = "Veri yok.";
            $statusCode = 400;
        } else {
            try {
                if (isset($content->data) && count($content->data) > 0) {
                    switch ($table) {
                        case 'activity_control_questions':{
                            $this->_table = 'ActivityControlQuestion';
                            /* @var $repo ActivityControlQuestion */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:ActivityControlQuestion');
                            //$arr_where_base=array('type','opname','question','answertype');
                            //$arr_where=array('type'=>null,'opname'=>null,'question'=>null,'answertype'=>null);
                            $arr_where_base = array('opname','documentnumber','question');
                            $arr_where = array('opname' => null,'documentnumber' => null,'question' => null);
                            break;
                        }
                        case 'control_questions':{
                            $this->_table = 'ControlQuestion';
                            /* @var $repo ControlQuestion */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:ControlQuestion');
                            $arr_where_base = array('type','opname','question'/*,'answertype','documentnumber'*/);
                            $arr_where = array('type' => null,'opname' => null,'question' => null/*,'answertype'=>null,'documentnumber'=>null*/);
                            break;
                        }
                        case 'documents':{
                            $this->_table = 'Document';
                            /* @var $repo Document */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:Document');
                            $arr_where_base = array('path');
                            $arr_where = array('path' => null);
                            break;
                        }
                        case 'employees':
                            $this->_table = 'Employee';
                            /* @var $repo Employee */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:Employee');
                            $arr_where_base = array('code');
                            $arr_where = array('code' => null);
                            break;
                        case 'job_rotation_teams':
                            $this->_table = 'JobRotationTeam';
                            /* @var $repo JobRotationTeam */
                            $repo = $this->getDoctrine()
                            ->getRepository('App:JobRotationTeam');
                            $arr_where_base = array('code');
                            $arr_where = array('code' => null);
                            break;
                        case 'kanban_operations':
                            $this->_table = 'KanbanOperation';
                            /* @var $repo KanbanOperation */
                            $repo = $this->getDoctrine()
                            ->getRepository('App:KanbanOperation');
                            $arr_where_base = array('product');
                            $arr_where = array('product' => null);
                            break;
                        case 'mould_details':
                            $this->_table = 'MouldDetail';
                            /* @var $repo MouldDetail */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:MouldDetail');
                            $arr_where_base = array('mould','mouldgroup','opcode','opnumber','opname');
                            $arr_where = array('mould' => null,'mouldgroup' => null,'opcode' => null,'opnumber' => null,'opname' => null);
                            break;
                        case 'mould_groups':
                            $this->_table = 'MouldGroup';
                            /* @var $repo MouldGroup */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:MouldGroup');
                            $arr_where_base = array('code','mould');
                            $arr_where = array('code' => null,'mould' => null);
                            break;
                        case 'moulds':
                        case 'moulds_banned':
                            $this->_table = 'Mould';
                            /* @var $repo Mould */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:Mould');
                            $arr_where_base = array('code');
                            $arr_where = array('code' => null);
                            break;
                        case 'penetrations':
                            $this->_table = 'Penetration';
                            /* @var $repo Penetration */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:Penetration');
                            $arr_where_base = array('opname','timecreate');
                            $arr_where = array('opname' => null,'timecreate' => null);
                            break;
                        case 'product_trees':
                            $this->_table = 'ProductTree';
                            /* @var $repo ProductTree */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:ProductTree');
                            $arr_where_base = array('parent','code','materialtype','name','stockcode','stockname','client');
                            $arr_where = array('parent' => null,'code' => null,'materialtype' => null,'name' => null,'stockcode' => null,'stockname' => null,'client' => null);
                            break;
                        case 'product_trees_packcapacity':
                            $this->_table = 'ProductTree';
                            /* @var $repo ProductTree */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:ProductTree');
                            $arr_where_base = array('materialtype','name','stockcode');
                            $arr_where = array('materialtype' => 'H','name' => null,'stockcode' => null);
                            break;
                        case 'product_trees_tpp':
                            $this->_table = 'ProductTree';
                            /* @var $repo ProductTree */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:ProductTree');
                            $arr_where_base = array('materialtype','name');
                            $arr_where = array('materialtype' => 'O','name' => null);
                            break;
                        case 'reject_types':
                            $this->_table = 'RejectType';
                            /* @var $repo RejectType */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:RejectType');
                            $arr_where_base = array('code','extcode');
                            $arr_where = array('code' => null,'extcode' => null);
                            break;
                        case 'stocks':
                            $this->_table = 'Stock';
                            /* @var $repo Stock */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:Stock');
                            $arr_where_base = array( 'code', 'opcode');
                            $arr_where = array('code' => null, 'opcode' => null);
                            break;
                        case 'task_case_controls':
                            $this->_table = 'TaskCaseControl';
                            /* @var $repo TaskCaseControl */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:TaskCaseControl');
                            $arr_where_base = array( 'code', 'opcode');
                            $arr_where = array('code' => null, 'opcode' => null);
                            break;
                        case 'task_imports':
                            $em = $this->getDoctrine()->getManager();
                            $conn = $em->getConnection();
                            $sql = "delete FROM task_imports where status='P'";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                            $this->_table = 'TaskImport';
                            /* @var $repo TaskImport */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:TaskImport');
                            $arr_where_base = array( 'erprefnumber', 'opcode', 'opnumber', 'opname');
                            $arr_where = array('erprefnumber' => null, 'opcode' => null, 'opnumber' => null, 'opname' => null);
                            break;
                        case 'task_lists':
                            $this->_table = 'TaskList';
                            /* @var $repo TaskList */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:TaskList');
                            $arr_where_base = array( 'erprefnumber', 'opcode', 'opnumber');
                            $arr_where = array('erprefnumber' => null, 'opcode' => null, 'opnumber' => null,'deleted' => null);
                            break;
                        case 'task_plans':
                            $this->_table = 'task_plans';
                            break;
                        case 'task_list_lasers':
                            $this->_table = 'TaskListLaser';
                            /* @var $repo TaskListLaser */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:TaskListLaser');
                            $arr_where_base = array( 'description', 'client1');
                            $arr_where = array('description' => null, 'client1' => null);
                            break;
                        case 'task_reject_details':
                            $this->_table = 'TaskRejectDetail';
                            /* @var $repo TaskRejectDetail */
                            $repo = $this->getDoctrine()
                                ->getRepository('App:TaskRejectDetail');
                            $arr_where_base = array( 'client', 'opname', 'time', 'rejecttype', 'quantityscrap', 'importfilename');
                            $arr_where = array('client' => null, 'opname' => null, 'time' => null, 'rejecttype' => null, 'quantityscrap' => null, 'importfilename' => null);
                            break;
                    }
                    if ($this->_table !== null) {
                        $em = $this->getDoctrine()->getManager();
                        $conn = $em->getConnection();
                        $conn->beginTransaction();
                        try {
                            if ($table === 'task_plans') {
                                $sql = "truncate table task_plan_employees";
                                $stmt = $conn->prepare($sql);
                                $stmt->execute();
                            }
                            $dirty = false;
                            for ($i = 0;$i < count($content->data);$i++) {
                                $row = $content->data[$i];
                                if ($customer === 'oskim' && $table === 'control_questions' && $i == 0) {
                                    $arr = explode('-', $row->opname);
                                    $cc = 0;
                                    if (count($arr) >= 2) {
                                        $opcode = implode('-', array_splice($arr, 0, count($arr) - 1));
                                        $sql_cq = 'SELECT record_id 
                                            from control_questions 
                                            where delete_user_id is null and opname like :opname and type=:type';
                                        $stmt_cq = $conn->prepare($sql_cq);
                                        $stmt_cq->bindValue('opname', $opcode."%");
                                        $stmt_cq->bindValue('type', $row->type);
                                        $stmt_cq->execute();
                                        $records_cq = $stmt_cq->fetchAll();
                                        $cc = count($records_cq);
                                        if ($cc > 0 && $cc < 50) {
                                            for ($i_cq = 0;$i_cq < $cc;$i_cq++) {
                                                $row_cq = $records_cq[$i_cq];
                                                /* @var $repo_cq ControlQuestion */
                                                $repo_cq = $this->getDoctrine()
                                                ->getRepository('App:ControlQuestion');
                                                $entity_cq = $repo_cq->findOneBy(array('recordId' => $row_cq["record_id"]));
                                                if ($entity_cq !== null) {
                                                    if (is_callable(array($entity_cq, "setUpdateuserId"))) {
                                                        $entity_cq->setUpdateuserId($userId);
                                                    }
                                                    if (is_callable(array($entity_cq, "setDeleteuserId"))) {
                                                        $entity_cq->setDeleteuserId($userId);
                                                    }
                                                    $em->persist($entity_cq);
                                                    $em->flush();
                                                    $em->remove($entity_cq);
                                                    //if(($i_cq % $batchSize) === 0){
                                                    //    $em->flush();
                                                    //    $em->clear();
                                                    //    //$em->remove($entity);
                                                    //    //$em->flush();
                                                    //}
                                                }
                                            }
                                            $em->flush();
                                            $em->clear();
                                        } else {
                                            if ($cc > 0) {
                                                throw new \Error('Kontrol soru import silinecek kayıt sayısı:'.$cc.',opname:'.$opcode.'. İşlem yapılamıyor.');
                                            }
                                        }
                                    }
                                }
                                if ($customer === 'sahince' && $table === 'product_trees' && $i == 0) {
                                    $sql_pt = 'SELECT * 
                                        from product_trees 
                                        where finish is null and code = :code ';
                                    $stmt_pt = $conn->prepare($sql_pt);
                                    $stmt_pt->bindValue('code', $row->code);
                                    $stmt_pt->execute();
                                    $records_pt = $stmt_pt->fetchAll();
                                    if (count($records_pt) > 0) {
                                        for ($i_pt = 0;$i_pt < count($records_pt);$i_pt++) {
                                            $row_pt = $records_pt[$i_pt];
                                            /* @var $repo_cq ProductTree */
                                            $repo_pt = $this->getDoctrine()
                                            ->getRepository('App:ProductTree');
                                            $entity_pt = $repo_pt->findOneBy(array('recordId' => $row_pt["record_id"]));
                                            if ($entity_pt !== null) {
                                                if (is_callable(array($entity_pt, "setUpdateuserId"))) {
                                                    $entity_pt->setUpdateuserId($userId);
                                                }
                                                if (is_callable(array($entity_pt, "setDeleteuserId"))) {
                                                    $entity_pt->setDeleteuserId($userId);
                                                }
                                                $em->persist($entity_pt);
                                                $em->flush();
                                                $em->remove($entity_pt);
                                            }
                                        }
                                        $em->flush();
                                        $em->clear();
                                    }
                                }
                                if ($table !== 'task_plans') {
                                    $proc = '';
                                    if ($table == 'product_trees_packcapacity') {
                                        $row->materialtype = 'H';
                                    }
                                    if ($table == 'product_trees_tpp') {
                                        $row->materialtype = 'O';
                                    }
                                    foreach ($arr_where_base as $item) {
                                        if (($table == 'task_reject_details' && $item == 'time') || ($table == 'penetrations' && $item == 'timecreate')) {
                                            $arr_where[$item] = new \DateTime($row->$item);
                                        } else {
                                            $arr_where[$item] = (isset($row->$item) ? $row->$item : null);
                                        }
                                    }
                                    $entity = $repo->findOneBy($arr_where);
                                    if ($entity !== null) {
                                        if (is_callable(array($entity, "setUpdateuserId"))) {
                                            $entity->setUpdateuserId($userId);
                                        }
                                        switch ($table) {
                                            case 'activity_control_questions':
                                                if ($entity->getPeriod() != $row->period
                                                    || $entity->getStart() != new \DateTime($row->start)
                                                    || (/*$row->finish!=""&&*/$entity->getFinish() != new \DateTime($row->finish))
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'control_questions':
                                                if ($entity->getValuerequire() != $row->valuerequire
                                                    || $entity->getValuemin() != $row->valuemin
                                                    || $entity->getValuemax() != $row->valuemax
                                                    || $entity->getDocumentnumber() != $row->documentnumber
                                                    || $entity->getAnswertype() != $row->answertype
                                                    || $entity->getPath() != $row->path
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'documents':
                                                if ($entity->getCurrentversion() !== $row->currentversion
                                                    || $entity->getOpcode() !== $row->opcode
                                                    || $entity->getOpname() !== $row->opname
                                                    || $entity->getLastupdate() !== $row->lastupdate
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'employees':
                                                if ($entity->getName() != $row->name
                                                    || $entity->getIsexpert() != $this->is_true($row->isexpert)
                                                    || $entity->getIsoperator() != $this->is_true($row->isoperator)
                                                    || $entity->getIsmaintenance() != $this->is_true($row->ismaintenance)
                                                    || $entity->getIsmould() != $this->is_true($row->ismould)
                                                    || $entity->getIsquality() != $this->is_true($row->isquality)
                                                    || (isset($row->secret) && $entity->getSecret() != $row->secret)
                                                    //||$entity->getJobrotationteam()!=$row->jobrotationteam
                                                    || $entity->getStart() != new \DateTime($row->start)
                                                    || (/*$row->finish!=""&&*/$entity->getFinish() != new \DateTime($row->finish))
                                                    || ($customer === 'SAHINCE' && $entity->getLastseen() != new \DateTime($row->lastseen))
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'job_rotation_teams':
                                                if ($entity->getStart() != new \DateTime($row->start)
                                                    || (/*$row->finish!=""&&*/$entity->getFinish() != new \DateTime($row->finish))
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'kanban_operations':
                                                if ($entity->getCurrentboxcount() != $row->currentboxcount
                                                || $entity->getPackaging() != $row->packaging
                                                || ($entity->getBoxcount() != $row->boxcount && $row->boxcount)
                                                || ($entity->getMinboxcount() != $row->minboxcount && $row->minboxcount)
                                                || ($entity->getMaxboxcount() != $row->maxboxcount && $row->maxboxcount)
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'mould_details':
                                                if ($entity->getLeafmask() != $row->leafmask
                                                    || $entity->getStart() != new \DateTime($row->start)
                                                    || (/*$row->finish!=""&&*/$entity->getFinish() != new \DateTime($row->finish))
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'mould_groups':
                                                if ($entity->getProductionmultiplier() != intval($row->productionmultiplier)
                                                    || $entity->getIntervalmultiplier() != intval($row->intervalmultiplier)
                                                    || $entity->getSetup() != intval($row->setup)
                                                    || $entity->getCycletime() != $row->cycletime
                                                    || $entity->getLotcount() != intval($row->lotcount)
                                                    || $entity->getStart() != new \DateTime($row->start)
                                                    || (isset($row->program) && $entity->getProgram() != $row->program)
                                                    || (/*$row->finish!=""&&*/$entity->getFinish() != new \DateTime($row->finish))
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'moulds':
                                                if ($entity->getDescription() != $row->description
                                                    || $entity->getMouldtype() != $row->mouldtype
                                                    || $entity->getSerialnumber() != $row->serialnumber
                                                    || $entity->getStart() != new \DateTime($row->start)
                                                    || (/*$row->finish!=""&&*/$entity->getFinish() != new \DateTime($row->finish))
                                                    || isset($row->groups)
                                                    || (isset($row->issensormould) &&$entity->getIssensormould()!=$row->issensormould)
                                                ) {
                                                    if (isset($content->deleteoldrecord)) {
                                                        $bt = $entity->getBantime();
                                                        $entity->setDeleteuserId($userId);
                                                        $em->persist($entity);
                                                        $em->flush();
                                                        $em->remove($entity);
                                                        $em->flush();
                                                        $entity = null;
                                                        $repo_g = $this->getDoctrine()
                                                            ->getRepository('App:MouldGroup');
                                                        $entities_g = $repo_g->findBy(array('mould' => $row->code));
                                                        foreach ($entities_g as $entity_g) {
                                                            $dirty = true;
                                                            $entity_g->setDeleteuserId($userId);
                                                            $em->persist($entity_g);
                                                            $em->flush();
                                                            $em->remove($entity_g);
                                                            $em->flush();
                                                        }
                                                        $repo_d = $this->getDoctrine()
                                                            ->getRepository('App:MouldDetail');
                                                        $entities_d = $repo_d->findBy(array('mould' => $row->code));
                                                        foreach ($entities_d as $entity_d) {
                                                            $dirty = true;
                                                            $entity_d->setDeleteuserId($userId);
                                                            $em->persist($entity_d);
                                                            $em->flush();
                                                            $em->remove($entity_d);
                                                            $em->flush();
                                                        }
                                                        $em->clear();
                                                        $entity = $this->getNewEntity();
                                                        $entity->setBantime($bt);
                                                        $entity->setCreateuserId($userId);
                                                        $proc = 'I';
                                                    } else {
                                                        $proc = 'U';
                                                    }
                                                }
                                                break;
                                            case 'moulds_banned':
                                                if ($row->bantime != "") {
                                                    $entity->setBantime(new \DateTime($row->bantime));
                                                } else {
                                                    $entity->setBantime(null);
                                                }
                                                $proc = 'U';
                                                break;
                                            case 'penetrations':
                                                $proc = 'I';
                                                $entity = $this->getNewEntity();
                                                break;
                                            case 'product_trees':
                                                if ($entity->getDescription() != $row->description
                                                    || $entity->getTpp() != $row->tpp
                                                    || $entity->getQuantity() != $row->quantity
                                                    || $entity->getUnit_quantity() != $row->unit_quantity
                                                    || $entity->getOpcounter() != intval($row->opcounter)
                                                    || $entity->getPack() != $row->pack
                                                    || $entity->getPackcapacity() != $row->packcapacity
                                                    || $entity->getUnit_packcapacity() != $row->unit_packcapacity
                                                    || $entity->getOporder() != intval($row->oporder)
                                                    || $entity->getPreptime() != intval($row->preptime)
                                                    || $entity->getQualitycontrolrequire() != $this->is_true($row->qualitycontrolrequire)
                                                    || $entity->getTask() != $this->is_true($row->task)
                                                    || $entity->getFeeder() != $row->feeder
                                                    || $entity->getDrainer() != $row->drainer
                                                    || $entity->getCarrierfeeder() != $row->carrierfeeder
                                                    || $entity->getCarrierdrainer() != $row->carrierdrainer
                                                    || $entity->getGoodsplanner() != $row->goodsplanner
                                                    || $entity->getDelivery() != $row->delivery
                                                    || $entity->getProductionmultiplier() != intval($row->productionmultiplier)
                                                    || $entity->getIntervalmultiplier() != intval($row->intervalmultiplier)
                                                    || $entity->getStart() != new \DateTime($row->start)
                                                    || (/*$row->finish!=""&&*/$entity->getFinish() != new \DateTime($row->finish))
                                                    || $entity->getLocationsource() != $row->locationsource
                                                    || $entity->getLocationdestination() != $row->locationdestination
                                                    || (isset($row->empcount) && $entity->getEmpcount() != $row->empcount)
                                                    || (isset($row->isdefault) && $entity->getIsdefault() != $this->is_true($row->isdefault))
                                                    || (isset($row->notificationtime) && $entity->getNotificationtime() != $row->notificationtime)
                                                    || (isset($row->lotcount) && $entity->getLotcount() != $row->lotcount)
                                                    || $customer == 'kaitek' || $customer == 'oskim'
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'product_trees_packcapacity':
                                                if ($entity->getPackcapacity() != $row->packcapacity) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'product_trees_tpp':
                                                if ($entity->getTpp() != $row->tpp) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'reject_types':
                                                if ($entity->getCode() != $row->code
                                                    || $entity->getExtcode() != $row->extcode
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'stocks':
                                                if (intval($entity->getPackcapacity()) != intval($row->packcapacity)
                                                    || intval($entity->getQuantityremaining()) != intval($row->quantityremaining)
                                                    || $entity->getClients() != $row->clients
                                                    || $entity->getFilename() != $row->filename
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'task_case_controls':
                                                if (intval($entity->getPackcapacity()) != intval($row->packcapacity)
                                                    || intval($entity->getQuantityremaining()) != intval($row->quantityremaining)
                                                    || $entity->getClients() != $row->clients
                                                    || $entity->getFilename() != $row->filename
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'task_imports':
                                                if ($entity->getProductcount() != intval($row->productcount)
                                                    || $entity->getClient() != $row->client
                                                    || $entity->getDeadline() != new \DateTime($row->deadline)
                                                    || $entity->getStatus() != $row->status
                                                    || intval($entity->getTpp()) != intval($row->tpp)
                                                ) {
                                                    $proc = 'U';
                                                }
                                                break;
                                            case 'task_lists':
                                                if ($entity->getProductcount() != intval($row->productcount)
                                                    || $entity->getClient() != $row->client
                                                    || $entity->getDeadline() != new \DateTime($row->deadline)
                                                    || (isset($row->plannedstart) && $row->plannedstart !== '' && $row->plannedstart !== null && $entity->getPlannedstart() != new \DateTime($row->plannedstart))
                                                    || (isset($row->plannedfinish) && $row->plannedfinish !== '' && $row->plannedfinish !== null && $entity->getPlannedfinish() != new \DateTime($row->plannedfinish))
                                                    || substr($entity->getOpdescription(), 0, 250) != substr($row->opdescription, 0, 250)
                                                    || ($customer === 'SAHINCE' && $entity->getPlannedfinish() != new \DateTime($row->plannedfinish))
                                                    || ($customer == 'oskim' && $entity->getFinish() != null && $entity->getStart() == null && $entity->getProductdonecount() == 0)
                                                    || ($customer == 'oskim' && isset($row->mouldaddress) && $entity->getMouldaddress() != $row->mouldaddress)
                                                    || ($customer == 'oskim' && isset($row->taskinfo) && $entity->getTaskinfo() != $row->taskinfo)
                                                ) {
                                                    if ($customer == 'ermetal') {
                                                        if ($entity->getFinish() !== null) {
                                                            $proc = '';
                                                        } else {
                                                            $proc = 'U';
                                                        }
                                                    } else {
                                                        $proc = 'U';
                                                    }
                                                }
                                                break;
                                                //eski sistemden yapılan aktarımlarda işin daha önce aktarılmamış olması gerekiyor.
                                                //o nedenle bu kısım kapatıldı.
                                                //case 'task_lists':
                                                //    if( $entity->getProductcount()!=intval($row->productcount)
                                                //        ||$entity->getClient()!=$row->client
                                                //        ||$entity->getDeadline()!=new \DateTime($row->deadline)
                                                //    ){
                                                //        $proc='U';
                                                //    }
                                                //    break;
                                            case 'task_list_lasers':
                                                if ($entity->getDescription() != $row->description
                                                    || $entity->getClient1() != $row->client1
                                                    || $customer == 'kaitek' || $customer == 'oskim'
                                                ) {
                                                    if ($customer == 'ermetal') {
                                                        if ($entity->getFinish() !== null) {
                                                            $proc = '';
                                                        } else {
                                                            $proc = 'U';
                                                        }
                                                    } else {
                                                        $proc = 'U';
                                                    }
                                                }
                                                break;
                                            case 'task_reject_details':
                                                if ($entity->getClient() != $row->client
                                                    || $entity->getOpname() != $row->opname
                                                    || $entity->getRejecttype() != $row->rejecttype
                                                    || $entity->getQuantityScrap() != intval($row->quantityscrap)
                                                    || $entity->getTime() != new \DateTime($row->time)
                                                    || (isset($row->importfilename) && $entity->getImportfilename() != $row->importfilename)
                                                ) {
                                                    $proc = 'U';
                                                    //if($customer=='ermetal'){
                                                    //    if($entity->getFinish()!==null){
                                                    //        $proc='';
                                                    //    }else{
                                                    //        $proc='U';
                                                    //    }
                                                    //}else{
                                                    //    $proc='U';
                                                    //}
                                                }
                                                break;
                                        }
                                        if ($table !== 'kanban_operations' && $table !== 'product_trees_packcapacity' && $table !== 'product_trees_tpp' && $table !== 'task_imports') {
                                            $entity->setCreateuserId($userId);
                                        }
                                    } else {
                                        if ($table !== 'kanban_operations' && $table !== 'product_trees_packcapacity' && $table !== 'product_trees_tpp') {
                                            $entity = $this->getNewEntity();
                                            if ($table !== 'task_imports') {
                                                $entity->setCreateuserId($userId);
                                            }
                                            $proc = 'I';
                                        }
                                        if ($customer === 'sahince' && $table === 'product_trees' && ($row->finish !== "" && $row->finish !== "null" && $row->finish !== null)) {
                                            $proc = '';
                                        }
                                    }
                                    if ($proc != '') {
                                        switch ($table) {
                                            case 'activity_control_questions':
                                                if (isset($row->erprefnumber) && !isset($row->documentnumber)) {
                                                    $row->documentnumber = $row->erprefnumber;
                                                }
                                                $row->type = "SETUP-AYAR";
                                                $row->answertype = "Evet-Hayır";
                                                $row->valuerequire = null;
                                                $row->valuemin = null;
                                                $row->valuemax = null;
                                                $entity->setType($row->type);
                                                $entity->setOpname($row->opname);
                                                $entity->setDocumentnumber($row->documentnumber);
                                                $entity->setQuestion($row->question);
                                                $entity->setAnswertype($row->answertype);
                                                $entity->setValuerequire($row->valuerequire);
                                                $entity->setValuemin($row->valuemin);
                                                $entity->setValuemax($row->valuemax);
                                                $entity->setPeriod($row->period);
                                                $entity->setStart(new \DateTime($row->start));
                                                if (isset($row->finish)) {
                                                    if ($row->finish != "") {
                                                        $entity->setFinish(new \DateTime($row->finish));
                                                    } else {
                                                        $entity->setFinish(null);
                                                    }
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                break;
                                            case 'control_questions':
                                                $entity->setType($row->type);
                                                $entity->setOpname($row->opname);
                                                $entity->setQuestion($row->question);
                                                $entity->setAnswertype($row->answertype);
                                                $entity->setValuerequire($row->valuerequire);
                                                $entity->setValuemin($row->valuemin);
                                                $entity->setValuemax($row->valuemax);
                                                $entity->setDocumentnumber($row->documentnumber);
                                                $entity->setPath((isset($row->path) ? $row->path : null));
                                                break;
                                            case 'documents':
                                                $entity->setType($row->type);
                                                $entity->setPath($row->path);
                                                $entity->setOpcode($row->opcode);
                                                $entity->setOpnumber($row->opnumber);
                                                $entity->setOpname($row->opname);
                                                $entity->setCurrentversion($row->currentversion);
                                                $entity->setLastupdate(new \DateTime($row->lastupdate));
                                                break;
                                            case 'employees':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setName($row->name);
                                                $entity->setSecret($row->code);
                                                $entity->setIsexpert($this->is_true($row->isexpert));
                                                $entity->setIsoperator($this->is_true($row->isoperator));
                                                $entity->setIsmaintenance($this->is_true($row->ismaintenance));
                                                $entity->setIsmould($this->is_true($row->ismould));
                                                $entity->setIsquality($this->is_true($row->isquality));
                                                if (isset($row->secret)) {
                                                    $entity->setSecret($row->secret);
                                                }
                                                //$entity->setJobrotationteam($row->jobrotationteam);
                                                $entity->setStart(new \DateTime($row->start));
                                                if ($row->finish != "") {
                                                    $entity->setFinish(new \DateTime($row->finish));
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                if ($customer === 'SAHINCE') {
                                                    if ($row->lastseen != "") {
                                                        $entity->setLastseen(new \DateTime($row->lastseen));
                                                    } else {
                                                        $entity->setLastseen(null);
                                                    }
                                                }
                                                break;
                                            case 'job_rotation_teams':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setStart(new \DateTime($row->start));
                                                if ($row->finish != "") {
                                                    $entity->setFinish(new \DateTime($row->finish));
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                break;
                                            case 'mould_details':
                                                if (!isset($row->mould)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setMould($row->mould);
                                                $entity->setMouldgroup($row->mouldgroup);
                                                $entity->setOpcode($row->opcode);
                                                $entity->setOpnumber($row->opnumber);
                                                $entity->setOpname($row->opname);
                                                $entity->setLeafmask($row->leafmask);
                                                $entity->setStart(new \DateTime($row->start));
                                                if ($row->finish != "") {
                                                    $entity->setFinish(new \DateTime($row->finish));
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                if ($proc == 'I') {
                                                    $entity->setLeafmaskcurrent($row->leafmask);
                                                }
                                                break;
                                            case 'mould_groups':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setMould($row->mould);
                                                $entity->setProductionmultiplier($row->productionmultiplier);
                                                $entity->setIntervalmultiplier($row->intervalmultiplier);
                                                $entity->setSetup($row->setup);
                                                $entity->setCycletime($row->cycletime);
                                                $entity->setLotcount($row->lotcount ? $row->lotcount : 100);
                                                $entity->setStart(new \DateTime($row->start));
                                                if (isset($row->program)) {
                                                    $entity->setProgram($row->program);
                                                }
                                                if ($row->finish != "") {
                                                    $entity->setFinish(new \DateTime($row->finish));
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                if ($proc == 'I') {
                                                    $entity->setCounter(0);
                                                    $entity->setSetup(1800);
                                                } else {
                                                    $entity->setCounter($entity->getCounter());
                                                    $entity->setSetup($entity->getSetup() > 0 ? $entity->getSetup() : 1800);
                                                }
                                                break;
                                            case 'moulds':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setDescription($row->description);
                                                $entity->setMouldtype($row->mouldtype);
                                                $entity->setSerialnumber($row->serialnumber);
                                                $entity->setStart(new \DateTime($row->start));
                                                $entity->setIssensormould($this->is_true($row->issensormould));
                                                if (isset($row->finish)) {
                                                    if ($row->finish != "") {
                                                        $entity->setFinish(new \DateTime($row->finish));
                                                    } else {
                                                        $entity->setFinish(null);
                                                    }
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                if ($proc == 'I') {
                                                    $entity->setCounter(0);
                                                }
                                                if (isset($row->groups)) {
                                                    //$em->persist($entity);
                                                    //$em->clear();
                                                    //$dirty=false;
                                                    //$b_persist=false;
                                                    for ($g = 0;$g < count($row->groups);$g++) {
                                                        $row_g = $row->groups[$g];
                                                        if (isset($row->finish)) {
                                                            if ($row->finish != "") {
                                                                $row_g->finish = $row->finish;
                                                            }
                                                        }
                                                        $proc_g = '';
                                                        $repo_g = $this->getDoctrine()
                                                            ->getRepository('App:MouldGroup');
                                                        $entity_g = $repo_g->findOneBy(array('code' => $row_g->code,'mould' => $row_g->mould));
                                                        if ($entity_g !== null) {
                                                            if (is_callable(array($entity_g, "setUpdateuserId"))) {
                                                                $entity_g->setUpdateuserId($userId);
                                                            }
                                                            if ($entity_g->getProductionmultiplier() != intval($row_g->productionmultiplier)
                                                                || $entity_g->getIntervalmultiplier() != intval($row_g->intervalmultiplier)
                                                                || $entity_g->getSetup() != intval($row_g->setup)
                                                                || $entity_g->getCycletime() != $row_g->cycletime
                                                                || $entity_g->getLotcount() != intval($row_g->lotcount)
                                                                || $entity_g->getStart() != new \DateTime($row_g->start)
                                                                || (isset($row_g->program) && $entity_g->getProgram() != $row_g->program)
                                                                || (isset($row_g->finish) && $row_g->finish != "" && $entity_g->getFinish() != new \DateTime($row_g->finish))
                                                                || isset($row_g->details)
                                                            ) {
                                                                $proc_g = 'U';
                                                            }
                                                        } else {
                                                            $entity_g = new MouldGroup();
                                                            $entity_g->setCreateuserId($userId);
                                                            $proc_g = 'I';
                                                        }
                                                        if ($proc_g != '') {
                                                            if (!isset($row_g->code)) {
                                                                throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                            }
                                                            $entity_g->setCode($row_g->code);
                                                            $entity_g->setMould($row_g->mould);
                                                            $entity_g->setProductionmultiplier($row_g->productionmultiplier);
                                                            $entity_g->setIntervalmultiplier($row_g->intervalmultiplier);
                                                            $entity_g->setSetup($row_g->setup);
                                                            $entity_g->setCycletime($row_g->cycletime);
                                                            $entity_g->setLotcount($row_g->lotcount ? $row_g->lotcount : 100);
                                                            $entity_g->setStart(new \DateTime($row_g->start));
                                                            if (isset($row_g->program)) {
                                                                $entity_g->setProgram($row_g->program);
                                                            }
                                                            if (isset($row_g->finish)) {
                                                                if ($row_g->finish != "") {
                                                                    $entity_g->setFinish(new \DateTime($row_g->finish));
                                                                } else {
                                                                    $entity_g->setFinish(null);
                                                                }
                                                            } else {
                                                                $entity_g->setFinish(null);
                                                            }
                                                            if ($proc_g == 'I') {
                                                                $entity_g->setCounter(0);
                                                                $entity_g->setSetup(1800);
                                                            } else {
                                                                $entity_g->setCounter($entity_g->getCounter());
                                                                $entity_g->setSetup($entity_g->getSetup() > 0 ? $entity_g->getSetup() : 1800);
                                                            }
                                                            $em->persist($entity_g);
                                                        }
                                                        if (isset($row_g->details)) {
                                                            for ($d = 0;$d < count($row_g->details);$d++) {
                                                                $row_d = $row_g->details[$d];
                                                                if (isset($row->finish)) {
                                                                    if ($row->finish != "") {
                                                                        $row_d->finish = $row->finish;
                                                                    }
                                                                }
                                                                if (isset($row_g->finish)) {
                                                                    if ($row_g->finish != "") {
                                                                        $row_d->finish = $row_g->finish;
                                                                    }
                                                                }
                                                                $proc_d = '';
                                                                $repo_d = $this->getDoctrine()
                                                                    ->getRepository('App:MouldDetail');
                                                                $entity_d = $repo_d->findOneBy(array('mould' => $row_d->mould,'mouldgroup' => $row_d->mouldgroup,'opcode' => $row_d->opcode,'opnumber' => $row_d->opnumber,'opname' => $row_d->opname));
                                                                if ($entity_d !== null) {
                                                                    if (is_callable(array($entity_d, "setUpdateuserId"))) {
                                                                        $entity_d->setUpdateuserId($userId);
                                                                    }
                                                                    if ($entity_d->getLeafmask() !== $row_d->leafmask
                                                                        || $entity_d->getStart() != new \DateTime($row_d->start)
                                                                        || (isset($row_d->finish) && $row_d->finish != "" && $entity_d->getFinish() != new \DateTime($row_d->finish))
                                                                    ) {
                                                                        $proc_d = 'U';
                                                                    }
                                                                } else {
                                                                    $entity_d = new MouldDetail();
                                                                    $entity_d->setCreateuserId($userId);
                                                                    $proc_d = 'I';
                                                                }
                                                                if ($proc_d != '') {
                                                                    if (!isset($row_d->mould)) {
                                                                        throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                                    }
                                                                    $entity_d->setMould($row_d->mould);
                                                                    $entity_d->setMouldgroup($row_d->mouldgroup);
                                                                    $entity_d->setOpcode($row_d->opcode);
                                                                    $entity_d->setOpnumber($row_d->opnumber);
                                                                    $entity_d->setOpname($row_d->opname);
                                                                    $entity_d->setLeafmask($row_d->leafmask);
                                                                    $entity_d->setStart(new \DateTime($row_d->start));
                                                                    if (isset($row_d->finish)) {
                                                                        if ($row_d->finish != "") {
                                                                            $entity_d->setFinish(new \DateTime($row_d->finish));
                                                                        } else {
                                                                            $entity_d->setFinish(null);
                                                                        }
                                                                    }
                                                                    if ($proc_d == 'I') {
                                                                        $entity_d->setLeafmaskcurrent($row_d->leafmask);
                                                                    } else {
                                                                        $entity_d->setLeafmaskcurrent($row_d->leafmaskcurrent);
                                                                    }
                                                                    $em->persist($entity_d);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case 'penetrations':
                                                $entity->setOpname($row->opname);
                                                $entity->setTimecreate(new \DateTime($row->timecreate));
                                                if (isset($row->title)) {
                                                    $entity->setTitle($row->title);
                                                }
                                                if (isset($row->message)) {
                                                    $entity->setMessage($row->message);
                                                }
                                                break;
                                            case 'product_trees':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $row->tpp = $row->tpp === '' ? null : $row->tpp;
                                                $row->quantity = $row->quantity === '' ? null : $row->quantity;
                                                $row->packcapacity = $row->packcapacity === '' ? null : $row->packcapacity;
                                                $row->intervalmultiplier = $row->intervalmultiplier === '' ? null : $row->intervalmultiplier;
                                                $entity->setParent($row->parent);
                                                $entity->setCode($row->code);
                                                $entity->setNumber($row->number);
                                                $entity->setDescription($row->description);
                                                $entity->setMaterialtype($row->materialtype);
                                                $entity->setName($row->name);
                                                $entity->setStockcode($row->stockcode);
                                                $entity->setStockname($row->stockname);
                                                $entity->setTpp($row->tpp);
                                                $entity->setQuantity($row->quantity);
                                                $entity->setUnit_quantity($row->unit_quantity);
                                                $entity->setOpcounter($row->opcounter);
                                                $entity->setPack($row->pack);
                                                $entity->setPackcapacity($row->packcapacity);
                                                $entity->setUnit_packcapacity($row->unit_packcapacity);
                                                $entity->setOporder($row->oporder);
                                                $entity->setPreptime($row->preptime);
                                                $entity->setQualitycontrolrequire($this->is_true($row->qualitycontrolrequire));
                                                $entity->setTask($this->is_true($row->task));
                                                $entity->setFeeder($row->feeder);
                                                $entity->setDrainer($row->drainer);
                                                $entity->setCarrierfeeder($row->carrierfeeder);
                                                $entity->setCarrierdrainer($row->carrierdrainer);
                                                $entity->setGoodsplanner($row->goodsplanner);
                                                $entity->setDelivery($this->is_true($row->delivery));
                                                $entity->setProductionmultiplier($row->productionmultiplier);
                                                $entity->setIntervalmultiplier($row->intervalmultiplier);
                                                $entity->setStart(new \DateTime($row->start));
                                                $entity->setLocationsource($row->locationsource);
                                                $entity->setLocationdestination($row->locationdestination);
                                                if (isset($row->empcount)) {
                                                    $entity->setEmpcount($row->empcount);
                                                }
                                                if (isset($row->isdefault)) {
                                                    $entity->setIsdefault($this->is_true($row->isdefault));
                                                }
                                                if (isset($row->client)) {
                                                    $entity->setClient($row->client);
                                                }
                                                if (isset($row->notificationtime)) {
                                                    $entity->setNotificationtime($row->notificationtime);
                                                }
                                                if (isset($row->lotcount)) {
                                                    $entity->setLotcount($row->lotcount);
                                                }
                                                if ($row->finish != "") {
                                                    $entity->setFinish(new \DateTime($row->finish));
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                if ($row->materialtype === 'O' && ($customer == 'kaitek' || $customer == 'oskim')) {
                                                    $sql_md = "SELECT * from mould_details where mould not like 'HP0%' and opname=:opname";
                                                    $stmt_md = $conn->prepare($sql_md);
                                                    $stmt_md->bindValue('opname', $row->name);
                                                    $stmt_md->execute();
                                                    $records_md = $stmt_md->fetchAll();
                                                    if (count($records_md) > 0) {
                                                        foreach ($records_md as $row_md) {
                                                            $repo_md = $this->getDoctrine()
                                                            ->getRepository('App:MouldDetail')
                                                            ->findBy(array('recordId' => $row_md["record_id"]));
                                                            foreach ($repo_md as $entity_md) {
                                                                if ($entity_md->getDeleteuserId() == null) {
                                                                    if (is_callable(array($entity, "setUpdateuserId"))) {
                                                                        $entity_md->setUpdateuserId($userId);
                                                                    }
                                                                    if (is_callable(array($entity, "setDeleteuserId"))) {
                                                                        $entity_md->setDeleteuserId($userId);
                                                                    }
                                                                    $em->persist($entity_md);
                                                                    $em->flush();
                                                                    $em->remove($entity_md);
                                                                    $em->flush();
                                                                }
                                                            }
                                                            $sql_mdg = "SELECT * from mould_details where mouldgroup=:mouldgroup";
                                                            $stmt_mdg = $conn->prepare($sql_mdg);
                                                            $stmt_mdg->bindValue('mouldgroup', $row_md["mouldgroup"]);
                                                            $stmt_mdg->execute();
                                                            $records_mdg = $stmt_mdg->fetchAll();
                                                            if (count($records_mdg) === 0) {
                                                                $repo_mg = $this->getDoctrine()
                                                                ->getRepository('App:MouldGroup')
                                                                ->findBy(array('code' => $row_md["mouldgroup"]));
                                                                foreach ($repo_mg as $entity_mg) {
                                                                    if ($entity_mg->getDeleteuserId() == null) {
                                                                        if (is_callable(array($entity, "setUpdateuserId"))) {
                                                                            $entity_mg->setUpdateuserId($userId);
                                                                        }
                                                                        if (is_callable(array($entity, "setDeleteuserId"))) {
                                                                            $entity_mg->setDeleteuserId($userId);
                                                                        }
                                                                        $em->persist($entity_mg);
                                                                        $em->flush();
                                                                        $em->remove($entity_mg);
                                                                        $em->flush();
                                                                    }
                                                                }
                                                                $sql_mg = "SELECT * from mould_groups where mould=:mould";
                                                                $stmt_mg = $conn->prepare($sql_mg);
                                                                $stmt_mg->bindValue('mould', $row_md["mould"]);
                                                                $stmt_mg->execute();
                                                                $records_mg = $stmt_mg->fetchAll();
                                                                if (count($records_mg) === 0) {
                                                                    $repo_m = $this->getDoctrine()
                                                                    ->getRepository('App:Mould')
                                                                    ->findBy(array('code' => $row_md["mould"]));
                                                                    foreach ($repo_m as $entity_m) {
                                                                        if ($entity_m->getDeleteuserId() == null) {
                                                                            if (is_callable(array($entity, "setUpdateuserId"))) {
                                                                                $entity_m->setUpdateuserId($userId);
                                                                            }
                                                                            if (is_callable(array($entity, "setDeleteuserId"))) {
                                                                                $entity_m->setDeleteuserId($userId);
                                                                            }
                                                                            $em->persist($entity_m);
                                                                            $em->flush();
                                                                            $em->remove($entity_m);
                                                                            $em->flush();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case 'product_trees_packcapacity':
                                                $entity->setPackcapacity($row->packcapacity);
                                                break;
                                            case 'product_trees_tpp':
                                                $entity->setTpp($row->tpp);
                                                break;
                                            case 'reject_types':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setExtcode($row->extcode);
                                                $entity->setDescription($row->description);
                                                $entity->setStart(new \DateTime());
                                                $entity->setIslisted(true);
                                                if ($row->finish != "") {
                                                    $entity->setFinish(new \DateTime($row->finish));
                                                } else {
                                                    $entity->setFinish(null);
                                                }
                                                break;
                                            case 'stocks':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setOpcode($row->opcode);
                                                $entity->setPackcapacity($row->packcapacity);
                                                $entity->setQuantityremaining($row->quantityremaining);
                                                $entity->setClients($row->clients);
                                                $entity->setFilename($row->filename);
                                                break;
                                            case 'task_case_controls':
                                                if (!isset($row->code)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setOpcode($row->opcode);
                                                $entity->setPackcapacity($row->packcapacity);
                                                $entity->setQuantityremaining($row->quantityremaining);
                                                $entity->setClients($row->clients);
                                                $entity->setFilename($row->filename);
                                                break;
                                            case 'task_imports':
                                                if (!isset($row->opcode)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setCode($row->code);
                                                $entity->setErprefnumber($row->erprefnumber);
                                                $entity->setOpcode($row->opcode);
                                                $entity->setOpnumber($row->opnumber);
                                                $entity->setOpname($row->opname);
                                                $entity->setOpdescription(substr($row->opdescription, 0, 250));
                                                $entity->setProductcount($row->productcount);
                                                $entity->setDeadline(new \DateTime($row->deadline));
                                                $entity->setClient($row->client);
                                                $entity->setType($row->type);
                                                $entity->setTpp($row->tpp);
                                                $entity->setStatus($row->status);
                                                //ermetal pres uygulamasında iş bilgilerinden kalıp tanımlarının güncellenmesi
                                                if ($customer == 'ermetal' && $uri == '/pres' && isset($row->tpp)) {
                                                    if (intval($row->opnumber) !== 0) {
                                                        $row->opname = $row->opcode.'-'.$row->opnumber;
                                                    } else {
                                                        $row->opname = $row->opcode;
                                                    }

                                                    $repo_md = $this->getDoctrine()
                                                        ->getRepository('App:MouldDetail');
                                                    $entity_md = $repo_md->findOneBy(array('finish' => null,'opname' => $row->opname));
                                                    if ($entity_md !== null) {
                                                        $repo_mg = $this->getDoctrine()
                                                            ->getRepository('App:MouldGroup');
                                                        $entity_mg = $repo_mg->findOneBy(array('finish' => null,'code' => $entity_md->getMouldgroup()));
                                                        if ($entity_mg !== null) {
                                                            if ($entity_mg->getCycletime() !== $row->tpp && is_callable(array($entity_mg, "setUpdateuserId"))) {
                                                                $entity_mg->setUpdateuserId($userId);
                                                                $entity_mg->setCycletime($row->tpp);
                                                                $em->persist($entity_mg);
                                                                $dirty = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case 'task_lists':
                                                if (!isset($row->opcode)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $entity->setErprefnumber($row->erprefnumber);
                                                if ($proc === 'U' && $entity->getFinish() !== null) {
                                                    $proc = 'I';
                                                }
                                                //iş başlamamış ise veya client değişmemiş ise doğrudan güncelleme
                                                if ($proc == 'I') {
                                                    $entity->setCode($row->code);
                                                }
                                                if ($proc === 'U' && $entity->getClient() !== $row->client && $entity->getStart() !== null) {
                                                    //başlamış işin istasyonu değişti
                                                    $entity->setUpdateuserId($userId);
                                                    $entity->setOpcode($row->opcode);
                                                    $entity->setOpnumber($row->opnumber);
                                                    $entity->setOpname($row->opname);
                                                    $entity->setOpdescription(substr($row->opdescription, 0, 250));
                                                    $entity->setProductcount($row->productcount);
                                                    if (new \DateTime($row->deadline) > $entity->getDeadline()) {
                                                        $entity->setDeadline(new \DateTime($row->deadline));
                                                    }
                                                    if (isset($row->plannedstart) && $row->plannedstart !== '' && $row->plannedstart !== null) {
                                                        $entity->setPlannedstart(new \DateTime($row->plannedstart));
                                                    }
                                                    if (isset($row->plannedfinish) && $row->plannedfinish !== '' && $row->plannedfinish !== null) {
                                                        $entity->setPlannedfinish(new \DateTime($row->plannedfinish));
                                                    }
                                                    //$entity->setDuration($row->duration);
                                                    //$entity->setOfftime($row->offtime);
                                                    //$entity->setTotaltime($row->totaltime);
                                                    $entity->setClient($row->client);
                                                    $entity->setType($row->type);
                                                    $entity->setTpp($row->tpp);
                                                    $entity->setTaskfromerp(true);
                                                    if (isset($row->mouldaddress)) {
                                                        $entity->setMouldaddress($row->mouldaddress);
                                                    }
                                                    if (isset($row->taskinfo)) {
                                                        $entity->setTaskinfo(json_encode($row->taskinfo));
                                                    }
                                                    if (
                                                        $customer == 'oskim' && $entity->getFinish() != null
                                                        && (($entity->getStart() == null && $entity->getProductdonecount() == 0)
                                                            || ($entity->getStart() !== null && $entity->getProductdonecount() < $entity->getProductcount()))
                                                    ) {
                                                        $entity->setFinish(null);
                                                    }
                                                    ////eski iş kapatılıyor
                                                    //$entity->setFinish(new \DateTime());
                                                    //$entity->setTfddescription('import-change-station');
                                                    //$em->persist($entity);
                                                    ////yeni iş için yeni kayıt açılıyor
                                                    //$entity_new = $this->getNewEntity();
                                                    //$entity_new->setCreateuserId($userId);
                                                    ////$entity_new->setUpdateuserId($userId);
                                                    //$entity_new->setCode($row->code);
                                                    //$entity_new->setErprefnumber($row->erprefnumber);
                                                    //$entity_new->setOpcode($row->opcode);
                                                    //$entity_new->setOpnumber($row->opnumber);
                                                    //$entity_new->setOpname($row->opname);
                                                    //$entity_new->setOpdescription(substr($row->opdescription, 0, 250));
                                                    //$entity_new->setProductcount($row->productcount);
                                                    //$entity_new->setDeadline(new \DateTime($row->deadline));
                                                    //if (isset($row->plannedstart) && $row->plannedstart!=='' && $row->plannedstart!==null) {
                                                    //    $entity_new->setPlannedstart(new \DateTime($row->plannedstart));
                                                    //}
                                                    //if (isset($row->plannedfinish) && $row->plannedfinish!=='' && $row->plannedfinish!==null) {
                                                    //    $entity_new->setPlannedfinish(new \DateTime($row->plannedfinish));
                                                    //}
                                                    //$entity_new->setClient(substr($row->client, 0, 50));
                                                    //$entity_new->setType($row->type);
                                                    //$entity_new->setTpp($row->tpp);
                                                    //$entity_new->setStart($entity->getStart());
                                                    //$entity_new->setTaskfromerp(true);
                                                    //$entity_new->setProductdonecount($entity->getProductdonecount());
                                                    //$entity_new->setScrappart($entity->getScrappart());
                                                    //if (isset($row->mouldaddress)) {
                                                    //    $entity_new->setMouldaddress($row->mouldaddress);
                                                    //}
                                                    //if (isset($row->taskinfo)) {
                                                    //    $entity_new->setTaskinfo(json_encode($row->taskinfo));
                                                    //}
                                                    //$em->persist($entity_new);
                                                    //$dirty=true;
                                                } else {
                                                    $entity->setUpdateuserId($userId);
                                                    $entity->setOpcode($row->opcode);
                                                    $entity->setOpnumber($row->opnumber);
                                                    $entity->setOpname($row->opname);
                                                    $entity->setOpdescription(substr($row->opdescription, 0, 250));
                                                    $entity->setProductcount($row->productcount);
                                                    if (new \DateTime($row->deadline) > $entity->getDeadline()) {
                                                        $entity->setDeadline(new \DateTime($row->deadline));
                                                    }
                                                    if (isset($row->plannedstart) && $row->plannedstart !== '' && $row->plannedstart !== null) {
                                                        $entity->setPlannedstart(new \DateTime($row->plannedstart));
                                                    }
                                                    if (isset($row->plannedfinish) && $row->plannedfinish !== '' && $row->plannedfinish !== null) {
                                                        $entity->setPlannedfinish(new \DateTime($row->plannedfinish));
                                                    }
                                                    //$entity->setDuration($row->duration);
                                                    //$entity->setOfftime($row->offtime);
                                                    //$entity->setTotaltime($row->totaltime);
                                                    $entity->setClient($row->client);
                                                    $entity->setType($row->type);
                                                    $entity->setTpp($row->tpp);
                                                    $entity->setTaskfromerp(true);
                                                    if (isset($row->mouldaddress)) {
                                                        $entity->setMouldaddress($row->mouldaddress);
                                                    }
                                                    if (isset($row->taskinfo)) {
                                                        $entity->setTaskinfo(json_encode($row->taskinfo));
                                                    }
                                                    if (
                                                        $customer == 'oskim' && $entity->getFinish() != null
                                                        && (($entity->getStart() == null && $entity->getProductdonecount() == 0)
                                                            || ($entity->getStart() !== null && $entity->getProductdonecount() < $entity->getProductcount()))
                                                    ) {
                                                        $entity->setFinish(null);
                                                    }
                                                }
                                                break;
                                            case 'task_list_lasers':
                                                if (!isset($row->description)) {
                                                    throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                }
                                                $uuid = $this->gen_uuid();
                                                $entity->setCode($uuid);
                                                $entity->setDescription($row->description);
                                                $entity->setProduction($row->production);
                                                $entity->setPlannedstart(new \DateTime($row->plannedstart));
                                                $entity->setClient1($row->client1);
                                                $entity->setClient2($row->client2);
                                                if (isset($row->detail)) {
                                                    for ($d = 0;$d < count($row->detail);$d++) {
                                                        $row_d = $row->detail[$d];
                                                        $proc_d = '';
                                                        $repo_d = $this->getDoctrine()
                                                            ->getRepository('App:TaskListLaserDetail');
                                                        $entity_d = $repo_d->findOneBy(array('opname' => $row_d->opname,'erprefnumber' => $row_d->erprefnumber));
                                                        if ($entity_d !== null) {
                                                            if (is_callable(array($entity_d, "setUpdateuserId"))) {
                                                                $entity_d->setUpdateuserId($userId);
                                                            }
                                                            if ($entity_d->getLeafmask() !== $row_d->leafmask || $entity_d->getProductcount() !== $row_d->leafmask * $row->production) {
                                                                $proc_d = 'U';
                                                            }
                                                        } else {
                                                            $entity_d = new TaskListLaserDetail();
                                                            $entity_d->setCreateuserId($userId);
                                                            $proc_d = 'I';
                                                        }
                                                        if ($proc_d != '') {
                                                            if (!isset($row_d->erprefnumber)) {
                                                                throw new \Error('Dosya içinde içi boş kayıt var. İşlem yapılamıyor.');
                                                            }
                                                            $entity_d->setTasklistlaser($uuid);
                                                            $entity_d->setOpcode($row_d->opcode);
                                                            $entity_d->setOpnumber($row_d->opnumber);
                                                            $entity_d->setOpname($row_d->opname);
                                                            $entity_d->setErprefnumber($row_d->erprefnumber);
                                                            $entity_d->setLeafmask($row_d->leafmask);
                                                            $entity_d->setProductcount($row_d->leafmask * $row->production);
                                                            $em->persist($entity_d);
                                                        }
                                                    }
                                                }
                                                break;
                                            case 'task_reject_details':
                                                $entity->setType($row->type);
                                                $entity->setClient($row->client);
                                                $entity->setDay(new \DateTime($row->day));
                                                $entity->setJobrotation($row->jobrotation);
                                                $entity->setRejecttype($row->rejecttype);
                                                $entity->setMaterialunit("Adet");
                                                $entity->setTime(new \DateTime($row->time));
                                                $entity->setOpcode($row->opcode);
                                                $entity->setOpnumber($row->opnumber);
                                                $entity->setOpname($row->opname);
                                                $entity->setQuantityReject(0);
                                                $entity->setQuantityRetouch(0);
                                                $entity->setQuantityScrap($row->quantityscrap);
                                                $entity->setErprefnumber($row->erprefnumber);
                                                $entity->setIsexported(true);
                                                if (isset($row->importfilename)) {
                                                    $entity->setImportfilename($row->importfilename);
                                                }
                                                break;
                                        }
                                        if ($table == 'kanban_operations' && $proc == 'U') {
                                            $entities = $repo->findBy($arr_where);
                                            foreach ($entities as $entity) {
                                                $dirty = true;
                                                $entity->setCurrentboxcount($row->currentboxcount);
                                                $entity->setPackaging($row->packaging);
                                                if ($row->boxcount && intval($row->boxcount) > 0) {
                                                    $entity->setBoxcount($row->boxcount);
                                                }
                                                if ($row->minboxcount && intval($row->minboxcount) > 0) {
                                                    $entity->setMinboxcount($row->minboxcount);
                                                }
                                                if ($row->maxboxcount && intval($row->maxboxcount) > 0) {
                                                    $entity->setMaxboxcount($row->maxboxcount);
                                                }
                                                $em->persist($entity);
                                            }
                                        } else {
                                            $em->persist($entity);
                                            $dirty = true;
                                        }
                                        if (($i % $batchSize) === 0) {
                                            $em->flush();
                                            $em->clear();
                                        }
                                        $_row = $row;
                                    } else {
                                        if ($dirty) {
                                            $em->flush();
                                            $em->clear();
                                            $dirty = false;
                                        } else {
                                            $em->clear();
                                        }
                                    }
                                } else {
                                    if ($row->idx !== '') {
                                        $sql_select = "select tp.* ,pt.description 
                                        from task_plans tp 
                                        left join product_trees pt on pt.name=tp.opname and pt.materialtype='O' and pt.finish is null and pt.delete_user_id is null
                                        where tp.idx=:idx";
                                        $stmt_select = $conn->prepare($sql_select);
                                        $stmt_select->bindValue('idx', $row->idx);
                                        $stmt_select->execute();
                                        $records_tp = $stmt_select->fetchAll();
                                        if (count($records_tp) > 0) {
                                            foreach ($records_tp as $row_tp) {
                                                if (!isset($row->status)) {
                                                    $row->status = $row_tp->status;
                                                }
                                                //statu P olanlar için task_lists kaydı oluşturulmayacak
                                                if ($row->status !== 'P') {
                                                    $sql_update = "update task_plans 
                                                        set client=:client,plannedstart=:plannedstart,plannedfinish=:plannedfinish
                                                        where idx=:idx and id=:id";
                                                    $stmt_update = $conn->prepare($sql_update);
                                                    $stmt_update->bindValue('client', $row->client);
                                                    $stmt_update->bindValue('plannedstart', $row->plannedstart);
                                                    $stmt_update->bindValue('plannedfinish', $row->plannedfinish);
                                                    //2021-12-13 statü değeri planet tarafından değiştiriliyor
                                                    //,status=:status
                                                    //$stmt_update->bindValue('status', $row->status);
                                                    $stmt_update->bindValue('idx', $row->idx);
                                                    $stmt_update->bindValue('id', $row_tp["id"]);
                                                    $stmt_update->execute();
                                                    $sql_update = "update task_plans 
                                                        set status=:status
                                                        where idx=:idx and id=:id and status<>'Devam Eden'";
                                                    $stmt_update = $conn->prepare($sql_update);
                                                    $stmt_update->bindValue('status', $row->status);
                                                    $stmt_update->bindValue('idx', $row->idx);
                                                    $stmt_update->bindValue('id', $row_tp["id"]);
                                                    $stmt_update->execute();
                                                    if ($row->employees && $row->status !== 'DEL') {
                                                        for ($emp = 0;$emp < count($row->employees);$emp++) {
                                                            $row_emp = $row->employees[$emp];
                                                            $sql_insert = "insert into task_plan_employees 
                                                            (idx,employee,client,opcode,opnumber,opname,erprefnumber,plannedstart,plannedfinish)
                                                            values (:idx,:employee,:client,:opcode,:opnumber,:opname,:erprefnumber,:plannedstart,:plannedfinish)";
                                                            $stmt_insert = $conn->prepare($sql_insert);
                                                            $stmt_insert->bindValue('idx', $row->idx);
                                                            $stmt_insert->bindValue('employee', $row_emp->code);
                                                            $stmt_insert->bindValue('client', $row->client);
                                                            $stmt_insert->bindValue('opcode', $row_tp["opcode"]);
                                                            $stmt_insert->bindValue('opnumber', $row_tp["opnumber"]);
                                                            $stmt_insert->bindValue('opname', $row_tp["opname"]);
                                                            $stmt_insert->bindValue('erprefnumber', $row_tp["erprefnumber"]);
                                                            $stmt_insert->bindValue('plannedstart', $row_emp->plannedstart);
                                                            $stmt_insert->bindValue('plannedfinish', $row_emp->plannedfinish);
                                                            $stmt_insert->execute();
                                                        }
                                                    }
                                                    if ($row->status !== 'DEL') {
                                                        $repo_tl = $this->getDoctrine()->getRepository('App:TaskList');
                                                        $entity_tl = $repo_tl->findOneBy(array('code' => $row_tp["code"],'erprefnumber' => $row_tp["erprefnumber"], 'opcode' => $row_tp["opcode"], 'opnumber' => $row_tp["opnumber"],'deleted' => null));
                                                        if ($entity_tl !== null) {
                                                            if ((
                                                                $entity_tl->getProductcount() != intval($row_tp["productcount"])
                                                                || $entity_tl->getClient() != $row->client
                                                                || $entity_tl->getDeadline() != new \DateTime($row_tp["deadline"])
                                                                || $entity_tl->getPlannedstart() != new \DateTime($row->plannedstart)
                                                                || $entity_tl->getPlannedfinish() != new \DateTime($row->plannedfinish)
                                                                || $entity_tl->getIdx() != $row->idx
                                                                || $entity_tl->getStatus() != $row->status
                                                            )
                                                            ) {
                                                                if ($entity_tl->getStart() == null && $entity_tl->getProductdonecount() == 0) {
                                                                    $entity_tl->setProductcount($row_tp["productcount"]);
                                                                    $entity_tl->setTpp($row_tp["tpp"]);
                                                                    $entity_tl->setIdx($row->idx);
                                                                }
                                                                $entity_tl->setClient($row->client);
                                                                $entity_tl->setStatus($row->status);
                                                                $entity_tl->setDeadline(new \DateTime($row_tp["deadline"]));
                                                                $entity_tl->setPlannedstart(new \DateTime($row->plannedstart));
                                                                $entity_tl->setPlannedfinish(new \DateTime($row->plannedfinish));
                                                                $em->persist($entity_tl);
                                                                $em->flush();
                                                                $em->clear();
                                                            }
                                                        } else {
                                                            $entity_tl = new TaskList();
                                                            $entity_tl->setCode($row_tp["code"]);
                                                            $entity_tl->setIdx($row->idx);
                                                            $entity_tl->setStatus($row->status);
                                                            $entity_tl->setCreateuserId($userId);
                                                            $entity_tl->setErprefnumber($row_tp["erprefnumber"]);
                                                            $entity_tl->setOpcode($row_tp["opcode"]);
                                                            $entity_tl->setOpnumber($row_tp["opnumber"]);
                                                            $entity_tl->setOpname($row_tp["opname"]);
                                                            $entity_tl->setOpdescription($row_tp["description"]);
                                                            $entity_tl->setProductcount($row_tp["productcount"]);
                                                            $entity_tl->setDeadline(new \DateTime($row_tp["deadline"]));
                                                            $entity_tl->setPlannedstart(new \DateTime($row->plannedstart));
                                                            $entity_tl->setPlannedfinish(new \DateTime($row->plannedfinish));
                                                            $entity_tl->setClient($row->client);
                                                            $entity_tl->setType('quantity');
                                                            $entity_tl->setTpp($row_tp["tpp"]);
                                                            $entity_tl->setTaskfromerp(true);
                                                            $em->persist($entity_tl);
                                                            $em->flush();
                                                            $em->clear();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if ($dirty) {
                                $em->flush();
                                $em->clear();
                                $dirty = false;
                            } else {
                                $em->clear();
                            }
                            $conn->commit();
                        } catch (\Exception $e) {
                            // Rollback the failed transaction attempt
                            $conn->rollback();
                            $resp['success'] = false;
                            $resp['message'] = $e->getMessage()." \n".(isset($_row) ? " Parent Record:".json_encode($_row) : "");
                            $resp['arr_err'] = json_encode($_arr_err);
                            $statusCode = 400;
                        } catch (\Error $er) {
                            $conn->rollback();
                            $resp['success'] = false;
                            $resp['message'] = $er->getMessage()." \n".(isset($_row) ? " Parent Record:".json_encode($_row) : "");
                            $resp['arr_err'] = json_encode($_arr_err);
                            $statusCode = 400;
                        }
                    }
                }
                if ($table === 'documents') {
                    if (isset($content->deleted) && count($content->deleted) > 0) {
                        $em = $this->getDoctrine()->getManager();
                        $conn = $em->getConnection();
                        $conn->beginTransaction();
                        try {
                            for ($i = 0;$i < count($content->deleted);$i++) {
                                $row = $content->deleted[$i];
                                /* @var $repo Document */
                                $repo = $this->getDoctrine()
                                ->getRepository('App:Document')->findBy(array(
                                    'type' => $row->type
                                    ,'opname' => $row->opname
                                ));
                                foreach ($repo as $entity) {
                                    if ($entity->getDeleteuserId() == null) {
                                        if (is_callable(array($entity, "setUpdateuserId"))) {
                                            $entity->setUpdateuserId($userId);
                                        }
                                        if (is_callable(array($entity, "setDeleteuserId"))) {
                                            $entity->setDeleteuserId($userId);
                                        }
                                        $em->persist($entity);
                                        $em->flush();
                                        $em->remove($entity);
                                        $em->flush();
                                    }
                                }
                            }
                            $conn->commit();
                        } catch (\Exception $e) {
                            // Rollback the failed transaction attempt
                            $conn->rollback();
                            $resp['success'] = false;
                            $resp['message'] = $e->getMessage();
                            $resp['arr_err'] = json_encode($_arr_err);
                            $statusCode = 400;
                        }
                    }
                }
                if ($table === 'task_plans') {
                    //planlama yazılımından gelen dosya sonrasında task_plans tablosunda idx boş olan kayıt var ise
                    //bu iş için devam etmeyen bir tasklist kaydı varsa silinecek
                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();
                    $conn->beginTransaction();
                    try {
                        //$sql='SELECT tl.code,tl.record_id
                        //FROM task_plans tp
                        //left join task_lists tl on tl.code=tp.code
                        //left join (SELECT client,erprefnumber,opname
                        //    from client_production_details
                        //    where type in (\'c_p\',\'c_p_confirm\') and finish is null and day>current_date - interval \'3\' day
                        //    group by client,erprefnumber,opname) cpd on cpd.client=tl.client and cpd.erprefnumber=tl.erprefnumber and cpd.opname=tl.opname
                        //where (coalesce(tp.idx,\'\')=\'\' or tp.idx<>tl.idx) and tl.start is null and cpd.client is null';
                        /*$sql="SELECT tla.* from task_lists_audit tla left join task_lists tl on tl.code=tla.code where tla.revtype='DEL' and tla.deadline>'2020-08-01' and tla.id between 65474 and 66481 and tl.id is null order by tla.id";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $records = $stmt->fetchAll();
                        foreach ($records as $row_tp) {
                            $entity_tl = new TaskList();
                            $entity_tl->setCode($row_tp["code"]);
                            $entity_tl->setIdx($row_tp["idx"]);
                            $entity_tl->setStatus($row_tp["status"]);
                            $entity_tl->setCreateuserId($userId);
                            $entity_tl->setErprefnumber($row_tp["erprefnumber"]);
                            $entity_tl->setOpcode($row_tp["opcode"]);
                            $entity_tl->setOpnumber($row_tp["opnumber"]);
                            $entity_tl->setOpname($row_tp["opname"]);
                            $entity_tl->setOpdescription($row_tp["description"]);
                            $entity_tl->setProductcount($row_tp["productcount"]);
                            $entity_tl->setDeadline(new \DateTime($row_tp["deadline"]));
                            $entity_tl->setPlannedstart(new \DateTime($row_tp["plannedstart"]));
                            $entity_tl->setPlannedfinish(new \DateTime($row_tp["plannedfinish"]));
                            $entity_tl->setClient($row_tp["client"]);
                            $entity_tl->setType('quantity');
                            $entity_tl->setTpp($row_tp["tpp"]);
                            $entity_tl->setTaskfromerp(true);
                            $em->persist($entity_tl);
                            $em->flush();
                            $em->clear();
                        }*/
                        $sql = "SELECT tl.code,tl.record_id 
                        FROM task_plans tp
                        join task_lists tl on tl.code=tp.code
                        left join (SELECT client,erprefnumber,opname 
                            from client_production_details 
                            where type in ('c_p','c_p_confirm') and finish is null and day>current_date - interval '3' day 
                            group by client,erprefnumber,opname) cpd on cpd.client=tl.client and cpd.erprefnumber=tl.erprefnumber and cpd.opname=tl.opname
                        where tl.start is null and tl.finish is null and cpd.client is null and (coalesce(tp.idx,'')='' or tp.status='DEL' or (tp.status='P' and tp.plannedstart='0001-01-01 00:00:00')) ";
                        //@@todo:planet tarafı konveyör işlerinin geri dönüşündeki idx sorununu çözünce aşağıdaki koşul devreye alınacak
                        //(coalesce(tp.idx,'')='' or tp.status=\'DEL\' or tp.plannedstart is null)
                        //$sql="select * from task_lists where idx='a2800192-d1c5-43ec-e8ce-1f63c9cc9cb4' and start is null";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $records = $stmt->fetchAll();
                        if (count($records) > 0) {
                            for ($i = 0;$i < count($records);$i++) {
                                $row = $records[$i];
                                /* @var $repo TaskList */
                                $repo = $this->getDoctrine()
                                ->getRepository('App:TaskList');
                                $entity = $repo->findOneBy(array('code' => $row["code"], 'recordId' => $row["record_id"]));
                                if ($entity !== null) {
                                    if (is_callable(array($entity, "setUpdateuserId"))) {
                                        $entity->setUpdateuserId($userId);
                                    }
                                    if (is_callable(array($entity, "setDeleteuserId"))) {
                                        $entity->setDeleteuserId($userId);
                                    }
                                    $entity->setFinish(new \DateTime());
                                    $entity->setTfddescription('import_delete_plan');
                                    $em->persist($entity);
                                    $em->flush();
                                    $em->remove($entity);
                                    $em->flush();
                                    if (($i % $batchSize) === 0) {
                                        $em->flush();
                                        $em->clear();
                                    }
                                }
                            }
                            $em->flush();
                            $em->clear();
                        }
                        $conn->commit();
                    } catch (\Exception $e) {
                        // Rollback the failed transaction attempt
                        $conn->rollback();
                        $resp['success'] = false;
                        $resp['message'] = $e->getMessage();
                        $resp['arr_err'] = json_encode($_arr_err);
                        $statusCode = 400;
                    }
                }
                if ($table === 'product_trees') {
                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();
                    $conn->beginTransaction();
                    try {
                        $sql = 'SELECT po.parent code,max(po.description)description,\'M\' materialtype,po.parent "name",\'2015-01-01 00:00:00\' "start"
                                from product_trees po 
                                left join product_trees pm on pm.code=po.parent and pm.parent is null and pm.materialtype in (\'M\',\'Y\')
                                where po.materialtype=\'O\' and pm.id is null
                                GROUP BY po.parent';
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $records = $stmt->fetchAll();
                        if (count($records) > 0) {
                            for ($i = 0;$i < count($records);$i++) {
                                $row = $records[$i];
                                $entity = $this->getNewEntity();
                                $entity->setCreateuserId($userId);
                                $entity->setCode($row["code"]);
                                $entity->setDescription($row["description"]);
                                $entity->setMaterialtype($row["materialtype"]);
                                $entity->setName($row["name"]);
                                $entity->setStart(new \DateTime($row["start"]));
                                $em->persist($entity);
                                if (($i % $batchSize) === 0) {
                                    $em->flush();
                                    $em->clear();
                                }
                            }
                            $em->flush();
                            $em->clear();
                        }
                        $conn->commit();
                    } catch (\Exception $e) {
                        // Rollback the failed transaction attempt
                        $conn->rollback();
                        $resp['success'] = false;
                        $resp['message'] = $e->getMessage();
                        $resp['arr_err'] = json_encode($_arr_err);
                        $statusCode = 400;
                    }
                }
                if ($table === 'task_lists' && $customer !== 'oskim') {
                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();
                    $conn->beginTransaction();
                    try {
                        $sql = 'SELECT code,record_id 
                            from task_lists 
                            where finish is null and productdonecount<productcount and taskfromerp=true and deadline<current_date - interval \'360 day\' 
                            order by deadline';
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $records = $stmt->fetchAll();
                        if (count($records) > 0) {
                            for ($i = 0;$i < count($records);$i++) {
                                $row = $records[$i];
                                /* @var $repo TaskList */
                                $repo = $this->getDoctrine()
                                ->getRepository('App:TaskList');
                                $entity = $repo->findOneBy(array('code' => $row["code"], 'recordId' => $row["record_id"]));
                                if ($entity !== null) {
                                    if (is_callable(array($entity, "setUpdateuserId"))) {
                                        $entity->setUpdateuserId($userId);
                                    }
                                    if (is_callable(array($entity, "setDeleteuserId"))) {
                                        $entity->setDeleteuserId($userId);
                                    }
                                    $entity->setFinish(new \DateTime());
                                    $entity->setTfddescription('import');
                                    $em->persist($entity);
                                    $em->flush();
                                    $em->remove($entity);
                                    $em->flush();
                                    if (($i % $batchSize) === 0) {
                                        $em->flush();
                                        $em->clear();
                                    }
                                }
                            }
                            $em->flush();
                            $em->clear();
                        }
                        $conn->commit();
                    } catch (\Exception $e) {
                        // Rollback the failed transaction attempt
                        $conn->rollback();
                        $resp['success'] = false;
                        $resp['message'] = $e->getMessage();
                        $resp['arr_err'] = json_encode($_arr_err);
                        $statusCode = 400;
                    }
                }
                if ($table === 'task_imports' || $table === 'task_lists') {
                    //erp'den gelen silinecek işler
                    if (isset($content->deleted) && count($content->deleted) > 0) {
                        $em = $this->getDoctrine()->getManager();
                        $conn = $em->getConnection();
                        $conn->beginTransaction();
                        try {
                            for ($i = 0;$i < count($content->deleted);$i++) {
                                $row = $content->deleted[$i];
                                $sql_ti = "delete from task_imports where erprefnumber=:erprefnumber";
                                $stmt_ti = $conn->prepare($sql_ti);
                                $stmt_ti->bindValue('erprefnumber', $row->erprefnumber);
                                $stmt_ti->execute();
                                $sql_tp = "delete from task_plans where erprefnumber=:erprefnumber";
                                $stmt_tp = $conn->prepare($sql_tp);
                                $stmt_tp->bindValue('erprefnumber', $row->erprefnumber);
                                $stmt_tp->execute();
                                /* @var $repo TaskList */
                                $repo = $this->getDoctrine()
                                ->getRepository('App:TaskList')->findBy(array('erprefnumber' => $row->erprefnumber,'finish' => null,'deleted' => null));
                                foreach ($repo as $entity) {
                                    if ($entity->getDeleteuserId() == null) {
                                        if (is_callable(array($entity, "setUpdateuserId"))) {
                                            $entity->setUpdateuserId($userId);
                                        }
                                        if (is_callable(array($entity, "setDeleteuserId"))) {
                                            $entity->setDeleteuserId($userId);
                                        }
                                        $entity->setFinish(new \DateTime());
                                        $entity->setTfddescription('import_deleted');
                                        $em->persist($entity);
                                        $em->flush();
                                        //if ($customer === 'ermetal') {
                                        $em->remove($entity);
                                        $em->flush();
                                        //}
                                    }
                                }
                            }
                            $em->flush();
                            $em->clear();
                            $conn->commit();
                        } catch (\Exception $e) {
                            // Rollback the failed transaction attempt
                            $conn->rollback();
                            $resp['success'] = false;
                            $resp['message'] = $e->getMessage();
                            $resp['arr_err'] = json_encode($_arr_err);
                            $statusCode = 400;
                        }
                    }
                }
                if ($table === 'task_list_lasers') {
                    //erp'den gelen silinecek işler
                    if (isset($content->deleted) && count($content->deleted) > 0) {
                        $em = $this->getDoctrine()->getManager();
                        $conn = $em->getConnection();
                        $conn->beginTransaction();
                        try {
                            for ($i = 0;$i < count($content->deleted);$i++) {
                                $row = $content->deleted[$i];
                                $sql_tll = "select code from task_list_lasers where finish is null and description=:description";
                                $stmt_tll = $conn->prepare($sql_tll);
                                $stmt_tll->bindValue('description', $row->description);
                                $stmt_tll->execute();
                                $recordsTLL = $stmt_tll->fetchAll();
                                if (count($recordsTLL) !== 0) {
                                    $code = $recordsTLL[0]["code"];
                                    /* @var $repoTLLD TaskListLaserDetail */
                                    $repoTLLD = $this->getDoctrine()
                                    ->getRepository('App:TaskListLaserDetail')->findBy(array('tasklistlaser' => $code,'deleted' => null));
                                    foreach ($repoTLLD as $entity) {
                                        if ($entity->getDeleteuserId() == null) {
                                            if (is_callable(array($entity, "setUpdateuserId"))) {
                                                $entity->setUpdateuserId($userId);
                                            }
                                            if (is_callable(array($entity, "setDeleteuserId"))) {
                                                $entity->setDeleteuserId($userId);
                                            }
                                            //$entity->setFinish(new \DateTime());
                                            //$entity->setTfdescription('import_deleted');
                                            $em->persist($entity);
                                            $em->flush();
                                            //$em->remove($entity);
                                            //$em->flush();
                                        }
                                    }
                                    /* @var $repoTLL TaskListLaser */
                                    $repoTLL = $this->getDoctrine()
                                    ->getRepository('App:TaskListLaser')->findBy(array('code' => $code,'finish' => null,'deleted' => null));
                                    foreach ($repoTLL as $entity) {
                                        if ($entity->getDeleteuserId() == null) {
                                            if (is_callable(array($entity, "setUpdateuserId"))) {
                                                $entity->setUpdateuserId($userId);
                                            }
                                            if (is_callable(array($entity, "setDeleteuserId"))) {
                                                $entity->setDeleteuserId($userId);
                                            }
                                            $entity->setFinish(new \DateTime());
                                            $entity->setTfdescription('import_deleted');
                                            $em->persist($entity);
                                            $em->flush();
                                            //$em->remove($entity);
                                            //$em->flush();
                                        }
                                    }
                                }
                            }
                            $conn->commit();
                        } catch (\Exception $e) {
                            // Rollback the failed transaction attempt
                            $conn->rollback();
                            $resp['success'] = false;
                            $resp['message'] = $e->getMessage();
                            $resp['arr_err'] = json_encode($_arr_err);
                            $statusCode = 400;
                        }
                    }
                }
                if ($table === 'task_reject_details') {
                    if (isset($content->deleted) && count($content->deleted) > 0) {
                        $em = $this->getDoctrine()->getManager();
                        $conn = $em->getConnection();
                        $conn->beginTransaction();
                        try {
                            for ($i = 0;$i < count($content->deleted);$i++) {
                                $row = $content->deleted[$i];
                                /* @var $repo TaskRejectDetail */
                                $repo = $this->getDoctrine()
                                ->getRepository('App:TaskRejectDetail')->findBy(array(
                                    'erprefnumber' => $row->erprefnumber
                                    ,'client' => $row->client
                                    ,'time' => new \DateTime($row->time)
                                    ,'rejecttype' => $row->rejecttype
                                    ,'opname' => $row->opname
                                ));
                                foreach ($repo as $entity) {
                                    if ($entity->getDeleteuserId() == null) {
                                        if (is_callable(array($entity, "setUpdateuserId"))) {
                                            $entity->setUpdateuserId($userId);
                                        }
                                        if (is_callable(array($entity, "setDeleteuserId"))) {
                                            $entity->setDeleteuserId($userId);
                                        }
                                        $em->persist($entity);
                                        $em->flush();
                                        $em->remove($entity);
                                        $em->flush();
                                    }
                                }
                            }
                            $conn->commit();
                        } catch (\Exception $e) {
                            // Rollback the failed transaction attempt
                            $conn->rollback();
                            $resp['success'] = false;
                            $resp['message'] = $e->getMessage();
                            $resp['arr_err'] = json_encode($_arr_err);
                            $statusCode = 400;
                        }
                    }
                }
                if ($table === 'kanban_operations|clear') {
                    $em = $this->getDoctrine()->getManager();
                    $conn = $em->getConnection();
                    $conn->beginTransaction();
                    try {
                        /* @var $repo_ko KanbanOperation */
                        $repo_ko = $this->getDoctrine()
                        ->getRepository('App:KanbanOperation');
                        $entity_ko = $repo_ko->findAll();
                        foreach ($entity_ko as $ko) {
                            $ko->setBoxcount(0);
                            $em->persist($ko);
                            $em->flush();
                        }
                        $conn->commit();
                    } catch (\Exception $e) {
                        // Rollback the failed transaction attempt
                        $conn->rollback();
                        $resp['success'] = false;
                        $resp['message'] = $e->getMessage();
                        $resp['arr_err'] = json_encode($_arr_err);
                        $statusCode = 400;
                    }
                }
            } catch (Exception $e) {
                $resp['success'] = false;
                $resp['message'] = $e->getMessage();
                $resp['arr_err'] = json_encode($_arr_err);
                $statusCode = 400;
            }
        }
        $time_end = microtime(true);
        $resp['duration'] = $time_end - $time_start;
        $resp['arr_err'] = json_encode($_arr_err);
        $resp['message'] = mb_convert_encoding($resp['message'], 'UTF-8', 'UTF-8');
        return new JsonResponse($resp, $statusCode);
    }
}
