<?php

namespace App\Controller;

use App\Entity\OperationAuthorization;
use App\Entity\OperationAuthorizationAnswer;
use App\Entity\OperationAuthorizationHistory;
use Doctrine\ORM\EntityManager;

use Kaitek\Bundle\FrameworkBundle\Controller\BaseController as ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OperationAuthorizationEmployeeController extends ControllerBase{
         
    CONST ENTITY = 'App:OperationAuthorization';

    public function __construct(RequestStack $request,ContainerInterface $container)
    {
        parent::__construct($request,$container);
        $this->_queryType=self::QUERY_TYPE_SQL;
    }

    public function getQBQuery()
    {
        $queries = array();
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb = $qb->select('oae.id,oae.opname,oae.l1,oae.l2,oae.l3,oae.l4,oae.l5,oae.version')
                ->from('App:OperationAuthorization', 'oae')
                ->where('oae.deleteuserId is null')
                ->orderBy('oae.opname', 'ASC');
        $queries['OperationAuthorizationEmployee'] = array('qb' => $qb, 'getAll' => true);
        return $queries;
    }

    public function getSqlStr() {
        $queries = array();
        $_sql = "SELECT SELECT pt.name opname,COALESCE(oa.l1,0)l1,COALESCE(oa.l2,0)l2,COALESCE(oa.l3,0)l3,COALESCE(oa.l4,0)l4,COALESCE(oa.l5,0)l5
            from (SELECT name,description from product_trees where finish is null and materialtype='O' GROUP BY name,description) pt
            left join operation_authorizations oa on oa.opname=pt.name and oa.employee=:employee
            where @@where@@
            order by pt.name";
        $queries['OperationAuthorizationEmployee'] = array('sql' => $_sql, 'getAll' => true);
        return $queries;
    }

    /**
     * @Route(path="/OperationAuthorizationEmployee/getQuestions", name="OperationAuthorizationEmployee-getQuestions", options={"expose"=true}, methods={"POST"})
     */
    public function getQuestionsAction(Request $request, $_locale){
        $cbg=$this->checkBeforeGet($request);
        if($cbg===true){
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $p=$request->request->all();
            $employee = $p["employee"];
            $opname = $p["opname"];
            $sql_a="SELECT masterid from operation_authorization_answers where opname=:opname and employee=:employee GROUP BY masterid order by masterid desc limit 1";
            $stmt_a = $conn->prepare($sql_a);
            $stmt_a->bindValue('opname', $opname);
            $stmt_a->bindValue('employee', $employee);
            $stmt_a->execute();
            $records_a = $stmt_a->fetchAll();
            $w='  ';
            if(count($records_a)>0){
                $q=$records_a[0];
                $w=' and oaa.masterId='.$q['masterid'];
            }
            $sql="SELECT oaq.*, oaa.masterId,oaa.opname,oaa.employee,COALESCE(oaa.answer,0)answer,COALESCE(oaa.description,'')description
                from operation_authorization_questions oaq
                left join operation_authorization_answers oaa on oaa.question=oaq.question and oaa.employee=:employee and oaa.opname=:opname $w
                where oaq.delete_user_id is null and COALESCE(oaq.opname,:opname)=:opname
                order by oaq.listorder";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('opname', $opname);
            $stmt->bindValue('employee', $employee);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $records = array("totalProperty"=>count($data),"records"=>$data);
            return new JsonResponse($records);
        }else{
            return $cbg;
        }
    }
    
    /**
     * @Route(path="/OperationAuthorizationEmployee/setRights", name="OperationAuthorizationEmployee-setRights", options={"expose"=true}, methods={"POST"})
     */
    public function setRightsAction(Request $request, $_locale){
        $cba=$this->checkBeforeAdd($request);
        if($cba===true){
            $p=$request->request->all();
            $employee = $p["employee"];
            $opname = $p["opname"];
            $user = $this->getUser();
            $userId = $user->getId();
            $description = isset($p["description"])?$p["description"]:'';
            $op=$p['op'];
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                if( !isset($p["l5"]) ){
                    $p["l5"]=0;
                }
                if($op=='up'){
                    if( isset($p["l5"])&&intval($p["l5"])==0&&intval($p["l4"])==1 ){
                        $p["l5"]=1;
                    }else{
                        $p["l5"]=intval($p["l5"]);
                    }
                    if( isset($p["l4"])&&intval($p["l4"])==0&&intval($p["l3"])==1 ){
                        $p["l4"]=1;
                    }else{
                        $p["l4"]=intval($p["l4"]);
                    }
                    if( isset($p["l3"])&&intval($p["l3"])==0&&intval($p["l2"])==1 ){
                        $sql_t="SELECT erprefnumber from client_production_details where type=:type and employee=:employee and opname=:opname GROUP BY erprefnumber limit 5";
                        $stmt_t = $conn->prepare($sql_t);
                        $stmt_t->bindValue('type', "e_p");
                        $stmt_t->bindValue('employee', $employee);
                        $stmt_t->bindValue('opname', $opname);
                        $stmt_t->execute();
                        $records_t = $stmt_t->fetchAll();
                        if(count($records_t)<3){
                            $message=($this->_container==null?$this->container:$this->_container)->get('translator')->trans('OperationAuthorizationEmployee.errInsufficientTaskCount', array(), 'OperationAuthorizationEmployee');
                            return $this->msgError($message);
                        }
                        $sql_q="SELECT * from operation_authorization_questions where delete_user_id is null limit 1";
                        $stmt_q = $conn->prepare($sql_q);
                        $stmt_q->execute();
                        $records_q = $stmt_q->fetchAll();
                        if(count($records_q)>0){
                            $sql_a="SELECT masterid from operation_authorization_answers where opname=:opname and employee=:employee GROUP BY masterid order by masterid desc limit 1";
                            $stmt_a = $conn->prepare($sql_a);
                            $stmt_a->bindValue('opname', $opname);
                            $stmt_a->bindValue('employee', $employee);
                            $stmt_a->execute();
                            $records_a = $stmt_a->fetchAll();
                            if(count($records_a)>0){
                                $q=$records_a[0];
                                $m_id=$q['masterid'];
                                $sql_b="SELECT oaq.*, oaa.masterId,oaa.opname,oaa.employee,COALESCE(oaa.answer,0)answer,COALESCE(oaa.description,'')description
                                from operation_authorization_questions oaq
                                left join operation_authorization_answers oaa on oaa.question=oaq.question and oaa.employee=:employee and oaa.opname=:opname and oaa.masterid=:masterid
                                where oaq.delete_user_id is null and COALESCE(oaq.opname,:opname)=:opname and oaa.answer=0
                                order by oaq.listorder";
                                $stmt_b = $conn->prepare($sql_b);
                                $stmt_b->bindValue('opname', $opname);
                                $stmt_b->bindValue('employee', $employee);
                                $stmt_b->bindValue('masterid', $m_id);
                                $stmt_b->execute();
                                $records_b = $stmt_b->fetchAll();
                                if(count($records_b)>0){
                                    return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('OperationAuthorizationEmployee.errQuestionNotProper', array(), 'OperationAuthorizationEmployee') );
                                }
                            }else{
                                return $this->msgError( ($this->_container==null?$this->container:$this->_container)->get('translator')->trans('OperationAuthorizationEmployee.errQuestionsNotAnswered', array(), 'OperationAuthorizationEmployee') );
                            }
                        }
                        $p["l3"]=1;
                    }else{
                        $p["l3"]=intval($p["l3"]);
                    }
                    if( isset($p["l2"])&&intval($p["l2"])==0&&intval($p["l1"])==1 ){
                        $sql_t="SELECT erprefnumber from client_production_details where type=:type and employee=:employee and opname=:opname GROUP BY erprefnumber limit 5";
                        $stmt_t = $conn->prepare($sql_t);
                        $stmt_t->bindValue('type', "e_p");
                        $stmt_t->bindValue('employee', $employee);
                        $stmt_t->bindValue('opname', $opname);
                        $stmt_t->execute();
                        $records_t = $stmt_t->fetchAll();
                        if(count($records_t)<3){
                            $message=($this->_container==null?$this->container:$this->_container)->get('translator')->trans('OperationAuthorizationEmployee.errInsufficientTaskCount', array(), 'OperationAuthorizationEmployee');
                            return $this->msgError($message);
                        }
                        $p["l2"]=1;
                    }else{
                        $p["l2"]=intval($p["l2"]);
                    }
                    if( isset($p["l1"]) ){
                        $p["l1"]=1;
                    }
                }else{
                    if( isset($p["l1"])&&intval($p["l1"])==1&&isset($p["l2"])&&intval($p["l2"])==0 ){
                        $p["l1"]=0;
                    }else{
                        $p["l1"]=intval($p["l1"]);
                    }
                    if( isset($p["l2"])&&intval($p["l2"])==1&&isset($p["l3"])&&intval($p["l3"])==0 ){
                        $p["l2"]=0;
                    }else{
                        $p["l2"]=intval($p["l2"]);
                    }
                    if( isset($p["l3"])&&intval($p["l3"])==1&&isset($p["l4"])&&intval($p["l4"])==0 ){
                        $p["l3"]=0;
                    }else{
                        $p["l3"]=intval($p["l3"]);
                    }
                    if( isset($p["l4"])&&intval($p["l4"])==1&&isset($p["l5"])&&intval($p["l5"])==0 ){
                        $p["l4"]=0;
                    }else{
                        $p["l4"]=intval($p["l4"]);
                    }
                    if(intval($p["l5"])==1){
                        $p["l5"]=0;
                    }
                }
                $entity = $this->getDoctrine()
                    ->getRepository(self::ENTITY)
                    ->findOneBy(array('employee' => $employee,'opname'=>$opname ));
                if($entity){
                    $entity->setUpdateuserId($userId);
                    $entity->setL1($p["l1"]);
                    $entity->setL2($p["l2"]);
                    $entity->setL3($p["l3"]);
                    $entity->setL4($p["l4"]);
                    $entity->setL5($p["l5"]);
                    $entity->setTime(new \Datetime());
                
                    $em->persist($entity);
                    $em->flush();
                }else{
                    $entity = new OperationAuthorization();
                    $entity->setCreateuserId($userId);
                    $entity->setUpdateuserId($userId);
                    $entity->setEmployee($employee);
                    $entity->setOpname($opname);
                    $entity->setL1($p["l1"]);
                    $entity->setL2($p["l2"]);
                    $entity->setL3($p["l3"]);
                    $entity->setL4($p["l4"]);
                    $entity->setL5($p["l5"]);
                    $entity->setTime(new \Datetime());
                
                    $em->persist($entity);
                    $em->flush();
                }
                $entity_h = new OperationAuthorizationHistory();
                $entity_h->setCreateuserId($userId);
                $entity_h->setUpdateuserId($userId);
                $entity_h->setEmployee($employee);
                $entity_h->setOpname($opname);
                $entity_h->setL1($p["l1"]);
                $entity_h->setL2($p["l2"]);
                $entity_h->setL3($p["l3"]);
                $entity_h->setL4($p["l4"]);
                $entity_h->setL5($p["l5"]);
                $entity_h->setTime(new \Datetime());
                $entity_h->setUserid($userId);
                $entity_h->setDescription($description);
                
                $em->persist($entity_h);
                $em->flush();

                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            $records = array("totalProperty"=>0,"records"=>[]);
            return new JsonResponse($records);
        }else{
            return $cba;
        }
    }

    /**
     * @Route(path="/OperationAuthorizationEmployee/setRightsLevelAllOperations", name="OperationAuthorizationEmployee-setRightsLevelAllOperations", options={"expose"=true}, methods={"POST"})
     */
    public function setRightsLevelAllOperationsAction(Request $request, $_locale){
        //$cba=$this->checkBeforeAdd($request);
        //if($cba===true){
            $p=$request->request->all();
            $employee = $p["employee"];
            //$user = $this->getUser();
            $userId = 1;//$user->getId();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $sql="SELECT oa.id,pt.name opname,COALESCE(oa.l1,0)l1,COALESCE(oa.l2,0)l2,COALESCE(oa.l3,0)l3,COALESCE(oa.l4,0)l4,COALESCE(oa.l5,0)l5
                from (SELECT name,description from product_trees where finish is null and materialtype='O' GROUP BY name,description) pt
                left join operation_authorizations oa on oa.opname=pt.name and oa.employee=:employee
                where COALESCE(oa.l3,0)=0
                order by pt.name";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('employee', $employee);
                $stmt->execute();
                $records = $stmt->fetchAll();
                if (count($records)>0) {
                    $i=0;
                    foreach ($records as $row) {
                        if($row['id']==null){
                            $entity = new OperationAuthorization();
                        }else{
                            $entity = $this->getDoctrine()
                            ->getRepository(self::ENTITY)
                            ->find($row['id']);
                        }
                        
                        $entity->setCreateuserId($userId);
                        $entity->setUpdateuserId($userId);
                        $entity->setEmployee($employee);
                        $entity->setOpname($row['opname']);
                        $entity->setL1(1);
                        $entity->setL2(1);
                        $entity->setL3(1);
                        $entity->setL4(0);
                        $entity->setL5(0);
                        $entity->setTime(new \Datetime());
                        $em->persist($entity);
    
                        $entity_h = new OperationAuthorizationHistory();
                        $entity_h->setCreateuserId($userId);
                        $entity_h->setUpdateuserId($userId);
                        $entity_h->setEmployee($employee);
                        $entity_h->setOpname($row['opname']);
                        $entity_h->setL1(1);
                        $entity_h->setL2(1);
                        $entity_h->setL3(1);
                        $entity_h->setL4(0);
                        $entity_h->setL5(0);
                        $entity_h->setTime(new \Datetime());
                        $entity_h->setUserid($userId);
                        $entity_h->setDescription('Tüm operasyonlar yetki 1 verilmesi');
                        $em->persist($entity_h);
                        if($i++>200){
                            $i=0;
                            $em->flush();
                        }
                    }
                    $em->flush();
                    $conn->commit();
                }
            } catch (\Exception $e) {
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            $records = array("totalProperty"=>1,"records"=>array("employee"=>$employee));
            return new JsonResponse($records);
        //}else{
        //    return $cba;
        //}
    }

    /**
     * @Route(path="/OperationAuthorizationEmployee/setAnswers", name="OperationAuthorizationEmployee-setAnswers", options={"expose"=true}, methods={"POST"})
     */
    public function setAnswersAction(Request $request, $_locale){
        $cba=$this->checkBeforeAdd($request);
        if($cba===true){
            $p=$request->request->all();
            $employee = $p["employee"];
            $opname = $p["opname"];
            $user = $this->getUser();
            $userId = $user->getId();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $conn->beginTransaction();
            try {
                $sql_a="SELECT masterid from operation_authorization_answers where opname=:opname and employee=:employee GROUP BY masterid order by masterid desc limit 1";
                $stmt_a = $conn->prepare($sql_a);
                $stmt_a->bindValue('opname', $opname);
                $stmt_a->bindValue('employee', $employee);
                $stmt_a->execute();
                $records_a = $stmt_a->fetchAll();
                if(count($records_a)>0){
                    $masterId=intval($records_a[0]["masterid"])+1;
                }else{
                    $masterId=1;
                }
                foreach($p["answers"] as $item){
                    $entity=new OperationAuthorizationAnswer();
                    $entity->setCreateuserId($userId);
                    $entity->setUpdateuserId($userId);
                    $entity->setMasterId($masterId);
                    $entity->setEmployee($employee);
                    $entity->setOpname($opname);
                    $entity->setQuestion($item['question']);
                    $entity->setAnswer($item['answer']);
                    $entity->setDescription(isset($item['description'])?$item['description']:'');

                    $em->persist($entity);
                    $em->flush();
                }
                $conn->commit();
            } catch (\Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
            $records = array("totalProperty"=>$count,"records"=>$data);
            return new JsonResponse($records);
        }else{
            return $cba;
        }
    }

    /**
     * @Route(path="/OperationAuthorizationEmployee", name="OperationAuthorizationEmployee-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $r=new \StdClass();
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0, //$this instanceof BaseAuditControllerInterface,
                'data' => array('OperationAuthorizationEmployee'=>array("totalProperty"=>1,"records"=>array())),
                'extras' => array()
            );

            return $this->render('Modules/OperationAuthorizationEmployee.html.twig', $data);
        } else {
            return $cbg;
        }
    }
    
    /**
     * @Route(path="/OperationAuthorizationEmployee/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="OperationAuthorizationEmployee-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            //$records = $this->getAllRecords($this, $request, $pg, $lm);
            $p = $request->query->all();
            $_sql = "SELECT pt.name opname,pt.description,COALESCE(oa.l1,0)l1,COALESCE(oa.l2,0)l2,COALESCE(oa.l3,0)l3,COALESCE(oa.l4,0)l4,COALESCE(oa.l5,0)l5
                from (SELECT name,description from product_trees where finish is null and materialtype='O' GROUP BY name,description) pt
                left join operation_authorizations oa on oa.opname=pt.name and oa.employee=:employee
                where 1=1 ".($p['opname']!=''?" and pt.name like :opname ":"")."
                order by pt.name LIMIT :lim";
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $stmt = $conn->prepare($_sql);
            $stmt->bindValue('employee', $p['employee']);
            $stmt->bindValue('lim', $lm);
            if($p['opname']!=''){
                $stmt->bindValue('opname', "%".$p['opname']."%");
            }
            $stmt->execute();
            $data = $stmt->fetchAll();
            $count=count($data);
            $records=array();
            $records['OperationAuthorizationEmployee'] = array("totalProperty"=>$count,"records"=>$data);
            
            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }

    /**
     * @Route(path="/OperationAuthorizationEmployee/checkEmployeeRight", name="OperationAuthorizationEmployee-checkEmployeeRight", options={"expose"=true}, methods={"POST"})
     */
    public function checkEmployeeRightAction(Request $request, $_locale)
    {
        $parameters=json_decode($request->getContent());
        $arr=[];
        foreach($parameters->opname as $op){
            $arr[]=$op;
        }
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql="SELECT pt.name,oauth.id,oauth.employee,oauth.opname,oauth.l1,oauth.l2,oauth.l3,oauth.l4,oauth.l5,oauth.time
        from (SELECT name,description from product_trees where finish is null and materialtype='O' GROUP BY name,description) pt 
        left join operation_authorizations oauth on oauth.opname=pt.name and oauth.employee=:employee 
        where pt.name in ('".implode("','",$parameters->opname)."') 
        order by pt.name";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('employee', $parameters->employee);
        // $stmt->bindValue('materialtype', 'O');
        //$stmt->bindValue('opname', $arr);
        $stmt->execute();
        $records=$stmt->fetchAll();
        return new JsonResponse(array("totalProperty"=>count($records),"records"=>$records));
    }
}