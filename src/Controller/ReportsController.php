<?php
/**
 * Created by PhpStorm.
 * User: oguzhan
 * Date: 24.8.2017
 * Time: 14:33
 * window.open(window.location.origin+window.location.pathname.split("/").splice(0,2).join("/")+'/QB/index.php?report=clientproductiondetail');
 */

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use App\Controller\DcsBaseController as DcsBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Layout;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\Collection\CellsFactory;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Settings;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportsController extends DcsBaseController
{
    public $fieldname_opname;
    public $fieldname_opname_cpd;
    public function __construct(RequestStack $request, ContainerInterface $container)
    {
        parent::__construct($request, $container);
        $this->fieldname_opname = 'coalesce(opname,opdescription)';
        $this->fieldname_opname_cpd = 'coalesce(cpd.opname,cpd.opdescription)';
    }

    /**
     * @Route(path="/Reports/{pg}/{lm}/{table}/{fieldId}/{fieldDisplay}/{val}", requirements={"pg": "\d+","lm": "\d+"}, name="Reports-getComboValues-special", options={"expose"=true}, methods={"GET"})
     */
    public function getComboValuesSpecial(Request $request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val = '', $where = '')
    {
        if ($request->query->get('options') !== '') {
            $options = json_decode($request->query->get('options'));
            if ($options->where === 'M') {
                $_where = " and materialtype in ('M','YM') /*and parent is null*/ and finish is null ";
            }
            if ($options->where === 'O') {
                $_where = " and materialtype in ('O') and parent is not null and finish is null ";
            }
        }
        return parent::getComboValues($request, $_locale, $pg, $lm, $table, $fieldId, $fieldDisplay, $val, $_where);
    }

    /**
     * @Route(path="/m2m/Reports", name="Reports-get", options={"expose"=true}, methods={"POST"})
     */
    public function m2mReportsAction(Request $request, $_locale)
    {
        return $this->reportsAction($request);
    }

    /**
     * @Route(path="/Reports", name="Reports-view", options={"expose"=true}, methods={"GET"})
     */
    public function reportsAction(Request $request)
    {
        //$jsonString=$request->getContent();
        //$parsedRuleGroup = ($this->_container==null?$this->container:$this->_container)->get('fl_qbjs_parser.json_query_parser.doctrine_orm_parser')->parseJsonString($jsonString, ClientProductionDetail::class);
        ///** @var EntityManager $em */
        //$em = $this->getDoctrine()->getManager();
        //$str_where = $parsedRuleGroup->getQueryString();
        //$query = $em->createQuery(implode(explode('WHERE',$str_where)," WHERE (object.type='c_p') AND "));
        //$query->setParameters($parsedRuleGroup->getParameters());
        //$records=$query->getArrayResult();
        //$str_request=urldecode($request->getContent());
        //$str_sql=implode("=",array_splice(explode("=",explode("&",urldecode($str_request))[0]),1));
        //$str_params=implode("=",array_splice(explode("=",explode("&",urldecode($str_request))[1]),1));
        $uri = strtolower($request->getBaseUrl());
        $requestUri = explode('m2m', $request->getRequestUri());
        if (count($requestUri) > 1) {
            $_data = $request->request->all();
            $str_where = $_data["sql"];
            $str_params = $_data["params"];
            $report = $_data["report"];
            $title = $_data["title"];
        } else {
            $str_where = $request->query->get("sql");
            $str_params = $request->query->get("params");
            $report = $request->query->get("report");
            $title = $request->query->get("title");
        }
        $obj_params = json_decode($str_params);
        foreach ($obj_params as $key => $value) {
            if (strpos($key, ".") !== false) {
                $_key = str_replace('.', '_', $key);
                $obj_params->$_key = $value;
                unset($obj_params->$key);
                $str_where = str_replace($key, $_key, $str_where);
            }
        }
        $str_where = $str_where !== '' ? ' AND ' . $str_where : '';
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        switch ($report) {
            case 'client-production-signals':
                $sql='SELECT client "İstasyon",day "Gün",jobrotation "Vardiya",
                    time "Zaman",mould "Kalıp Kodu",mouldgroup "Kalıp Grup Kodu",
                    opsayi "Operatör Sayısı",refsayi "Operasyon Sayısı",losttime "Kayı Süre (Sn)",
                    signalcount "Sinyal Adedi",serialnumber "Kalıp Seri No"
                from client_production_signals 
                where type=\'c_s_g\' and signalcount>0 and ((refsayi=0 ) or (opsayi=0 and losttime=300)) ' . $str_where . '
                order by time';
                break;
            case 'client-product-analyze':
                $sql = 'select cpdx.client "İstasyon",cpdx.day "Gün",cpdx.jobrotation "Vardiya" 
                    ,cpdx.opname "Operasyon",cpdx.opdescription "Açıklama",cpdx.erprefnumber "ERP Ref"
                    ,cpdx.productioncurrent "Üretilen Adet",cpdx.start "Başlangıç",cpdx.finish "Bitiş"
                    ,cpdx.sure "Süre",cpdx.suresn "Süre Sn",cpdx.losttime "Toplam Kayıp Süre"
                    ,cpdx.suresn-cpdx.losttime "Net Çalışma Süresi"
                    ,round(cast(((cpdx.suresn-cpdx.losttime)*cpdx.tpp/cpdx.toplamtpp) as decimal),4) "Ortalama Net Süre"
                    ,round(cast(((cpdx.suresn-cpdx.losttime)*cpdx.tpp/cpdx.toplamtpp)/cpdx.productioncurrent as decimal),4) "Gerçekleşen Birim Süre"
                    ,cpdx.tpp "Ürün Ağacı Süre",cpdx.mould "Kalıp Kodu",cpdx.oncekikalipsoksure "Önceki Kalıp Sökme Süre"
                    ,cpdx.setupsure "Setup-Ayar Süre",cpdx.kalipsoksure "Kalıp Sökme Süre",cpdx.losttime-cpdx.setupsure-cpdx.kalipsoksure "Diğer Kayıplar"
                from (
                    select cpda.client,cpda.day,cpda.jobrotation
                        ,cpda.opname,cpda.opdescription,cpda.erprefnumber
                        ,cpda.productioncurrent,cpda.start,cpda.finish,cpda.finish-cpda.start sure
                        ,extract(epoch from (cpda.finish::timestamp - cpda.start::timestamp)) suresn
                        ,cpda.losttime,cpda.tpp
                        ,case when cpda.client like \'RK%\' then (SELECT m.code 
                            from mould_details md 
                            join mould_groups mg on mg.mould=md.mould and mg.code=md.mouldgroup
                            join moulds m on m.code=md.mould
                            where md.opname = cpda.opname and md.mould not like \'RK%\'
                            limit 1) else cpda.mould end mould
                        /*,(SELECT coalesce((select sum(ks1.kalipsoksure) kalipsoksure from (
                            SELECT sum(( cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) )) kalipsoksure
                            from client_lost_details 
                            where type=\'l_c\' and losttype=\'KALIP SÖKME\' and start>=cpd1.start and finish<=cpd1.finish and client=cpd1.client and day=cpda.day   
                            GROUP BY start,finish,losttype)ks1),0) kalipsoksure
                        from client_production_details cpd1 
                        where cpd1.type=\'c_p\' and cpd1.productioncurrent>0  and cpd1.client=cpda.client and cpd1.start<cpda.start 
                        order by cpd1.start desc limit 1) oncekikalipsoksure*/
                        ,0 oncekikalipsoksure
                        ,coalesce((select sum(ss.setupsure) setupsure from (
                            SELECT sum((cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) )) setupsure
                            from client_lost_details 
                            where type=\'l_c\' and losttype=\'SETUP-AYAR\' and start>=cpda.start and finish<=cpda.finish and client=cpda.client and day=cpda.day  
                            GROUP BY start,finish,losttype)ss),0) setupsure
                        ,coalesce((select sum(ks.kalipsoksure) kalipsoksure from (
                            SELECT sum((cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) )) kalipsoksure
                        from client_lost_details 
                        where type=\'l_c\' and losttype=\'KALIP SÖKME\' and start>=cpda.start and finish<=cpda.finish and client=cpda.client and day=cpda.day 
                        GROUP BY start,finish,losttype)ks),0) kalipsoksure
                        ,(select sum(ptt.tpp)tpp 
                            from client_production_details cpdt 
                            join product_trees ptt on ptt.name=cpdt.opname and ptt.materialtype=\'O\' and ptt.finish is null '.($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(ptt.client,cpdt.client)=cpdt.client " : "").'
                            where cpdt.type=\'c_p\' and cpdt.client=cpda.client and cpdt.day=cpda.day and cpdt.jobrotation=cpda.jobrotation and cpdt.start=cpda.start) toplamtpp
                    from (
                        SELECT cpd.client,cpd.day,cpd.jobrotation
                            ,cpd.opname,cpd.opdescription,cpd.erprefnumber
                            ,cpd.productioncurrent,cpd.start,cpd.finish,cpd.finish-cpd.start sure
                            ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            ,cpd.losttime,pt.tpp,min(cpd.mould)mould
                        from client_production_details cpd
                        left join product_trees pt on pt.name=cpd.opname and pt.materialtype=\'O\' and pt.finish is null '.($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "").'
                        where cpd.type in (\'c_p\',\'c_p_confirm\',\'c_p_out\') and cpd.finish>cpd.start and cpd.productioncurrent>0 
                        and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                        ' . $str_where . ' 
                        group by cpd.client,cpd.day,cpd.jobrotation
                            ,cpd.opname,cpd.opdescription,cpd.erprefnumber
                            ,cpd.productioncurrent,cpd.start,cpd.finish
                            ,cpd.losttime,pt.tpp
                        order by cpd.client, cpd.day, cpd.jobrotation, cpd.start
                    )cpda
                )cpdx';
                break;
            case 'productiondetail-client':
                //$obj_params->type_1 = 'c_p';
                $sql = ' SELECT cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya"
                        ,cpd.opname "Operasyon",cpd.opdescription "Açıklama",cpd.mould "Kalıp",cpd.leafmaskcurrent "Maske",cpd.erprefnumber "ERP Ref"
                        ,cpd.productioncurrent "Üretilen Adet",cpd.start "Başlangıç",cpd.finish "Bitiş",cpd.finish-cpd.start "Süre"
                        ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) "Süre Sn"
                        ,round(cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                        ,cpd.calculatedtpp "Birim Süre"
                        ,pt.tpp "Ürün Ağacı Süre"
                        ,cpd.calculatedtpp*cpd.productioncurrent "Yapilan İş"
                        ,case when cpd.taskfromerp=true then \'Evet\' else \'Hayır\' end "ERP Görevi"
                        ,cpd.losttime "Toplam Kayıp Süre Sn"
                        ,cpd.taskfinishtype "İş Bitirme Tipi"
                    from client_production_details cpd
                    left join product_trees pt on pt.name=cpd.opname and pt.materialtype=\'O\' and pt.finish is null ' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' || $_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and pt.isdefault=true and pt.client=cpd.client " : "") . '
                    where cpd.type in (\'c_p\',\'c_p_confirm\',\'c_p_out\') and cpd.finish>cpd.start and cpd.productioncurrent>0 
                    and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                    ' . $str_where . ' 
                    order by cpd.client, cpd.day, cpd.jobrotation, cpd.start';
                break;
            case 'productiondetail-client-total':
                //$obj_params->type_1 = 'c_p';
                $sql = 'select cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya"
                        ,cpd.mould "Kalıp",cpd.opname "Operasyon",cpd.opdescription "Açıklama",cpd.erprefnumber "ERP Ref"
                        ,sum(cpd.productioncurrent)"Üretilen Adet" 
                        ,cpd.calculatedtpp "Birim Süre"
                        ,cpd.calculatedtpp*sum(cpd.productioncurrent) "Yapilan İş"
                        ,pt.tpp "Birim Süre Ağaç",pt.tpp*sum(cpd.productioncurrent) "Yapilan İş Ağaç"
                    from client_production_details cpd
                    left join product_trees pt on pt.name=cpd.opname and pt.materialtype=\'O\' and pt.finish is null ' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' || $_SERVER['HTTP_HOST'] == '10.0.0.101' ? ' and pt.client=cpd.client ' : '') . ' 
                    where cpd.type in (\'c_p\',\'c_p_confirm\',\'c_p_out\') 
                    and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                    ' . $str_where . '
                    group by cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.opname,cpd.opdescription,cpd.erprefnumber,cpd.calculatedtpp,pt.tpp
                    having sum(cpd.productioncurrent)>0
                    order by cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.opname,cpd.erprefnumber,cpd.calculatedtpp';
                break;
            case 'productiondetail-total':
                //$obj_params->type_1 = 'c_p';
                $sql = 'select cpd.day "Gün",cpd.jobrotation "Vardiya",cpd.opname "Operasyon",cpd.opdescription "Açıklama"
                        ,sum(cpd.productioncurrent)"Üretilen Adet" 
                        ,cpd.calculatedtpp "Birim Süre"
                        ,cpd.calculatedtpp*sum(cpd.productioncurrent) "Yapilan İş"
                    from client_production_details cpd
                    where cpd.type in (\'c_p\',\'c_p_confirm\',\'c_p_out\') 
                    and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                    ' . $str_where . '
                    group by cpd.day,cpd.jobrotation,cpd.opname,cpd.opdescription,cpd.calculatedtpp
                    having sum(cpd.productioncurrent)>0
                    order by cpd.day,cpd.jobrotation,cpd.opname,cpd.calculatedtpp';
                break;
            case 'productiondetail-employee':
                //$obj_params->type_1 = 'e_p';
                $sql = 'SELECT cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya",cpd.opname "Operasyon",cpd.opdescription "Açıklama"
                        ,cpd.erprefnumber "ERP Ref",cpd.employee "Sicil No",e.name "Operatör"
                        ,cpd.productioncurrent "Üretilen Adet",cpd.start "Başlangıç",cpd.finish "Bitiş",cpd.finish-cpd.start "Süre"
                        ,round(cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                        ,case when cpd.taskfromerp=true then \'Evet\' else \'Hayır\' end "ERP Görevi"
                        ,round(COALESCE(cpd.calculatedtpp,pt_o.tpp)) "Birim Süre"
		                ,TO_CHAR(concat(cast(round(cpd.productioncurrent*COALESCE(COALESCE(cpd.calculatedtpp,pt_o.tpp),0)) as varchar), \' second\')::interval, \'HH24:MI:SS\') "Teorik Süre"
		                ,round(cpd.productioncurrent*COALESCE(COALESCE(cpd.calculatedtpp,pt_o.tpp),0)/60) "Teorik Süre Dk"
                    from client_production_details cpd
                    join employees e on e.code = cpd.employee
                    left join product_trees pt_o on pt_o.name=cpd.opname and pt_o.finish is null and pt_o.materialtype=\'O\' '.($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt_o.client,cpd.client)=cpd.client " : "").'
                    where cpd.type in (\'e_p\',\'e_p_confirm\',\'e_p_out\') 
                    and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                    ' . $str_where . ' 
                    order by cpd.client, cpd.day, cpd.jobrotation, cpd.start';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'productiondetail-employee-total':
                //$obj_params->type_1 = 'e_p';
                $sql = 'select cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya",cpd.employee "Sicil No",e.name "Operatör"
                    ,cpd.opname "Operasyon",cpd.opdescription "Açıklama",cpd.erprefnumber "ERP Ref",sum(cpd.productioncurrent)"Üretilen Adet"
                from client_production_details cpd
                join employees e on e.code=cpd.employee
                where cpd.type in (\'e_p\',\'e_p_confirm\',\'e_p_out\') 
                and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                ' . $str_where . ' 
                group by cpd.client,cpd.day,cpd.jobrotation,e.name,cpd.mould,cpd.mouldgroup,cpd.opname,cpd.opdescription,cpd.erprefnumber,cpd.employee
                having sum(cpd.productioncurrent)>0
                order by cpd.client,cpd.day,cpd.jobrotation,e.name,cpd.mould,cpd.mouldgroup,cpd.opname,cpd.erprefnumber,cpd.employee';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'client-work-time':
                $sql = 'select cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya"
                    ,sum(productioncurrent)"Üretilen Adet",round(sum(duration)/60) "Süre (Dk)"
                from (
                    SELECT cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish,sum(cpd.productioncurrent)productioncurrent
                        ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) duration
                    from client_production_details cpd 
                    where cpd.type=\'c_p\' and cpd.productioncurrent>0 
                    and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                    ' . $str_where . ' 
                    GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish
                )cpd
                GROUP BY cpd.client,cpd.day,cpd.jobrotation
                order by cpd.client,cpd.day,cpd.jobrotation';
                break;
            case 'cnc-work-time':
                $sql = 'select cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya"
                    ,cpd.opname "Operasyon",cpd.opdescription "Açıklama",cpd.erprefnumber "ERP Ref"
                    ,cpd.start "Başlangıç",cpd.finish "Bitiş",cpd.finish-cpd.start "Süre"
                    ,round(cpd.suresnis/60) "Toplam Süre"
                    ,round(cpd.losttime/60) "Duruş Süresi"
                    ,round((cpd.suresnis-cpd.losttime)/60) "Çalışma Süresi"
                from (
                    SELECT cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish 
                        ,cpd.opname,cpd.opdescription,cpd.erprefnumber,cpd.losttime
                        ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresnis
                    from client_production_details cpd
                    where cpd.type=\'c_p\' and cpd.client like \'CNC%\' and cpd.finish is not null 
                    ' . $str_where . '
                    GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.opname,cpd.opdescription,cpd.erprefnumber,cpd.losttime,cpd.start,cpd.finish 
                )cpd
                order by cpd.client,cpd.start';
                break;
            case 'cnc-full-time':
                $sql = 'select subq.client,subq.day,subq.jobrotation 
                    ,min(subq.start) wstart,max(subq.finish) wfinish
                    ,sum(round(subq.suresnis/60)) suredkis
                    ,sum(round(subq.suresndurus/60)) suredkdurus
                from 
                (SELECT cnc.client,cnc.type,cnc.day,cnc.jobrotation,cnc.start,cnc.finish 
                    ,cast(extract(epoch from (COALESCE(cnc.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cnc.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresnis,0 suresndurus
                from client_production_details cnc
                where cnc.type=\'c_p\' and cnc.client like \'CNC%\' and cnc.finish is not null 
                ' . $str_where . '
                union all
                SELECT cnc.client,cnc.type,cnc.day,cnc.jobrotation,cnc.start,cnc.finish 
                    ,0 suresnis
                    ,cast(extract(epoch from (COALESCE(cnc.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cnc.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresndurus
                from client_lost_details cnc
                where cnc.type=\'l_c\' and cnc.client like \'CNC%\' and cnc.finish is not null 
                ' . $str_where . '
                ) subq
                group by subq.client,subq.day,subq.jobrotation
                order by min(subq.start)';
                break;
            case 'cnc-full-time-detail':
                $sql = '
                select subq.client "İstasyon",subq.day "Gün",subq.jobrotation "Vardiya"
                    ,subq.opname "Operasyon",subq.opdescription "Açıklama",subq.erprefnumber "ERP Ref"
                    ,subq.losttype "Kayıp Tipi"
                    ,subq.start "Başlangıç",subq.finish "Bitiş",subq.finish-subq.start "Süre"
                    ,round(subq.suresnis/60) "Çalışma Süresi"
                    ,round(subq.suresndurus/60) "Duruş Süresi"
                from 
                (SELECT cnc.client,cnc.type,cnc.day,cnc.jobrotation,cnc.start,cnc.finish 
                    ,cnc.opname,cnc.opdescription,cnc.erprefnumber,\'\' losttype
                    ,cast(extract(epoch from (COALESCE(cnc.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cnc.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresnis,0 suresndurus
                from client_production_details cnc
                where cnc.type=\'c_p\' and cnc.client like \'CNC%\' and cnc.finish is not null 
                ' . $str_where . '
                union all
                SELECT cnc.client,cnc.type,cnc.day,cnc.jobrotation,cnc.start,cnc.finish 
                    ,cnc.opname,cnc.opdescription,cnc.erprefnumber,cnc.losttype,0 suresnis
                    ,cast(extract(epoch from (COALESCE(cnc.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cnc.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresndurus
                from client_lost_details cnc
                where cnc.type=\'l_c\' and cnc.client like \'CNC%\' and cnc.finish is not null 
                ' . $str_where . '
                ) subq
                order by subq.start';
                break;
            case 'employee-efficiency-1':
                $strcount = '1';
                $strtpp = 'coalesce(cpd.calculatedtpp,pt.tpp)';
                //if ($_SERVER['HTTP_HOST']=='10.10.0.10') {
                //    $strcount="(case when cpd.client in ('K419 ','K425','K428') then count(*) else 1 end)";
                //}
                if ($_SERVER['HTTP_HOST'] == '10.0.0.101' || $_SERVER['HTTP_HOST'] == '10.10.0.10') {
                    $strcount = 'count(*)';
                }
                if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                    $strcount = 'count(*)';
                }
                if ($_SERVER['HTTP_HOST'] == '10.0.0.101') {
                    $strtpp = 'coalesce(pt.tpp,cpd.calculatedtpp)';
                }
                $sql = "select b.clientgroupcode \"Hat\",b.client \"İstasyon\"
                    ,case b.type when 'e_p' then 'Verimot' when 'e_p_out' then 'Sistem Dışı' else 'Elle Girilen' end \"Kayıt Tipi\"
                    ,b.day \"Gün\",b.jobrotation \"Vardiya\",b.optype \"Operatör Türü\",b.employee \"Sicil No\",b.name \"Adı\"
                    ,b.opname \"Operasyon\",b.opdescription \"Açıklama\",b.erprefnumber \"ERP Ref\",b.start \"Başlangıç\",b.finish \"Bitiş\"
                    ,b.sure \"Geçen Süre\",b.productioncurrent \"Üretilen Adet\",b.calculatedtpp \"Birim Süre\"
                    ,b.gereklisure \"Gereken Süre\"
                    ,b.mlosttime \"Mola Süresi\",b.losttime \"Kayıp Süre\"
                    ,case when (b.sure - b.mlosttime)>0 then round(cast(100*b.gereklisure/(b.sure-b.mlosttime) as decimal(20,4)),2) else 0 end \"Verim\"
                    ,case when (b.sure - b.mlosttime - b.losttime)>0 then round(cast(100*b.gereklisure/(b.sure-b.mlosttime-b.losttime) as decimal(20,4)),2) else 0 end \"Tempo\"
                    " . ($_SERVER['HTTP_HOST'] === '10.10.0.10' ? ' ,b.isexpert "Usta",b.isoperator "Operatör",b.ismaintenance "Bakım",b.ismould "Kalıp",b.isquality "Kalite" ' : ' ') . "
                from (
                    select cgd.clientgroupcode,a.client,a.type,a.day,a.jobrotation,a.employee,e.name
                        ,concat(case e.isexpert when true then 'Usta,' else null end
                            ,case e.isoperator when true then 'Operatör,' else null end
                            ,case e.ismaintenance when true then 'Bakım,' else null end
                            ,case e.ismould when true then 'Kalıp,' else null end
                            ,case e.isquality when true then 'Kalite' else null end) optype
                        ,e.isexpert,e.isoperator,e.ismaintenance,e.ismould,e.isquality
                        ,a.opname,a.opdescription,a.erprefnumber,a.start,a.finish
                        ,cast(extract(epoch from (COALESCE(a.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(a.start,CURRENT_TIMESTAMP)::timestamp)) as integer) sure
                        ,a.productioncurrent,a.calculatedtpp,round(a.productioncurrent*a.calculatedtpp/a.kalipsayisi) gereklisure
                        ,(SELECT coalesce(sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)),0) mlosttime 
                            from client_lost_details cld 
                            where type in ".($_SERVER['HTTP_HOST'] === '10.10.0.10' && $uri === '/sahince' ? " ('l_e','l_e_t') " : "('l_e')")." and losttype in ('CUMA PAYDOSU','ÇAY MOLASI','YEMEK MOLASI') and finish is not null 
                                and cld.client not in (SELECT code from clients where workflow='CNC')
                                and client=a.client and day=a.day and jobrotation=a.jobrotation and start>=a.start and finish<=a.finish and employee=a.employee)mlosttime
                        ,(SELECT coalesce(sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)),0) losttime 
                            from client_lost_details cld 
                            where type in ".($_SERVER['HTTP_HOST'] === '10.10.0.10' && $uri === '/sahince'  ? " ('l_e','l_e_t') " : "('l_e')")." and losttype not in ('CUMA PAYDOSU','ÇAY MOLASI','YEMEK MOLASI') and finish is not null 
                                and cld.client not in (SELECT code from clients where workflow='CNC')
                                and client=a.client and day=a.day and jobrotation=a.jobrotation and start>=a.start and finish<=a.finish and employee=a.employee)losttime
                    from (
                        select x.client,x.type,x.day,x.jobrotation,x.employee,x.start,max(x.finish)finish
                            ,string_agg(distinct x.opname,',')opname,string_agg(distinct x.opdescription,',')opdescription
                            ,string_agg(distinct x.erprefnumber,',')erprefnumber
                            ,round(sum(x.productioncurrent)/1)productioncurrent
                            ,sum(x.calculatedtpp)calculatedtpp,count(*) kalipsayisi
                        from (
                            select cpd.client,cpd.type,cpd.day,cpd.jobrotation,cpd.employee,cpd.start,max(cpd.finish)finish,cpd.mould
                                ,string_agg(distinct cpd.opname,',')opname,string_agg(distinct cpd.opdescription,',')opdescription
                                ,string_agg(distinct cpd.erprefnumber,',')erprefnumber
                                ,round(sum(cpd.productioncurrent)/count(*)) productioncurrent
                                ,round(sum($strtpp)/$strcount,2) calculatedtpp
                            from client_production_details cpd
                            left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] === '10.10.0.10' && $uri !== '/sahince' || $_SERVER['HTTP_HOST'] === '10.0.0.101' ? 'and pt.client=cpd.client' : ' ') . "
                            where cpd.type in ('e_p','e_p_confirm') and cpd.productioncurrent>0
                            and cpd.client not in (SELECT code from clients where workflow='CNC')
                            " . $str_where . "
                            GROUP BY cpd.client,cpd.type,cpd.day,cpd.jobrotation,cpd.employee,cpd.start,cpd.mould
                            union all 
							select cpd.client,cpd.type,cpd.day,cpd.jobrotation,cpd.employee,cpd.start,max(cpd.finish)finish,cpd.mould
                                ,string_agg(distinct cpd.opname,',')opname,string_agg(distinct cpd.opdescription,',')opdescription
                                ,string_agg(distinct cpd.erprefnumber,',')erprefnumber
                                ,round(sum(cpd.productioncurrent)/count(*)) productioncurrent
                                ,round(sum(coalesce(cpd.calculatedtpp,pt.tpp))/coalesce(nullif((select count(*) from client_production_details cpd_out where cpd_out.client=cpd.client and cpd_out.day=cpd.day and cpd_out.jobrotation=cpd.jobrotation and cpd_out.start=cpd.start),0),1),2) calculatedtpp
                            from client_production_details cpd
                            left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' ".($_SERVER['HTTP_HOST'] === '10.10.0.10' && $uri !== '/sahince' || $_SERVER['HTTP_HOST'] === '10.0.0.101'  ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                            where cpd.type in ('e_p_out') and cpd.productioncurrent>0
                            and cpd.client not in (SELECT code from clients where workflow='CNC')
                            " . $str_where . "
                            GROUP BY cpd.client,cpd.type,cpd.day,cpd.jobrotation,cpd.employee,cpd.start,cpd.mould
                        )x
                        GROUP BY x.client,x.type,x.day,x.jobrotation,x.employee,x.start
                    )a
                    left join employees e on e.code=a.employee
                    left join client_group_details cgd on cgd.clientgroup='watch' and cgd.client=a.client
                    where 
                        " . (($_SERVER['HTTP_HOST'] == '10.0.0.101' || $_SERVER['HTTP_HOST'] == '10.10.0.10') ? ' 1=1 ' : ' e.isexpert=false and e.ismould=false and e.isquality=false and e.ismaintenance=false ') . "
                        
                )b
                where b.sure>0
                order by b.name,b.start";
                //" . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . "
                /*
                select cpd.client,cpd.day,cpd.jobrotation,cpd.employee,cpd.start,max(cpd.finish)finish
                            ,string_agg(distinct cpd.opname,',')opname,string_agg(distinct cpd.opdescription,',')opdescription
                            ,string_agg(distinct cpd.erprefnumber,',')erprefnumber
                            ,round(sum(cpd.productioncurrent)/count(*))productioncurrent
                            ,round(sum($strtpp)/$strcount,2)calculatedtpp
                        from client_production_details cpd
                        left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' ".($_SERVER['HTTP_HOST']==='10.10.0.10'?'and pt.client=cpd.client':' ')."
                        where cpd.type in ('e_p','e_p_confirm','e_p_out')
                        and cpd.client not in (SELECT code from clients where workflow='CNC')
                        ".$str_where."
                        GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.employee,cpd.start
                        having sum(cpd.productioncurrent)>0
                */
                break;
            case 'lostdetail-client':
                $obj_params->type_1 = 'l_c';
                $sql = 'SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya",cld.losttype "Kayıp Tipi"
                    ,cld.descriptionlost "Açıklama",cld.faultdevice "Ekipman"
                    ,cld.start "Başlangıç",cld.finish "Bitiş",cld.finish-cld.start "Süre"
                    ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                    from client_lost_details cld
                    where cld.type = :type_1 /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/ ' . $str_where . ' 
                    order by cld.client, cld.day, cld.jobrotation, cld.start
                ';
                break;
            case 'lostdetail-client-group':
                $obj_params->type_1 = 'l_c';
                $sql = 'SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya",cld.losttype "Kayıp Tipi"
                    ,cld.descriptionlost "Açıklama",cld.faultdevice "Ekipman"
                    ,cld.start "Başlangıç",cld.finish "Bitiş",cld.finish-cld.start "Süre"
                    ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                        ,lgd.lostgroupcode "Kayıp Grubu"
                    from client_lost_details cld
                    join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup=\'lostgroup1\'
                    where cld.type = :type_1 /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/ ' . $str_where . ' 
                    order by cld.client, cld.day, cld.jobrotation, cld.start
                ';
                break;
            case 'lostdetail-client-total':
                $obj_params->type_1 = 'l_c';
                $sql = 'SELECT a.client "İstasyon",a.day "Gün",a.jobrotation "Vardiya",a.losttype "Kayıp Tipi",sum(a.sure) "Süre Dk" 
                from (
                    SELECT cld.client,cld.day,cld.jobrotation,cld.losttype                    
                    ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) sure
                    from client_lost_details cld
                    where cld.type = :type_1 /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/ ' . $str_where . ' 
                )a
                group by a.client,a.day,a.jobrotation,a.losttype
                order by a.client,a.day,a.jobrotation,a.losttype
                ';
                break;
            case 'lostdetail-client-task':
                $obj_params->type_1 = 'l_c_t';
                $sql = 'SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya"
                    ,cld.mould "Kalıp",cld.opname "Operasyon",cld.opdescription "Açıklama",cld.erprefnumber "ERP Ref"
                    ,cld.losttype "Kayıp Tipi"
                    ,cld.start "Başlangıç",cld.finish "Bitiş",cld.finish-cld.start "Süre"
                    ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                    from client_lost_details cld
                    where cld.type = :type_1 /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/
                    ' . $str_where . ' 
                    order by cld.client, cld.day, cld.jobrotation, cld.start
                ';
                break;
            case 'lostdetail-employee':
                //$obj_params->type_1 = 'l_e';
                $sql = 'SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya",cld.losttype "Kayıp Tipi"
                    ,cld.descriptionlost "Açıklama",cld.employee "Sicil No",e.name "Operatör"
                    ,cld.start "Başlangıç",cld.finish "Bitiş",cld.finish-cld.start "Süre"
                    ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                    ,lgd.lostgroupcode "Kayıp Grubu"
                from client_lost_details cld
                join employees e on e.code=cld.employee
                left join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup=\'OPVERIM\'
                where cld.type in (\'l_e\',\'l_e_out\') 
                    ' . (($_SERVER['HTTP_HOST'] == '172.16.1.149') ? ' and e.isoperator=true ' : '') . ' 
                    /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/
                    ' . $str_where . ' 
                order by cld.client, cld.day, cld.jobrotation, cld.start
                ';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                $sql = 'SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya"
                    ,string_agg( cld.erprefnumber,\'|\' order by cld.opname) "Erp Ref"
		            ,string_agg( cld.opname,\'|\' order by cld.opname) "Operasyon"
                    ,cld.losttype "Kayıp Tipi"
                    ,max(cld.descriptionlost) "Açıklama",cld.employee "Sicil No",e.name "Operatör"
                    ,cld.start "Başlangıç",cld.finish "Bitiş",cld.finish-cld.start "Süre"
                    ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                    ,lgd.lostgroupcode "Kayıp Grubu"
                from client_lost_details cld
                join employees e on e.code=cld.employee
                left join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup=\'OPVERIM\'
                where cld.type in (\'l_e\',\'l_e_out\',\'l_e_t\',\'l_e_t_out\')  
                    ' . (($_SERVER['HTTP_HOST'] == '172.16.1.149') ? ' and e.isoperator=true ' : '') . ' 
                    /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/
                    ' . $str_where . ' 
                GROUP BY cld.client,cld.day,cld.jobrotation,cld.losttype,cld.employee,e.name,cld.start,cld.finish,lgd.lostgroupcode
                order by cld.client, cld.day, cld.jobrotation, cld.start';
                break;
            case 'lostdetail-employee-total':
                //$obj_params->type_1 = 'l_e';
                $sql = 'select a.client "İstasyon",a.day "Gün",a.jobrotation "Vardiya",a.employee "Sicil No",a.name "Operatör"
                    ,a.losttype "Kayıp Tipi",sum(a.sure) "Süre Dk"
                from(
                    SELECT cld.client,cld.day,cld.jobrotation,cld.losttype
                        ,cld.employee,e.name
                        ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) sure
                    from client_lost_details cld
                    join employees e on e.code=cld.employee
                    left join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup=\'OPVERIM\'
                    where cld.type in (\'l_e\',\'l_e_out\') 
                    ' . (($_SERVER['HTTP_HOST'] == '172.16.1.149') ? ' and e.isoperator=true ' : '') . ' 
                    /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/
                    ' . $str_where . ' 
                )a
                group by a.client, a.day, a.jobrotation, a.employee,a.name,a.losttype
                order by a.client, a.day, a.jobrotation, a.employee,a.name,a.losttype
                ';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'lostdetail-employee-task':
                //$obj_params->type_1 = 'l_e_t';
                $sql = 'SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya"
                    ,cld.mould "Kalıp",cld.opname "Operasyon",cld.opdescription "Açıklama",cld.erprefnumber "ERP Ref"
                    ,cld.losttype "Kayıp Tipi",cld.descriptionlost "Açıklama"
                    ,cld.employee "Sicil No",e.name "Operatör"
                    ,cld.start "Başlangıç",cld.finish "Bitiş",cld.finish-cld.start "Süre"
                    ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) "Süre Dk"
                    from client_lost_details cld
                    join employees e on e.code=cld.employee
                    where cld.type in (\'l_e\',\'l_e_out\',\'l_e_t\',\'l_e_t_out\')
                        ' . (($_SERVER['HTTP_HOST'] == '172.16.1.149') ? ' and e.isoperator=true ' : '') . ' 
                        /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/
                        ' . $str_where . ' 
                    order by cld.client, cld.day, cld.jobrotation, cld.start
                ';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'task-reject-detail-client':
                $obj_params->type_1 = 't_r';
                $sql = 'SELECT trd.client "İstasyon",trd.day "Gün",trd.jobrotation "Vardiya"
                    ,trd.time "Zaman",trd.erprefnumber "ERP Ref",trd.opname "Operasyon",trd.opdescription "Açıklama"
                    ,trd.rejecttype "Iskarta Tipi",trd.quantityreject "Miktar",trd.quantityretouch "Rötuş",pt.tpp "Birim Süre"
                    ,(case when trd.rejecttype in (\'Ayar Iskartası\') then 0 else trd.quantityreject end)*pt.tpp "Iskarta Süresi"
                    ,trd.importfilename "Tespit Edlien İstasyon",trd.rejectlot "LOT"
                from task_reject_details trd 
                left join product_trees pt on pt.name=trd.opname and pt.materialtype=\'O\' and pt.finish is null ' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=trd.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,trd.client)=trd.client " : "").'
                where trd.type=:type_1 ' . $str_where . ' 
                order by trd.client,trd.day,trd.jobrotation,trd.time ';
                break;
            case 'task-reject-detail-employee':
                $obj_params->type_1 = 'e_r';
                $sql = 'SELECT trd.client "İstasyon",trd.day "Gün",trd.jobrotation "Vardiya"
                    ,trd.time "Zaman",trd.erprefnumber "ERP Ref",trd.opname "Operasyon",trd.opdescription "Açıklama"
                    ,trd.rejecttype "Iskarta Tipi",trd.quantityreject "Miktar",pt.tpp "Birim Süre"
                    ,(case when trd.rejecttype in (\'Ayar Iskartası\') then 0 else trd.quantityreject end)*pt.tpp "Iskarta Süresi"
                    ,trd.employee "Sicil No",e.name "Operatör"
                    ,trd.importfilename "Tespit Edlien İstasyon",trd.rejectlot "LOT"
                from task_reject_details trd 
                left join product_trees pt on pt.name=trd.opname and pt.materialtype=\'O\' and pt.finish is null ' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=trd.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,trd.client)=trd.client " : "").'
                join employees e on e.code=trd.employee
                where trd.type=:type_1 ' . $str_where . ' 
                order by trd.client,trd.day,trd.jobrotation,trd.time ';
                break;
            case 'mh-gec-tasima':
                $t1 = $obj_params->cm_starttime_1;
                $sql_start = "SELECT 
                    concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                from job_rotations 
                where (finish is null or finish<now())
                order by beginval desc limit 1";
                $stmt_start = $conn->prepare($sql_start);
                $stmt_start->bindValue('t1', $obj_params->cm_starttime_1);
                $stmt_start->execute();
                $records_start = $stmt_start->fetchAll();
                $obj_params->cm_starttime_1 = $records_start[0]["jr_start"];

                $sql_finish = "SELECT 
                    concat(cast(case when beginval>endval then :t2::timestamp + INTERVAL '1 day' else :t2::timestamp end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                order by endval desc limit 1";
                $stmt_finish = $conn->prepare($sql_finish);
                $stmt_finish->bindValue('t2', (isset($obj_params->cm_starttime_2) ? $obj_params->cm_starttime_2 : $t1));
                $stmt_finish->execute();
                $records_finish = $stmt_finish->fetchAll();
                $obj_params->cm_starttime_2 = $records_finish[0]["jr_end"];
                $sql = "select cm_1.id,cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici,cm_1.erprefnumber
                    ,concat('E|',cm_1.code) code,concat('E|',cm_1.stockcode) stockcode,cm_1.opno,cm_1.kasa,cm_1.quantitynecessary kasaici
                    ,cm_1.cikis,cm_1.varis,cm_1.baslangic,cm_1.atama,cm_1.bitis
                    ,cm_1.hedef,cm_1.sure,cm_1.suresicil,cm_1.molasuresi
                    ,cm_1.sure-cm_1.molasuresi netsureis
                    ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                    ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                    ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                    ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                    ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                    ,cm_1.changetime as \"Değişim Süresi\"
                    ,cm_1.day AS \"Tarih\",cast(cm_1.start as time) AS \"Oturum Açma\",cast(cm_1.finish as time) AS \"Oturum Kapatma\"
                    ,cm_1.jobrotation AS \"Vardiya\"
                    ,case cm_1.jobrotationteam 
                        when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                        when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                        when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                        when 'Mustafa Kayıkçı' then 'V4 Mustafa KAYIKÇI'
                        when 'Öner Ütücü' then 'V5 Öner ÜTÜCÜ'
                        else cm_1.jobrotationteam end as \"Vardiya Ekibi\"
                from (
                    SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                        ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                        ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                        ,round(extract(epoch from (cm.targettime::timestamp - cm.starttime::timestamp))/60) hedef
                        ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                        ,round(extract(epoch from (cm.actualfinishtime::timestamp - cm.starttime::timestamp))/60) sure
                        ,round(extract(epoch from (cm.actualfinishtime::timestamp - cm.assignmenttime::timestamp))/60) suresicil
                        ,cs.start,cs.finish,cs.day,cs.jobrotation
                        ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                        ,coalesce(cct.changetime,0)changetime
                    from case_movements cm
                    left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                    left join case_change_times cct on cct.casename=cm.casename
                    left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                    where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                        " . $str_where . " 
                )cm_1
                order by cm_1.baslangic
                ";
                break;
            case 'mh-gec-tasima-client':
                $t1 = $obj_params->cm_starttime_1;
                $sql_start = "SELECT 
                    concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                from job_rotations 
                where (finish is null or finish<now())
                order by beginval desc limit 1";
                $stmt_start = $conn->prepare($sql_start);
                $stmt_start->bindValue('t1', $obj_params->cm_starttime_1);
                $stmt_start->execute();
                $records_start = $stmt_start->fetchAll();
                $obj_params->cm_starttime_1 = $records_start[0]["jr_start"];

                $sql_finish = "SELECT 
                    concat(cast(case when beginval>endval then :t2::timestamp + INTERVAL '1 day' else :t2::timestamp end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                order by endval desc limit 1";
                $stmt_finish = $conn->prepare($sql_finish);
                $stmt_finish->bindValue('t2', (isset($obj_params->cm_starttime_2) ? $obj_params->cm_starttime_2 : $t1));
                $stmt_finish->execute();
                $records_finish = $stmt_finish->fetchAll();
                $obj_params->cm_starttime_2 = $records_finish[0]["jr_end"];
                $sql = "select cm_1.id,cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici,cm_1.erprefnumber
                    ,concat('E|',cm_1.code) code,concat('E|',cm_1.stockcode) stockcode,cm_1.opno,cm_1.kasa,cm_1.quantitynecessary kasaici
                    ,cm_1.cikis,cm_1.varis,cm_1.baslangic,cm_1.atama,cm_1.bitis
                    ,cm_1.hedef,cm_1.sure,cm_1.suresicil,cm_1.molasuresi
                    ,cm_1.sure-cm_1.molasuresi netsureis
                    ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                    ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                    ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                    ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                    ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                    ,cm_1.changetime as \"Değişim Süresi\"
                    ,cm_1.day AS \"Tarih\",cast(cm_1.start as time) AS \"Oturum Açma\",cast(cm_1.finish as time) AS \"Oturum Kapatma\"
                    ,cm_1.jobrotation AS \"Vardiya\"
                    ,case cm_1.jobrotationteam 
                        when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                        when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                        when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                        when 'Mustafa Kayıkçı' then 'V4 Mustafa KAYIKÇI'
                        when 'Öner Ütücü' then 'V5 Öner ÜTÜCÜ'
                        else cm_1.jobrotationteam end as \"Vardiya Ekibi\"
                from (
                    SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                        ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                        ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                        ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                        ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                        ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                        ,cs.start,cs.finish,cs.day,cs.jobrotation
                        ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                        ,coalesce(cct.changetime,0)changetime
                    from case_movements cm
                    left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                    left join case_change_times cct on cct.casename=cm.casename
                    left join clients c on c.code=cm.locationsource and c.workflow<>'Gölge Cihaz'
                    where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                        " . $str_where . " 
                )cm_1
                order by cm_1.baslangic
                ";
                break;
            case 'mh-performans':
                $t1 = $obj_params->t1_1;
                $sql_start = "SELECT 
                    concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                from job_rotations 
                where (finish is null or finish<now())
                order by beginval desc limit 1";
                $stmt_start = $conn->prepare($sql_start);
                $stmt_start->bindValue('t1', $obj_params->t1_1);
                $stmt_start->execute();
                $records_start = $stmt_start->fetchAll();
                $obj_params->t1_1 = $records_start[0]["jr_start"];

                $sql_finish = "SELECT 
                    concat(cast(case when beginval>endval then :t2::timestamp + INTERVAL '1 day' else :t2::timestamp end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                order by endval desc limit 1";
                $stmt_finish = $conn->prepare($sql_finish);
                $stmt_finish->bindValue('t2', (isset($obj_params->t1_2) ? $obj_params->t1_2 : $t1));
                $stmt_finish->execute();
                $records_finish = $stmt_finish->fetchAll();
                $obj_params->t1_2 = $records_finish[0]["jr_end"];
                $sql = "select a.tasiyicikod,a.sicil,a.adsoyad,count(*)atanan,sum(a.giden)giden,sum(a.yapilan)yapilan,sum(a.geciken)geciken,sum(a.yapilan)-sum(a.geciken)zamaninda
                ,round(100*(sum(a.giden))/count(*),2) iskaybi
                ,round(100*(sum(a.yapilan)-sum(a.geciken))/count(*),2) verim
                ,round(case when sum(a.yapilan)>0 then 100*(sum(a.yapilan)-sum(a.geciken))/sum(a.yapilan) else 0 end ,2) tempo
                ,a.day AS \"Tarih\",cast(a.start as time) AS \"Oturum Açma\",cast(a.finish as time) AS \"Oturum Kapatma\"
                ,a.jobrotation AS \"Vardiya\"
                ,case a.jobrotationteam 
                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                    when 'Mustafa Kayıkçı' then 'V4 Mustafa KAYIKÇI'
                    when 'Öner Ütücü' then 'V5 Öner ÜTÜCÜ'
                    else a.jobrotationteam end as \"Vardiya Ekibi\"
                from (
                    SELECT cmd.carrier tasiyicikod,cmd.empcode sicil,cmd.empname adsoyad,case when cmd.donetime is null then 0 else 1 end yapilan
                        ,case when cmd.donetime is null then 1 else 0 end giden
                        ,case when cmd.donetime is not null and cm.actualfinishtime-coalesce(cm.breaktimeseconds,0)*interval '1 second'>
                            cm.assignmenttime+(cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer))*interval '1 second' then 1 else 0 end geciken
                        ,cs.start,cs.finish,cs.day,cs.jobrotation
                        ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                    from case_movement_details cmd
                    join case_movements cm on cm.id=cmd.casemovementid
                    left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                    left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                    where c.id is not null and (cmd.canceltime is null or (cmd.canceltime is not null and cmd.description='Oturum kapatma zaman aşımı')) 
                        and cs.day is not null
                        and cm.assignmenttime between :t1_1 and :t1_2
                )a
                GROUP  BY a.tasiyicikod,a.sicil,a.adsoyad,a.start,a.finish,a.day,a.jobrotation,a.jobrotationteam
                having ( sum(a.yapilan)>0 or sum(a.geciken)>0 or (sum(a.yapilan)-sum(a.geciken))>0 )
                order  BY a.tasiyicikod,a.sicil,a.adsoyad,a.day,a.jobrotation,a.jobrotationteam";
                break;
            case 'mh-client-task-cases':
                $sql = 'SELECT tc.id,tc.erprefnumber "ERP Ref",cm.locationdestination "Lokasyon"
                    ,concat(\'E|\',tc.opcode) "Op. Kodu",tc.opnumber "Op. No",concat(\'E|\',tc.stockcode) "Stok Kodu"
                    ,tc.casename "Kasa Tipi"
                    ,case when tc.casetype=\'O\' then \'Boş Kasa\' else \'Hammadde K.\' end "Tip",tc.packcapacity "Kapasite",tc.quantityremaining "Kasa İçi"
                    ,tc.status "Statü",cm.carrier "Taşıyıcı",cm.empcode "Sicil",cm.empname "Personel",cm.actualfinishtime "Geliş Zamanı"
                from task_cases tc
                join case_movements cm on cm.id=tc.movementdetail
                where tc.status not in (\'CLOSE\',\'CLOSED\',\'SYSTEMCLOSE\',\'LABELDELETED\',\'DELIVERED\') 
                    and cm.actualfinishtime>CURRENT_DATE -interval \'3\' day ' . $str_where . ' 
                order by cm.locationdestination,tc.erprefnumber,cm.actualfinishtime';
                break;
            case 'mh-carrier-time':
                $t1 = $obj_params->t1_1;
                $sql_start = "SELECT 
                    concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                from job_rotations 
                where (finish is null or finish<now())
                order by beginval desc limit 1";
                $stmt_start = $conn->prepare($sql_start);
                $stmt_start->bindValue('t1', $obj_params->t1_1);
                $stmt_start->execute();
                $records_start = $stmt_start->fetchAll();
                $obj_params->t1_1 = $records_start[0]["jr_start"];

                $sql_finish = "SELECT 
                    concat(cast(case when beginval>endval then :t2::timestamp + INTERVAL '1 day' else :t2::timestamp end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                order by endval desc limit 1";
                $stmt_finish = $conn->prepare($sql_finish);
                $stmt_finish->bindValue('t2', (isset($obj_params->t1_2) ? $obj_params->t1_2 : $t1));
                $stmt_finish->execute();
                $records_finish = $stmt_finish->fetchAll();
                $obj_params->t1_2 = $records_finish[0]["jr_end"];

                $sql_gun_sayi = "SELECT ceil(EXTRACT(EPOCH FROM :t2::timestamp - :t1::timestamp)/86400) gun_sayi";
                $stmt_gun_sayi = $conn->prepare($sql_gun_sayi);
                $stmt_gun_sayi->bindValue('t1', $obj_params->t1_1);
                $stmt_gun_sayi->bindValue('t2', $obj_params->t1_2);
                $stmt_gun_sayi->execute();
                $records_gun_sayi = $stmt_gun_sayi->fetchAll();
                $gun_sayi = $records_gun_sayi[0]["gun_sayi"];

                $sql_mola_suresi = "select cast(sum(molasuresi)/60 as numeric(10,2)) molasuresi 
                from (
                    select EXTRACT(EPOCH FROM oo.finishtime::interval-oo.starttime::interval)molasuresi
                    from (	
                        select d1.day,trim(to_char(d1.day, 'day')) daystr,t1.hour,o.id,o.clients,o.losttype,o.name,o.repeattype,o.days,o.startday,o.starttime,o.finishday,o.finishtime,COALESCE(o.clearonovertime,false)clearonovertime
                        from (select cast(:d1 as date) + s.t as day from generate_series(0,:gun) as s(t))d1
                        join (select (s.t ||' minute')::interval as hour from generate_series(0,1439) as s(t)) t1 on 1=1
                        join offtimes o on o.finish is null and cast(t1.hour as varchar) between cast(o.starttime as varchar) and cast(o.finishtime as varchar) and strpos(lower(days),trim(to_char(d1.day, 'day')))>0
                    )oo
                    where (oo.day>:d1 or (oo.day=:d1 and oo.starttime>:h1)) and (oo.day<:d2 or (oo.day=:d2 and oo.starttime<:h2))
                    group by oo.day,oo.daystr,oo.id,oo.clients,oo.losttype,oo.name,oo.repeattype,oo.days,oo.startday,oo.starttime,oo.finishday,oo.finishtime,oo.clearonovertime
                )z";
                $stmt_mola_suresi = $conn->prepare($sql_mola_suresi);
                $stmt_mola_suresi->bindValue('d1', substr($obj_params->t1_1, 0, 10));
                $stmt_mola_suresi->bindValue('gun', $gun_sayi);
                $stmt_mola_suresi->bindValue('h1', substr($obj_params->t1_1, 11, 5));
                $stmt_mola_suresi->bindValue('d2', substr($obj_params->t1_2, 0, 10));
                $stmt_mola_suresi->bindValue('h2', substr($obj_params->t1_2, 11, 5));
                $stmt_mola_suresi->execute();
                $records_mola_suresi = $stmt_mola_suresi->fetchAll();
                $mola_suresi = $records_mola_suresi[0]["molasuresi"];

                $sql = "select b.tasiyicikod,b.gorevsayi,b.gorevsayisi
                    ,cast(((b.sure-b.bossure)/60) as numeric(10,2))gorevsuredk
                    ,cast(b.bossure/60 as numeric(10,2)) bossuredk
                    ,cast(((b.sure-b.bossure)/60)/b.gorevsayisi as numeric(10,2))ortsuredk
                    ,cast($mola_suresi as numeric(10,2))molasuredk
                    ,cast((b.sure/60)-$mola_suresi as numeric(10,2))suredk
                    ,cast(((b.sure-b.bossure)/60)/((b.sure/60)-$mola_suresi) as numeric(10,2)) doluluk
                from (
                    select a.tasiyicikod,string_agg(a.gorevsayi,',')gorevsayi,sum(a.sayi)gorevsayisi
                        ,COALESCE((SELECT sum(cast(extract(epoch from (COALESCE(ces.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(ces.start,CURRENT_TIMESTAMP)::timestamp)) as integer))
                        from carrier_empty_sessions ces where ces.carrier=a.tasiyicikod and ces.start>:t1_1 and ces.finish<:t1_2 and finish-start<'08:00:00'),0)bossure
                            ,cast(extract(epoch from (COALESCE(t1_2,CURRENT_TIMESTAMP)::timestamp-COALESCE(t1_1,CURRENT_TIMESTAMP)::timestamp)) as integer) sure
                    from (
                        SELECT cm.carrier tasiyicikod,cast(concat(cm.tasktype,'-', count(*)) as varchar)gorevsayi,count(*) sayi
                            ,cast(sum(cm.breaktimeseconds)/60 as numeric(10,2))moladk
                        from case_movements cm
                        left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                        where cm.status in ('CLOSED','CLOSED-CASE-UPDATE') and cm.assignmenttime is not null and c.id is not null and cm.actualfinishtime between :t1_1 and :t1_2
                        GROUP BY cm.carrier,cm.tasktype
                    )a
                    GROUP BY a.tasiyicikod
                )b";
                break;
            case 'mh-employee-time':
                $t1 = $obj_params->t1_1;
                $sql_start = "SELECT 
                    concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                from job_rotations 
                where (finish is null or finish<now())
                order by beginval desc limit 1";
                $stmt_start = $conn->prepare($sql_start);
                $stmt_start->bindValue('t1', $obj_params->t1_1);
                $stmt_start->execute();
                $records_start = $stmt_start->fetchAll();
                $obj_params->t1_1 = $records_start[0]["jr_start"];

                $sql_finish = "SELECT 
                    concat(cast(case when beginval>endval then :t2::timestamp + INTERVAL '1 day' else :t2::timestamp end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                order by endval desc limit 1";
                $stmt_finish = $conn->prepare($sql_finish);
                $stmt_finish->bindValue('t2', (isset($obj_params->t1_2) ? $obj_params->t1_2 : $t1));
                $stmt_finish->execute();
                $records_finish = $stmt_finish->fetchAll();
                $obj_params->t1_2 = $records_finish[0]["jr_end"];

                $sql_gun_sayi = "SELECT ceil(EXTRACT(EPOCH FROM :t2::timestamp - :t1::timestamp)/86400) gun_sayi";
                $stmt_gun_sayi = $conn->prepare($sql_gun_sayi);
                $stmt_gun_sayi->bindValue('t1', $obj_params->t1_1);
                $stmt_gun_sayi->bindValue('t2', $obj_params->t1_2);
                $stmt_gun_sayi->execute();
                $records_gun_sayi = $stmt_gun_sayi->fetchAll();
                $gun_sayi = $records_gun_sayi[0]["gun_sayi"];

                $sql_mola_suresi = "select cast(sum(molasuresi)/60 as numeric(10,2)) molasuresi 
                from (
                    select EXTRACT(EPOCH FROM oo.finishtime::interval-oo.starttime::interval)molasuresi
                    from (	
                        select d1.day,trim(to_char(d1.day, 'day')) daystr,t1.hour,o.id,o.clients,o.losttype,o.name,o.repeattype,o.days,o.startday,o.starttime,o.finishday,o.finishtime,COALESCE(o.clearonovertime,false)clearonovertime
                        from (select cast(:d1 as date) + s.t as day from generate_series(0,:gun) as s(t))d1
                        join (select (s.t ||' minute')::interval as hour from generate_series(0,1439) as s(t)) t1 on 1=1
                        join offtimes o on o.finish is null and cast(t1.hour as varchar) between cast(o.starttime as varchar) and cast(o.finishtime as varchar) and strpos(lower(days),trim(to_char(d1.day, 'day')))>0
                    )oo
                    where (oo.day>:d1 or (oo.day=:d1 and oo.starttime>:h1)) and (oo.day<:d2 or (oo.day=:d2 and oo.starttime<:h2))
                    group by oo.day,oo.daystr,oo.id,oo.clients,oo.losttype,oo.name,oo.repeattype,oo.days,oo.startday,oo.starttime,oo.finishday,oo.finishtime,oo.clearonovertime
                )z";
                $stmt_mola_suresi = $conn->prepare($sql_mola_suresi);
                $stmt_mola_suresi->bindValue('d1', substr($obj_params->t1_1, 0, 10));
                $stmt_mola_suresi->bindValue('gun', $gun_sayi);
                $stmt_mola_suresi->bindValue('h1', substr($obj_params->t1_1, 11, 5));
                $stmt_mola_suresi->bindValue('d2', substr($obj_params->t1_2, 0, 10));
                $stmt_mola_suresi->bindValue('h2', substr($obj_params->t1_2, 11, 5));
                $stmt_mola_suresi->execute();
                $records_mola_suresi = $stmt_mola_suresi->fetchAll();
                $mola_suresi = $records_mola_suresi[0]["molasuresi"] / 3;

                $sql = "select b.sicil,b.adsoyad,b.gorevsayi,b.gorevsayisi
                    ,cast(((b.sure-b.bossure)/60) as numeric(10,2))gorevsuredk
                    ,cast(b.bossure/60 as numeric(10,2)) bossuredk
                    ,cast(((b.sure-b.bossure)/60)/b.gorevsayisi as numeric(10,2))ortsuredk
                    ,cast($mola_suresi as numeric(10,2))molasuredk
                    ,cast((b.sure/60)-$mola_suresi as numeric(10,2))suredk
                    ,cast(((b.sure-b.bossure)/60)/((b.sure/60)-$mola_suresi) as numeric(10,2)) doluluk
                from (
                    select a.sicil,a.adsoyad,string_agg(a.gorevsayi,',')gorevsayi,sum(a.sayi)gorevsayisi
                        ,COALESCE((SELECT sum(cast(extract(epoch from (COALESCE(ces.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(ces.start,CURRENT_TIMESTAMP)::timestamp)) as integer))
                        from carrier_empty_sessions ces where ces.empcode=a.sicil and ces.start>:t1_1 and ces.finish<:t1_2 and finish-start<'08:00:00'),0)bossure
                            ,cast(extract(epoch from (COALESCE(t1_2,CURRENT_TIMESTAMP)::timestamp-COALESCE(t1_1,CURRENT_TIMESTAMP)::timestamp)) as integer)/3 sure
                    from (
                        SELECT cm.empcode sicil,cm.empname adsoyad,cast(concat(cm.tasktype,'-', count(*)) as varchar)gorevsayi,count(*) sayi
                        from case_movements cm
                        left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                        where cm.status in ('CLOSED','CLOSED-CASE-UPDATE') and cm.assignmenttime is not null and c.id is not null and cm.actualfinishtime between :t1_1 and :t1_2
                        GROUP BY cm.empcode,cm.empname,cm.tasktype
                    )a
                    GROUP BY a.sicil,a.adsoyad
                )b";
                break;
            case 'mh-silinen-etiketler':
                $t1 = $obj_params->cl_canceltime_1;
                $sql_start = "SELECT 
                    concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                from job_rotations 
                where (finish is null or finish<now())
                order by beginval desc limit 1";
                $stmt_start = $conn->prepare($sql_start);
                $stmt_start->bindValue('t1', $obj_params->cl_canceltime_1);
                $stmt_start->execute();
                $records_start = $stmt_start->fetchAll();
                $obj_params->cl_canceltime_1 = $records_start[0]["jr_start"];

                $sql_finish = "SELECT 
                    concat(cast(case when beginval>endval then :t2::timestamp + INTERVAL '1 day' else :t2::timestamp end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                order by endval desc limit 1";
                $stmt_finish = $conn->prepare($sql_finish);
                $stmt_finish->bindValue('t2', (isset($obj_params->cl_canceltime_2) ? $obj_params->cl_canceltime_2 : $t1));
                $stmt_finish->execute();
                $records_finish = $stmt_finish->fetchAll();
                $obj_params->cl_canceltime_2 = $records_finish[0]["jr_end"];
                $sql = 'SELECT cl.erprefnumber "KD No",cl.time "Oluşum Zamanı",cl.canceldescription "İptal Açıklama",cl.canceltime "İptal Zaman"
                    ,cl.cancelusername "İptal Kullanıcı" 
                    ,(select tl.opname from task_lists tl where tl.erprefnumber=cl.erprefnumber limit 1) "Operasyon"
                from case_labels cl 
                where cl.status in (\'DELETED\',\'LABELDELETED\') ' . $str_where . '
                order by cl.time';
                break;
            case 'mh-hata-listesi':
                $t1 = $obj_params->time_1;
                $sql_start = "SELECT 
                    concat(cast(case when beginval>endval then :t1::timestamp - INTERVAL '1 day' else :t1::timestamp end as varchar(10)),' ',beginval,':00')::timestamp jr_start
                from job_rotations 
                where (finish is null or finish<now())
                order by beginval desc limit 1";
                $stmt_start = $conn->prepare($sql_start);
                $stmt_start->bindValue('t1', $obj_params->time_1);
                $stmt_start->execute();
                $records_start = $stmt_start->fetchAll();
                $obj_params->time_1 = $records_start[0]["jr_start"];

                $sql_finish = "SELECT 
                    concat(cast(case when beginval>endval then :t2::timestamp + INTERVAL '1 day' else :t2::timestamp end as varchar(10)),' ',endval,':59')::timestamp jr_end
                from job_rotations 
                where (finish is null or finish<now())
                order by endval desc limit 1";
                $stmt_finish = $conn->prepare($sql_finish);
                $stmt_finish->bindValue('t2', (isset($obj_params->time_2) ? $obj_params->time_2 : $t1));
                $stmt_finish->execute();
                $records_finish = $stmt_finish->fetchAll();
                $obj_params->time_2 = $records_finish[0]["jr_end"];
                $sql = 'SELECT "time" "Zaman",path,message from _log_exception where 1=1 ' . $str_where . ' order by time';
                break;
            case 'oa-list':
                $sql = 'SELECT oa.employee "Sicil No",e.name "Operatör",oa.opname "Operasyon",oa.l1,oa.l2,oa.l3,oa.l4,oa.time "Zaman" 
                from operation_authorizations oa
                join employees e on e.code=oa.employee
                where oa.delete_user_id is null ' . $str_where . ' 
                order by e.name,oa.opname';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'oa-history':
                $sql = 'SELECT oah.employee "Sicil No",e.name "Operatör",oah.opname "Operasyon",oah.l1,oah.l2,oah.l3,oah.l4,oah.time "Zaman",u.username "Kullanıcı",oah.description "Açıklama"
                from operation_authorization_history oah
                join employees e on e.code=oah.employee
                join _user u on u.id=cast(oah.userid as int)
                where oah.delete_user_id is null ' . $str_where . ' 
                order by e.name,oah.opname,oah.time';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'oa-log':
                $sql = 'SELECT oadl.time "Zaman",oadl.employee "Sicil No",e.name "Operatör",oadl.opname "Operasyon",oadl.type,concat(ae.code,\'-\',ae.name) "Usta"
                from operation_authorization_device_logs oadl
                join employees e on e.code=oadl.employee
                left join employees ae on ae.code=oadl.authemployee
                where 1=1 ' . $str_where . ' 
                order by oadl.time,e.name,oadl.opname';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'oa-require-list':
                $sql = 'SELECT e.code "Sicil",e.name "Operatör",tlop.opname "Operasyon",oa.l1,oa.l2,oa.l3,oa.l4
                from employees e 
                join (SELECT tl.opname from task_lists tl where tl.start>current_date - interval \'60\' day GROUP BY tl.opname) tlop on 1=1
                left join operation_authorizations oa on oa.employee=e.code and oa.opname=tlop.opname
                where e.isoperator=true ' . $str_where . '
                order by e.code,tlop.opname';
                //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                break;
            case 'oa-forward-list':
                //".($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                $sql = 'SELECT oa.opname,e.code,e.name,case when oa.l2=0 then \'Seviye 2 geçiş uygun\' else \'Seviye 3 geçiş uygun\' end durum
                from operation_authorizations oa 
                join product_trees pt on pt.name=oa.opname and pt.materialtype=\'O\' and pt.finish is null
                join employees e on e.code=oa.employee
                join (select cpdy.employee,cpdy.opname,count(*)
                    from (
                        SELECT cpd.employee,cpd.opname,cpd.erprefnumber
                        from client_production_details cpd 
                        where cpd.type=\'e_p\' and cpd.day>CURRENT_DATE - interval \'1 year\' and cpd.erprefnumber is not null 
                        GROUP BY cpd.erprefnumber,cpd.employee,cpd.opname 
                    )cpdy
                    GROUP BY cpdy.employee,cpdy.opname
                    having count(*)>2) cpdx on cpdx.employee=e.code and cpdx.opname=oa.opname
                where (oa.l3=0 or oa.l2=0) ' . $str_where . '
                order by e.name,oa.opname';
                break;
            case 'work-out-system':
                //$sql='SELECT * from(
                //    SELECT \'Sistem Dışı Çalışma\' "Tip",wos.client "İstasyon",wos.day "Gün",wos.jobrotation "Vardiya"
                //        ,wos.time "Zaman",wos.employee "Sicil",e.name "Operatör"
                //        ,wos.opnames "Operasyon",wos.description "Açıklama"
                //    from work_out_system wos
                //    left join employees e on e.code=wos.employee
                //    where 1=1 and e.finish is null '.$str_where.'
                //    union all
                //    SELECT \'Elle Veri Girişi\' "Tip",cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya"
                //        ,cpd.start "Başlangıç",cpd.finish "Bitiş",cpd.employee "Sicil",cpd.name "Operatör"
                //        ,cpd.opnames "Operasyon",\'\' "Açıklama"
                //    from (
                //        SELECT wos.client,wos.day,wos.jobrotation
                //            ,wos.time,string_agg(distinct wos.employee,\'\')employee,string_agg(distinct e.name,\'\') "name"
                //            ,wos.opname opnames,wos.start,wos.finish
                //        from client_production_details wos
                //        left join employees e on e.code=wos.employee
                //        where wos.type in (\'c_p_out\',\'e_p_out\') '.$str_where.'
                //        GROUP BY wos.client,wos.day,wos.jobrotation,wos.time,wos.opname,wos.start,wos.finish
                //    )cpd
                //)z
                //order by z."Zaman"';
                $sql = 'SELECT case 
                        when cpd.type in (\'c_p_out\',\'e_p_out\') then \'Elle Veri Girişi\' 
                        when cpd.type in (\'c_p_unconfirm\',\'e_p_unconfirm\') then \'Sistem Dışı Çalışma Onaysız\'
                        when cpd.type in (\'c_p_confirm\',\'e_p_confirm\') then \'Sistem Dışı Çalışma Onaylı\' end "Tip"
                    ,cpd.client "İstasyon",cpd.day "Gün",cpd.jobrotation "Vardiya"
                    ,cpd.start "Başlangıç",cpd.finish "Bitiş",cpd.employee "Sicil",cpd.name "Operatör"
                    ,cpd.opnames "Operasyon",COALESCE(cpd.taskfinishdescription,\'\') "Açıklama"
                from (
                    SELECT cpd.client,cpd.day,cpd.jobrotation
                        ,cpd.time,string_agg(distinct cpd.employee,\'|\')employee,string_agg(distinct e.name,\'|\') "name"
                        ,string_agg(distinct cpd.opname,\'|\') opnames,cpd.start,cpd.finish,cpd.type,max(cpd.taskfinishdescription) taskfinishdescription
                    from client_production_details cpd 
                    left join employees e on e.code=cpd.employee
                    where cpd.type in (\'e_p_out\',\'e_p_confirm\',\'e_p_unconfirm\') 
                    and cpd.client not in (SELECT code from clients where workflow in (\'CNC\'))
                    ' . $str_where . ' 
                    GROUP BY cpd.client,cpd.day,cpd.jobrotation,cpd.time,cpd.start,cpd.finish,cpd.type,cpd.employee
                )cpd
                order by cpd.start';
                break;
            case 'work-out-system-summary':
                $sql = 'select round(sum(e_p_out_sure)/60)"Elle girilen işlerin süresi (dk)"
                    ,round(sum(e_p_el)/60)"El İşçilik/Menteşe (dk)"
                    ,round(sum(e_p_unconfirm_sure)/60)"Sistem dışı çalışma süresi (dk)"
                    ,round(sum(e_p_sure)/60)"Verimot çalışma süresi (dk)"
                    ,round((sum(e_p_sure)+sum(e_p_unconfirm_sure)+sum(e_p_out_sure))/60)"Toplam çalışma süresi (dk)"
                    ,cast(100*round(sum(e_p_sure)/60)/round((sum(e_p_sure)+sum(e_p_unconfirm_sure)+sum(e_p_out_sure))/60)as decimal(20,2)) "Oran"
                from (
                    select a2.type
                        ,case when a2.type in (\'e_p\',\'e_p_confirm\') then sum(a2.suresn) else 0 end e_p_sure
                        ,case when a2.type=\'e_p_out\' then sum(a2.suresnelveri) else 0 end e_p_out_sure
                        ,case when a2.type=\'e_p_unconfirm\' then sum(a2.suresn) else 0 end e_p_unconfirm_sure
                        ,sum(a2.suresnel) e_p_el
                    from (
                        select a1.*,case when a1.client like \'03.0%\' and a1.type<>\'e_p_out\' then cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) else 0 end suresn 
                        ,case when (a1.client not like \'03.0%\' and a1.type<>\'e_p_out\' or a1.client like \'Mentese%\') then cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) else 0 end suresnel 
                        ,case when a1.client not like \'Mentese%\' and a1.type=\'e_p_out\' then cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) else 0 end suresnelveri 
                        from (	
                            SELECT case when coalesce(cpd.mould,\'\')=\'\' and coalesce(cpd.mouldgroup,\'\')=\'\' and cpd.type in (\'e_p\',\'e_p_confirm\') and cpd.update_user_id>1 then \'e_p_unconfirm\' else cpd.type end "type"
                                ,cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish
                            from client_production_details cpd
                            where ( (cpd.type in (\'e_p\',\'e_p_confirm\',\'e_p_unconfirm\') and cpd.client not in (SELECT code from clients where workflow in (\'CNC\',\'Kataforez\'))) 
                            or (cpd.type in (\'e_p_out\') and cpd.client in (SELECT code from clients where workflow in (\'CNC\',\'Kataforez\'))) )
                            ' . $str_where . ' 
                        )a1
                        group by a1.type,a1.client,a1.day,a1.jobrotation,a1.start,a1.finish
                        union all
                        select a1.*,case when a1.client like \'03.0%\' and a1.type<>\'e_p_out\' then cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) else 0 end suresn 
                        ,case when a1.client not like \'03.0%\' and a1.type<>\'e_p_out\' then cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) else 0 end suresnel 
                        ,case when a1.type=\'e_p_out\' then cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) else 0 end suresnelveri 
                        from (	
                            SELECT case when coalesce(cpd.mould,\'\')=\'\' and coalesce(cpd.mouldgroup,\'\')=\'\' and cpd.type in (\'e_p\',\'e_p_confirm\') and cpd.update_user_id>1 then \'e_p_unconfirm\' else cpd.type end "type"
                                ,cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish
                            from client_production_details cpd
                            where cpd.type in (\'e_p_out\') 
                            and cpd.client not in (SELECT code from clients where workflow in (\'CNC\',\'Kataforez\'))
                            ' . $str_where . ' 
                            group by case when coalesce(cpd.mould,\'\')=\'\' and coalesce(cpd.mouldgroup,\'\')=\'\' and cpd.type in (\'e_p\',\'e_p_confirm\') and cpd.update_user_id>1 
                            then \'e_p_unconfirm\' else cpd.type end,cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish,cpd.employee
                        )a1
                    )a2
                    group by a2.type
                )a3';
                /*
                $sql='select round(sum(e_p_out_sure)/60)"Elle girilen işlerin süresi (dk)"
                    ,round(sum(e_p_el)/60)"El İşçilik/Menteşe (dk)"
                    ,round(sum(e_p_unconfirm_sure)/60)"Sistem dışı çalışma süresi (dk)"
                    ,round(sum(e_p_sure)/60)"Verimot çalışma süresi (dk)"
                    ,round((sum(e_p_sure)+sum(e_p_unconfirm_sure)+sum(e_p_out_sure))/60)"Toplam çalışma süresi (dk)"
                    ,cast(100*round(sum(e_p_sure)/60)/round((sum(e_p_sure)+sum(e_p_unconfirm_sure)+sum(e_p_out_sure))/60)as decimal(20,2)) "Oran"
                from (
                    select a2.type
                        ,case when a2.type in (\'e_p\',\'e_p_confirm\') then sum(a2.suresn) else 0 end e_p_sure
                        ,case when a2.type=\'e_p_out\' then sum(a2.suresn) else 0 end e_p_out_sure
                        ,case when a2.type=\'e_p_unconfirm\' then sum(a2.suresn) else 0 end e_p_unconfirm_sure
                        ,sum(a2.suresnel) e_p_el
                    from (
                        select a1.*,case when a1.client like \'03.0%\' then cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) else 0 end suresn
                        ,case when a1.client like \'03.0%\' then 0 else cast(extract(epoch from (a1.finish::timestamp-a1.start)) as integer) end suresnel
                        from (
                            SELECT case when coalesce(cpd.mould,'')='' and coalesce(cpd.mouldgroup,'')='' and cpd.type in (\'e_p\',\'e_p_confirm\') and cpd.update_user_id>1 then \'e_p_unconfirm\' else cpd.type end "type"
                                ,cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.finish
                            from client_production_details cpd
                            where cpd.type in (\'e_p\',\'e_p_confirm\',\'e_p_unconfirm\',\'e_p_out\')
                            and cpd.client not in (SELECT code from clients where workflow in (\'CNC\'))
                            '.$str_where.'
                        )a1
                        group by a1.type,a1.client,a1.day,a1.jobrotation,a1.start,a1.finish
                    )a2
                    group by a2.type
                )a3';
                */
                break;
            case 'pres-setup-detail':
                if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                    $sql = 'select client "İstasyon",mould "Kalıp",opdescription "Operasyon",basla "Başlangıç",bitir "Bitiş"
                        ,setup "Setup",kayip "Kayıp",toplam "Toplam",personeller "Personeller",descriptionlost "Açıklama"
                    from (
                        select client,mould
                            ,string_agg(opname,\',\' order by opdescription)opname
                            ,string_agg(opdescription,\',\' order by opdescription)opdescription
                            ,min(cld.start) basla,max(cld.finish) bitir,max(descriptionlost)descriptionlost
                            ,round(sum(setup)/60) setup,round(sum(kayip)/60) kayip,round((sum(setup)+sum(kayip))/60) toplam
                            ,(SELECT string_agg(distinct concat(cld_e.employee,\'-\',e.name),\',\')
                                from client_lost_details cld_e 
                                join employees e on e.code=cld_e.employee
                                where cld_e.employee is not null and cld_e.client=cld.client and cld_e.day=min(cld.day)
                                and cld_e.client not in (SELECT code from clients where workflow=\'CNC\')
                                and cld_e.losttype in (\'SETUP-AYAR\',\'SETUP AYAR\') and cld_e.start>=min(cld.start) 
                                and cld_e.finish<=max(cld.finish))personeller
                        from (
                            SELECT client,day,jobrotation,tasklist,mould,opname,opdescription,start,finish,losttype
                                ,case when losttype=\'SETUP-AYAR\' then cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) else 0 end setup
                                ,case when losttype=\'SETUP AYAR\' then cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) else 0 end kayip
                                ,replace(descriptionlost,\'null\',\'\')descriptionlost
                            from client_lost_details 
                            where type=\'l_c_t\' and losttype in (\'SETUP-AYAR\',\'SETUP AYAR\') 
                            and client not in (SELECT code from clients where workflow=\'CNC\')
                            ' . $str_where . ' 
                            /*order by start*/
                        )cld
                        group by client,mould,opname,start,finish
                    )c_l_d
                    group by client,mould,opdescription,basla,bitir,setup,kayip,toplam,personeller,descriptionlost
                    order by client,basla,bitir';
                } else {
                    $sql = 'select client "İstasyon",mould "Kalıp",string_agg( opdescription,\',\' order by opdescription) "Operasyon",basla "Başlangıç",bitir "Bitiş"
                        ,setup "Setup",kayip "Kayıp",toplam "Toplam",personeller "Personeller",string_agg(descriptionlost,\' \') "Açıklama"
                    from (
                        select cld.client,mould
                            ,string_agg(opname,\',\' order by opdescription)opname
                            ,string_agg(distinct opdescription,\',\' order by opdescription)opdescription
                            ,min(cld.start) basla,max(cld.finish) bitir,max(descriptionlost)descriptionlost
                            ,round(sum(setup)/60) setup,round(sum(kayip)/60) kayip,round((sum(setup)+sum(kayip))/60) toplam
                            ,(SELECT string_agg(distinct concat(cld_e.employee,\'-\',e.name),\',\')
                                from client_lost_details cld_e 
                                join employees e on e.code=cld_e.employee
                                where cld_e.employee is not null and cld_e.client=cld.client and cld_e.day=min(cld.day)
                                and cld_e.client not in (SELECT code from clients where workflow=\'CNC\')
                                and cld_e.losttype in (\'SETUP-AYAR\',\'SETUP AYAR\') and cld_e.start>=min(cld.start) 
                                and cld_e.finish<=max(cld.finish))personeller
                        from (
                            SELECT client,day,jobrotation,erprefnumber,tasklist,mould,opname,opdescription,start,finish,losttype
                                ,case when losttype=\'SETUP-AYAR\' then cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) else 0 end setup
                                ,case when losttype=\'SETUP AYAR\' then cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) else 0 end kayip
                                ,replace(descriptionlost,\'null\',\'\')descriptionlost
                            from client_lost_details
                            where type=\'l_c_t\' and losttype in (\'SETUP-AYAR\',\'SETUP AYAR\') 
                            and client not in (SELECT code from clients where workflow=\'CNC\')
                            ' . $str_where . ' 
                            /*order by start*/
                        )cld
                        left join product_trees pt on pt.name=cld.opname and pt.materialtype=\'O\' and pt.finish is null '.($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cld.client)=cld.client " : "").'
                        where coalesce(pt.name,\'\') not like \'%DENEME%\'
                        group by cld.client,mould,erprefnumber
                    )c_l_d
                    group by client,mould,basla,bitir,setup,kayip,toplam,personeller
                    order by client,basla,bitir';
                }
                break;
            case 'fault-list':
                $sql = 'SELECT client "İstasyon",losttype "Arıza Tipi",min("start")"Başlangıç",max(finish)"Bitiş" 
                    ,round(cast(extract(epoch from max(finish)::timestamp-min("start")::timestamp)) as integer)/3600,4) as "Saat"
					,round(cast(extract(epoch from max(finish)::timestamp-min("start")::timestamp)) as integer)/60,2) as "Dakika"
					,cast(extract(epoch from max(finish)::timestamp-min("start")::timestamp)) as integer) as "Saniye"
                from client_lost_details 
                where type=\'l_c\' and losttype in (\'ARIZA KALIP-APARAT\',\'ARIZA MAKİNE ELEKTRİK\',\'ARIZA MAKİNE MEKANİK\',\'ARIZA ÜRETİM\') ' . $str_where . ' 
                group by client,losttype,tasklist
                order by min("start")';
                break;
            case 'kanban-full':
                $sql = 'SELECT client "İstasyon",concat(\'E|\',product) "Mamül",boxcount "Ekran Kart Sayısı"
                    , minboxcount "Min",maxboxcount "Max",currentboxcount "Kart Sayısı Stok"
                    ,packaging "Kasa İçi",hourlydemand "Saatlik Talep"
                from kanban_operations 
                where currentboxcount>boxcount ' . $str_where . ' 
                order by product';
                break;
            case 'kanban-over-log':
                $sql = 'SELECT client "İstasyon",concat(\'E|\',product) "Mamül"
                    ,boxcount "Ekran Kart Sayısı"
                    ,currentboxcount "Kart Sayısı Stok"
                    ,currentboxcount-boxcount "Fark"
                    ,cast(time as date) "Tarih" ,cast(time as time) "Saat"
                from kanban_over_products 
                where 1=1 ' . $str_where . ' 
                order by product';
                break;
            case 'control-answers':
                $sql = 'SELECT ca.type "Tip",ca.client "İstasyon",ca.day "Gün",ca.jobrotation "Vardiya",ca.opname "Operasyon"
                    ,ca.erprefnumber "ERP Ref",ca.employee "Sicil No",e.name "Personel"
                    ,ca.time "Zaman",ca.question "Soru",ca.answertype "Cevap Tipi",ca.valuerequire "Beklenen"
                    ,ca.valuemin "Min",ca.valuemax "Max",ca.answer "Cevap",ca.description "Açıklama"
                from control_answers ca
                join employees e on e.code=ca.employee
                where 1=1 ' . $str_where . ' 
                order by ca.time';
                break;
            case 'remote-quality-reject-list':
                $sql = 'SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya",cld.descriptionlost "Açıklama"
                    ,cld.opname "Operasyon",cld.erprefnumber  "ERP Ref",cld.employee "Sicil No",e.name "Kalite Operatörü"
                    ,cld.start "Başlangıç",cld.finish "Bitiş"
                from client_production_details cpd 
                join client_lost_details cld on cld.client=cpd.client and cld.day=cpd.day and cld.jobrotation=cpd.jobrotation and cld.finish=cpd.finish
                left join employees e on e.code=cld.employee
                where cpd.taskfinishtype=\'KALİTE RED\' 
                    and cld.losttype=\'KALİTE ONAY\' 
                    and cld.type=\'l_e_t\' 
                    and cpd.type=\'c_p\' 
                    and cld.descriptionlost not like \' %\'
                    and cpd.client not in (SELECT code from clients where workflow=\'CNC\')
                    ' . $str_where . '
                order by cld.start';
                break;
            case 'taskplan-employee':
                $sql = 'SELECT tpe.employee "Sicil No",e.name "Operatör",tpe.client "İstasyon"
                    ,concat(\'E|\',string_agg(tpe.opname,\'|\' ORDER BY tpe.id)) "Operasyon"
                    ,concat(\'E|\',string_agg(tpe.erprefnumber,\'|\' ORDER BY tpe.id)) "ERP Ref"
                    ,tpe.plannedstart "Başlangıç",tpe.plannedfinish "Bitiş" 
                from task_plan_employees tpe 
                join (
                    SELECT id,code,beginval,endval ,current_date,current_time
                        ,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)) "day" 
                        ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))<=endval then current_date - INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',beginval,\':00\')::timestamp jr_start 
                        ,concat(cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)),\' \',endval,\':59\')::timestamp +interval \'1 second\' jr_end 
                    from job_rotations 	
                    where (finish is null or finish<now())	
                        and (	
                            (endval>beginval and cast(current_time as varchar(5)) between beginval and endval)	
                            or 
                            (endval<beginval and (cast(current_time as varchar(5))>=beginval or cast(current_time as varchar(5))<=endval) )	
                        )
                )sv on tpe.plannedstart>=sv.jr_start and tpe.plannedfinish<=sv.jr_end
                join employees e on e.code=tpe.employee
                where 1=1 ' . $str_where . '
                GROUP BY tpe.employee,e.name,tpe.client,tpe.plannedstart,tpe.plannedfinish
                order by e.name,tpe.plannedstart';
                break;
            case 'taskplan-reality':
                $sql = "SELECT erprefnumber \"ERP Ref\",opname \"Operasyon\",client \"İstasyon\"
                ,plannedstart \"Pl. Başlangıç\",plannedfinish \"Pl. Bitiş\"
                ,cast(coalesce(start,CURRENT_TIMESTAMP(0)) as VARCHAR(19)) \"Başlangıç\",finish \"Bitiş\"
                ,case when coalesce(start,CURRENT_TIMESTAMP(0))>plannedstart then 
                        cast(extract(epoch from (COALESCE(start,CURRENT_TIMESTAMP)::timestamp-COALESCE(plannedstart,CURRENT_TIMESTAMP)::timestamp)) as integer)
                    else 
                        cast(extract(epoch from (COALESCE(plannedstart,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                    end \"Gecikme sn\"
                ,round(case when coalesce(start,CURRENT_TIMESTAMP(0))>plannedstart then 
                        cast(extract(epoch from (COALESCE(start,CURRENT_TIMESTAMP)::timestamp-COALESCE(plannedstart,CURRENT_TIMESTAMP)::timestamp)) as integer)
                    else 
                        cast(extract(epoch from (COALESCE(plannedstart,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) end/60) \"Gecikme dk\"
                ,round(cast(case when coalesce(start,CURRENT_TIMESTAMP(0))>plannedstart then 
                        cast(extract(epoch from (COALESCE(start,CURRENT_TIMESTAMP)::timestamp-COALESCE(plannedstart,CURRENT_TIMESTAMP)::timestamp)) as integer)
                    else 
                        cast(extract(epoch from (COALESCE(plannedstart,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) end/3600 as decimal),2) \"Gecikme sa\"
                from task_lists 
                where plannedstart is not null " . $str_where . "
                order by plannedstart";
                break;
            case 'fault-sessions':
                $sql = "SELECT fs.idx \"Grup Kodu\",fs.client \"İstasyon\",fs.losttype \"Arıza Tipi\",fs.faulttype \"Arıza Kodu\"
                    ,fs.faultdescription \"Arıza Açıklaması\",fs.faultdevice \"Arızalı Ekipman\",fs.descriptionlost \"Kayıp Açıklaması\"
                ,fs.employee\"Bildiren Sicil\",ef.name \"Bildiren Personel\",fs.start \"Arıza Başlangıç\",fs.finish \"Arıza Bitiş\"
                ,fs.employeeintervention \"Müdahale Eden sicil\",efi.name \"Bildiren Personel\"
                ,fs.startintervention \"Müdahale Başlangıç\",fs.finishintervention \"Müdahale Bitiş\",fs.interventionnok \"Müdahale Red Açıklaması\"
                ,fs.employeeinterventionconfirm \"Onaylayan Sicil\",efc.name \"Onaylayan Personel\"
                ,fs.startinterventionconfirm \"Onay Başlangıç\",fs.finishinterventionconfirm \"Onay Bitiş\",fs.interventionconfirmnok \"Onay Red Açıklaması\"
                ,case fs.status 
                    when 'INTERVENTION_REJECT' then 'Müdahale Red' 
                    when 'INTERVENTION_CONFIRM_REJECT' then 'Müdahale Onay Red' 
                    when 'INTERVENTION_CONFIRM_ACCEPT' then 'Müdahale Onay' end \"Durum\"
                FROM fault_sessions fs
                join employees ef on ef.code=fs.employee
                join employees efi on efi.code=fs.employeeintervention
                join employees efc on efc.code=fs.employeeinterventionconfirm
                where 1=1 " . $str_where . "
                order by fs.client,fs.start";
                break;
            case 'task_sessions_logs':
                $sql = "select tsr.*
                    ,case when tsr.fark>0 and tsr.fark<60 then cast(tsr.fark as varchar) else '' end \"1 Saatten Az\"
                    ,case when tsr.fark>=60 and tsr.fark<240 then cast(tsr.fark as varchar) else '' end \"4 Saatten Az\"
                    ,case when tsr.fark>=240 and tsr.fark<480 then cast(tsr.fark as varchar) else '' end \"1 Vardiyadan Az\"
                    ,case when tsr.fark>=480 and tsr.fark<960 then cast(tsr.fark as varchar) else '' end \"2 Vardiyadan Az\"
                    ,case when tsr.fark>=960 and tsr.fark<1440 then cast(tsr.fark as varchar) else '' end \"3 Vardiyadan Az\"
                    ,case when tsr.fark>=1440 then cast(tsr.fark as varchar) else '' end \"3 Vardiyadan Çok\"
                from (	
                    SELECT ts.idx,string_agg(distinct ts.client, '-') \"İstasyon\"
                        ,string_agg(ts.erprefnumber, '-' order by ts.id)\"ERP Ref\"
                        ,string_agg(ts.opname, '-' order by ts.id) \"Operasyon\"
                        ,ts.plannedstart \"Pl. Başlangıç\",ts.start \"Başlangıç\"
                        ,(case when plannedstart>=start then 
                        cast(extract(epoch from (plannedstart-start)) as integer) else 
                        cast(extract(epoch from (start-plannedstart)) as integer) end
                        /60) fark
                    from task_sessions ts
                    where ts.plannedstart is not null " . $str_where . "
                    GROUP BY ts.idx,ts.plannedstart,ts.start
                ) tsr
                where tsr.fark>0
                order by tsr.fark desc";
                break;
            case 'task_cases_logs':
                $sql = 'SELECT client "İstasyon",casetype "Kasa Tipi",casenumber "Kasa No",caselot "Lot",opname "Operasyon",stockname "Açıklama",casename "Kasa Adı",packcapacity "Kapasite",quantityremaining "Kasa İçi",quantityremainingfirst "Kasa İçi İlk/Tesellüm Miktarı",employees "Sicil",time "Zaman"
                from task_cases_logs
                where 1=1 ' . $str_where . '
                order by time';
                break;
            case 'task_cases_logs_delivery':
                $sql = 'SELECT max(client)"İstasyon",erprefnumber "ERP Ref",casenumber "Kasa No",caselot "Sıra",opname "Operasyon"
                    ,packcapacity "Kapasite",quantityremaining "Tesellüm Adedi",min(time)"Zaman",max(employees)"Siciller"
                from task_cases_logs 
                where casetype=\'O\' and stockname like \'DELIVER%\' ' . $str_where . '
                GROUP BY erprefnumber,casenumber,caselot,opname,packcapacity,quantityremaining
                order by casenumber';
                break;
            case 'reworks_working':
                $sql = 'select xx.client "İstasyon",xx.day "Gün",xx.jobrotation "Vardiya"
                    ,xx.opname "Operasyon",xx.reworktype "Tipi",xx.opdescription "Açıklama"
                    ,xx.start "Başlangıç",xx.finish "Bitiş",xx.duration "Süre"
                    ,xx.employee "Sicil No",xx.name "Operatör"
                    ,xx.production "Toplam Miktar",xx.quantityscrap "Iskarta Miktarı" 
                    ,xx.production-xx.quantityscrap "Yeniden İşlem Miktarı"
                    ,xx.rejecttype "Iskarta Tipi"
                from (
                SELECT cpd.client,cpd.day,cpd.jobrotation
                    ,cpd.opname,r.reworktype,cpd.opdescription
                    ,cpd.start,cpd.finish,cpd.finish-cpd.start duration
                    ,cpd.employee,e.name,cpd.production
                    ,coalesce((SELECT sum(trd.quantityscrap) from task_reject_details trd where trd.client=cpd.client and trd.employee=cpd.employee and trd.day=cpd.day and trd.jobrotation=cpd.jobrotation),0) quantityscrap
                    ,(SELECT max(trd.rejecttype) from task_reject_details trd where trd.client=cpd.client and trd.employee=cpd.employee and trd.day=cpd.day and trd.jobrotation=cpd.jobrotation) rejecttype
                from client_production_details cpd
                join employees e on e.code=cpd.employee
                left join reworks r on cpd.tasklist=r.code and cpd.opname=r.opname
                where cpd.client like \'99%\' ' . $str_where . '
                )xx 
                order by xx.start';
                break;
            case 'delivery_logs':
                //and time>'2024-12-10 14:50:00'
                $sql='select detail.erprefnumber "ERP Ref",detail.opname "Operasyon"
                    ,detail.caselot "Kasa Lotu",detail.casenumber "Kasa No"
                    ,detail.time "Zaman",detail.packcapacity "Kasa içi Miktar"
                    ,round(pt.tpp,2) "Birim Süre",round(detail.packcapacity*pt.tpp) "Toplam Süre" 
                    ,case when detail.auto=1 then \'Otomatik\' else \'Manuel\' end "İşlem Tipi"
                    ,detail.auto,detail.manual
                from (
                    SELECT erprefnumber,opname,caselot,casenumber,min(time)time,1 auto,0 manual,round(packcapacity)packcapacity
                    from task_cases_logs 
                    where casetype=\'O\' and stockname like \'DELIVER-AUTO%\' ' . $str_where . '
                    GROUP BY erprefnumber,opname,caselot,casenumber,packcapacity
                    union ALL
                    SELECT tcl.erprefnumber,tcl.opname,tcl.caselot,tcl.casenumber,min(tcl.time)time,0 auto,1 manual,round(coalesce(nullif(tcl.packcapacity,0),max(ptk.packcapacity)))packcapacity
                    from task_cases_logs tcl
                    left join product_trees ptk on ptk.name=opname and ptk.materialtype=\'K\'
                    where tcl.casetype=\'O\' and tcl.stockname = \'DELIVER-MANUAL-QAD\' ' . $str_where . '
                    GROUP BY tcl.erprefnumber,tcl.opname,tcl.caselot,tcl.casenumber,tcl.packcapacity
                )detail
                left join product_trees pt on pt.name=detail.opname and pt.materialtype=\'O\'
                order by detail.time';
                break;
            case 'document_view_logs':
                $sql='SELECT dvl.client "İstasyon",cgd.clientgroupcode "Hat",dvl.day "Gün",dvl.jobrotation "Vardiya",dvl.employee "Sicil",e.name "Ad Soyad"
                    ,dvl.erprefnumber "ERP Ref",dvl.opname "Operasyon",max(dvl.time) "Eğitim Zamanı",max(d.lastupdate) "SOP Güncelleme Zamanı"
                FROM document_view_logs dvl
                left join documents d on d.path=dvl.path
                left join employees e on e.code=dvl.employee
                left join client_group_details cgd on cgd.client=dvl.client and cgd.clientgroup=\'watch\'
                where 1=1 ' . $str_where . '
                GROUP BY dvl.client,cgd.clientgroupcode,dvl.day,dvl.jobrotation,dvl.employee,e.name,dvl.erprefnumber,dvl.opname
                order by max(dvl.time)';
                break;
            default:
                return $this->msgError("Hata Oluştu! Rapor bulunamadı.");
                break;
        }
        $stmt = $conn->prepare($sql);
        foreach ($obj_params as $key => $value) {
            $stmt->bindValue($key, $value);
        }
        $stmt->execute();
        $records = $stmt->fetchAll();
        //if (count($records) == 0) {
        //    return $this->msgError("Belirtilen kriterlere uygun kayıt bulunamadı.");
        //}
        $filename = strtolower($this->replace_tr($title)) . "-" . date("Y_m_d_H_i_s", time());
        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getProperties()
            ->setCreator("Kaitek - Reports")
            ->setLastModifiedBy("Kaitek - Reports")
            ->setTitle("Kaitek - Reports")
            ->setSubject("Kaitek - Reports")
            ->setDescription($filename)
            ->setKeywords('Verimot')
            ->setCategory('-');
        $objPHPExcel->setActiveSheetIndex(0);
        $col = 1;
        $row = 2;
        $sheet = $objPHPExcel->getActiveSheet();
        $_strcol = Coordinate::stringFromColumnIndex($col++);
        $sheet->setCellValue($_strcol . $row, 'Sıra');
        $sheet->getColumnDimension($_strcol)->setAutoSize(true);
        $sheet->getStyle($_strcol . $row)->getFont()->setBold(true);
        if (count($records) > 0) {
            $first = true;
            $_lc = 0;
            foreach ($records as $item => $kayit) {
                if ($first) {
                    foreach ($kayit as $key => $val) {
                        $_strcol = Coordinate::stringFromColumnIndex($col++);
                        $sheet->setCellValue($_strcol . $row, strip_tags($key)/*strtoupper($this->replace_tr(strip_tags($key)))*/);
                        $sheet->getColumnDimension($_strcol)->setAutoSize(true);
                        $sheet->getStyle($_strcol . $row)->getFont()->setBold(true);
                    }
                    $row++;
                    $first = false;
                    $_lc = $col;
                    /*
                    Not Alanı
                    */
                    //$colcount=0;
                    //$sheet->mergeCells(chr($col+$colcount)."1:".chr($col+$_lc)."1");
                    //$sheet->getStyle(chr($col+$colcount)."1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
                    //$sheet->setCellValue(chr($col+$colcount)."1", "NOT: $_d1-$_d2 arası kayıtlar listelenmiştir.");
                    //$sheet->getStyle(chr($col+$colcount)."1")->getFont()->setBold(true);
                    /*
                    Başlık Alanı
                    */
                    $sheet->mergeCells("A1:" . $_strcol . "1");
                    $sheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->setCellValue("A1", $title);
                    $sheet->getStyle("A1")->getFont()->setBold(true);
                }
                $col = 2;
                $sheet->setCellValue("A" . $row, $row - 2);
                foreach ($kayit as $rowtitle => $val) {
                    $_strcol = Coordinate::stringFromColumnIndex($col++);
                    $pos = strpos($val, "|");
                    if ($pos == 1) {
                        $val = substr($val, 2);
                        $sheet->setCellValueExplicit($_strcol . $row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/, DataType::TYPE_STRING);
                    } else {
                        $sheet->setCellValue($_strcol . $row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/);
                    }
                }
                //switch ($name){
                //    case 'rprPlanZamanUyum':
                //        if($kayit["erken_gec"]==1){
                //            $c_erken++;
                //        }else{
                //            $c_gec++;
                //        }
                //        $c_toplam++;
                //        break;
                //    case 'rprPlanMiktarUyum':
                //        if($kayit["erken_gec"]==1){
                //            $c_erken++;
                //        }else{
                //            $c_gec++;
                //        }
                //        $c_toplam++;
                //        break;
                //}
                $row++;
            }
            $col = 1;
            foreach ($kayit as $rowtitle => $val) {
                $_strcol = Coordinate::stringFromColumnIndex($col++);
                $sheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
            $col = 1;
            switch ($report) {
                case 'lostdetail-client-group':{
                    $objPHPExcel->createSheet();
                    $objDetailSheet = $objPHPExcel->setActiveSheetIndex(1);
                    $objDetailSheet->setTitle('Özet');
                    $titledetail = "Tiplerine Göre İstasyon Kayıpları";
                    $sql_detail = 'SELECT a.lostgroupcode "Kayıp Grubu",sum(a.sure) "Süre Dk",round(sum(a.sure)/60,2) "Süre Saat" 
                    from(
                        SELECT cld.client "İstasyon",cld.day "Gün",cld.jobrotation "Vardiya",cld.losttype "Kayıp Tipi"
                            ,cld.descriptionlost "Açıklama",cld.faultdevice "Ekipman"
                            ,cld.start "Başlangıç",cld.finish "Bitiş",cld.finish-cld.start "Süre"
                            ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) sure
                                ,lgd.lostgroupcode
                        from client_lost_details cld
                        join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup=\'lostgroup1\'
                        where cld.type = :type_1 /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/ ' . $str_where . '
                    )a
                    group by a.lostgroupcode
                    order by a.lostgroupcode
                    ';
                    $stmt_detail = $conn->prepare($sql_detail);
                    foreach ($obj_params as $key => $value) {
                        $stmt_detail->bindValue($key, $value);
                    }
                    $stmt_detail->execute();
                    $records_detail = $stmt_detail->fetchAll();
                    $col = 1;
                    $row = 2;
                    //$sheet = $objPHPExcel->getActiveSheet();
                    $_strcol = Coordinate::stringFromColumnIndex($col++);
                    $objDetailSheet->setCellValue($_strcol . $row, 'Sıra');
                    $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                    $objDetailSheet->getStyle($_strcol . $row)->getFont()->setBold(true);
                    $first = true;
                    foreach ($records_detail as $item => $kayit) {
                        if ($first) {
                            foreach ($kayit as $key => $val) {
                                $_strcol = Coordinate::stringFromColumnIndex($col++);
                                $objDetailSheet->setCellValue($_strcol . $row, strip_tags($key)/*strtoupper($this->replace_tr(strip_tags($key)))*/);
                                $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                                $objDetailSheet->getStyle($_strcol . $row)->getFont()->setBold(true);
                            }
                            $row++;
                            $first = false;
                            $_lc = $col;
                            /*
                            Başlık Alanı
                            */
                            $objDetailSheet->mergeCells("A1:" . $_strcol . "1");
                            $objDetailSheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                            $objDetailSheet->setCellValue("A1", $titledetail);
                            $objDetailSheet->getStyle("A1")->getFont()->setBold(true);
                        }
                        $col = 2;
                        $objDetailSheet->setCellValue("A" . $row, $row - 2);
                        foreach ($kayit as $rowtitle => $val) {
                            $_strcol = Coordinate::stringFromColumnIndex($col++);
                            $pos = strpos($val, "|");
                            if ($pos == 1) {
                                $val = substr($val, 2);
                                $objDetailSheet->setCellValueExplicit($_strcol . $row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/, DataType::TYPE_STRING);
                            } else {
                                $objDetailSheet->setCellValue($_strcol . $row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/);
                            }
                        }
                        $row++;
                    }
                    $col = 1;
                    foreach ($kayit as $rowtitle => $val) {
                        $_strcol = Coordinate::stringFromColumnIndex($col++);
                        $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                    }
                    break;
                }
                case 'lostdetail-employee-total':{
                    $objPHPExcel->createSheet();
                    $objDetailSheet = $objPHPExcel->setActiveSheetIndex(1);
                    $objDetailSheet->setTitle('Özet');
                    $titledetail = "Tiplerine Göre Operatör Kayıpları";
                    $sql_detail = 'SELECT a.losttype "Kayıp Tipi",sum(a.sure) "Süre Dk",round(sum(a.sure)/60,2) "Süre Saat"
                    from(
                        SELECT cld.client,cld.day,cld.jobrotation,cld.losttype
                            ,cld.employee,e.name
                            ,round(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/60,0) sure
                        from client_lost_details cld
                        join employees e on e.code=cld.employee
                        left join lost_group_details lgd on lgd.losttype=cld.losttype and lgd.lostgroup=\'OPVERIM\'
                        where cld.type in (\'l_e\',\'l_e_out\')
                        ' . (($_SERVER['HTTP_HOST'] == '172.16.1.149') ? ' and e.isoperator=true ' : '') . ' 
                        /*and cld.client not in (SELECT code from clients where workflow=\'CNC\')*/
                        ' . $str_where . ' 
                    )a
                    group by a.losttype
                    order by a.losttype
                    ';
                    //' . ($_SERVER['HTTP_HOST'] == '10.10.0.10' ? '' : ' and e.finish is null ') . '
                    $stmt_detail = $conn->prepare($sql_detail);
                    foreach ($obj_params as $key => $value) {
                        $stmt_detail->bindValue($key, $value);
                    }
                    $stmt_detail->execute();
                    $records_detail = $stmt_detail->fetchAll();
                    $col = 1;
                    $row = 2;
                    //$sheet = $objPHPExcel->getActiveSheet();
                    $_strcol = Coordinate::stringFromColumnIndex($col++);
                    $objDetailSheet->setCellValue($_strcol . $row, 'Sıra');
                    $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                    $objDetailSheet->getStyle($_strcol . $row)->getFont()->setBold(true);
                    $first = true;
                    foreach ($records_detail as $item => $kayit) {
                        if ($first) {
                            foreach ($kayit as $key => $val) {
                                $_strcol = Coordinate::stringFromColumnIndex($col++);
                                $objDetailSheet->setCellValue($_strcol . $row, strip_tags($key)/*strtoupper($this->replace_tr(strip_tags($key)))*/);
                                $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                                $objDetailSheet->getStyle($_strcol . $row)->getFont()->setBold(true);
                            }
                            $row++;
                            $first = false;
                            $_lc = $col;
                            /*
                            Başlık Alanı
                            */
                            $objDetailSheet->mergeCells("A1:" . $_strcol . "1");
                            $objDetailSheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                            $objDetailSheet->setCellValue("A1", $titledetail);
                            $objDetailSheet->getStyle("A1")->getFont()->setBold(true);
                        }
                        $col = 2;
                        $objDetailSheet->setCellValue("A" . $row, $row - 2);
                        foreach ($kayit as $rowtitle => $val) {
                            $_strcol = Coordinate::stringFromColumnIndex($col++);
                            $pos = strpos($val, "|");
                            if ($pos == 1) {
                                $val = substr($val, 2);
                                $objDetailSheet->setCellValueExplicit($_strcol . $row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/, DataType::TYPE_STRING);
                            } else {
                                $objDetailSheet->setCellValue($_strcol . $row, strip_tags($val)/*strtoupper($this->replace_tr(strip_tags($val)))*/);
                            }
                        }
                        $row++;
                    }
                    $col = 1;
                    foreach ($kayit as $rowtitle => $val) {
                        $_strcol = Coordinate::stringFromColumnIndex($col++);
                        $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                    }
                    break;
                }
                case 'mh-gec-tasima':{
                    if ($_SERVER['HTTP_HOST'] !== '10.0.0.101' && $_SERVER['HTTP_HOST'] !== '10.10.0.10') {
                        $objPHPExcel->createSheet();
                        $objDetailSheet = $objPHPExcel->setActiveSheetIndex(1);
                        $objDetailSheet->setTitle('Özet');
                        $objDetailSheet->mergeCells("A1:G1");
                        $objDetailSheet->setCellValue("A1", "GECİKEN TAŞIMA GÖREVLERİ ÖZET");
                        $objDetailSheet->getStyle("A1")->getFont()->setBold(true);
                        $objDetailSheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                        $sql_c = "select cm_2.tasiyici,cm_2.jobrotationteam,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                            ,sum(cm_2.yapilmasicildk)gecikendakika,sum(cm_2.changetime)changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and coalesce(cs.finish,CURRENT_TIMESTAMP(0)) and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.tasiyici,cm_2.jobrotationteam
                        order by case cm_2.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else cm_2.jobrotationteam end,cm_2.tasiyici";
                        $stmt_c = $conn->prepare($sql_c);
                        foreach ($obj_params as $key => $value) {
                            $stmt_c->bindValue($key, $value);
                        }
                        $stmt_c->execute();
                        $records_c = $stmt_c->fetchAll();
                        $team = '';
                        $col = 1;
                        $row = 3;
                        //$_strcol = Coordinate::stringFromColumnIndex($col++);
                        foreach ($records_c as $item => $kayit) {
                            if ($team !== $kayit["jobrotationteam"]) {
                                if ($team !== '') {
                                    $row++;
                                }
                                $team = $kayit["jobrotationteam"];
                                $objDetailSheet->setCellValue("A" . $row, $team);
                                $objDetailSheet->setCellValue("B" . $row, "Atanan Adet");
                                $objDetailSheet->setCellValue("C" . $row, "Geciken Adet");
                                $objDetailSheet->setCellValue("D" . $row, "Gecikme Dk");
                                $objDetailSheet->setCellValue("E" . $row, "Değişim Sn");
                                $objDetailSheet->getStyle("A" . $row . ":E" . $row)->getFont()->setBold(true);
                                $row++;
                            }
                            $objDetailSheet->setCellValue("A" . $row, $kayit["tasiyici"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["changetime"]);
                            $row++;
                        }

                        $sql_g = "select cm_2.tasiyici,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                            ,sum(cm_2.yapilmasicildk)gecikendakika,cast(sum(cm_2.yapilmasicildk)/60 as decimal(10,2))gecikensaat
                            ,round(sum(cm_2.changetime)/3600,2) changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.tasiyici
                        order by cm_2.tasiyici";
                        $stmt_g = $conn->prepare($sql_g);
                        foreach ($obj_params as $key => $value) {
                            $stmt_g->bindValue($key, $value);
                        }
                        $stmt_g->execute();
                        $records_g = $stmt_g->fetchAll();
                        $row++;
                        $objDetailSheet->setCellValue("A" . $row, "Genel Toplam");
                        $objDetailSheet->setCellValue("B" . $row, "Atanan Adet");
                        $objDetailSheet->setCellValue("C" . $row, "Geciken Adet");
                        $objDetailSheet->setCellValue("D" . $row, "Gecikme Dk");
                        $objDetailSheet->setCellValue("E" . $row, "Gecikme Sa");
                        $objDetailSheet->setCellValue("F" . $row, "Değişim Sa");
                        $objDetailSheet->getStyle("A" . $row . ":F" . $row)->getFont()->setBold(true);
                        $row++;
                        foreach ($records_g as $item => $kayit) {
                            $objDetailSheet->setCellValue("A" . $row, $kayit["tasiyici"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["gecikensaat"]);
                            $objDetailSheet->setCellValue("F" . $row, $kayit["changetime"]);
                            $row++;
                        }

                        $sql_v = "select cm_2.jobrotationteam,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                            ,sum(cm_2.yapilmasicildk)gecikendakika,cast(sum(cm_2.yapilmasicildk)/60 as decimal(10,2))gecikensaat
                            ,sum(cm_2.changetime)/3600 changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.jobrotationteam
                        order by case cm_2.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else cm_2.jobrotationteam end";
                        $stmt_v = $conn->prepare($sql_v);
                        foreach ($obj_params as $key => $value) {
                            $stmt_v->bindValue($key, $value);
                        }
                        $stmt_v->execute();
                        $records_v = $stmt_v->fetchAll();
                        $row++;
                        $objDetailSheet->setCellValue("A" . $row, "Vardiya");
                        $objDetailSheet->setCellValue("B" . $row, "Atanan Adet");
                        $objDetailSheet->setCellValue("C" . $row, "Geciken Adet");
                        $objDetailSheet->setCellValue("D" . $row, "Gecikme DK");
                        $objDetailSheet->setCellValue("E" . $row, "Gecikme Sa");
                        $objDetailSheet->setCellValue("F" . $row, "Değişim Sa");
                        $objDetailSheet->getStyle("A" . $row . ":E" . $row)->getFont()->setBold(true);
                        $row++;
                        $gorev = 0;
                        $geciken = 0;
                        $gecikendakika = 0;
                        $gecikensaat = 0;
                        $changetime = 0;
                        foreach ($records_v as $item => $kayit) {
                            $gorev += intval($kayit["gorev"]);
                            $geciken += intval($kayit["geciken"]);
                            $gecikendakika += intval($kayit["gecikendakika"]);
                            $gecikensaat += intval($kayit["gecikensaat"]);
                            $changetime += intval($kayit["changetime"]);
                            $objDetailSheet->setCellValue("A" . $row, $kayit["jobrotationteam"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["gecikensaat"]);
                            $objDetailSheet->setCellValue("F" . $row, $kayit["changetime"]);
                            $row++;
                        }
                        $objDetailSheet->getStyle("A" . $row . ":F" . $row)->getFont()->setBold(true);
                        $objDetailSheet->setCellValue("A" . $row, "Genel Toplam");
                        $objDetailSheet->setCellValue("B" . $row, $gorev);
                        $objDetailSheet->setCellValue("C" . $row, $geciken);
                        $objDetailSheet->setCellValue("D" . $row, $gecikendakika);
                        $objDetailSheet->setCellValue("E" . $row, $gecikensaat);
                        $objDetailSheet->setCellValue("F" . $row, $changetime);
                        $row++;

                        $sql_e = "select cm_2.sicil,cm_2.adsoyad,cm_2.jobrotationteam,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                            ,sum(cm_2.yapilmasicildk)gecikendakika,cast(sum(cm_2.yapilmasicildk)/60 as decimal(10,2))gecikensaat
                            ,sum(cm_2.changetime)/3600 changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.sicil,cm_2.adsoyad,cm_2.jobrotationteam
                        order by case cm_2.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else cm_2.jobrotationteam end,cm_2.sicil,cm_2.adsoyad";
                        $stmt_e = $conn->prepare($sql_e);
                        foreach ($obj_params as $key => $value) {
                            $stmt_e->bindValue($key, $value);
                        }
                        $stmt_e->execute();
                        $records_e = $stmt_e->fetchAll();
                        $row++;
                        $objDetailSheet->setCellValue("A" . $row, "Sicil");
                        $objDetailSheet->setCellValue("B" . $row, "Personel");
                        $objDetailSheet->setCellValue("C" . $row, "Vardiya");
                        $objDetailSheet->setCellValue("D" . $row, "Atanan Adet");
                        $objDetailSheet->setCellValue("E" . $row, "Geciken Adet");
                        $objDetailSheet->setCellValue("F" . $row, "Gecikme Dk");
                        $objDetailSheet->setCellValue("G" . $row, "Gecikme Sa");
                        $objDetailSheet->setCellValue("H" . $row, "Değişim Sa");
                        $objDetailSheet->getStyle("A" . $row . ":H" . $row)->getFont()->setBold(true);
                        $row++;
                        foreach ($records_e as $item => $kayit) {
                            $objDetailSheet->setCellValue("A" . $row, $kayit["sicil"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["adsoyad"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["jobrotationteam"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("F" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("G" . $row, $kayit["gecikensaat"]);
                            $objDetailSheet->setCellValue("H" . $row, $kayit["changetime"]);
                            $row++;
                        }
                        $col = 1;
                        foreach ($kayit as $rowtitle => $val) {
                            $_strcol = Coordinate::stringFromColumnIndex($col++);
                            $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                        }
                    }
                    break;
                }
                case 'mh-gec-tasima-client':{
                    if ($_SERVER['HTTP_HOST'] !== '10.0.0.101' && $_SERVER['HTTP_HOST'] !== '10.10.0.10') {
                        $objPHPExcel->createSheet();
                        $objDetailSheet = $objPHPExcel->setActiveSheetIndex(1);
                        $objDetailSheet->setTitle('Özet');
                        $objDetailSheet->mergeCells("A1:G1");
                        $objDetailSheet->setCellValue("A1", "GECİKEN TAŞIMA GÖREVLERİ ÖZET");
                        $objDetailSheet->getStyle("A1")->getFont()->setBold(true);
                        $objDetailSheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                        $sql_c = "select cm_2.tasiyici,cm_2.jobrotationteam,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                            ,sum(cm_2.yapilmasicildk)gecikendakika,sum(cm_2.changetime)changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and coalesce(cs.finish,CURRENT_TIMESTAMP(0)) and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationsource and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.tasiyici,cm_2.jobrotationteam
                        order by case cm_2.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else cm_2.jobrotationteam end,cm_2.tasiyici";
                        $stmt_c = $conn->prepare($sql_c);
                        foreach ($obj_params as $key => $value) {
                            $stmt_c->bindValue($key, $value);
                        }
                        $stmt_c->execute();
                        $records_c = $stmt_c->fetchAll();
                        $team = '';
                        $col = 1;
                        $row = 3;
                        //$_strcol = Coordinate::stringFromColumnIndex($col++);
                        foreach ($records_c as $item => $kayit) {
                            if ($team !== $kayit["jobrotationteam"]) {
                                if ($team !== '') {
                                    $row++;
                                }
                                $team = $kayit["jobrotationteam"];
                                $objDetailSheet->setCellValue("A" . $row, $team);
                                $objDetailSheet->setCellValue("B" . $row, "Atanan Adet");
                                $objDetailSheet->setCellValue("C" . $row, "Geciken Adet");
                                $objDetailSheet->setCellValue("D" . $row, "Gecikme Dk");
                                $objDetailSheet->setCellValue("E" . $row, "Değişim Sn");
                                $objDetailSheet->getStyle("A" . $row . ":E" . $row)->getFont()->setBold(true);
                                $row++;
                            }
                            $objDetailSheet->setCellValue("A" . $row, $kayit["tasiyici"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["changetime"]);
                            $row++;
                        }

                        $sql_g = "select cm_2.tasiyici,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                            ,sum(cm_2.yapilmasicildk)gecikendakika,cast(sum(cm_2.yapilmasicildk)/60 as decimal(10,2))gecikensaat
                            ,round(sum(cm_2.changetime)/3600,2) changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationsource and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.tasiyici
                        order by cm_2.tasiyici";
                        $stmt_g = $conn->prepare($sql_g);
                        foreach ($obj_params as $key => $value) {
                            $stmt_g->bindValue($key, $value);
                        }
                        $stmt_g->execute();
                        $records_g = $stmt_g->fetchAll();
                        $row++;
                        $objDetailSheet->setCellValue("A" . $row, "Genel Toplam");
                        $objDetailSheet->setCellValue("B" . $row, "Atanan Adet");
                        $objDetailSheet->setCellValue("C" . $row, "Geciken Adet");
                        $objDetailSheet->setCellValue("D" . $row, "Gecikme Dk");
                        $objDetailSheet->setCellValue("E" . $row, "Gecikme Sa");
                        $objDetailSheet->setCellValue("F" . $row, "Değişim Sa");
                        $objDetailSheet->getStyle("A" . $row . ":F" . $row)->getFont()->setBold(true);
                        $row++;
                        foreach ($records_g as $item => $kayit) {
                            $objDetailSheet->setCellValue("A" . $row, $kayit["tasiyici"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["gecikensaat"]);
                            $objDetailSheet->setCellValue("F" . $row, $kayit["changetime"]);
                            $row++;
                        }

                        $sql_v = "select cm_2.jobrotationteam,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                        ,sum(cm_2.yapilmasicildk)gecikendakika,cast(sum(cm_2.yapilmasicildk)/60 as decimal(10,2))gecikensaat
                            ,sum(cm_2.changetime)/3600 changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationsource and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.jobrotationteam
                        order by case cm_2.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else cm_2.jobrotationteam end";
                        $stmt_v = $conn->prepare($sql_v);
                        foreach ($obj_params as $key => $value) {
                            $stmt_v->bindValue($key, $value);
                        }
                        $stmt_v->execute();
                        $records_v = $stmt_v->fetchAll();
                        $row++;
                        $objDetailSheet->setCellValue("A" . $row, "Vardiya");
                        $objDetailSheet->setCellValue("B" . $row, "Atanan Adet");
                        $objDetailSheet->setCellValue("C" . $row, "Geciken Adet");
                        $objDetailSheet->setCellValue("D" . $row, "Gecikme DK");
                        $objDetailSheet->setCellValue("E" . $row, "Gecikme Sa");
                        $objDetailSheet->setCellValue("F" . $row, "Değişim Sa");
                        $objDetailSheet->getStyle("A" . $row . ":E" . $row)->getFont()->setBold(true);
                        $row++;
                        $gorev = 0;
                        $geciken = 0;
                        $gecikendakika = 0;
                        $gecikensaat = 0;
                        $changetime = 0;
                        foreach ($records_v as $item => $kayit) {
                            $gorev += intval($kayit["gorev"]);
                            $geciken += intval($kayit["geciken"]);
                            $gecikendakika += intval($kayit["gecikendakika"]);
                            $gecikensaat += intval($kayit["gecikensaat"]);
                            $changetime += intval($kayit["changetime"]);
                            $objDetailSheet->setCellValue("A" . $row, $kayit["jobrotationteam"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["gecikensaat"]);
                            $objDetailSheet->setCellValue("F" . $row, $kayit["changetime"]);
                            $row++;
                        }
                        $objDetailSheet->getStyle("A" . $row . ":F" . $row)->getFont()->setBold(true);
                        $objDetailSheet->setCellValue("A" . $row, "Genel Toplam");
                        $objDetailSheet->setCellValue("B" . $row, $gorev);
                        $objDetailSheet->setCellValue("C" . $row, $geciken);
                        $objDetailSheet->setCellValue("D" . $row, $gecikendakika);
                        $objDetailSheet->setCellValue("E" . $row, $gecikensaat);
                        $objDetailSheet->setCellValue("F" . $row, $changetime);
                        $row++;

                        $sql_e = "select cm_2.sicil,cm_2.adsoyad,cm_2.jobrotationteam,count(*)gorev,sum(cm_2.yapilmasiciladet)geciken
                            ,sum(cm_2.yapilmasicildk)gecikendakika,cast(sum(cm_2.yapilmasicildk)/60 as decimal(10,2))gecikensaat
                            ,sum(cm_2.changetime)/3600 changetime
                        from (
                            select cm_1.sicil,cm_1.adsoyad,cm_1.tasiyici
                                ,cm_1.sure-cm_1.molasuresi netsureis
                                ,cm_1.suresicil-cm_1.molasuresi netsuresicil
                                ,case when cm_1.sure>cm_1.hedef then 'GECIKME' else '' end yapilma
                                ,case when cm_1.sure>cm_1.hedef then 1 else 0 end yapilmaadet
                                ,case when cm_1.sure>cm_1.hedef then round(cm_1.sure-cm_1.hedef) else 0 end yapilmadk
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 'GECIKME' else '' end yapilmasicil
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then 1 else 0 end yapilmasiciladet
                                ,case when cm_1.suresicil-cm_1.molasuresi>cm_1.hedef then round(cm_1.suresicil-cm_1.molasuresi-cm_1.hedef) else 0 end yapilmasicildk
                                ,cm_1.changetime 
                                ,cm_1.jobrotation
                                ,case cm_1.jobrotationteam 
                                    when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' 
                                    when 'MUSTAFA HAN' then 'V2 Harun ASAN' 
                                    when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' 
                                    else cm_1.jobrotationteam end as jobrotationteam
                            from (
                                SELECT cm.id,cm.empcode sicil,cm.empname adsoyad,cm.carrier tasiyici,cm.erprefnumber
                                    ,cm.code,cm.stockcode,cm.number opno,cm.casename kasa,cm.quantitynecessary
                                    ,cm.locationsource cikis,cm.locationdestination varis,cm.starttime baslangic
                                    ,cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60 hedef
                                    ,cm.assignmenttime atama,cm.actualfinishtime bitis,round(COALESCE(cm.breaktimeseconds,0)/60) molasuresi
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) sure
                                    ,round(cast(extract(epoch from (COALESCE(cm.actualfinishtime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.assignmenttime,CURRENT_TIMESTAMP)::timestamp)) as integer)/60) suresicil
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                    ,coalesce(cct.changetime,0)changetime
                                from case_movements cm
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join case_change_times cct on cct.casename=cm.casename
                                left join clients c on c.code=cm.locationsource and c.workflow<>'Gölge Cihaz'
                                where cm.actualfinishtime is not null and cm.carrier is not null and cm.status='CLOSED' and c.id is not null 
                                " . $str_where . " 
                            )cm_1
                        )cm_2
                        where cm_2.jobrotationteam is not null
                        group by cm_2.sicil,cm_2.adsoyad,cm_2.jobrotationteam
                        order by case cm_2.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else cm_2.jobrotationteam end,cm_2.sicil,cm_2.adsoyad";
                        $stmt_e = $conn->prepare($sql_e);
                        foreach ($obj_params as $key => $value) {
                            $stmt_e->bindValue($key, $value);
                        }
                        $stmt_e->execute();
                        $records_e = $stmt_e->fetchAll();
                        $row++;
                        $objDetailSheet->setCellValue("A" . $row, "Sicil");
                        $objDetailSheet->setCellValue("B" . $row, "Personel");
                        $objDetailSheet->setCellValue("C" . $row, "Vardiya");
                        $objDetailSheet->setCellValue("D" . $row, "Atanan Adet");
                        $objDetailSheet->setCellValue("E" . $row, "Geciken Adet");
                        $objDetailSheet->setCellValue("F" . $row, "Gecikme Dk");
                        $objDetailSheet->setCellValue("G" . $row, "Gecikme Sa");
                        $objDetailSheet->setCellValue("H" . $row, "Değişim Sa");
                        $objDetailSheet->getStyle("A" . $row . ":H" . $row)->getFont()->setBold(true);
                        $row++;
                        foreach ($records_e as $item => $kayit) {
                            $objDetailSheet->setCellValue("A" . $row, $kayit["sicil"]);
                            $objDetailSheet->setCellValue("B" . $row, $kayit["adsoyad"]);
                            $objDetailSheet->setCellValue("C" . $row, $kayit["jobrotationteam"]);
                            $objDetailSheet->setCellValue("D" . $row, $kayit["gorev"]);
                            $objDetailSheet->setCellValue("E" . $row, $kayit["geciken"]);
                            $objDetailSheet->setCellValue("F" . $row, $kayit["gecikendakika"]);
                            $objDetailSheet->setCellValue("G" . $row, $kayit["gecikensaat"]);
                            $objDetailSheet->setCellValue("H" . $row, $kayit["changetime"]);
                            $row++;
                        }
                        $col = 1;
                        foreach ($kayit as $rowtitle => $val) {
                            $_strcol = Coordinate::stringFromColumnIndex($col++);
                            $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                        }
                    }
                    break;
                }
                case 'mh-performans':{
                    if ($_SERVER['HTTP_HOST'] !== '10.0.0.101' && $_SERVER['HTTP_HOST'] !== '10.10.0.10') {
                        $objPHPExcel->createSheet();
                        $objDetailSheet = $objPHPExcel->setActiveSheetIndex(1);
                        $objDetailSheet->setTitle('Özet');
                        $objDetailSheet->mergeCells("A1:K1");
                        $objDetailSheet->setCellValue("A1", "VERİM");
                        $objDetailSheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                        $sql_v = "select count(*)atanan,sum(a.giden)giden,sum(a.yapilan)yapilan,sum(a.geciken)geciken,sum(a.yapilan)-sum(a.geciken)zamaninda
                        ,round(100*(sum(a.giden))/count(*),2) iskaybi
                        ,round(100*(sum(a.yapilan)-sum(a.geciken))/count(*),2) verim
                        ,round(case when sum(a.yapilan)>0 then 100*(sum(a.yapilan)-sum(a.geciken))/sum(a.yapilan) else 0 end ,2) tempo
                        ,case a.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else a.jobrotationteam end jobrotationteam
                        from (
                            SELECT cmd.carrier tasiyicikod,cmd.empcode sicil,cmd.empname adsoyad,case when cmd.donetime is null then 0 else 1 end yapilan
                                ,case when cmd.donetime is null then 1 else 0 end giden
                                ,case when cmd.donetime is not null and cm.actualfinishtime-coalesce(cm.breaktimeseconds,0)*interval '1 second'>
                                    cm.assignmenttime+(cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer))*interval '1 second' then 1 else 0 end geciken
                                ,cs.start,cs.finish,cs.day,cs.jobrotation
                                ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                            from case_movement_details cmd
                            join case_movements cm on cm.id=cmd.casemovementid
                            left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                            left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                            where c.id is not null and (cmd.canceltime is null or (cmd.canceltime is not null and cmd.description='Oturum kapatma zaman aşımı')) 
                                and cs.day is not null
                                and cm.assignmenttime between :t1_1 and :t1_2
                        )a
                        GROUP  BY a.jobrotationteam
                        having ( sum(a.yapilan)>0 or sum(a.geciken)>0 or (sum(a.yapilan)-sum(a.geciken))>0 )
                        order by case a.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else a.jobrotationteam end";
                        $stmt_v = $conn->prepare($sql_v);
                        foreach ($obj_params as $key => $value) {
                            $stmt_v->bindValue($key, $value);
                        }
                        $stmt_v->execute();
                        $records_v = $stmt_v->fetchAll();
                        $sayac = 0;
                        $toplamverim = 0;
                        foreach ($records_v as $item_v => $kayit_v) {
                            $toplamverim += $kayit_v["verim"];
                            if ($sayac === 0) {
                                $objDetailSheet->setCellValue("A2", $kayit_v["jobrotationteam"]);
                                $objDetailSheet->setCellValue("B2", $kayit_v["verim"]);
                                $objDetailSheet->setCellValue("A4", "Taşıyıcı");
                                $objDetailSheet->setCellValue("B4", "Verim");
                            }
                            if ($sayac === 1) {
                                $objDetailSheet->setCellValue("D2", $kayit_v["jobrotationteam"]);
                                $objDetailSheet->setCellValue("E2", $kayit_v["verim"]);
                                $objDetailSheet->setCellValue("D4", "Taşıyıcı");
                                $objDetailSheet->setCellValue("E4", "Verim");
                            }
                            if ($sayac === 2) {
                                $objDetailSheet->setCellValue("G2", $kayit_v["jobrotationteam"]);
                                $objDetailSheet->setCellValue("H2", $kayit_v["verim"]);
                                $objDetailSheet->setCellValue("G4", "Taşıyıcı");
                                $objDetailSheet->setCellValue("H4", "Verim");
                            }
                            $sayac++;
                        }
                        $objDetailSheet->setCellValue("J2", "Genel Verim");
                        $objDetailSheet->setCellValue("K2", round($toplamverim / $sayac, 2));
                        $objDetailSheet->setCellValue("J4", "Taşıyıcı");
                        $objDetailSheet->setCellValue("K4", "Verim");

                        $sql_t = "select a.tasiyicikod,count(*)atanan,sum(a.giden)giden,sum(a.yapilan)yapilan,sum(a.geciken)geciken,sum(a.yapilan)-sum(a.geciken)zamaninda
                            ,round(100*(sum(a.giden))/count(*),2) iskaybi
                            ,round(100*(sum(a.yapilan)-sum(a.geciken))/count(*),2) verim
                            ,round(case when sum(a.yapilan)>0 then 100*(sum(a.yapilan)-sum(a.geciken))/sum(a.yapilan) else 0 end ,2) tempo
                            ,case a.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else a.jobrotationteam end jobrotationteam
                            from (
                                SELECT cmd.carrier tasiyicikod,cmd.empcode sicil,cmd.empname adsoyad,case when cmd.donetime is null then 0 else 1 end yapilan
                                    ,case when cmd.donetime is null then 1 else 0 end giden
                                    ,case when cmd.donetime is not null and cm.actualfinishtime-coalesce(cm.breaktimeseconds,0)*interval '1 second'>
                                        cm.assignmenttime+(cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer))*interval '1 second' then 1 else 0 end geciken
                                    ,cs.start,cs.finish,cs.day,cs.jobrotation
                                    ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                                from case_movement_details cmd
                                join case_movements cm on cm.id=cmd.casemovementid
                                left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                                left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                                where c.id is not null and (cmd.canceltime is null or (cmd.canceltime is not null and cmd.description='Oturum kapatma zaman aşımı')) 
                                    and cs.day is not null
                                    and cm.assignmenttime between :t1_1 and :t1_2
                            )a
                            GROUP  BY a.tasiyicikod,a.jobrotationteam
                            having ( sum(a.yapilan)>0 or sum(a.geciken)>0 or (sum(a.yapilan)-sum(a.geciken))>0 )
                        order by a.tasiyicikod,case a.jobrotationteam when 'HASAN BAYER' then 'V1 Yaşar KOŞUN' when 'MUSTAFA HAN' then 'V2 Harun ASAN' when 'SERKAN CEYHAN' then 'V3 Ferdi YILMAZ' else a.jobrotationteam end";
                        $stmt_t = $conn->prepare($sql_t);
                        foreach ($obj_params as $key => $value) {
                            $stmt_t->bindValue($key, $value);
                        }
                        $stmt_t->execute();
                        $records_t = $stmt_t->fetchAll();
                        $sayac = 0;
                        $toplamverim = 0;
                        $tmtp_t = '';
                        $row = 5;
                        $sayactasiyici = 0;
                        foreach ($records_t as $item_t => $kayit_t) {
                            if ($tmtp_t !== $kayit_t["tasiyicikod"]) {
                                if ($sayac > 0) {
                                    $objDetailSheet->setCellValue("J" . ($row + $sayactasiyici), $tmtp_t);
                                    $objDetailSheet->setCellValue("K" . ($row + $sayactasiyici), round($toplamverim / $sayac, 2));
                                    $sayactasiyici++;
                                }
                                $toplamverim = 0;
                                $tmtp_t = $kayit_t["tasiyicikod"];
                                $sayac = 0;
                            }
                            $toplamverim += $kayit_t["verim"];
                            if ($sayac === 0) {
                                $objDetailSheet->setCellValue("A" . ($row + $sayactasiyici), $tmtp_t);
                                $objDetailSheet->setCellValue("B" . ($row + $sayactasiyici), $kayit_t["verim"]);
                            }
                            if ($sayac === 1) {
                                $objDetailSheet->setCellValue("D" . ($row + $sayactasiyici), $tmtp_t);
                                $objDetailSheet->setCellValue("E" . ($row + $sayactasiyici), $kayit_t["verim"]);
                            }
                            if ($sayac === 2) {
                                $objDetailSheet->setCellValue("G" . ($row + $sayactasiyici), $tmtp_t);
                                $objDetailSheet->setCellValue("H" . ($row + $sayactasiyici), $kayit_t["verim"]);
                            }
                            $sayac++;
                        }
                        $objDetailSheet->setCellValue("J" . ($row + $sayactasiyici), $tmtp_t);
                        $objDetailSheet->setCellValue("K" . ($row + $sayactasiyici), round($toplamverim / $sayac, 2));

                        $col = 1;
                        for ($col = 1;$col < 12;$col++) {
                            $_strcol = Coordinate::stringFromColumnIndex($col);
                            $objDetailSheet->getColumnDimension($_strcol)->setAutoSize(true);
                        }

                        $sql_p_t = "select a.sicil,a.adsoyad,a.tasiyicikod,count(*)atanan,sum(a.giden)giden,sum(a.yapilan)yapilan,sum(a.geciken)geciken,sum(a.yapilan)-sum(a.geciken)zamaninda
                        ,round(100*(sum(a.giden))/count(*),2) iskaybi
                        ,round(100*(sum(a.yapilan)-sum(a.geciken))/count(*),2) verim
                        ,round(case when sum(a.yapilan)>0 then 100*(sum(a.yapilan)-sum(a.geciken))/sum(a.yapilan) else 0 end ,2) tempo
                        from (
                            SELECT cmd.carrier tasiyicikod,cmd.empcode sicil,cmd.empname adsoyad,case when cmd.donetime is null then 0 else 1 end yapilan
                                ,case when cmd.donetime is null then 1 else 0 end giden
                                ,case when cmd.donetime is not null and cm.actualfinishtime-coalesce(cm.breaktimeseconds,0)*interval '1 second'>
                                    cm.assignmenttime+(cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer))*interval '1 second' then 1 else 0 end geciken
                                ,cs.start,cs.finish,cs.day,cs.jobrotation
                                ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                            from case_movement_details cmd
                            join case_movements cm on cm.id=cmd.casemovementid
                            left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                            left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                            where c.id is not null and (cmd.canceltime is null or (cmd.canceltime is not null and cmd.description='Oturum kapatma zaman aşımı')) 
                                and cs.day is not null
                                and cm.assignmenttime between :t1_1 and :t1_2
                        )a
                        GROUP  BY a.sicil,a.adsoyad,a.tasiyicikod
                        having ( sum(a.yapilan)>0 or sum(a.geciken)>0 or (sum(a.yapilan)-sum(a.geciken))>0 )
                        order by a.sicil";
                        $stmt_p_t = $conn->prepare($sql_p_t);
                        foreach ($obj_params as $key => $value) {
                            $stmt_p_t->bindValue($key, $value);
                        }
                        $stmt_p_t->execute();
                        $records_p_t = $stmt_p_t->fetchAll();
                        $row = $row + $sayactasiyici + 2;
                        $objDetailSheet->setCellValue("A" . $row, "Sicil");
                        $objDetailSheet->mergeCells("B" . $row . ":C" . $row);
                        $objDetailSheet->setCellValue("B" . $row, "Adı");
                        $objDetailSheet->setCellValue("D" . $row, "Taşıyıcı");
                        $objDetailSheet->setCellValue("E" . $row, "Op. Verim");
                        $row++;
                        $sayac = 0;
                        foreach ($records_p_t as $item_p_t => $kayit_p_t) {
                            $objDetailSheet->setCellValue("A" . ($row + $sayac), $kayit_p_t["sicil"]);
                            $objDetailSheet->mergeCells("B" . ($row + $sayac) . ":C" . ($row + $sayac));
                            $objDetailSheet->setCellValue("B" . ($row + $sayac), $kayit_p_t["adsoyad"]);
                            $objDetailSheet->setCellValue("D" . ($row + $sayac), $kayit_p_t["tasiyicikod"]);
                            $objDetailSheet->setCellValue("E" . ($row + $sayac), $kayit_p_t["verim"]);
                            $sayac++;
                        }
                        $row = $row + $sayac + 2;
                        $sql_p = "select a.sicil,a.adsoyad,count(*)atanan,sum(a.giden)giden,sum(a.yapilan)yapilan,sum(a.geciken)geciken,sum(a.yapilan)-sum(a.geciken)zamaninda
                        ,round(100*(sum(a.giden))/count(*),2) iskaybi
                        ,round(100*(sum(a.yapilan)-sum(a.geciken))/count(*),2) verim
                        ,round(case when sum(a.yapilan)>0 then 100*(sum(a.yapilan)-sum(a.geciken))/sum(a.yapilan) else 0 end ,2) tempo
                        from (
                            SELECT cmd.carrier tasiyicikod,cmd.empcode sicil,cmd.empname adsoyad,case when cmd.donetime is null then 0 else 1 end yapilan
                                ,case when cmd.donetime is null then 1 else 0 end giden
                                ,case when cmd.donetime is not null and cm.actualfinishtime-coalesce(cm.breaktimeseconds,0)*interval '1 second'>
                                    cm.assignmenttime+(cast(extract(epoch from (COALESCE(cm.targettime,CURRENT_TIMESTAMP)::timestamp-COALESCE(cm.starttime,CURRENT_TIMESTAMP)::timestamp)) as integer))*interval '1 second' then 1 else 0 end geciken
                                ,cs.start,cs.finish,cs.day,cs.jobrotation
                                ,(SELECT jobrotationteam from job_rotation_employees where /*isovertime=false and*/ day=cs.day and jobrotation=cs.jobrotation limit 1)jobrotationteam
                            from case_movement_details cmd
                            join case_movements cm on cm.id=cmd.casemovementid
                            left join carrier_sessions cs on cm.actualfinishtime between cs.start and cs.finish and cm.carrier=cs.carrier and cm.empcode=cs.empcode
                            left join clients c on c.code=cm.locationdestination and c.workflow<>'Gölge Cihaz'
                            where c.id is not null and (cmd.canceltime is null or (cmd.canceltime is not null and cmd.description='Oturum kapatma zaman aşımı')) 
                                and cs.day is not null
                                and cm.assignmenttime between :t1_1 and :t1_2
                        )a
                        GROUP  BY a.sicil,a.adsoyad
                        having ( sum(a.yapilan)>0 or sum(a.geciken)>0 or (sum(a.yapilan)-sum(a.geciken))>0 )
                        order by a.sicil";
                        $stmt_p = $conn->prepare($sql_p);
                        foreach ($obj_params as $key => $value) {
                            $stmt_p->bindValue($key, $value);
                        }
                        $stmt_p->execute();
                        $records_p = $stmt_p->fetchAll();
                        // $row=$row+$sayactasiyici+2;
                        $objDetailSheet->setCellValue("A" . $row, "Sicil");
                        $objDetailSheet->mergeCells("B" . $row . ":C" . $row);
                        $objDetailSheet->setCellValue("B" . $row, "Adı");
                        $objDetailSheet->setCellValue("D" . $row, "Op. G. Verim");
                        $row++;
                        $sayac = 0;
                        foreach ($records_p as $item_p => $kayit_p) {
                            $objDetailSheet->setCellValue("A" . ($row + $sayac), $kayit_p["sicil"]);
                            $objDetailSheet->mergeCells("B" . ($row + $sayac) . ":C" . ($row + $sayac));
                            $objDetailSheet->setCellValue("B" . ($row + $sayac), $kayit_p["adsoyad"]);
                            $objDetailSheet->setCellValue("D" . ($row + $sayac), $kayit_p["verim"]);
                            $sayac++;
                        }
                    }
                    break;
                }
            }
        }
        $sheet->setTitle('Liste');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // create the writer
        $objWriter = new Xlsx($objPHPExcel);
        //$objWriter->setOffice2003Compatibility(true);
        //$objWriter->setIncludeCharts(TRUE);
        //$objPHPExcel->disconnectWorksheets();
        //unset($objPHPExcel);
        $response = new StreamedResponse(
            function () use ($objWriter) {
                $objWriter->save('php://output');
            },
            200,
            array()
        );
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename . '.xlsx'
        );
        //$response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
        //$response->headers->set('Pragma', 'public');
        //$response->headers->set('Cache-Control', 'maxage=1');
        //$response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Type', 'application/download');
        $response->headers->set('Expires', '0');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->sendHeaders();
        return $response;
    }

    /**
     * @Route(path="/Reports/{pg}/{lm}", requirements={"pg": "\d+","lm": "\d+"}, name="Reports-add", options={"expose"=true}, methods={"POST"})
     */
    public function postAction(Request $request, $_locale, $pg, $lm)
    {
        $uri = strtolower($request->getBaseUrl());
        $_data = json_decode($request->getContent());
        $reportname = $_data->reportname;
        $_arr_bold = array(
            'font'  => array(
                'bold'  => true
            ));
        $_arr_color_red = array(
            'font'  => array(
                'color' => array('rgb' => 'FF0000')
            ));
        $_arr_border = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => Border::BORDER_THIN
                )
            )
        );
        $table_header_row = 2;
        $_chart = false;
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $ondalikhane = 2;
        $ondalikhanegoster = 2;
        $objPHPExcel = new Spreadsheet();
        if ($reportname === 'AnalyzeLost') {
            $_chart = true;
            $name = "KayipAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Kayip_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Kayıp Tipi");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->client_lost_details;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->losttype);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("Kayıp Analizi");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeLostClient') {
            $_chart = true;
            $name = "KayipIstasyonAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Kayip_Istasyon_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "İstasyon");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->client_lost_details;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->client);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title($_data->losttype . " Kayıp Süreler (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeLostOEE') {
            $_chart = true;
            $name = "KayipAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Kayip_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Kayıp Tipi");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->client_lost_details;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->lostgroupcode);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("Kayıp Analizi");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeLostOperation') {
            $_chart = true;
            $name = "KayipOperasyonAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Kayip_Operasyon_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Operasyon");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->lost_operation;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->task);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title($_data->losttype . " Kayıp Süreler (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeOperationLost') {
            $_chart = true;
            $name = "OperasyonKayipAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Operasyon_Kayip_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Operasyon");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->operation_lost;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->task);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("Kayıp Süreler (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeOperationLostDetail') {
            $_chart = true;
            $name = "OperasyonKayipDetayAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Operasyon_Kayip_Detay_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Kayıp Tipi");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->operation_lost_detail;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->losttype);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title($_data->opname . " Kayıp Süreler (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeLostOperator') {
            $_chart = true;
            $name = "KayipOperatorAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Kayip_Operator_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Operatör");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->lost_operator;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->employee . '-' . $row->employeename);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title($_data->losttype . " Kayıp Süreler (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeShortLost') {
            $_chart = true;
            $name = "KisaDurusAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Kisa_Durus_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Kayıp Tipi");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->client_lost_details;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->losttype);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("Kısa Duruş Analizi");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeShortLostOEE') {
            $_chart = true;
            $name = "KisaDurusAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Kisa_Durus_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Kayıp Tipi");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->client_lost_details;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->lostgroupcode);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("Kısa Duruş Analizi");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeSlowdown') {
            $_chart = true;
            $name = "IstasyonHizKaybiAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Istasyon_Hiz_Kaybi_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "İstasyon");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->client_slowdown;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->client);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("İstasyon Hız Kayıpları (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'AnalyzeSlowdownOperation') {
            $_chart = true;
            $name = "OperasyonHizKaybiAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $ws_cld = "Operasyon_Hiz_Kaybi_Analizi";
            $objWorksheet->setTitle($ws_cld);
            $objWorksheet->setCellValue("A" . ($table_header_row - 1), "Operasyon");
            $objWorksheet->setCellValue("B" . ($table_header_row - 1), "Süre (dk)");
            $_cld = $_data->client_slowdown;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objWorksheet->setCellValue("A" . $satir, $row->opname);
                $objWorksheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("Operasyon Hız Kayıpları (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart2',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'DashboardClient') {
            $_chart = true;
            $name = "IstasyonAnalizi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objOEESheet = $objPHPExcel->setActiveSheetIndex(0);
            $objOEESheet->setTitle("OEE");

            $objOEESheet->setCellValue("B13", "ÜRETİM MÜD.");
            $objOEESheet->setCellValue("C13", "FABRİKA MÜD.");
            $objOEESheet->setCellValue("D13", "GENEL MÜD.");

            $objOEESheet->setCellValue("A14", "Kullanılabilirlik");
            $objOEESheet->setCellValue("A15", "Performans");
            $objOEESheet->setCellValue("A16", "Kalite");
            $objOEESheet->setCellValue("A17", "OEE");
            $_oee_data = $_data->oee[0];
            $objOEESheet->setCellValue("B14", round($_oee_data->kul_ur, $ondalikhanegoster));
            $objOEESheet->setCellValue("B15", round($_oee_data->per_ur, $ondalikhanegoster));
            $objOEESheet->setCellValue("B16", round($_oee_data->kal_ur, $ondalikhanegoster));
            $objOEESheet->setCellValue("B17", round($_oee_data->oee_ur, $ondalikhanegoster));

            $objOEESheet->setCellValue("C14", round($_oee_data->kul_fab / 2, $ondalikhanegoster));
            $objOEESheet->setCellValue("C15", round($_oee_data->per_fab / 3, $ondalikhanegoster));
            $objOEESheet->setCellValue("C16", round($_oee_data->kal_fab / 4, $ondalikhanegoster));
            $objOEESheet->setCellValue("C17", round($_oee_data->oee_fab / 5, $ondalikhanegoster));

            $objOEESheet->setCellValue("D14", round($_oee_data->kul_gen / 6, $ondalikhanegoster));
            $objOEESheet->setCellValue("D15", round($_oee_data->per_gen / 7, $ondalikhanegoster));
            $objOEESheet->setCellValue("D16", round($_oee_data->kal_gen / 8, $ondalikhanegoster));
            $objOEESheet->setCellValue("D17", round($_oee_data->oee_gen / 10, $ondalikhanegoster));

            for ($j = 1;$j <= 20;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objOEESheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $dataseriesLabels_oee = array(
                new DataSeriesValues('String', 'OEE!$a$14', null, 1),
                new DataSeriesValues('String', 'OEE!$a$15', null, 1),
                new DataSeriesValues('String', 'OEE!$a$16', null, 1),
                new DataSeriesValues('String', 'OEE!$a$17', null, 1),
            );
            $xAxisTickValues_oee = array(
                new DataSeriesValues('String', 'OEE!$B$13:$D$13', null, 12),	//
            );
            $dataSeriesValues_oee = array(
                new DataSeriesValues('Number', 'OEE!$B$14:$D$14', null, 12),
                new DataSeriesValues('Number', 'OEE!$B$15:$D$15', null, 12),
                new DataSeriesValues('Number', 'OEE!$B$16:$D$16', null, 12),
                new DataSeriesValues('Number', 'OEE!$B$17:$D$17', null, 12),
            );

            //	Build the dataseries
            $series_oee = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_oee) - 1),			// plotOrder
                $dataseriesLabels_oee,								// plotLabel
                $xAxisTickValues_oee,								// plotCategory
                $dataSeriesValues_oee								// plotValues
            );
            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            $series_oee->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_oee = new Layout();
            $layout_oee->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_oee = new PlotArea($layout_oee, array($series_oee));
            //	Set the chart legend
            $legend_oee = new Legend(Legend::POSITION_RIGHT, null, false);
            $title_oee = new Title("OEE");

            //	Create the chart
            $chart_oee = new Chart(
                'chart1',		// name
                $title_oee,			// title
                $legend_oee,		// legend
                $plotarea_oee,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_oee->setTopLeftPosition('A1');
            $chart_oee->setBottomRightPosition('G12');

            //	Add the chart to the worksheet
            $objOEESheet->addChart($chart_oee);

            $objPHPExcel->createSheet();
            $objProdListSheet = $objPHPExcel->setActiveSheetIndex(1);
            $ws_cpd = "Uretim_miktar";
            $objProdListSheet->setTitle($ws_cpd);
            $objProdListSheet->setCellValue("A" . ($table_header_row - 1), "Operasyon");
            $objProdListSheet->setCellValue("B" . ($table_header_row - 1), "Üretim Adedi");
            $_cpd = $_data->client_production_details;
            $satir = $table_header_row;
            foreach ($_cpd as $row) {
                $objProdListSheet->setCellValue("A" . $satir, $row->opname);
                $objProdListSheet->setCellValue("B" . $satir, $row->productioncurrent);
                $satir++;
            }
            $dataseriesLabels_cpd = array(new DataSeriesValues('String', $ws_cpd . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cpd = array(new DataSeriesValues('Number', $ws_cpd . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cpd = array(
                new DataSeriesValues('String', $ws_cpd . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cpd = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cpd) - 1),			// plotOrder
                $dataseriesLabels_cpd,								// plotLabel
                $xAxisTickValues_cpd,								// plotCategory
                $dataSeriesValues_cpd								// plotValues
            );
            $series_cpd->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cpd = new Layout();
            $layout_cpd->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cpd = new PlotArea($layout_cpd, array($series_cpd));
            //	Set the chart legend
            //$legend_cpd = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cpd = new Title("Üretim Miktarları");

            //	Create the chart
            $chart_cpd = new Chart(
                'chart2',		// name
                $title_cpd,			// title
                null,		// legend
                $plotarea_cpd,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cpd->setTopLeftPosition('C1');
            $chart_cpd->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objProdListSheet->addChart($chart_cpd);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objProdListSheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->createSheet();
            $objLostListSheet = $objPHPExcel->setActiveSheetIndex(2);
            $ws_cld = "Kayip_sure";
            $objLostListSheet->setTitle($ws_cld);
            $objLostListSheet->setCellValue("A" . ($table_header_row - 1), "Kayıp Tipi");
            $objLostListSheet->setCellValue("B" . ($table_header_row - 1), "Süre");
            $_cld = $_data->client_lost_details;
            $satir = $table_header_row;
            foreach ($_cld as $row) {
                $objLostListSheet->setCellValue("A" . $satir, $row->losttype);
                $objLostListSheet->setCellValue("B" . $satir, $row->suredk);
                $satir++;
            }
            $dataseriesLabels_cld = array(new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cld = array(new DataSeriesValues('Number', $ws_cld . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cld = array(
                new DataSeriesValues('String', $ws_cld . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cld = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cld) - 1),			// plotOrder
                $dataseriesLabels_cld,								// plotLabel
                $xAxisTickValues_cld,								// plotCategory
                $dataSeriesValues_cld								// plotValues
            );
            $series_cld->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cld = new Layout();
            $layout_cld->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cld = new PlotArea($layout_cld, array($series_cld));
            //	Set the chart legend
            //$legend_cld = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cld = new Title("Kayıp Süreler (dk)");

            //	Create the chart
            $chart_cld = new Chart(
                'chart3',		// name
                $title_cld,			// title
                null,		// legend
                $plotarea_cld,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cld->setTopLeftPosition('C1');
            $chart_cld->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objLostListSheet->addChart($chart_cld);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objLostListSheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->createSheet();
            $objProdSignalSheet = $objPHPExcel->setActiveSheetIndex(3);
            $ws_cs = "Sinyal_adetleri";
            $objProdSignalSheet->setTitle($ws_cs);
            $objProdSignalSheet->setCellValue("A" . ($table_header_row - 1), "Zaman");
            $objProdSignalSheet->setCellValue("B" . ($table_header_row - 1), "Sinyal Sayısı");
            $_cs = $_data->client_productions;
            $satir = $table_header_row;
            foreach ($_cs as $row) {
                $objProdSignalSheet->setCellValue("A" . $satir, $row->strstart);
                $objProdSignalSheet->setCellValue("B" . $satir, $row->production);
                $satir++;
            }
            $dataseriesLabels_cs = array(new DataSeriesValues('String', $ws_cs . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 1));
            $dataSeriesValues_cs = array(new DataSeriesValues('Number', $ws_cs . '!$B$' . $table_header_row . ':$B$' . ($satir - 1), null, 1));
            $xAxisTickValues_cs = array(
                new DataSeriesValues('String', $ws_cs . '!$A$' . $table_header_row . ':$A$' . ($satir - 1), null, 5),	//
            );
            $series_cs = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues_cs) - 1),			// plotOrder
                $dataseriesLabels_cs,								// plotLabel
                $xAxisTickValues_cs,								// plotCategory
                $dataSeriesValues_cs								// plotValues
            );
            $series_cs->setPlotDirection(DataSeries::DIRECTION_COL);
            $layout_cs = new Layout();
            $layout_cs->setShowVal(true);
            //	Set the series in the plot area
            $plotarea_cs = new PlotArea($layout_cs, array($series_cs));
            //	Set the chart legend
            //$legend_cs = new Legend(Legend::POSITION_RIGHT, NULL, false);
            $title_cs = new Title("Sinyal Adetleri");

            //	Create the chart
            $chart_cs = new Chart(
                'chart4',		// name
                $title_cs,			// title
                null,		// legend
                $plotarea_cs,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart_cs->setTopLeftPosition('C1');
            $chart_cs->setBottomRightPosition('P' . ($table_header_row + 20));

            //	Add the chart to the worksheet
            $objProdSignalSheet->addChart($chart_cs);

            for ($j = 1;$j <= 3;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objProdSignalSheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            $objPHPExcel->setActiveSheetIndex(0);
        }
        if ($reportname === 'EmployeeLostDay') {
            $_day = $_data->day;
            if ($_day == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "OperatorKayipSureGun_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code from lost_types where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();

            $toplam = 0;
            $durustoplamlari = new \stdClass();
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->setTitle("Kayıplar");

            $objWorksheet->setCellValue('A2', "PERSONEL");
            $objWorksheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $durustoplamlari->$_strcol = 0;
                $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $j++;
            }
            $_strcol = Coordinate::stringFromColumnIndex($j++);
            $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
            $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

            $objWorksheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
            $objWorksheet->mergeCells("A1:" . $_strcol . "1");
            $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', $_day . " DURUŞ SÜRELERİ");

            $sql_emp = "select * 
                from employees e 
                where e.isoperator=true 
                order by e.name";
            //and e.finish is null
            $conn = $em->getConnection();
            $stmt = $conn->prepare($sql_emp);
            $stmt->execute();
            $emp = $stmt->fetchAll();
            $r = 3;
            foreach ($emp as $prow => $personel) {
                $objWorksheet->setCellValue('A' . $r, $personel["name"]);
                $sqlopdurus = "
                select * from (SELECT code from lost_types where finish is null order by code)l
                left join (
                    select a.day,a.losttype,a.employee,sum(a.suresn)suresn from (
                        SELECT day,losttype,employee
                            ,cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                        from client_lost_details 
                        where day=:day /*and client not in (SELECT code from clients where workflow='CNC')*/
                        and type in ('l_e','l_e_out') and employee=:employee)a
                    GROUP BY a.day,a.losttype,a.employee
                )k on k.losttype=l.code
                order by l.code
            ";
                $conn = $em->getConnection();
                $stmt = $conn->prepare($sqlopdurus);
                $stmt->bindValue('day', $_day);
                $stmt->bindValue('employee', $personel["code"]);
                $stmt->execute();
                $lost = $stmt->fetchAll();
                $pdurustoplam = 0;
                $j = 2;
                foreach ($lost as $drow => $opdurus) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($opdurus["suresn"] != '') {
                        $durustoplamlari->$_strcol += $opdurus["suresn"];
                        $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                        $pdurustoplam += $opdurus["suresn"];
                    }
                    $j++;
                }
                if ($pdurustoplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $toplam += $pdurustoplam;
                $r++;
            }
            $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
            $objWorksheet->getStyle('A' . $r)->getFont()->setBold(true);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                if ($durustoplamlari->$_strcol > 0) {
                    $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $j++;
            }

            if ($toplam > 0) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
            }
            $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $objWorksheet->getRowDimension(2)->setRowHeight(150);
        }
        if ($reportname === 'EmployeeLostMonth') {
            $_year = $_data->year;
            $_month = $_data->month;
            if ($_year == null || $_month == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "OperatorKayipSureAy_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code from lost_types where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();

            $toplam = 0;
            $durustoplamlari = new \stdClass();
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->setTitle("Kayıplar");

            $objWorksheet->setCellValue('A2', "PERSONEL");
            $objWorksheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $durustoplamlari->$_strcol = 0;
                $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $j++;
            }
            $_strcol = Coordinate::stringFromColumnIndex($j++);
            $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
            $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

            $objWorksheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
            $objWorksheet->mergeCells("A1:" . $_strcol . "1");
            $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', "Yıl:$_year Ay:$_month DURUŞ SÜRELERİ");

            $sql_emp = "select * 
            from employees e 
            where e.isoperator=true 
            order by e.name";
            //and e.finish is null
            $conn = $em->getConnection();
            $stmt = $conn->prepare($sql_emp);
            $stmt->execute();
            $emp = $stmt->fetchAll();
            $r = 3;
            foreach ($emp as $prow => $personel) {
                $objWorksheet->setCellValue('A' . $r, $personel["name"]);
                $sqlopdurus = "
                    select * from (SELECT code from lost_types where finish is null order by code)l
                    left join (
                        select a.month,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT EXTRACT(month FROM day)as month,losttype,employee
                                ,cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from client_lost_details 
                            where cast(day as varchar(4))=:year and EXTRACT(month FROM day)=:month 
                            /*and client not in (SELECT code from clients where workflow='CNC')*/
                            and type in ('l_e','l_e_out') and employee=:employee)a
                        GROUP BY a.month,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                ";
                $conn = $em->getConnection();
                $stmt = $conn->prepare($sqlopdurus);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('month', $_month);
                $stmt->bindValue('employee', $personel["code"]);
                $stmt->execute();
                $lost = $stmt->fetchAll();
                $pdurustoplam = 0;
                $j = 2;
                foreach ($lost as $drow => $opdurus) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($opdurus["suresn"] != '') {
                        $durustoplamlari->$_strcol += $opdurus["suresn"];
                        $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                        $pdurustoplam += $opdurus["suresn"];
                    }
                    $j++;
                }
                if ($pdurustoplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $toplam += $pdurustoplam;
                $r++;
            }
            $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
            $objWorksheet->getStyle('A' . $r)->getFont()->setBold(true);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                if ($durustoplamlari->$_strcol > 0) {
                    $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $j++;
            }

            if ($toplam > 0) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
            }
            $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $objWorksheet->getRowDimension(2)->setRowHeight(150);
        }
        if ($reportname === 'EmployeeLostWeek') {
            $_year = $_data->year;
            $_week = $_data->week;
            if ($_year == null || $_week == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "OperatorKayipSureHafta_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code from lost_types where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $toplam = 0;
            $durustoplamlari = new \stdClass();
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->setTitle("Kayıplar");

            $objWorksheet->setCellValue('A2', "PERSONEL");
            $objWorksheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $durustoplamlari->$_strcol = 0;
                $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $j++;
            }
            $_strcol = Coordinate::stringFromColumnIndex($j++);
            $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
            $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

            $objWorksheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
            $objWorksheet->mergeCells("A1:" . $_strcol . "1");
            $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', "Yıl:$_year Hafta:$_week DURUŞ SÜRELERİ");

            $sql_emp = "select * 
            from employees e 
            where e.isoperator=true
            order by e.name";
            // and e.finish is null
            $conn = $em->getConnection();
            $stmt = $conn->prepare($sql_emp);
            $stmt->execute();
            $emp = $stmt->fetchAll();
            $r = 3;
            foreach ($emp as $prow => $personel) {
                $objWorksheet->setCellValue('A' . $r, $personel["name"]);
                $sqlopdurus = "
                    select * from (SELECT code from lost_types where finish is null order by code)l
                    left join (
                        select a.week,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT EXTRACT(WEEK FROM day)week,losttype,employee
                                ,cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from client_lost_details 
                            where cast(day as varchar(4))=:year and EXTRACT(WEEK FROM day)=:week 
                            /*and client not in (SELECT code from clients where workflow='CNC')*/
                            and type in ('l_e','l_e_out') and employee=:employee)a
                        GROUP BY a.week,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                ";
                $conn = $em->getConnection();
                $stmt = $conn->prepare($sqlopdurus);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('week', $_week);
                $stmt->bindValue('employee', $personel["code"]);
                $stmt->execute();
                $lost = $stmt->fetchAll();
                $pdurustoplam = 0;
                $j = 2;
                foreach ($lost as $drow => $opdurus) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($opdurus["suresn"] != '') {
                        $durustoplamlari->$_strcol += $opdurus["suresn"];
                        $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                        $pdurustoplam += $opdurus["suresn"];
                    }
                    $j++;
                }
                if ($pdurustoplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $toplam += $pdurustoplam;
                $r++;
            }
            $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
            $objWorksheet->getStyle('A' . $r)->getFont()->setBold(true);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                if ($durustoplamlari->$_strcol > 0) {
                    $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $j++;
            }

            if ($toplam > 0) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
            }
            $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $objWorksheet->getRowDimension(2)->setRowHeight(150);
        }
        if ($reportname === 'EmployeeLostYear') {
            $_year = $_data->year;
            if ($_year == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "OperatorKayipSureYil_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code from lost_types where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();

            $toplam = 0;
            $durustoplamlari = new \stdClass();
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->setTitle("Kayıplar");

            $objWorksheet->setCellValue('A2', "PERSONEL");
            $objWorksheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $durustoplamlari->$_strcol = 0;
                $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $j++;
            }
            $_strcol = Coordinate::stringFromColumnIndex($j++);
            $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
            $objWorksheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

            $objWorksheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
            $objWorksheet->mergeCells("A1:" . $_strcol . "1");
            $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', " Yıl:$_year DURUŞ SÜRELERİ");

            $sql_emp = "select * 
            from employees e 
            where e.isoperator=true
            order by e.name";
            // and e.finish is null
            $conn = $em->getConnection();
            $stmt = $conn->prepare($sql_emp);
            $stmt->execute();
            $emp = $stmt->fetchAll();
            $r = 3;
            foreach ($emp as $prow => $personel) {
                $objWorksheet->setCellValue('A' . $r, $personel["name"]);
                $sqlopdurus = "
                    select * from (SELECT code from lost_types where finish is null order by code)l
                    left join (
                        select a.year,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT cast(day as varchar(4))as year,losttype,employee
                                ,cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from client_lost_details 
                            where cast(day as varchar(4))=:year and type in ('l_e','l_e_out') 
                            /*and client not in (SELECT code from clients where workflow='CNC')*/
                            and employee=:employee)a
                        GROUP BY a.year,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                ";
                $conn = $em->getConnection();
                $stmt = $conn->prepare($sqlopdurus);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('employee', $personel["code"]);
                $stmt->execute();
                $lost = $stmt->fetchAll();
                $pdurustoplam = 0;
                $j = 2;
                foreach ($lost as $drow => $opdurus) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($opdurus["suresn"] != '') {
                        $durustoplamlari->$_strcol += $opdurus["suresn"];
                        $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                        $pdurustoplam += $opdurus["suresn"];
                    }
                    $j++;
                }
                if ($pdurustoplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $toplam += $pdurustoplam;
                $r++;
            }
            $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
            $objWorksheet->getStyle('A' . $r)->getFont()->setBold(true);

            $j = 2;
            foreach ($duruslar as $drow => $duruskod) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                if ($durustoplamlari->$_strcol > 0) {
                    $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                }
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $j++;
            }

            if ($toplam > 0) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
            }
            $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $objWorksheet->getRowDimension(2)->setRowHeight(150);
        }
        if ($reportname === 'EmployeeTaskSummary') {
            $_day = $_data->day;
            $_employee = $_data->employee;
            if ($_day == null || ($_employee == null || $_employee == '')) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $type_osk = "cld.type";
            if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                $type_osk = "case when cld.losttype in ('DENEMELER','NUMUNE URETIM','ROBOT KAYNAK PROGRAM YAZMA','TEK TARAFLI BURÇ ÇAKMA') then 'e_p' else cld.type end as \"type\"";
            }
            $name = "OperatorGorevDetay_" . $_day . "_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "
            select a.type,a.jobrotation,case when a.type in ('e_p','e_p_confirm','e_p_out') then 'ÜRETİM' when a.type in ('e_p_unconfirm') then 'ÜRETİM-Onaysız' when a.type in ('l_e','l_e_out') then 'OPERATÖR KAYIP' else 'GÖREV KAYIP' end tip
                ,a.client,/*a.day,a.jobrotation,*/ a.opname
                ,case when a.type in ('e_p','e_p_confirm','e_p_out') then a.productioncurrent else null end productioncurrent
                ,a.scrap
                ,case when a.type in ('e_p','e_p_confirm','e_p_out') then null else a.losttype end losttype
                ,a.start,a.finish,a.suresn,round(a.suresn/60)suredk,a.calculatedtpp
            from (	
                SELECT cpd.type,cpd.client,cpd.day,cpd.jobrotation,cpd.opname,sum(cpd.productioncurrent)productioncurrent,''losttype,cpd.start,cpd.finish,coalesce(cpd.calculatedtpp ,0) calculatedtpp
                    ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                    ,coalesce((SELECT sum(trd.quantityreject) 
                        from task_reject_details trd 
                        where trd.type='e_r' and trd.client=cpd.client and trd.day=cpd.day and trd.jobrotation=cpd.jobrotation and trd.employee=cpd.employee and trd.erprefnumber=cpd.erprefnumber),0)scrap
                from client_production_details cpd 
                where cpd.type in ('e_p','e_p_confirm') and cpd.day=:day and cpd.employee=:employee 
                and cpd.client not in (SELECT code from clients where workflow='CNC')
                GROUP BY cpd.type,cpd.client,cpd.day,cpd.jobrotation,cpd.opname,cpd.start,cpd.finish,cpd.calculatedtpp,cpd.employee,cpd.erprefnumber
                union all
                SELECT cpd.type,cpd.client,cpd.day,cpd.jobrotation,cpd.opname,sum(cpd.productioncurrent)productioncurrent,''losttype,cpd.start,cpd.finish,round(coalesce(cpd.calculatedtpp ,0)/coalesce(nullif((select count(*) from client_production_details cpd_out where cpd_out.client=cpd.client and cpd_out.day=cpd.day and cpd_out.jobrotation=cpd.jobrotation and cpd_out.start=cpd.start),0),1),2) calculatedtpp
                    ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer)/coalesce(nullif((select count(*) from client_production_details cpd_out where cpd_out.client=cpd.client and cpd_out.day=cpd.day and cpd_out.jobrotation=cpd.jobrotation and cpd_out.start=cpd.start),0),1) suresn
                    ,coalesce((SELECT sum(trd.quantityreject) 
                        from task_reject_details trd 
                        where trd.type='e_r' and trd.client=cpd.client and trd.day=cpd.day and trd.jobrotation=cpd.jobrotation and trd.employee=cpd.employee and trd.erprefnumber=cpd.erprefnumber),0)scrap
                from client_production_details cpd 
                where cpd.type='e_p_out' and cpd.day=:day and cpd.employee=:employee 
                and cpd.client not in (SELECT code from clients where workflow='CNC')
                GROUP BY cpd.type,cpd.client,cpd.day,cpd.jobrotation,cpd.opname,cpd.start,cpd.finish,cpd.calculatedtpp,cpd.employee,cpd.erprefnumber
                union all
                SELECT " . $type_osk . ",cld.client,cld.day,cld.jobrotation,cld.opname,0 productioncurrent,cld.losttype,cld.start,cld.finish,0 calculatedtpp
                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                    ,null scrap
                from client_lost_details cld 
                where cld.day=:day and cld.employee=:employee 
                and cld.client not in (SELECT code from clients where workflow='CNC')
                GROUP BY cld.type,cld.client,cld.day,cld.jobrotation,cld.opname,cld.losttype,cld.start,cld.finish,cld.employee,cld.erprefnumber
            )a
            where a.suresn>0
            order by a.start,a.type
            ";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->bindValue('employee', $_employee);
            $stmt->execute();
            $records = $stmt->fetchAll();

            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->getStyle('A1:O9')->getFont()->setBold(true);
            if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                $objWorksheet->mergeCells('A1:O1');
            } else {
                $objWorksheet->mergeCells('A1:J1');
            }
            $objWorksheet->getStyle('A1:O1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $objWorksheet->setCellValue('A1', "Operatör Görev Detay");

            $objWorksheet->setCellValue('A9', "İşlem Tipi");
            $objWorksheet->setCellValue('B9', "İstasyon");
            $objWorksheet->setCellValue('C9', "Operasyon");
            $objWorksheet->setCellValue('D9', "Üretim Adedi");
            $objWorksheet->setCellValue('E9', "Kayıp Tipi");
            $objWorksheet->setCellValue('F9', "Iskarta" . (($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') ? "/Karantina" : "") . " Adedi");
            $objWorksheet->setCellValue('G9', "Başlangıç");
            $objWorksheet->setCellValue('H9', "Bitiş");
            $objWorksheet->setCellValue('I9', "Süre(sn)");
            if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                $objWorksheet->setCellValue('J9', "Süre(dk)");
                $objWorksheet->setCellValue('K9', "Tip");
                $objWorksheet->setCellValue('L9', "Hedef Miktar");
                $objWorksheet->setCellValue('M9', "Performans %");
                $objWorksheet->setCellValue('N9', "Tempo %");
                $objWorksheet->setCellValue('O9', "Ürün Ağacı Süre (sn)");
            }
            $rowId = 10;
            $sure_calisma = 0;
            $sure_kayip = 0;
            $rowId_uretim = 0;
            $sure_kayip_uretim_ici = 0;
            $sure_kayip_uretim_disi = 0;
            $sure_mola_uretim_ici = 0;
            $sure_mola = 0;
            $prod = 0;
            $scrap = 0;
            $calisma_bitis = '';
            $calisma_sure = 0;
            $calisma_kayip_sure = 0;
            $calisma_uretilen = 0;
            $calculatedtpp = 0;
            $toplam_calisma_sure = 0;
            $perf_x_sure = 0;
            $tempo_x_sure = 0;
            $calisma_sayisi = 0;
            $jr = '';
            $last_finish = '';
            $arr_jr = [];
            foreach ($records as $row => $item) {
                if ($item['jobrotation'] !== '' && !in_array($item['jobrotation'], $arr_jr)) {
                    $arr_jr[] = $item['jobrotation'];
                }
                if ($jr === '' && $item['jobrotation'] !== '') {
                    $jr = $item['jobrotation'];
                }
                $objWorksheet->setCellValue('A' . $rowId, $item["tip"]);
                $objWorksheet->setCellValue('B' . $rowId, $item["client"]);
                $objWorksheet->setCellValue('C' . $rowId, $item["opname"]);
                $objWorksheet->setCellValue('D' . $rowId, $item["productioncurrent"]);
                $objWorksheet->setCellValue('E' . $rowId, $item["losttype"]);
                $objWorksheet->setCellValue('F' . $rowId, $item["scrap"]);
                $objWorksheet->setCellValue('G' . $rowId, $item["start"]);
                $objWorksheet->setCellValue('H' . $rowId, $item["finish"]);
                $objWorksheet->setCellValue('I' . $rowId, $item["suresn"]);
                $objWorksheet->setCellValue('J' . $rowId, $item["suredk"]);
                switch ($item["type"]) {
                    case 'e_p':
                    case 'e_p_confirm':
                    case 'e_p_out':
                        $color = 'bddbe6';
                        if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                            $objWorksheet->setCellValue('O' . $rowId, $item["calculatedtpp"]);
                            if ($rowId_uretim !== 0 && $calculatedtpp !== 0) {
                                $hedef = round(($calisma_sure - $sure_mola_uretim_ici) / $calculatedtpp);
                                if ($hedef > 0) {
                                    $perf = round(100 * $calisma_uretilen / round(($calisma_sure - $sure_mola_uretim_ici) / $calculatedtpp));
                                    $tempo = round(100 * $calisma_uretilen / round(($calisma_sure - $calisma_kayip_sure) / $calculatedtpp));
                                } else {
                                    $perf = 0;
                                    $tempo = 0;
                                }
                                $toplam_calisma_sure += $calisma_sure;
                                $perf_x_sure += $calisma_sure * $perf;
                                $tempo_x_sure += $calisma_sure * $tempo;
                                $calisma_sayisi++;
                                $objWorksheet->setCellValue('L' . $rowId_uretim, $hedef);
                                $objWorksheet->setCellValue('M' . $rowId_uretim, $perf);
                                $objWorksheet->setCellValue('N' . $rowId_uretim, $tempo);
                            }
                            $rowId_uretim = $rowId;
                            $calisma_bitis = $item["finish"];
                            $calisma_sure = $item["suresn"];
                            $calisma_kayip_sure = 0;
                            $calisma_uretilen = $item["productioncurrent"];
                            $calculatedtpp = $item["calculatedtpp"];
                            $sure_mola_uretim_ici = 0;
                        }
                        if ($last_finish !== $item["finish"]) {
                            $last_finish = $item["finish"];
                            $sure_calisma += (int)$item["suresn"];
                            $prod += (int)$item["productioncurrent"];
                            $scrap += (int)$item["scrap"];
                        }
                        break;
                    case 'e_p_unconfirm':
                        $color = '92d050';
                        if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                            $objWorksheet->setCellValue('O' . $rowId, $item["calculatedtpp"]);
                            if ($rowId_uretim !== 0 && $calculatedtpp !== 0) {
                                $hedef = round(($calisma_sure - $sure_mola_uretim_ici) / $calculatedtpp);
                                if ($hedef > 0) {
                                    $perf = round(100 * $calisma_uretilen / round(($calisma_sure - $sure_mola_uretim_ici) / $calculatedtpp));
                                    $tempo = round(100 * $calisma_uretilen / round(($calisma_sure - $calisma_kayip_sure) / $calculatedtpp));
                                } else {
                                    $perf = 0;
                                    $tempo = 0;
                                }
                                $toplam_calisma_sure += $calisma_sure;
                                $perf_x_sure += $calisma_sure * $perf;
                                $tempo_x_sure += $calisma_sure * $tempo;
                                $calisma_sayisi++;
                                $objWorksheet->setCellValue('L' . $rowId_uretim, $hedef);
                                $objWorksheet->setCellValue('M' . $rowId_uretim, $perf);
                                $objWorksheet->setCellValue('N' . $rowId_uretim, $tempo);
                            }
                            $rowId_uretim = $rowId;
                            $calisma_bitis = $item["finish"];
                            $calisma_sure = $item["suresn"];
                            $calisma_kayip_sure = 0;
                            $calisma_uretilen = $item["productioncurrent"];
                            $calculatedtpp = $item["calculatedtpp"];
                            $sure_mola_uretim_ici = 0;
                        }
                        if ($last_finish !== $item["finish"]) {
                            $last_finish = $item["finish"];
                            $sure_calisma += (int)$item["suresn"];
                            $prod += (int)$item["productioncurrent"];
                            $scrap += (int)$item["scrap"];
                        }
                        break;
                    case 'l_e':
                    case 'l_e_out':
                        $color = 'FF8080';
                        if (($item["losttype"] == 'ÇAY MOLASI' && (int)$item["suresn"] < 600) || ($item["losttype"] == 'YEMEK MOLASI' && (int)$item["suresn"] < 1800)) {
                            $color = 'b2d44a';
                        }
                        if ($item["losttype"] == 'ÇAY MOLASI' || $item["losttype"] == 'YEMEK MOLASI') {
                            $sure_mola += (int)$item["suresn"];
                        } else {
                            $sure_kayip += (int)$item["suresn"];
                        }
                        if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                            if ($calisma_bitis === '' || ($calisma_bitis !== '' && $calisma_bitis <= $item["start"])) {
                                $sure_kayip_uretim_disi += (int)$item["suresn"];
                                $objWorksheet->setCellValue('K' . $rowId, "Üretim Dışı");
                            } else {
                                if ($item["losttype"] == 'ÇAY MOLASI' || $item["losttype"] == 'YEMEK MOLASI') {
                                    $sure_mola_uretim_ici += (int)$item["suresn"];
                                }
                                $sure_kayip_uretim_ici += (int)$item["suresn"];
                                $calisma_kayip_sure += (int)$item["suresn"];
                                $objWorksheet->setCellValue('K' . $rowId, "Üretim İçi");
                            }
                        }
                        break;
                    case 'l_e_t':
                        $color = 'f7e5b1';
                        break;
                        //case 'l_e_t':
                        //    $color='9ebb87';//ded5ce
                        //    break;
                    default:
                        # code...
                        break;
                }
                if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                    $objWorksheet->getStyle("A$rowId:O$rowId")->getFill()->applyFromArray(array(
                        'fillType' => Fill::FILL_SOLID,
                        'startColor' => array('rgb' => $color)
                    ));
                } else {
                    $objWorksheet->getStyle("A$rowId:J$rowId")->getFill()->applyFromArray(array(
                        'fillType' => Fill::FILL_SOLID,
                        'startColor' => array('rgb' => $color)
                    ));
                }
                $rowId++;
            }
            if ($rowId_uretim !== 0 && $calculatedtpp !== 0) {
                $hedef = round(($calisma_sure - $sure_mola_uretim_ici) / $calculatedtpp);
                if ($hedef > 0) {
                    $perf = round(100 * $calisma_uretilen / round(($calisma_sure - $sure_mola_uretim_ici) / $calculatedtpp));
                    $tempo = round(100 * $calisma_uretilen / round(($calisma_sure - $calisma_kayip_sure) / $calculatedtpp));
                } else {
                    $perf = 0;
                    $tempo = 0;
                }
                $toplam_calisma_sure += $calisma_sure;
                $perf_x_sure += $calisma_sure * $perf;
                $tempo_x_sure += $calisma_sure * $tempo;
                $calisma_sayisi++;
                $objWorksheet->setCellValue('L' . $rowId_uretim, $hedef);
                $objWorksheet->setCellValue('M' . $rowId_uretim, $perf);
                $objWorksheet->setCellValue('N' . $rowId_uretim, $tempo);
            }
            $sql = "select * from employees where finish is null and code=:code";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('code', $_employee);
            $stmt->execute();
            $emp = $stmt->fetchAll();
            $sql = "SELECT * from job_rotation_employees where employee=:employee and day=:day";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('employee', $_employee);
            $stmt->bindValue('day', $_day);
            $stmt->execute();
            $jre = $stmt->fetchAll();
            $objWorksheet->setCellValue('A3', 'Tarih');
            $objWorksheet->mergeCells("B3:C3");
            $objWorksheet->setCellValue('B3', $_day);
            $objWorksheet->setCellValue('A4', 'Sicil');
            $objWorksheet->mergeCells("B4:C4");
            $objWorksheet->setCellValue('B4', $_employee . '-' . $emp[0]['name']);
            if ($_SERVER['HTTP_HOST'] === '10.0.0.101' || $_SERVER['HTTP_HOST'] === '192.168.1.40') {
                $worktime = 600;
                $breaktime = 60;
                $worktimeover = 0;
                $breaktimeover = 0;
                if (count($arr_jr) > 1) {
                    $worktimeover = 120;
                    $breaktimeover = 15;
                }
                $objWorksheet->setCellValue('A5', 'Toplam Üretilen Adet');
                $objWorksheet->setCellValue('B5', $prod);
                $objWorksheet->setCellValue('A6', 'Iskarta/Karantina Adet');
                $objWorksheet->setCellValue('B6', $scrap);
                $objWorksheet->setCellValue('A7', 'Kalite Performansı');
                $objWorksheet->setCellValue('B7', ($prod > 0 ? round(100 * ($prod - $scrap) / $prod, 2) : 0));
                $objWorksheet->setCellValue('E2', 'Çalışma Süresi (dk)');
                $objWorksheet->setCellValue('F2', $worktime);
                $objWorksheet->setCellValue('E3', 'Mola Süresi (dk)');
                $objWorksheet->setCellValue('F3', $breaktime);
                $objWorksheet->setCellValue('E4', 'Çalışma Süresi-FM (dk)');
                $objWorksheet->setCellValue('F4', $worktimeover);
                $objWorksheet->setCellValue('E5', 'Mola Süresi-FM (dk)');
                $objWorksheet->setCellValue('F5', $breaktimeover);
                $objWorksheet->setCellValue('E6', 'Toplam Çalışma Süresi (dk)');
                $objWorksheet->setCellValue('F6', round($sure_calisma / 60));
                $objWorksheet->setCellValue('G6', 'Net Çalışma Süresi (dk)');
                $objWorksheet->setCellValue('H6', round($sure_calisma / 60) - round($sure_kayip_uretim_ici / 60));
                $objWorksheet->setCellValue('G7', 'Üretim İçi Kayıp (dk)');
                $objWorksheet->setCellValue('H7', round($sure_kayip_uretim_ici / 60));
                $objWorksheet->setCellValue('G8', 'Üretim Dışı Kayıp (dk)');
                $objWorksheet->setCellValue('H8', round($sure_kayip_uretim_disi / 60));
                $objWorksheet->setCellValue('E7', 'Toplam Kayıp Süresi (dk)');
                $objWorksheet->setCellValue('F7', round(($sure_kayip + $sure_mola) / 60));
                $objWorksheet->setCellValue('E8', 'Boşta Geçen Süre (dk)');
                $objWorksheet->setCellValue('F8', $worktime + $worktimeover - round($sure_calisma / 60) - round($sure_kayip_uretim_disi / 60));
                $objWorksheet->setCellValue('G2', 'Kullanabilirlik %');
                $kul = round(100 * (round($sure_calisma / 60) - round($sure_kayip_uretim_ici / 60)) / ($worktime + $worktimeover - $breaktime - $breaktimeover), 2);
                $objWorksheet->setCellValue('H2', ($kul > 100 ? 100 : $kul));
                $objWorksheet->setCellValue('G3', 'Performans %');
                $objWorksheet->setCellValue('H3', ($calisma_sayisi > 0 ? round(($perf_x_sure / $toplam_calisma_sure), 2) : 0));
                $objWorksheet->setCellValue('G4', 'Tempo %');
                $objWorksheet->setCellValue('H4', ($calisma_sayisi > 0 ? round(($tempo_x_sure / $toplam_calisma_sure), 2) : 0));
            } else {
                $objWorksheet->setCellValue('A5', 'Vardiya');
                $objWorksheet->mergeCells("B5:C5");
                if (count($jre) > 0) {
                    $objWorksheet->setCellValue('B5', $jre[0]['jobrotationteam'] . '-' . $jre[0]['jobrotation']);
                } else {
                    $objWorksheet->setCellValue('B5', $jr);
                }
                $worktime = count($jre) > 0 ? $jre[0]['worktime'] : 480;
                $breaktime = count($jre) > 0 ? $jre[0]['breaktime'] : 50;
                $objWorksheet->setCellValue('E2', 'Çalışılabilecek Süre(dk)');
                $objWorksheet->setCellValue('F2', $worktime);
                $objWorksheet->setCellValue('E3', 'Tanımlı Mola Süresi(dk)');
                $objWorksheet->setCellValue('F3', $breaktime);
                $objWorksheet->setCellValue('E4', 'Toplam Çalışma Süresi(dk)');
                $objWorksheet->setCellValue('F4', round($sure_calisma / 60));
                $objWorksheet->setCellValue('E5', 'Toplam Kayıp Süresi(dk)');
                $objWorksheet->setCellValue('F5', round($sure_kayip / 60));
                $objWorksheet->setCellValue('E6', 'Toplam Mola Süresi(dk)');
                $objWorksheet->setCellValue('F6', round($sure_mola / 60));
                $objWorksheet->setCellValue('G5', 'Harcanan İş Gücü(dk)');
                $objWorksheet->setCellValue('H5', round($sure_calisma / 60) + round($sure_kayip / 60));
                $objWorksheet->setCellValue('G6', 'Kalan İş Gücü(dk)');
                $objWorksheet->setCellValue('H6', $worktime - round($sure_calisma / 60) - round($sure_kayip / 60));
                $sql = "select * from employee_efficiency where day=:day and employee=:employee order by fulltime desc limit 1";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('day', $_day);
                $stmt->bindValue('employee', $_employee);
                $stmt->execute();
                $emp_e = $stmt->fetchAll();
                if (count($emp_e) > 0) {
                    $objWorksheet->setCellValue('G2', 'Verim %');
                    $objWorksheet->setCellValue('H2', $emp_e[0]["efficiency"]);
                    $objWorksheet->setCellValue('G3', 'Tempo %');
                    $objWorksheet->setCellValue('H3', $emp_e[0]["tempo"]);
                }
            }

            for ($j = 1;$j < 20;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        if ($reportname === 'JobRotationEfficiencyMonth') {
            $_chart = true;
            $_year = $_data->year;
            if ($_year == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "VardiyaVerimAy_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $objWorksheet->getColumnDimension('B')->setWidth(6);
            $objWorksheet->getColumnDimension('O')->setAutoSize(true);
            $objWorksheet->getStyle('A1:n1')->getFont()->setBold(true);

            $objWorksheet->mergeCells("A1:n1");
            $objWorksheet->getStyle('A1:n1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', "VARDİYALAR AYLIK VERİM GRAFİĞİ");

            $r = 20;
            $objWorksheet->getStyle("a$r:R$r")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle("A$r:R$r")->getFont()->setBold(true);
            $objWorksheet->setCellValue('A' . $r++, 'Vardiyalar');
            $objWorksheet->getStyle("A21:a32")->getFont()->setBold(true);
            $objWorksheet->getStyle('A21:a32')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
            //$objWorksheet->getStyle('b21:b29')->getFont()->setSize(9);
            $sql = "SELECT code from job_rotation_teams where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records_jrt = $stmt->fetchAll();
            if (count($records_jrt) == 0) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ReportJobRotationEfficiencyMonth.errJobRotationTeams', array(), 'ReportJobRotationEfficiencyMonth'), 401);
            }
            $months = array(
                array("month" => "Ocak","number" => 1,"optime" => 0,"labor" => 0),
                array("month" => "Şubat","number" => 2,"optime" => 0,"labor" => 0),
                array("month" => "Mart","number" => 3,"optime" => 0,"labor" => 0),
                array("month" => "Nisan","number" => 4,"optime" => 0,"labor" => 0),
                array("month" => "Mayıs","number" => 5,"optime" => 0,"labor" => 0),
                array("month" => "Haziran","number" => 6,"optime" => 0,"labor" => 0),
                array("month" => "Temmuz","number" => 7,"optime" => 0,"labor" => 0),
                array("month" => "Ağustos","number" => 8,"optime" => 0,"labor" => 0),
                array("month" => "Eylül","number" => 9,"optime" => 0,"labor" => 0),
                array("month" => "Ekim","number" => 10,"optime" => 0,"labor" => 0),
                array("month" => "Kasım","number" => 11,"optime" => 0,"labor" => 0),
                array("month" => "Aralık","number" => 12,"optime" => 0,"labor" => 0)
            );
            $jre = array();
            foreach ($records_jrt as $row => $item) {
                $tmp = array(
                    array("month" => "Ocak","number" => 1,"optime" => 0,"labor" => 0),
                    array("month" => "Şubat","number" => 2,"optime" => 0,"labor" => 0),
                    array("month" => "Mart","number" => 3,"optime" => 0,"labor" => 0),
                    array("month" => "Nisan","number" => 4,"optime" => 0,"labor" => 0),
                    array("month" => "Mayıs","number" => 5,"optime" => 0,"labor" => 0),
                    array("month" => "Haziran","number" => 6,"optime" => 0,"labor" => 0),
                    array("month" => "Temmuz","number" => 7,"optime" => 0,"labor" => 0),
                    array("month" => "Ağustos","number" => 8,"optime" => 0,"labor" => 0),
                    array("month" => "Eylül","number" => 9,"optime" => 0,"labor" => 0),
                    array("month" => "Ekim","number" => 10,"optime" => 0,"labor" => 0),
                    array("month" => "Kasım","number" => 11,"optime" => 0,"labor" => 0),
                    array("month" => "Aralık","number" => 12,"optime" => 0,"labor" => 0)
                );
                $jre[] = array("jobrotation" => "","jobrotationteam" => $item["code"],"optime" => 0,"labor" => 0,"efficiency" => 0,"data" => $tmp);
            }
            if (count($jre) > 0) {
                $objWorksheet->mergeCells("A21:a23");
                $objWorksheet->setCellValue('A21', $jre[0]["jobrotationteam"]);
                $objWorksheet->setCellValue('b21', 'Y.İş');
                $objWorksheet->setCellValue('b22', 'İşgücü');
                $objWorksheet->setCellValue('b23', 'Verim');
                $objWorksheet->getStyle('b23:j23')->getFont()->setBold(true);
            }
            if (count($jre) > 1) {
                $objWorksheet->mergeCells("A24:a26");
                $objWorksheet->setCellValue('A24', $jre[1]["jobrotationteam"]);
                $objWorksheet->setCellValue('b24', 'Y.İş');
                $objWorksheet->setCellValue('b25', 'İşgücü');
                $objWorksheet->setCellValue('b26', 'Verim');
                $objWorksheet->getStyle('b26:j26')->getFont()->setBold(true);
            }
            if (count($jre) > 2) {
                $objWorksheet->mergeCells("A27:a29");
                $objWorksheet->setCellValue('A27', $jre[2]["jobrotationteam"]);
                $objWorksheet->setCellValue('b27', 'Y.İş');
                $objWorksheet->setCellValue('b28', 'İşgücü');
                $objWorksheet->setCellValue('b29', 'Verim');
                $objWorksheet->getStyle('b29:j29')->getFont()->setBold(true);
            }
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $objWorksheet->mergeCells("A30:a32");
            $objWorksheet->setCellValue('A30', 'Atölye');
            $objWorksheet->setCellValue('b30', 'Y.İş');
            $objWorksheet->setCellValue('b31', 'İşgücü');
            $objWorksheet->setCellValue('b32', 'Verim');
            $objWorksheet->getStyle('b32:j32')->getFont()->setBold(true);

            //$objWorksheet->getStyle('b34:o34')->getFont()->setBold(true);

            $sql = "select b.month,b.jobrotationteam
                ,round(sum(b.optime)/60,2)optime,round(sum(b.labor)/60,2)labor 
            from (
                SELECT a.day,cast(SUBSTRING(cast(a.day as varchar(10)),6,2) as integer) as month,a.jobrotation 
                    ,max(a.jobrotationteam_g)jobrotationteam,sum(a.optime)optime,sum(a.labor)labor
                from (
                    SELECT ee.day,ee.jobrotation
                        ,case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end jobrotationteam_g
                        ,sum(ee.optime) optime
                        ,sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0) labor
                    from employee_efficiency ee
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                    where cast(ee.day as varchar(4))=:year
                    GROUP BY ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                )a
                GROUP BY a.day,a.jobrotation
            )b
            group by b.month,b.jobrotationteam
            order by b.month,b.jobrotationteam";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('year', $_year);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $r = 20;
            $j = 3;
            foreach ($months as $row_months => $item_months) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, $item_months["month"]);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            if (count($records) > 0) {
                foreach ($records as $row => $item) {
                    $idx = 0;
                    foreach ($jre as $row_team => $item_team) {
                        if ($item_team["jobrotationteam"] == $item["jobrotationteam"]) {
                            $jre[$idx]["optime"] += floatval($item["optime"]);
                            $jre[$idx]["labor"] += floatval($item["labor"]);
                            $idx2 = 0;
                            foreach ($jre[$idx]["data"] as $row_months => $item_months) {
                                if ($item_months["number"] == $item["month"]) {
                                    $jre[$idx]["data"][$idx2]["optime"] = $item["optime"];
                                    $jre[$idx]["data"][$idx2]["labor"] = $item["labor"];
                                }
                                $idx2++;
                            }
                        }
                        $idx++;
                    }
                    $idx = 0;
                    foreach ($months as $row_months => $item_months) {
                        if ($item_months["number"] == $item["month"]) {
                            $months[$idx]["optime"] += floatval($item["optime"]);
                            $months[$idx]["labor"] += floatval($item["labor"]);
                        }
                        $idx++;
                    }
                }
                $idx_t = 0;
                $r = 21;
                foreach ($jre as $row_team => $item_team) {
                    if ($idx_t == 0) {
                        $r = 21;
                    }
                    if ($idx_t == 1) {
                        $r = 24;
                    }
                    if ($idx_t == 2) {
                        $r = 27;
                    }
                    $j = 3;
                    $optime = 0;
                    $labor = 0;
                    foreach ($item_team["data"] as $item_months) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $optime += floatval($item_months["optime"]);
                        $labor += floatval($item_months["labor"]);
                        $objWorksheet->setCellValue($_strcol . ($r), $item_months["optime"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 1), $item_months["labor"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 2), number_format((100 * $item_months["optime"] / ($item_months["labor"] > 0 ? $item_months["labor"] : 1)), $ondalikhanegoster));
                    }
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . ($r), $optime);
                    $objWorksheet->setCellValue($_strcol . ($r + 1), $labor);
                    $objWorksheet->setCellValue($_strcol . ($r + 2), number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster));
                    $idx_t++;
                }
                $j = 3;
                $optime = 0;
                $labor = 0;
                foreach ($months as $row_months => $item_months) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $optime += floatval($item_months["optime"]);
                    $labor += floatval($item_months["labor"]);
                    $objWorksheet->setCellValue($_strcol . '30', $item_months["optime"]);
                    $objWorksheet->setCellValue($_strcol . '31', $item_months["labor"]);
                    $objWorksheet->setCellValue($_strcol . '32', number_format((100 * $item_months["optime"] / ($item_months["labor"] > 0 ? $item_months["labor"] : 1)), $ondalikhanegoster));
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . '30', $optime);
                $objWorksheet->setCellValue($_strcol . '31', $labor);
                $objWorksheet->setCellValue($_strcol . '32', number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster));
            }
            //mjd(1,1);
            $objWorksheet->getStyle('A21')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '4f81bd')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B23:O23')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '4f81bd')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A24')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => 'c0504d')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B26:O26')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => 'c0504d')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A27')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '9bbb59')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B29:O29')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '9bbb59')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A30')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '8064a2')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B32:O32')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '8064a2')
                                    ),
                        )
            );

            $dataseriesLabels = array(
                new DataSeriesValues('String', 'Worksheet!$a$21', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$24', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$27', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$30', null, 1),
            );

            $xAxisTickValues = array(
                new DataSeriesValues('String', 'Worksheet!$C$20:$N$20', null, 12),	//
            );

            $dataSeriesValues1 = array(
                new DataSeriesValues('Number', 'Worksheet!$C$23:$N$23', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$26:$N$26', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$29:$N$29', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$32:$N$32', null, 12),
            );

            //	Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues1) - 1),			// plotOrder
                $dataseriesLabels,								// plotLabel
                $xAxisTickValues,								// plotCategory
                $dataSeriesValues1								// plotValues
            );
            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            $series1->setPlotDirection(DataSeries::DIRECTION_COL);

            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            //	Set the series in the plot area
            $plotarea = new PlotArea(null, array($series1));
            //	Set the chart legend
            $legend = new Legend(Legend::POSITION_RIGHT, null, false);
            //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

            //	Create the chart
            $chart = new Chart(
                'chart1',		// name
                null,//$title,			// title
                $legend,		// legend
                $plotarea,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart->setTopLeftPosition('B2');
            $chart->setBottomRightPosition('Q19');

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart);
        }
        if ($reportname === 'JobRotationEfficiencyWeek') {
            $_chart = true;
            $_year = $_data->year;
            if ($_year == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "VardiyaVerimHafta_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $objWorksheet->getColumnDimension('B')->setWidth(6);
            $objWorksheet->getColumnDimension('K')->setWidth(3);
            $objWorksheet->getStyle('A1:q1')->getFont()->setBold(true);

            $objWorksheet->mergeCells("A1:BC1");
            $objWorksheet->getStyle('A1:BC1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', "VARDİYALAR HAFTALIK VERİM GRAFİĞİ");

            $r = 20;
            $objWorksheet->getStyle("a$r:BC$r")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle("A$r:BC$r")->getFont()->setBold(true);
            $objWorksheet->setCellValue('A' . $r++, 'Vardiyalar');
            $objWorksheet->getStyle("A21:a32")->getFont()->setBold(true);
            $objWorksheet->getStyle('A21:a32')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
            //$objWorksheet->getStyle('b21:b29')->getFont()->setSize(9);
            $sql = "SELECT code from job_rotation_teams where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records_jrt = $stmt->fetchAll();
            if (count($records_jrt) == 0) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ReportJobRotationEfficiencyWeek.errJobRotationTeams', array(), 'ReportJobRotationEfficiencyWeek'), 401);
            }
            $weeks = array();
            for ($i = 1;$i < 53;$i++) {
                $weeks[] = array("week" => $i,"optime" => 0,"labor" => 0);
            }
            $jre = array();
            foreach ($records_jrt as $row => $item) {
                $tmp = array();
                foreach ($weeks as $row_weeks => $item_weeks) {
                    $tmp[] = array("week" => $item_weeks["week"],"optime" => $item_weeks["optime"],"labor" => $item_weeks["labor"]);
                }
                $jre[] = array("jobrotation" => "","jobrotationteam" => $item["code"],"optime" => 0,"labor" => 0,"efficiency" => 0,"data" => $tmp);
            }
            if (count($jre) > 0) {
                $objWorksheet->mergeCells("A21:a23");
                $objWorksheet->setCellValue('A21', $jre[0]["jobrotationteam"]);
                $objWorksheet->setCellValue('b21', 'Y.İş');
                $objWorksheet->setCellValue('b22', 'İşgücü');
                $objWorksheet->setCellValue('b23', 'Verim');
            }
            if (count($jre) > 1) {
                $objWorksheet->mergeCells("A24:a26");
                $objWorksheet->setCellValue('A24', $jre[1]["jobrotationteam"]);
                $objWorksheet->setCellValue('b24', 'Y.İş');
                $objWorksheet->setCellValue('b25', 'İşgücü');
                $objWorksheet->setCellValue('b26', 'Verim');
            }
            if (count($jre) > 2) {
                $objWorksheet->mergeCells("A27:a29");
                $objWorksheet->setCellValue('A27', $jre[2]["jobrotationteam"]);
                $objWorksheet->setCellValue('b27', 'Y.İş');
                $objWorksheet->setCellValue('b28', 'İşgücü');
                $objWorksheet->setCellValue('b29', 'Verim');
            }
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $objWorksheet->mergeCells("A30:a32");
            $objWorksheet->setCellValue('A30', 'Atölye');
            $objWorksheet->setCellValue('b30', 'Y.İş');
            $objWorksheet->setCellValue('b31', 'İşgücü');
            $objWorksheet->setCellValue('b32', 'Verim');

            //$objWorksheet->getStyle('b34:o34')->getFont()->setBold(true);

            $sql = "select a.week,max(jobrotationteam_g)jobrotationteam,sum(a.optime)optime,sum(a.labor)labor 
            from (
                SELECT ee.week,ee.jobrotation,case when ee.isovertime=false then jre.jobrotationteam else jre.jobrotationteam end jobrotationteam_g
                    ,round(sum(ee.optime)/60,2) optime
                    ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) labor
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                where cast(ee.day as varchar(4))=:year
                GROUP BY ee.week,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                order by ee.week,ee.jobrotation,jre.jobrotationteam
            )a
            GROUP BY a.week,a.jobrotation
            order by a.week,max(jobrotationteam_g)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('year', $_year);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $r = 20;
            $j = 3;
            foreach ($weeks as $row_weeks => $item_weeks) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, $item_weeks["week"]);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                if (count($jre) > 0) {
                    $objWorksheet->getStyle($_strcol . '23')->getFont()->setBold(true);
                }
                if (count($jre) > 1) {
                    $objWorksheet->getStyle($_strcol . '26')->getFont()->setBold(true);
                }
                if (count($jre) > 2) {
                    $objWorksheet->getStyle($_strcol . '29')->getFont()->setBold(true);
                }
                $objWorksheet->getStyle($_strcol . '32')->getFont()->setBold(true);
            }

            if (count($records) > 0) {
                foreach ($records as $row => $item) {
                    $idx = 0;
                    foreach ($jre as $row_team => $item_team) {
                        if ($item_team["jobrotationteam"] == $item["jobrotationteam"]) {
                            $jre[$idx]["optime"] += floatval($item["optime"]);
                            $jre[$idx]["labor"] += floatval($item["labor"]);
                            $idx2 = 0;
                            foreach ($jre[$idx]["data"] as $row_weeks => $item_weeks) {
                                if ($item_weeks["week"] == $item["week"]) {
                                    $jre[$idx]["data"][$idx2]["optime"] = $item["optime"];
                                    $jre[$idx]["data"][$idx2]["labor"] = $item["labor"];
                                }
                                $idx2++;
                            }
                        }
                        $idx++;
                    }
                    $idx = 0;
                    foreach ($weeks as $row_weeks => $item_weeks) {
                        if ($item_weeks["week"] == $item["week"]) {
                            $weeks[$idx]["optime"] += floatval($item["optime"]);
                            $weeks[$idx]["labor"] += floatval($item["labor"]);
                        }
                        $idx++;
                    }
                }
                $idx_t = 0;
                $r = 21;
                foreach ($jre as $row_team => $item_team) {
                    if ($idx_t == 0) {
                        $r = 21;
                    }
                    if ($idx_t == 1) {
                        $r = 24;
                    }
                    if ($idx_t == 2) {
                        $r = 27;
                    }
                    $j = 3;
                    $optime = 0;
                    $labor = 0;
                    foreach ($item_team["data"] as $item_weeks) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $optime += floatval($item_weeks["optime"]);
                        $labor += floatval($item_weeks["labor"]);
                        $objWorksheet->setCellValue($_strcol . ($r), $item_weeks["optime"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 1), $item_weeks["labor"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 2), number_format((100 * $item_weeks["optime"] / ($item_weeks["labor"] > 0 ? $item_weeks["labor"] : 1)), $ondalikhanegoster));
                    }
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . ($r), $optime);
                    $objWorksheet->setCellValue($_strcol . ($r + 1), $labor);
                    $objWorksheet->setCellValue($_strcol . ($r + 2), number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster));
                    $idx_t++;
                }
                $j = 3;
                $optime = 0;
                $labor = 0;
                foreach ($weeks as $row_weeks => $item_weeks) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $optime += floatval($item_weeks["optime"]);
                    $labor += floatval($item_weeks["labor"]);
                    $objWorksheet->setCellValue($_strcol . '30', $item_weeks["optime"]);
                    $objWorksheet->setCellValue($_strcol . '31', $item_weeks["labor"]);
                    $objWorksheet->setCellValue($_strcol . '32', number_format((100 * $item_weeks["optime"] / ($item_weeks["labor"] > 0 ? $item_weeks["labor"] : 1)), $ondalikhanegoster));
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . '30', $optime);
                $objWorksheet->setCellValue($_strcol . '31', $labor);
                $objWorksheet->setCellValue($_strcol . '32', number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster));
            }
            //mjd(1,1);
            $objWorksheet->getStyle('A21')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '4f81bd')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B23:BC23')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '4f81bd')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A24')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => 'c0504d')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B26:BC26')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => 'c0504d')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A27')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '9bbb59')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B29:BC29')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '9bbb59')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A30')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '8064a2')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B32:BC32')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '8064a2')
                                    ),
                        )
            );

            $dataseriesLabels = array(
                new DataSeriesValues('String', 'Worksheet!$a$21', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$24', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$27', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$30', null, 1),
            );

            $xAxisTickValues = array(
                new DataSeriesValues('String', 'Worksheet!$C$20:$BB$20', null, 12),	//
            );

            $dataSeriesValues1 = array(
                new DataSeriesValues('Number', 'Worksheet!$C$23:$BB$23', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$26:$BB$26', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$29:$BB$29', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$32:$BB$32', null, 12),
            );

            //	Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues1) - 1),			// plotOrder
                $dataseriesLabels,								// plotLabel
                $xAxisTickValues,								// plotCategory
                $dataSeriesValues1								// plotValues
            );
            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            $series1->setPlotDirection(DataSeries::DIRECTION_COL);

            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            //	Set the series in the plot area
            $plotarea = new PlotArea(null, array($series1));
            //	Set the chart legend
            $legend = new Legend(Legend::POSITION_RIGHT, null, false);
            //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

            //	Create the chart
            $chart = new Chart(
                'chart1',		// name
                null,//$title,			// title
                $legend,		// legend
                $plotarea,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart->setTopLeftPosition('B2');
            $chart->setBottomRightPosition('BD19');

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart);
        }
        if ($reportname === 'JobRotationEfficiencyWeekDetail') {
            $_chart = true;
            $_year = $_data->year;
            $_week = $_data->week;
            if ($_year == null || $_week == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "VardiyaVerimHaftaDetay_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $objWorksheet->getColumnDimension('B')->setWidth(6);
            $objWorksheet->getStyle('A1:q1')->getFont()->setBold(true);

            $objWorksheet->mergeCells("A1:k1");
            $objWorksheet->getStyle('A1:k1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', "VARDİYALAR HAFTALIK VERİM GRAFİĞİ");

            $r = 20;
            $objWorksheet->getStyle("a$r:J$r")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle("A$r:J$r")->getFont()->setBold(true);
            $objWorksheet->setCellValue('A' . $r++, 'Vardiyalar');
            $objWorksheet->getStyle("A21:a48")->getFont()->setBold(true);
            $objWorksheet->getStyle('A21:a48')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
            //$objWorksheet->getStyle('b21:b29')->getFont()->setSize(9);
            $sql = "SELECT code from job_rotation_teams where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records_jrt = $stmt->fetchAll();
            if (count($records_jrt) == 0) {
                return $this->msgError(($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('ReportJobRotationEfficiencyWeekDetail.errJobRotationTeams', array(), 'ReportJobRotationEfficiencyWeekDetail'), 401);
            }
            $sql_days = "select cast(date_trunc('week', (select to_timestamp('$_week $_year','IW IYYY')::date as week_start)) as date) + i as day
                ,0 optime,0 labor
            from generate_series(0,6) i";
            $stmt = $conn->prepare($sql_days);
            $stmt->execute();
            $days = $stmt->fetchAll();
            $jre = array();
            foreach ($records_jrt as $row => $item) {
                $tmp = array();
                foreach ($days as $row_days => $item_days) {
                    $tmp[] = array("day" => $item_days["day"],"optime" => $item_days["optime"],"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => $item_days["labor"]);
                }
                $jre[] = array("jobrotation" => "","jobrotationteam" => $item["code"],"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"data" => $tmp);
            }
            if (count($jre) > 0) {
                $objWorksheet->mergeCells("A21:a27");
                $objWorksheet->setCellValue('A21', $jre[0]["jobrotationteam"]);
                $objWorksheet->setCellValue('b21', 'Y.İş');
                $objWorksheet->setCellValue('b22', 'Normal Çalışma');
                $objWorksheet->setCellValue('b23', 'Fazla Mesai');
                $objWorksheet->setCellValue('b24', 'Devamsızlık');
                $objWorksheet->setCellValue('b25', 'Zorunlu Kayıp');
                $objWorksheet->setCellValue('b26', 'İşgücü');
                $objWorksheet->setCellValue('b27', 'Verim');
                $objWorksheet->getStyle('b27:j27')->getFont()->setBold(true);
            }
            if (count($jre) > 1) {
                $objWorksheet->mergeCells("A28:a34");
                $objWorksheet->setCellValue('A28', $jre[1]["jobrotationteam"]);
                $objWorksheet->setCellValue('b28', 'Y.İş');
                $objWorksheet->setCellValue('b29', 'Normal Çalışma');
                $objWorksheet->setCellValue('b30', 'Fazla Mesai');
                $objWorksheet->setCellValue('b31', 'Devamsızlık');
                $objWorksheet->setCellValue('b32', 'Zorunlu Kayıp');
                $objWorksheet->setCellValue('b33', 'İşgücü');
                $objWorksheet->setCellValue('b34', 'Verim');
                $objWorksheet->getStyle('b34:j34')->getFont()->setBold(true);
            }
            if (count($jre) > 2) {
                $objWorksheet->mergeCells("A35:a41");
                $objWorksheet->setCellValue('A35', $jre[2]["jobrotationteam"]);
                $objWorksheet->setCellValue('b35', 'Y.İş');
                $objWorksheet->setCellValue('b36', 'Normal Çalışma');
                $objWorksheet->setCellValue('b37', 'Fazla Mesai');
                $objWorksheet->setCellValue('b38', 'Devamsızlık');
                $objWorksheet->setCellValue('b39', 'Zorunlu Kayıp');
                $objWorksheet->setCellValue('b40', 'İşgücü');
                $objWorksheet->setCellValue('b41', 'Verim');
                $objWorksheet->getStyle('b41:j41')->getFont()->setBold(true);
            }
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $objWorksheet->mergeCells("A42:a48");
            $objWorksheet->setCellValue('A42', 'Atölye');
            $objWorksheet->setCellValue('b42', 'Y.İş');
            $objWorksheet->setCellValue('b43', 'Normal Çalışma');
            $objWorksheet->setCellValue('b44', 'Fazla Mesai');
            $objWorksheet->setCellValue('b45', 'Devamsızlık');
            $objWorksheet->setCellValue('b46', 'Zorunlu Kayıp');
            $objWorksheet->setCellValue('b47', 'İşgücü');
            $objWorksheet->setCellValue('b48', 'Verim');
            $objWorksheet->getStyle('b48:j48')->getFont()->setBold(true);

            //$objWorksheet->getStyle('b34:o34')->getFont()->setBold(true);

            $sql = "select a.day,a.jobrotation,max(a.jobrotationteam_g)jobrotationteam
                ,sum(a.optime)optime,sum(a.worktime)worktime,sum(a.overtime)overtime
                ,sum(a.discontinuity)discontinuity,sum(a.mlosttime)mlosttime
                ,sum(a.labor)labor
            from (
                SELECT ee.day,ee.jobrotation,case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end jobrotationteam_g
                    ,round(sum(ee.optime)/60,2) optime
                    ,round(sum(case when ee.isovertime=false then ee.fulltime-ee.breaktime else 0 end)/60,2) worktime
                    ,round(sum(case when ee.isovertime=true then ee.fulltime-ee.breaktime else 0 end)/60,2) overtime 
                    ,round(sum(ee.discontinuity)/60,2)discontinuity
                    ,round(sum(ee.mlosttime)/60,2)mlosttime
                    ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) labor
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                where cast(ee.day as varchar(4))=:year and ee.week=:week
                GROUP BY ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                order by ee.day,ee.jobrotation
            )a
            GROUP BY a.day,a.jobrotation
            order by a.day,a.jobrotation,max(a.jobrotationteam_g)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('year', $_year);
            $stmt->bindValue('week', $_week);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $r = 20;
            $j = 3;
            foreach ($days as $row_days => $item_days) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, $item_days["day"]);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }

            if (count($records) > 0) {
                foreach ($records as $row => $item) {
                    $idx = 0;
                    foreach ($jre as $row_team => $item_team) {
                        if ($item_team["jobrotationteam"] == $item["jobrotationteam"]) {
                            $jre[$idx]["optime"] += floatval($item["optime"]);
                            $jre[$idx]["worktime"] += floatval($item["worktime"]);
                            $jre[$idx]["overtime"] += floatval($item["overtime"]);
                            $jre[$idx]["discontinuity"] += floatval($item["discontinuity"]);
                            $jre[$idx]["mlosttime"] += floatval($item["mlosttime"]);
                            $jre[$idx]["labor"] += floatval($item["labor"]);
                            $idx2 = 0;
                            foreach ($jre[$idx]["data"] as $row_days => $item_days) {
                                if ($item_days["day"] == $item["day"]) {
                                    $jre[$idx]["data"][$idx2]["optime"] = $item["optime"];
                                    $jre[$idx]["data"][$idx2]["worktime"] = $item["worktime"];
                                    $jre[$idx]["data"][$idx2]["overtime"] = $item["overtime"];
                                    $jre[$idx]["data"][$idx2]["discontinuity"] = $item["discontinuity"];
                                    $jre[$idx]["data"][$idx2]["mlosttime"] = $item["mlosttime"];
                                    $jre[$idx]["data"][$idx2]["labor"] = $item["labor"];
                                }
                                $idx2++;
                            }
                        }
                        $idx++;
                    }
                    $idx = 0;
                    foreach ($days as $row_days => $item_days) {
                        if ($item_days["day"] == $item["day"]) {
                            $days[$idx]["optime"] += floatval($item["optime"]);
                            $days[$idx]["worktime"] += floatval($item["worktime"]);
                            $days[$idx]["overtime"] += floatval($item["overtime"]);
                            $days[$idx]["discontinuity"] += floatval($item["discontinuity"]);
                            $days[$idx]["mlosttime"] += floatval($item["mlosttime"]);
                            $days[$idx]["labor"] += floatval($item["labor"]);
                        }
                        $idx++;
                    }
                }
                $idx_t = 0;
                $r = 21;
                foreach ($jre as $row_team => $item_team) {
                    if ($idx_t == 0) {
                        $r = 21;
                    }
                    if ($idx_t == 1) {
                        $r = 28;
                    }
                    if ($idx_t == 2) {
                        $r = 35;
                    }
                    $j = 3;
                    $optime = 0;
                    $worktime = 0;
                    $overtime = 0;
                    $discontinuity = 0;
                    $mlosttime = 0;
                    $labor = 0;
                    foreach ($item_team["data"] as $item_days) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $optime += floatval($item_days["optime"]);
                        $worktime += floatval($item_days["worktime"]);
                        $overtime += floatval($item_days["overtime"]);
                        $discontinuity += floatval($item_days["discontinuity"]);
                        $mlosttime += floatval($item_days["mlosttime"]);
                        $labor += floatval($item_days["labor"]);
                        $objWorksheet->setCellValue($_strcol . ($r), $item_days["optime"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 1), $item_days["worktime"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 2), $item_days["overtime"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 3), $item_days["discontinuity"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 4), $item_days["mlosttime"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 5), $item_days["labor"]);
                        $objWorksheet->setCellValue($_strcol . ($r + 6), number_format((100 * $item_days["optime"] / ($item_days["labor"] > 0 ? $item_days["labor"] : 1)), $ondalikhanegoster));
                    }
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . ($r), $optime);
                    $objWorksheet->setCellValue($_strcol . ($r + 1), $worktime);
                    $objWorksheet->setCellValue($_strcol . ($r + 2), $overtime);
                    $objWorksheet->setCellValue($_strcol . ($r + 3), $discontinuity);
                    $objWorksheet->setCellValue($_strcol . ($r + 4), $mlosttime);
                    $objWorksheet->setCellValue($_strcol . ($r + 5), $labor);
                    $objWorksheet->setCellValue($_strcol . ($r + 6), number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster));
                    $idx_t++;
                }
                $j = 3;
                $optime = 0;
                $worktime = 0;
                $overtime = 0;
                $discontinuity = 0;
                $mlosttime = 0;
                $labor = 0;
                foreach ($days as $row_days => $item_days) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $optime += floatval($item_days["optime"]);
                    $labor += floatval($item_days["labor"]);
                    $objWorksheet->setCellValue($_strcol . '42', $item_days["optime"]);
                    $objWorksheet->setCellValue($_strcol . '43', $item_days["worktime"]);
                    $objWorksheet->setCellValue($_strcol . '44', $item_days["overtime"]);
                    $objWorksheet->setCellValue($_strcol . '45', $item_days["discontinuity"]);
                    $objWorksheet->setCellValue($_strcol . '46', $item_days["mlosttime"]);
                    $objWorksheet->setCellValue($_strcol . '47', $item_days["labor"]);
                    $objWorksheet->setCellValue($_strcol . '48', number_format((100 * $item_days["optime"] / ($item_days["labor"] > 0 ? $item_days["labor"] : 1)), $ondalikhanegoster));
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . '42', $optime);
                $objWorksheet->setCellValue($_strcol . '43', $worktime);
                $objWorksheet->setCellValue($_strcol . '44', $overtime);
                $objWorksheet->setCellValue($_strcol . '45', $discontinuity);
                $objWorksheet->setCellValue($_strcol . '46', $mlosttime);
                $objWorksheet->setCellValue($_strcol . '47', $labor);
                $objWorksheet->setCellValue($_strcol . '48', number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster));
            }
            //mjd(1,1);
            $objWorksheet->getStyle('A21')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '4f81bd')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B27:J27')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '4f81bd')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A28')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => 'c0504d')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B34:J34')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => 'c0504d')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A35')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '9bbb59')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B41:J41')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '9bbb59')
                                    ),
                        )
            );

            $objWorksheet->getStyle('A42')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '8064a2')
                                    ),
                        )
            );
            $objWorksheet->getStyle('B48:J48')->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '8064a2')
                                    ),
                        )
            );

            $dataseriesLabels = array(
                new DataSeriesValues('String', 'Worksheet!$a$21', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$28', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$35', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$42', null, 1),
            );

            $xAxisTickValues = array(
                new DataSeriesValues('String', 'Worksheet!$C$20:$I$20', null, 12),	//
            );

            $dataSeriesValues1 = array(
                new DataSeriesValues('Number', 'Worksheet!$C$27:$I$27', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$34:$I$34', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$41:$I$41', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$48:$I$48', null, 12),
            );

            //	Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues1) - 1),			// plotOrder
                $dataseriesLabels,								// plotLabel
                $xAxisTickValues,								// plotCategory
                $dataSeriesValues1								// plotValues
            );
            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            $series1->setPlotDirection(DataSeries::DIRECTION_COL);

            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            //	Set the series in the plot area
            $plotarea = new PlotArea(null, array($series1));
            //	Set the chart legend
            $legend = new Legend(Legend::POSITION_RIGHT, null, false);
            //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

            //	Create the chart
            $chart = new Chart(
                'chart1',		// name
                null,//$title,			// title
                $legend,		// legend
                $plotarea,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart->setTopLeftPosition('B2');
            $chart->setBottomRightPosition('L19');

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart);
        }
        if ($reportname === 'JobRotationEmployeeEfficiency') {
            $_day = $_data->day;
            if ($_day == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "OperatorVerimGun_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->getStyle('A1:Q3')->getFont()->setBold(true);

            $objWorksheet->mergeCells("A1:Q1");
            $objWorksheet->getStyle('A1:Q1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', $_day . " OPERATÖR VERİM");
            //2020-09-02 fazla mesai tanımı yapılınca normal mesai formeni gelmiyordu else '' yapıldı
            $sql = "select a.day,a.jobrotation,max(b.jobrotationteam)jobrotationteam
                ,sum(a.worktime)worktime,sum(a.optime)optime
                ,round(100*(sum(a.optime)/(sum(a.worktime))),2)jreefficiency
            from (
                SELECT ee.day,ee.jobrotation,jre.jobrotationteam
                    ,case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end jobrotationteam_g
                    ,(sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))worktime,sum(ee.optime)optime
                FROM employee_efficiency ee 
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee and jre.isovertime=ee.isovertime
                where ee.day=:day
                group by ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                order by ee.jobrotation,jre.jobrotationteam
            )a
            join (SELECT *
                from (
                    SELECT ee.day,ee.jobrotation,jre.jobrotationteam
                        ,(sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))worktime,sum(ee.optime)optime
                        ,ROW_NUMBER() OVER (PARTITION BY ee.jobrotation ORDER BY (sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0)) desc) AS rank_no
                    FROM employee_efficiency ee
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee and jre.isovertime=ee.isovertime
                    where ee.day=:day
                    group by ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                    order by ee.jobrotation,(sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))
                )x
                where x.rank_no=1)b on b.day=a.day and b.jobrotation=a.jobrotation
            GROUP BY a.day,a.jobrotation
            order by a.day,a.jobrotation,max(a.jobrotationteam_g)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->execute();
            $jobrotations = $stmt->fetchAll();
            $i = 0;
            foreach ($jobrotations as $row => $item) {
                switch ($i) {
                    case 0:
                        $sk = "A";
                        $pk = "B";
                        $dk = "C";
                        $vk = "D";
                        $tk = "E";
                        //$objWorksheet->getColumnDimension('A')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('B')->setWidth(20);
                        //$objWorksheet->getColumnDimension('C')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('D')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('E')->setAutoSize(true);
                        $objWorksheet->getColumnDimension('F')->setWidth(1);
                        break;
                    case 1:
                        $sk = "G";
                        $pk = "H";
                        $dk = "I";
                        $vk = "J";
                        $tk = "K";
                        //$objWorksheet->getColumnDimension('G')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('H')->setWidth(20);
                        //$objWorksheet->getColumnDimension('I')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('J')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('K')->setAutoSize(true);
                        $objWorksheet->getColumnDimension('L')->setWidth(1);
                        break;
                    case 2:
                        $sk = "M";
                        $pk = "N";
                        $dk = "O";
                        $vk = "P";
                        $tk = "Q";
                        //$objWorksheet->getColumnDimension('M')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('N')->setWidth(20);
                        //$objWorksheet->getColumnDimension('O')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('P')->setAutoSize(true);
                        //$objWorksheet->getColumnDimension('Q')->setAutoSize(true);
                        break;
                }
                $sql_ee = "SELECT e.name,e.code,round(ee.losttime/60,2)losttime,round(ee.efficiency,2)efficiency,round(ee.tempo,2)tempo
                    ,case when ee.efficiency<=80 then 'F08080' when ee.efficiency>80 and ee.efficiency<=85 then 'FFFFE0' else '90EE90' end color
                from employee_efficiency ee
                left join employees e on e.code=ee.employee
                where ee.day=:day and ee.jobrotation=:jobrotation
                order by e.name";
                // and e.finish is null
                $stmt_ee = $conn->prepare($sql_ee);
                $stmt_ee->bindValue('day', $_day);
                $stmt_ee->bindValue('jobrotation', $item['jobrotation']);
                $stmt_ee->execute();
                $records_ee = $stmt_ee->fetchAll();
                $objWorksheet->getColumnDimension($pk)->setWidth(25);
                $objWorksheet->mergeCells($sk . "2:" . $tk . "2");
                $objWorksheet->getStyle($sk . "2:" . $tk . "2")->getFont()->setBold(true);
                $objWorksheet->setCellValue($sk . '2', $item["jobrotationteam"] . " % " . $item["jreefficiency"]);
                $objWorksheet->getStyle($sk . '2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue($sk . '3', "SİCİL");
                $objWorksheet->setCellValue($pk . '3', "PERSONEL");
                $objWorksheet->setCellValue($dk . '3', "DURUŞ");
                $objWorksheet->setCellValue($vk . '3', "VERİM");
                $objWorksheet->setCellValue($tk . '3', "TEMPO");
                $r = 4;
                foreach ($records_ee as $rowd => $itemd) {
                    $objWorksheet->setCellValue($sk . $r, $itemd["code"]);
                    $objWorksheet->setCellValue($pk . $r, $itemd["name"]);
                    $objWorksheet->setCellValue($dk . $r, $itemd["losttime"]);
                    $objWorksheet->setCellValue($vk . $r, $itemd["efficiency"]);
                    $objWorksheet->getStyle($vk . $r)->getFill()->applyFromArray(array(
                        'fillType' => Fill::FILL_SOLID,
                        'startColor' => array(
                                'rgb' => $itemd["color"]
                        )
                    ));
                    $objWorksheet->setCellValue($tk . $r, $itemd["tempo"]);
                    $r++;
                }
                $i++;
            }
        }
        if ($reportname === 'JobrotationLostDay') {
            $_day = $_data->day;
            if ($_day == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "VardiyaKayipSureGun_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode 
            from lost_types lt 
            left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code 
            where lt.finish is null 
            order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "select a.day,a.jobrotation,coalesce(nullif(max(a.jobrotationteam_g),''),max(a.jobrotationteam)) jobrotationteam
            from (
                SELECT ee.day,ee.jobrotation,jre.jobrotationteam,case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end jobrotationteam_g
                FROM employee_efficiency ee 
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee and jre.isovertime=ee.isovertime
                where ee.day=:day
                group by ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
            )a
            GROUP BY a.day,a.jobrotation
            order by a.jobrotation,max(a.jobrotationteam_g)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["jobrotationteam"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                    'fillType'	=> Fill::FILL_SOLID,
                                    'color'		=> array('rgb' => '9bbb59')
                                ),
                            )
                        );
                    }
                    $j++;
                }
                //$_strcol = Coordinate::stringFromColumnIndex($j++);
                //$objWorksheet->setCellValue($_strcol."2","Zorunlu Kayıplar");
                //$objWorkSheet->getStyle($_strcol."2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                //$objWorksheet->getColumnDimension($_strcol)->setWidth(8);
                //$_strcol = Coordinate::stringFromColumnIndex($j++);
                //$objWorksheet->setCellValue($_strcol."2","Diğer Kayıplar");
                //$objWorkSheet->getStyle($_strcol."2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                //$objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', $row["day"] . " " . $row["jobrotationteam"] . " DURUŞ SÜRELERİ");

                $sql_emp = "select * from(
                    SELECT e.code,e.name,round(ee.efficiency,2)efficiency,round(ee.tempo,2)tempo
                    from employee_efficiency ee
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee and jre.isovertime=ee.isovertime
                    left join employees e on e.code=ee.employee 
                    where jre.day=:day and jre.jobrotation=:jobrotation)a
                order by a.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('day', $_day);
                $stmt->bindValue('jobrotation', $row['jobrotation']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;
                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                        select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                        left join (
                            select a.day,a.jobrotationteam,a.losttype,a.employee,sum(a.suresn)suresn from (
                                SELECT eld.day,jre.jobrotationteam,status losttype,eld.employee
                                    ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                from employee_lost_details eld
                                left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                                where eld.day=:day and jre.jobrotation=:jobrotation and eld.employee=:employee)a
                            GROUP BY a.day,a.jobrotationteam,a.losttype,a.employee
                        )k on k.losttype=l.code
                        order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('day', $_day);
                    $stmt->bindValue('jobrotation', $row['jobrotation']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $e_m_losttime = 0;//zorunlu
                    $e_losttime = 0;//zorunsuz
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, $personel["efficiency"]);
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, $personel["tempo"]);
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $e_m_losttime += $opdurus["suresn"];
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        } else {
                            $e_losttime += $opdurus["suresn"];
                        }
                        $j++;
                    }
                    //$_strcol = Coordinate::stringFromColumnIndex($j++);
                    //if($e_m_losttime>0){
                    //    $objWorksheet->setCellValue($_strcol.$r,round($e_m_losttime/60,$ondalikhanegoster));
                    //}
                    //$_strcol = Coordinate::stringFromColumnIndex($j++);
                    //if($e_losttime>0){
                    //    $objWorksheet->setCellValue($_strcol.$r,round($e_losttime/60,$ondalikhanegoster));
                    //}
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    if ($pdurustoplam > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                $sql_jr = "select a.day,a.jobrotation,max(a.jobrotationteam_g)jobrotationteam
                    ,sum(a.worktime)worktime,sum(a.optime)optime,sum(a.worktimewithoutlost)worktimewithoutlost
                    ,round(100*(sum(a.optime)/(sum(a.worktime))),2)jreefficiency
                    ,round(100*(sum(a.optime)/(sum(a.worktimewithoutlost))),2)jretempo
                from (
                    SELECT ee.day,ee.jobrotation,jre.jobrotationteam,case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end jobrotationteam_g
                        ,(sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))worktime
                        ,sum(ee.optime)optime
                        ,sum(ee.worktimewithoutlost) worktimewithoutlost
                    FROM employee_efficiency ee 
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee and jre.isovertime=ee.isovertime
                    where ee.day=:day and jre.jobrotation=:jobrotation
                    group by ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                    order by ee.jobrotation,jre.jobrotationteam
                )a
                GROUP BY a.day,a.jobrotation
                order by a.jobrotation,max(a.jobrotationteam_g)";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('day', $_day);
                $stmt->bindValue('jobrotation', $row['jobrotation']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                $_strcol = Coordinate::stringFromColumnIndex($j++);
                if ($toplam > 0) {
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;
            }
            $objPHPExcel->removeSheetByIndex($i);
        }
        if ($reportname === 'JobrotationLostMonth') {
            $_year = $_data->year;
            $_month = $_data->month;
            if ($_year == null || $_month == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "VardiyaKayipSureAy_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code from job_rotation_teams where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["code"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', "Yıl:$_year Ay:$_month " . $row["code"] . " DURUŞ SÜRELERİ");

                $sql_emp = "SELECT cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer),jre.jobrotationteam,e.code,e.name
                    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                left join employees e on e.code=ee.employee
                where cast(ee.day as varchar(4))=:year and cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer)=:month and jre.jobrotationteam=:jobrotationteam
                GROUP BY cast(ee.day as varchar(4)),cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer),jre.jobrotationteam,e.code,e.name
                order by e.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('month', $_month);
                $stmt->bindValue('jobrotationteam', $row['code']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;

                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                    select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                    left join (
                        select a.jobrotationteam,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT cast(SUBSTRING(cast(eld.day as varchar(10)),6,2) as integer) as \"month\",jre.jobrotationteam,status losttype,eld.employee
                                ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from employee_lost_details eld
                            left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                            where cast(eld.day as varchar(4))=:year and cast(SUBSTRING(cast(eld.day as varchar(10)),6,2) as integer)=:month and jre.jobrotationteam=:jobrotationteam and eld.employee=:employee)a
                        GROUP BY a.month,a.jobrotationteam,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('year', $_year);
                    $stmt->bindValue('month', $_month);
                    $stmt->bindValue('jobrotationteam', $row['code']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktime"] > 0 ? $personel["worktime"] : 1), $ondalikhanegoster));
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktimewithoutlost"] > 0 ? $personel["worktimewithoutlost"] : 1), $ondalikhanegoster));
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        }
                        $j++;
                    }
                    if ($pdurustoplam > 0) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                //$sql_jr="SELECT jre.jobrotationteam
                //    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                //from employee_efficiency ee
                //left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                //where cast(ee.day as varchar(4))=:year and cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer)=:month and jre.jobrotationteam=:jobrotationteam
                //GROUP BY jre.jobrotationteam";
                $sql_jr = "select b.month,b.jobrotationteam
                    ,sum(b.optime)optime,sum(b.worktime)worktime ,sum(b.worktimewithoutlost)worktimewithoutlost
                from (
                    SELECT a.day,cast(SUBSTRING(cast(a.day as varchar(10)),6,2) as integer) as month,a.jobrotation 
                        ,max(a.jobrotationteam_g)jobrotationteam,sum(a.optime)optime,sum(a.worktime)worktime,sum(a.worktimewithoutlost)worktimewithoutlost
                    from (
                        SELECT ee.day,ee.jobrotation,case when ee.isovertime=false then jre.jobrotationteam else jre.jobrotationteam end jobrotationteam_g
                            ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                            ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) worktime
                        from employee_efficiency ee
                        left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                        where cast(ee.day as varchar(4))=:year and cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer)=:month and jre.jobrotationteam=:jobrotationteam
                        GROUP BY ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                        order by ee.day,ee.jobrotation,jre.jobrotationteam
                    )a
                    GROUP BY a.day,a.jobrotation
                )b
                group by b.month,b.jobrotationteam
                order by b.month,b.jobrotationteam";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('month', $_month);
                $stmt->bindValue('jobrotationteam', $row['code']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                if ($toplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;
            }
            $objPHPExcel->removeSheetByIndex($i);
        }
        if ($reportname === 'JobrotationLostWeek') {
            $_year = $_data->year;
            $_week = $_data->week;
            if ($_year == null || $_week == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "VardiyaKayipSureHafta_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code from job_rotation_teams where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["code"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', "Yıl:$_year Hafta:$_week " . $row["code"] . " DURUŞ SÜRELERİ");

                $sql_emp = "SELECT jre.jobrotationteam,e.code,e.name
                    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                left join employees e on e.code=ee.employee
                where cast(ee.day as varchar(4))=:year and ee.week=:week and jre.jobrotationteam=:jobrotationteam
                GROUP BY ee.week,jre.jobrotationteam,e.code,e.name
                order by e.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('week', $_week);
                $stmt->bindValue('jobrotationteam', $row['code']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;
                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                    select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                    left join (
                        select a.jobrotationteam,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT eld.day,jre.jobrotationteam,status losttype,eld.employee
                                ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from employee_lost_details eld
                            left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                            where cast(eld.day as varchar(4))=:year and eld.week=:week and jre.jobrotationteam=:jobrotationteam and eld.employee=:employee)a
                        GROUP BY a.jobrotationteam,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('year', $_year);
                    $stmt->bindValue('week', $_week);
                    $stmt->bindValue('jobrotationteam', $row['code']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktime"] > 0 ? $personel["worktime"] : 1), $ondalikhanegoster));
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktimewithoutlost"] > 0 ? $personel["worktimewithoutlost"] : 1), $ondalikhanegoster));
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        }
                        $j++;
                    }
                    if ($pdurustoplam > 0) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                //$sql_jr="SELECT jre.jobrotationteam
                //    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                //from employee_efficiency ee
                //left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                //where cast(ee.day as varchar(4))=:year and ee.week=:week and jre.jobrotationteam=:jobrotationteam
                //GROUP BY jre.jobrotationteam";
                $sql_jr = "select a.week,max(jobrotationteam_g)jobrotationteam,sum(a.optime)optime,sum(a.worktime)worktime,sum(a.worktimewithoutlost)worktimewithoutlost 
                from (
                    SELECT ee.week,ee.jobrotation,case when ee.isovertime=false then jre.jobrotationteam else jre.jobrotationteam end jobrotationteam_g
                        ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                        ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) worktime
                    from employee_efficiency ee
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                    where cast(ee.day as varchar(4))=:year and ee.week=:week and jre.jobrotationteam=:jobrotationteam
                    GROUP BY ee.week,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                    order by ee.week,ee.jobrotation,jre.jobrotationteam
                )a
                GROUP BY a.week,a.jobrotation
                order by a.week,max(jobrotationteam_g)";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('week', $_week);
                $stmt->bindValue('jobrotationteam', $row['code']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                if ($toplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;
            }
            $objPHPExcel->removeSheetByIndex($i);
        }
        if ($reportname === 'JobrotationLostYear') {
            $_year = $_data->year;
            if ($_year == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "VardiyaKayipSureYil_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code from job_rotation_teams where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["code"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', "Yıl:$_year " . $row["code"] . " DURUŞ SÜRELERİ");

                $sql_emp = "SELECT jre.jobrotationteam,e.code,e.name
                    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                left join employees e on e.code=ee.employee
                where cast(ee.day as varchar(4))=:year and jre.jobrotationteam=:jobrotationteam
                GROUP BY cast(ee.day as varchar(4)),jre.jobrotationteam,e.code,e.name
                order by e.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('jobrotationteam', $row['code']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;
                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                    select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                    left join (
                        select a.jobrotationteam,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT cast(eld.day as varchar(4)) as \"year\",jre.jobrotationteam,status losttype,eld.employee
                                ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from employee_lost_details eld
                            left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                            where cast(eld.day as varchar(4))=:year and jre.jobrotationteam=:jobrotationteam and eld.employee=:employee)a
                        GROUP BY a.year,a.jobrotationteam,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('year', $_year);
                    $stmt->bindValue('jobrotationteam', $row['code']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktime"] > 0 ? $personel["worktime"] : 1), $ondalikhanegoster));
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktimewithoutlost"] > 0 ? $personel["worktimewithoutlost"] : 1), $ondalikhanegoster));
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        }
                        $j++;
                    }
                    if ($pdurustoplam > 0) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                //$sql_jr="SELECT jre.jobrotationteam
                //    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                //from employee_efficiency ee
                //left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                //where cast(ee.day as varchar(4))=:year and jre.jobrotationteam=:jobrotationteam
                //GROUP BY jre.jobrotationteam";
                $sql_jr = "select b.year,b.jobrotationteam
                    ,sum(b.optime)optime,sum(b.worktime)worktime ,sum(b.worktimewithoutlost)worktimewithoutlost
                from (
                    SELECT a.day,cast(SUBSTRING(cast(a.day as varchar(10)),1,4) as integer) as year,a.jobrotation 
                        ,max(a.jobrotationteam_g)jobrotationteam,sum(a.optime)optime,sum(a.worktime)worktime,sum(a.worktimewithoutlost)worktimewithoutlost
                    from (
                        SELECT ee.day,ee.jobrotation,case when ee.isovertime=false then jre.jobrotationteam else jre.jobrotationteam end jobrotationteam_g
                            ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                            ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) worktime
                        from employee_efficiency ee
                        left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                        where cast(ee.day as varchar(4))=:year and jre.jobrotationteam=:jobrotationteam
                        GROUP BY ee.day,ee.jobrotation,jre.jobrotationteam,ee.isovertime
                        order by ee.day,ee.jobrotation,jre.jobrotationteam
                    )a
                    GROUP BY a.day,a.jobrotation
                )b
                where b.jobrotationteam<>''
                group by b.year,b.jobrotationteam
                order by b.year,b.jobrotationteam";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('jobrotationteam', $row['code']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                if ($toplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;
            }
            $objPHPExcel->removeSheetByIndex($i);
        }
        if ($reportname === 'JobRotationProcessPerformanceMonth') {
            $_chart = true;
            $_year = $_data->year;
            if ($_year == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "SurecPerformansAy_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $objWorksheet->getColumnDimension('B')->setWidth(6);
            $objWorksheet->getColumnDimension('O')->setAutoSize(true);
            $objWorksheet->getStyle('A1:n1')->getFont()->setBold(true);

            $objWorksheet->mergeCells("A1:n1");
            $objWorksheet->getStyle('A1:n1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', "SÜREÇ PERFORMANS AY GRAFİĞİ");

            $r = 20;
            $objWorksheet->getStyle("a$r:n$r")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle("A$r:n$r")->applyFromArray(
                array('fill' => array(
                                        'fillType'		=> Fill::FILL_SOLID,
                                        'color'		=> array('rgb' => '800080')
                                    ),
                        )
            );
            $objWorksheet->getStyle("A$r:n$r")->getFont()->setBold(true)->getColor()->setRGB('ffffff');
            $objWorksheet->setCellValue('A' . $r++, 'Ay');
            $objWorksheet->getStyle("B$r:R$r")->getFont()->setBold(true);
            $objWorksheet->setCellValue('A' . $r++, 'Yapılan İş');
            $objWorksheet->setCellValue('A' . $r++, 'Normal Çalışma');
            $objWorksheet->setCellValue('A' . $r++, 'Fazla Mesai');
            $objWorksheet->setCellValue('A' . $r++, 'Devamsızlık');
            $objWorksheet->setCellValue('A' . $r++, 'Zorunlu Kayıplar');
            $objWorksheet->getStyle("B$r:R$r")->getFont()->setBold(true);
            $objWorksheet->setCellValue('A' . $r++, 'İşgücü');
            $objWorksheet->getStyle("B$r:R$r")->getFont()->setBold(true)->getColor()->setRGB('4f81bd');
            $objWorksheet->setCellValue('A' . $r++, 'Aylık verim');
            $objWorksheet->setCellValue('A' . $r++, 'Yıllık yapılan iş');
            $objWorksheet->setCellValue('A' . $r++, 'Yıllık işgücü');
            $objWorksheet->getStyle("B$r:R$r")->getFont()->setBold(true)->getColor()->setRGB('c0504d');
            $objWorksheet->setCellValue('A' . $r++, $_year . ' Yılı verimi');
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);

            $months = array(
                array("month" => "Ocak","number" => 1,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Şubat","number" => 2,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Mart","number" => 3,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Nisan","number" => 4,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Mayıs","number" => 5,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Haziran","number" => 6,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Temmuz","number" => 7,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Ağustos","number" => 8,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Eylül","number" => 9,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Ekim","number" => 10,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Kasım","number" => 11,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0),
                array("month" => "Aralık","number" => 12,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0)
            );

            $sql = "SELECT cast(SUBSTRING(cast(day as varchar(10)),6,2) as integer) as month
                ,sum(optime)optime
                ,sum(case when isovertime=false then fulltime-breaktime else 0 end) worktime
                ,sum(case when isovertime=true then fulltime-breaktime else 0 end) overtime 
                ,sum(discontinuity)discontinuity
                ,sum(mlosttime)mlosttime
            from employee_efficiency 
            where cast(day as varchar(4))=:year
            GROUP BY cast(SUBSTRING(cast(day as varchar(10)),6,2) as integer)
            order by cast(SUBSTRING(cast(day as varchar(10)),6,2) as integer)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('year', $_year);
            $stmt->execute();
            foreach ($stmt->fetchAll() as $row => $item) {
                $idx = 0;
                foreach ($months as $row_months => $item_months) {
                    if ($item["month"] == $item_months["number"]) {
                        $months[$idx]["optime"] += floatval($item["optime"]);
                        $months[$idx]["worktime"] += floatval($item["worktime"]);
                        $months[$idx]["overtime"] += floatval($item["overtime"]);
                        $months[$idx]["discontinuity"] += floatval($item["discontinuity"]);
                        $months[$idx]["mlosttime"] += floatval($item["mlosttime"]);
                    }
                    $idx++;
                }
            }

            $idx = 0;
            $optime = 0;
            $labor = 0;
            foreach ($months as $row_months => $item_months) {
                $months[$idx]["labor"] = floatval($item_months["worktime"]) + floatval($item_months["overtime"]) - floatval($item_months["discontinuity"]) - floatval($item_months["mlosttime"]);
                $optime = round($months[$idx]["optime"] / 60, 2);
                $labor = round($months[$idx]["labor"] / 60, 2);
                $months[$idx]["efficiency"] = number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster);
                $months[$idx]["sumoptime"] += floatval($optime) + floatval($idx > 0 ? $months[$idx - 1]["sumoptime"] : 0);
                $months[$idx]["sumlabor"] += floatval($labor) + floatval($idx > 0 ? $months[$idx - 1]["sumlabor"] : 0);
                $months[$idx]["sumefficiency"] = number_format((100 * $months[$idx]["sumoptime"] / ($months[$idx]["sumlabor"] > 0 ? $months[$idx]["sumlabor"] : 1)), $ondalikhanegoster);
                ;
                $idx++;
            }
            $r = 20;
            $j = 3;
            foreach ($months as $row_months => $item_months) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, $item_months["month"]);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);

                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getStyle($_strcol . '21')->getFont()->setBold(true);
                $objWorksheet->getStyle($_strcol . '26')->getFont()->setBold(true);

                $objWorksheet->setCellValue($_strcol . ($r + 1), round(($item_months["optime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 2), round(($item_months["worktime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 3), round(($item_months["overtime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 4), round(($item_months["discontinuity"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 5), round(($item_months["mlosttime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 6), round(($item_months["labor"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 7), round(($item_months["efficiency"]), $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 8), round(($item_months["sumoptime"]), $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 9), round(($item_months["sumlabor"]), $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 10), round(($item_months["sumefficiency"]), $ondalikhanegoster));
            }

            $dataseriesLabels1 = array(
                new DataSeriesValues('String', 'Worksheet!$A$27', null, 1),	//
            );
            $dataseriesLabels2 = array(
                new DataSeriesValues('String', 'Worksheet!$A$30', null, 1),	//
            );

            $xAxisTickValues = array(
                new DataSeriesValues('String', 'Worksheet!$C$20:$N$20', null, 12),	//
            );

            $dataSeriesValues1 = array(
                new DataSeriesValues('Number', 'Worksheet!$C$27:$N$27', null, $i - 1),
            );

            //	Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_BARCHART,		// plotType
                DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                range(0, count($dataSeriesValues1) - 1),			// plotOrder
                $dataseriesLabels1,								// plotLabel
                $xAxisTickValues,								// plotCategory
                $dataSeriesValues1								// plotValues
            );

            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            $series1->setPlotDirection(DataSeries::DIRECTION_COL);

            $dataSeriesValues2 = array(
                new DataSeriesValues('Number', 'Worksheet!$C$30:$N$30', null, $i - 1),
            );

            //	Build the dataseries
            $series2 = new DataSeries(
                DataSeries::TYPE_LINECHART,		// plotType
                DataSeries::GROUPING_STANDARD,	// plotGrouping
                range(0, count($dataSeriesValues2) - 1),			// plotOrder
                $dataseriesLabels2,								// plotLabel
                [],											// plotCategory
                $dataSeriesValues2								// plotValues
            );
            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            //	Set the series in the plot area
            $plotarea = new PlotArea(null, array($series1, $series2));
            //	Set the chart legend
            $legend = new Legend(Legend::POSITION_RIGHT, null, false);
            //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

            //	Create the chart
            $chart = new Chart(
                'chart1',		// name
                null,//$title,			// title
                $legend,		// legend
                $plotarea,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart->setTopLeftPosition('B2');
            $chart->setBottomRightPosition('Q19');

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart);
        }
        if ($reportname === 'JobRotationProcessPerformanceWeek') {
            $_chart = true;
            $_year = $_data->year;
            if ($_year == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "SurecPerformansHafta_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $objWorksheet->getColumnDimension('B')->setWidth(6);
            $objWorksheet->getColumnDimension('K')->setWidth(3);
            $objWorksheet->getStyle('A1:q1')->getFont()->setBold(true);

            $objWorksheet->mergeCells("A1:BB1");
            $objWorksheet->getStyle('A1:BB1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->setCellValue('A1', "SÜREÇ PERFORMANS HAFTA GRAFİĞİ");

            $r = 20;
            $objWorksheet->getStyle("a$r:BB$r")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle("A$r:BB$r")->applyFromArray(
                array('fill' => array(
                    'fillType'		=> Fill::FILL_SOLID,
                    'color'		=> array('rgb' => '800080')
                    ),
                )
            );
            $objWorksheet->getStyle("A$r:BB$r")->getFont()->setBold(true)->getColor()->setRGB('ffffff');
            $objWorksheet->setCellValue('A' . $r++, 'Hafta');

            $objWorksheet->setCellValue('A' . $r++, 'Yapılan İş');
            $objWorksheet->setCellValue('A' . $r++, 'Normal Çalışma');
            $objWorksheet->setCellValue('A' . $r++, 'Fazla Mesai');
            $objWorksheet->setCellValue('A' . $r++, 'Devamsızlık');
            $objWorksheet->setCellValue('A' . $r++, 'Zorunlu Kayıplar');
            $objWorksheet->getStyle("B$r:BB$r")->getFont()->setBold(true);
            $objWorksheet->setCellValue('A' . $r++, 'İşgücü');
            $objWorksheet->getStyle("B$r:BB$r")->getFont()->setBold(true)->getColor()->setRGB('4f81bd');
            $objWorksheet->setCellValue('A' . $r++, 'Haftalık verim');
            $objWorksheet->setCellValue('A' . $r++, 'Yıllık toplam yapılan iş');
            $objWorksheet->setCellValue('A' . $r++, 'Yıllık toplam işgücü');
            $objWorksheet->getStyle("B$r:BB$r")->getFont()->setBold(true)->getColor()->setRGB('c0504d');
            $objWorksheet->setCellValue('A' . $r++, $_year . ' Yılı verimi');
            $objWorksheet->getColumnDimension('A')->setAutoSize(true);
            $weeks = array();
            for ($i = 1;$i < 53;$i++) {
                $weeks[] = array("week" => $i,"optime" => 0,"worktime" => 0,"overtime" => 0,"discontinuity" => 0,"mlosttime" => 0,"labor" => 0,"efficiency" => 0,"sumoptime" => 0,"sumlabor" => 0,"sumefficiency" => 0);
            }

            $sql = "SELECT week
                ,sum(optime)optime
                ,sum(case when isovertime=false then fulltime-breaktime else 0 end) worktime
                ,sum(case when isovertime=true then fulltime-breaktime else 0 end) overtime 
                ,sum(discontinuity)discontinuity
                ,sum(mlosttime)mlosttime
            from employee_efficiency 
            where cast(day as varchar(4))=:year
            GROUP BY week
            order by week";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('year', $_year);
            $stmt->execute();
            foreach ($stmt->fetchAll() as $row => $item) {
                $idx = 0;
                foreach ($weeks as $row_weeks => $item_weeks) {
                    if ($item["week"] == $item_weeks["week"]) {
                        $weeks[$idx]["optime"] += floatval($item["optime"]);
                        $weeks[$idx]["worktime"] += floatval($item["worktime"]);
                        $weeks[$idx]["overtime"] += floatval($item["overtime"]);
                        $weeks[$idx]["discontinuity"] += floatval($item["discontinuity"]);
                        $weeks[$idx]["mlosttime"] += floatval($item["mlosttime"]);
                    }
                    $idx++;
                }
            }

            $idx = 0;
            $optime = 0;
            $labor = 0;
            foreach ($weeks as $row_weeks => $item_weeks) {
                $weeks[$idx]["labor"] = floatval($item_weeks["worktime"]) + floatval($item_weeks["overtime"]) - floatval($item_weeks["discontinuity"]) - floatval($item_weeks["mlosttime"]);
                $optime = round($weeks[$idx]["optime"] / 60, 2);
                $labor = round($weeks[$idx]["labor"] / 60, 2);
                $weeks[$idx]["efficiency"] = number_format((100 * $optime / ($labor > 0 ? $labor : 1)), $ondalikhanegoster);
                $weeks[$idx]["sumoptime"] += floatval($optime) + floatval($idx > 0 ? $weeks[$idx - 1]["sumoptime"] : 0);
                $weeks[$idx]["sumlabor"] += floatval($labor) + floatval($idx > 0 ? $weeks[$idx - 1]["sumlabor"] : 0);
                $weeks[$idx]["sumefficiency"] = number_format((100 * $weeks[$idx]["sumoptime"] / ($weeks[$idx]["sumlabor"] > 0 ? $weeks[$idx]["sumlabor"] : 1)), $ondalikhanegoster);
                ;
                $idx++;
            }
            $r = 20;
            $j = 3;
            foreach ($weeks as $row_weeks => $item_weeks) {
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, $item_weeks["week"]);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);

                $objWorksheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getStyle($_strcol . '21')->getFont()->setBold(true);
                $objWorksheet->getStyle($_strcol . '26')->getFont()->setBold(true);

                $objWorksheet->setCellValue($_strcol . ($r + 1), round(($item_weeks["optime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 2), round(($item_weeks["worktime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 3), round(($item_weeks["overtime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 4), round(($item_weeks["discontinuity"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 5), round(($item_weeks["mlosttime"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 6), round(($item_weeks["labor"]) / 60, $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 7), round(($item_weeks["efficiency"]), $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 8), round(($item_weeks["sumoptime"]), $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 9), round(($item_weeks["sumlabor"]), $ondalikhanegoster));
                $objWorksheet->setCellValue($_strcol . ($r + 10), round(($item_weeks["sumefficiency"]), $ondalikhanegoster));
            }

            $dataseriesLabels = array(
                new DataSeriesValues('String', 'Worksheet!$a$27', null, 1),
                new DataSeriesValues('String', 'Worksheet!$a$30', null, 1),
            );

            $xAxisTickValues = array(
                new DataSeriesValues('String', 'Worksheet!$C$20:$BB$20', null, 12),	//
            );

            $dataSeriesValues1 = array(
                new DataSeriesValues('Number', 'Worksheet!$C$27:$BB$27', null, 12),
                new DataSeriesValues('Number', 'Worksheet!$C$30:$BB$30', null, 12),
            );

            //	Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_LINECHART,		// plotType
                DataSeries::GROUPING_STANDARD,	// plotGrouping
                range(0, count($dataSeriesValues1) - 1),			// plotOrder
                $dataseriesLabels,								// plotLabel
                $xAxisTickValues,								// plotCategory
                $dataSeriesValues1								// plotValues
            );
            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            $series1->setPlotDirection(DataSeries::DIRECTION_COL);

            //	Set additional dataseries parameters
            //		Make it a vertical column rather than a horizontal bar graph

            //	Set the series in the plot area
            $plotarea = new PlotArea(null, array($series1));
            //	Set the chart legend
            $legend = new Legend(Legend::POSITION_RIGHT, null, false);
            //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

            //	Create the chart
            $chart = new Chart(
                'chart1',		// name
                null,//$title,			// title
                $legend,		// legend
                $plotarea,		// plotArea
                true,			// plotVisibleOnly
                'gap',				// displayBlanksAs
                null,			// xAxisLabel
                null			// yAxisLabel
            );

            //	Set the position where the chart should appear in the worksheet
            $chart->setTopLeftPosition('B2');
            $chart->setBottomRightPosition('BE19');

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart);
        }
        if ($reportname === 'KanbanOperationAll') {
            $name = "KanbanUrunListesi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT * from kanban_operations order by product,client";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();

            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->getStyle('A1:H2')->getFont()->setBold(true);
            $objWorksheet->mergeCells('A1:H1');
            $objWorksheet->getStyle('A1:H1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $objWorksheet->setCellValue('A1', "Kanban Ürün Listesi");

            $objWorksheet->setCellValue('A2', "İstasyon");
            $objWorksheet->setCellValue('B2', "Ürün");
            $objWorksheet->setCellValue('C2', "Kart Sayısı");
            $objWorksheet->setCellValue('D2', "Min Kart");
            $objWorksheet->setCellValue('E2', "Max Kart");
            $objWorksheet->setCellValue('F2', "Stok Kart");
            $objWorksheet->setCellValue('G2', "Kasa İçi");
            $objWorksheet->setCellValue('H2', "Saatlik Talep");
            $rowId = 3;
            foreach ($records as $row => $item) {
                $objWorksheet->setCellValueExplicit('A' . $rowId, $item["client"], DataType::TYPE_STRING);
                $objWorksheet->setCellValueExplicit('B' . $rowId, $item["product"], DataType::TYPE_STRING);
                $objWorksheet->setCellValue('C' . $rowId, $item["boxcount"]);
                $objWorksheet->setCellValue('D' . $rowId, $item["minboxcount"]);
                $objWorksheet->setCellValue('E' . $rowId, $item["maxboxcount"]);
                $objWorksheet->setCellValue('F' . $rowId, $item["currentboxcount"]);
                $objWorksheet->setCellValue('G' . $rowId, $item["packaging"]);
                $objWorksheet->setCellValue('H' . $rowId, $item["hourlydemand"]);
                $rowId++;
            }
            for ($j = 1;$j < 50;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        if ($reportname === 'LostTypeAll') {
            $name = "KayipKodlariListesi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT lt.listorder,lt.code,lgd.lostgroup,lgd.lostgroupcode,lt.ioevent
                ,case when lt.closenoemployee=true then 1 else 0 end closenoemployee
                ,case when lt.islisted=true then 1 else 0 end islisted
                ,case when lt.isclient=true then 1 else 0 end  isclient
                ,case when lt.isemployee=true then 1 else 0 end  isemployee
                ,case when lt.isshortlost=true then 1 else 0 end  isshortlost
                ,case lt.authorizeloststart when 'isexpert' then 'Usta' when 'ismould' then 'Kalıp' when 'ismaintenance' then 'Bakım' when 'isoperator' then 'Operatör' when 'isquality' then 'Kalite' else '' end authorizeloststart
                ,case lt.authorizelostfinish when 'isexpert' then 'Usta' when 'ismould' then 'Kalıp' when 'ismaintenance' then 'Bakım' when 'isoperator' then 'Operatör' when 'isquality' then 'Kalite' else '' end authorizelostfinish
            from lost_types lt 
            left join lost_group_details lgd on lgd.losttype=lt.code 
            where lt.finish is null and lgd.lostgroup like 'OEE%'
            order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();

            $objWorksheet = $objPHPExcel->getActiveSheet();
            //$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $objWorksheet->getStyle('A1:L2')->getFont()->setBold(true);
            $objWorksheet->mergeCells('A1:L1');
            $objWorksheet->getStyle('A1:L1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $objWorksheet->setCellValue('A1', "Kayıp Kodları Listesi");

            $objWorksheet->setCellValue('A2', "Sıra");
            $objWorksheet->setCellValue('B2', "Kayıp kodu");
            $objWorksheet->setCellValue('C2', "OEE Ana Grup");
            $objWorksheet->setCellValue('D2', "OEE Detay Grup");
            $objWorksheet->setCellValue('E2', "Olay");
            $objWorksheet->setCellValue('F2', "Kimse Yok İse Kapat");
            $objWorksheet->setCellValue('G2', "Cihazda Görünsün");
            $objWorksheet->setCellValue('H2', "İstasyon Kaybı");
            $objWorksheet->setCellValue('I2', "Operatör Kaybı");
            $objWorksheet->setCellValue('J2', "OEE Kısa Duruş");
            $objWorksheet->setCellValue('K2', "Başlatan Yetkili");
            $objWorksheet->setCellValue('L2', "Bitiren Yetkili");
            $rowId = 3;
            foreach ($records as $row => $item) {
                $objWorksheet->setCellValue('A' . $rowId, $item["listorder"]);
                $objWorksheet->setCellValue('B' . $rowId, $item["code"]);
                $objWorksheet->setCellValue('C' . $rowId, $item["lostgroup"]);
                $objWorksheet->setCellValue('D' . $rowId, $item["lostgroupcode"]);
                $objWorksheet->setCellValue('E' . $rowId, $item["ioevent"]);
                $objWorksheet->setCellValue('F' . $rowId, $item["closenoemployee"]);
                $objWorksheet->setCellValue('G' . $rowId, $item["islisted"]);
                $objWorksheet->setCellValue('H' . $rowId, $item["isclient"]);
                $objWorksheet->setCellValue('I' . $rowId, $item["isemployee"]);
                $objWorksheet->setCellValue('J' . $rowId, $item["isshortlost"]);
                $objWorksheet->setCellValue('K' . $rowId, $item["authorizeloststart"]);
                $objWorksheet->setCellValue('L' . $rowId, $item["authorizelostfinish"]);
                $rowId++;
            }
            for ($j = 1;$j < 50;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        if ($reportname === 'MonthProdAnalyze') {
            $_yil = $_data->year;
            $_opname = (isset($_data->opname) ? $_data->opname : null);
            $_client = (isset($_data->client) ? $_data->client : null);
            if ($_yil == null || (($_opname == null || $_opname == '') && ($_client == null || $_client == ''))) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $_day = $_yil . '-01-01';
            $istr_cpd = $_client == "" ? "" : " and cpd.client in ('" . implode("','", explode(',', $_client)) . "') ";
            $istr_cld = $_client == "" ? "" : " and cld.client in ('" . implode("','", explode(',', $_client)) . "') ";
            $rstr = "";
            $tstr = "";
            if ($_opname != '') {
                $referans = explode(',', $_opname);
                foreach ($referans as $item) {
                    $tstr .= ($tstr == "" ? "(" : " or ") . "opname like '" . $item . "%'";
                }
                if ($tstr != "") {
                    $rstr = " and " . $tstr . ") ";
                }
            }
            $name = "UretimAnaliz_" . $_yil . "_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "
            select a.client,a.txt_month,a.opname,a.productioncurrent,a.sureteorik
                ,case when a.txt_month<'2018-10' then a.suregercek else a.suregercek-b.surekayip-b.suremola end suregercek
                ,b.surekayip,b.suremola
                ,case when a.txt_month<'2018-10' then a.suregercek+b.surekayip+b.suremola else a.suregercek end as suretoplam
            from (
                select ur.client,to_char(date_trunc('month', ur.day), 'YYYY-MM') AS txt_month,ur.opname
                    ,sum(ur.productioncurrent)productioncurrent
                    ,sum(round(ur.suresnuretim/ur.sayi)) sureteorik,sum(round(ur.suresngercek/ur.sayi)) suregercek
                from (
                    SELECT cpd.client,cpd.day,cpd.opname,cpd.productioncurrent
                        ,(select count(cpd0.*) 
                            from client_production_details cpd0 
                            where cpd0.type=cpd.type and cpd0.day=cpd.day and cpd0.jobrotation=cpd.jobrotation and cpd0.mould=cpd.mould 
                            and cpd0.client not in (SELECT code from clients where workflow='CNC')
                            and cpd0.mouldgroup=cpd.mouldgroup and cpd0.start=cpd.start and cpd0.finish=cpd.finish)as sayi
                        ,cpd.productioncurrent*cpd.calculatedtpp suresnuretim
                        ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresngercek
                    from client_production_details cpd 
                    where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.finish is not null 
                    and cpd.client not in (SELECT code from clients where workflow='CNC')
                    and cpd.productioncurrent>0 and cpd.day>=:day " . $istr_cpd . $rstr . " 
                )ur
                where ur.sayi>0
                GROUP BY ur.client,date_trunc('month', ur.day),ur.opname)a
            left join (
                select ls.client,ls.txt_month,ls.opname
                    ,sum(ls.suremola)suremola,sum(ls.surekayip)surekayip
                from (
                    select lost.client,to_char(date_trunc('month', lost.day), 'YYYY-MM') AS txt_month,lost.opname
                        ,case when lost.mola=1 then sum(round(lost.surekayip/lost.sayi)) else 0 end suremola
                        ,case when lost.mola=0 then sum(round(lost.surekayip/lost.sayi)) else 0 end surekayip
                    from (
                        SELECT cld.client,cld.day,cld.losttype,cld.opname
                            ,case when cld.losttype='ÇAY MOLASI' or cld.losttype='YEMEK MOLASI' then 1 else 0 end mola
                            ,(select count(cld0.*) 
                                from client_lost_details cld0 
                                where cld0.type=cld.type and cld0.day=cld.day and cld0.jobrotation=cld.jobrotation 
                                and cld0.client not in (SELECT code from clients where workflow='CNC')
                                and cld0.mould=cld.mould and cld0.mouldgroup=cld.mouldgroup and cld0.start=cld.start and cld0.finish=cld.finish)as sayi
                            ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) surekayip
                        from client_lost_details cld
                        where cld.type='l_c_t' and cld.finish is not null 
                        and cld.client not in (SELECT code from clients where workflow='CNC')
                        and cld.losttype not in ('GÖREVDE KİMSE YOK','URETIM SONU ISKARTA BEKLEME') and cld.day>=:day " . $istr_cld . $rstr . " 
                    )lost
                    where lost.sayi>0
                    GROUP BY lost.client,date_trunc('month', lost.day),lost.opname,lost.mola
                )ls
                GROUP BY ls.client,ls.txt_month,ls.opname
            )b on b.client=a.client and b.txt_month=a.txt_month and b.opname=a.opname
            ";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            //if($istr_cpd!==''){
            //    $stmt->bindValue('client_cpd', explode(',',$_client));
            //}
            //if($istr_cld!==''){
            //    $stmt->bindValue('client_cld', explode(',',$_client));
            //}
            $stmt->execute();
            $records = $stmt->fetchAll();

            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->getStyle('A1:I1')->getFont()->setBold(true);
            $objWorksheet->setCellValue('A1', "Operasyon");
            $objWorksheet->setCellValue('B1', "İstasyon");
            $objWorksheet->setCellValue('C1', "Dönem");
            $objWorksheet->setCellValue('D1', "Üretim Adedi");
            $objWorksheet->setCellValue('E1', "Teorik Süre");
            $objWorksheet->setCellValue('F1', "Gerçek Süre");
            $objWorksheet->setCellValue('G1', "Duruş");
            $objWorksheet->setCellValue('H1', "Mola");
            $objWorksheet->setCellValue('I1', "Toplam Süre");
            $rowId = 2;
            foreach ($records as $row => $item) {
                $objWorksheet->setCellValue('A' . $rowId, $item["opname"]);
                $objWorksheet->setCellValue('B' . $rowId, $item["client"]);
                $objWorksheet->setCellValue('C' . $rowId, $item["txt_month"]);
                $objWorksheet->setCellValue('D' . $rowId, $item["productioncurrent"]);
                $objWorksheet->setCellValue('E' . $rowId, $item["sureteorik"]);
                $objWorksheet->setCellValue('F' . $rowId, $item["suregercek"]);
                $objWorksheet->setCellValue('G' . $rowId, $item["surekayip"]);
                $objWorksheet->setCellValue('H' . $rowId, $item["suremola"]);
                $objWorksheet->setCellValue('I' . $rowId, $item["suretoplam"]);
                $rowId++;
            }
        }
        if ($reportname === 'EmployeeProdAnalyze') {
            $_day1 = $_data->day1;
            $_day2 = $_data->day2;
            $_opname = $_data->opname;
            $name = "PersonelUretimAnaliz_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "select *
                ,ds.suresngercek-ds.suremola-ds.surekayip uretimsuresi
                ,cast((ds.suresngercek-ds.suremola-ds.surekayip)/ds.productioncurrent as decimal(10,2)) gercekbirimsure
                ,cast(ds.calculatedtpp/((ds.suresngercek-ds.suremola-ds.surekayip)/ds.productioncurrent)*100 as decimal(10,2)) tempo
            from (	
                select cpd.client,cpd.day,cpd.jobrotation,cpd.opname,cpd.employee,e.name personel
                    ,cpd.productioncurrent,cpd.calculatedtpp 
                    ,cpd.start,cpd.finish
                    ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresngercek
                    ,coalesce((select sum(ls.suremola)suremola
                        from (
                            select lost.client,lost.opname,sum(round(lost.surekayip/lost.sayi)) suremola
                            from (
                                SELECT cld.client,cld.day,cld.losttype,cld.opname,cld.employee
                                    ,case when cld.losttype='ÇAY MOLASI' or cld.losttype='YEMEK MOLASI' then 1 else 0 end mola
                                    ,(select count(cld0.*) 
                                        from client_lost_details cld0 
                                        where cld0.type=cld.type and cld0.day=cld.day and cld0.jobrotation=cld.jobrotation 
                                            and cld0.client not in (SELECT code from clients where workflow='CNC')
                                            and cld0.mould=cld.mould and cld0.mouldgroup=cld.mouldgroup and cld0.start=cld.start and cld0.finish=cld.finish)as sayi
                                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) surekayip
                                from client_lost_details cld
                                where cld.type='l_e_t' and cld.finish is not null 
                                    and cld.client not in (SELECT code from clients where workflow='CNC')
                                    and cld.losttype  in ('ÇAY MOLASI','YEMEK MOLASI') 
                                    and cld.day>=:day1 and cld.day<=:day2 and cld.opname=:opname
                                    and cld.start>=cpd.start and cld.finish<=cpd.finish
                            )lost
                            where lost.sayi>0
                            GROUP BY lost.client,lost.opname
                        )ls
                        GROUP BY ls.client,ls.opname),0)suremola
                    ,coalesce((select sum(ls.surekayip)surekayip
                        from (
                            select lost.client,lost.opname,sum(round(lost.surekayip/lost.sayi)) surekayip
                            from (
                                SELECT cld.client,cld.day,cld.losttype,cld.opname,cld.employee
                                    ,(select count(cld0.*) 
                                        from client_lost_details cld0 
                                        where cld0.type=cld.type and cld0.day=cld.day and cld0.jobrotation=cld.jobrotation 
                                            and cld0.client not in (SELECT code from clients where workflow='CNC')
                                            and cld0.mould=cld.mould and cld0.mouldgroup=cld.mouldgroup and cld0.start=cld.start and cld0.finish=cld.finish)as sayi
                                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) surekayip
                                from client_lost_details cld
                                where cld.type='l_e_t' and cld.finish is not null 
                                    and cld.client not in (SELECT code from clients where workflow='CNC')
                                    and cld.losttype not in ('GÖREVDE KİMSE YOK','URETIM SONU ISKARTA BEKLEME','ÇAY MOLASI','YEMEK MOLASI') 
                                    and cld.day>=:day1 and cld.day<=:day2 and cld.opname=:opname
                                    and cld.start>=cpd.start and cld.finish<=cpd.finish
                            )lost
                            where lost.sayi>0
                            GROUP BY lost.client,lost.opname
                        )ls
                        GROUP BY ls.client,ls.opname),0)surekayip
                from client_production_details cpd 
                join employees e on e.code=cpd.employee
                where cpd.type in ('e_p') and cpd.finish is not null 
                    and cpd.client not in (SELECT code from clients where workflow='CNC')
                    and cpd.productioncurrent>0 
                    and cpd.day>=:day1 and cpd.day<=:day2 and cpd.opname=:opname
            )ds	
            where ds.suresngercek-ds.suremola-ds.surekayip>0
            order by ds.start";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day1', $_day1);
            $stmt->bindValue('day2', $_day2);
            $stmt->bindValue('opname', $_opname);

            $stmt->execute();
            $records = $stmt->fetchAll();

            $objWorksheet = $objPHPExcel->getActiveSheet();
            $objWorksheet->getStyle('A1:I1')->getFont()->setBold(true);
            $objWorksheet->setCellValue('A1', "Operasyon");
            $objWorksheet->setCellValue('B1', "İstasyon");
            $objWorksheet->setCellValue('C1', "Gün");
            $objWorksheet->setCellValue('D1', "Sicil");
            $objWorksheet->setCellValue('E1', "Personel");
            $objWorksheet->setCellValue('F1', "Başlangıç");
            $objWorksheet->setCellValue('G1', "Bitiş");
            $objWorksheet->setCellValue('H1', "Üretim Adedi");
            $objWorksheet->setCellValue('I1', "Gerçek Süre");
            $objWorksheet->setCellValue('J1', "Mola");
            $objWorksheet->setCellValue('K1', "Kayıp");
            $objWorksheet->setCellValue('L1', "Üretim Süresi");
            $objWorksheet->setCellValue('M1', "Gerçek Birim Süre");
            $objWorksheet->setCellValue('N1', "Birim Süre");
            $objWorksheet->setCellValue('O1', "Tempo");
            $rowId = 2;
            foreach ($records as $row => $item) {
                $objWorksheet->setCellValue('A' . $rowId, $item["opname"]);
                $objWorksheet->setCellValue('B' . $rowId, $item["client"]);
                $objWorksheet->setCellValue('C' . $rowId, $item["day"]);
                $objWorksheet->setCellValue('D' . $rowId, $item["employee"]);
                $objWorksheet->setCellValue('E' . $rowId, $item["personel"]);
                $objWorksheet->setCellValue('F' . $rowId, $item["start"]);
                $objWorksheet->setCellValue('G' . $rowId, $item["finish"]);
                $objWorksheet->setCellValue('H' . $rowId, $item["productioncurrent"]);
                $objWorksheet->setCellValue('I' . $rowId, $item["suresngercek"]);
                $objWorksheet->setCellValue('J' . $rowId, $item["suremola"]);
                $objWorksheet->setCellValue('K' . $rowId, $item["surekayip"]);
                $objWorksheet->setCellValue('L' . $rowId, $item["uretimsuresi"]);
                $objWorksheet->setCellValue('M' . $rowId, $item["gercekbirimsure"]);
                $objWorksheet->setCellValue('N' . $rowId, $item["calculatedtpp"]);
                $objWorksheet->setCellValue('O' . $rowId, $item["tempo"]);
                $rowId++;
            }
        }
        if ($reportname === 'PlanJobRotation') {
            $_day = $_data->day;
            $_jobrotation = $_data->jobrotation;
            $_client_group_details = isset($_data->client_group_details) ? $_data->client_group_details : false;
            if ($_day == null || (($_jobrotation == null || $_jobrotation == ''))) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "UretimProgrami_" . $_day . "_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $taskcountforclient = (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? 9 : 6);
            $sql_c = "
            select c.* 
                from clients c 
                " . ($_client_group_details ? "join client_group_details cgd on cgd.clientgroup='locationclient' and cgd.client=c.code and cgd.clientgroupcode=:clientgroupcode" : "") . "
                join client_group_details cgd_o on cgd_o.clientgroup='PlanJobRotation' and cgd_o.client=c.code 
                where c.workflow<>'Gölge Cihaz'
                order by case when cgd_o.id is null then c.code else cgd_o.clientgroupcode end
            ";
            //if($_client_group_details){
            //    $sql_c="select c.*
            //    from clients c
            //    join client_group_details cgd on cgd.clientgroup='locationclient' and cgd.client=c.code and cgd.clientgroupcode=:clientgroupcode
            //    order by c.code";
            //}
            $stmt_c = $conn->prepare($sql_c);
            if ($_client_group_details) {
                $stmt_c->bindValue('clientgroupcode', $_client_group_details);
            }
            $stmt_c->execute();
            $records_c = $stmt_c->fetchAll();
            $opcounter = "coalesce(nullif(pt.opcounter,0),1)";
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $opcounter = "coalesce((SELECT coalesce(cmg.operatorcount,1)
                from client_mould_groups cmg
                join client_mould_details cmd on cmd.clientmouldgroup=cmg.code and cmg.client=cmd.client
                join mould_groups mg on mg.code=cmd.mouldgroup
                join mould_details md on md.mould=mg.mould and md.mouldgroup=mg.code
                where cmg.client=tl.client and md.opname=tl.opname limit 1),coalesce(nullif(pt.opcounter,0),1))";
            }
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $objWorksheet->getStyle('A1:J2')->getFont()->setBold(true);
            $objWorksheet->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $objWorksheet->mergeCells("A1:J1");
            } else {
                $objWorksheet->mergeCells("A1:I1");
            }
            $objWorksheet->setCellValue('A1', "Vardiya Bazında Üretim Programı - " . $_day . " - " . $_jobrotation);
            $objWorksheet->setCellValue('A2', "İstasyon");
            $objWorksheet->setCellValue('B2', "Sıra");
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $objWorksheet->setCellValue('C2', "Kalıp");
                $objWorksheet->setCellValue('D2', "Operasyon");
                $objWorksheet->setCellValue('E2', "Erp Ref No");
                $objWorksheet->setCellValue('F2', "Operatör Sayısı");
                $objWorksheet->setCellValue('G2', "Planlanan Miktar");
                $objWorksheet->setCellValue('H2', "Saatlik Hedef");
                $objWorksheet->setCellValue('I2', "Pl. Başlama");
                $objWorksheet->setCellValue('J2', "Pl. Bitiş");
            } else {
                $objWorksheet->setCellValue('C2', "Erp Ref No");
                $objWorksheet->setCellValue('D2', "Operasyon");
                $objWorksheet->setCellValue('E2', "Operatör Sayısı");
                $objWorksheet->setCellValue('F2', "Planlanan Miktar");
                $objWorksheet->setCellValue('G2', "Saatlik Hedef");
                $objWorksheet->setCellValue('H2', "Pl. Başlama");
                $objWorksheet->setCellValue('I2', "Pl. Bitiş");
            }

            $rowId = 3;
            foreach ($records_c as $row_c) {
                $objWorksheet->mergeCells('A' . $rowId . ':A' . ($rowId + $taskcountforclient - 1));
                $objWorksheet->getStyle('A' . $rowId)->getFont()->setBold(true);
                $objWorksheet->getStyle('A' . $rowId)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->setCellValue('A' . $rowId, $row_c["code"]);
                $arr_jr = explode('-', $_jobrotation);
                // $sql="SELECT tl.*,pt.opcounter,tl.productcount-tl.productdonecount vardiyaplanlanan
                //     ,substring(cast(tl.plannedstart as varchar(20)),6,11) ps
                //     ,substring(cast(tl.plannedstart+(tl.productcount-tl.productdonecount)*tl.tpp * interval '1 second' as varchar(20)),6,11) pf
                // from task_lists tl
                // left join product_trees pt on pt.name=tl.opname and pt.finish is null and pt.materialtype='O'
                // where tl.finish is null and tl.plannedstart is not null and tl.client=:client /*and tl.plannedstart between :ps1 and :ps2 */
                // order by tl.plannedstart limit :lim";
                $sql = "SELECT tl.client
                    ,string_agg(tl.opname,',' order by tl.erprefnumber) opname
					,string_agg(distinct tl.opdescription,',')opdescription
                    ,string_agg(tl.erprefnumber,',' order by tl.erprefnumber)erprefnumber
                    ,tl.opcounter,tl.tpp
                    ,substring(cast(tl.plannedstart as varchar(20)),6,11) ps
                    ,substring(cast(tl.plannedfinish as varchar(20)),6,11) pf 
                    ,max(tl.productcount)-min(tl.productdonecount) vardiyaplanlanan
                from (
                    SELECT tl.client,tl.erprefnumber,tl.opname,tl.idx," . $opcounter . " opcounter,tl.productcount
                        ,tl.productdonecount,tl.plannedstart,tl.plannedfinish,coalesce(nullif(tl.tpp,0),1)tpp
                        ,(select max(m.description) from mould_details md join moulds m on m.code=md.mould where md.opname=tl.opname )opdescription
                    from task_lists tl
                    left join product_trees pt on pt.name=tl.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=tl.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,tl.client)=tl.client " : "")."
                    where tl.finish is null and tl.plannedstart is not null
                        and tl.taskfromerp=true and tl.deleted_at is null
                        and tl.plannedstart<CURRENT_DATE + interval '" . (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? "25" : "25") . " days'
                        " . (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? " and coalesce(tl.status,'')<>'P' " : "") . "
                        and tl.client=:client /*and tl.plannedstart between :ps1 and :ps2 */
                )tl
                GROUP BY tl.client,tl.opcounter,tl.plannedstart,tl.plannedfinish,tl.tpp
                order by tl.plannedstart limit :lim";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $row_c["code"]);
                $stmt->bindValue('lim', $taskcountforclient);
                $stmt->execute();
                $records = $stmt->fetchAll();
                $i = 1;
                foreach ($records as $row) {
                    $objWorksheet->setCellValue('B' . $rowId, $i);
                    if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                        $objWorksheet->setCellValue('C' . $rowId, $row["opdescription"]);
                        $objWorksheet->setCellValueExplicit('D' . $rowId, $row["opname"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValueExplicit('E' . $rowId, $row["erprefnumber"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValue('F' . $rowId, $row["opcounter"]);
                        $objWorksheet->setCellValue('G' . $rowId, $row["vardiyaplanlanan"]);
                        $objWorksheet->setCellValue('H' . $rowId, round(3600 / (($row["tpp"] == 0 || $row["tpp"] == '0') ? 1 : $row["tpp"])));
                        $objWorksheet->setCellValue('I' . $rowId, $row["ps"]);
                        $objWorksheet->setCellValue('J' . $rowId, $row["pf"]);
                    } else {
                        $objWorksheet->setCellValueExplicit('C' . $rowId, $row["erprefnumber"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValueExplicit('D' . $rowId, $row["opname"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValue('E' . $rowId, $row["opcounter"]);
                        $objWorksheet->setCellValue('F' . $rowId, $row["vardiyaplanlanan"]);
                        $objWorksheet->setCellValue('G' . $rowId, round(3600 / (($row["tpp"] == 0 || $row["tpp"] == '0') ? 1 : $row["tpp"])));
                        $objWorksheet->setCellValue('H' . $rowId, $row["ps"]);
                        $objWorksheet->setCellValue('I' . $rowId, $row["pf"]);
                    }
                    $rowId++;
                    $i++;
                }
                for ($i;$i <= $taskcountforclient;$i++) {
                    $objWorksheet->setCellValue('B' . ($rowId++), $i);
                }
            }
            $sql_pn = "SELECT * from plan_notes order by recordorder";
            $stmt_pn = $conn->prepare($sql_pn);
            $stmt_pn->execute();
            $records_pn = $stmt_pn->fetchAll();
            foreach ($records_pn as $row_pn) {
                $rowId++;
                $objWorksheet->mergeCells("A" . $rowId . ":I" . $rowId);
                $objWorksheet->setCellValue('A' . $rowId, $row_pn["description"]);
            }
            $rowId--;
            $_strcol_start = Coordinate::stringFromColumnIndex(1);
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $_strcol_finish = Coordinate::stringFromColumnIndex(10);
            } else {
                $_strcol_finish = Coordinate::stringFromColumnIndex(9);
            }
            $objWorksheet->getStyle($_strcol_start . "1:" . $_strcol_finish . ($rowId))->applyFromArray($_arr_border);
            for ($j = 1;$j < 21;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        if ($reportname === 'PlanJobRotation2') {
            $_day = $_data->day;
            $_jobrotation = $_data->jobrotation;
            $_client_group_details = isset($_data->client_group_details) ? $_data->client_group_details : false;
            if ($_day == null || (($_jobrotation == null || $_jobrotation == ''))) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "UretimProgrami_" . $_day . "_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $taskcountforclient = (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? 9 : 6);
            $sql_c = "
            select c.* 
                from clients c 
                " . ($_client_group_details ? "join client_group_details cgd on cgd.clientgroup='locationclient' and cgd.client=c.code and cgd.clientgroupcode=:clientgroupcode" : "") . "
                join client_group_details cgd_o on cgd_o.clientgroup='PlanJobRotation' and cgd_o.client=c.code 
                where c.workflow<>'Gölge Cihaz'
                order by case when cgd_o.id is null then c.code else cgd_o.clientgroupcode end
            ";
            //if($_client_group_details){
            //    $sql_c="select c.*
            //    from clients c
            //    join client_group_details cgd on cgd.clientgroup='locationclient' and cgd.client=c.code and cgd.clientgroupcode=:clientgroupcode
            //    order by c.code";
            //}
            $stmt_c = $conn->prepare($sql_c);
            if ($_client_group_details) {
                $stmt_c->bindValue('clientgroupcode', $_client_group_details);
            }
            $stmt_c->execute();
            $records_c = $stmt_c->fetchAll();
            $opcounter = "coalesce(nullif(pt.opcounter,0),1)";
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $opcounter = "coalesce((SELECT coalesce(cmg.operatorcount,1)
                from client_mould_groups cmg
                join client_mould_details cmd on cmd.clientmouldgroup=cmg.code and cmg.client=cmd.client
                join mould_groups mg on mg.code=cmd.mouldgroup
                join mould_details md on md.mould=mg.mould and md.mouldgroup=mg.code
                where cmg.client=tl.client and md.opname=tl.opname limit 1),coalesce(nullif(pt.opcounter,0),1))";
            }
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $objWorksheet->getStyle('A1:J2')->getFont()->setBold(true);
            $objWorksheet->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $objWorksheet->mergeCells("A1:J1");
            } else {
                $objWorksheet->mergeCells("A1:I1");
            }
            $objWorksheet->setCellValue('A1', "Vardiya Bazında Üretim Programı - " . $_day . " - " . $_jobrotation);
            $objWorksheet->setCellValue('A2', "İstasyon");
            $objWorksheet->setCellValue('B2', "Sıra");
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $objWorksheet->setCellValue('C2', "Kalıp");
                $objWorksheet->setCellValue('D2', "Operasyon");
                $objWorksheet->setCellValue('E2', "Erp Ref No");
                $objWorksheet->setCellValue('F2', "Operatör Sayısı");
                $objWorksheet->setCellValue('G2', "Planlanan Miktar");
                $objWorksheet->setCellValue('H2', "Saatlik Hedef");
                $objWorksheet->setCellValue('I2', "Pl. Başlama");
                $objWorksheet->setCellValue('J2', "Pl. Bitiş");
            } else {
                $objWorksheet->setCellValue('C2', "Erp Ref No");
                $objWorksheet->setCellValue('D2', "Operasyon");
                $objWorksheet->setCellValue('E2', "Operatör Sayısı");
                $objWorksheet->setCellValue('F2', "Planlanan Miktar");
                $objWorksheet->setCellValue('G2', "Saatlik Hedef");
                $objWorksheet->setCellValue('H2', "Pl. Başlama");
                $objWorksheet->setCellValue('I2', "Pl. Bitiş");
            }

            $rowId = 3;
            foreach ($records_c as $row_c) {
                $objWorksheet->mergeCells('A' . $rowId . ':A' . ($rowId + $taskcountforclient - 1));
                $objWorksheet->getStyle('A' . $rowId)->getFont()->setBold(true);
                $objWorksheet->getStyle('A' . $rowId)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->setCellValue('A' . $rowId, $row_c["code"]);
                $arr_jr = explode('-', $_jobrotation);
                // $sql="SELECT tl.*,pt.opcounter,tl.productcount-tl.productdonecount vardiyaplanlanan
                //     ,substring(cast(tl.plannedstart as varchar(20)),6,11) ps
                //     ,substring(cast(tl.plannedstart+(tl.productcount-tl.productdonecount)*tl.tpp * interval '1 second' as varchar(20)),6,11) pf
                // from task_lists tl
                // left join product_trees pt on pt.name=tl.opname and pt.finish is null and pt.materialtype='O'
                // where tl.finish is null and tl.plannedstart is not null and tl.client=:client /*and tl.plannedstart between :ps1 and :ps2 */
                // order by tl.plannedstart limit :lim";
                $sql = "SELECT tl.client
                    ,string_agg(tl.opname,', ' order by tl.erprefnumber) opname
					,string_agg(distinct tl.opdescription,', ')opdescription
                    ,string_agg(tl.erprefnumber,', ' order by tl.erprefnumber)erprefnumber
                    ,tl.opcounter,tl.tpp
                    ,substring(cast(tl.plannedstart as varchar(20)),6,11) ps
                    ,substring(cast(tl.plannedfinish as varchar(20)),6,11) pf 
                    ,max(tl.productcount)-min(tl.productdonecount) vardiyaplanlanan
                from (
                    SELECT tl.client,tl.erprefnumber,tl.opname,tl.idx," . $opcounter . " opcounter,tl.productcount
                        ,tl.productdonecount,tl.plannedstart,tl.plannedfinish,coalesce(nullif(tl.tpp,0),1)tpp
                        ,(select max(m.description) from mould_details md join moulds m on m.code=md.mould where md.opname=tl.opname )opdescription
                    from task_lists tl
                    left join product_trees pt on pt.name=tl.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=tl.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,tl.client)=tl.client " : "")."
                    where tl.finish is null and tl.plannedstart is not null
                        and tl.taskfromerp=true and tl.deleted_at is null
                        and tl.plannedstart<CURRENT_DATE + interval '" . (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? "25" : "25") . " days'
                        " . (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? " and coalesce(tl.status,'')<>'P' " : "") . "
                        and tl.client=:client /*and tl.plannedstart between :ps1 and :ps2 */
                )tl
                GROUP BY tl.client,tl.opcounter,tl.plannedstart,tl.plannedfinish,tl.tpp
                order by tl.plannedstart limit :lim";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $row_c["code"]);
                $stmt->bindValue('lim', $taskcountforclient);
                $stmt->execute();
                $records = $stmt->fetchAll();
                $i = 1;
                foreach ($records as $row) {
                    $objWorksheet->setCellValue('B' . $rowId, $i);
                    if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                        $objWorksheet->setCellValue('C' . $rowId, $row["opdescription"]);
                        $objWorksheet->setCellValueExplicit('D' . $rowId, $row["opname"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValueExplicit('E' . $rowId, $row["erprefnumber"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValue('F' . $rowId, $row["opcounter"]);
                        $objWorksheet->setCellValue('G' . $rowId, $row["vardiyaplanlanan"]);
                        $objWorksheet->setCellValue('H' . $rowId, round(3600 / (($row["tpp"] == 0 || $row["tpp"] == '0') ? 1 : $row["tpp"])));
                        $objWorksheet->setCellValue('I' . $rowId, $row["ps"]);
                        $objWorksheet->setCellValue('J' . $rowId, $row["pf"]);
                    } else {
                        $objWorksheet->setCellValueExplicit('C' . $rowId, $row["erprefnumber"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValueExplicit('D' . $rowId, $row["opname"], DataType::TYPE_STRING);
                        $objWorksheet->setCellValue('E' . $rowId, $row["opcounter"]);
                        $objWorksheet->setCellValue('F' . $rowId, $row["vardiyaplanlanan"]);
                        $objWorksheet->setCellValue('G' . $rowId, round(3600 / (($row["tpp"] == 0 || $row["tpp"] == '0') ? 1 : $row["tpp"])));
                        $objWorksheet->setCellValue('H' . $rowId, $row["ps"]);
                        $objWorksheet->setCellValue('I' . $rowId, $row["pf"]);
                    }
                    $rowId++;
                    $i++;
                }
                for ($i;$i <= $taskcountforclient;$i++) {
                    $objWorksheet->setCellValue('B' . ($rowId++), $i);
                }
            }
            $sql_pn = "SELECT * from plan_notes order by recordorder";
            $stmt_pn = $conn->prepare($sql_pn);
            $stmt_pn->execute();
            $records_pn = $stmt_pn->fetchAll();
            foreach ($records_pn as $row_pn) {
                $rowId++;
                $objWorksheet->mergeCells("A" . $rowId . ":I" . $rowId);
                $objWorksheet->setCellValue('A' . $rowId, $row_pn["description"]);
            }
            $rowId--;
            $_strcol_start = Coordinate::stringFromColumnIndex(1);
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $_strcol_finish = Coordinate::stringFromColumnIndex(10);
            } else {
                $_strcol_finish = Coordinate::stringFromColumnIndex(9);
            }
            $objWorksheet->getStyle($_strcol_start . "1:" . $_strcol_finish . ($rowId))->applyFromArray($_arr_border);
            for ($j = 1;$j < 21;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        if ($reportname === 'TaskPlanAll') {
            $name = "BirlesenIslerListesi_" . date("Y-m-d_H:i:s");
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT idx,code,client,opcode,opnumber,opname,erprefnumber,productcount
                ,cyclecount,deadline,tpp,clientmouldgroup,mould,mouldgroup,isdefault
                ,operatorcount,status,plannedstart,plannedfinish
            from task_plans 
            order by deadline,cyclecount,idx";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();

            $objWorksheet = $objPHPExcel->getActiveSheet();
            //$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $objWorksheet->getStyle('A1:S2')->getFont()->setBold(true);
            $objWorksheet->mergeCells('A1:S1');
            $objWorksheet->getStyle('A1:S1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $objWorksheet->setCellValue('A1', "Birleşen İş Listesi");

            $objWorksheet->setCellValue('A2', "IDX");
            $objWorksheet->setCellValue('B2', "Kod");
            $objWorksheet->setCellValue('C2', "İstasyon");
            $objWorksheet->setCellValue('D2', "Op. Kodu");
            $objWorksheet->setCellValue('E2', "Op. No");
            $objWorksheet->setCellValue('F2', "Operasyon");
            $objWorksheet->setCellValue('G2', "Erp Ref");
            $objWorksheet->setCellValue('H2', "İş Emri Miktarı");
            $objWorksheet->setCellValue('I2', "Çevrim Sayısı");
            $objWorksheet->setCellValue('J2', "Vade Tarihi");
            $objWorksheet->setCellValue('K2', "Birim Süre");
            $objWorksheet->setCellValue('L2', "İstasyon Kalıp Grubu");
            $objWorksheet->setCellValue('M2', "Kalıp Kodu");
            $objWorksheet->setCellValue('N2', "Kalıp Grup Kodu");
            $objWorksheet->setCellValue('O2', "Ön Tanımlı");
            $objWorksheet->setCellValue('P2', "Operatör Sayısı");
            $objWorksheet->setCellValue('Q2', "Durum");
            $objWorksheet->setCellValue('R2', "Pl Başlama");
            $objWorksheet->setCellValue('S2', "Pl Bitiş");
            $rowId = 3;
            foreach ($records as $row => $item) {
                $objWorksheet->setCellValue('A' . $rowId, $item["idx"]);
                $objWorksheet->setCellValue('B' . $rowId, $item["code"]);
                $objWorksheet->setCellValue('C' . $rowId, $item["client"]);
                $objWorksheet->setCellValueExplicit('D' . $rowId, $item["opcode"], DataType::TYPE_STRING);
                $objWorksheet->setCellValue('E' . $rowId, $item["opnumber"]);
                $objWorksheet->setCellValueExplicit('F' . $rowId, $item["opname"], DataType::TYPE_STRING);
                $objWorksheet->setCellValue('G' . $rowId, $item["erprefnumber"]);
                $objWorksheet->setCellValue('H' . $rowId, $item["productcount"]);
                $objWorksheet->setCellValue('I' . $rowId, $item["cyclecount"]);
                $objWorksheet->setCellValue('J' . $rowId, $item["deadline"]);
                $objWorksheet->setCellValue('K' . $rowId, $item["tpp"]);
                $objWorksheet->setCellValueExplicit('L' . $rowId, $item["clientmouldgroup"], DataType::TYPE_STRING);
                $objWorksheet->setCellValueExplicit('M' . $rowId, $item["mould"], DataType::TYPE_STRING);
                $objWorksheet->setCellValueExplicit('N' . $rowId, $item["mouldgroup"], DataType::TYPE_STRING);
                $objWorksheet->setCellValue('O' . $rowId, $item["isdefault"]);
                $objWorksheet->setCellValue('P' . $rowId, $item["operatorcount"]);
                $objWorksheet->setCellValue('Q' . $rowId, $item["status"]);
                $objWorksheet->setCellValue('R' . $rowId, $item["plannedstart"]);
                $objWorksheet->setCellValue('S' . $rowId, $item["plannedfinish"]);
                $rowId++;
            }
            for ($j = 1;$j < 50;$j++) {
                $_strcol = Coordinate::stringFromColumnIndex($j);
                $objWorksheet->getColumnDimension($_strcol)->setAutoSize(true);
            }
        }
        if ($reportname === 'TeamLeaderEfficiencyDay') {
            $_chart = true;
            $_day = $_data->day;
            if ($_day == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "TakimLideriVerimGun_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code,REPLACE(code, ' ', '_') title from team_leaders where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["title"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }
                //$_strcol = Coordinate::stringFromColumnIndex($j++);
                //$objWorksheet->setCellValue($_strcol."2","Zorunlu Kayıplar");
                //$objWorkSheet->getStyle($_strcol."2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                //$objWorksheet->getColumnDimension($_strcol)->setWidth(8);
                //
                //$_strcol = Coordinate::stringFromColumnIndex($j++);
                //$objWorksheet->setCellValue($_strcol."2","Diğer Kayıplar");
                //$objWorkSheet->getStyle($_strcol."2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                //$objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', $row["day"] . " " . $row["code"] . " DURUŞ SÜRELERİ");

                $sql_emp = "select * from(
                    SELECT e.code,e.name,round(ee.efficiency,2)efficiency,round(ee.tempo,2)tempo
                    from employee_efficiency ee
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee and jre.isovertime=ee.isovertime
                    left join employees e on e.code=ee.employee
                    where jre.day=:day and ee.teamleader=:teamleader)a
                order by a.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('day', $_day);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;
                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                        select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                        left join (
                            select a.day,a.teamleader,a.losttype,a.employee,sum(a.suresn)suresn from (
                                SELECT eld.day,eld.teamleader,status losttype,eld.employee
                                    ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                from employee_lost_details eld
                                left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                                where eld.day=:day and eld.teamleader=:teamleader and eld.employee=:employee)a
                            GROUP BY a.day,a.teamleader,a.losttype,a.employee
                        )k on k.losttype=l.code
                        order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('day', $_day);
                    $stmt->bindValue('teamleader', $row['code']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $e_m_losttime = 0;//zorunlu
                    $e_losttime = 0;//zorunsuz
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, $personel["efficiency"]);
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, $personel["tempo"]);
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $e_m_losttime += $opdurus["suresn"];
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        } else {
                            $e_losttime += $opdurus["suresn"];
                        }
                        $j++;
                    }
                    //$_strcol = Coordinate::stringFromColumnIndex($j++);
                    //if($e_m_losttime>0){
                    //    $objWorksheet->setCellValue($_strcol.$r,round($e_m_losttime/60,$ondalikhanegoster));
                    //}
                    //$_strcol = Coordinate::stringFromColumnIndex($j++);
                    //if($e_losttime>0){
                    //    $objWorksheet->setCellValue($_strcol.$r,round($e_losttime/60,$ondalikhanegoster));
                    //}
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    if ($pdurustoplam > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $chart_data_last_row = $r - 1;
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                $sql_jr = "select a.day,max(a.teamleader_g)teamleader
                    ,sum(a.worktime)worktime,sum(a.optime)optime,sum(a.worktimewithoutlost)worktimewithoutlost
                    ,round(100*(sum(a.optime)/(sum(a.worktime))),2)jreefficiency
                    ,round(100*(sum(a.optime)/(sum(a.worktimewithoutlost))),2)jretempo
                from (
                    SELECT ee.day,ee.teamleader
                        ,/*case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end*/ee.teamleader teamleader_g
                        ,(sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))worktime
                        ,sum(ee.optime)optime
                        ,sum(ee.worktimewithoutlost) worktimewithoutlost
                    FROM employee_efficiency ee 
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee and jre.isovertime=ee.isovertime
                    where ee.day=:day and ee.teamleader=:teamleader 
                    group by ee.day,ee.teamleader,ee.isovertime
                    order by ee.teamleader
                )a
                GROUP BY a.day,a.teamleader
                order by a.teamleader,max(a.teamleader_g)";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('day', $_day);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                $_strcol = Coordinate::stringFromColumnIndex($j++);
                if ($toplam > 0) {
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;

                $ws = $row["title"];
                $dataseriesLabels = array(
                    new DataSeriesValues('String', $ws . '!$c$2', null, 1),
                    new DataSeriesValues('String', $ws . '!$d$2', null, 1),
                );
                $xAxisTickValues = array(
                    new DataSeriesValues('String', $ws . '!$B$3:$B$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),	//
                );
                $dataSeriesValues1 = array(
                    new DataSeriesValues('Number', $ws . '!$C$3:$C$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                    new DataSeriesValues('Number', $ws . '!$D$3:$D$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                );
                //	Build the dataseries
                $series1 = new DataSeries(
                    DataSeries::TYPE_BARCHART,		// plotType
                    DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                    range(0, count($dataSeriesValues1) - 1),			// plotOrder
                    $dataseriesLabels,								// plotLabel
                    $xAxisTickValues,								// plotCategory
                    $dataSeriesValues1								// plotValues
                );
                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                $series1->setPlotDirection(DataSeries::DIRECTION_COL);

                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                //	Set the series in the plot area
                $plotarea = new PlotArea(null, array($series1));
                //	Set the chart legend
                $legend = new Legend(Legend::POSITION_RIGHT, null, false);
                //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

                //	Create the chart
                $chart = new Chart(
                    'chart1' . $ws,	// name
                    null,//$title,			// title
                    $legend,		// legend
                    $plotarea,		// plotArea
                    true,			// plotVisibleOnly
                    'gap',			// displayBlanksAs
                    null,			// xAxisLabel
                    null			// yAxisLabel
                );

                //	Set the position where the chart should appear in the worksheet
                $chart->setTopLeftPosition('A' . ($r + 1));
                $chart->setBottomRightPosition('Z' . ($r + 20));

                if ($toplam > 0) {
                    //	Add the chart to the worksheet
                    $objWorksheet->addChart($chart);
                }
            }
            $objPHPExcel->removeSheetByIndex($i);
        }
        if ($reportname === 'TeamLeaderEfficiencyMonth') {
            $_chart = true;
            $_year = $_data->year;
            $_month = $_data->month;
            if ($_year == null || $_month == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "TakimLideriVerimAy_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code,REPLACE(code, ' ', '_') title from team_leaders where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["title"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', "Yıl:$_year Ay:$_month " . $row["code"] . " DURUŞ SÜRELERİ");

                $sql_emp = "SELECT cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer),e.teamleader,e.code,e.name
                    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                left join employees e on e.code=ee.employee
                where cast(ee.day as varchar(4))=:year and cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer)=:month and e.teamleader=:teamleader
                GROUP BY cast(ee.day as varchar(4)),cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer),e.teamleader,e.code,e.name
                order by e.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('month', $_month);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;

                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                    select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                    left join (
                        select a.teamleader,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT cast(SUBSTRING(cast(eld.day as varchar(10)),6,2) as integer) as \"month\",e.teamleader,status losttype,eld.employee
                                ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from employee_lost_details eld
                            left join employees e on e.code=eld.employee
                            left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                            where cast(eld.day as varchar(4))=:year and cast(SUBSTRING(cast(eld.day as varchar(10)),6,2) as integer)=:month and e.teamleader=:teamleader and eld.employee=:employee)a
                        GROUP BY a.month,a.teamleader,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('year', $_year);
                    $stmt->bindValue('month', $_month);
                    $stmt->bindValue('teamleader', $row['code']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktime"] > 0 ? $personel["worktime"] : 1), $ondalikhanegoster));
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktimewithoutlost"] > 0 ? $personel["worktimewithoutlost"] : 1), $ondalikhanegoster));
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        }
                        $j++;
                    }
                    if ($pdurustoplam > 0) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $chart_data_last_row = $r - 1;
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                $sql_jr = "select b.month,b.teamleader
                    ,sum(b.optime)optime,sum(b.worktime)worktime ,sum(b.worktimewithoutlost)worktimewithoutlost
                from (
                    SELECT a.day,cast(SUBSTRING(cast(a.day as varchar(10)),6,2) as integer) as month
                        ,max(a.teamleader_g)teamleader,sum(a.optime)optime,sum(a.worktime)worktime,sum(a.worktimewithoutlost)worktimewithoutlost
                    from (
                        SELECT ee.day,e.teamleader
                            ,/*case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end*/e.teamleader teamleader_g
                            ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                            ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) worktime
                        from employee_efficiency ee
                        left join employees e on e.code=ee.employee
                        left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                        where cast(ee.day as varchar(4))=:year and cast(SUBSTRING(cast(ee.day as varchar(10)),6,2) as integer)=:month and e.teamleader=:teamleader
                        GROUP BY ee.day,e.teamleader,ee.isovertime
                        order by ee.day,e.teamleader
                    )a
                    GROUP BY a.day,a.teamleader
                )b
                group by b.month,b.teamleader
                order by b.month,b.teamleader";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('month', $_month);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                if ($toplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;

                $ws = $row["title"];
                $dataseriesLabels = array(
                    new DataSeriesValues('String', $ws . '!$c$2', null, 1),
                    new DataSeriesValues('String', $ws . '!$d$2', null, 1),
                );
                $xAxisTickValues = array(
                    new DataSeriesValues('String', $ws . '!$B$3:$B$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),	//
                );
                $dataSeriesValues1 = array(
                    new DataSeriesValues('Number', $ws . '!$C$3:$C$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                    new DataSeriesValues('Number', $ws . '!$D$3:$D$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                );
                //	Build the dataseries
                $series1 = new DataSeries(
                    DataSeries::TYPE_BARCHART,		// plotType
                    DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                    range(0, count($dataSeriesValues1) - 1),			// plotOrder
                    $dataseriesLabels,								// plotLabel
                    $xAxisTickValues,								// plotCategory
                    $dataSeriesValues1								// plotValues
                );
                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                $series1->setPlotDirection(DataSeries::DIRECTION_COL);

                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                //	Set the series in the plot area
                $plotarea = new PlotArea(null, array($series1));
                //	Set the chart legend
                $legend = new Legend(Legend::POSITION_RIGHT, null, false);
                //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

                //	Create the chart
                $chart = new Chart(
                    'chart1' . $ws,	// name
                    null,//$title,			// title
                    $legend,		// legend
                    $plotarea,		// plotArea
                    true,			// plotVisibleOnly
                    'gap',				// displayBlanksAs
                    null,			// xAxisLabel
                    null			// yAxisLabel
                );

                //	Set the position where the chart should appear in the worksheet
                $chart->setTopLeftPosition('A' . ($r + 1));
                $chart->setBottomRightPosition('Z' . ($r + 20));

                if ($toplam > 0) {
                    //	Add the chart to the worksheet
                    $objWorksheet->addChart($chart);
                }
            }
            $objPHPExcel->removeSheetByIndex($i);
        }
        if ($reportname === 'TeamLeaderEfficiencyWeek') {
            $_chart = true;
            $_year = $_data->year;
            $_week = $_data->week;
            if ($_year == null || $_week == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "TakimLideriVerimHafta_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code,REPLACE(code, ' ', '_') title from team_leaders where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["title"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', "Yıl:$_year Hafta:$_week " . $row["code"] . " DURUŞ SÜRELERİ");

                $sql_emp = "SELECT ee.teamleader,e.code,e.name
                    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                left join employees e on e.code=ee.employee
                where cast(ee.day as varchar(4))=:year and ee.week=:week and ee.teamleader=:teamleader
                GROUP BY ee.week,ee.teamleader,e.code,e.name
                order by e.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('week', $_week);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;
                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                    select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                    left join (
                        select a.teamleader,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT eld.day,eld.teamleader,status losttype,eld.employee
                                ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from employee_lost_details eld
                            left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                            where cast(eld.day as varchar(4))=:year and eld.week=:week and eld.teamleader=:teamleader and eld.employee=:employee)a
                        GROUP BY a.teamleader,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('year', $_year);
                    $stmt->bindValue('week', $_week);
                    $stmt->bindValue('teamleader', $row['code']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktime"] > 0 ? $personel["worktime"] : 1), $ondalikhanegoster));
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktimewithoutlost"] > 0 ? $personel["worktimewithoutlost"] : 1), $ondalikhanegoster));
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        }
                        $j++;
                    }
                    if ($pdurustoplam > 0) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $chart_data_last_row = $r - 1;
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                $sql_jr = "select a.week,max(teamleader_g)teamleader,sum(a.optime)optime,sum(a.worktime)worktime,sum(a.worktimewithoutlost)worktimewithoutlost 
                from (
                    SELECT ee.week,ee.teamleader
                        ,/*case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end*/ee.teamleader teamleader_g
                        ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                        ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) worktime
                    from employee_efficiency ee
                    left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                    where cast(ee.day as varchar(4))=:year and ee.week=:week and ee.teamleader=:teamleader
                    GROUP BY ee.week,ee.teamleader,ee.isovertime
                    order by ee.week,ee.teamleader
                )a
                GROUP BY a.week,a.teamleader
                order by a.week,max(teamleader_g)";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('week', $_week);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                if ($toplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;

                $ws = $row["title"];
                $dataseriesLabels = array(
                    new DataSeriesValues('String', $ws . '!$c$2', null, 1),
                    new DataSeriesValues('String', $ws . '!$d$2', null, 1),
                );
                $xAxisTickValues = array(
                    new DataSeriesValues('String', $ws . '!$B$3:$B$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),	//
                );
                $dataSeriesValues1 = array(
                    new DataSeriesValues('Number', $ws . '!$C$3:$C$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                    new DataSeriesValues('Number', $ws . '!$D$3:$D$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                );
                //	Build the dataseries
                $series1 = new DataSeries(
                    DataSeries::TYPE_BARCHART,		// plotType
                    DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                    range(0, count($dataSeriesValues1) - 1),			// plotOrder
                    $dataseriesLabels,								// plotLabel
                    $xAxisTickValues,								// plotCategory
                    $dataSeriesValues1								// plotValues
                );
                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                $series1->setPlotDirection(DataSeries::DIRECTION_COL);

                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                //	Set the series in the plot area
                $plotarea = new PlotArea(null, array($series1));
                //	Set the chart legend
                $legend = new Legend(Legend::POSITION_RIGHT, null, false);
                //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

                //	Create the chart
                $chart = new Chart(
                    'chart1' . $ws,	// name
                    null,//$title,			// title
                    $legend,		// legend
                    $plotarea,		// plotArea
                    true,			// plotVisibleOnly
                    'gap',			// displayBlanksAs
                    null,			// xAxisLabel
                    null			// yAxisLabel
                );

                //	Set the position where the chart should appear in the worksheet
                $chart->setTopLeftPosition('A' . ($r + 1));
                $chart->setBottomRightPosition('Z' . ($r + 20));

                if ($toplam > 0) {
                    //	Add the chart to the worksheet
                    $objWorksheet->addChart($chart);
                }
            }
            $objPHPExcel->removeSheetByIndex($i);
        }
        if ($reportname === 'TeamLeaderEfficiencyYear') {
            $_chart = true;
            $_year = $_data->year;
            if ($_year == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $name = "TakimLideriVerimYil_" . date("Y-m-d_H:i:s");
            $sql = "SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $duruslar = $stmt->fetchAll();
            $objPHPExcel->getProperties()
                ->setCreator("Kaitek - Reports")
                ->setLastModifiedBy("Kaitek - Reports")
                ->setTitle("Kaitek - Reports")
                ->setSubject("Kaitek - Reports")
                ->setDescription($name)
                ->setKeywords('Verimot')
                ->setCategory('-');
            $sql = "SELECT code,REPLACE(code, ' ', '_') title from team_leaders where finish is null order by code";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $i = 0;
            foreach ($records as $item => $row) {
                $toplam = 0;
                $durustoplamlari = new \stdClass();
                $objWorkSheet = $objPHPExcel->createSheet($i);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $objWorkSheet->setTitle($row["title"]);

                $objWorksheet->setCellValue('A2', "SİCİL");
                $objWorkSheet->getStyle('A2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $objWorksheet->setCellValue('B2', "PERSONEL");
                $objWorkSheet->getStyle('B2')->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);

                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "VERİM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "TEMPO");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    $durustoplamlari->$_strcol = 0;
                    $objWorksheet->setCellValue($_strcol . "2", $duruskod["code"]);
                    $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                    $objWorksheet->getColumnDimension($_strcol)->setWidth(7);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . "2")->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . "2", "GENEL TOPLAM");
                $objWorkSheet->getStyle($_strcol . "2")->getAlignment()->setTextRotation(90)->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
                $objWorksheet->getColumnDimension($_strcol)->setWidth(8);

                $objWorkSheet->getStyle("A1:" . $_strcol . "2")->getFont()->setBold(true);
                $objWorksheet->mergeCells("A1:" . $_strcol . "1");
                $objWorksheet->getStyle("A1:" . $_strcol . "1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objWorksheet->setCellValue('A1', "Yıl:$_year " . $row["code"] . " DURUŞ SÜRELERİ");

                $sql_emp = "SELECT ee.teamleader,e.code,e.name
                    ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktime)/60,2) worktime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                from employee_efficiency ee
                left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                left join employees e on e.code=ee.employee
                where cast(ee.day as varchar(4))=:year and ee.teamleader=:teamleader
                GROUP BY cast(ee.day as varchar(4)),ee.teamleader,e.code,e.name
                order by e.name";
                // and e.finish is null
                $stmt = $conn->prepare($sql_emp);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $emp = $stmt->fetchAll();
                $r = 3;
                foreach ($emp as $prow => $personel) {
                    $objWorksheet->setCellValue('A' . $r, $personel["code"]);
                    $objWorksheet->setCellValue('B' . $r, $personel["name"]);
                    $sqlopdurus = "
                    select * from (SELECT lt.code,lgd.lostgroupcode from lost_types lt left join lost_group_details lgd on lgd.lostgroup='OPVERIM' and lgd.losttype=lt.code where lt.finish is null order by lt.code)l
                    left join (
                        select a.teamleader,a.losttype,a.employee,sum(a.suresn)suresn from (
                            SELECT cast(eld.day as varchar(4)) as \"year\",eld.teamleader,status losttype,eld.employee
                                ,cast(extract(epoch from (COALESCE(eld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(eld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                            from employee_lost_details eld
                            left join job_rotation_employees jre on jre.day=eld.day and jre.jobrotation=eld.jobrotation and jre.employee=eld.employee
                            where cast(eld.day as varchar(4))=:year and eld.teamleader=:teamleader and eld.employee=:employee)a
                        GROUP BY a.year,a.teamleader,a.losttype,a.employee
                    )k on k.losttype=l.code
                    order by l.code
                    ";
                    $stmt = $conn->prepare($sqlopdurus);
                    $stmt->bindValue('year', $_year);
                    $stmt->bindValue('teamleader', $row['code']);
                    $stmt->bindValue('employee', $personel["code"]);
                    $stmt->execute();
                    $lost = $stmt->fetchAll();
                    $pdurustoplam = 0;
                    $j = 3;
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktime"] > 0 ? $personel["worktime"] : 1), $ondalikhanegoster));
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($personel["optime"] * 100 / ($personel["worktimewithoutlost"] > 0 ? $personel["worktimewithoutlost"] : 1), $ondalikhanegoster));
                    foreach ($lost as $drow => $opdurus) {
                        $_strcol = Coordinate::stringFromColumnIndex($j);
                        if ($opdurus["suresn"] != '') {
                            $durustoplamlari->$_strcol += $opdurus["suresn"];
                            $objWorksheet->setCellValue($_strcol . $r, round($opdurus["suresn"] / 60, $ondalikhanegoster));
                            $pdurustoplam += $opdurus["suresn"];
                        }
                        if ($opdurus["lostgroupcode"] == 'zorunlu') {
                            $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                                array('fill' => array(
                                                    'fillType'		=> Fill::FILL_SOLID,
                                                    'color'		=> array('rgb' => '9bbb59')
                                                ),
                                    )
                            );
                        }
                        $j++;
                    }
                    if ($pdurustoplam > 0) {
                        $_strcol = Coordinate::stringFromColumnIndex($j++);
                        $objWorksheet->setCellValue($_strcol . $r, round($pdurustoplam / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    $toplam += $pdurustoplam;
                    $r++;
                }
                $chart_data_last_row = $r - 1;
                $objWorksheet->mergeCells('A' . $r . ":B" . $r);
                $objWorksheet->setCellValue('A' . $r, "Genel Toplam");
                $objWorkSheet->getStyle('A' . $r)->getFont()->setBold(true);
                $sql_jr = "select b.year,b.teamleader
                    ,sum(b.optime)optime,sum(b.worktime)worktime ,sum(b.worktimewithoutlost)worktimewithoutlost
                from (
                    SELECT a.day,cast(SUBSTRING(cast(a.day as varchar(10)),1,4) as integer) as year
                        ,max(a.teamleader_g)teamleader,sum(a.optime)optime,sum(a.worktime)worktime,sum(a.worktimewithoutlost)worktimewithoutlost
                    from (
                        SELECT ee.day,ee.teamleader
                            ,/*case when ee.isovertime=false then jre.jobrotationteam else (case EXTRACT(DOW FROM ee.day) when 0 then jre.jobrotationteam else '' end) end*/ee.teamleader teamleader_g
                            ,round(sum(ee.optime)/60,2) optime,round(sum(ee.worktimewithoutlost)/60,2) worktimewithoutlost
                            ,round((sum(ee.worktime)-sum(ee.mlosttime)-coalesce(sum(ee.discontinuity),0))/60,2) worktime
                        from employee_efficiency ee
                        left join job_rotation_employees jre on jre.day=ee.day and jre.jobrotation=ee.jobrotation and jre.employee=ee.employee
                        where cast(ee.day as varchar(4))=:year and ee.teamleader=:teamleader
                        GROUP BY ee.day,ee.teamleader,ee.isovertime
                        order by ee.day,ee.teamleader
                    )a
                    GROUP BY a.day,a.teamleader
                )b
                where b.teamleader<>''
                group by b.year,b.teamleader
                order by b.year,b.teamleader";
                $stmt = $conn->prepare($sql_jr);
                $stmt->bindValue('year', $_year);
                $stmt->bindValue('teamleader', $row['code']);
                $stmt->execute();
                $records_jr = $stmt->fetchAll();
                $j = 3;
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktime"] > 0 ? $records_jr[0]["worktime"] : 1), $ondalikhanegoster));
                $_strcol = Coordinate::stringFromColumnIndex($j++);
                $objWorksheet->setCellValue($_strcol . $r, round($records_jr[0]["optime"] * 100 / ($records_jr[0]["worktimewithoutlost"] > 0 ? $records_jr[0]["worktimewithoutlost"] : 1), $ondalikhanegoster));
                foreach ($duruslar as $drow => $duruskod) {
                    $_strcol = Coordinate::stringFromColumnIndex($j);
                    if ($durustoplamlari->$_strcol > 0) {
                        $objWorksheet->setCellValue($_strcol . $r, round($durustoplamlari->$_strcol / 60, $ondalikhanegoster));
                    }
                    $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                    if ($duruskod["lostgroupcode"] == 'zorunlu') {
                        $objWorksheet->getStyle($_strcol . $r)->applyFromArray(
                            array('fill' => array(
                                                'fillType'		=> Fill::FILL_SOLID,
                                                'color'		=> array('rgb' => '9bbb59')
                                            ),
                                )
                        );
                    }
                    $j++;
                }

                if ($toplam > 0) {
                    $_strcol = Coordinate::stringFromColumnIndex($j++);
                    $objWorksheet->setCellValue($_strcol . $r, round($toplam / 60, $ondalikhanegoster));
                }
                $objWorkSheet->getStyle($_strcol . $r)->getFont()->setBold(true);
                $objWorksheet->getColumnDimension('A')->setAutoSize(true);
                $objWorksheet->getColumnDimension('B')->setAutoSize(true);
                $objWorksheet->getRowDimension(2)->setRowHeight(150);
                $i++;

                $ws = $row["title"];
                $dataseriesLabels = array(
                    new DataSeriesValues('String', $ws . '!$c$2', null, 1),
                    new DataSeriesValues('String', $ws . '!$d$2', null, 1),
                );
                $xAxisTickValues = array(
                    new DataSeriesValues('String', $ws . '!$B$3:$B$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),	//
                );
                $dataSeriesValues1 = array(
                    new DataSeriesValues('Number', $ws . '!$C$3:$C$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                    new DataSeriesValues('Number', $ws . '!$D$3:$D$' . $chart_data_last_row, null, ($chart_data_last_row - 2)),
                );
                //	Build the dataseries
                $series1 = new DataSeries(
                    DataSeries::TYPE_BARCHART,		// plotType
                    DataSeries::GROUPING_CLUSTERED,	// plotGrouping
                    range(0, count($dataSeriesValues1) - 1),			// plotOrder
                    $dataseriesLabels,								// plotLabel
                    $xAxisTickValues,								// plotCategory
                    $dataSeriesValues1								// plotValues
                );
                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                $series1->setPlotDirection(DataSeries::DIRECTION_COL);

                //	Set additional dataseries parameters
                //		Make it a vertical column rather than a horizontal bar graph

                //	Set the series in the plot area
                $plotarea = new PlotArea(null, array($series1));
                //	Set the chart legend
                $legend = new Legend(Legend::POSITION_RIGHT, null, false);
                //$title = new \PhpOffice\PhpSpreadsheet\Chart\Title("$hafta. HAFTA KAYNAK BÖLÜMÜ KAYIP ZAMANLARIN DAĞILIMI");

                //	Create the chart
                $chart = new Chart(
                    'chart1' . $ws,	// name
                    null,//$title,			// title
                    $legend,		// legend
                    $plotarea,		// plotArea
                    true,			// plotVisibleOnly
                    'gap',				// displayBlanksAs
                    null,			// xAxisLabel
                    null			// yAxisLabel
                );

                //	Set the position where the chart should appear in the worksheet
                $chart->setTopLeftPosition('A' . ($r + 1));
                $chart->setBottomRightPosition('Z' . ($r + 20));
                if ($toplam > 0) {
                    //	Add the chart to the worksheet
                    $objWorksheet->addChart($chart);
                }
            }
            $objPHPExcel->removeSheetByIndex($i);
        }

        $objWriter = new Xlsx($objPHPExcel);
        //$objWriter->setOffice2003Compatibility(true);
        if ($_chart === true) {
            $objWriter->setIncludeCharts(true);
        }
        //$objPHPExcel->disconnectWorksheets();
        //unset($objPHPExcel);
        $response = new StreamedResponse(
            function () use ($objWriter) {
                $objWriter->save('php://output');
            },
            200,
            array()
        );
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $name . '.xlsx'
        );
        //$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Type', 'application/download');
        $response->headers->set('Expires', '0');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->sendHeaders();
        return $response;
    }

    /**
     * @Route(path="/Reports/getReport", name="Reports-getReport", options={"expose"=true}, methods={"POST"})
     */
    public function getReportAction(Request $request, $_locale)
    {
        $uri = strtolower($request->getBaseUrl());
        $_data = $request->request->all();
        if (count($_data) === 0) {
            $_data = json_decode($request->getContent());
        } else {
            $_data = json_decode(json_encode($_data, JSON_FORCE_OBJECT));
        }
        $reportname = $_data->reportname;
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $_shortlost = 10;
        if ($reportname === 'AnalyzeLost') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'opdescription';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh1 = '';
            if ($_jobrotation !== '' && $_jobrotation !== null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam !== '' && $_jobrotationteam !== null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam 
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select case when b.info='' then b.losttype else b.info end losttype,sum(suredk) suredk,count(*)sayi 
                from (
                    select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                        ,a.task
                        ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                        ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                    from (
                        select z.client,z.day,z.jobrotation
                            ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                            ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                            ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                            ,z.endval,z.beginval
                        from (
                            SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)+(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                            from client_lost_details cld 
                            left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                            left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                            " . $sql_j . "
                            where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                            and cld.day between '" . $_s . "' and '" . $_f . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                        )z
                    )a
                    left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
                    left join lost_types lt on lt.code=a.losttype and lt.finish is null
                    where a.suresn>0 
                )b
                group by case when b.info='' then b.losttype else b.info end
                order by sum(suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['client_lost_details'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeLostClient') {
            $_losttype = isset($_data->losttype) ? $_data->losttype : null;
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'opdescription';
            }
            $_s = $_data->start;
            $_f = $_data->finish;

            if ($_s == null || $_f == null || $_losttype == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh1 = '';
            if ($_jobrotation !== '' && $_jobrotation !== null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam !== '' && $_jobrotationteam !== null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam 
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select b.client,sum(suredk) suredk,count(*)sayi 
                from (
                    select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                        ,a.task
                        ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                        ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                    from (
                        select z.client,z.day,z.jobrotation
                            ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                            ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                            ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                            ,z.endval,z.beginval
                        from (
                            SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)+(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                            from client_lost_details cld 
                            left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                            left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                            " . $sql_j . "
                            where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/ 
                            and cld.day between '" . $_s . "' and '" . $_f . "' and cld.losttype='" . $_losttype . "' " . $wh1 . "
                        )z
                    )a
                    left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
                    left join lost_types lt on lt.code=a.losttype and lt.finish is null
                    where a.suresn>0 
                )b
                where case when b.info='' then b.losttype else b.info end='" . $_losttype . "'
                group by b.client
                order by sum(suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['losttype'] = $_losttype;
            $records['client_lost_details'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeLostOEE') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'opdescription';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh1 = '';
            if ($_jobrotation !== '' && $_jobrotation !== null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam !== '' && $_jobrotationteam !== null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam 
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select case when b.info='' then b.lostgroupcode else b.info end lostgroupcode,sum(suredk) suredk,count(*)sayi 
                from (
                    select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                        ,a.task
                        ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                        ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                    from (
                        select z.client,z.day,z.jobrotation
                            ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                            ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                            ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                            ,z.endval,z.beginval
                        from (
                            SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)+(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                            from client_lost_details cld 
                            left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                            left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                            " . $sql_j . "
                            where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                            and cld.day between '" . $_s . "' and '" . $_f . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                        )z
                    )a
                    left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
                    left join lost_types lt on lt.code=a.losttype and lt.finish is null
                    where a.suresn>0 
                )b
                where case when b.info='' then b.lostgroupcode else b.info end is not null
                group by case when b.info='' then b.lostgroupcode else b.info end
                order by sum(suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['client_lost_details'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeLostOperation') {
            $_losttype = isset($_data->losttype) ? $_data->losttype : null;
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'coalesce(opdescription,opname)';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null || $_losttype == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh1 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select b.task,sum(suredk) suredk,count(*)sayi 
                from (
                    select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                        ,a.task
                        ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                        ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                    from (
                        select z.client,z.day,z.jobrotation
                            ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                            ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                            ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                            ,z.endval,z.beginval
                        from (
                            SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)+(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                            from client_lost_details cld 
                            left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                            left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                            " . $sql_j . "
                            where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                            and cld.day between '" . $_s . "' and '" . $_f . "' and cld.losttype='" . $_losttype . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                        )z
                    )a
                    left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
                    left join lost_types lt on lt.code=a.losttype and lt.finish is null
                    where a.suresn>0 and a.task<>''
                )b
                where case when b.info='' then b.losttype else b.info end='" . $_losttype . "'
                group by b.task
                order by sum(suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['losttype'] = $_losttype;
            $records['lost_operation'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeOperationLost') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'coalesce(opdescription,opname)';
            }
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname_cpd = 'coalesce(cpd.opdescription,cpd.opname)';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();

            $wh1 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $wh3 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh3 = ' and cpd.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql_j_cpd = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j_cpd = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cpd.day=jobrotationteam.day and cpd.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select ddd.task,sum(ddd.suredk)suredk,sum(ddd.sayi)sayi 
                from (
                    select b.task,sum(suredk) suredk,count(*)sayi 
                    from (
                        select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                            ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                            ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                            ,a.task
                            ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                            ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                        from (
                            select z.client,z.day,z.jobrotation
                                ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                    when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                    else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                                ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                                ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                                ,z.endval,z.beginval
                            from (
                                SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                                        +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                    ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                    ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                                from client_lost_details cld 
                                left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                                left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                                " . $sql_j . "
                                where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                                and cld.day between '" . $_s . "' and '" . $_f . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                            )z
                        )a
                        join (select * from lost_group_details where lostgroup like 'OEE%' and lostgroup not in ('OEE-uretimekapali','OEE-planlidurus')) lgd on lgd.losttype=a.losttype
                        left join lost_types lt on lt.code=a.losttype and lt.finish is null
                        where a.suresn>0 and a.task<>'' 
                    )b
                    where 1=1
                    group by b.task
                    union all
                    SELECT d.opname as task,round(cast(sum(d.slowdown)/60 as numeric),0) suredk,count(*) sayi 
                    from (
                        select y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown,string_agg(y.opname, ',' order by y.opname)opname 
                        from (
                            select x.client,x.day,x.jobrotation,x.start,min(x.finish)finish,sum(x.suresn)suresn,sum(x.losttime)losttime
                                ,string_agg(distinct x.mould,',') mould
                                ,string_agg(distinct x.mouldgroup,',') mouldgroup
                                ,string_agg(x.production,',') production
                                ,string_agg(x.productioncurrent,',') productioncurrent
                                ,string_agg(distinct x.opname,',') opname
                                ,string_agg(distinct x.erprefnumber,',') erprefnumber
                                ,round(sum(x.proctime))proctime,round(sum(x.proctimecurrent))proctimecurrent
                                ,sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)sss
                                ,case when sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0 then sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent) else 0 end slowdown
                                ,min(x.calculatedtpp)calculatedtpp
                            from (
                                SELECT z.client,z.day,z.jobrotation,z.start,max(z.finish)finish,round(max(z.suresn)/z.operasyonsayisi) suresn,z.mould,z.mouldgroup
                                    ,string_agg(cast(z.production as varchar(10)),',') production
                                    ,string_agg(cast(z.productioncurrent as varchar(10)),',') productioncurrent
                                    ,string_agg(distinct z.opname,',') opname
                                    ,string_agg(distinct z.erprefnumber,',') erprefnumber
                                    ,round(sum(z.proctime)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctime
                                    ,round(sum(z.proctimecurrent)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctimecurrent
                                    ,round(avg(z.calculatedtpp),2) calculatedtpp
                                    ,z.operasyonsayisi
                                    ,(SELECT sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) )losttimex
                                    from client_lost_details cld where type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                                    and day=z.day and client=z.client and start>=z.start and finish<=max(z.finish) and losttype not in ('YETKİNLİK GÖZLEM'))/z.operasyonsayisi losttime
                                from (
                                    SELECT cpd.id,cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent,cpd.start,cpd.finish," . $this->fieldname_opname_cpd . " opname
                                        ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                        ,coalesce(cpd.calculatedtpp,pt.tpp)calculatedtpp
                                        ,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber
                                        ,coalesce(cpd.opsayi,0)opsayi
                                        ,coalesce(nullif((
                                            select count(*)
                                            from client_production_details 
                                            where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type
                                        ),0),1)operasyonsayisi
                                        ,case when exists(select c.workflow from clients c where c.code=cpd.client and c.workflow='Değişken Üretim Aparatlı') then 1 else 0 end ispres
                                    from client_production_details cpd 
                                    left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=cpd.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                                    " . $sql_j_cpd . "
                                    where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>0 
                                        /*and cpd.client not in (SELECT code from clients where workflow='CNC')*/
                                        and cpd.day between '" . $_s . "' and '" . $_f . "' and cpd.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh3 . "
                                )z
                                group by z.client,z.day,z.jobrotation,z.start,z.mould,z.mouldgroup,z.opname,z.operasyonsayisi
                            )x
                            group by x.client,x.day,x.jobrotation,x.mould,x.mouldgroup,x.opname,x.start,x.operasyonsayisi
                            having sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0
                        )y
                        group by y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown
                    )d
                    group by d.opname
                )ddd
                group by ddd.task
                order by sum(ddd.suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['operation_lost'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeOperationLostDetail') {
            $_opname = isset($_data->opname) ? $_data->opname : null;
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'coalesce(opdescription,opname)';
            }
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname_cpd = 'coalesce(cpd.opdescription,cpd.opname)';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null || $_opname == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();

            $wh1 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $wh3 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh3 = ' and cpd.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql_j_cpd = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j_cpd = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cpd.day=jobrotationteam.day and cpd.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select ddd.task,ddd.losttype,sum(ddd.suredk)suredk,sum(ddd.sayi)sayi 
                from (
                    select b.task,b.losttype,sum(suredk) suredk,count(*)sayi 
                    from (
                        select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                            ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                            ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                            ,a.task
                            ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                            ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                        from (
                            select z.client,z.day,z.jobrotation
                                ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                    when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                    else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                                ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                                ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                                ,z.endval,z.beginval
                            from (
                                SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                                        +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                    ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                    ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                                from client_lost_details cld 
                                left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                                left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                                " . $sql_j . "
                                where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                                and cld.day between '" . $_s . "' and '" . $_f . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                            )z
                        )a
                        join (select * from lost_group_details where lostgroup like 'OEE%' and lostgroup not in ('OEE-uretimekapali','OEE-planlidurus')) lgd on lgd.losttype=a.losttype
                        left join lost_types lt on lt.code=a.losttype and lt.finish is null
                        where a.suresn>0 and a.task<>''
                    )b
                    where b.task='" . $_opname . "'
                    group by b.task,b.losttype
                    union all
                    SELECT d.opname as task,'HIZ KAYBI' losttype,round(cast(sum(d.slowdown)/60 as numeric),0) suredk,count(*) sayi 
                    from (
                        select y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown,string_agg(y.opname, ',' order by y.opname)opname 
                        from (
                            select x.client,x.day,x.jobrotation,x.start,min(x.finish)finish,sum(x.suresn)suresn,sum(x.losttime)losttime
                                ,string_agg(distinct x.mould,',') mould
                                ,string_agg(distinct x.mouldgroup,',') mouldgroup
                                ,string_agg(x.production,',') production
                                ,string_agg(x.productioncurrent,',') productioncurrent
                                ,string_agg(distinct x.opname,',') opname
                                ,string_agg(distinct x.erprefnumber,',') erprefnumber
                                ,round(sum(x.proctime))proctime,round(sum(x.proctimecurrent))proctimecurrent
                                ,sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)sss
                                ,case when sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0 then sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent) else 0 end slowdown
                                ,min(x.calculatedtpp)calculatedtpp
                            from (
                                SELECT z.client,z.day,z.jobrotation,z.start,max(z.finish)finish,round(max(z.suresn)/z.operasyonsayisi) suresn,z.mould,z.mouldgroup
                                    ,string_agg(cast(z.production as varchar(10)),',') production
                                    ,string_agg(cast(z.productioncurrent as varchar(10)),',') productioncurrent
                                    ,string_agg(distinct z.opname,',') opname
                                    ,string_agg(distinct z.erprefnumber,',') erprefnumber
                                    ,round(sum(z.proctime)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctime
                                    ,round(sum(z.proctimecurrent)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctimecurrent
                                    ,round(avg(z.calculatedtpp),2) calculatedtpp
                                    ,z.operasyonsayisi
                                    ,(SELECT sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) )losttimex
                                    from client_lost_details cld where type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                                    and day=z.day and client=z.client and start>=z.start and finish<=max(z.finish) and losttype not in ('YETKİNLİK GÖZLEM'))/z.operasyonsayisi losttime
                                from (
                                    SELECT cpd.id,cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent,cpd.start,cpd.finish," . $this->fieldname_opname_cpd . " opname
                                        ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                        ,coalesce(cpd.calculatedtpp,pt.tpp)calculatedtpp
                                        ,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber
                                        ,coalesce(cpd.opsayi,0)opsayi
                                        ,coalesce(nullif((
                                            select count(*)
                                            from client_production_details 
                                            where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type
                                        ),0),1)operasyonsayisi
                                        ,case when exists(select c.workflow from clients c where c.code=cpd.client and c.workflow='Değişken Üretim Aparatlı') then 1 else 0 end ispres
                                    from client_production_details cpd 
                                    left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=cpd.client " : "") .($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                                    " . $sql_j_cpd . "
                                    where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>0 
                                        /*and cpd.client not in (SELECT code from clients where workflow='CNC')*/
                                        and cpd.day between '" . $_s . "' and '" . $_f . "' and cpd.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh3 . "
                                )z
                                group by z.client,z.day,z.jobrotation,z.start,z.mould,z.mouldgroup,z.opname,z.operasyonsayisi
                            )x
                            group by x.client,x.day,x.jobrotation,x.mould,x.mouldgroup,x.opname,x.start,x.operasyonsayisi
                            having sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0
                        )y
                        group by y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown
                    )d
                    where d.opname='" . $_opname . "'
                    group by d.opname
                )ddd
                group by ddd.task,ddd.losttype
                order by sum(ddd.suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['opname'] = $_opname;
            $records['operation_lost_detail'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeLostOperator') {
            $_losttype = isset($_data->losttype) ? $_data->losttype : null;
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'coalesce(opdescription,opname)';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null || $_losttype == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh1 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "select b.employee,b.employeename,sum(suredk) suredk,count(*)sayi 
                from (
                    select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                        ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost,a.employee,a.employeename
                    from (
                        select z.client,z.day,z.jobrotation
                            ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                            ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                            ,z.endval,z.beginval,z.employee,z.employeename
                        from (
                            SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                                    +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost,cld.employee,e.name employeename
                            from client_lost_details cld 
                            left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                            left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                            left join employees e on e.code=cld.employee
                            " . $sql_j . "
                            where cld.type='l_e' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                            and cld.day between '" . $_s . "' and '" . $_f . "' and cld.losttype='" . $_losttype . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                        )z
                    )a
                    left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
                    left join lost_types lt on lt.code=a.losttype and lt.finish is null
                    where a.suresn>0 and a.employeename<>''
                )b
                group by b.employee,b.employeename
                order by sum(suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['losttype'] = $_losttype;
            $records['lost_operator'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeShortLost') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'opdescription';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh1 = '';
            if ($_jobrotation !== '' && $_jobrotation !== null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam !== '' && $_jobrotationteam !== null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam 
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select b.losttype,sum(suredk) suredk,count(*)sayi 
                from (
                    select a.client,a.day,a.jobrotation,a.losttype,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                        ,a.task
                        ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                        ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                    from (
                        select z.client,z.day,z.jobrotation
                            ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                            ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                            ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                            ,z.endval,z.beginval
                        from (
                            SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                                    +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                            from client_lost_details cld 
                            left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                            left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                            " . $sql_j . "
                            where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                            and cld.day between '" . $_s . "' and '" . $_f . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                        )z
                    )a
                    left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
                    left join lost_types lt on lt.code=a.losttype and lt.finish is null
                    where a.suresn>0 
                )b
                where b.info<>''
                group by b.losttype
                order by sum(suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['client_lost_details'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeShortLostOEE') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'opdescription';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh1 = '';
            if ($_jobrotation !== '' && $_jobrotation !== null) {
                $wh1 = ' and cld.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam !== '' && $_jobrotationteam !== null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam 
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cld.day=jobrotationteam.day and cld.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                select b.lostgroupcode,sum(suredk) suredk,count(*)sayi 
                from (
                    select a.client,a.day,a.jobrotation,a.start,a.finish,round(cast(a.suresn/60 as numeric),0)suredk,round(cast(a.suresn/3600 as numeric),4) suresaat,a.suresn
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then 'OEE-uretimekapali' else lgd.lostgroup end lostgroup
                        ,case when ((lgd.lostgroup is null and (a.losttype='GÖREVDE KİMSE YOK' or a.losttype='İŞ YOK')) or a.task like '%DENEME%' or (date_part('dow',a.finish)=0) and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) then case when ((date_part('dow',a.finish)=0 and round(cast(a.suresn/60 as numeric),0)>1 and a.endval is not null and a.beginval is not null) or a.task like '%DENEME%') then 'Çalışılmayan Süre' else 'Sipariş Yetersizliği' end else lgd.lostgroupcode end lostgroupcode
                        ,a.task
                        ,case when ((lgd.lostgroup='OEE-kullanilabilirlik' or lgd.lostgroup='OEE-performans') and a.losttype<>'VARDİYA DEĞİŞİMİ' and a.task not like '%DENEME%' and round(cast(a.suresn/60 as numeric),0)<=" . $_shortlost . " and lt.isshortlost=true) then 'KISA DURUS' else '' end info
                        ,a.opsayi,a.descriptionlost,a.sourceclient,a.sourcelosttype,a.sourcedescriptionlost
                    from (
                        select z.client,z.day,z.jobrotation
                            ,case when (z.losttype='GÖREVDE KİMSE YOK' or z.losttype='') and z.suresn<=" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'VARDİYA DEĞİŞİMİ'
                                when z.losttype='' and z.suresn>" . (60 * (int)$_shortlost) . " and (z.endval is not null or z.beginval is not null) then 'GÖREVDE KİMSE YOK' 
                                else z.losttype end losttype,z.start,z.finish,z.suresn,case when z.refsayi>0 then z.opsayi/z.refsayi else z.opsayi end opsayi
                            ,z.descriptionlost,z.sourceclient,z.sourcelosttype,z.sourcedescriptionlost
                            ,coalesce((SELECT string_agg(distinct " . $this->fieldname_opname . ",',') opname from client_lost_details where type='l_c_t' and client=z.client and day=z.day and jobrotation=z.jobrotation and start=z.start),'')as task
                            ,z.endval,z.beginval
                        from (
                            SELECT cld.client,cld.day,cld.jobrotation,cld.losttype,cld.start,cld.finish
                                ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer)
                                    +(case when jr.endval is not null and jrs.beginval is not null then 1 else 0 end) suresn,jr.endval,jrs.beginval
                                ,coalesce(cld.opsayi,0)opsayi,coalesce(cld.refsayi,0)refsayi
                                ,cld.descriptionlost,cld.sourceclient,cld.sourcelosttype,cld.sourcedescriptionlost
                            from client_lost_details cld 
                            left join job_rotations jr on substr(cast(cld.finish as varchar),12,5)=jr.endval
                            left join job_rotations jrs on substr(cast(cld.start as varchar),12,5)=jrs.beginval
                            " . $sql_j . "
                            where cld.type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                            and cld.day between '" . $_s . "' and '" . $_f . "' and cld.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh1 . "
                        )z
                    )a
                    left join (select * from lost_group_details where lostgroup like 'OEE%') lgd on lgd.losttype=a.losttype
                    left join lost_types lt on lt.code=a.losttype and lt.finish is null
                    where a.suresn>0 
                )b
                where b.info<>''
                group by b.lostgroupcode
                order by sum(suredk) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['client_lost_details'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeSlowdown') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'opdescription';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh2 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh2 = ' and cpd.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cpd.day=jobrotationteam.day and cpd.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                SELECT d.client,round(cast(sum(d.slowdown)/60 as numeric),0) suredk 
                from (
                    select y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown 
                    from (
                        select x.client,x.day,x.jobrotation,x.start,min(x.finish)finish,sum(x.suresn)suresn,sum(x.losttime)losttime
                            ,string_agg(distinct x.mould,',') mould
                            ,string_agg(distinct x.mouldgroup,',') mouldgroup
                            ,string_agg(x.production,',') production
                            ,string_agg(x.productioncurrent,',') productioncurrent
                            ,string_agg(distinct x.opname,',') opname
                            ,string_agg(distinct x.erprefnumber,',') erprefnumber
                            ,round(sum(x.proctime))proctime,round(sum(x.proctimecurrent))proctimecurrent
                            ,sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)sss
                            ,case when sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0 then sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent) else 0 end slowdown
                            ,min(x.calculatedtpp)calculatedtpp
                        from (
                            SELECT z.client,z.day,z.jobrotation,z.start,max(z.finish)finish,round(max(z.suresn)/z.operasyonsayisi) suresn,z.mould,z.mouldgroup
                                ,string_agg(cast(z.production as varchar(10)),',') production
                                ,string_agg(cast(z.productioncurrent as varchar(10)),',') productioncurrent
                                ,string_agg(distinct z.opname,',') opname
                                ,string_agg(distinct z.erprefnumber,',') erprefnumber
                                ,round(sum(z.proctime)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctime
                                ,round(sum(z.proctimecurrent)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctimecurrent
                                ,round(avg(z.calculatedtpp),2) calculatedtpp
                                ,z.operasyonsayisi
                                ,(SELECT sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) )losttimex
                                from client_lost_details cld where type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                                and day=z.day and client=z.client and start>=z.start and finish<=max(z.finish) and losttype not in ('YETKİNLİK GÖZLEM'))/z.operasyonsayisi losttime
                            from (
                                SELECT cpd.id,cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent,cpd.start,cpd.finish,cpd.opname
                                    ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                    ,coalesce(cpd.calculatedtpp,pt.tpp)calculatedtpp
                                    ,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber
                                    ,coalesce(cpd.opsayi,0)opsayi
                                    ,coalesce(nullif((
                                        select count(*)
                                        from client_production_details 
                                        where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type
                                    ),0),1)operasyonsayisi
                                    ,case when exists(select c.workflow from clients c where c.code=cpd.client and c.workflow='Değişken Üretim Aparatlı') then 1 else 0 end ispres
                                from client_production_details cpd 
                                left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=cpd.client " : "") .($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                                " . $sql_j . "
                                where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>0 
                                    /*and cpd.client not in (SELECT code from clients where workflow='CNC')*/
                                    and cpd.day between '" . $_s . "' and '" . $_f . "' and cpd.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh2 . "
                            )z
                            group by z.client,z.day,z.jobrotation,z.start,z.mould,z.mouldgroup,z.opname,z.operasyonsayisi
                        )x
                        group by x.client,x.day,x.jobrotation,x.mould,x.mouldgroup,x.opname,x.start,x.operasyonsayisi
                        having sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0
                    )y
                    group by y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown
                )d
                group by d.client
                order by sum(d.slowdown) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['client_slowdown'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'AnalyzeSlowdownOperation') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            $_jobrotationteam = isset($_data->jobrotationteam) ? $_data->jobrotationteam : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'coalesce(cpd.opdescription,cpd.opname)';
            }
            $_s = $_data->start;
            $_f = $_data->finish;
            $_cl = $_data->clients;

            if ($_s == null || $_f == null || $_cl == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $wh2 = '';
            if ($_jobrotation != '' && $_jobrotation != null) {
                $wh2 = ' and cpd.jobrotation=\'' . $_jobrotation . '\' ';
            }
            $sql_j = '';
            if ($_jobrotationteam != '' && $_jobrotationteam != null) {
                $sql_j = " join (
                    SELECT jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                    from job_rotation_employees jre
                    join employees e on e.code=jre.employee
                    where jre.day between '" . $_s . "' and '" . $_f . "' and e.jobrotationteam='" . $_jobrotationteam . "'
                    GROUP BY jre.day,jre.jobrotation,jre.beginval,jre.endval,e.jobrotationteam
                ) jobrotationteam on cpd.day=jobrotationteam.day and cpd.jobrotation=jobrotationteam.jobrotation ";
            }
            $sql = "
                SELECT d.opname,round(cast(sum(d.slowdown)/60 as numeric),0) suredk 
                from (
                    select y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown,string_agg(y.opname, ',' order by y.opname)opname 
                    from (
                        select x.client,x.day,x.jobrotation,x.start,min(x.finish)finish,sum(x.suresn)suresn,sum(x.losttime)losttime
                            ,string_agg(distinct x.mould,',') mould
                            ,string_agg(distinct x.mouldgroup,',') mouldgroup
                            ,string_agg(x.production,',') production
                            ,string_agg(x.productioncurrent,',') productioncurrent
                            ,string_agg(distinct x.opname,',') opname
                            ,string_agg(distinct x.erprefnumber,',') erprefnumber
                            ,round(sum(x.proctime))proctime,round(sum(x.proctimecurrent))proctimecurrent
                            ,sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)sss
                            ,case when sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0 then sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent) else 0 end slowdown
                            ,min(x.calculatedtpp)calculatedtpp
                        from (
                            SELECT z.client,z.day,z.jobrotation,z.start,max(z.finish)finish,round(max(z.suresn)/z.operasyonsayisi) suresn,z.mould,z.mouldgroup
                                ,string_agg(cast(z.production as varchar(10)),',') production
                                ,string_agg(cast(z.productioncurrent as varchar(10)),',') productioncurrent
                                ,string_agg(distinct z.opname,',') opname
                                ,string_agg(distinct z.erprefnumber,',') erprefnumber
                                ,round(sum(z.proctime)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctime
                                ,round(sum(z.proctimecurrent)/case when max(z.ispres)=1 then z.operasyonsayisi else 1 end)proctimecurrent
                                ,round(avg(z.calculatedtpp),2) calculatedtpp
                                ,z.operasyonsayisi
                                ,(SELECT sum(cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) )losttimex
                                from client_lost_details cld where type='l_c' /*and cld.client not in (SELECT code from clients where workflow='CNC')*/
                                and day=z.day and client=z.client and start>=z.start and finish<=max(z.finish) and losttype not in ('YETKİNLİK GÖZLEM'))/z.operasyonsayisi losttime
                            from (
                                SELECT cpd.id,cpd.client,cpd.day,cpd.jobrotation,cpd.mould,cpd.mouldgroup,cpd.production,cpd.productioncurrent,cpd.start,cpd.finish," . $this->fieldname_opname . " opname
                                    ,cast(extract(epoch from (COALESCE(cpd.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cpd.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                                    ,coalesce(cpd.calculatedtpp,pt.tpp)calculatedtpp
                                    ,cpd.production*coalesce(cpd.calculatedtpp,pt.tpp) proctime,cpd.productioncurrent*coalesce(cpd.calculatedtpp,pt.tpp) proctimecurrent,cpd.erprefnumber
                                    ,coalesce(cpd.opsayi,0)opsayi
                                    ,coalesce(nullif((
                                        select count(*)
                                        from client_production_details 
                                        where client=cpd.client and day=cpd.day and jobrotation=cpd.jobrotation and start=cpd.start and type=cpd.type
                                    ),0),1)operasyonsayisi
                                    ,case when exists(select c.workflow from clients c where c.code=cpd.client and c.workflow='Değişken Üretim Aparatlı') then 1 else 0 end ispres
                                from client_production_details cpd 
                                left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=cpd.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                                " . $sql_j . "
                                where cpd.type in ('c_p','c_p_confirm','c_p_out') and cpd.productioncurrent>0 
                                    /*and cpd.client not in (SELECT code from clients where workflow='CNC')*/
                                    and cpd.day between '" . $_s . "' and '" . $_f . "' and cpd.client in ('" . implode("','", explode(',', $_cl)) . "') " . $wh2 . "
                            )z
                            group by z.client,z.day,z.jobrotation,z.start,z.mould,z.mouldgroup,z.opname,z.operasyonsayisi
                        )x
                        group by x.client,x.day,x.jobrotation,x.mould,x.mouldgroup,x.opname,x.start,x.operasyonsayisi
                        having sum(x.suresn)-sum(x.losttime)-sum(x.proctimecurrent)>0
                    )y
                    group by y.client,y.day,y.jobrotation,y.start,y.finish,y.slowdown
                )d
                group by d.opname
                order by sum(d.slowdown) desc
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records['client_slowdown'] = $stmt->fetchAll();
            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'DashboardClient') {
            $_jobrotation = isset($_data->jobrotation) ? $_data->jobrotation : null;
            if (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149')) {
                $this->fieldname_opname = 'opdescription';
            }
            $_day = $_data->day;
            $_client = $_data->client;

            if ($_day == null || $_client == null) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $records = array();
            $clients = $this->oeeCalculate(10, $_day, $_day, $_client, $_jobrotation, null, $this->fieldname_opname, null, $uri);
            foreach ($clients as $row => $client) {
                $records['oee'] = array(array("client" => $_client
                    ,"kul_ur" => $clients[$row]["oee_carpan_kul_ur"]
                    ,"per_ur" => $clients[$row]["oee_carpan_per_ur"]
                    ,"kal_ur" => $clients[$row]["oee_carpan_kal_ur"]
                    ,"oee_ur" => round($clients[$row]["oee_ur"], 1)
                    ,"kul_fab" => $clients[$row]["oee_carpan_kul_fab"]
                    ,"per_fab" => $clients[$row]["oee_carpan_per_fab"]
                    ,"kal_fab" => $clients[$row]["oee_carpan_kal_fab"]
                    ,"oee_fab" => round($clients[$row]["oee_fab"], 1)
                    ,"kul_gen" => $clients[$row]["oee_carpan_kul_gen"]
                    ,"per_gen" => $clients[$row]["oee_carpan_per_gen"]
                    ,"kal_gen" => $clients[$row]["oee_carpan_kal_gen"]
                    ,"oee_gen" => round($clients[$row]["oee_gen"], 1)
                ));
            }
            $sql = "SELECT aaa.losttype,count(*)sayi,round(cast(sum(aaa.suresn)/60 as decimal),2) suredk from (
                SELECT cld.client,cld.day,cld.losttype,cld.start,cld.finish
                    ,cast(extract(epoch from (COALESCE(cld.finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(cld.start,CURRENT_TIMESTAMP)::timestamp)) as integer) suresn
                from client_lost_details cld 
                where cld.type='l_c_t' and cld.client=:client and cld.day=:day " . ($_jobrotation !== null ? " and cld.jobrotation=:jobrotation " : "") . "
                group by cld.client,cld.day,cld.losttype,cld.start,cld.finish)aaa
            where aaa.suresn>5
            GROUP BY aaa.losttype
            order BY round(cast(sum(aaa.suresn)/60 as decimal),2) desc";
            $stmt = $conn->prepare($sql);
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->bindValue('client', $_client);
            if ($_jobrotation !== null) {
                $stmt->bindValue('jobrotation', $_jobrotation);
            }
            $stmt->execute();
            $records['client_lost_details'] = $stmt->fetchAll();

            $sql = "SELECT string_agg(" . $this->fieldname_opname . ",',' order by " . $this->fieldname_opname . ") opname
                ,round( sum(productioncurrent)/count(*) ) productioncurrent 
                ,round( ( cast(extract(epoch from (COALESCE(finish,CURRENT_TIMESTAMP)::timestamp-COALESCE(start,CURRENT_TIMESTAMP)::timestamp)) as integer) - max(losttime) ) / avg(calculatedtpp) ) productiontarget
            from client_production_details 
            where type='c_p' and client=:client and day=:day 
            and client not in (SELECT code from clients where workflow='CNC')
            " . ($_jobrotation !== null ? " and jobrotation=:jobrotation " : "") . "
            GROUP BY start,finish 
            having sum(productioncurrent)>0
            order by start,finish";
            $stmt = $conn->prepare($sql);
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->bindValue('client', $_client);
            if ($_jobrotation !== null) {
                $stmt->bindValue('jobrotation', $_jobrotation);
            }
            $stmt->execute();
            $records['client_production_details'] = $stmt->fetchAll();

            $sql = "SELECT concat(substr(cast(time as varchar), 6,9),case when substr(cast(time as varchar), 15,2)<'30' then '00' else '30' end) strstart,sum(production)production
            from client_production_signals 
            where type in ('c_s_g') and client=:client and day=:day 
            and client not in (SELECT code from clients where workflow='CNC')
            " . ($_jobrotation !== null ? " and jobrotation=:jobrotation " : "") . "
            GROUP BY concat(substr(cast(time as varchar), 6,9),case when substr(cast(time as varchar), 15,2)<'30' then '00' else '30' end)
            order by concat(substr(cast(time as varchar), 6,9),case when substr(cast(time as varchar), 15,2)<'30' then '00' else '30' end)";
            $stmt = $conn->prepare($sql);
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->bindValue('client', $_client);
            if ($_jobrotation !== null) {
                $stmt->bindValue('jobrotation', $_jobrotation);
            }
            $stmt->execute();
            $records['client_productions'] = $stmt->fetchAll();

            $sql = "SELECT rejecttype,substring(cast(time as varchar(19)),12,8) saat,erprefnumber,opname,quantityretouch,quantityscrap 
            from task_reject_details 
            where type='t_r' and day=:day and client=:client " . ($_jobrotation !== null ? " and jobrotation=:jobrotation " : "") . "
            order by time desc";
            $stmt = $conn->prepare($sql);
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->bindValue('client', $_client);
            if ($_jobrotation !== null) {
                $stmt->bindValue('jobrotation', $_jobrotation);
            }
            $stmt->execute();
            $records['client_reject_details'] = $stmt->fetchAll();

            $sql = "SELECT rejecttype,sum(quantityretouch)quantityretouch,sum(quantityscrap)quantityscrap
            from task_reject_details 
            where type='t_r' and day=:day and client=:client " . ($_jobrotation !== null ? " and jobrotation=:jobrotation " : "") . "
            GROUP BY rejecttype";
            $stmt = $conn->prepare($sql);
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('day', $_day);
            $stmt->bindValue('client', $_client);
            if ($_jobrotation !== null) {
                $stmt->bindValue('jobrotation', $_jobrotation);
            }
            $stmt->execute();
            $records['client_rejects'] = $stmt->fetchAll();

            $records['reportname'] = $reportname;
            return new JsonResponse($records);
        }
        if ($reportname === 'PlanJobRotation') {
            $_day = (isset($_data->day) ? $_data->day : null);
            $_jobrotation = (isset($_data->jobrotation) ? $_data->jobrotation : null);
            $_client_group_details = (isset($_data->client_group_details) ? $_data->client_group_details : null);
            if ($_day == null || (($_jobrotation == null || $_jobrotation == ''))) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $result = array("program" => array(),"notlar" => "");
            $taskcountforclient = (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? 9 : 6);
            $sql_c = "select c.* from clients c where c.workflow<>'Gölge Cihaz' order by c.code";
            if ($_client_group_details) {
                $sql_c = "select c.* 
                from clients c 
                join client_group_details cgd on cgd.clientgroup='locationclient' and cgd.client=c.code and cgd.clientgroupcode=:clientgroupcode
                where c.workflow<>'Gölge Cihaz'
                order by c.code";
            }
            $opcounter = "coalesce(nullif(pt.opcounter,0),1)";
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $opcounter = "coalesce((SELECT coalesce(cmg.operatorcount,1)
                from client_mould_groups cmg
                join client_mould_details cmd on cmd.clientmouldgroup=cmg.code and cmg.client=cmd.client
                join mould_groups mg on mg.code=cmd.mouldgroup
                join mould_details md on md.mould=mg.mould and md.mouldgroup=mg.code
                where cmg.client=tl.client and md.opname=tl.opname limit 1),coalesce(nullif(pt.opcounter,0),1))";
            }
            $stmt_c = $conn->prepare($sql_c);
            if ($_client_group_details) {
                $stmt_c->bindValue('clientgroupcode', $_client_group_details);
            }
            $stmt_c->execute();
            $records_c = $stmt_c->fetchAll();
            foreach ($records_c as $row_c) {
                $sql = "SELECT tl.client
                    ,string_agg(tl.opname,',' order by tl.erprefnumber) opname
					,string_agg(distinct tl.opdescription,',')opdescription
                    ,string_agg(tl.erprefnumber,',' order by tl.erprefnumber)erprefnumber
                    ,tl.opcounter,tl.tpp
                    ,substring(cast(tl.plannedstart as varchar(20)),6,11) ps
                    ,substring(cast(tl.plannedfinish as varchar(20)),6,11) pf 
                    ,max(tl.productcount)-min(tl.productdonecount) vardiyaplanlanan
                    ,round(3600/tl.tpp) hedefuretimsaat
                from (
                    SELECT tl.client,tl.erprefnumber,tl.opname,tl.idx," . $opcounter . " opcounter,tl.productcount
                        ,tl.productdonecount,tl.plannedstart,tl.plannedfinish,coalesce(nullif(tl.tpp,0),1)tpp
                        ,(select max(m.description) from mould_details md join moulds m on m.code=md.mould where md.opname=tl.opname )opdescription
                    from task_lists tl
                    left join product_trees pt on pt.name=tl.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=tl.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                    where tl.finish is null and tl.plannedstart is not null 
                        and tl.taskfromerp=true and tl.deleted_at is null
                        and tl.client=:client /*and tl.plannedstart between :ps1 and :ps2 */
                        " . (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? " and coalesce(tl.status,'')<>'P' " : "") . "
                        and tl.plannedstart<cast(:day1 as date)+ interval '" . (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '9') ? "25" : "25") . " day'
                )tl
                GROUP BY tl.client,tl.opcounter,tl.plannedstart,tl.plannedfinish,tl.tpp
                order by tl.plannedstart limit :lim";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue('client', $row_c["code"]);
                $stmt->bindValue('day1', $_day);
                $stmt->bindValue('lim', $taskcountforclient);
                $stmt->execute();
                $records = $stmt->fetchAll();
                $result["program"][] = array("id" => $row_c["id"],"code" => $row_c["code"],"tasks" => $records);
                $sql_pn = "SELECT * from plan_notes order by recordorder";
                $stmt_pn = $conn->prepare($sql_pn);
                $stmt_pn->execute();
                $result["notlar"] = $stmt_pn->fetchAll();
            }
            return new JsonResponse(array("totalProperty" => 1,"records" => $result));
        }
        if ($reportname === 'PlanJobRotation2') {
            $_day = (isset($_data->day) ? $_data->day : null);
            $_jobrotation = (isset($_data->jobrotation) ? $_data->jobrotation : null);
            $_client_group_details = (isset($_data->client_group_details) ? $_data->client_group_details : null);
            if ($_day == null || (($_jobrotation == null || $_jobrotation == ''))) {
                return $this->msgError(
                    ($this->_container == null ? $this->container : $this->_container)->get('translator')->trans('err.main.control_parameters', array(), 'App'),
                    401
                );
            }
            $result = array("program" => array(),"notlar" => "");
            //$taskcountforclient = (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? 9 : 6);
            $sql_c = "select c.code from clients c where c.workflow<>'Gölge Cihaz' order by c.code";
            if ($_client_group_details) {
                $sql_c = "select c.code 
                from clients c 
                join client_group_details cgd on cgd.clientgroup='locationclient' and cgd.client=c.code and cgd.clientgroupcode=:clientgroupcode
                where c.workflow<>'Gölge Cihaz'
                order by c.code";
            }
            $opcounter = "coalesce(nullif(pt.opcounter,0),1)";
            if ($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') {
                $opcounter = "coalesce((SELECT coalesce(cmg.operatorcount,1)
                from client_mould_groups cmg
                join client_mould_details cmd on cmd.clientmouldgroup=cmg.code and cmg.client=cmd.client
                join mould_groups mg on mg.code=cmd.mouldgroup
                join mould_details md on md.mould=mg.mould and md.mouldgroup=mg.code
                where cmg.client=tl.client and md.opname=tl.opname limit 1),coalesce(nullif(pt.opcounter,0),1))";
            }
            $sql = "SELECT tl.client
                ,string_agg(tl.opname,', ' order by tl.erprefnumber) opname
                ,string_agg(distinct tl.opdescription,', ')opdescription
                ,string_agg(tl.erprefnumber,', ' order by tl.erprefnumber)erprefnumber
                ,tl.opcounter,tl.tpp
                ,(cast(tl.plannedstart as varchar(20))) ps
                ,(cast(tl.plannedfinish as varchar(20))) pf 
                ,max(tl.productcount)-min(tl.productdonecount) vardiyaplanlanan
                ,round(3600/tl.tpp) hedefuretimsaat
            from (
                SELECT tl.client,tl.erprefnumber,tl.opname,tl.idx," . $opcounter . " opcounter,tl.productcount
                    ,tl.productdonecount,tl.plannedstart,tl.plannedfinish,coalesce(nullif(tl.tpp,0),1)tpp
                    ,(select max(m.description) from mould_details md join moulds m on m.code=md.mould where md.opname=tl.opname )opdescription
                from task_lists tl
                left join product_trees pt on pt.name=tl.opname and pt.finish is null and pt.materialtype='O' " . ($_SERVER['HTTP_HOST'] == '10.10.0.10' && $uri !== '/sahince' ? " and pt.isdefault=true and pt.client=tl.client " : "") . ($_SERVER['HTTP_HOST'] == '10.0.0.101' ? " and coalesce(pt.client,cpd.client)=cpd.client " : "")."
                where tl.finish is null and tl.plannedstart is not null 
                    and tl.taskfromerp=true and tl.deleted_at is null
                    and tl.client in (".$sql_c.")
                    " . (($uri == '/pres' && $_SERVER['HTTP_HOST'] == '172.16.1.149') ? " and coalesce(tl.status,'')<>'P' " : "") . "
                    and tl.plannedstart<cast(:day1 as date)+ interval '25 day'
            )tl
            GROUP BY tl.client,tl.opcounter,tl.plannedstart,tl.plannedfinish,tl.tpp
            order by tl.plannedstart";
            $stmt = $conn->prepare($sql);
            //$stmt->bindValue('client', $row_c["code"]);
            if ($_client_group_details) {
                $stmt->bindValue('clientgroupcode', $_client_group_details);
            }
            $stmt->bindValue('day1', $_day);
            //$stmt->bindValue('lim', $taskcountforclient);
            $stmt->execute();
            $records = $stmt->fetchAll();
            $result["tasks"] = $records;
            $sql_pn = "SELECT * from plan_notes order by recordorder";
            $stmt_pn = $conn->prepare($sql_pn);
            $stmt_pn->execute();
            $result["notlar"] = $stmt_pn->fetchAll();
            return new JsonResponse(array("totalProperty" => 1,"records" => $result));
        }
    }

    /**
     * @Route(path="/Reports", name="Reports-module", options={"expose"=true}, methods={"POST"})
     */
    public function renderBackendModule(Request $request, $_locale)
    {
        $reportname = $request->get('reportname');
        //$cbg = $this->checkBeforeGet($request);
        //if ($cbg === true) {
        $data = array(
            'modulename' => $request->request->get('modulename'),
            'audit' => 0,
            'data' => array(),
            'extras' => array()
        );
        if ($reportname === 'AnalyzeLost') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
        }
        if ($reportname === 'AnalyzeLostClient') {
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $lost_types = $this->getComboValues($request, $_locale, 1, 1000, 'lost_types');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['lost_types'] = json_decode($lost_types->getContent())->records;
        }
        if ($reportname === 'AnalyzeLostOEE') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
        }
        if ($reportname === 'AnalyzeLostOperation') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $lost_types = $this->getComboValues($request, $_locale, 1, 100, 'lost_types');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
            $data['extras']['lost_types'] = json_decode($lost_types->getContent())->records;
        }
        if ($reportname === 'AnalyzeOperationLost') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
        }
        if ($reportname === 'AnalyzeOperationLostDetail') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
        }
        if ($reportname === 'AnalyzeLostOperator') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $lost_types = $this->getComboValues($request, $_locale, 1, 100, 'lost_types');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
            $data['extras']['lost_types'] = json_decode($lost_types->getContent())->records;
        }
        if ($reportname === 'AnalyzeShortLost') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
        }
        if ($reportname === 'AnalyzeShortLostOEE') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
        }
        if ($reportname === 'AnalyzeSlowdown') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
        }
        if ($reportname === 'AnalyzeSlowdownOperation') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $job_rotation_teams = $this->getComboValues($request, $_locale, 1, 100, 'job_rotation_teams');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
            $data['extras']['job_rotation_teams'] = json_decode($job_rotation_teams->getContent())->records;
        }
        if ($reportname === 'DashboardClient') {
            $clients = $this->getComboValues($request, $_locale, 1, 100, 'clients');
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(),
                'extras' => array()
            );
            $data['extras']['clients'] = json_decode($clients->getContent())->records;
            $data['extras']['job_rotations'] = json_decode($job_rotations->getContent())->records;
        }
        if ($reportname === 'EmployeeTaskSummary') {
            $employees = $this->getComboValues($request, $_locale, 1, 1000, 'employees', 'code', "concat(code,'-',name)");
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array('employees' => json_decode($employees->getContent())->records),
                'extras' => array()
            );
        }
        if ($reportname === 'PlanJobRotation'||$reportname === 'PlanJobRotation2') {
            $sql = "SELECT clientgroupcode as id,clientgroupcode as code 
            FROM client_group_details 
            where clientgroup='locationclient' 
            GROUP BY clientgroupcode 
            order by clientgroupcode";
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $client_group_details = $stmt->fetchAll();
            $job_rotations = $this->getComboValues($request, $_locale, 1, 100, 'job_rotations');
            $data = array(
                'modulename' => $request->request->get('modulename'),
                'audit' => 0,
                'data' => array(
                    'client_group_details' => $client_group_details
                    ,'job_rotations' => json_decode($job_rotations->getContent())->records
                ),
                'extras' => array()
            );
        }
        return $this->render('Reports/' . $reportname . '.html.twig', $data);

        //} else {
        //    return $cbg;
        //}
    }

    /**
     * @Route(path="/Reports/all/{pg}/{lm}", defaults={"pg": 1, "lm": 25}, requirements={"pg": "\d+","lm": "\d+"}, name="Reports-showall", options={"expose"=true}, methods={"GET"})
     */
    public function showAllAction(Request $request, $_locale, $pg, $lm)
    {
        $cbg = $this->checkBeforeGet($request);
        if ($cbg === true) {
            $records = $this->getAllRecords($this, $request, $pg, $lm);

            return new JsonResponse($records);
        } else {
            return $cbg;
        }
    }
}
