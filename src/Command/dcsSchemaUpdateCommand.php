<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\Tools\SchemaTool;

class dcsSchemaUpdateCommand extends \Doctrine\ORM\Tools\Console\Command\SchemaTool\AbstractCommand
{
    /**
     * @var string
     */
    protected $name = 'dcs:schema:update';

    /**
     * @var string
     */
    protected $path = 'D:\\Dev\\NodeApps\\verimotRT-Client\\src';

    protected function configure()
    {
        $this
        ->setName($this->name)
        ->setDescription(
            'Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata.'
        )
        ->setDefinition(array(
            new InputOption(
                'complete',
                null,
                InputOption::VALUE_NONE,
                'If defined, all assets of the database which are not relevant to the current metadata will be dropped.'
            ),

            new InputOption(
                'dump-sql',
                null,
                InputOption::VALUE_NONE,
                'Dumps the generated SQL statements to the screen (does not execute them).'
            ),
            new InputOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Causes the generated SQL statements to be physically executed against your database.'
            ),
            new InputOption(
                'dcs',
                'd',
                InputOption::VALUE_NONE,
                'Dcs database sync.'
            ),
        ));

        $this->setHelp(
            <<<EOT
The <info>%command.name%</info> command generates the SQL needed to
synchronize the database schema with the current mapping metadata of the
default entity manager.

For example, if you add metadata for a new column to an entity, this command
would generate and output the SQL needed to add the new column to the database:

<info>%command.name% --dump-sql</info>

Alternatively, you can execute the generated queries:

<info>%command.name% --force</info>

If both options are specified, the queries are output and then executed:

<info>%command.name% --dump-sql --force</info>

Finally, be aware that if the <info>--complete</info> option is passed, this
task will drop all database assets (e.g. tables, etc) that are *not* described
by the current metadata. In other words, without this option, this task leaves
untouched any "extra" tables that exist in the database, but which aren't
described by any metadata.

<comment>Hint:</comment> If you have a database with tables that should not be managed
by the ORM, you can use a DBAL functionality to filter the tables and sequences down
on a global level:

    \$config->setFilterSchemaAssetsExpression(\$regexp);
EOT
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ui = new SymfonyStyle($input, $output);
        //$emHelper = $this->getHelper('em');

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

        $metadatas = $em->getMetadataFactory()->getAllMetadata();

        if (! empty($metadatas)) {
            // Create SchemaTool
            $tool = new SchemaTool($em);

            return $this->executeSchemaCommand($input, $output, $tool, $metadatas, $ui);
        } else {
            $output->writeln('No Metadata Classes to process.');
            return 0;
        }
    }

    protected function executeSchemaCommand(InputInterface $input, OutputInterface $output, SchemaTool $schemaTool, array $metadatas, SymfonyStyle $ui)
    {
        // Defining if update is complete or not (--complete not defined means $saveMode = true)
        $saveMode = ! $input->getOption('complete');

        $sqls = $schemaTool->getUpdateSchemaSql($metadatas, $saveMode);

        if (0 === count($sqls)) {
            $output->writeln('Nothing to update - your database is already in sync with the current entity metadata.');

            return 0;
        }

        $dumpSql = true === $input->getOption('dump-sql');
        $force   = true === $input->getOption('force');
        $sync    = true === $input->getOption('dcs');

        if ($dumpSql) {
            $output->writeln(implode(';' . PHP_EOL, $sqls) . ';');
        }

        /*if($sync){
            $this->sequelizeMigration($output,$sqls);
        }
        return 0;*/
        if ($force) {
            if ($dumpSql) {
                $output->writeln('');
            }
            $output->writeln('Updating database schema...');
            $schemaTool->updateSchema($metadatas, $saveMode);
            if ($sync) {
                $this->sequelizeMigration($output, $sqls);
            }
            $pluralization = (1 === count($sqls)) ? 'query was' : 'queries were';

            $output->writeln(sprintf('Database schema updated successfully! "<info>%s</info>" %s executed', count($sqls), $pluralization));
        }

        if ($dumpSql || $force) {
            return 0;
        }

        $output->writeln('<comment>ATTENTION</comment>: This operation should not be executed in a production environment.');
        $output->writeln('           Use the incremental update to detect changes during development and use');
        $output->writeln('           the SQL DDL provided to manually update your database in production.');
        $output->writeln('');

        $output->writeln(sprintf('The Schema-Tool would execute <info>"%s"</info> queries to update the database.', count($sqls)));
        $output->writeln('Please run the operation by passing one - or both - of the following options:');

        $output->writeln(sprintf('    <info>%s --force</info> to execute the command', $this->getName()));
        $output->writeln(sprintf('    <info>%s --dump-sql</info> to dump the SQL statements to the screen', $this->getName()));

        return 1;
    }

    protected function sequelizeMigration(OutputInterface $output, $sqls)
    {
        $dbg = 0;
        foreach ($sqls as $query) {
            $query = str_replace(', ', ',', $query);
            if (strpos($query, '_audit', 0) === false && strpos($query, 'COMMENT ON COLUMN', 0) === false) {
                $output->writeln($query);
                $proc = "";
                $table = "";
                $column = "";
                $type = "";
                $ctype = "";
                $now = \DateTime::createFromFormat('U.u', microtime(true));
                $_name = $now->format("YmdHisu");
                if (strpos($query, 'ALTER', 0) !== false) {
                    $tmp = explode(' ', $query);
                    switch ($tmp[1]) {
                        case "TABLE":
                            $table = $tmp[2];
                            $proc = $tmp[3];
                            $column = $tmp[4];

                            $fname = $_name."-alter-".$proc."-".$table.".js";
                            $migpath = $this->path."\\migrations\\".$fname;
                            $modpath = $this->path."\\models\\".$table.".js";
                            $modfile = fopen($modpath, "r") or die("Unable to open file!");
                            $content = fread($modfile, filesize($modpath));
                            fclose($modfile);
                            if ($proc === "ADD") {
                                $type = $tmp[5];
                                if ($type == "BOOLEAN" || $type == "BOOL") {
                                    $ctype = "BOOLEAN";
                                }
                                if ($type == "INT" || $type == "INTEGER") {
                                    $ctype = "INTEGER";
                                }
                                if (strpos($type, "VARCHAR", 0) !== false) {
                                    $ctype = str_replace("VARCHAR", "STRING", $type);
                                }
                                if (strpos($type, "TIMESTAMP", 0) !== false) {
                                    $ctype = "DATE";
                                }
                                if ($type == "DATE") {
                                    $ctype = "DATEONLY";
                                }
                                if ($type == "TEXT") {
                                    $ctype = "TEXT";
                                }
                                if (strpos($type, "JSON", 0) !== false) {
                                    $ctype = "JSON";
                                }
                                if (strpos($type, "NUMERIC", 0) !== false) {
                                    $ctype = str_replace("NUMERIC", "DECIMAL", $type);
                                }
                                if (strpos($content, $column.':', 0) === false) {
                                    $tmpcontent = explode('{', $content);
                                    $tmpcontent2 = explode(',', $tmpcontent[2]);
                                    $lastItem = "\xA".str_replace("\xA", "", str_replace("}", "", $tmpcontent2[count($tmpcontent2) - 2])).",\xA"
                                            . "     $column: DataTypes.$ctype\xA} ";
                                    $tmpcontent2[count($tmpcontent2) - 2] = $lastItem;
                                    $tmpcontent[2] = implode(",", $tmpcontent2);
                                    $content2 = implode("{", $tmpcontent);
                                    if ($dbg == 0) {
                                        $myfile = fopen($modpath, "w") or die("Unable to open file!");
                                        fwrite($myfile, $content2);
                                        fclose($myfile);
                                    }

                                    $str = "'use strict';\xA"
                                    . "module.exports = {\xA"
                                    . "  up: function(queryInterface, Sequelize) {\xA"
                                    . "    return queryInterface.addColumn({\xA"
                                    . "		tableName: '$table',\xA"
                                    . "		schema: 'public'\xA"
                                    . "	  },\xA"
                                    . "	  '$column',\xA"
                                    . "	  Sequelize.$ctype\xA"
                                    . "	);\xA"
                                    . "  },\xA"
                                    . "  down: function(queryInterface, Sequelize) {\xA"
                                    . "  }\xA"
                                    . "};";
                                    if ($dbg == 0) {
                                        $myfile = fopen($migpath, "w") or die("Unable to open file!");
                                        fwrite($myfile, $str);
                                        fclose($myfile);
                                    } else {
                                        $output->writeln($str);
                                    }
                                } else {
                                    $output->writeln($table.".js model file has already exists ".$column." column.");
                                }
                            }
                            if ($proc === "DROP") {
                                if (strpos($content, $column, 0) !== false) {
                                    $tmpcontent = explode('{', $content);
                                    $tmpcontent1 = explode('}', $tmpcontent[2]);
                                    $tmpcontent2 = explode(',', $tmpcontent1[0]);
                                    $idx = -1;
                                    for ($i = 0;$i < count($tmpcontent2);$i++) {
                                        if (strpos($tmpcontent2[$i], $column, 0) !== false) {
                                            $idx = $i;
                                        }
                                    }
                                    if ($idx > -1) {
                                        unset($tmpcontent2[$idx]);
                                        if (count($tmpcontent2) > $idx) {
                                            if (strpos($tmpcontent2[$idx + 1], ":", 0) === false) {
                                                unset($tmpcontent2[$idx + 1]);
                                            }
                                        }
                                        $tmpcontent1[0] = implode(",", $tmpcontent2);
                                        $tmpcontent[2] = implode("}", $tmpcontent1);
                                        $content2 = implode("{", $tmpcontent);
                                        if ($dbg == 0) {
                                            $myfile = fopen($modpath, "w") or die("Unable to open file!");
                                            fwrite($myfile, $content2);
                                            fclose($myfile);
                                        } else {
                                            $output->writeln($content2);
                                        }
                                        $str = "'use strict';\xA"
                                            . "module.exports = {\xA"
                                            . "  up: function(queryInterface, Sequelize) {\xA"
                                            . "    return queryInterface.removeColumn({\xA"
                                            . "		tableName: '$table',\xA"
                                            . "		schema: 'public'\xA"
                                            . "	  },\xA"
                                            . "	  '$column'\xA"
                                            . "	);\xA"
                                            . "  },\xA"
                                            . "  down: function(queryInterface, Sequelize) {\xA"
                                            . "  }\xA"
                                            . "};";
                                        if ($dbg == 0) {
                                            $myfile = fopen($migpath, "w") or die("Unable to open file!");
                                            fwrite($myfile, $str);
                                            fclose($myfile);
                                        }
                                    }
                                } else {
                                    $output->writeln($table.".js model file does not exists ".$column." column.");
                                }
                            }
                            if ($proc === "ALTER") {
                                if (strpos($content, $column, 0) !== false) {
                                    $optype = $tmp[5];
                                    $type = $tmp[6];
                                    if ($type == "BOOLEAN" || $type == "BOOL") {
                                        $ctype = "BOOLEAN";
                                    }
                                    if ($type == "INT" || $type == "INTEGER") {
                                        $ctype = "INTEGER";
                                    }
                                    if (strpos($type, "VARCHAR", 0) !== false) {
                                        $ctype = str_replace("VARCHAR", "STRING", $type);
                                    }
                                    if (strpos($type, "TIMESTAMP", 0) !== false) {
                                        $ctype = "DATE";
                                    }
                                    if ($type == "DATE") {
                                        $ctype = "DATEONLY";
                                    }
                                    if ($type == "TEXT") {
                                        $ctype = "TEXT";
                                    }
                                    if (strpos($type, "JSON", 0) !== false) {
                                        $ctype = "JSON";
                                    }
                                    if (strpos($type, "NUMERIC", 0) !== false) {
                                        $ctype = str_replace("NUMERIC", "DECIMAL", $type);
                                    }
                                    if ($optype === "TYPE") {
                                        $tmpcontent = explode('{', $content);
                                        $tmpcontent1 = explode('}', $tmpcontent[2]);
                                        $tmpcontent2 = explode(',', $tmpcontent1[0]);
                                        $idx = -1;
                                        for ($i = 0;$i < count($tmpcontent2);$i++) {
                                            if (strpos($tmpcontent2[$i], $column, 0) !== false) {
                                                $idx = $i;
                                            }
                                        }
                                        if ($idx > -1) {
                                            $tmpcontent2[$idx] = "\xA     $column: DataTypes.$ctype";
                                            if (count($tmpcontent2) > $idx) {
                                                if (strpos($tmpcontent2[$idx + 1], ":", 0) === false) {
                                                    unset($tmpcontent2[$idx + 1]);
                                                }
                                            }
                                            $tmpcontent1[0] = implode(",", $tmpcontent2);
                                            $tmpcontent[2] = implode("}", $tmpcontent1);
                                            $content2 = implode("{", $tmpcontent);
                                            if ($dbg == 0) {
                                                $myfile = fopen($modpath, "w") or die("Unable to open file!");
                                                fwrite($myfile, $content2);
                                                fclose($myfile);
                                            } else {
                                                $output->writeln($content2);
                                            }
                                            $str = "'use strict';\xA"
                                                . "module.exports = {\xA"
                                                . "  up: function(queryInterface, Sequelize) {\xA"
                                                . "    return queryInterface.changeColumn('$table','$column',{\xA"
                                                . "		type: Sequelize.$ctype\xA"
                                                . "	  }\xA"
                                                . "	);\xA"
                                                . "  },\xA"
                                                . "  down: function(queryInterface, Sequelize) {\xA"
                                                . "  }\xA"
                                                . "};";
                                            if ($dbg == 0) {
                                                $myfile = fopen($migpath, "w") or die("Unable to open file!");
                                                fwrite($myfile, $str);
                                                fclose($myfile);
                                            } else {
                                                $output->writeln($str);
                                            }
                                        }
                                    }
                                } else {
                                    $output->writeln($table.".js model file does not exists ".$column." column.");
                                }
                            }
                            if ($proc === "RENAME") {
                                $columnfrom = $tmp[5];
                                $columnto = $tmp[7];
                                if (strpos($content, $columnfrom, 0) !== false) {
                                    $tmpcontent = explode('{', $content);
                                    $tmpcontent1 = explode('}', $tmpcontent[2]);
                                    $tmpcontent2 = explode(',', $tmpcontent1[0]);
                                    $idx = -1;
                                    for ($i = 0;$i < count($tmpcontent2);$i++) {
                                        if (strpos($tmpcontent2[$i], $columnfrom, 0) !== false) {
                                            $idx = $i;
                                        }
                                    }
                                    if ($idx > -1) {
                                        $tmpcontent2[$idx] = str_replace($columnfrom, $columnto, $tmpcontent2[$idx]);
                                        $tmpcontent1[0] = implode(",", $tmpcontent2);
                                        $tmpcontent[2] = implode("}", $tmpcontent1);
                                        $content2 = implode("{", $tmpcontent);
                                        if ($dbg == 0) {
                                            $myfile = fopen($modpath, "w") or die("Unable to open file!");
                                            fwrite($myfile, $content2);
                                            fclose($myfile);
                                        } else {
                                            $output->writeln($content2);
                                        }
                                        $str = "'use strict';\xA"
                                            . "module.exports = {\xA"
                                            . "  up: function(queryInterface, Sequelize) {\xA"
                                            . "    return queryInterface.renameColumn('$table','$columnfrom','$columnto');\xA"
                                            . "  },\xA"
                                            . "  down: function(queryInterface, Sequelize) {\xA"
                                            . "  }\xA"
                                            . "};";
                                        if ($dbg == 0) {
                                            $myfile = fopen($migpath, "w") or die("Unable to open file!");
                                            fwrite($myfile, $str);
                                            fclose($myfile);
                                        } else {
                                            $output->writeln($str);
                                        }
                                    }
                                } else {
                                    $output->writeln($table.".js model file does not exists ".$columnfrom." column.");
                                }
                                $output->writeln($table." RENAME ".$columnfrom."---".$columnto." column.");
                            }
                            break;
                        case "COLUMN":
                            $output->writeln("COLUMN");
                            break;
                    }
                }
                if (strpos($query, 'CREATE', 0) !== false) {
                    $tmp = explode(' ', str_replace('CREATE ', '', $query));
                    switch ($tmp[0]) {
                        case "TABLE":
                            $fields = array();
                            $nlist = array('id','create_user_id','created_at','update_user_id','updated_at','delete_user_id','deleted_at','version','primary');
                            $table = $tmp[1];
                            $strFields = substr(substr($query, 0, -1), strpos($query, $table, 0) + strlen($table) + 2);
                            $arrFields = explode(",", $strFields);
                            for ($i = 0;$i < count($arrFields);$i++) {
                                $field = explode(" ", $arrFields[$i]);
                                $name = strtolower($field[0]);
                                $type = $field[1];
                                $ctype = "";
                                if ($type == "BOOLEAN" || $type == "BOOL") {
                                    $ctype = "boolean";
                                }
                                if ($type == "INT" || $type == "INTEGER") {
                                    $ctype = "integer";
                                }
                                if (strpos($type, "VARCHAR", 0) !== false) {
                                    $ctype = str_replace("VARCHAR", "string", $type);
                                }
                                if (strpos($type, "TIMESTAMP", 0) !== false) {
                                    $ctype = "date";
                                }
                                if ($type == "DATE") {
                                    $ctype = "dateonly";
                                }
                                if ($type == "TEXT") {
                                    $ctype = "text";
                                }
                                if (strpos($type, "JSON", 0) !== false) {
                                    $ctype = "json";
                                }
                                if (strpos($type, "NUMERIC", 0) !== false) {
                                    $ctype = str_replace("NUMERIC", "decimal", $type);
                                }
                                if (in_array($name, $nlist) == false && $name !== null) {
                                    if ($i > 0 && strpos($arrFields[$i - 1], "NUMERIC", 0) !== false && is_numeric(substr($name, 0, 1))) {
                                        $fields[count($fields) - 1] .= "|".$name;
                                    } else {
                                        $fields[] = "$name:$ctype";
                                    }
                                }
                            }
                            $modpath = $this->path."\\models\\".$table.".js";
                            if (file_exists($modpath)) {
                                unlink($modpath);
                            }
                            $command = 'node_modules\.bin\sequelize model:create --name '.$table.' --attributes "'.implode(", ", $fields).'" ';
                            $output->writeln($command);
                            exec(substr($this->path, 0, 2)." && cd ".$this->path." && ".$command);
                            break;
                        case "INDEX":
                            $idx_name = $tmp[1];
                            $table = $tmp[3];
                            $fields = str_replace(',', '\',\'', str_replace(')', '', str_replace('(', '', $tmp[4])));

                            $fname = $_name."-create-index-".$idx_name.".js";
                            $migpath = $this->path."\\migrations\\".$fname;

                            $str = "'use strict';\xA"
                                . "module.exports = {\xA"
                                . "  up: function(queryInterface, Sequelize) {\xA"
                                . "    return queryInterface.addIndex(\xA"
                                . "		'$table',\xA"
                                . "		['$fields'],\xA"
                                . "  { 	  indexName: '$idx_name'\xA"
                                . "  });},\xA"
                                . "  down: function(queryInterface, Sequelize) {\xA"
                                . "     queryInterface.removeIndex('$table', '$idx_name');\xA"
                                . "  }\xA"
                                . "};";
                            if ($dbg == 0) {
                                $myfile = fopen($migpath, "w") or die("Unable to open file!");
                                fwrite($myfile, $str);
                                fclose($myfile);
                            } else {
                                $output->writeln($str);
                            }

                            break;
                    }
                }

                if (strpos($query, 'DROP', 0) !== false) {
                    $tmp = explode(' ', str_replace('DROP ', '', $query));
                    switch ($tmp[0]) {
                        case "INDEX":
                            $idx_name = $tmp[1];
                            $table = explode('__', $idx_name)[1];
                            $fname = $_name."-drop-index-".$idx_name.".js";
                            $migpath = $this->path."\\migrations\\".$fname;

                            $str = "'use strict';\xA"
                                . "module.exports = {\xA"
                                . "  up: function(queryInterface, Sequelize) {\xA"
                                . "    return queryInterface.removeIndex(\xA"
                                . "		'$table',\xA"
                                . "		'$idx_name');\xA"
                                . "  }//,\xA"
                                . "  //down: function(queryInterface, Sequelize) {\xA"
                                . "  //   queryInterface.removeIndex('$table', '$idx_name');\xA"
                                . "  //}\xA"
                                . "};";
                            if ($dbg == 0) {
                                $myfile = fopen($migpath, "w") or die("Unable to open file!");
                                fwrite($myfile, $str);
                                fclose($myfile);
                            } else {
                                $output->writeln($str);
                            }
                            break;
                    }
                }
            }
        }
        exec(substr($this->path, 0, 2)." && cd ".$this->path." && node_modules\.bin\sequelize db:migrate");
    }
}
