<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
#use Symfony\Component\Console\Question\Question;
use Doctrine\Common\Annotations\AnnotationReader;

class dcsSequelizeMigrateCommand extends Command
{
    protected function configure()
    {
         $this
          ->setName('dcs:sequelize:migrate')
          ->setDescription('Verimot RT sequelize migrate islemi yapar')
          ->addOption('entity', null, InputOption::VALUE_REQUIRED, 'entity name')
          ->addOption('path', null, InputOption::VALUE_REQUIRED, 'folder path')
          ->addOption('force', null, InputOption::VALUE_REQUIRED, 'drop before create', '0')
          ->setHelp(<<<EOT
<info>kai:sequelize:migrate</info> Verimot RT sequelize migrate islemi yapar:</info>
<info>--entity "App\\Entity\\JobRotation"</info>
<info>--path "D:\\Dev\\Node Apps\\verimotRT-Client\\src"</info>
<info>--force "1"</info>
<info>php bin/console kai:sequelize:migrate</info>

EOT
            )
        /*$this
            ->setName('kai:sequelize:migrate')
            ->setDescription('Verimot RT sequelize migrate islemi yapar')
            ->setDefinition(array(
                new InputArgument('entity', InputArgument::REQUIRED, 'entity'),
                new InputArgument('path', InputArgument::REQUIRED, 'path'),
                new InputArgument('force', InputArgument::OPTIONAL, 'force','0')
            ))
            ->setHelp(<<<EOT
<info>kai:sequelize:migrate</info> Verimot RT sequelize migrate islemi yapar:</info>
<info>--entity "App/JobRotation"</info>
<info>--path "D:\\Dev\\Node Apps\\verimotRT-Client\\src"</info>
<info>--force "1"</info>
<info>php bin/console kai:sequelize:migrate</info>

EOT
            )*/
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fields=array();
        $entity = $input->getOption('entity');
        $path   = $input->getOption('path');
        $force  = $input->getOption('force');
        $originalObject=new $entity();
        $reflectionObject = new \ReflectionObject($originalObject);
        $reader = new AnnotationReader();
        $nlist=array('id','create_user_id','created_at','update_user_id','updated_at','delete_user_id','deleted_at');
        foreach ($reflectionObject->getProperties() as $reflectionProperty) {
            $propertyAnnotation = $reader->getPropertyAnnotation($reflectionProperty, 'Doctrine\ORM\Mapping\Column');
            if($propertyAnnotation==null){
                $propertyAnnotation = $reader->getPropertyAnnotation($reflectionProperty, 'Doctrine\ORM\Mapping\JoinColumn');
                $type="integer";
            }else{
                $type=$propertyAnnotation->type;
                $length=$propertyAnnotation->length;
                $precision=$propertyAnnotation->precision;
                $scale=$propertyAnnotation->scale;
                //$scale=0;//sequelize datatype tanımında , olması durumunda çalışmıyor
            }
            $name=strtolower($propertyAnnotation->name);
            if(in_array($name, $nlist)==false&&$name!==null){
                if($type=="smallint"||$type=="integer"){
                    $fields[]="$name:integer";
                }
                if($type=="bigint"){
                    $fields[]="$name:bigint";
                }
                if($type=="decimal"){
                    $fields[]="$name:decimal".($precision>0?"(".$precision.($scale>0?"|".$scale:"").")":"");
                }
                if($type=="float"){
                    $fields[]="$name:float".($precision>0?"(".$precision.($scale>0?"|".$scale:"").")":"");
                }
                if($type=="string"){
                    $fields[]="$name:string".($length>0?"(".$length.")":"");
                }
                if($type=="text"){
                    $fields[]="$name:text";
                }
                if($type=="boolean"){
                    $fields[]="$name:boolean";
                }
                if($type=="date"){
                    $fields[]="$name:dateonly";
                }
                if($type=="datetime"){
                    $fields[]="$name:date";
                }
                if($type=="json_array"){
                    $fields[]="$name:json";
                }
                //$output->writeln(sprintf('name: <comment>%s</comment> type: <comment>%s</comment> length: <comment>%s</comment>', $name,$type,$length));
            }
        }
        $em = $this->getContainer()->get('doctrine')->getManager();
        $table=$em->getClassMetadata($entity)->getTableName();
        $command='node_modules\.bin\sequelize model:create --name '.$table.' --attributes "'.implode ( ", " , $fields ).'" '.($force==1?"--force":"");
        $output->writeln(sprintf('Migrated entity: <comment>%s</comment> path: <comment>%s</comment>', $entity,$path));
        $output->writeln(sprintf('Command: <comment>%s</comment> ', $command));
        exec(substr($path, 0, 2)." && cd ".$path." && ".$command." && node_modules\.bin\sequelize db:migrate");
        //$output->writeln(sprintf('Debug: <comment>%s</comment> ', substr($path, 0, 2)." && cd ".$path." && ".$command." && node_modules\.bin\sequelize db:migrate"));
    }
    
    
}
