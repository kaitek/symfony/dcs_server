<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Kaitek\Bundle\FrameworkBundle\Entity\Menu;
#use App\Entity\BaseParam;
use App\Entity\ClientType;
use App\Entity\IOEvent;
use App\Entity\JobRotation;
use App\Entity\LostType;
use App\Entity\MaterialType;
use App\Entity\MaterialUnit;
use App\Entity\MouldType;
use App\Entity\MovementTaskType;
use App\Entity\TaskFinishType;

class dcsPostInstallCommand extends Command
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }
    protected function configure()
    {
        $this
            ->setName('dcs:post:install')
            ->setDescription('Ilk kurulum esnasinda tip tablolarinin kayitlarini olusturur.')
            ->setHelp(
                <<<EOT
<info>kai:post:install</info> komutu tip toblolari kayitlarini olusturur:

  <info>php bin/console dcs:post:install</info>

EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$ui = new SymfonyStyle($input, $output);
        $text = 'Running post install ... ';
        $output->writeln($text);
        //$command = $this->getApplication()->find('kai:post:install');
        //$arguments = array(
        //    //'--force' => true
        //    ''
        //);
        //$input = new ArrayInput($arguments);
        //$returnCode = $command->run($input, $output);
        //if($returnCode == 0) {
        //    $text = 'main menus successfully created ...';
        //    $output->writeln($text);
        //}
        //menü
        $menus = array(
            array("sira" => 1,"parentid" => 0,"menuid" => "parameters"
                ,"title" => "App.menu.parameters"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-cog","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "Client"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "Client"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 2,"parentid" => "","menuid" => "LostType"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "LostType"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 3,"parentid" => "","menuid" => "Employee"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "Employee"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 4,"parentid" => "","menuid" => "TeamLeader"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "TeamLeader"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 5,"parentid" => "","menuid" => "Mould"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "Mould"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 6,"parentid" => "","menuid" => "ClientMouldGroup"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "ClientMouldGroup"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 8,"parentid" => "","menuid" => "ProductTree"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 500,"modulename" => "ProductTree"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 9,"parentid" => "","menuid" => "Changeversion"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Changeversion"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 10,"parentid" => "","menuid" => "RejectType"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "RejectType"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 11,"parentid" => "","menuid" => "FaultType"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "FaultType"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 12,"parentid" => "","menuid" => "FaultInterventionType"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "FaultInterventionType"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 13,"parentid" => "","menuid" => "FaultIntervention"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 600,"modulename" => "FaultIntervention"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 14,"parentid" => "","menuid" => "TaskFinishType"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "TaskFinishType"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 15,"parentid" => "","menuid" => "ControlQuestion"
                    ,"title" => ""
                    ,"url" => "","width" => 1100,"height" => 400,"modulename" => "ControlQuestion"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                array("sira" => 16,"parentid" => "","menuid" => "TaskCaseControl"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "TaskCaseControl"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
                //array("sira"=>13,"parentid"=>"","menuid"=>"SourceLostTypeChange"
                //    ,"title"=>""
                //    ,"url"=>"","width"=>1000,"height"=>400,"modulename"=>"SourceLostTypeChange"
                //    ,"cls"=>"","iconcls"=>"","script"=>""
                //    ,"multiplewindow"=>1,"maximizable"=>1,"resizable"=>1
                //    ,"mobiledevice"=>1,"parent"=>"parameters"),
                //array("sira"=>14,"parentid"=>"","menuid"=>"SourceLostTypeChangeDone"
                //    ,"title"=>""
                //    ,"url"=>"","width"=>1000,"height"=>400,"modulename"=>"SourceLostTypeChangeDone"
                //    ,"cls"=>"","iconcls"=>"","script"=>""
                //    ,"multiplewindow"=>1,"maximizable"=>1,"resizable"=>1
                //    ,"mobiledevice"=>1,"parent"=>"parameters"),
                array("sira" => 17,"parentid" => "","menuid" => "ClientShadow"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "ClientShadow"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "parameters"),
            array("sira" => 2,"parentid" => 0,"menuid" => "plan"
                ,"title" => "App.menu.plan.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "Scheduler"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 600,"modulename" => "Scheduler"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 2,"parentid" => "","menuid" => "TaskList"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "TaskList"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 3,"parentid" => "","menuid" => "TaskListLaser"
                    ,"title" => ""
                    ,"url" => "","width" => 700,"height" => 400,"modulename" => "TaskListLaser"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 4,"parentid" => "","menuid" => "ReworkType"
                    ,"title" => ""
                    ,"url" => "","width" => 500,"height" => 400,"modulename" => "ReworkType"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 5,"parentid" => "","menuid" => "Rework"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "Rework"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 6,"parentid" => "","menuid" => "Offtime"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "Offtime"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 7,"parentid" => "","menuid" => "Overtime"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "Overtime"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 8,"parentid" => "","menuid" => "PlanNote"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "PlanNote"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 10,"parentid" => "","menuid" => "pl-sp1"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 11,"parentid" => "","menuid" => "R-PlanJobRotation"
                    ,"title" => ""
                    ,"url" => "","width" => 950,"height" => 450,"modulename" => "Reports-PlanJobRotation"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 12,"parentid" => "","menuid" => "R-PlanJobRotation2"
                    ,"title" => ""
                    ,"url" => "","width" => 950,"height" => 450,"modulename" => "Reports-PlanJobRotation2"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 12,"parentid" => "","menuid" => "R-TaskPlan-Employee"
                    ,"title" => "App.menu.reports_plan.taskplan-employee"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=taskplan-employee');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 13,"parentid" => "","menuid" => "R-TaskPlan-Reality"
                    ,"title" => "App.menu.reports_plan.taskplan-reality"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=taskplan-reality');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 14,"parentid" => "","menuid" => "R-TaskSession-Logs"
                    ,"title" => "App.menu.reports_plan.task_sessions_logs"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=task_sessions_logs');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 15,"parentid" => "","menuid" => "R-TaskPlan-Setup"
                    ,"title" => "App.menu.reports_plan.taskplan-setup"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/modules/watch/setup.php');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
                array("sira" => 20,"parentid" => "","menuid" => "pl-sp2"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "plan"),
                //array("sira"=>21,"parentid"=>"","menuid"=>"TaskExport"
                //    ,"title"=>""
                //    ,"url"=>"","width"=>400,"height"=>200,"modulename"=>"TaskExport"
                //    ,"cls"=>"","iconcls"=>"fa-calendar","script"=>""
                //    ,"multiplewindow"=>1,"maximizable"=>1,"resizable"=>1
                //    ,"mobiledevice"=>1,"parent"=>"plan"),
                array("sira" => 22,"parentid" => "","menuid" => "TaskPlan"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "TaskPlan"
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "plan"),
            array("sira" => 3,"parentid" => 0,"menuid" => "jobrotation"
                    ,"title" => "App.menu.jobrotation.main"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "JobRotation"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "JobRotation"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 2,"parentid" => "","menuid" => "JobRotationTeam"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "JobRotationTeam"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 3,"parentid" => "","menuid" => "JobRotationEmployee"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "JobRotationEmployee"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 4,"parentid" => "","menuid" => "JobRotationEmployeeChange"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "JobRotationEmployeeChange"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 5,"parentid" => "","menuid" => "JobRotationWorkPeople"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "JobRotationWorkPeople"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 30,"parentid" => "","menuid" => "EmployeeEfficiencyCalculate"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "EmployeeEfficiencyCalculate"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 31,"parentid" => "","menuid" => "ee-sp1"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 32,"parentid" => "","menuid" => "R-JobRotationEmployeeEfficiency"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobRotationEmployeeEfficiency"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 33,"parentid" => "","menuid" => "R-JobrotationLostDay"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobrotationLostDay"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 34,"parentid" => "","menuid" => "R-JobrotationLostWeek"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobrotationLostWeek"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 35,"parentid" => "","menuid" => "R-JobrotationLostMonth"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobrotationLostMonth"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 36,"parentid" => "","menuid" => "R-JobrotationLostYear"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobrotationLostYear"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 37,"parentid" => "","menuid" => "R-JobRotationEfficiencyWeekDetail"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobRotationEfficiencyWeekDetail"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 38,"parentid" => "","menuid" => "R-JobRotationEfficiencyWeek"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobRotationEfficiencyWeek"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 39,"parentid" => "","menuid" => "R-JobRotationEfficiencyMonth"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobRotationEfficiencyMonth"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 40,"parentid" => "","menuid" => "R-JobRotationProcessPerformanceWeek"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobRotationProcessPerformanceWeek"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 41,"parentid" => "","menuid" => "R-JobRotationProcessPerformanceMonth"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-JobRotationProcessPerformanceMonth"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 51,"parentid" => "","menuid" => "ee-sp2"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 52,"parentid" => "","menuid" => "R-TeamLeaderEfficiencyDay"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-TeamLeaderEfficiencyDay"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 53,"parentid" => "","menuid" => "R-TeamLeaderEfficiencyWeek"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-TeamLeaderEfficiencyWeek"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 54,"parentid" => "","menuid" => "R-TeamLeaderEfficiencyMonth"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-TeamLeaderEfficiencyMonth"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
                array("sira" => 55,"parentid" => "","menuid" => "R-TeamLeaderEfficiencyYear"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-TeamLeaderEfficiencyYear"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "jobrotation"),
            array("sira" => 4,"parentid" => 0,"menuid" => "editsystemdata"
                ,"title" => "App.menu.editsystemdata.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-calendar","script" => ""
                ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                ,"mobiledevice" => 1,"parent" => ""),
                //array("sira"=>1,"parentid"=>"","menuid"=>"LostTypeChange"
                //    ,"title"=>""
                //    ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"LostTypeChange"
                //    ,"cls"=>"","iconcls"=>"","script"=>""
                //    ,"multiplewindow"=>1,"maximizable"=>1,"resizable"=>1
                //    ,"mobiledevice"=>1,"parent"=>"editsystemdata"),
                array("sira" => 2,"parentid" => "","menuid" => "ClientLostDetail"
                    ,"title" => ""
                    ,"url" => "","width" => 1100,"height" => 400,"modulename" => "ClientLostDetail"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "editsystemdata"),
                array("sira" => 3,"parentid" => "","menuid" => "TaskRejectDetail"
                    ,"title" => ""
                    ,"url" => "","width" => 1100,"height" => 700,"modulename" => "TaskRejectDetail"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "editsystemdata"),
                array("sira" => 4,"parentid" => "","menuid" => "ClientProductionDetail"
                    ,"title" => ""
                    ,"url" => "","width" => 1100,"height" => 400,"modulename" => "ClientProductionDetail"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "editsystemdata"),
                array("sira" => 5,"parentid" => "","menuid" => "ClientProductionDetailSystemOut"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 400,"modulename" => "ClientProductionDetailSystemOut"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "editsystemdata"),
            array("sira" => 5,"parentid" => 0,"menuid" => "RemoteWatch"
                ,"title" => "App.menu.watchclient"
                ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-dashboard","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/modules/watch/index.php?nodeport=8805');"
                ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                ,"mobiledevice" => 1,"parent" => ""),
            array("sira" => 6,"parentid" => 0,"menuid" => "WatchEmployee"
                ,"title" => ""
                ,"url" => "","width" => 1100,"height" => 500,"modulename" => "WatchEmployee"
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                ,"mobiledevice" => 1,"parent" => ""),
            array("sira" => 7,"parentid" => 0,"menuid" => "CaseDeliver"
                ,"title" => ""
                ,"url" => "","width" => 900,"height" => 500,"modulename" => "CaseDeliver"
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                ,"mobiledevice" => 1,"parent" => ""),
            array("sira" => 8,"parentid" => 0,"menuid" => "quality"
                ,"title" => "App.menu.quality.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "QualityRejectionType"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "QualityRejectionType"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "quality"),
                array("sira" => 2,"parentid" => "","menuid" => "q-sp-1"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "quality"),
                array("sira" => 3,"parentid" => "","menuid" => "RemoteQualityConfirmation"
                    ,"title" => ""
                    ,"url" => "","width" => 1100,"height" => 500,"modulename" => "RemoteQualityConfirmation"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "quality"),
                array("sira" => 4,"parentid" => "","menuid" => "R-remote-quality-reject-list"
                    ,"title" => "App.menu.reports.remote-quality-reject-list"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=remote-quality-reject-list');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "quality"),
                array("sira" => 5,"parentid" => "","menuid" => "QualityCaseCall"
                    ,"title" => ""
                    ,"url" => "","width" => 700,"height" => 400,"modulename" => "QualityCaseCall"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "quality"),
            array("sira" => 9,"parentid" => 0,"menuid" => "kanban"
                ,"title" => "App.menu.kanban.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "KanbanOperation"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "KanbanOperation"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "kanban"),
                array("sira" => 2,"parentid" => "","menuid" => "KanbanWatch"
                    ,"title" => "App.menu.kanban.kanban-watch"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-dashboard","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/modules/kanban/index.php');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "kanban"),
                array("sira" => 3,"parentid" => "","menuid" => "k-sp-1"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "kanban"),
                array("sira" => 10,"parentid" => "","menuid" => "R-kanban-full"
                    ,"title" => "App.menu.kanban.kanban-full"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=kanban-full');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "kanban"),
                array("sira" => 11,"parentid" => "","menuid" => "R-kanban-over-log"
                    ,"title" => "App.menu.kanban.kanban-over-log"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=kanban-over-log');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "kanban"),
            array("sira" => 10,"parentid" => 0,"menuid" => "materialprepare"
                ,"title" => "App.menu.materialprepare.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "MH-System-Info"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "MaterialPrepare"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 2,"parentid" => "","menuid" => "MH-Label-Delete"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "CaseLabel"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 3,"parentid" => "","menuid" => "MH-Taskcase-Edit"
                    ,"title" => ""
                    ,"url" => "","width" => 1100,"height" => 400,"modulename" => "TaskCase"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 4,"parentid" => "","menuid" => "MH-Casemovement-Edit"
                    ,"title" => ""
                    ,"url" => "","width" => 1200,"height" => 400,"modulename" => "CaseMovement"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 5,"parentid" => "","menuid" => "MH-ProductionStorage"
                    ,"title" => ""
                    ,"url" => "","width" => 750,"height" => 400,"modulename" => "ProductionStorage"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 6,"parentid" => "","menuid" => "MH-WaitMaterial"
                    ,"title" => ""
                    ,"url" => "","width" => 850,"height" => 400,"modulename" => "WaitMaterial"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 7,"parentid" => "","menuid" => "MH-CaseLocation"
                    ,"title" => ""
                    ,"url" => "","width" => 850,"height" => 400,"modulename" => "CaseLocation"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 9,"parentid" => "","menuid" => "mh-sp-1"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 10,"parentid" => "","menuid" => "R-mh-gec-tasima"
                    ,"title" => "App.menu.materialprepare.mh-gec-tasima"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-gec-tasima');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 11,"parentid" => "","menuid" => "R-mh-gec-tasima-client"
                    ,"title" => "App.menu.materialprepare.mh-gec-tasima-client"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-gec-tasima-client');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 12,"parentid" => "","menuid" => "R-mh-performans"
                    ,"title" => "App.menu.materialprepare.mh-performans"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-performans');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 13,"parentid" => "","menuid" => "R-mh-client-task-cases"
                    ,"title" => "App.menu.materialprepare.mh-client-task-cases"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-client-task-cases');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 14,"parentid" => "","menuid" => "R-mh-carrier-time"
                    ,"title" => "App.menu.materialprepare.mh-carrier-time"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-carrier-time');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 15,"parentid" => "","menuid" => "R-mh-employee-time"
                    ,"title" => "App.menu.materialprepare.mh-employee-time"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-employee-time');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 16,"parentid" => "","menuid" => "R-mh-silinen-etiketler"
                    ,"title" => "App.menu.materialprepare.mh-silinen-etiketler"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-silinen-etiketler');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
                array("sira" => 17,"parentid" => "","menuid" => "R-mh-hata-listesi"
                    ,"title" => "App.menu.materialprepare.mh-hata-listesi"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=mh-hata-listesi');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "materialprepare"),
            array("sira" => 11,"parentid" => 0,"menuid" => "operationauthorization"
                    ,"title" => "App.menu.operationauthorization.main"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "OA-System-Info"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "OperationAuthorization"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 2,"parentid" => "","menuid" => "OperationAuthorizationQuestion"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "OperationAuthorizationQuestion"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 3,"parentid" => "","menuid" => "OperationAuthorizationEmployee"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "OperationAuthorizationEmployee"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 10,"parentid" => "","menuid" => "oa-sp1"
                    ,"title" => "|"
                    ,"url" => "","width" => 0,"height" => 0,"modulename" => ""
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 11,"parentid" => "","menuid" => "R-oa-list"
                    ,"title" => "App.menu.operationauthorization.oa-list"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=oa-list');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 12,"parentid" => "","menuid" => "R-oa-history"
                    ,"title" => "App.menu.operationauthorization.oa-history"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=oa-history');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 13,"parentid" => "","menuid" => "R-oa-log"
                    ,"title" => "App.menu.operationauthorization.oa-log"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=oa-log');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 14,"parentid" => "","menuid" => "R-oa-require-list"
                    ,"title" => "App.menu.operationauthorization.oa-require-list"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=oa-require-list');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
                array("sira" => 15,"parentid" => "","menuid" => "R-oa-forward-list"
                    ,"title" => "App.menu.operationauthorization.oa-forward-list"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=oa-forward-list');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "operationauthorization"),
            array("sira" => 12,"parentid" => 0,"menuid" => "documentmodule"
                ,"title" => "App.menu.document.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "Document"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "Document"
                    ,"cls" => "","iconcls" => "","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "documentmodule"),
            array("sira" => 100,"parentid" => 0,"menuid" => "reports_clients"
                ,"title" => "App.menu.reports_clients.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "R-Client-Signal"
                    ,"title" => "App.menu.reports_clients.client-production-signals"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=client-production-signals');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 1,"parentid" => "","menuid" => "R-Client-ProductionAnalyze"
                    ,"title" => "App.menu.reports_clients.client-product-analyze"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=client-product-analyze');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 2,"parentid" => "","menuid" => "R-ProductionDetail-Client"
                    ,"title" => "App.menu.reports_clients.productiondetail-client"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=productiondetail-client');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 3,"parentid" => "","menuid" => "R-ProductionDetail-Client-Total"
                    ,"title" => "App.menu.reports_clients.productiondetail-client-total"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=productiondetail-client-total');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 4,"parentid" => "","menuid" => "R-ProductionDetail-Total"
                    ,"title" => "App.menu.reports_clients.productiondetail-total"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=productiondetail-total');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 7,"parentid" => "","menuid" => "R-LostDetail-Client"
                    ,"title" => "App.menu.reports_clients.lostdetail-client"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=lostdetail-client');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 8,"parentid" => "","menuid" => "R-LostDetail-Client-Total"
                    ,"title" => "App.menu.reports_clients.lostdetail-client-total"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=lostdetail-client-total');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 9,"parentid" => "","menuid" => "R-LostDetail-Client-Group"
                    ,"title" => "App.menu.reports_clients.lostdetail-client-group"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=lostdetail-client-group');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 11,"parentid" => "","menuid" => "R-LostDetail-Client-Task"
                    ,"title" => "App.menu.reports_clients.lostdetail-client-task"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=lostdetail-client-task');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 12,"parentid" => "","menuid" => "R-TaskRejectDetail-Client"
                    ,"title" => "App.menu.reports_clients.task-reject-detail-client"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=task-reject-detail-client');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 13,"parentid" => "","menuid" => "R-Worktime-Client"
                    ,"title" => "App.menu.reports_clients.client-work-time"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=client-work-time');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 14,"parentid" => "","menuid" => "R-Worktime-Client-CNC"
                    ,"title" => "App.menu.reports_clients.cnc-work-time"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=cnc-work-time');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 15,"parentid" => "","menuid" => "R-Fulltime-Client-CNC"
                    ,"title" => "App.menu.reports_clients.cnc-full-time"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=cnc-full-time');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
                array("sira" => 16,"parentid" => "","menuid" => "R-Fulltimedetail-Client-CNC"
                    ,"title" => "App.menu.reports_clients.cnc-full-time-detail"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=cnc-full-time-detail');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_clients"),
            array("sira" => 110,"parentid" => 0,"menuid" => "reports_employees"
                ,"title" => "App.menu.reports_employees.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 5,"parentid" => "","menuid" => "R-ProductionDetail-Employee"
                    ,"title" => "App.menu.reports_employees.productiondetail-employee"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=productiondetail-employee');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 6,"parentid" => "","menuid" => "R-ProductionDetail-Employee-Total"
                    ,"title" => "App.menu.reports_employees.productiondetail-employee-total"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=productiondetail-employee-total');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 9,"parentid" => "","menuid" => "R-LostDetail-Employee"
                    ,"title" => "App.menu.reports_employees.lostdetail-employee"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=lostdetail-employee');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 10,"parentid" => "","menuid" => "R-LostDetail-Employee-Total"
                    ,"title" => "App.menu.reports_employees.lostdetail-employee-total"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=lostdetail-employee-total');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 12,"parentid" => "","menuid" => "R-LostDetail-Employee-Task"
                    ,"title" => "App.menu.reports_employees.lostdetail-employee-task"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=lostdetail-employee-task');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 13,"parentid" => "","menuid" => "R-TaskRejectDetail-Employee"
                    ,"title" => "App.menu.reports_employees.task-reject-detail-employee"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=task-reject-detail-employee');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 14,"parentid" => "","menuid" => "R-ETS-1"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-EmployeeTaskSummary"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 15,"parentid" => "","menuid" => "R-EmployeeLostDay"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-EmployeeLostDay"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 16,"parentid" => "","menuid" => "R-EmployeeLostWeek"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-EmployeeLostWeek"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 17,"parentid" => "","menuid" => "R-EmployeeLostMonth"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-EmployeeLostMonth"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 18,"parentid" => "","menuid" => "R-EmployeeLostYear"
                    ,"title" => ""
                    ,"url" => "","width" => 400,"height" => 200,"modulename" => "Reports-EmployeeLostYear"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
                array("sira" => 20,"parentid" => "","menuid" => "R-employee-efficiency-1"
                    ,"title" => "App.menu.reports_employees.employee-efficiency-1"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=employee-efficiency-1');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_employees"),
            array("sira" => 120,"parentid" => 0,"menuid" => "reports_analyze"
                ,"title" => "App.menu.reports_analyze.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "R-OEE-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "ReportOee"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 2,"parentid" => "","menuid" => "DashboardClient"
                    ,"title" => ""
                    ,"url" => "","width" => 1000,"height" => 600,"modulename" => "Reports-DashboardClient"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 3,"parentid" => "","menuid" => "R-AnalyzeLost-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeLost"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 4,"parentid" => "","menuid" => "R-AnalyzeLostOEE-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeLostOEE"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 5,"parentid" => "","menuid" => "R-AnalyzeLostClient-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeLostClient"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 6,"parentid" => "","menuid" => "R-AnalyzeLostOperation-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeLostOperation"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 7,"parentid" => "","menuid" => "R-AnalyzeLostOperator-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeLostOperator"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 8,"parentid" => "","menuid" => "R-AnalyzeShortLost-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeShortLost"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 9,"parentid" => "","menuid" => "R-AnalyzeShortLostOEE-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeShortLostOEE"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 10,"parentid" => "","menuid" => "R-AnalyzeSlowdown-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeSlowdown"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 11,"parentid" => "","menuid" => "R-AnalyzeSlowdownOperation-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeSlowdownOperation"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 12,"parentid" => "","menuid" => "R-AnalyzeOperationLost-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeOperationLost"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
                array("sira" => 13,"parentid" => "","menuid" => "R-AnalyzeOperationLostDetail-1"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-AnalyzeOperationLostDetail"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports_analyze"),
            array("sira" => 130,"parentid" => 0,"menuid" => "reports"
                ,"title" => "App.menu.reports.main"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "R-MonthProdAnalyze"
                    ,"title" => ""
                    ,"url" => "","width" => 600,"height" => 400,"modulename" => "Reports-MonthProdAnalyze"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 2,"parentid" => "","menuid" => "R-EmployeeProdAnalyze"
                    ,"title" => ""
                    ,"url" => "","width" => 500,"height" => 200,"modulename" => "Reports-EmployeeProdAnalyze"
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 3,"parentid" => "","menuid" => "R-Work-Out-System"
                    ,"title" => "App.menu.reports.work-out-system"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=work-out-system');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 4,"parentid" => "","menuid" => "R-Work-Out-System-Summary"
                    ,"title" => "App.menu.reports.work-out-system-summary"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=work-out-system-summary');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 5,"parentid" => "","menuid" => "R-pres-setup-detail"
                    ,"title" => "App.menu.reports.pres-setup-detail"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=pres-setup-detail');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 6,"parentid" => "","menuid" => "R-fault-list"
                    ,"title" => "App.menu.reports.fault-list"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=fault-list');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 7,"parentid" => "","menuid" => "R-control-answers"
                    ,"title" => "App.menu.reports.control-answers"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=control-answers');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 8,"parentid" => "","menuid" => "R-fault-sessions"
                    ,"title" => "App.menu.reports.fault-sessions"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=fault-sessions');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 9,"parentid" => "","menuid" => "R-TaskCases-Logs"
                    ,"title" => "App.menu.reports_plan.task_cases_logs"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=task_cases_logs');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 10,"parentid" => "","menuid" => "R-TaskCases-Logs-Delivery"
                    ,"title" => "App.menu.reports_plan.task_cases_logs_delivery"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=task_cases_logs_delivery');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 11,"parentid" => "","menuid" => "R-Reworks-working"
                    ,"title" => "App.menu.reports.reworks_working"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=reworks_working');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 12,"parentid" => "","menuid" => "R-Delivery-logs"
                    ,"title" => "App.menu.reports.delivery_logs"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=delivery_logs');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
                array("sira" => 13,"parentid" => "","menuid" => "R-Document-view-logs"
                    ,"title" => "App.menu.reports.document_view_logs"
                    ,"url" => "","width" => null,"height" => null,"modulename" => ""
                    ,"cls" => "","iconcls" => "fa-list-alt","script" => "window.open(window.location.origin+window.location.pathname.split(\"/\").splice(0,2).join(\"/\")+'/QB/index.php?report=document_view_logs');"
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "reports"),
            array("sira" => 900,"parentid" => 0,"menuid" => "systemparameters"
                ,"title" => "App.menu.systemparameters"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-cog","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => ""),
                array("sira" => 1,"parentid" => "","menuid" => "BaseParam"
                    ,"title" => ""
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "BaseParam"
                    ,"cls" => "","iconcls" => "fa-cog","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "systemparameters")
            ,array("sira" => 9998,"parentid" => 0,"menuid" => "adminMenu"
                ,"title" => "KaitekFrameworkBundle.menu.admin"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-cog","script" => ""
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => "")
                //,array("sira"=>1,"parentid"=>"","menuid"=>"Role"
                //    ,"title"=>""
                //    ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"Role"
                //    ,"cls"=>"","iconcls"=>"fa-lock","script"=>""
                //    ,"multiplewindow"=>1,"maximizable"=>1,"resizable"=>1
                //    ,"mobiledevice"=>1,"parent"=>"adminMenu")
                //,array("sira"=>2,"parentid"=>"","menuid"=>"menuManager"
                //    ,"title"=>""
                //    ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"Menu"
                //    ,"cls"=>"","iconcls"=>"fa-bars","script"=>""
                //    ,"multiplewindow"=>1,"maximizable"=>1,"resizable"=>1
                //    ,"mobiledevice"=>1,"parent"=>"adminMenu")
                ,array("sira" => 3,"parentid" => "","menuid" => "newUser"
                    ,"title" => "title"
                    ,"url" => "","width" => 900,"height" => 400,"modulename" => "User"
                    ,"cls" => "","iconcls" => "fa-users","script" => ""
                    ,"multiplewindow" => 1,"maximizable" => 1,"resizable" => 1
                    ,"mobiledevice" => 1,"parent" => "adminMenu")
                //,array("sira"=>4,"parentid"=>"","menuid"=>"onlineUser"
                //    ,"title"=>""
                //    ,"url"=>"","width"=>900,"height"=>400,"modulename"=>"OnlineUser"
                //    ,"cls"=>"","iconcls"=>"fa-user-plus","script"=>""
                //    ,"multiplewindow"=>1,"maximizable"=>1,"resizable"=>1
                //    ,"mobiledevice"=>1,"parent"=>"adminMenu")
            ,array("sira" => 9999,"parentid" => 0,"menuid" => "logout"
                ,"title" => "KaitekFrameworkBundle.general.logout"
                ,"url" => "","width" => null,"height" => null,"modulename" => ""
                ,"cls" => "","iconcls" => "fa-sign-out","script" => "app.logout();"
                ,"multiplewindow" => 0,"maximizable" => 0,"resizable" => 0
                ,"mobiledevice" => 1,"parent" => "")

        );
        $em = $this->container->get('doctrine')->getManager();
        $conn = $em->getConnection();

        $stmt = $conn->prepare("delete from _user_right where menu_id in (SELECT id from _menu where menuid in ('LostTypeChange','Role','menuManager','onlineUser','SourceLostTypeChange','SourceLostTypeChangeDone','p-sp1','TaskExport'))");
        $stmt->execute();
        $stmt = $conn->prepare("delete from _menu where menuid in ('LostTypeChange','Role','menuManager','onlineUser','SourceLostTypeChange','SourceLostTypeChangeDone','p-sp1','TaskExport')");
        $stmt->execute();

        $menurepository = $em->getRepository('KaitekFrameworkBundle:Menu');
        foreach ($menus as $item) {
            $menuId = $item["menuid"];
            $parentId = $item["parentid"];
            if ($item["parent"] != "") {
                $precord = $menurepository->loadMenuByMenuId($item["parent"]);
                if ($precord !== null) {
                    $parentId = $precord->getId();
                } else {
                    $parentId = 0;
                }
            }
            $record = $menurepository->loadMenuByMenuId($menuId);
            if ($record == null) {
                $menu = new Menu();
                $menu->setCreateuserId(0);
            } else {
                $menu = $record;
            }
            $menu->setSira($item["sira"]);
            $menu->setParentId($parentId);
            $menu->setMenuId($item["menuid"]);
            $menu->setTitle($item["title"]);
            $menu->setUrl($item["url"]);
            $menu->setWidth($item["width"]);
            $menu->setHeight($item["height"]);
            $menu->setModuleName($item["modulename"]);
            $menu->setCls($item["cls"]);
            $menu->setIconCls($item["iconcls"]);
            if (($menuId === 'RemoteWatch' && $record == null) || $menuId !== 'RemoteWatch') {
                $menu->setScript($item["script"]);
            }
            $menu->setMultipleWindow($item["multiplewindow"]);
            $menu->setMaximizable($item["maximizable"]);
            $menu->setResizable($item["resizable"]);
            $menu->setMobileDevice($item["mobiledevice"]);

            $conn->beginTransaction();
            try {
                $em->persist($menu);
                $em->flush();
                $conn->commit();
                if ($record == null) {
                    $output->writeln(sprintf('Olusturulan menu: <comment>%s</comment>', $menuId));
                } else {
                    $output->writeln(sprintf('Guncellenen menu: <comment>%s</comment>', $menuId));
                }
            } catch (Exception $e) {
                // Rollback the failed transaction attempt
                $conn->rollback();
                //throw $e;
                return $this->msgError($e->getMessage());
            }
        }
        $is_first_run = true;
        $text = 'client type start.';
        $output->writeln($text);
        //client type
        $clientTypes = array("Pres","Sabit Punta Tek Kafa","Sabit Punta Çift Kafa"
            ,"Askı Punta","Robot-Punta","Robot-Gazaltı","Plastik Enjeksiyon"
            ,"Manuel Gazaltı","Kaynak Makinesi","Poke Yoke Aparatı"
            ,"Pres Kalıbı","Punta Fikstürü","Kataforez"
            ,"Kaynak Fikstürü","Markalama Presi","Axe Montaj","İkaz Lambası"
            ,"Taşıma Robotu","Kreyn","Branda Kapı","Konveyör","Kalıp Sensörü"
        );
        $clientTypeRepository = $em->getRepository('App:ClientType');
        foreach ($clientTypes as $item) {
            #$output->writeln("ClientType-".$item);
            $clientTypeIdrecord = $clientTypeRepository->findOneBy(array('code' => $item));
            if ($clientTypeIdrecord == null) {
                $ct = new ClientType();
                $ct->setCreateuserId(0);
                $ct->setCode($item);
                $ct->setDescription($item);
                $conn->beginTransaction();
                try {
                    $em->persist($ct);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            } else {
                $is_first_run = false;
            }
        }
        if ($is_first_run) {
            $text = 'job rotation start.';
            $output->writeln($text);
            $jobRotations = array(
                array("code" => "07:30-15:30","beginval" => "07:30","endval" => "15:29","start" => new \DateTime())
                ,array("code" => "15:30-23:30","beginval" => "15:30","endval" => "23:29","start" => new \DateTime())
                ,array("code" => "23:30-07:30","beginval" => "23:30","endval" => "07:29","start" => new \DateTime())
            );
            $jobRotationRepository = $em->getRepository('App:JobRotation');
            foreach ($jobRotations as $item) {
                //$output->writeln("lostType-".$item["code"]);
                $jobRotationrecord = $jobRotationRepository->findOneBy(array('code' => $item["code"]));
                if ($jobRotationrecord == null) {
                    $jr = new JobRotation();
                    $jr->setCreateuserId(0);
                    $jr->setCode($item["code"]);
                    $jr->setBeginval($item["beginval"]);
                    $jr->setEndval($item["endval"]);
                    $jr->setStart($item["start"]);
                    $conn->beginTransaction();
                    try {
                        $em->persist($jr);
                        $em->flush();
                        $conn->commit();
                    } catch (Exception $e) {
                        // Rollback the failed transaction attempt
                        $conn->rollback();
                        //throw $e;
                        return $this->msgError($e->getMessage());
                    }
                }
            }
        }

        $text = 'lost type start.';
        $output->writeln($text);
        //lost type
        $lostTypes = array(
            array("code" => "DİĞER","description" => "DİĞER","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "ÇAY MOLASI","description" => "ÇAY MOLASI","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "YEMEK MOLASI","description" => "YEMEK MOLASI","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "SETUP-AYAR","description" => "SETUP-AYAR","isemployeerequired" => false,"authorizeloststart" => "isexpert","authorizelostfinish" => "isexpert","time" => 0,"isoperationnumberrequired" => false,"start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "KALIP SÖKME","description" => "KALIP SÖKME","isemployeerequired" => false,"authorizeloststart" => "isexpert","authorizelostfinish" => "isexpert","time" => 0,"isoperationnumberrequired" => false,"start" => new \DateTime(),"islisted" => false,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "MALZEME BEKLEME","description" => "MALZEME BEKLEME","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "İHTİYAÇ MOLASI","description" => "İHTİYAÇ MOLASI","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "KASA DEĞİŞİMİ","description" => "KASA DEĞİŞİMİ","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "TEMİZLİK","description" => "TEMİZLİK","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "İŞ YOK","description" => "İŞ YOK","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "GÖREVDE KİMSE YOK","description" => "GÖREVDE KİMSE YOK","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "TATİL","description" => "TATİL","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "KASAYA PARÇA KOYMA","description" => "KASAYA PARÇA KOYMA","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "ÖNLEYİCİ BAKIM","description" => "ÖNLEYİCİ BAKIM","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => true,"start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "PLANLI BAKIM","description" => "PLANLI BAKIM","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => true,"start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "HURDA TEMİZLİĞİ","description" => "HURDA TEMİZLİĞİ","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "ARIZA KALIP-APARAT","description" => "ARIZA KALIP-APARAT","isemployeerequired" => false,"authorizeloststart" => "isexpert","authorizelostfinish" => "ismould","time" => 0,"isoperationnumberrequired" => true,"start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "ARIZA MAKİNE MEKANİK","description" => "ARIZA MAKİNE MEKANİK","isemployeerequired" => false,"authorizeloststart" => "isexpert","authorizelostfinish" => "ismaintenance","time" => 0,"isoperationnumberrequired" => true,"start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "ARIZA MAKİNE ELEKTRİK","description" => "ARIZA MAKİNE ELEKTRİK","isemployeerequired" => false,"authorizeloststart" => "isexpert","authorizelostfinish" => "ismaintenance","time" => 0,"isoperationnumberrequired" => true,"start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "ARIZA ÜRETİM","description" => "ARIZA ÜRETİM","isemployeerequired" => false,"authorizeloststart" => "isexpert","authorizelostfinish" => "isexpert","time" => 0,"isoperationnumberrequired" => true,"start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "EĞİTİM","description" => "EĞİTİM","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "TOPLANTI","description" => "TOPLANTI","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "KALİTE BEKLEME","description" => "KALİTE BEKLEME","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "KALİTE ONAY","description" => "KALİTE ONAY","isemployeerequired" => false,"authorizeloststart" => "isexpert","authorizelostfinish" => "isquality","time" => 0,"isoperationnumberrequired" => false,"start" => new \DateTime(),"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
            ,array("code" => "MAKİNE AYAR","description" => "MAKİNE AYAR","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => true,"isopenallports" => false)
            ,array("code" => "FORKLİFT BEKLEME","description" => "FORKLİFT BEKLEME","isemployeerequired" => false,"time" => 0,"isoperationnumberrequired" => false,"ioevent" => "output","start" => new \DateTime(),"closenoemployee" => true,"islisted" => true,"isexpertemployeerequired" => false,"isopenallports" => false)
        );
        $lostTypeRepository = $em->getRepository('App:LostType');
        foreach ($lostTypes as $item) {
            //$output->writeln("lostType-".$item["code"]);
            $lostTyperecord = $lostTypeRepository->findOneBy(array('code' => $item["code"]));
            if ($lostTyperecord == null) {
                $lt = new LostType();
                $lt->setCreateuserId(0);
                $lt->setCode($item["code"]);
                $lt->setDescription($item["description"]);
                $lt->setIsemployeerequired($item["isemployeerequired"]);
                $lt->setIsexpertemployeerequired($item["isexpertemployeerequired"]);
                $lt->setTime($item["time"]);
                $lt->setIsoperationnumberrequired($item["isoperationnumberrequired"]);
                $lt->setStart($item["start"]);
                if (isset($item["ioevent"])) {
                    $lt->setIOEvent($item["ioevent"]);
                }
                if (isset($item["closenoemployee"])) {
                    $lt->setClosenoemployee($item["closenoemployee"]);
                }
                if (isset($item["islisted"])) {
                    $lt->setIslisted($item["islisted"]);
                }
                $conn->beginTransaction();
                try {
                    $em->persist($lt);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            }
        }
        $text = 'ioevent start.';
        $output->writeln($text);
        //io event
        $ioevents = array(
            array("code" => "input","iotype" => "I")
            ,array("code" => "input_ready","iotype" => "I")
            ,array("code" => "input_error","iotype" => "I")
            ,array("code" => "input_nok","iotype" => "I")
            ,array("code" => "clear_ready","iotype" => "I")
            ,array("code" => "input_mandatory","iotype" => "I")
            ,array("code" => "input_calibration","iotype" => "I")
            ,array("code" => "input_close_pc","iotype" => "I")
            ,array("code" => "input_lost","iotype" => "I")
            ,array("code" => "output","iotype" => "O")
            ,array("code" => "p-output-set","iotype" => "O")
            ,array("code" => "p-output-1","iotype" => "O")
            ,array("code" => "p-output-2","iotype" => "O")
            ,array("code" => "p-output-3","iotype" => "O")
            ,array("code" => "p-output-4","iotype" => "O")
            ,array("code" => "light_red","iotype" => "O")
            ,array("code" => "light_green","iotype" => "O")
            ,array("code" => "light_yellow","iotype" => "O")
            ,array("code" => "light_blue","iotype" => "O")
            ,array("code" => "light_white","iotype" => "O")
            ,array("code" => "buzzer","iotype" => "O")
            ,array("code" => "output-sensor","iotype" => "O")
        );
        $IOEventRepository = $em->getRepository('App:IOEvent');
        foreach ($ioevents as $item) {
            #$output->writeln("io-event-".$item["code"]);
            $ioeventrecord = $IOEventRepository->findOneBy(array('code' => $item["code"]));
            if ($ioeventrecord == null) {
                $os = new IOEvent();
                $os->setCreateuserId(0);
                $os->setCode($item["code"]);
                $os->setIotype($item["iotype"]);
                $conn->beginTransaction();
                try {
                    $em->persist($os);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            }
        }
        $text = 'material type start.';
        $output->writeln($text);
        //material type
        $materialTypes = array(
            array("code" => "Mamül","value" => "M")
            //,array("code"=>"Yarı Mamül","value"=>"Y")
            ,array("code" => "Hammadde","value" => "H")
            ,array("code" => "Operasyon","value" => "O")
            ,array("code" => "Çıktı Kasa","value" => "K")
            //,array("code"=>"Set Arabası","value"=>"S")
        );
        $materialTypeRepository = $em->getRepository('App:MaterialType');
        foreach ($materialTypes as $item) {
            #$output->writeln("MaterialType-".$item["code"]);
            $materialTypeIdrecord = $materialTypeRepository->findOneBy(array('code' => $item["code"]));
            if ($materialTypeIdrecord == null) {
                $mt = new MaterialType();
                $mt->setCreateuserId(0);
                $mt->setCode($item["code"]);
                $mt->setValue($item["value"]);
                $conn->beginTransaction();
                try {
                    $em->persist($mt);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            }
        }
        $text = 'material unit start.';
        $output->writeln($text);
        //material unit
        $materialunits = array("Adet","Kilogram","Gram","Ton","Metre","Santimetre");
        $materialUnitRepository = $em->getRepository('App:MaterialUnit');
        foreach ($materialunits as $item) {
            $materialunitrecord = $materialUnitRepository->findOneBy(array('code' => $item));
            if ($materialunitrecord == null) {
                $mu = new MaterialUnit();
                $mu->setCreateuserId(0);
                $mu->setCode($item);
                $conn->beginTransaction();
                try {
                    $em->persist($mu);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            }
        }
        $text = 'mould type start.';
        $output->writeln($text);
        //mould type
        $mouldTypes = array("Pres Kalıbı","Kaynak Fikstürü","Punta Fikstürü"
            ,"Plastik Enjeksiyon Kalıbı"
        );
        $mouldTypeRepository = $em->getRepository('App:MouldType');
        foreach ($mouldTypes as $item) {
            #$output->writeln("MouldType-".$item);
            $mouldTypeIdrecord = $mouldTypeRepository->findOneBy(array('code' => $item));
            if ($mouldTypeIdrecord == null) {
                $mt = new MouldType();
                $mt->setCreateuserId(0);
                $mt->setCode($item);
                $conn->beginTransaction();
                try {
                    $em->persist($mt);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            }
        }
        $text = 'MovementTaskType type start.';
        $output->writeln($text);
        //MovementTaskType
        $tasimagorevtip = array(
            array("tasktype" => "GRV001","label" => "BOŞ KASA->ÜRETIM","description" => "Boş taşıyıcı sahasından üretime boş taşıyıcı taşınması")
            ,array("tasktype" => "GRV002","label" => "Y.AMBAR->ÜRETİM","description" => "Yarımamül Ambardan üretime alt bileşen malzeme taşınması")
            ,array("tasktype" => "GRV003","label" => "YMA-MHA Yarı Mamül","description" => "Yarımamül ambarından malzeme hazırlık alanına yarımamül taşınması*-hareketi yok")
            ,array("tasktype" => "GRV004","label" => "MHAZ->ÜRETIM","description" => "Malzeme hazırlık alanından üretime alt bileşen malzeme taşınması")
            ,array("tasktype" => "GRV005","label" => "ÜRETİM->MUAYENE","description" => "Üretimden muayene sahasına mamüllerin taşınması")
            ,array("tasktype" => "GRV006","label" => "MUAYENE->Y.AMBAR","description" => "Muayene sahasından yarımamül ambarına yarımamüllerin taşınması*")
            ,array("tasktype" => "GRV007","label" => "ÜRETİM->Y.AMBAR","description" => "Üretimden yarımamül ambarına yarımamüllerin taşınması")
            ,array("tasktype" => "GRV008","label" => "ÜRETİM->M.AMBAR","description" => "Üretimden mamül ambarına mamüllerin taşınması")
            ,array("tasktype" => "GRV009","label" => "ÜRETİM İÇİ","description" => "Üretim esnasında oluşan ara komplelerin bir tezgahtan diğerine taşınması")
            ,array("tasktype" => "GRV010","label" => "G.MUAYENE->Y.AMBAR","description" => "Giriş muayene sahasından yarımamül ambarına yarımamüllerin taşınması*-hareketi yok")
            ,array("tasktype" => "GRV011","label" => "URT-BTS Boş Kasa","description" => "Üretimden boş taşıyıcı sahasına boş hammadde kasası taşınması")
        );
        $movementTaskTypeRepository = $em->getRepository('App:MovementTaskType');
        foreach ($tasimagorevtip as $item) {
            #$output->writeln("MouldType-".$item);
            $itemrecord = $movementTaskTypeRepository->findOneBy(array('tasktype' => $item["tasktype"]));
            if ($itemrecord == null) {
                $mt = new MovementTaskType();
                $mt->setTasktype($item["tasktype"]);
                $mt->setDescription($item["description"]);
                $mt->setLabel($item["label"]);
                $conn->beginTransaction();
                try {
                    $em->persist($mt);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            }
        }

        $text = 'TaskFinishType start.';
        $output->writeln($text);
        //TaskFinishType
        $taskfinishtypes = array(
            array("code" => "ALT PARCA YOK","recreate" => "1","orderfield" => "1")
            ,array("code" => "KALITE SORUNU","recreate" => "1","orderfield" => "2")
            ,array("code" => "PLANLAMA TALEBI","recreate" => "1","orderfield" => "3")
            ,array("code" => "DIGER","recreate" => "1","orderfield" => "4")
            ,array("code" => "SAYAC SORUNU","recreate" => "0","orderfield" => "5")
        );
        $TaskFinishTypeRepository = $em->getRepository('App:TaskFinishType');
        foreach ($taskfinishtypes as $item) {
            #$output->writeln("io-event-".$item["code"]);
            $taskfinishtyperecord = $TaskFinishTypeRepository->findOneBy(array('code' => $item["code"]));
            if ($taskfinishtyperecord == null) {
                $tft = new TaskFinishType();
                $tft->setCreateuserId(0);
                $tft->setCode($item["code"]);
                $tft->setRecreate($item["recreate"]);
                $tft->setOrderfield($item["orderfield"]);
                $conn->beginTransaction();
                try {
                    $em->persist($tft);
                    $em->flush();
                    $conn->commit();
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $conn->rollback();
                    //throw $e;
                    return $this->msgError($e->getMessage());
                }
            }
        }

        $output->writeln(sprintf('Setup finished...'));
    }
}
