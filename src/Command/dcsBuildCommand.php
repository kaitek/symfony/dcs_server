<?php

namespace App\Command;

;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ContainerInterface;

class dcsBuildCommand extends Command
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {

        $this
            ->setName('dcs:build')
            ->setDescription('DCS environment build command.')
            ->setDefinition(array(
                new InputOption(
                    'dev',
                    'd',
                    InputOption::VALUE_NONE,
                    'Environment option.'
                ),
            ))
            ->setHelp(
                <<<EOT
<info>kai:build</info> komutu environment build yapar:

  for production
  <info>php bin/console kai:build</info>

  for dev
  <info>php bin/console kai:build --dev</info>
  <info>php bin/console kai:build -d</info>

EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filesystem = new Filesystem();
        //if ($filesystem->exists('./web/css')) {
        //    $filesystem->remove('./web/css');
        //}
        //if ($filesystem->exists('./web/js')) {
        //    $filesystem->remove('./web/js');
        //}
        $dev   = true === $input->getOption('dev');
        //chdir($this->container->getParameter('kernel.root_dir').'/../');
        chdir($this->container->getParameter('kernel.project_dir'));
        $output->writeln($this->container->getParameter('kernel.project_dir'));
        if($dev==1) {
            $out=shell_exec('php ./bin/console "--ansi" "fos:js-routing:dump" "--target=./src/Resources/assets/scripts/app_routes.js"');
            $output->writeln($out);
            $p=getcwd();
            copy($p."/src/Resources/assets/scripts/app_routes.js", $p."/public/assets/scripts/app_routes.js");
            //$out=shell_exec('npm run build:dev');
            //$output->writeln($out);

            $out=shell_exec('php ./bin/console "--ansi" "cache:clear"');
            $output->writeln($out);

            //$output->writeln(sprintf('<comment>%s</comment>', $out));

            //$out=shell_exec('php ./bin/console "--ansi" "cache:clear"');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
            //$out=shell_exec('php ./bin/console "--ansi" "fos:js-routing:dump" "--target=./src/Resources/assets/scripts/app_routes.js"');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
            //$out=shell_exec('gulp build --env development');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
            //$out=shell_exec('php ./bin/console "--ansi" "assetic:dump"');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
        } else {
            $out=shell_exec('php ./bin/console "--ansi" "--env=prod" "fos:js-routing:dump" "--target=./src/Resources/assets/scripts/app_routes.js"');
            $output->writeln($out);
            $p=getcwd();
            copy($p."/src/Resources/assets/scripts/app_routes.js", $p."/public/assets/scripts/app_routes.js");

            //$out=shell_exec('npm run build');
            //$output->writeln($out);

            $out=shell_exec('php ./bin/console "--ansi" "--env=prod" "cache:clear"');
            $output->writeln($out);

            //$out=shell_exec('php ./bin/console "--ansi" "cache:clear" "--env=prod" "--no-debug"');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
            //$out=shell_exec('php ./bin/console "--ansi" "fos:js-routing:dump" "-e=prod" "--target=./src/Resources/assets/scripts/app_routes.js"');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
            //$out=shell_exec('gulp build --env production');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
            //$out=shell_exec('php ./bin/console "--ansi" "assetic:dump" "--env=prod"');
            //$output->writeln(sprintf('<comment>%s</comment>', $out));
        }
        $output->writeln('Build finished...');
        return 0;
    }

}
