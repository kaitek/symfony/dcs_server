<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class dcsSetupCommand extends Command
{
     protected function configure()
    {

        $this
            ->setName('dcs:setup')
            ->setDescription('DCS setup command.')
            ->setHelp(<<<EOT
<info>dcs:setup</info> komutu dcs setup islemini yapar:

  <info>php bin/console dcs:setup</info>

EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $out=shell_exec('php ./bin/console "--ansi" "dcs:build"');
        $output->writeln(sprintf('<comment>%s</comment>', $out));
        $out=shell_exec('php ./bin/console "--ansi" "doctrine:schema:update" "-f"');
        $output->writeln(sprintf('<comment>%s</comment>', $out));
        $out=shell_exec('php ./bin/console "--ansi" "fos:user:create" "admin" "admin@example.com" "Admin_99" "--super-admin"');
        $output->writeln(sprintf('<comment>%s</comment>', $out));
        $out=shell_exec('php ./bin/console "--ansi" "dcs:post:install"');
        $output->writeln(sprintf('<comment>%s</comment>', $out));
        
        $output->writeln('Setup finished...');
    }

}
