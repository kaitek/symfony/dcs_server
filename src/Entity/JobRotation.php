<?php

namespace App\Entity;

use App\Model\JobRotation as MJobRotation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobRotation
 *
 * @ORM\Table(name="job_rotations",indexes={@ORM\Index(name="idx__job_rotations__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\JobRotationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class JobRotation extends MJobRotation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"07:30-15:30 şeklinde vardiya bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotation.code.maxMessage")
     * @Assert\NotBlank(message="JobRotation.code.not_blank")
     * @Assert\NotNull(message="JobRotation.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="beginval", type="string", length=8)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 8, maxMessage = "JobRotation.beginval.maxMessage")
     * @Assert\NotBlank(message="JobRotation.beginval.not_blank")
     * @Assert\NotNull(message="JobRotation.beginval.not_blank")
     */
    protected $beginval;

    /**
     * @var string
     *
     * @ORM\Column(name="endval", type="string", length=8)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 8, maxMessage = "JobRotation.endval.maxMessage")
     * @Assert\NotBlank(message="JobRotation.endval.not_blank")
     * @Assert\NotNull(message="JobRotation.endval.not_blank")
     */
    protected $endval;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="JobRotation.start.not_blank")
     * @Assert\NotNull(message="JobRotation.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

