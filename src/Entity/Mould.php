<?php

namespace App\Entity;

use App\Model\Mould as MMould;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Mould
 *
 * @ORM\Table(name="moulds",indexes={@ORM\Index(name="idx__moulds__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\MouldRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Mould extends MMould
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Mould.code.maxMessage")
     * @Assert\NotBlank(message="Mould.code.not_blank")
     * @Assert\NotNull(message="Mould.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "Mould.description.maxMessage")
     */
    protected $description;

    /**
     * @var int
     *
     * @ORM\Column(name="counter", type="integer", options={"comment":"kalıbın üretim sayacı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $counter=0;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldtype", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Mould.mouldtype.maxMessage")
     * @Assert\NotBlank(message="Mould.mouldtype.not_blank")
     * @Assert\NotNull(message="Mould.mouldtype.not_blank")
     */
    protected $mouldtype;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", length=255)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "Mould.serialnumber.maxMessage")
     * @Assert\NotBlank(message="Mould.serialnumber.not_blank")
     * @Assert\NotNull(message="Mould.serialnumber.not_blank")
     */
    protected $serialnumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="Mould.start.not_blank")
     * @Assert\NotNull(message="Mould.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="client1", type="string", length=50, nullable=true, options={"comment":"client.code ön tanımlı istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Mould.client.maxMessage")
     * @Assert\NotBlank(message="Mould.client.not_blank")
     * @Assert\NotNull(message="Mould.client.not_blank")
     */
    protected $client1;

    /**
     * @var string
     *
     * @ORM\Column(name="client2", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Mould.client.maxMessage")
     */
    protected $client2;

    /**
     * @var string
     *
     * @ORM\Column(name="client3", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Mould.client.maxMessage")
     */
    protected $client3;

    /**
     * @var string
     *
     * @ORM\Column(name="client4", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Mould.client.maxMessage")
     */
    protected $client4;

    /**
     * @var string
     *
     * @ORM\Column(name="client5", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Mould.client.maxMessage")
     */
    protected $client5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bantime", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $bantime;

    /**
     * @var bool
     *
     * @ORM\Column(name="issensormould", type="boolean",nullable=true)
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $issensormould=false;

}
