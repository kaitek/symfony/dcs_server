<?php

namespace App\Entity;

use App\Model\OperationAuthorizationHistory as MOperationAuthorizationHistory;
use Doctrine\ORM\Mapping as ORM;

/**
 * OperationAuthorizationHistory
 *
 * @ORM\Table(name="operation_authorization_history")
 * @ORM\Entity(repositoryClass="App\Repository\OperationAuthorizationHistoryRepository")
 */
class OperationAuthorizationHistory extends MOperationAuthorizationHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     */
    protected $opname;

    /**
     * @var int
     *
     * @ORM\Column(name="l1", type="smallint", options={"default" : 0})
     */
    protected $l1;

    /**
     * @var int
     *
     * @ORM\Column(name="l2", type="smallint", options={"default" : 0})
     */
    protected $l2;

    /**
     * @var int
     *
     * @ORM\Column(name="l3", type="smallint", options={"default" : 0})
     */
    protected $l3;

    /**
     * @var int
     *
     * @ORM\Column(name="l4", type="smallint", options={"default" : 0})
     */
    protected $l4;

    /**
     * @var int
     *
     * @ORM\Column(name="l5", type="smallint", options={"default" : 0})
     */
    protected $l5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="userid", type="string", length=255)
     */
    protected $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    protected $description;

}

