<?php

namespace App\Entity;

use App\Model\FaultType as MFaultType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FaultType
 *
 * @ORM\Table(name="fault_types",indexes={@ORM\Index(name="idx__fault_types__client_faultgroupcode", columns={"client","faultgroupcode"})})
 * @ORM\Entity(repositoryClass="App\Repository\FaultTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FaultType extends MFaultType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"dışa aktarımda kullanılacak olan arızakodu değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "FaultType.code.maxMessage")
     * @Assert\NotBlank(message="FaultType.code.not_blank")
     * @Assert\NotNull(message="FaultType.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true, options={"comment":"arıza kodu açıklaması"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "FaultType.description.maxMessage")
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, nullable=true, options={"comment":"client.code değeri veya null-bütün istasyonlar için geçerli kodlar için"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "FaultType.client.maxMessage")
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="faultgroupcode", type="string", length=50, options={"comment":"c_e:İstasyon Elektrik,c_m:İstasyon Mekanik,m:Kalıp-Fikstür-Aparat,p:Üretim"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "FaultType.faultgroupcode.maxMessage")
     * @Assert\NotBlank(message="FaultType.faultgroupcode.not_blank")
     * @Assert\NotNull(message="FaultType.faultgroupcode.not_blank")
     */
    protected $faultgroupcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", options={"comment":"kodun geçerlilik başlangıç zamanı"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="FaultType.start.not_blank")
     * @Assert\NotNull(message="FaultType.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true, options={"comment":"kodun geçerlilik bitiş zamanı"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

