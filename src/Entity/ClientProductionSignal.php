<?php

namespace App\Entity;

use App\Model\ClientProductionSignal as MClientProductionSignal;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientProductionSignal
 *
 * @ORM\Table(name="client_production_signals",indexes={@ORM\Index(name="idx__client_production_signals__type_day_jobrotation_client", columns={"type","day","jobrotation","client"}),@ORM\Index(name="idx__client_production_signals__type_day", columns={"type","day"}),@ORM\Index(name="idx__client_production_signals__employee_day", columns={"employee","day"}),@ORM\Index(name="idx__client_production_signals__employee_opname", columns={"employee","opname"}),@ORM\Index(name="idx__client_production_signals__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientProductionSignalRepository")
 */
class ClientProductionSignal extends MClientProductionSignal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"c_s:client_signal-c_p:client_production-e_p:employee_production"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code alanındaki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $jobrotation;

    /**
     * @var int
     *
     * @ORM\Column(name="port", type="integer", nullable=true, options={"comment":"sinyalin geldiği port numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     * @Assert\Length(max = 200)
     */
    protected $port;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", options={"comment":"üretim siyalinin zamanı"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50, nullable=true, options={"comment":"mould.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mould;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50, nullable=true, options={"comment":"mouldgroup.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60)
     */
    protected $opname;

    /**
     * @var int
     *
     * @ORM\Column(name="production", type="integer", options={"comment":"her gelen sinyalde çıkması gereken adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $production=0;

    /**
     * @var int
     *
     * @ORM\Column(name="productioncurrent", type="integer", options={"comment":"her gelen sinyalde çıkan adet, kalıp arıza yoksa production ile aynı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productioncurrent=0;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, nullable=true, options={"comment":"employee.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, nullable=true, options={"comment":"erp referans numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $erprefnumber;

    /**
     * @var int
     *
     * @ORM\Column(name="gap", type="integer", options={"comment":"aynı üretimdeki bir önceki üretim sinyali ile aradaki fark"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $gap=0;

    /**
     * @var decimal
     *
     * @ORM\Column(name="energy", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"harcanan enerji miktarı"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $energy;

    /**
     * @var decimal
     *
     * @ORM\Column(name="cycletime", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"üretim esnasındaki hesaplanan birim üretim süresi"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $cycletime;

    /**
     * @var bool
     *
     * @ORM\Column(name="isexported", type="boolean", nullable=true, options={"comment":"export edildi mi"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isexported=true;

    /**
     * @var int
     *
     * @ORM\Column(name="opsayi", type="integer", nullable=true, options={"comment":"operatör sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opsayi;

    /**
     * @var int
     *
     * @ORM\Column(name="refsayi", type="integer", nullable=true, options={"comment":"referans sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $refsayi;

    /**
     * @var int
     *
     * @ORM\Column(name="losttime", type="integer", nullable=true, options={"comment":"üretim oturumunda yaşanan toplam kayıp süre"})
     */
    protected $losttime=0;

    /**
     * @var int
     *
     * @ORM\Column(name="signalcount", type="integer", options={"comment":"sinyal sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $signalcount=0;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", nullable=true, length=255)
     */
    protected $serialnumber;
}
