<?php

namespace App\Entity;

use App\Model\Client as MClient;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Client
 *
 * @ORM\Table(name="clients",indexes={@ORM\Index(name="idx__clients__code", columns={"code"}),@ORM\Index(name="idx__clients__record_id", columns={"record_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client extends MClient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Client.code.maxMessage")
     * @Assert\NotBlank(message="Client.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "Client.description.maxMessage")
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $uuid;

    /**
     * @var bool
     *
     * @ORM\Column(name="isactive", type="boolean", options={"default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Client.isactive.not_null")
     */
    protected $isactive=false;

    /**
     * @var int
     *
     * @ORM\Column(name="ganttorder", type="integer", nullable=true)
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $ganttorder;

    /**
     * @var string
     *
     * @ORM\Column(name="clientweight", type="integer", nullable=true, options={"default" : 100})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      notInRangeMessage = "Client.clientweight.notInRangeMessage"
     * )
     */
    protected $clientweight=100;

    /**
     * @var bool
     *
     * @ORM\Column(name="connected", type="boolean", options={"default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     */
    protected $connected=false;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="statustime", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $statustime;

    /**
     * @ORM\Column(name="workflow", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Client.workflow.maxMessage")
     */
    protected $workflow;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="string", length=10, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "Client.info.maxMessage")
     */
    protected $info;

    /**
     * @var int
     *
     * @ORM\Column(name="watchorder", type="integer", nullable=true)
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $watchorder;

    /**
     * @var bool
     *
     * @ORM\Column(name="materialpreparation", type="boolean", options={"default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Client.materialpreparation.not_null")
     */
    protected $materialpreparation=false;

    /**
    * @var string
    *
    * @ORM\Column(name="ip", type="string", length=15, nullable=true)
    * @Assert\Type(
    *      type="string",
    *      message="The value {{ value }} is not a valid {{ type }}."
    * )
    * @Assert\Length(max = 15)
    */
    protected $ip;

    /**
     * @var int
     *
     * @ORM\Column(name="pincode", type="integer", nullable=true, options={"comment":"cihaz kilidi açmak için geçici pin değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $pincode;

    /**
    * @var string
    *
    * @ORM\Column(name="versionapp", type="string", length=15, options={"default" : "1.0.0"})
    * @Assert\Type(
    *      type="string",
    *      message="The value {{ value }} is not a valid {{ type }}."
    * )
    * @Assert\Length(max = 15)
    * @Assert\NotBlank()
    */
    protected $versionapp="1.0.0";

    /**
    * @var string
    *
    * @ORM\Column(name="versionshell", type="string", length=15, options={"default" : "1.0.0"})
    * @Assert\Type(
    *      type="string",
    *      message="The value {{ value }} is not a valid {{ type }}."
    * )
    * @Assert\Length(max = 15)
    * @Assert\NotBlank()
    */
    protected $versionshell="1.0.0";

    /**
    * @var string
    *
    * @ORM\Column(name="computername", type="string", length=50, options={"default" : ""})
    * @Assert\Type(
    *      type="string",
    *      message="The value {{ value }} is not a valid {{ type }}."
    * )
    * @Assert\Length(max = 50)
    * @Assert\NotBlank()
    */
    protected $computername="undefined";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastprodstarttime", type="datetime", nullable=true, options={"comment":"son üretime başlama zamanı, üretim sinayi veya durum değişiminde güncellenir. iş yok başlama, kayıp bitirme, yeni işe başlamada değer set edilir."})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $lastprodstarttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastprodtime", type="datetime", nullable=true, options={"comment":"son üretim sinyali zamanı, her üretim sinyalinde güncellenir"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $lastprodtime;

    /**
     * @var int
     *
     * @ORM\Column(name="inputready", type="integer", nullable=true, options={"comment":"üretim tetiği geldikten sonra bekleyeceği max süre"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $inputready;
}
