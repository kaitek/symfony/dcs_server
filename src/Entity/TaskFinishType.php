<?php

namespace App\Entity;

use App\Model\TaskFinishType as MTaskFinishType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskFinishType
 *
 * @ORM\Table(name="task_finish_types",indexes={@ORM\Index(name="idx__task_finish_types__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskFinishTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskFinishType extends MTaskFinishType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"iş bitirme sebebi kodu"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskFinishType.code.maxMessage")
     * @Assert\NotBlank(message="TaskFinishType.code.not_blank")
     * @Assert\NotNull(message="TaskFinishType.code.not_blank")
     */
    protected $code;

    /**
     * @var bool
     *
     * @ORM\Column(name="recreate", type="boolean", options={"default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="TaskFinishType.recreate.not_null")
     */
    protected $recreate=false;

    /**
     * @var int
     *
     * @ORM\Column(name="orderfield", type="integer")
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskFinishType.orderfield.not_blank")
     * @Assert\NotNull(message="TaskFinishType.orderfield.not_blank")
     */
    protected $orderfield;
}
