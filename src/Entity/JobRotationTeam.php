<?php

namespace App\Entity;

use App\Model\JobRotationTeam as MJobRotationTeam;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobRotationTeam.
 *
 * @ORM\Table(name="job_rotation_teams")
 * @ORM\Entity(repositoryClass="App\Repository\JobRotationTeamRepository")
 */
class JobRotationTeam extends MJobRotationTeam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotationTeam.code.maxMessage")
     * @Assert\NotBlank(message="JobRotationTeam.code.not_blank")
     * @Assert\NotNull(message="JobRotationTeam.code.not_blank")
     */
    protected $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="JobRotationTeam.start.not_blank")
     * @Assert\NotNull(message="JobRotationTeam.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}
