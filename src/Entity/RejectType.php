<?php

namespace App\Entity;

use App\Model\RejectType as MRejectType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RejectType
 *
 * @ORM\Table(name="reject_types")
 * @ORM\Entity(repositoryClass="App\Repository\RejectTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RejectType extends MRejectType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "RejectType.code.maxMessage")
     * @Assert\NotBlank(message="RejectType.code.not_blank")
     * @Assert\NotNull(message="RejectType.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="extcode", type="string", length=50, nullable=true, options={"comment":"dışarı aktarımda kullanıcak olan kod"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "RejectType.extcode.maxMessage")
     */
    protected $extcode;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "RejectType.description.maxMessage")
     */
    protected $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="islisted", type="boolean", options={"comment":"cihaz üzerinden kaybın seçilebilir olmasını belirtir","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="RejectType.islisted.not_null")
     */
    protected $islisted=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isoeeaffect", type="boolean", nullable=true, options={"comment":"tanımlı ıskarta tipinin oee etkileme durumunu belirtir","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isoeeaffect=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isproductionaffect", type="boolean", nullable=true, options={"comment":"tanımlı ıskarta tipinin üretim adedini etkileme durumunu belirtir","default" : 0})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isproductionaffect=false;

    /**
     * @var int
     *
     * @ORM\Column(name="listorder", type="integer", nullable=true,  options={"comment":"cihazdaki listelemenin sıralama değeri","default" : 0})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $listorder=0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="RejectType.start.not_blank")
     * @Assert\NotNull(message="RejectType.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;
}
