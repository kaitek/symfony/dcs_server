<?php

namespace App\Entity;

use App\Model\ClientGroup as MClientGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientGroup
 *
 * @ORM\Table(name="client_groups",indexes={@ORM\Index(name="idx__client_groups__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientGroupRepository")
 */
class ClientGroup extends MClientGroup
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"kaynak1-2 şeklinde ana atölye adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientGroup.code.maxMessage")
     * @Assert\NotBlank(message="ClientGroup.code.not_blank")
     * @Assert\NotNull(message="ClientGroup.code.not_blank")
     */
    protected $code;
    
    /**
     * @var string
     *
     * @ORM\Column(name="groupcode", type="string", length=50, options={"comment":"plan,izleme,android tv şeklinde alt grup adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientGroup.groupcode.maxMessage")
     * @Assert\NotBlank(message="ClientGroup.groupcode.not_blank")
     * @Assert\NotNull(message="ClientGroup.groupcode.not_blank")
     */
    protected $groupcode;  

}

