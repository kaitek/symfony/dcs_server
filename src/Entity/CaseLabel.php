<?php

// HasLifecycleCallbacks yeni sistemde aktif olacak
//

namespace App\Entity;

use App\Model\CaseLabel as MCaseLabel;
use Doctrine\ORM\Mapping as ORM;

/**
 * CaseLabel
 *
 * @ORM\Table(name="case_labels",indexes={@ORM\Index(name="idx__case_labels__erprefnumber", columns={"erprefnumber"})})
 * @ORM\Entity(repositoryClass="App\Repository\CaseLabelRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CaseLabel extends MCaseLabel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="canceldescription", type="string", length=255, nullable=true)
     */
    protected $canceldescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="canceltime", type="datetime", nullable=true)
     */
    protected $canceltime;

    /**
     * @var string
     *
     * @ORM\Column(name="cancelusername", type="string", length=100, nullable=true)
     */
    protected $cancelusername;

    /**
     * @var integer
     *
     * @ORM\Column(name="synced", type="integer")
     */
    protected $synced;
}
