<?php

namespace App\Entity;

use App\Model\FaultInterventionType as MFaultInterventionType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FaultInterventionType
 *
 * @ORM\Table(name="fault_intervention_types",indexes={@ORM\Index(name="idx__fault_intervention_types__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\FaultInterventionTypeRepository")
 */
class FaultInterventionType extends MFaultInterventionType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "FaultInterventionType.code.maxMessage")
     * @Assert\NotBlank(message="FaultInterventionType.code.not_blank")
     * @Assert\NotNull(message="FaultInterventionType.code.not_blank")
     */
    protected $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "FaultInterventionType.description.maxMessage")
     */
    protected $description;
}
