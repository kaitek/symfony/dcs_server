<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncMessage
 * 
 * @ORM\Table(name="__sync_messages",indexes={@ORM\Index(name="idx____sync_messages__priority", columns={"priority"}),@ORM\Index(name="idx____sync_messages__status", columns={"status"}),@ORM\Index(name="idx____sync_messages__time", columns={"time"})})
 * @ORM\Entity(repositoryClass="App\Repository\SyncMessageRepository")
 */
class SyncMessage
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  protected $id;

  /**
   * @var string
   *
   * @ORM\Column(name="queuename", type="string", length=50)
   */
  protected $queuename;

  /**
   * @var string
   *
   * @ORM\Column(name="messagetype", type="string", length=50)
   */
  protected $messagetype;

  /**
   * @var int
   *
   * @ORM\Column(name="priority", type="integer", options={"default" : 999})
   */
  protected $priority=999;

  /**
   * @var string
   *
   * @ORM\Column(name="status", type="string", length=50)
   */
  protected $status;

  /**
   * @var string
   *
   * @ORM\Column(name="time", type="string", length=50)
   */
  protected $time;

  /**
   * @var json
   *
   * @ORM\Column(name="data", type="json")
   */
  protected $data;

  /**
   * @var json
   *
   * @ORM\Column(name="info", type="json")
   */
  protected $info;//counter,recordCount,tableName
}

