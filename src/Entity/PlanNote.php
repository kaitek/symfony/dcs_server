<?php

namespace App\Entity;

use App\Model\PlanNote as MPlanNote;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PlanNote
 * 
 * @ORM\Table(name="plan_notes")
 * @ORM\Entity(repositoryClass="App\Repository\PlanNoteRepository")
 */
class PlanNote extends MPlanNote
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    protected $type;

    /**
     * @var int
     *
     * @ORM\Column(name="recordorder", type="integer", options={"comment":"sıralama","default" : 0})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $recordorder=0;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "PlanNote.description.maxMessage")
     * @Assert\NotBlank(message="PlanNote.description.not_blank")
     * @Assert\NotNull(message="PlanNote.description.not_blank")
     */
    protected $description;
}

