<?php

namespace App\Entity;

use App\Model\ClientDetail as MClientDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientDetail
 *
 * @ORM\Table(name="client_details",indexes={@ORM\Index(name="idx__client_details__client", columns={"client"}),@ORM\Index(name="idx__client_details__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientDetailRepository")
 */
class ClientDetail extends MClientDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientDetail.code.maxMessage")
     * @Assert\NotBlank(message="ClientDetail.code.not_blank")
     * @Assert\NotNull(message="ClientDetail.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ClientDetail.description.maxMessage")
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="clienttype", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientDetail.clienttype.maxMessage")
     * @Assert\NotBlank(message="ClientDetail.clienttype.not_blank")
     * @Assert\NotNull(message="ClientDetail.clienttype.not_blank")
     */
    protected $clienttype;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientDetail.client.maxMessage")
     * @Assert\NotBlank(message="ClientDetail.client.not_blank")
     * @Assert\NotNull(message="ClientDetail.client.not_blank")
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="iotype", type="string", length=1, nullable=true, options={"comment":"I-O"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 1, maxMessage = "ClientDetail.iotype.maxMessage")
     */
    protected $iotype;
    
    /**
     * @var int
     *
     * @ORM\Column(name="portnumber", type="integer", nullable=true, options={"comment":"port numarası"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $portnumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ioevent", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientDetail.ioevent.maxMessage")
     */
    protected $ioevent;

    /**
     * @var string
     *
     * @ORM\Column(name="triggertype", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientDetail.triggertype.maxMessage")
     */
    protected $triggertype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ClientDetail.start.not_blank")
     * @Assert\NotNull(message="ClientDetail.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

