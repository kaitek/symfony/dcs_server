<?php

namespace App\Entity;

use App\Model\ActivityControlQuestion as MActivityControlQuestion;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ActivityControlQuestion
 *
 * @ORM\Table(name="activity_control_questions",indexes={@ORM\Index(name="idx__activity_control_questions__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\ActivityControlQuestionRepository")
 */
class ActivityControlQuestion extends MActivityControlQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"sorunun sorulacağı yer, setup bitimi,kalite onay gibi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ActivityControlQuestion.type.maxMessage")
     * @Assert\NotBlank(message="ActivityControlQuestion.type.not_blank")
     * @Assert\NotNull(message="ActivityControlQuestion.type.not_blank")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, options={"comment":"operasyon adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "ActivityControlQuestion.opname.maxMessage")
     * @Assert\NotBlank(message="ActivityControlQuestion.opname.not_blank")
     * @Assert\NotNull(message="ActivityControlQuestion.opname.not_blank")
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="documentnumber", type="string", length=50, options={"comment":"erp tarafındaki evrak numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ActivityControlQuestion.documentnumber.maxMessage")
     * @Assert\NotBlank(message="ActivityControlQuestion.documentnumber.not_blank")
     * @Assert\NotNull(message="ActivityControlQuestion.documentnumber.not_blank")
     */
    protected $documentnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255, options={"comment":"soru metni"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ActivityControlQuestion.question.maxMessage")
     * @Assert\NotBlank(message="ActivityControlQuestion.question.not_blank")
     * @Assert\NotNull(message="ActivityControlQuestion.question.not_blank")
     */
    protected $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answertype", type="string", length=50, options={"comment":"cevap tipi, evet-hayır,sayı değeri,yazı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ActivityControlQuestion.answertype.maxMessage")
     * @Assert\NotBlank(message="ActivityControlQuestion.answertype.not_blank")
     * @Assert\NotNull(message="ActivityControlQuestion.answertype.not_blank")
     */
    protected $answertype;

    /**
     * @var string
     *
     * @ORM\Column(name="valuerequire", type="string", length=100, nullable=true, options={"comment":"beklenen değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ActivityControlQuestion.valuerequire.maxMessage")
     */
    protected $valuerequire;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemin", type="string", length=100, nullable=true, options={"comment":"kabul edilebilir alt sınır"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ActivityControlQuestion.valuemin.maxMessage")
     */
    protected $valuemin;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemax", type="string", length=100, nullable=true, options={"comment":"kabul edilebilir üst sınır"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ActivityControlQuestion.valuemax.maxMessage")
     */
    protected $valuemax;

    /**
     * @var string
     *
     * @ORM\Column(name="period", type="string", length=100, nullable=true, options={"comment":"kontrol periyodu, her üretim, hafta, ay"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ActivityControlQuestion.period.maxMessage")
     */
    protected $period;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ActivityControlQuestion.start.not_blank")
     * @Assert\NotNull(message="ActivityControlQuestion.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

