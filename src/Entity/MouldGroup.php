<?php

namespace App\Entity;

use App\Model\MouldGroup as MMouldGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MouldGroup
 *
 * @ORM\Table(name="mould_groups",indexes={@ORM\Index(name="idx__mould_groups__mould_code", columns={"mould","code"})})
 * @ORM\Entity(repositoryClass="App\Repository\MouldGroupRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MouldGroup extends MMouldGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldGroup.code.maxMessage")
     * @Assert\NotBlank(message="MouldGroup.code.not_blank")
     * @Assert\NotNull(message="MouldGroup.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldGroup.mould.maxMessage")
     * @Assert\NotBlank(message="MouldGroup.mould.not_blank")
     * @Assert\NotNull(message="MouldGroup.mould.not_blank")
     */
    protected $mould;

    /**
     * @var int
     *
     * @ORM\Column(name="productionmultiplier", type="integer", options={"comment":"erp birim üretim süresi değeri düşürmede kullanılan çarpan"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Range(
     *      min = 10,
     *      max = 100,
     *      notInRangeMessage = "MouldGroup.productionmultiplier.notInRangeMessage"
     * )
     * @Assert\NotBlank(message="MouldGroup.productionmultiplier.not_blank")
     * @Assert\NotNull(message="MouldGroup.productionmultiplier.not_blank")
     */
    protected $productionmultiplier=100;

    /**
     * @var decimal
     *
     * @ORM\Column(name="intervalmultiplier", type="decimal", precision=10, scale=4, options={"comment":"sistemin duruşa geçmesi için kullanılan çarpan"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * * @Assert\Range(
     *      min = 1,
     *      max = 100,
     *      notInRangeMessage = "MouldGroup.intervalmultiplier.notInRangeMessage"
     * )
     * @Assert\NotBlank(message="MouldGroup.intervalmultiplier.not_blank")
     * @Assert\NotNull(message="MouldGroup.intervalmultiplier.not_blank")
     */
    protected $intervalmultiplier=5;

    /**
     * @var int
     *
     * @ORM\Column(name="counter", type="integer", options={"comment":"kalıp grubu üretim sinyali sayacı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $counter=0;

    /**
     * @var int
     *
     * @ORM\Column(name="setup", type="integer", options={"comment":"ayar süresi sn değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="MouldGroup.setup.not_blank")
     * @Assert\NotNull(message="MouldGroup.setup.not_blank")
     */
    protected $setup=600;

    /**
     * @var decimal
     *
     * @ORM\Column(name="cycletime", type="decimal", precision=10, scale=4, options={"comment":"çevrim süresi sn değeri"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="MouldGroup.cycletime.not_blank")
     * @Assert\NotNull(message="MouldGroup.cycletime.not_blank")
     */
    protected $cycletime=0;

    /**
     * @var int
     *
     * @ORM\Column(name="lotcount", type="integer")
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="MouldGroup.lotcount.not_blank")
     * @Assert\NotNull(message="MouldGroup.lotcount.not_blank")
     */
    protected $lotcount=100;

    /**
     * @var string
     *
     * @ORM\Column(name="program", type="string", length=4, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 4, maxMessage = "MouldGroup.program.maxMessage")
     */
    protected $program;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="MouldGroup.start.not_blank")
     * @Assert\NotNull(message="MouldGroup.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}
