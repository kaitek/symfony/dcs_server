<?php

namespace App\Entity;

use App\Model\MaterialType as MMaterialType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MaterialType
 *
 * @ORM\Table(name="material_types")
 * @ORM\Entity(repositoryClass="App\Repository\MaterialTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MaterialType extends MMaterialType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"mamül,Yarı Mamül,Operasyon,hammadde, Çıktı Kasa, set arabası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MaterialType.code.maxMessage")
     * @Assert\NotBlank(message="MaterialType.code.not_blank")
     * @Assert\NotNull(message="MaterialType.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=5, options={"comment":"M,Y,O,H,K,S"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 5, maxMessage = "MaterialType.value.maxMessage")
     * @Assert\NotBlank(message="MaterialType.value.not_blank")
     * @Assert\NotNull(message="MaterialType.value.not_blank")
     */
    protected $value;

}

