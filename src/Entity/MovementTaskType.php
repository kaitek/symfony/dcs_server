<?php

namespace App\Entity;

use App\Model\MovementTaskType as MMovementTaskType;
use Doctrine\ORM\Mapping as ORM;

/**
 * MovementTaskType
 *
 * @ORM\Table(name="movement_task_types")
 * @ORM\Entity(repositoryClass="App\Repository\MovementTaskTypeRepository")
 */
class MovementTaskType extends MMovementTaskType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tasktype", type="string", length=10)
     */
    protected $tasktype;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50)
     */
    protected $label;

}

