<?php
/* tpp değeri import sonrası bu tabloya kayıt yazılırken o anki producttree
 * değeri alınarak doldurulacak
 *
 * finish zaman değeri tipi quantity olan işler için miktara ulaşıldığında
 * otomatik olarak atanacak. tipi time olan işlerde bir daha seçilememesi için
 * elle web arayüz üzerinden kapatılabilecek
 *
 */

namespace App\Entity;

use App\Model\TaskSession as MTaskSession;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskSession
 *
 * @ORM\Table(name="task_sessions",indexes={@ORM\Index(name="idx__task_sessions__erprefnumber", columns={"erprefnumber"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskSessionRepository")
 */
class TaskSession extends MTaskSession
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idx", type="string", length=60, nullable=true, options={"comment":"birleştirilmiş görevlerin tanımlanması için kullanılan değer"})
     */
    protected $idx;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=60, options={"comment":"client+opcode+opnumber+erprefnumber+time"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "TaskSession.code.maxMessage")
     * @ ORM\GeneratedValue(strategy="UUID") Burada generated value kullanılırsa ID alanına yazmaya çalışıyor
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, options={"comment":"mrp iş emri no kd-jobno vs"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskSession.erprefnumber.maxMessage")
     * @Assert\NotBlank(message="TaskSession.erprefnumber.not_blank")
     * @Assert\NotNull(message="TaskSession.erprefnumber.not_blank")
     */
    protected $erprefnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, options={"comment":"operasyon kodu, producttree.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskSession.opcode.maxMessage")
     * @Assert\NotBlank(message="TaskSession.opcode.not_blank")
     * @Assert\NotNull(message="TaskSession.opcode.not_blank")
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", options={"comment":"operasyon numarası, producttree.number"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskSession.opnumber.not_blank")
     * @Assert\NotNull(message="TaskSession.opnumber.not_blank")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "TaskSession.opname.maxMessage")
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true, options={"comment":"producttree.description alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "TaskSession.opdescription.maxMessage")
     */
    protected $opdescription;

    /**
     * @var int
     *
     * @ORM\Column(name="productcount", type="integer", options={"comment":"üretilecek adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskSession.productcount.not_blank")
     * @Assert\NotNull(message="TaskSession.productcount.not_blank")
     */
    protected $productcount;

    /**
     * @var int
     *
     * @ORM\Column(name="productdonecount", type="integer", options={"default" : 0,"comment":"toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdonecount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="productdoneactivity", type="integer", options={"default" : 0,"comment":"aktivitede toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdoneactivity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="scrapactivity", type="integer", options={"default" : 0,"comment":"aktivitedeki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrapactivity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="scrappart", type="integer", options={"default" : 0,"comment":"iş emrindeki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrappart = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", options={"comment":"01.01.2017 formatında gün değeri"})
     * @ Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskSession.deadline.not_blank")
     * @Assert\NotNull(message="TaskSession.deadline.not_blank")
     */
    protected $deadline;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=500, options={"comment":"client.code ön tanımlı istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 500, maxMessage = "TaskSession.client.maxMessage")
     * @Assert\NotBlank(message="TaskSession.client.not_blank")
     * @Assert\NotNull(message="TaskSession.client.not_blank")
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"time-quantity"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskSession.type.maxMessage")
     * @Assert\NotBlank(message="TaskSession.type.not_blank")
     * @Assert\NotNull(message="TaskSession.type.not_blank")
     */
    protected $type;

    /**
     * @var decimal
     *
     * @ORM\Column(name="tpp", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"birim üretim süresi sn değeri"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $tpp;

    /**
     * @var bool
     *
     * @ORM\Column(name="taskfromerp", type="boolean", nullable=true, options={"comment":"true ise sistemden gelmiş görev, false ise elle eklenmiş olan görev"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $taskfromerp = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="taskfinishtype", type="string", length=50, nullable=true, options={"comment":"cihazdan girilen görevin bitirilme tipi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskSession.taskfinishtype.maxMessage")
     */
    protected $taskfinishtype;

    /**
     * @var string
     *
     * @ORM\Column(name="finishdescription", type="string", length=255, nullable=true, options={"comment":"cihazdan girilen görevin bitirilme açıklaması"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "TaskSession.finishdescription.maxMessage")
     */
    protected $finishdescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plannedstart", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskSession.plannedstart.not_blank")
     * @Assert\NotNull(message="TaskSession.plannedstart.not_blank")
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plannedfinish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $plannedfinish;

    /**
     * @var string
     *
     * @ORM\Column(name="resourceid", type="string", length=50, nullable=true, options={"comment":"robot ise fikstür bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskSession.resourceid.maxMessage")
     */
    protected $resourceid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true, options={"comment":"durum değeri"})
     */
    protected $status;
}
