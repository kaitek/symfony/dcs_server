<?php

namespace App\Entity;

use App\Model\MovementTaskTypeCarrier as MMovementTaskTypeCarrier;
use Doctrine\ORM\Mapping as ORM;

/**
 * MovementTaskTypeCarrier
 *
 * @ORM\Table(name="movement_task_type_carriers")
 * @ORM\Entity(repositoryClass="App\Repository\MovementTaskTypeCarrierRepository")
 */
class MovementTaskTypeCarrier extends MMovementTaskTypeCarrier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tasktype", type="string", length=10)
     */
    protected $tasktype;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier", type="string", length=10)
     */
    protected $carrier;

}

