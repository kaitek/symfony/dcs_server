<?php

namespace App\Entity;

use App\Model\KanbanOverProduct as MKanbanOverProduct;
use Doctrine\ORM\Mapping as ORM;

/**
 * KanbanOverProduct
 *
 * @ORM\Table(name="kanban_over_products",indexes={@ORM\Index(name="idx__kanban_over_products__product", columns={"product"})})
 * @ORM\Entity(repositoryClass="App\Repository\KanbanOverProductRepository")
 */
class KanbanOverProduct extends MKanbanOverProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string", length=60)
     */
    protected $product;

    /**
     * @var int
     *
     * @ORM\Column(name="boxcount", type="integer")
     */
    protected $boxcount;

    /**
     * @var int
     *
     * @ORM\Column(name="currentboxcount", type="integer")
     */
    protected $currentboxcount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

}

