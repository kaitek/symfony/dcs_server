<?php

namespace App\Entity;

use App\Model\TeamLeader as MTeamLeader;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TeamLeader
 *
 * @ORM\Table(name="team_leaders")
 * @ORM\Entity(repositoryClass="App\Repository\TeamLeaderRepository")
 */
class TeamLeader extends MTeamLeader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TeamLeader.code.maxMessage")
     * @Assert\NotBlank(message="TeamLeader.code.not_blank")
     * @Assert\NotNull(message="TeamLeader.code.not_blank")
     */
    protected $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TeamLeader.start.not_blank")
     * @Assert\NotNull(message="TeamLeader.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}
