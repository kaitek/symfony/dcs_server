<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskPlan
 *
 * @ORM\Table(name="task_plans",indexes={@ORM\Index(name="idx__task_plans__client", columns={"client"}),@ORM\Index(name="idx__task_plans__erprefnumber", columns={"erprefnumber"}),@ORM\Index(name="idx__task_plans__opcode_opnumber", columns={"opcode","opnumber"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskPlanRepository")
 */
class TaskPlan 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idx", type="string", length=60, options={"comment":"birleştirilmiş görevlerin tanımlanması için kullanılan değer"})
     */
    protected $idx;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=60, options={"comment":"client+opcode+opnumber+erprefnumber+time"})
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code ön tanımlı istasyon bilgisi."})
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, options={"comment":"operasyon kodu, producttree.code"})
      */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", options={"comment":"operasyon numarası, producttree.number"})
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, options={"comment":"producttree.name alanı değeri"})
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, options={"comment":"mrp iş emri no kd-jobno vs"})
     */
    protected $erprefnumber;

    /**
     * @var int
     *
     * @ORM\Column(name="cyclecount", type="integer", options={"comment":"çevrim sayısı", "default":0})
     */
    protected $cyclecount;

    /**
     * @var int
     *
     * @ORM\Column(name="productcount", type="integer", options={"comment":"üretilecek adet"})
     */
    protected $productcount;

    /**
     * @var int
     *
     * @ORM\Column(name="productdonecount", type="integer", options={"comment":"üretilmiş adet", "default":0})
     */
    protected $productdonecount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", options={"comment":"01.01.2017 formatında gün değeri"})
     */
    protected $deadline;

    /**
     * @var int
     *
     * @ORM\Column(name="setup", type="integer", nullable=true, options={"comment":"setup süresi saniye", "default":0})
     */
    protected $setup;

    /**
     * @var decimal
     *
     * @ORM\Column(name="tpp", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"birim üretim süresi sn değeri"})
     */
    protected $tpp;

    /**
     * @var string
     *
     * @ORM\Column(name="clientmouldgroup", type="string", length=50, nullable=true, options={"comment":"clientmouldgroup"})
     */
    protected $clientmouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50, nullable=true, options={"comment":"mould.code değeri"})
     */
    protected $mould;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50, nullable=true, options={"comment":"mouldgroup.code değeri"})
     */
    protected $mouldgroup;

    /**
     * @var int
     *
     * @ORM\Column(name="operatorcount", type="integer", nullable=true)
     */
    protected $operatorcount=1;

    /**
     * @var bool
     *
     * @ORM\Column(name="isdefault", type="boolean", nullable=true, options={"comment":"seçili ise ön tanımlı üretim varyasyonu olduğunu belirtir"})
     */
    protected $isdefault=false;

    /**
     * @var int
     *
     * @ORM\Column(name="mask", type="integer", nullable=true, options={"comment":"üretim maskesi"})
     */
    protected $mask;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plannedstart", type="datetime", nullable=true)
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="plannedfinish", type="datetime", nullable=true)
     */
    protected $plannedfinish;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=true, options={"default" : 0,"comment":"birim süre (sn)*miktar"})
     */
    protected $duration;

    /**
     * @var int
     *
     * @ORM\Column(name="offtime", type="integer", nullable=true, options={"default" : 0,"comment":"işin içindeki mola süresi"})
     */
    protected $offtime;

    /**
     * @var int
     *
     * @ORM\Column(name="totaltime", type="integer", nullable=true, options={"default" : 0,"comment":"(birim süre*miktar)+mola süresi"})
     */
    protected $totaltime;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true, options={"comment":"durum değeri"})
     */
    protected $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="issendbyforce", type="boolean", nullable=true, options={"comment":"birleşmeyip gönderilmesi istenen işler için eklendi"})
     */
    protected $issendbyforce=false;

}

