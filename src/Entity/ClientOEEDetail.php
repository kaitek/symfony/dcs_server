<?php

namespace App\Entity;

use App\Model\ClientOEEDetail as MClientOEEDetail;
use Doctrine\ORM\Mapping as ORM;

/**
 * ClientOEEDetail
 *
 * @ORM\Table(name="client_oee_details",indexes={@ORM\Index(name="idx__client_oee_details__client_day_jobrotation", columns={"client","day","jobrotation"}),@ORM\Index(name="idx__client_oee_details__client", columns={"client"}),@ORM\Index(name="idx__client_oee_details__day_jobrotation", columns={"day","jobrotation"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientOEEDetailRepository")
 */
class ClientOEEDetail extends MClientOEEDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date")
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50)
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="kul", type="decimal", precision=10, scale=4)
     */
    protected $kul;

    /**
     * @var string
     *
     * @ORM\Column(name="per", type="decimal", precision=10, scale=4)
     */
    protected $per;

    /**
     * @var string
     *
     * @ORM\Column(name="kal", type="decimal", precision=10, scale=4)
     */
    protected $kal;

    /**
     * @var string
     *
     * @ORM\Column(name="oee", type="decimal", precision=10, scale=4)
     */
    protected $oee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     */
    protected $finish;

    /**
     * @var json
     *
     * @ORM\Column(name="c_k_s", type="json", nullable=true)
     */
    protected $c_k_s;

    /**
     * @var json
     *
     * @ORM\Column(name="c_s", type="json", nullable=true)
     */
    protected $c_s;

    /**
     * @var json
     *
     * @ORM\Column(name="c_n_s", type="json", nullable=true)
     */
    protected $c_n_s;

    /**
     * @var json
     *
     * @ORM\Column(name="i_s", type="json", nullable=true)
     */
    protected $i_s;

    /**
     * @var json
     *
     * @ORM\Column(name="p_c_s", type="json", nullable=true)
     */
    protected $p_c_s;

    /**
     * @var json
     *
     * @ORM\Column(name="e_c_s", type="json", nullable=true)
     */
    protected $e_c_s;

    /**
     * @var json
     *
     * @ORM\Column(name="kayipsaat", type="json", nullable=true)
     */
    protected $kayipsaat;

    /**
     * @var json
     *
     * @ORM\Column(name="kayipyuzde", type="json", nullable=true)
     */
    protected $kayipyuzde;

    /**
     * @var json
     *
     * @ORM\Column(name="oeeyuzde", type="json", nullable=true)
     */
    protected $oeeyuzde;

    /**
     * @var json
     *
     * @ORM\Column(name="oeedeger", type="json", nullable=true)
     */
    protected $oeedeger;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated_at;

}
