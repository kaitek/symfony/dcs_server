<?php

namespace App\Entity;

use App\Model\JobRotationEmployee as MJobRotationEmployee;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobRotationEmployee
 *
 * @ORM\Table(name="job_rotation_employees",indexes={@ORM\Index(name="idx__job_rotation_employees__day_jobrotation", columns={"day","jobrotation"})})
 * @ORM\Entity(repositoryClass="App\Repository\JobRotationEmployeeRepository")
 */
class JobRotationEmployee extends MJobRotationEmployee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="JobRotationEmployee.day.not_blank")
     * @Assert\NotNull(message="JobRotationEmployee.day.not_blank")
     */
    protected $day;

    /**
     * @var int
     *
     * @ORM\Column(name="week", type="smallint", options={"comment":"hafta numarası"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="JobRotationEmployee.week.not_blank")
     * @Assert\NotNull(message="JobRotationEmployee.week.not_blank")
     */
    protected $week;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30-15:30 şeklinde vardiya bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotationEmployee.jobrotation.maxMessage")
     * @Assert\NotBlank(message="JobRotationEmployee.jobrotation.not_blank")
     * @Assert\NotNull(message="JobRotationEmployee.jobrotation.not_blank")
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotationteam", type="string", length=50, options={"comment":"vardiya ekip lideri-vardiya adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotationEmployee.jobrotationteam.maxMessage")
     * @Assert\NotBlank(message="JobRotationEmployee.jobrotationteam.not_blank")
     * @Assert\NotNull(message="JobRotationEmployee.jobrotationteam.not_blank")
     */
    protected $jobrotationteam;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, options={"comment":"personel"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotationEmployee.employee.maxMessage")
     * @Assert\NotBlank(message="JobRotationEmployee.employee.not_blank")
     * @Assert\NotNull(message="JobRotationEmployee.employee.not_blank")
     */
    protected $employee;

    /**
     * @var bool
     *
     * @ORM\Column(name="isovertime", type="boolean", options={"comment":"fazla mesai flag alanı"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="JobRotationEmployee.isovertime.not_blank")
     */
    protected $isovertime=false;

    /**
     * @var string
     *
     * @ORM\Column(name="beginval", type="string", length=8, options={"comment":"07:30:00 şeklinde zaman bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 8, maxMessage = "JobRotationEmployee.beginval.maxMessage")
     * @Assert\NotBlank(message="JobRotationEmployee.beginval.not_blank")
     * @Assert\NotNull(message="JobRotationEmployee.beginval.not_blank")
     */
    protected $beginval;

    /**
     * @var string
     *
     * @ORM\Column(name="endval", type="string", length=8, options={"comment":"15:29:59 şeklinde zaman bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 8, maxMessage = "JobRotationEmployee.endval.maxMessage")
     * @Assert\NotBlank(message="JobRotationEmployee.endval.not_blank")
     * @Assert\NotNull(message="JobRotationEmployee.endval.not_blank")
     */
    protected $endval;

    /**
     * @var int
     *
     * @ORM\Column(name="worktime", type="integer", options={"comment":"çalışma süresi dk cinsinden"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="JobRotationEmployee.worktime.not_blank")
     */
    protected $worktime;
    
    /**
     * @var int
     *
     * @ORM\Column(name="breaktime", type="integer", options={"comment":"mola süresi dk cinsinden hesap yapılırken buradakinden fazla mola kullanılması durumunda fazlalık duruş olarak değerlendirilecek"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="JobRotationEmployee.breaktime.not_blank")
     */
    protected $breaktime;

}

