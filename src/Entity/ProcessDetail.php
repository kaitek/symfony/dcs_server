<?php

namespace App\Entity;

use App\Model\ProcessDetail as MProcessDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProcessDetail
 *
 * @ORM\Table(name="process_details",indexes={@ORM\Index(name="idx__process_details__type_day_jobrotation", columns={"type","day","jobrotation"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProcessDetailProcessRepository")
 */
class ProcessDetail extends MProcessDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"w_c_s, w_c_p, w_e_p, l_c, l_c_t, l_e, l_e_t"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code alanındaki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $client;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tasklist", type="string", length=200, nullable=true, options={"comment":"yapılacak görevin tasklist.code değeri(unique)"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 200)
     */
    protected $tasklist;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50, nullable=true, options={"comment":"mould.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mould;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50, nullable=true, options={"comment":"mouldgroup.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, nullable=true, options={"comment":"producttree.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", nullable=true, options={"comment":"producttree.number alanı değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true, options={"comment":"producttree.description alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $opdescription;
    
    /**
     * @var string
     *
     * @ORM\Column(name="leafmask", type="string", length=50, nullable=true, options={"comment":"moulddetail.leafmask veya 1"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $leafmask='01';

    /**
     * @var string
     *
     * @ORM\Column(name="leafmaskcurrent", type="string", length=50, nullable=true, options={"comment":"moulddetail.leafmaskcurrent veya 1"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $leafmaskcurrent='01';

    /**
     * @var int
     *
     * @ORM\Column(name="production", type="integer", options={"comment":"her gelen sinyalde çıkması gereken adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $production=0;

    /**
     * @var int
     *
     * @ORM\Column(name="productioncurrent", type="integer", options={"comment":"her gelen sinyalde çıkan adet, kalıp arıza yoksa production ile aynı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productioncurrent=0;
    
    /**
     * @var int
     *
     * @ORM\Column(name="reject", type="integer", nullable=true, options={"comment":"ıskarta olarak ayrılan toplam adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $reject=0;
    
    /**
     * @var int
     *
     * @ORM\Column(name="retouch", type="integer", nullable=true, options={"comment":"rötuş ile yeniden kullanılabilir olan toplam adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $retouch=0;
    
    /**
     * @var int
     *
     * @ORM\Column(name="scrap", type="integer", nullable=true, options={"comment":"hurda olarak atılacak olan toplam adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $scrap=0;
    
    /**
     * @var int
     *
     * @ORM\Column(name="cycle", type="integer", options={"comment":"alınan üretim sinyali sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $cycle=0;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, nullable=true, options={"comment":"employee.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $employee;
    
    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, nullable=true, options={"comment":"erp referans numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $erprefnumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="losttype", type="string", length=50, nullable=true, options={"comment":"lost_types.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $losttype;
    
    /**
     * @var int
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"comment":"toplam süre sn,lost ise kayıp, work ise iş süresi toplamı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $time=0;

}

