<?php

namespace App\Entity;

use App\Model\OperationAuthorization as MOperationAuthorization;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OperationAuthorization
 *
 * @ORM\Table(name="operation_authorizations",indexes={@ORM\Index(name="idx__operation_authorizations__employee", columns={"employee"}),@ORM\Index(name="idx__operation_authorizations__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\OperationAuthorizationRepository")
 */
class OperationAuthorization extends MOperationAuthorization
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     */
    protected $opname;

    /**
     * @var int
     *
     * @ORM\Column(name="l1", type="smallint", options={"default" : 0})
     */
    protected $l1;

    /**
     * @var int
     *
     * @ORM\Column(name="l2", type="smallint", options={"default" : 0})
     */
    protected $l2;

    /**
     * @var int
     *
     * @ORM\Column(name="l3", type="smallint", options={"default" : 0})
     */
    protected $l3;

    /**
     * @var int
     *
     * @ORM\Column(name="l4", type="smallint", options={"default" : 0})
     */
    protected $l4;

    /**
     * @var int
     *
     * @ORM\Column(name="l5", type="smallint", options={"default" : 0})
     */
    protected $l5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

}

