<?php

namespace App\Entity;

use App\Model\TaskRejectDetail as MTaskRejectDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskRejectDetail
 *
 * @ORM\Table(name="task_reject_details",indexes={@ORM\Index(name="idx__task_reject_details__type_day_jobrotation", columns={"type","day","jobrotation"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskRejectDetailRepository")
 */
class TaskRejectDetail extends MTaskRejectDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"t_r:task_reject-e_r:employee_reject"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code alanındaki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="rejecttype", type="string", length=50, options={"comment":"reject_types.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $rejecttype;

    /**
     * @var string
     *
     * @ORM\Column(name="materialunit", type="string", length=50, options={"comment":"material_units.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $materialunit;

    /**
     * @var string
     *
     * @ORM\Column(name="tasklist", type="string", length=200, nullable=true, options={"comment":"yapılacak görevin tasklist.code değeri(unique)"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     * @Assert\Length(max = 200)
     */
    protected $tasklist;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", options={"comment":"ıskartanın zamanı"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50, nullable=true, options={"comment":"mould.code değeri-var ise"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mould;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50, nullable=true, options={"comment":"mouldgroup.code değeri-var ise"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, nullable=true, options={"comment":"moulddetail.opcode veya producttree.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", nullable=true, options={"comment":"moulddetail.opnumber veya producttree.number alanı değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"moulddetail.name veya producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true, options={"comment":"tasklist.opdescription alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $opdescription;

    /**
     * @var int
     *
     * @ORM\Column(name="quantityreject", type="integer", options={"comment":"ıskarta olarak ayrılan miktar"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $quantityreject=0;

    /**
     * @var int
     *
     * @ORM\Column(name="quantityretouch", type="integer", options={"comment":"yeniden işlem ile tekrar ürün haline gelen miktar"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $quantityretouch=0;

    /**
     * @var int
     *
     * @ORM\Column(name="quantityscrap", type="integer", options={"comment":"hurda miktarı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $quantityscrap=0;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, nullable=true, options={"comment":"employee.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, nullable=true, options={"comment":"erp referans numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $erprefnumber;

    /**
     * @var bool
     *
     * @ORM\Column(name="isexported", type="boolean", nullable=true, options={"comment":"export edildi mi"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isexported=true;

    /**
     * @var string
     *
     * @ORM\Column(name="importfilename", type="string", length=255, nullable=true, options={"comment":""})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $importfilename;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true, options={"comment":"elle girilmiş olan ıskarta sebebi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="ispreprocess", type="boolean", nullable=true)
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $ispreprocess=false;

    /**
     * @var json
     *
     * @ORM\Column(name="materials", type="json", nullable=true)
     */
    protected $materials;

    /**
     * @var string
     *
     * @ORM\Column(name="rejectlot", type="string", length=50, nullable=true, options={"comment":"lot numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $rejectlot;

}
