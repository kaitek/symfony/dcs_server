<?php

namespace App\Entity;

use App\Model\CaseLocation as MCaseLocation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CaseLocation
 *
 * @ORM\Table(name="case_locations",indexes={@ORM\Index(name="idx__case_locations__stockcode", columns={"stockcode"})})
 * @ORM\Entity(repositoryClass="App\Repository\CaseLocationRepository")
 */
class CaseLocation extends MCaseLocation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="stockcode", type="string", length=50, options={"comment":"stok kodu"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "CaseLocation.stockcode.maxMessage")
     * @Assert\NotBlank(message="CaseLocation.stockcode.not_blank")
     * @Assert\NotNull(message="CaseLocation.stockcode.not_blank")
     */
    protected $stockcode;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=50, options={"comment":"stok lokasyon bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "CaseLocation.location.maxMessage")
     * @Assert\NotBlank(message="CaseLocation.location.not_blank")
     * @Assert\NotNull(message="CaseLocation.location.not_blank")
     */
    protected $location;

}

