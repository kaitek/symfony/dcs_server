<?php

namespace App\Entity;

use App\Model\ProductionStorage as MProductionStorage;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProductionStorage
 *
 * @ORM\Table(name="production_storages",indexes={@ORM\Index(name="idx__production_storages__erprefnumber", columns={"erprefnumber"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProductionStorageRepository")
 */
class ProductionStorage extends MProductionStorage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="casemovementidin", type="integer")
     */
    protected $casemovementidIn;

    /**
     * @var int
     *
     * @ORM\Column(name="casemovementidout", type="integer", nullable=true)
     */
    protected $casemovementidOut;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     */
    protected $code;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(name="stockcode", type="string", length=50)
     */
    protected $stockcode;

    /**
     * @var string
     *
     * @ORM\Column(name="casename", type="string", length=50)
     */
    protected $casename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    protected $quantity;

}

