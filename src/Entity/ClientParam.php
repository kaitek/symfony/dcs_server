<?php

namespace App\Entity;

use App\Model\ClientParam as MClientParam;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientParam
 *
 * @ORM\Table(name="client_params",indexes={@ORM\Index(name="idx__client_params__client", columns={"client"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientParamRepository")
 */
class ClientParam extends MClientParam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientParam.client.maxMessage")
     * @Assert\NotBlank(message="ClientParam.client.not_blank")
     * @Assert\NotNull(message="ClientParam.client.not_blank")
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, options={"comment":"parametre adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientParam.name.maxMessage")
     * @Assert\NotBlank(message="ClientParam.name.not_blank")
     * @Assert\NotNull(message="ClientParam.name.not_blank")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=50, options={"comment":"parametre değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientParam.value.maxMessage")
     * @Assert\NotBlank(message="ClientParam.value.not_blank")
     * @Assert\NotNull(message="ClientParam.value.not_blank")
     */
    protected $value;

}

