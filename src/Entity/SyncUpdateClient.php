<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncUpdateClient
 * 
 * @ORM\Table(name="__sync_update_clients",indexes={@ORM\Index(name="idx____sync_update_clients__tablename__client", columns={"tablename","client"})})
 * @ORM\Entity(repositoryClass="App\Repository\SyncUpdateClientRepository")
 */
class SyncUpdateClient
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  protected $id;

  /**
   * @var string
   *
   * @ORM\Column(name="client", type="string", length=50)
   */
  protected $client;

  /**
   * @var string
   *
   * @ORM\Column(name="tablename", type="string", length=100)
   */
  protected $tablename;

  /**
   * @var string
   *
   * @ORM\Column(name="lastupdate", type="string", length=50)
   */
  protected $lastupdate;

}

