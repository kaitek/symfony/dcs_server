<?php
namespace App\Entity;

use App\Model\Penetration as MPenetration;
use Doctrine\ORM\Mapping as ORM;

/**
 * Penetration
 *
 * @ORM\Table(name="penetrations",indexes={@ORM\Index(name="idx__penetrations__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\PenetrationRepository")
 */
class Penetration extends MPenetration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, options={"comment":"producttree.name alanı değeri"})
     */
    protected $opname;
       
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreate", type="datetime")
     */
    protected $timecreate;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="timeview", type="datetime", nullable=true)
     */
    protected $timeview;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=true, options={"comment":"mesaj başlığı"})
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=true, options={"comment":"mesaj içeriği"})
     */
    protected $message;

}

