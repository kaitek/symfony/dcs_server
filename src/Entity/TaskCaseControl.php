<?php

namespace App\Entity;

use App\Model\TaskCaseControl as MTaskCaseControl;
use App\Repository\TaskCaseControlRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * TaskCaseControl
 *
 * @ORM\Table(name="task_case_controls",indexes={@ORM\Index(name="idx__task_case_controls__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskCaseControlRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskCaseControl extends MTaskCaseControl
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50)
     */
    protected $opcode;

    /**
     * @var decimal
     *
     * @ORM\Column(name="packcapacity", type="decimal", precision=20, scale=4)
     */
    protected $packcapacity;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantityremaining", type="decimal", precision=20, scale=4)
     */
    protected $quantityremaining;

    /**
     * @var string
     *
     * @ORM\Column(name="clients", type="string", length=255)
     */
    protected $clients;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    protected $filename;
}
