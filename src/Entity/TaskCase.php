<?php

namespace App\Entity;

use App\Model\TaskCase as MTaskCase;
use Doctrine\ORM\Mapping as ORM;

/**
 * TaskCase
 * @ORM\Table(name="task_cases",indexes={@ORM\Index(name="idx__task_cases__erprefnumber", columns={"erprefnumber"}),@ORM\Index(name="idx__task_cases__opname", columns={"opname"}),@ORM\Index(name="idx__task_cases__casenumber", columns={"casenumber"}),@ORM\Index(name="idx__task_cases__record_id", columns={"record_id"}),@ORM\Index(name="idx__task_cases__status", columns={"status"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskCaseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskCase extends MTaskCase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code alanındaki değer"})
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="movementdetail", type="integer", nullable=true)
     */
    protected $movementdetail;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="casetype", type="string", length=5, options={"comment":"I/O"})
     */
    protected $casetype;

    /**
     * @var string
     *
     * @ORM\Column(name="casenumber", type="string", length=50, nullable=true, options={"comment":"kasa numarası"})
     */
    protected $casenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="caselot", type="string", length=50, nullable=true, options={"comment":"kasa lot numarası"})
     */
    protected $caselot;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50)
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="stockcode", type="string", length=50, nullable=true, options={"comment":"producttree.stockcode||null"})
     */
    protected $stockcode;

    /**
     * @var string
     *
     * @ORM\Column(name="stockname", type="string", length=60, nullable=true, options={"comment":"producttree.stockname||null"})
     */
    protected $stockname;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantityused", type="decimal", precision=20, scale=4, nullable=true, options={"comment":"O:kasaya her sinyalde koyulacak,I:kasaden her sinyalde çıkarılacak adet"})
     */
    protected $quantityused=1;

    /**
     * @var string
     *
     * @ORM\Column(name="casename", type="string", length=50, options={"comment":"producttree.pack"})
     */
    protected $casename;

    /**
     * @var decimal
     *
     * @ORM\Column(name="packcapacity", type="decimal", precision=20, scale=4, nullable=true, options={"comment":"producttree.packcapacity"})
     */
    protected $packcapacity;

    /**
     * @var string
     *
     * @ORM\Column(name="feeder", type="string", length=5, nullable=true, options={"comment":"besleme görevini yapan"})
     */
    protected $feeder;

    /**
     * @var int
     *
     * @ORM\Column(name="preptime", type="integer", options={"comment":"hazırlık süresi"})
     */
    protected $preptime;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantityremaining", type="decimal", precision=20, scale=4, nullable=true, options={"comment":"I:kasa içinde kalan miktar,O:kasa içine konulan miktar"})
     */
    protected $quantityremaining;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantitydeliver", type="decimal", precision=20, scale=4, nullable=true, options={"comment":"tesellüm edildiği andaki kasa miktarı"})
     */
    protected $quantitydeliver;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     */
    protected $finish;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deliverytime", type="datetime", nullable=true)
     */
    protected $deliverytime;

    /**
     * @var string
     *
     * @ORM\Column(name="finishtype", type="string", length=255, nullable=true, options={"comment":"kapatılma şekli açıklaması"})
     */
    protected $finishtype;

    /**
     * @var int
     *
     * @ORM\Column(name="conveyor", type="integer")
     */
    protected $conveyor=0;

    /**
     * @var string
     *
     * @ORM\Column(name="goodsplanner", type="string", length=255, nullable=true, options={"comment":"malzeme temin sorumlusu"})
     */
    protected $goodsplanner;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true, options={"comment":"durum, çalışıyor, bekliyor, tesellüm edildi"})
     */
    protected $status;
}
