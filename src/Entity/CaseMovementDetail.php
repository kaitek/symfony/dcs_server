<?php

namespace App\Entity;

use App\Model\CaseMovementDetail as MCaseMovementDetail;
use Doctrine\ORM\Mapping as ORM;

/**
 * CaseMovementDetail
 *
 * @ORM\Table(name="case_movement_details",indexes={@ORM\Index(name="idx__case_movement_details__casemovementId", columns={"casemovementId"}),@ORM\Index(name="idx__case_movement_details__carrier", columns={"carrier"})})
 * @ORM\Entity(repositoryClass="App\Repository\CaseMovementDetailRepository")
 */
class CaseMovementDetail extends MCaseMovementDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="casemovementId", type="integer")
     */
    protected $casemovementId;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier", type="string", length=50)
     */
    protected $carrier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assignmenttime", type="datetime")
     */
    protected $assignmenttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="canceltime", type="datetime", nullable=true)
     */
    protected $canceltime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="donetime", type="datetime", nullable=true)
     */
    protected $donetime;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="empcode", type="string", length=50)
     */
    protected $empcode;

    /**
     * @var string
     *
     * @ORM\Column(name="empname", type="string", length=100)
     */
    protected $empname;

}

