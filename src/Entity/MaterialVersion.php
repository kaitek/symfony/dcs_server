<?php

namespace App\Entity;

use App\Model\MaterialVersion as MMaterialVersion;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MaterialVersion
 *
 * @ORM\Table(name="material_versions",indexes={@ORM\Index(name="idx__material_versions__mastername", columns={"mastername"})})
 * @ORM\Entity(repositoryClass="App\Repository\MaterialVersionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MaterialVersion extends MMaterialVersion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mastername", type="string", length=50, options={"comment":"16.14085-40"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MaterialVersion.mastername.maxMessage")
     * @Assert\NotBlank(message="MaterialVersion.mastername.not_blank")
     * @Assert\NotNull(message="MaterialVersion.mastername.not_blank")
     */
    protected $masterName;

    /**
     * @var string
     *
     * @ORM\Column(name="versionname", type="string", length=10, options={"comment":"AA"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 10, maxMessage = "MaterialVersion.versionname.maxMessage")
     * @Assert\NotBlank(message="MaterialVersion.versionname.not_blank")
     * @Assert\NotNull(message="MaterialVersion.versionname.not_blank")
     */
    protected $versionName;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=50, options={"comment":"16.14085AA-40"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MaterialVersion.fullname.maxMessage")
     * @Assert\NotBlank(message="MaterialVersion.fullname.not_blank")
     * @Assert\NotNull(message="MaterialVersion.fullname.not_blank")
     */
    protected $fullName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="MaterialVersion.start.not_blank")
     * @Assert\NotNull(message="MaterialVersion.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

