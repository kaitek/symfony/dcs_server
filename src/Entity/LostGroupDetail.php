<?php

namespace App\Entity;

use App\Model\LostGroupDetail as MLostGroupDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LostGroupDetail
 *
 * @ORM\Table(name="lost_group_details",indexes={@ORM\Index(name="idx__lost_group_details__lostgroup", columns={"lostgroup"}),@ORM\Index(name="idx__client_group_details__lostgroupcode", columns={"lostgroupcode"})})
 * @ORM\Entity(repositoryClass="App\Repository\LostGroupDetailRepository")
 */
class LostGroupDetail extends MLostGroupDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lostgroup", type="string", length=50, options={"comment":"lostgroup.code oee,süreç performans gibi ana grup kodu"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostGroupDetail.lostgroup.maxMessage")
     * @Assert\NotBlank(message="LostGroupDetail.lostgroup.not_blank")
     * @Assert\NotNull(message="LostGroupDetail.lostgroup.not_blank")
     */
    protected $lostgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="lostgroupcode", type="string", length=50, options={"comment":"lostgroup.groupcode oee-kullanılabilirlik,oee-planlı duruşlar gibi değerler"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostGroupDetail.lostgroupcode.maxMessage")
     * @Assert\NotBlank(message="LostGroupDetail.lostgroupcode.not_blank")
     * @Assert\NotNull(message="LostGroupDetail.lostgroupcode.not_blank")
     */
    protected $lostgroupcode;

    /**
     * @var string
     *
     * @ORM\Column(name="losttype", type="string", length=50, options={"comment":"losttype.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostGroupDetail.losttype.maxMessage")
     * @Assert\NotBlank(message="LostGroupDetail.losttype.not_blank")
     * @Assert\NotNull(message="LostGroupDetail.losttype.not_blank")
     */
    protected $losttype;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=10, nullable=true, options={"comment":"renk kodu"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "LostGroupDetail.color.maxMessage")
     */
    protected $color;

}

