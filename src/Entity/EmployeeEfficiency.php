<?php

namespace App\Entity;

use App\Model\EmployeeEfficiency as MEmployeeEfficiency;
use Doctrine\ORM\Mapping as ORM;

/**
 * EmployeeEfficiency
 *
 * @ORM\Table(name="employee_efficiency",indexes={@ORM\Index(name="idx__employee_efficiency__day", columns={"day"}),@ORM\Index(name="idx__employee_efficiency__week", columns={"week"})})
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeEfficiencyRepository")
 */
class EmployeeEfficiency extends MEmployeeEfficiency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date")
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50)
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="teamleader", type="string", length=50, nullable=true)
     */
    protected $teamleader;

    /**
     * @var int
     *
     * @ORM\Column(name="week", type="integer")
     */
    protected $week;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var bool
     *
     * @ORM\Column(name="isovertime", type="boolean")
     */
    protected $isovertime;

    /**
     * @var string
     *
     * @ORM\Column(name="fulltime", type="decimal", precision=10, scale=4)
     */
    protected $fulltime;

    /**
     * @var string
     *
     * @ORM\Column(name="breaktime", type="decimal", precision=10, scale=4)
     */
    protected $breaktime;

    /**
     * @var string
     *
     * @ORM\Column(name="worktime", type="decimal", precision=10, scale=4)
     */
    protected $worktime;

    /**
     * @var string
     *
     * @ORM\Column(name="optime", type="decimal", precision=10, scale=4)
     */
    protected $optime;

    /**
     * @var string
     *
     * @ORM\Column(name="discontinuity", type="decimal", precision=10, scale=4, nullable=true)
     */
    protected $discontinuity;

    /**
     * @var string
     *
     * @ORM\Column(name="losttime", type="decimal", precision=10, scale=4)
     */
    protected $losttime;

    /**
     * @var string
     *
     * @ORM\Column(name="reworkoptime", type="decimal", precision=10, scale=4)
     */
    protected $reworkoptime;

    /**
     * @var string
     *
     * @ORM\Column(name="mreworkduration", type="decimal", precision=10, scale=4)
     */
    protected $mreworkduration;

    /**
     * @var string
     *
     * @ORM\Column(name="mlosttime", type="decimal", precision=10, scale=4)
     */
    protected $mlosttime;

    /**
     * @var string
     *
     * @ORM\Column(name="rlosttime", type="decimal", precision=10, scale=4)
     */
    protected $rlosttime;

    /**
     * @var string
     *
     * @ORM\Column(name="rmlosttime", type="decimal", precision=10, scale=4)
     */
    protected $rmlosttime;

    /**
     * @var string
     *
     * @ORM\Column(name="worktimewithoutlost", type="decimal", precision=10, scale=4)
     */
    protected $worktimewithoutlost;

    /**
     * @var string
     *
     * @ORM\Column(name="efficiency", type="decimal", precision=10, scale=4)
     */
    protected $efficiency;

    /**
     * @var string
     *
     * @ORM\Column(name="tempo", type="decimal", precision=10, scale=4)
     */
    protected $tempo;

}

