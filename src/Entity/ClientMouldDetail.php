<?php

namespace App\Entity;

use App\Model\ClientMouldDetail as MClientMouldDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientMouldDetail
 *
 * @ORM\Table(name="client_mould_details",indexes={@ORM\Index(name="idx__client_mould_details__client_clientmouldgroup_mouldgroup", columns={"client","clientmouldgroup","mouldgroup"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientMouldDetailRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ClientMouldDetail extends MClientMouldDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientMouldDetail.client.maxMessage")
     * @Assert\NotBlank(message="ClientMouldDetail.client.not_blank")
     * @Assert\NotNull(message="ClientMouldDetail.client.not_blank")
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="clientmouldgroup", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientMouldDetail.clientmouldgroup.maxMessage")
     * @Assert\NotBlank(message="ClientMouldDetail.clientmouldgroup.not_blank")
     * @Assert\NotNull(message="ClientMouldDetail.clientmouldgroup.not_blank")
     */
    protected $clientmouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientMouldDetail.mouldgroup.maxMessage")
     * @Assert\NotBlank(message="ClientMouldDetail.mouldgroup.not_blank")
     * @Assert\NotNull(message="ClientMouldDetail.mouldgroup.not_blank")
     */
    protected $mouldgroup;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ClientMouldDetail.start.not_blank")
     * @Assert\NotNull(message="ClientMouldDetail.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

