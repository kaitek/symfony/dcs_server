<?php

namespace App\Entity;

use App\Model\TaskListLaserDetail as MTaskListLaserDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskListLaserDetail
 *
 * @ORM\Table(name="task_list_laser_details",indexes={@ORM\Index(name="idx__task_list_laser_details__tasklistlaser_opcode", columns={"tasklistlaser","opcode"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskListLaserDetailRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskListLaserDetail extends MTaskListLaserDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tasklistlaser", type="string", length=50, options={"comment":"TaskListLaser.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaserDetail.TaskListLaser.maxMessage")
     * @Assert\NotBlank(message="TaskListLaserDetail.TaskListLaser.not_blank")
     * @Assert\NotNull(message="TaskListLaserDetail.TaskListLaser.not_blank")
     */
    protected $tasklistlaser;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaserDetail.opcode.maxMessage")
     * @Assert\NotBlank(message="TaskListLaserDetail.opcode.not_blank")
     * @Assert\NotNull(message="TaskListLaserDetail.opcode.not_blank")
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer")
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskListLaserDetail.opnumber.not_blank")
     * @Assert\NotNull(message="TaskListLaserDetail.opnumber.not_blank")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "TaskListLaserDetail.opname.maxMessage")
     * @Assert\NotBlank(message="TaskListLaserDetail.opname.not_blank")
     * @Assert\NotNull(message="TaskListLaserDetail.opname.not_blank")
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, options={"comment":"mrp iş emri no kd-jobno vs"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaserDetail.erprefnumber.maxMessage")
     * @Assert\NotBlank(message="TaskListLaserDetail.erprefnumber.not_blank")
     * @Assert\NotNull(message="TaskListLaserDetail.erprefnumber.not_blank")
     */
    protected $erprefnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="leafmask", type="integer", length=50, options={"comment":"kalıbın çıktı sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskListLaserDetail.leafmask.not_blank")
     * @Assert\NotNull(message="TaskListLaserDetail.leafmask.not_blank")
     */
    protected $leafmask=0;

    /**
     * @var int
     *
     * @ORM\Column(name="productcount", type="integer", options={"comment":"üretilecek adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskListLaserDetail.productcount.not_blank")
     * @Assert\NotNull(message="TaskListLaserDetail.productcount.not_blank")
     */
    protected $productcount;

    /**
     * @var int
     *
     * @ORM\Column(name="productdonecount", type="integer", options={"default" : 0,"comment":"toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdonecount=0;

    /**
     * @var int
     *
     * @ORM\Column(name="productdoneactivity", type="integer", options={"default" : 0,"comment":"aktivitede toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdoneactivity=0;

    /**
     * @var int
     *
     * @ORM\Column(name="productdonejobrotation", type="integer", options={"default" : 0,"comment":"vardiyada toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdonejobrotation=0;


    /**
     * @var int
     *
     * @ORM\Column(name="scrapactivity", type="integer", options={"default" : 0,"comment":"aktivitedeki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrapactivity=0;

    /**
     * @var int
     *
     * @ORM\Column(name="scrapjobrotation", type="integer", options={"default" : 0,"comment":"vardiyadaki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrapjobrotation=0;

    /**
     * @var int
     *
     * @ORM\Column(name="scrappart", type="integer", options={"default" : 0,"comment":"iş emrindeki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrappart=0;

}

