<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskPlanEmployee
 *
 * @ORM\Table(name="task_plan_employees",indexes={@ORM\Index(name="idx__task_plan_employees__employee", columns={"employee"}),@ORM\Index(name="idx__task_plan_employees__client", columns={"client"}),@ORM\Index(name="idx__task_plan_employees__erprefnumber", columns={"erprefnumber"}),@ORM\Index(name="idx__task_plan_employees__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskPlanEmployeeRepository")
 */
class TaskPlanEmployee 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idx", type="string", length=60, options={"comment":"birleştirilmiş görevlerin tanımlanması için kullanılan değer"})
     */
    protected $idx;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, options={"comment":"employee.code"})
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code ön tanımlı istasyon bilgisi."})
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, options={"comment":"operasyon kodu, producttree.code"})
      */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", options={"comment":"operasyon numarası, producttree.number"})
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, options={"comment":"producttree.name alanı değeri"})
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, options={"comment":"mrp iş emri no kd-jobno vs"})
     */
    protected $erprefnumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plannedstart", type="datetime", nullable=true)
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="plannedfinish", type="datetime", nullable=true)
     */
    protected $plannedfinish;

}

