<?php

namespace App\Entity;

use App\Model\CaseChangeTime as MCaseChangeTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * CaseChangeTime
 * 
 * @ORM\Table(name="case_change_times",indexes={@ORM\Index(name="idx__casechangetimes__casename", columns={"casename"})})
 * @ORM\Entity(repositoryClass="App\Repository\CaseChangeTimeRepository")
 */
class CaseChangeTime extends MCaseChangeTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="casename", type="string", length=50)
     */
    protected $casename;

    /**
     * @var int
     *
     * @ORM\Column(name="changetime", type="integer", options={"default" : 0})
     */
    protected $changetime;
}

