<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncUpdate
 * 
 * @ORM\Table(name="__sync_updates",indexes={@ORM\Index(name="idx____sync_updates__tablename", columns={"tablename"})})
 * @ORM\Entity(repositoryClass="App\Repository\SyncUpdateRepository")
 */
class SyncUpdate
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  protected $id;

  /**
   * @var string
   *
   * @ORM\Column(name="tablename", type="string", length=100)
   */
  protected $tablename;

  /**
   * @var string
   *
   * @ORM\Column(name="lastupdate", type="string", length=50)
   */
  protected $lastupdate;

}

