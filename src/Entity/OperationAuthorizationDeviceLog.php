<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OperationAuthorizationDeviceLog
 *
 * @ORM\Table(name="operation_authorization_device_logs")
 * @ORM\Entity(repositoryClass="App\Repository\OperationAuthorizationDeviceLogRepository")
 */
class OperationAuthorizationDeviceLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=255)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="authemployee", type="string", length=50, nullable=true)
     */
    protected $authemployee;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    protected $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="ismailsend", type="boolean", options={"default" : false})
     */
    protected $ismailsend;
}
