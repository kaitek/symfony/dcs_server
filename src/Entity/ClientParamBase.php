<?php

namespace App\Entity;

use App\Model\ClientParamBase as MClientParamBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientParamBase
 *
 * @ORM\Table(name="client_param_bases",indexes={@ORM\Index(name="idx__client_param_bases__optiongroup", columns={"optiongroup"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientParamBaseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ClientParamBase extends MClientParamBase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="optiongroup", type="string", length=100, options={"comment":"Opsiyon grubu adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ClientParamBase.optiongroup.maxMessage")
     * @Assert\NotBlank(message="ClientParamBase.optiongroup.not_blank")
     * @Assert\NotNull(message="ClientParamBase.optiongroup.not_blank")
     */
    protected $optiongroup;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, unique=true, options={"comment":"verimotRT Client uygulaması içindeki değişken adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientParamBase.name.maxMessage")
     * @Assert\NotBlank(message="ClientParamBase.name.not_blank")
     * @Assert\NotNull(message="ClientParamBase.name.not_blank")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=100, options={"comment":"verimotRT Client uygulaması içindeki görünen ad"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ClientParamBase.label.maxMessage")
     * @Assert\NotBlank(message="ClientParamBase.label.not_blank")
     * @Assert\NotNull(message="ClientParamBase.label.not_blank")
     */
    protected $label;

    /**
     * @var string
     *
     * @ORM\Column(name="optiontype", type="string", length=50, options={"comment":"verimotRT Client uygulaması içinde ne şekilde render edileceği bilgisi input-check-radio"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientParamBase.optiontype.maxMessage")
     * @Assert\NotBlank(message="ClientParamBase.optiontype.not_blank")
     * @Assert\NotNull(message="ClientParamBase.optiontype.not_blank")
     */
    protected $optiontype;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text", options={"comment":"opsiyon değerlerini içeren text-json veri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ClientParamBase.options.not_blank")
     * @Assert\NotNull(message="ClientParamBase.options.not_blank")
     */
    protected $options;

}

