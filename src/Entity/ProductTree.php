<?php

namespace App\Entity;

use App\Model\ProductTree as MProductTree;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductTree
 *
 * @ORM\Table(name="product_trees",indexes={@ORM\Index(name="idx__product_trees__code", columns={"code"}),@ORM\Index(name="idx__product_trees__parent", columns={"parent"}),@ORM\Index(name="idx__product_trees__name", columns={"name"}) })
 * @ORM\Entity(repositoryClass="App\Repository\ProductTreeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductTree extends MProductTree
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="parent", type="string", length=50, nullable=true, options={"comment":"producttree.code||null parent operasyon kodu"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $parent=null;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"operasyon kodu, versiyon olmadan"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.code.maxMessage")
     * @Assert\NotBlank(message="ProductTree.code.not_blank")
     * @Assert\NotNull(message="ProductTree.code.not_blank")
     */
    protected $code;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer", nullable=true, options={"comment":"operasyon no"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ProductTree.description.maxMessage")
     * @Assert\NotNull(message="ProductTree.description.not_null")
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="materialtype", type="string", length=50, options={"comment":"tip, mamül,hammadde, operasyon vs"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.materialtype.maxMessage")
     * @Assert\NotBlank(message="ProductTree.materialtype.not_blank")
     * @Assert\NotNull(message="ProductTree.materialtype.not_blank")
     */
    protected $materialtype;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, options={"comment":"operasyon tam adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "ProductTree.name.maxMessage")
     * @Assert\NotBlank(message="ProductTree.name.not_blank")
     * @Assert\NotNull(message="ProductTree.name.not_blank")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="stockcode", type="string", length=50, nullable=true, options={"comment":"hammadde stok kodu veya operasyon çıktısı stok kodu 16.14085-40"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.stockcode.maxMessage")
     */
    protected $stockcode;

    /**
     * @var string
     *
     * @ORM\Column(name="stockname", type="string", length=60, nullable=true, options={"comment":"erp tarafında stok tutulmuyorsa çapraz değer 16.14085/0040"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "ProductTree.stockname.maxMessage")
     */
    protected $stockname;

    /**
     * @var decimal
     *
     * @ORM\Column(name="tpp", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"birim üretim süresi sn değeri"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $tpp;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantity", type="decimal", precision=20, scale=4, nullable=true, options={"comment":"operasyonda kullanılan miktar"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_quantity", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.unit_quantity.maxMessage")
     */
    protected $unit_quantity;

    /**
     * @var int
     *
     * @ORM\Column(name="opcounter", type="integer", nullable=true, options={"comment":"operasyon içindeki operasyon sayısı, x kadar punta"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opcounter;

    /**
     * @var string
     *
     * @ORM\Column(name="pack", type="string", length=50, nullable=true, options={"comment":"hammadde veya operasyon çıktısının içine konulacağı kasa"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.pack.maxMessage")
     */
    protected $pack;

    /**
     * @var decimal
     *
     * @ORM\Column(name="packcapacity", type="decimal", precision=20, scale=4, nullable=true, options={"comment":"hammadde besleme veya operasyon çıktısı kasa kapasitesi"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $packcapacity;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_packcapacity", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.unit_packcapacity.maxMessage")
     *
     */
    protected $unit_packcapacity;

    /**
     * @var int
     *
     * @ORM\Column(name="oporder", type="integer", nullable=true, options={"comment":"birden fazla operasyonu olan üretimde operasyon sırasının opno ile belirtilmiyor olması durumunda kullanılıyor"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $oporder=1;

    /**
     * @var int
     *
     * @ORM\Column(name="preptime", type="integer", nullable=true, options={"comment":"hazırlık süresi (dakika)"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $preptime;

    /**
     * @var bool
     *
     * @ORM\Column(name="qualitycontrolrequire", type="boolean", nullable=true, options={"comment":"1 ise operasyon çıktısının kalite kontrol birimine gitmesi gerekiyor, 0 ise doğrudan sevkiyat birimine gidecek"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $qualitycontrolrequire;

    /**
     * @var bool
     *
     * @ORM\Column(name="task", type="boolean", nullable=true, options={"comment":"hammadde besleme,boş kasa besleme görev oluşturma değeri"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $task;

    /**
     * @var string
     *
     * @ORM\Column(name="feeder", type="string", length=5, nullable=true, options={"comment":"M:mhaz-F:forklift-P:pimesco-K:konveyor"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 5, maxMessage = "ProductTree.feeder.maxMessage")
     */
    protected $feeder;

    /**
     * @var string
     *
     * @ORM\Column(name="carrierfeeder", type="string", length=255, nullable=true, options={"comment":"besleyen taşıyıcı cihazların kodları , ile ayrılmış şekilde"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ProductTree.carrierfeeder.maxMessage")
     */
    protected $carrierfeeder;

    /**
     * @var string
     *
     * @ORM\Column(name="drainer", type="string", length=5, nullable=true, options={"comment":"M:mhaz-F:forklift-P:pimesco-K:konveyor"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 5, maxMessage = "ProductTree.drainer.maxMessage")
     */
    protected $drainer;

    /**
     * @var string
     *
     * @ORM\Column(name="carrierdrainer", type="string", length=255, nullable=true, options={"comment":"boşaltan taşıyıcı cihazların kodları , ile ayrılmış şekilde"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ProductTree.carrierdrainer.maxMessage")
     */
    protected $carrierdrainer;

    /**
     * @var string
     *
     * @ORM\Column(name="goodsplanner", type="string", length=255, nullable=true, options={"comment":"hammadde malzemenin tedarik sorumlusu adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ProductTree.goodsplanner.maxMessage")
     */
    protected $goodsplanner;

    /**
     * @var int
     *
     * @ORM\Column(name="productionmultiplier", type="integer", nullable=true, options={"comment":"birim üretim süresi çarpanı. erp=10, 80*10/100=8sn"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Range(
     *      min = 10,
     *      max = 100,
     *      notInRangeMessage = "ProductTree.productionmultiplier.notInRangeMessage"
     * )
     */
    protected $productionmultiplier;

    /**
     * @var decimal
     *
     * @ORM\Column(name="intervalmultiplier", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"duruşa geçmek için beklenecek parça başı üretim süresi"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Range(
     *      min = 1,
     *      max = 100,
     *      notInRangeMessage = "ProductTree.intervalmultiplier.notInRangeMessage"
     * )
     */
    protected $intervalmultiplier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ProductTree.start.not_blank")
     * @Assert\NotNull(message="ProductTree.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var bool
     *
     * @ORM\Column(name="delivery", type="boolean", nullable=true, options={"comment":"true ise bu operasyon kaydı için tesellüm yapılacak demektir"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $delivery;

    /**
     * @var string
     *
     * @ORM\Column(name="locationsource", type="string", length=50, nullable=true, options={"comment":"malzemenin getirileceği lokasyon"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.locationsource.maxMessage")
     */
    protected $locationsource;

    /**
     * @var string
     *
     * @ORM\Column(name="locationdestination", type="string", length=50, nullable=true, options={"comment":"malzemenin götürüleceği lokasyon"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.locationdestination.maxMessage")
     */
    protected $locationdestination;

    /**
     * @var int
     *
     * @ORM\Column(name="empcount", type="integer", nullable=true, options={"comment":"operasyonu yapacak operatör sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $empcount;

    /**
     * @var bool
     *
     * @ORM\Column(name="isdefault", type="boolean", nullable=true, options={"comment":"hammaddeler veya operasyonlar için alternatif tanımında kullanılıyor"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isdefault;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, nullable=true, options={"comment":"operasyonun yapıldığı istasyon"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ProductTree.client.maxMessage")
     */
    protected $client;

    /**
     * @var int
     *
     * @ORM\Column(name="notificationtime", type="integer", nullable=true, options={"comment":"operasyonda kullanılan malzemelerin bildirim süresi"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $notificationtime=2;

    /**
     * @var int
     *
     * @ORM\Column(name="lotcount", type="integer", nullable=true, options={"comment":"lot kontrol adedi"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $lotcount;
}
