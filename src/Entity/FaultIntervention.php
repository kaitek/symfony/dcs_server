<?php

namespace App\Entity;

use App\Model\FaultIntervention as MFaultIntervention;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FaultIntervention
 *
 * @ORM\Table(name="fault_interventions")
 * @ORM\Entity(repositoryClass="App\Repository\FaultInterventionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FaultIntervention extends MFaultIntervention
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clients", type="text", options={"comment":"| ile birleşmiş cihaz isimleri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="FaultIntervention.clients.not_blank")
     * @Assert\NotNull(message="FaultIntervention.clients.not_blank")
     */
    protected $clients;

    /**
     * @var string
     *
     * @ORM\Column(name="faultinterventiontype", type="string", length=50, options={"comment":"lost_types.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "FaultIntervention.losttype.maxMessage")
     * @Assert\NotBlank(message="FaultIntervention.losttype.not_blank")
     * @Assert\NotNull(message="FaultIntervention.losttype.not_blank")
     */
    protected $faultinterventiontype;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"comment":"planlı duruş adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "FaultIntervention.name.maxMessage")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="interventions", type="string", length=255, nullable=true, options={"comment":"müdahale edenler"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "FaultIntervention.interventions.maxMessage")
     */
    protected $interventions;

    /**
     * @var string
     *
     * @ORM\Column(name="controlname", type="string", length=255, nullable=true, options={"comment":"yapılan kontrol"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "FaultIntervention.controlname.maxMessage")
     */
    protected $controlname;

    /**
     * @var string
     *
     * @ORM\Column(name="controlofficers", type="string", length=255, nullable=true, options={"comment":"kontrol sorumluları"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "FaultIntervention.controlofficers.maxMessage")
     */
    protected $controlofficers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true, options={"comment":"Geçerlilik başlangıç tarihi"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true, options={"comment":"Geçerlilik bitiş tarihi"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="answertype", type="string", length=50, nullable=true, options={"comment":"cevap tipi, evet-hayır,sayı değeri,yazı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "FaultIntervention.answertype.maxMessage")
     * @Assert\NotBlank(message="FaultIntervention.answertype.not_blank")
     * @Assert\NotNull(message="FaultIntervention.answertype.not_blank")
     */
    protected $answertype;

    /**
     * @var string
     *
     * @ORM\Column(name="valuerequire", type="string", length=100, nullable=true, options={"comment":"beklenen değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "FaultIntervention.valuerequire.maxMessage")
     */
    protected $valuerequire;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemin", type="string", length=100, nullable=true, options={"comment":"kabul edilebilir alt sınır"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "FaultIntervention.valuemin.maxMessage")
     */
    protected $valuemin;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemax", type="string", length=100, nullable=true, options={"comment":"kabul edilebilir üst sınır"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "FaultIntervention.valuemax.maxMessage")
     */
    protected $valuemax;

    /**
     * @var string
     *
     * @ORM\Column(name="mailto", type="text", nullable=true, options={"comment":""})
     */
    protected $mailto;

    /**
     * @var bool
     *
     * @ORM\Column(name="isallowna", type="boolean", nullable=true, options={"comment":"gerekli değil olabilir mi"})
     */
    protected $isallowna=true;
}
