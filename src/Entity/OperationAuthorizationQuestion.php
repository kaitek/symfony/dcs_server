<?php

namespace App\Entity;

use App\Model\OperationAuthorizationQuestion as MOperationAuthorizationQuestion;
use Doctrine\ORM\Mapping as ORM;

/**
 * OperationAuthorizationQuestion
 *
 * @ORM\Table(name="operation_authorization_questions")
 * @ORM\Entity(repositoryClass="App\Repository\OperationAuthorizationQuestionRepository")
 */
class OperationAuthorizationQuestion extends MOperationAuthorizationQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected $question;

    /**
     * @var int
     *
     * @ORM\Column(name="answertype", type="smallint", options={"default" : 0})
     */
    protected $answertype=0;

    /**
     * @var int
     *
     * @ORM\Column(name="answermin", type="integer", nullable=true)
     */
    protected $answermin;

    /**
     * @var int
     *
     * @ORM\Column(name="answermax", type="integer", nullable=true)
     */
    protected $answermax;

    /**
     * @var bool
     *
     * @ORM\Column(name="ismandatory", type="boolean", options={"default" : false})
     */
    protected $ismandatory=false;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer", options={"default" : 0})
     */
    protected $score=0;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="smallint", options={"default" : 3})
     */
    protected $level=3;

    /**
     * @var int
     *
     * @ORM\Column(name="listorder", type="smallint", options={"default" : 0})
     */
    protected $listorder=0;

    /**
     * @var bool
     *
     * @ORM\Column(name="isdescriptionrequire", type="boolean", options={"default" : false})
     */
    protected $isdescriptionrequire=false;
}
