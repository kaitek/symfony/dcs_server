<?php

namespace App\Entity;

use App\Model\Employee as MEmployee;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Employee
 *
 * @ORM\Table(name="employees",indexes={@ORM\Index(name="idx__employees__code", columns={"code"}),@ORM\Index(name="idx__employees__record_id", columns={"record_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Employee extends MEmployee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"şirketteki sicil numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Employee.code.maxMessage")
     * @Assert\NotBlank(message="Employee.code.not_blank")
     * @Assert\NotNull(message="Employee.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "Employee.name.maxMessage")
     * @Assert\NotBlank(message="Employee.name.not_blank")
     * @Assert\NotNull(message="Employee.name.not_blank")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=100, nullable=true, options={"comment":"rfid-barkod numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "Employee.secret.maxMessage")
     */
    protected $secret;

    /**
     * @var bool
     *
     * @ORM\Column(name="isexpert", type="boolean")
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Employee.isexpert.not_null")
     */
    protected $isexpert=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isoperator", type="boolean")
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Employee.isoperator.not_null")
     */
    protected $isoperator=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="ismaintenance", type="boolean")
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Employee.ismaintenance.not_null")
     */
    protected $ismaintenance=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="ismould", type="boolean")
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Employee.ismould.not_null")
     */
    protected $ismould=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isquality", type="boolean")
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Employee.isquality.not_null")
     */
    protected $isquality=false;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotationteam", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Employee.jobrotationteam.maxMessage")
     */
    protected $jobrotationteam;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="Employee.start.not_blank")
     * @Assert\NotNull(message="Employee.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, nullable=true, options={"comment":"çalışıyor ise çalıştığı istasyona ait client.code alanındaki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $client;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tasklist", type="string", length=200, nullable=true, options={"comment":"çalışıyor ise çalıştığı göreve ait tasklist.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 200)
     */
    protected $tasklist;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", nullable=true, options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, nullable=true, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $jobrotation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastseen", type="datetime", nullable=true, options={"comment":"sistemde en son ne zaman bir cihazdan ayrılmış bilgisi"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $lastseen;

    /**
     * @var string
     *
     * @ORM\Column(name="strlastseen", type="string", length=50, nullable=true, options={"comment":"sistemde en son ne zaman bir cihazdan ayrılmış bilgisi string"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $strlastseen;

    /**
     * @var string
     *
     * @ORM\Column(name="teamleader", type="string", length=50, nullable=true, options={"comment":"takım lideri string"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $teamleader;
}

