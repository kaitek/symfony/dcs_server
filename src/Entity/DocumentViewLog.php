<?php

namespace App\Entity;

use App\Model\DocumentViewLog as MDocumentViewLog;
use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentViewLog
 *
 * @ORM\Table(name="document_view_logs",indexes={@ORM\Index(name="idx__document_view_logs__opname_erprefnumber", columns={"opname","erprefnumber"})})
 * @ORM\Entity(repositoryClass="App\Repository\DocumentViewLogRepository")
 */
class DocumentViewLog extends MDocumentViewLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    protected $path;
    
    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50)
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     */
    protected $opname;

    /**
     * @var int
     *
     * @ORM\Column(name="currentversion", type="integer")
     */
    protected $currentversion;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date")
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50)
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

}

