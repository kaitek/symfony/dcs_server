<?php

namespace App\Entity;

use App\Model\IOEvent as MIOEvent;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * IOEvent
 *
 * @ORM\Table(name="io_events")
 * @ORM\Entity(repositoryClass="App\Repository\IOEventRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IOEvent extends MIOEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "IOEvent.code.maxMessage")
     * @Assert\NotBlank(message="IOEvent.code.not_blank")
     * @Assert\NotNull(message="IOEvent.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="iotype", type="string", length=1, options={"comment":"I-O"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 1, maxMessage = "IOEvent.iotype.maxMessage")
     * @Assert\NotBlank(message="IOEvent.iotype.not_blank")
     * @Assert\NotNull(message="IOEvent.iotype.not_blank")
     */
    protected $iotype;

}

