<?php

namespace App\Entity;

use App\Model\MouldType as MMouldType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MouldType
 *
 * @ORM\Table(name="mould_types")
 * @ORM\Entity(repositoryClass="App\Repository\MouldTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MouldType extends MMouldType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldType.code.maxMessage")
     * @Assert\NotBlank(message="MouldType.code.not_blank")
     * @Assert\NotNull(message="MouldType.code.not_blank")
     */
    protected $code;

}

