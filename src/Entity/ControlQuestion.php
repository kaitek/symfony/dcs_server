<?php

namespace App\Entity;

use App\Model\ControlQuestion as MControlQuestion;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ControlQuestion
 *
 * @ORM\Table(name="control_questions",indexes={@ORM\Index(name="idx__control_questions__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\ControlQuestionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ControlQuestion extends MControlQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"sorunun sorulacağı yer, setup bitimi,kalite onay gibi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ControlQuestion.type.maxMessage")
     * @Assert\NotBlank(message="ControlQuestion.type.not_blank")
     * @Assert\NotNull(message="ControlQuestion.type.not_blank")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"operasyon adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "ControlQuestion.opname.maxMessage")
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255, options={"comment":"soru metni"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ControlQuestion.question.maxMessage")
     * @Assert\NotBlank(message="ControlQuestion.question.not_blank")
     * @Assert\NotNull(message="ControlQuestion.question.not_blank")
     */
    protected $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answertype", type="string", length=50, options={"comment":"cevap tipi, evet-hayır,sayı değeri,yazı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ControlQuestion.answertype.maxMessage")
     * @Assert\NotBlank(message="ControlQuestion.answertype.not_blank")
     * @Assert\NotNull(message="ControlQuestion.answertype.not_blank")
     */
    protected $answertype;

    /**
     * @var string
     *
     * @ORM\Column(name="valuerequire", type="string", length=100, nullable=true, options={"comment":"beklenen değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ControlQuestion.valuerequire.maxMessage")
     */
    protected $valuerequire;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemin", type="string", length=100, nullable=true, options={"comment":"kabul edilebilir alt sınır"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ControlQuestion.valuemin.maxMessage")
     */
    protected $valuemin;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemax", type="string", length=100, nullable=true, options={"comment":"kabul edilebilir üst sınır"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 100, maxMessage = "ControlQuestion.valuemax.maxMessage")
     */
    protected $valuemax;

    /**
     * @var string
     *
     * @ORM\Column(name="documentnumber", type="string", length=50, nullable=true, options={"comment":"erp tarafındaki evrak numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ControlQuestion.documentnumber.maxMessage")
     */
    protected $documentnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="clients", type="string", length=255, nullable=true, options={"comment":"istasyon adları"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ControlQuestion.clients.maxMessage")
     */
    protected $clients;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true, options={"comment":"istasyon adları"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ControlQuestion.path.maxMessage")
     */
    protected $path;

    /**
     * @var bool
     *
     * @ORM\Column(name="isallowna", type="boolean", nullable=true, options={"comment":"gerekli değil olabilir mi"})
     */
    protected $isallowna=true;
}
