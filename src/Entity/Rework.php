<?php

namespace App\Entity;

use App\Model\Rework as MRework;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rework
 *
 * @ORM\Table(name="reworks")
 * @ORM\Entity(repositoryClass="App\Repository\ReworkRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Rework extends MRework
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, nullable=true)
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="reworktype", type="string", length=50)
     * @Assert\NotNull(message="Rework.reworktype.not_null")
     */
    protected $reworktype;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=60)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, nullable=true)
     * @Assert\Length(max = 50, maxMessage = "Rework.opcode.maxMessage")
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", nullable=true)
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true)
     * @Assert\Length(max = 60, maxMessage = "Rework.opname.maxMessage")
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true)
     * @Assert\Length(max = 255, maxMessage = "Rework.description.maxMessage")
     */
    protected $opdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="tpp", type="decimal", precision=10, scale=4, nullable=true)
     */
    protected $tpp;

    /**
     * @var bool
     *
     * @ORM\Column(name="islisted", type="boolean", options={"default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="Rework.islisted.not_null")
     */
    protected $islisted=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="ismandatory", type="boolean", nullable=true, options={"default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $ismandatory=false;

    /**
     * @var int
     *
     * @ORM\Column(name="listorder", type="integer", nullable=true, options={"comment":"cihazdaki listelemenin sıralama değeri","default" : 0})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $listorder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="Rework.start.not_blank")
     * @Assert\NotNull(message="Rework.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;
}
