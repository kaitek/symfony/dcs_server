<?php

namespace App\Entity;

use App\Model\WorkOutSystem as MWorkOutSystem;
use Doctrine\ORM\Mapping as ORM;

/**
 * WorkOutSystem
 *
 * @ORM\Table(name="work_out_system",indexes={@ORM\Index(name="idx__work_out_system__day_jobrotation", columns={"day","jobrotation"})})
 * @ORM\Entity(repositoryClass="App\Repository\WorkOutSystemRepository")
 */
class WorkOutSystem extends MWorkOutSystem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date")
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50)
     */
    protected $jobrotation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="opnames", type="string", length=1000)
     */
    protected $opnames;

}

