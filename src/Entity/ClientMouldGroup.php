<?php

namespace App\Entity;

use App\Model\ClientMouldGroup as MClientMouldGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientMouldGroup
 *
 * @ORM\Table(name="client_mould_groups",indexes={@ORM\Index(name="idx__client_mould_groups__client_code", columns={"client","code"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientMouldGroupRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ClientMouldGroup extends MClientMouldGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientMouldGroup.code.maxMessage")
     * @Assert\NotBlank(message="ClientMouldGroup.code.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientMouldGroup.client.maxMessage")
     * @Assert\NotBlank(message="ClientMouldGroup.client.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.client.not_blank")
     */
    protected $client;

    /**
     * @var int
     *
     * @ORM\Column(name="productionmultiplier", type="integer", options={"comment":"erp birim üretim süresi değeri düşürmede kullanılan çarpan"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Range(
     *      min = 10,
     *      max = 100,
     *      notInRangeMessage = "ClientMouldGroup.productionmultiplier.notInRangeMessage"
     * )
     * @Assert\NotBlank(message="ClientMouldGroup.productionmultiplier.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.productionmultiplier.not_blank")
     */
    protected $productionmultiplier=100;

    /**
     * @var decimal
     *
     * @ORM\Column(name="intervalmultiplier", type="decimal", precision=10, scale=4, options={"comment":"sistemin duruşa geçmesi için kullanılan çarpan"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * * @Assert\Range(
     *      min = 1,
     *      max = 100,
     *      notInRangeMessage = "ClientMouldGroup.intervalmultiplier.notInRangeMessage"
     * )
     * @Assert\NotBlank(message="ClientMouldGroup.intervalmultiplier.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.intervalmultiplier.not_blank")
     */
    protected $intervalmultiplier=5;

    /**
     * @var int
     *
     * @ORM\Column(name="setup", type="integer", options={"comment":"ayar süresi sn değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ClientMouldGroup.setup.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.setup.not_blank")
     */
    protected $setup=600;

    /**
     * @var decimal
     *
     * @ORM\Column(name="cycletime", type="decimal", precision=10, scale=4, options={"comment":"çevrim süresi sn değeri"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ClientMouldGroup.cycletime.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.cycletime.not_blank")
     */
    protected $cycletime=0;

    /**
     * @var int
     *
     * @ORM\Column(name="lotcount", type="integer")
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ClientMouldGroup.lotcount.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.lotcount.not_blank")
     */
    protected $lotcount=100;

    /**
     * @var int
     *
     * @ORM\Column(name="operatorcount", type="integer", nullable=true)
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $operatorcount=1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="ClientMouldGroup.start.not_blank")
     * @Assert\NotNull(message="ClientMouldGroup.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var bool
     *
     * @ORM\Column(name="isdefault", type="boolean", nullable=true, options={"comment":"seçili ise ön tanımlı üretim varyasyonu olduğunu belirtir"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isdefault=false;

}
