<?php

namespace App\Entity;

use App\Model\TaskImport as MTaskImport;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskImport
 *
 * @ORM\Table(name="task_imports",indexes={@ORM\Index(name="idx__task_imports__erprefnumber", columns={"erprefnumber"}),@ORM\Index(name="idx__task_imports__erprefnumber_opnumber", columns={"erprefnumber","opnumber"}),@ORM\Index(name="idx__task_imports__opcode_opnumber", columns={"opcode","opnumber"}),@ORM\Index(name="idx__task_imports__type", columns={"type"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskImportRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskImport extends MTaskImport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=60, options={"comment":"client+opcode+opnumber+erprefnumber+time"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @ ORM\GeneratedValue(strategy="UUID") Burada generated value kullanılırsa ID alanına yazmaya çalışıyor
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, options={"comment":"mrp iş emri no kd-jobno vs"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskImport.erprefnumber.maxMessage")
     * @Assert\NotBlank(message="TaskImport.erprefnumber.not_blank")
     * @Assert\NotNull(message="TaskImport.erprefnumber.not_blank")
     */
    protected $erprefnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, options={"comment":"operasyon kodu, producttree.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskImport.opcode.maxMessage")
     * @Assert\NotBlank(message="TaskImport.opcode.not_blank")
     * @Assert\NotNull(message="TaskImport.opcode.not_blank")
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", options={"comment":"operasyon numarası, producttree.number"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskImport.opnumber.not_blank")
     * @Assert\NotNull(message="TaskImport.opnumber.not_blank")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true, options={"comment":"producttree.description alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opdescription;

    /**
     * @var int
     *
     * @ORM\Column(name="productcount", type="integer", options={"comment":"üretilecek adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskImport.productcount.not_blank")
     * @Assert\NotNull(message="TaskImport.productcount.not_blank")
     */
    protected $productcount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskImport.deadline.not_blank")
     * @Assert\NotNull(message="TaskImport.deadline.not_blank")
     */
    protected $deadline;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code ön tanımlı istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskImport.client.maxMessage")
     * @Assert\NotBlank(message="TaskImport.client.not_blank")
     * @Assert\NotNull(message="TaskImport.client.not_blank")
     */
    protected $client;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"time-quantity"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskImport.type.maxMessage")
     * @Assert\NotBlank(message="TaskImport.type.not_blank")
     * @Assert\NotNull(message="TaskImport.type.not_blank")
     */
    protected $type;

    /**
     * @var decimal
     *
     * @ORM\Column(name="tpp", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"birim üretim süresi sn değeri"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $tpp;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, options={"comment":"erp statu"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskImport.type.maxMessage")
     */
    protected $status;

}

