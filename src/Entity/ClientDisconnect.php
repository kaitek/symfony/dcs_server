<?php

namespace App\Entity;

use App\Model\ClientDisconnect as MClientDisconnect;
use Doctrine\ORM\Mapping as ORM;

/**
 * ClientDisconnect
 *
 * @ORM\Table(name="client_disconnects",indexes={@ORM\Index(name="idx__client_disconnects__client", columns={"client"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientDisconnectRepository")
 */
class ClientDisconnect extends MClientDisconnect
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"istasyon"})
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", options={"comment":"başlangıç"})
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true, options={"comment":"bitiş"})
     */
    protected $finish;

}

