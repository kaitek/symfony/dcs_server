<?php

namespace App\Entity;

use App\Model\CaseMovement as MCaseMovement;
use Doctrine\ORM\Mapping as ORM;

/**
 * CaseMovement
 *
 * @ORM\Table(name="case_movements",indexes={@ORM\Index(name="idx__case_movements__erprefnumber", columns={"erprefnumber"}),@ORM\Index(name="idx__case_movements__status", columns={"status"})})
 * @ORM\Entity(repositoryClass="App\Repository\CaseMovementRepository")
 */
class CaseMovement extends MCaseMovement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier", type="string", length=50, nullable=true)
     */
    protected $carrier;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="stockcode", type="string", length=50)
     */
    protected $stockcode;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint")
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(name="casename", type="string", length=50)
     */
    protected $casename;

    /**
     * @var string
     *
     * @ORM\Column(name="locationsource", type="string", length=255, nullable=true)
     */
    protected $locationsource;

    /**
     * @var string
     *
     * @ORM\Column(name="locationdestination", type="string", length=255, nullable=true)
     */
    protected $locationdestination;

    /**
     * @var string
     *
     * @ORM\Column(name="tasktype", type="string", length=20)
     */
    protected $tasktype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="starttime", type="datetime")
     */
    protected $starttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="targettime", type="datetime", nullable=true)
     */
    protected $targettime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assignmenttime", type="datetime", nullable=true)
     */
    protected $assignmenttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actualfinishtime", type="datetime", nullable=true)
     */
    protected $actualfinishtime;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantitynecessary", type="decimal", precision=20, scale=4)
     */
    protected $quantitynecessary;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantityready", type="decimal", precision=15, scale=4, nullable=true)
     */
    protected $quantityready;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantitydelivered", type="decimal", precision=20, scale=4, nullable=true)
     */
    protected $quantitydelivered;

    /**
     * @var string
     *
     * @ORM\Column(name="empcode", type="string", length=50, nullable=true)
     */
    protected $empcode;

    /**
     * @var string
     *
     * @ORM\Column(name="empname", type="string", length=100, nullable=true)
     */
    protected $empname;

    /**
     * @var string
     *
     * @ORM\Column(name="locationsourceinfo", type="string", length=255, nullable=true)
     */
    protected $locationsourceinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="locationdestinationinfo", type="string", length=255, nullable=true)
     */
    protected $locationdestinationinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="filepath", type="string", length=255, nullable=true)
     */
    protected $filepath;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="estimatewaitfinishtime", type="datetime", nullable=true)
     */
    protected $estimatewaitfinishtime;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="carriertype", type="string", length=10, nullable=true)
     */
    protected $carriertype;

    /**
     * @var int
     *
     * @ORM\Column(name="breaktimeseconds", type="integer", nullable=true)
     */
    protected $breaktimeseconds;

    /**
     * @var string
     *
     * @ORM\Column(name="carrierfeeder", type="string", length=255, nullable=true, options={"comment":"besleyen taşıyıcı cihazların kodları , ile ayrılmış şekilde"})
     */
    protected $carrierfeeder;

    /**
     * @var string
     *
     * @ORM\Column(name="carrierdrainer", type="string", length=255, nullable=true, options={"comment":"boşaltan taşıyıcı cihazların kodları , ile ayrılmış şekilde"})
     */
    protected $carrierdrainer;

    /**
     * @var string
     *
     * @ORM\Column(name="casenumbersource", type="string", length=255, nullable=true, options={"comment":"görev kasa numarası"})
     */
    protected $casenumbersource;

    /**
     * @var string
     *
     * @ORM\Column(name="casenumberdestination", type="string", length=255, nullable=true, options={"comment":"verilen kasa numarası"})
     */
    protected $casenumberdestination;
}
