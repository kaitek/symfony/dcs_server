<?php

namespace App\Entity;

use App\Model\ClientProductionDetail as MClientProductionDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientProductionDetail
 *
 * @ORM\Table(name="client_production_details",indexes={@ORM\Index(name="idx__client_production_details__type_day_jobrotation", columns={"type","day","jobrotation"}),@ORM\Index(name="idx__client_production_details__type_day", columns={"type","day"}),@ORM\Index(name="idx__client_production_details__employee_day", columns={"employee","day"}),@ORM\Index(name="idx__client_production_details__employee_opname", columns={"employee","opname"}),@ORM\Index(name="idx__client_production_details__record_id", columns={"record_id"}),@ORM\Index(name="idx__client_production_details__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientProductionDetailRepository")
 */
class ClientProductionDetail extends MClientProductionDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"c_s:client_signal-c_p:client_production-e_p:employee_production"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code alanındaki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="tasklist", type="string", length=200, options={"comment":"yapılacak görevin tasklist.code değeri(unique)"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     * @Assert\Length(max = 200)
     */
    protected $tasklist;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", options={"comment":"üretim siyalinin zamanı"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50, nullable=true, options={"comment":"mould.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mould;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50, nullable=true, options={"comment":"mouldgroup.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, nullable=true, options={"comment":"producttree.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", nullable=true, options={"comment":"producttree.number alanı değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true, options={"comment":"producttree.description alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $opdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="leafmask", type="string", length=50, nullable=true, options={"comment":"moulddetail.leafmask veya 1"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $leafmask='01';

    /**
     * @var string
     *
     * @ORM\Column(name="leafmaskcurrent", type="string", length=50, nullable=true, options={"comment":"moulddetail.leafmaskcurrent veya 1"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $leafmaskcurrent='01';

    /**
     * @var int
     *
     * @ORM\Column(name="production", type="integer", options={"comment":"her gelen sinyalde çıkması gereken adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $production=0;

    /**
     * @var int
     *
     * @ORM\Column(name="productioncurrent", type="integer", options={"comment":"her gelen sinyalde çıkan adet, kalıp arıza yoksa production ile aynı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productioncurrent=0;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, nullable=true, options={"comment":"employee.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, nullable=true, options={"comment":"erp referans numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $erprefnumber;

    /**
     * @var int
     *
     * @ORM\Column(name="gap", type="integer", options={"comment":"aynı üretimdeki bir önceki üretim sinyali ile aradaki fark"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $gap=0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var decimal
     *
     * @ORM\Column(name="energy", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"harcanan enerji miktarı"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $energy;

    /**
     * @var string
     *
     * @ORM\Column(name="taskfinishtype", type="string", length=50, nullable=true, options={"comment":"task_finish_type.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $taskfinishtype;

    /**
     * @var string
     *
     * @ORM\Column(name="taskfinishdescription", type="string", length=255, nullable=true, options={"comment":"görev bitirme açıklaması"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $taskfinishdescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="taskfromerp", type="boolean", nullable=true, options={"comment":"true ise sistemden gelmiş görev, false ise elle eklenmiş olan görev"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $taskfromerp=true;

    /**
     * @var decimal
     *
     * @ORM\Column(name="calculatedtpp", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"üretim esnasındaki hesaplanan birim üretim süresi"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $calculatedtpp;

    /**
     * @var bool
     *
     * @ORM\Column(name="isexported", type="boolean", nullable=true, options={"comment":"export edildi mi"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isexported=true;

    /**
     * @var int
     *
     * @ORM\Column(name="opsayi", type="integer", nullable=true, options={"comment":"operatör sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opsayi;

    /**
     * @var int
     *
     * @ORM\Column(name="refsayi", type="integer", nullable=true, options={"comment":"referans sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $refsayi;

    /**
     * @var int
     *
     * @ORM\Column(name="losttime", type="integer", nullable=true, options={"comment":"üretim oturumunda yaşanan toplam kayıp süre"})
     */
    protected $losttime=0;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", nullable=true, length=255)
     */
    protected $serialnumber;
}
