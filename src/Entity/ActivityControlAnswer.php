<?php

namespace App\Entity;

use App\Model\ActivityControlAnswer as MActivityControlAnswer;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityControlAnswer
 *
 * @ORM\Table(name="activity_control_answers",indexes={@ORM\Index(name="idx__activity_control_answers__type_day_opname_question", columns={"type","day","opname","question"}),@ORM\Index(name="idx__activity_control_answers__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\ActivityControlAnswerRepository")
 * 
 */
class ActivityControlAnswer extends MActivityControlAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date")
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50)
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="documentnumber", type="string", length=50)
     */
    protected $documentnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answertype", type="string", length=50)
     */
    protected $answertype;

    /**
     * @var string
     *
     * @ORM\Column(name="valuerequire", type="string", length=100, nullable=true)
     */
    protected $valuerequire;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemin", type="string", length=100, nullable=true)
     */
    protected $valuemin;

    /**
     * @var string
     *
     * @ORM\Column(name="valuemax", type="string", length=100, nullable=true)
     */
    protected $valuemax;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=100)
     */
    protected $answer;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="isexported", type="boolean", nullable=true, options={"comment":"export edildi mi"})
     */
    protected $isexported=true;

}

