<?php

namespace App\Entity;

use App\Model\QualityRejectionType as MQualityRejectionType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * QualityRejectionType
 * 
 * @ORM\Table(name="quality_rejection_types",indexes={@ORM\Index(name="idx__quality_rejection_types__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\QualityRejectionTypeRepository")
 */
class QualityRejectionType extends MQualityRejectionType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "QualityRejectionType.code.maxMessage")
     * @Assert\NotBlank(message="QualityRejectionType.code.not_blank")
     * @Assert\NotNull(message="QualityRejectionType.code.not_blank")
     */
    protected $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "QualityRejectionType.description.maxMessage")
     */
    protected $description;

}
