<?php

namespace App\Entity;

use App\Model\Overtime as MOvertime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Overtime
 *
 * @ORM\Table(name="overtimes")
 * @ORM\Entity(repositoryClass="App\Repository\OvertimeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Overtime extends MOvertime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clients", type="text", options={"comment":"| ile birleşmiş cihaz isimleri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="Overtime.clients.not_blank")
     * @Assert\NotNull(message="Overtime.clients.not_blank")
     */
    protected $clients;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"comment":"planlı duruş adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "Overtime.name.maxMessage")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="repeattype", type="string", length=50, options={"comment":"tekrar tipi 1-tekrarlamaz,2-günlük,3-haftalık"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "Overtime.repeattype.maxMessage")
     * @Assert\NotBlank(message="Overtime.repeattype.not_blank")
     * @Assert\NotNull(message="Overtime.repeattype.not_blank")
     */
    protected $repeattype;

    /**
     * @var string
     *
     * @ORM\Column(name="days", type="string", length=255, nullable=true, options={"comment":"| ile birleşmiş gün isimleri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "Overtime.days.maxMessage")
     */
    protected $days;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startday", type="date", nullable=true, options={"comment":"tekrarlamaz seçili ise dolu yoksa boş"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $startday;

    /**
     * @var string
     *
     * @ORM\Column(name="starttime", type="string", length=5, options={"comment":"başlangıç saat değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 5, maxMessage = "Overtime.starttime.maxMessage")
     * @Assert\NotBlank(message="Overtime.starttime.not_blank")
     * @Assert\NotNull(message="Overtime.starttime.not_blank")
     */
    protected $starttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finishday", type="date", nullable=true, options={"comment":"tekrarlamaz seçili ise dolu yoksa boş"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finishday;

    /**
     * @var string
     *
     * @ORM\Column(name="finishtime", type="string", length=5, options={"comment":"bitiş saat değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 5, maxMessage = "Overtime.finishtime.maxMessage")
     * @Assert\NotBlank(message="Overtime.finishtime.not_blank")
     * @Assert\NotNull(message="Overtime.finishtime.not_blank")
     */
    protected $finishtime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true, options={"comment":"Geçerlilik başlangıç tarihi"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true, options={"comment":"Geçerlilik bitiş tarihi"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

