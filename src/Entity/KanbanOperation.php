<?php

namespace App\Entity;

use App\Model\KanbanOperation as MKanbanOperation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * KanbanOperation
 *
 * @ORM\Table(name="kanban_operations",indexes={@ORM\Index(name="idx__kanban_operations__client", columns={"client"}),@ORM\Index(name="idx__kanban_operations__product", columns={"product"})})
 * @ORM\Entity(repositoryClass="App\Repository\KanbanOperationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class KanbanOperation extends MKanbanOperation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "KanbanOperation.client.maxMessage")
     * @Assert\NotBlank(message="KanbanOperation.client.not_blank")
     * @Assert\NotNull(message="KanbanOperation.client.not_blank")
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string", length=60, options={"comment":"product_tree.name değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "KanbanOperation.product.maxMessage")
     * @Assert\NotBlank(message="KanbanOperation.product.not_blank")
     * @Assert\NotNull(message="KanbanOperation.product.not_blank")
     */
    protected $product;

    /**
     * @var int
     *
     * @ORM\Column(name="boxcount", type="integer", options={"comment":"ekranda çizilecek kart sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="KanbanOperation.boxcount.not_blank")
     * @Assert\NotNull(message="KanbanOperation.boxcount.not_blank")
     */
    protected $boxcount=0;

    /**
     * @var int
     *
     * @ORM\Column(name="minboxcount", type="integer", options={"comment":"min kart değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="KanbanOperation.minboxcount.not_blank")
     * @Assert\NotNull(message="KanbanOperation.minboxcount.not_blank")
     */
    protected $minboxcount=0;

    /**
     * @var int
     *
     * @ORM\Column(name="maxboxcount", type="integer", options={"comment":"max kart değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="KanbanOperation.maxboxcount.not_blank")
     * @Assert\NotNull(message="KanbanOperation.maxboxcount.not_blank")
     */
    protected $maxboxcount=0;

    /**
     * @var int
     *
     * @ORM\Column(name="currentboxcount", type="integer", options={"comment":"mevcut kart sayısı", "default" : 0})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull()
     */
    protected $currentboxcount=0;

    /**
     * @var int
     *
     * @ORM\Column(name="packaging", type="integer", options={"comment":"kasa içi miktar"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="KanbanOperation.packaging.not_blank")
     * @Assert\NotNull(message="KanbanOperation.packaging.not_blank")
     */
    protected $packaging=0;

    /**
     * @var int
     *
     * @ORM\Column(name="hourlydemand", type="integer", options={"comment":"saatlik talep miktarı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="KanbanOperation.hourlydemand.not_blank")
     * @Assert\NotNull(message="KanbanOperation.hourlydemand.not_blank")
     */
    protected $hourlydemand=0;

}

