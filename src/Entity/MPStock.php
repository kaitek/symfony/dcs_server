<?php
/* tpp değeri import sonrası bu tabloya kayıt yazılırken o anki producttree
 * değeri alınarak doldurulacak
 *
 * finish zaman değeri tipi quantity olan işler için miktara ulaşıldığında
 * otomatik olarak atanacak. tipi time olan işlerde bir daha seçilememesi için
 * elle web arayüz üzerinden kapatılabilecek
 *
 */

namespace App\Entity;

use App\Model\MPStock as MMPStock;
use Doctrine\ORM\Mapping as ORM;

/**
 * MPStock
 *
 * @ORM\Table(name="mp_stocks",indexes={@ORM\Index(name="idx__mp_stocks__stockcode", columns={"stockcode"})})
 * @ORM\Entity(repositoryClass="App\Repository\MPStockRepository")
 */
class MPStock extends MMPStock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="stockcode", type="string", length=50)
     */
    protected $stockcode;

    /**
     * @var decimal
     *
     * @ORM\Column(name="qerp", type="decimal", precision=20, scale=4, options={"default" : 0})
     */
    protected $qerp;

    /**
     * @var decimal
     *
     * @ORM\Column(name="qreserved", type="decimal", precision=20, scale=4, options={"default" : 0})
     */
    protected $qreserved;

    /**
     * @var decimal
     *
     * @ORM\Column(name="qconsumed", type="decimal", precision=20, scale=4, options={"default" : 0})
     */
    protected $qconsumed;

    /**
     * @var decimal
     *
     * @ORM\Column(name="qstock", type="decimal", precision=20, scale=4, options={"default" : 0})
     */
    protected $qstock;

    /**
     * @var string
     *
     * @ORM\Column(name="lastupdate", type="string", length=50)
     */
    protected $lastupdate;
}
