<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskCasesLog
 *
 * @ORM\Table(name="task_cases_logs",indexes={@ORM\Index(name="idx__task_cases_logs__erprefnumber", columns={"erprefnumber"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskCasesLogRepository")
 */
class TaskCasesLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="casetype", type="string", length=5)
     */
    protected $casetype;

    /**
     * @var string
     *
     * @ORM\Column(name="casenumber", type="string", length=50, nullable=true)
     */
    protected $casenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="caselot", type="string", length=50, nullable=true)
     */
    protected $caselot;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50)
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="stockname", type="string", length=60, nullable=true)
     */
    protected $stockname;

    /**
     * @var string
     *
     * @ORM\Column(name="casename", type="string", length=50)
     */
    protected $casename;

    /**
     * @var decimal
     *
     * @ORM\Column(name="packcapacity", type="decimal", precision=20, scale=4, nullable=true)
     */
    protected $packcapacity;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantityremaining", type="decimal", precision=20, scale=4, nullable=true)
     */
    protected $quantityremaining;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantityremainingfirst", type="decimal", precision=20, scale=4, nullable=true)
     */
    protected $quantityremainingfirst;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="employees", type="string", length=255, nullable=true)
     */
    protected $employees;
}
