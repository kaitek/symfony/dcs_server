<?php

namespace App\Entity;

use App\Model\JobRotationWorkPeople as MJobRotationWorkPeople;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobRotationWorkPeople
 *
 * @ORM\Table(name="job_rotation_work_peoples",indexes={@ORM\Index(name="idx__job_rotation_work_peoples__day_jobrotation", columns={"day","jobrotation"})})
 * @ORM\Entity(repositoryClass="App\Repository\JobRotationWorkPeopleRepository")
 */
class JobRotationWorkPeople extends MJobRotationWorkPeople
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="JobRotationWorkPeople.day.not_blank")
     * @Assert\NotNull(message="JobRotationWorkPeople.day.not_blank")
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30-15:30 şeklinde vardiya bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotationWorkPeople.jobrotation.maxMessage")
     * @Assert\NotBlank(message="JobRotationWorkPeople.jobrotation.not_blank")
     * @Assert\NotNull(message="JobRotationWorkPeople.jobrotation.not_blank")
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotationteam", type="string", length=50, options={"comment":"vardiya ekip lideri-vardiya adı"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotationWorkPeople.jobrotationteam.maxMessage")
     * @Assert\NotBlank(message="JobRotationWorkPeople.jobrotationteam.not_blank")
     * @Assert\NotNull(message="JobRotationWorkPeople.jobrotationteam.not_blank")
     */
    protected $jobrotationteam;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, options={"comment":"personel"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "JobRotationWorkPeople.employee.maxMessage")
     * @Assert\NotBlank(message="JobRotationWorkPeople.employee.not_blank")
     * @Assert\NotNull(message="JobRotationWorkPeople.employee.not_blank")
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="beginval", type="string", length=8, options={"comment":"07:30:00 şeklinde zaman bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 8, maxMessage = "JobRotationWorkPeople.beginval.maxMessage")
     * @Assert\NotBlank(message="JobRotationWorkPeople.beginval.not_blank")
     * @Assert\NotNull(message="JobRotationWorkPeople.beginval.not_blank")
     */
    protected $beginval;

    /**
     * @var string
     *
     * @ORM\Column(name="endval", type="string", length=8, options={"comment":"15:29:59 şeklinde zaman bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 8, maxMessage = "JobRotationWorkPeople.endval.maxMessage")
     * @Assert\NotBlank(message="JobRotationWorkPeople.endval.not_blank")
     * @Assert\NotNull(message="JobRotationWorkPeople.endval.not_blank")
     */
    protected $endval;

}

