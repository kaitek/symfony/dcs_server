<?php

namespace App\Entity;

use App\Model\Carrier as MCarrier;
use Doctrine\ORM\Mapping as ORM;

/**
 * Carrier
 * 
 * @ORM\Table(name="carriers",indexes={@ORM\Index(name="idx__carriers__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\CarrierRepository")
 */
class Carrier extends MCarrier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     */
    protected $uuid;

    /**
     * @var bool
     *
     * @ORM\Column(name="connected", type="boolean", nullable=true)
     */
    protected $connected;

    /**
     * @var string
     *
     * @ORM\Column(name="workflow", type="string", length=50)
     */
    protected $workflow;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=true)
     */
    protected $ip;

    /**
     * @var int
     *
     * @ORM\Column(name="pincode", type="smallint")
     */
    protected $pincode;

    /**
     * @var string
     *
     * @ORM\Column(name="versionapp", type="string", length=15, nullable=true)
     */
    protected $versionapp;

    /**
     * @var string
     *
     * @ORM\Column(name="versionshell", type="string", length=15, nullable=true)
     */
    protected $versionshell;

    /**
     * @var string
     *
     * @ORM\Column(name="computername", type="string", length=50, nullable=true)
     */
    protected $computername;
}

