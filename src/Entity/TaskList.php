<?php
/* tpp değeri import sonrası bu tabloya kayıt yazılırken o anki producttree
 * değeri alınarak doldurulacak
 *
 * finish zaman değeri tipi quantity olan işler için miktara ulaşıldığında
 * otomatik olarak atanacak. tipi time olan işlerde bir daha seçilememesi için
 * elle web arayüz üzerinden kapatılabilecek
 *
 */

namespace App\Entity;

use App\Model\TaskList as MTaskList;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskList
 *
 * @ORM\Table(name="task_lists",indexes={@ORM\Index(name="idx__task_lists__erprefnumber", columns={"erprefnumber"}),@ORM\Index(name="idx__task_lists__erprefnumber_opnumber", columns={"erprefnumber","opnumber"}),@ORM\Index(name="idx__task_lists__opcode_opnumber", columns={"opcode","opnumber"}),@ORM\Index(name="idx__task_lists__type", columns={"type"}),@ORM\Index(name="idx__task_lists__finish", columns={"finish"}),@ORM\Index(name="idx__task_lists__record_id", columns={"record_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskListRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskList extends MTaskList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idx", type="string", length=60, nullable=true, options={"comment":"birleştirilmiş görevlerin tanımlanması için kullanılan değer"})
     */
    protected $idx;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, options={"comment":"mrp iş emri no kd-jobno vs"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskList.erprefnumber.maxMessage")
     * @Assert\NotBlank(message="TaskList.erprefnumber.not_blank")
     * @Assert\NotNull(message="TaskList.erprefnumber.not_blank")
     */
    protected $erprefnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, options={"comment":"operasyon kodu, producttree.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskList.opcode.maxMessage")
     * @Assert\NotBlank(message="TaskList.opcode.not_blank")
     * @Assert\NotNull(message="TaskList.opcode.not_blank")
     */
    protected $opcode;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=60, options={"comment":"client+opcode+opnumber+erprefnumber+time"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "TaskList.code.maxMessage")
     * @ ORM\GeneratedValue(strategy="UUID") Burada generated value kullanılırsa ID alanına yazmaya çalışıyor
     */
    protected $code;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", options={"comment":"operasyon numarası, producttree.number"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskList.opnumber.not_blank")
     * @Assert\NotNull(message="TaskList.opnumber.not_blank")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "TaskList.opname.maxMessage")
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true, options={"comment":"producttree.description alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "TaskList.opdescription.maxMessage")
     */
    protected $opdescription;

    /**
     * @var int
     *
     * @ORM\Column(name="productcount", type="integer", options={"comment":"üretilecek adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskList.productcount.not_blank")
     * @Assert\NotNull(message="TaskList.productcount.not_blank")
     */
    protected $productcount;

    /**
     * @var int
     *
     * @ORM\Column(name="productdonecount", type="integer", options={"default" : 0,"comment":"toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdonecount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="productdoneactivity", type="integer", options={"default" : 0,"comment":"aktivitede toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdoneactivity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="productdonejobrotation", type="integer", options={"default" : 0,"comment":"vardiyada toplam üretilmiş olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $productdonejobrotation = 0;


    /**
     * @var int
     *
     * @ORM\Column(name="scrapactivity", type="integer", options={"default" : 0,"comment":"aktivitedeki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrapactivity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="scrapjobrotation", type="integer", options={"default" : 0,"comment":"vardiyadaki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrapjobrotation = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="scrappart", type="integer", options={"default" : 0,"comment":"iş emrindeki toplam ıskarta olan adet"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $scrappart = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", options={"comment":"01.01.2017 formatında gün değeri"})
     * @ Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskList.deadline.not_blank")
     * @Assert\NotNull(message="TaskList.deadline.not_blank")
     */
    protected $deadline;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=500, options={"comment":"client.code ön tanımlı istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 500, maxMessage = "TaskList.client.maxMessage")
     * @Assert\NotBlank(message="TaskList.client.not_blank")
     * @Assert\NotNull(message="TaskList.client.not_blank")
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"time-quantity"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskList.type.maxMessage")
     * @Assert\NotBlank(message="TaskList.type.not_blank")
     * @Assert\NotNull(message="TaskList.type.not_blank")
     */
    protected $type;

    /**
     * @var decimal
     *
     * @ORM\Column(name="tpp", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"birim üretim süresi sn değeri"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $tpp;

    /**
     * @var bool
     *
     * @ORM\Column(name="taskfromerp", type="boolean", nullable=true, options={"comment":"true ise sistemden gelmiş görev, false ise elle eklenmiş olan görev"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $taskfromerp = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="tfddescription", type="string", length=255, nullable=true, options={"comment":"cihazdan girilen görevin açıklaması"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "TaskList.tfddescription.maxMessage")
     */
    protected $tfddescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plannedstart", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskList.plannedstart.not_blank")
     * @Assert\NotNull(message="TaskList.plannedstart.not_blank")
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plannedfinish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $plannedfinish;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=true, options={"default" : 0,"comment":"birim süre (sn)*miktar"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $duration;

    /**
     * @var int
     *
     * @ORM\Column(name="offtime", type="integer", nullable=true, options={"default" : 0,"comment":"işin içindeki mola süresi"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $offtime;

    /**
     * @var int
     *
     * @ORM\Column(name="totaltime", type="integer", nullable=true, options={"default" : 0,"comment":"(birim süre*miktar)+mola süresi"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $totaltime;

    /**
     * @var string
     *
     * @ORM\Column(name="resourceid", type="string", length=50, nullable=true, options={"comment":"robot ise fikstür bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskList.resourceid.maxMessage")
     */
    protected $resourceid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true, options={"comment":"durum değeri"})
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldaddress", type="string", length=50, nullable=true, options={"comment":"kalıp raf adresi"})
     */
    protected $mouldaddress;

    /**
   * @var json
   *
   * @ORM\Column(name="taskinfo", type="json", nullable=true)
   */
    protected $taskinfo;

}
