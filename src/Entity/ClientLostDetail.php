<?php

namespace App\Entity;

use App\Model\ClientLostDetail as MClientLostDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientLostDetail
 *
 * @ORM\Table(name="client_lost_details",indexes={@ORM\Index(name="idx__client_lost_details__type_day_jobrotation", columns={"type","day","jobrotation"}),@ORM\Index(name="idx__client_lost_details__type_day", columns={"type","day"}),@ORM\Index(name="idx__client_lost_details__client_day", columns={"client","day"}),@ORM\Index(name="idx__client_lost_details__employee_day", columns={"employee","day"}),@ORM\Index(name="idx__client_lost_details__record_id", columns={"record_id"}),@ORM\Index(name="idx__client_lost_details__opname", columns={"opname"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientLostDetailRepository")
 */
class ClientLostDetail extends MClientLostDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, options={"comment":"l_c:lost_client,l_c_t:lost_client_task,l_e_t:lost_employee_task-l_e:lost_employee"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code alanındaki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="losttype", type="string", length=50, options={"comment":"lost_types.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $losttype;

    /**
     * @var string
     *
     * @ORM\Column(name="faulttype", type="string", length=50, nullable=true, options={"comment":"fault_types.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $faulttype;

    /**
     * @var string
     *
     * @ORM\Column(name="faultdescription", type="string", length=255, nullable=true, options={"comment":"faulttype.description arıza kodu açıklaması"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $faultdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="faultgroupcode", type="string", length=50, nullable=true, options={"comment":"c_e:İstasyon Elektrik,c_m:İstasyon Mekanik,m:Kalıp-Fikstür-Aparat,p:Üretim"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $faultgroupcode;

    /**
     * @var string
     *
     * @ORM\Column(name="faultdevice", type="string", length=50, nullable=true, options={"comment":"client_detail.code alanındaki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $faultdevice;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionlost", type="string", length=255, nullable=true, options={"comment":"elle girilmiş olan duruş sebebi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $descriptionlost;

    /**
     * @var string
     *
     * @ORM\Column(name="tasklist", type="string", length=200, nullable=true, options={"comment":"yapılacak görevin tasklist.code değeri(unique)"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 200)
     */
    protected $tasklist;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50, nullable=true, options={"comment":"mould.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mould;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50, nullable=true, options={"comment":"mouldgroup.code değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $mouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50, nullable=true, options={"comment":"producttree.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer", nullable=true, options={"comment":"producttree.number alanı değeri"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="opdescription", type="string", length=255, nullable=true, options={"comment":"producttree.description alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $opdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, nullable=true, options={"comment":"employee.code alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50, nullable=true, options={"comment":"erp referans numarası"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $erprefnumber;

    /**
     * @var bool
     *
     * @ORM\Column(name="taskfromerp", type="boolean", nullable=true, options={"comment":"true ise sistemden gelmiş görev, false ise elle eklenmiş olan görev"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $taskfromerp=true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var decimal
     *
     * @ORM\Column(name="energy", type="decimal", precision=10, scale=4, nullable=true, options={"comment":"harcanan enerji miktarı"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $energy;

    /**
     * @var string
     *
     * @ORM\Column(name="sourceclient", type="string", length=50, nullable=true, options={"comment":"client.code alanındaki değer,kaybın sebebi olan istasyon"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $sourceclient;

    /**
     * @var string
     *
     * @ORM\Column(name="sourcelosttype", type="string", length=50, nullable=true, options={"comment":"lost_types.code"})
     * @Assert\Length(max = 50)
     */
    protected $sourcelosttype;

    /**
     * @var string
     *
     * @ORM\Column(name="sourcedescriptionlost", type="string", length=255, nullable=true, options={"comment":"elle girilmiş olan duruş sebebi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255)
     */
    protected $sourcedescriptionlost;

    /**
     * @var bool
     *
     * @ORM\Column(name="isexported", type="boolean", nullable=true, options={"comment":"export edildi mi"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isexported=true;

    /**
     * @var int
     *
     * @ORM\Column(name="opsayi", type="integer", nullable=true, options={"comment":"operatör sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $opsayi;

    /**
     * @var int
     *
     * @ORM\Column(name="refsayi", type="integer", nullable=true, options={"comment":"referans sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $refsayi;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", nullable=true, length=255)
     */
    protected $serialnumber;
}
