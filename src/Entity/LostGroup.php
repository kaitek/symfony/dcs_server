<?php

namespace App\Entity;

use App\Model\LostGroup as MLostGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LostGroup
 *
 * @ORM\Table(name="lost_groups",indexes={@ORM\Index(name="idx__lost_groups__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\LostGroupRepository")
 */
class LostGroup extends MLostGroup
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, options={"comment":"oee,süreç performans gibi ana grup kodu"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostGroup.code.maxMessage")
     * @Assert\NotBlank(message="LostGroup.code.not_blank")
     * @Assert\NotNull(message="LostGroup.code.not_blank")
     */
    protected $code;
    
    /**
     * @var string
     *
     * @ORM\Column(name="groupcode", type="string", length=50, options={"comment":"oee-kullanılabilirlik,oee-planlı duruşlar gibi değerler"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostGroup.groupcode.maxMessage")
     * @Assert\NotBlank(message="LostGroup.groupcode.not_blank")
     * @Assert\NotNull(message="LostGroup.groupcode.not_blank")
     */
    protected $groupcode;

}

