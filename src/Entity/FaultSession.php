<?php

namespace App\Entity;

// use App\Model\FaultSession as MFaultSession;
use Doctrine\ORM\Mapping as ORM;

/**
 * FaultSession
 *
 * @ORM\Table(name="fault_sessions",indexes={@ORM\Index(name="idx__fault_sessions__client", columns={"client"}),@ORM\Index(name="idx__fault_sessions__idx", columns={"idx"}),@ORM\Index(name="idx__fault_sessions__losttype", columns={"losttype"}),@ORM\Index(name="idx__fault_sessions__faulttype", columns={"faulttype"})})
 * @ORM\Entity(repositoryClass="App\Repository\FaultSessionRepository")
 */
class FaultSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idx", type="string", length=60, options={"comment":"arıza başına unique değer"})
     */
    protected $idx;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code alanındaki değer"})
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="losttype", type="string", length=50, options={"comment":"lost_types.code"})
     */
    protected $losttype;

    /**
     * @var string
     *
     * @ORM\Column(name="faulttype", type="string", length=50, options={"comment":"fault_types.code değeri"})
     */
    protected $faulttype;

    /**
     * @var string
     *
     * @ORM\Column(name="faultdescription", type="string", length=255, nullable=true, options={"comment":"faulttype.description arıza kodu açıklaması"})
     */
    protected $faultdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="faultgroupcode", type="string", length=50, options={"comment":"c_e:İstasyon Elektrik,c_m:İstasyon Mekanik,m:Kalıp-Fikstür-Aparat,p:Üretim"})
     */
    protected $faultgroupcode;

    /**
     * @var string
     *
     * @ORM\Column(name="faultdevice", type="string", length=50, nullable=true, options={"comment":"client_detail.code alanındaki değer"})
     */
    protected $faultdevice;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionlost", type="string", length=255, nullable=true, options={"comment":"elle girilmiş olan duruş sebebi"})
     */
    protected $descriptionlost;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50, options={"comment":"employee.code alanı değeri, arızayı bildiren"})
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="employeeintervention", type="string", length=255, nullable=true, options={"comment":"employee.code alanı değeri, arızaya müdahale edenler"})
     */
    protected $employeeintervention;

    /**
    * @var string
    *
    * @ORM\Column(name="employeeinterventionconfirm", type="string", length=255, nullable=true, options={"comment":"employee.code alanı değeri, arızaya müdahale onayı veren sicil"})
    */
    protected $employeeinterventionconfirm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", options={"comment":"arıza başlangıç"})
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true, options={"comment":"arıza bitiş"})
     */
    protected $finish;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startintervention", type="datetime", nullable=true, options={"comment":"müdahale başlangıç"})
     */
    protected $startintervention;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finishintervention", type="datetime", nullable=true, options={"comment":"müdahale bitiş"})
     */
    protected $finishintervention;

    /**
     * @var string
     *
     * @ORM\Column(name="interventionnok", type="text", nullable=true, options={"comment":"müdahale red maddeleri"})
     */
    protected $interventionnok;

    /**
     * @var string
     *
     * @ORM\Column(name="interventionnokdescription", type="string", length=255, nullable=true, options={"comment":"müdahale red açıklama"})
     */
    protected $interventionnokdescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startinterventionconfirm", type="datetime", nullable=true, options={"comment":"müdahale onay başlangıç"})
     */
    protected $startinterventionconfirm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finishinterventionconfirm", type="datetime", nullable=true, options={"comment":"müdahale onay bitiş"})
     */
    protected $finishinterventionconfirm;

    /**
     * @var string
     *
     * @ORM\Column(name="interventionconfirmnok", type="text", nullable=true, options={"comment":"müdahale onay red maddeleri"})
     */
    protected $interventionconfirmnok;

    /**
     * @var string
     *
     * @ORM\Column(name="interventionconfirmnokdescription", type="string", length=255, nullable=true, options={"comment":"müdahale onay red açıklama"})
     */
    protected $interventionconfirmnokdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", nullable=true, length=255, options={"comment":"varsa kalıp seri no"})
     */
    protected $serialnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, options={"comment":"kayıt statü değeri"})
     */
    protected $status;
}
