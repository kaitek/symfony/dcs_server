<?php

namespace App\Entity;

use App\Model\CarrierSession as MCarrierSession;
use Doctrine\ORM\Mapping as ORM;

/**
 * CarrierSession
 *
 * @ORM\Table(name="carrier_sessions",indexes={@ORM\Index(name="idx__carrier_sessions__carrier_empcode", columns={"carrier","empcode"}),@ORM\Index(name="idx__carrier_sessions__day", columns={"day"})})
 * @ORM\Entity(repositoryClass="App\Repository\CarrierSessionRepository")
 */
class CarrierSession extends MCarrierSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier", type="string", length=50)
     */
    protected $carrier;

    /**
     * @var string
     *
     * @ORM\Column(name="empcode", type="string", length=50)
     */
    protected $empcode;

    /**
     * @var string
     *
     * @ORM\Column(name="empname", type="string", length=100)
     */
    protected $empname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     */
    protected $finish;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", nullable=true, options={"comment":"2016-01-01 formatında gün değeri"})
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, nullable=true, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     */
    protected $jobrotation;

}

