<?php

namespace App\Entity;

use App\Model\TaskListLaser as MTaskListLaser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskListLaser
 *
 * @ORM\Table(name="task_list_lasers",indexes={@ORM\Index(name="idx__task_list_lasers__code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskListLaserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskListLaser extends MTaskListLaser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaser.code.maxMessage")
     * @Assert\NotBlank(message="TaskListLaser.code.not_blank")
     * @Assert\NotNull(message="TaskListLaser.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "TaskListLaser.description.maxMessage")
     */
    protected $description;

    /**
     * @var int
     *
     * @ORM\Column(name="production", type="integer", options={"comment":"kesimi yapılacak plaka sayısı"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskListLaser.production.not_blank")
     */
    protected $production=0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tfdescription", type="string", length=255, nullable=true, options={"comment":"bitirme sebebi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "TaskListLaser.tfdescription.maxMessage")
     */
    protected $tfdescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plannedstart", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="TaskListLaser.plannedstart.not_blank")
     * @Assert\NotNull(message="TaskListLaser.plannedstart.not_blank")
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="client1", type="string", length=50, nullable=true, options={"comment":"client.code ön tanımlı istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaser.client.maxMessage")
     */
    protected $client1;

    /**
     * @var string
     *
     * @ORM\Column(name="client2", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaser.client.maxMessage")
     */
    protected $client2;

    /**
     * @var string
     *
     * @ORM\Column(name="client3", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaser.client.maxMessage")
     */
    protected $client3;

    /**
     * @var string
     *
     * @ORM\Column(name="client4", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaser.client.maxMessage")
     */
    protected $client4;

    /**
     * @var string
     *
     * @ORM\Column(name="client5", type="string", length=50, nullable=true, options={"comment":"client.code alternatif istasyon bilgisi."})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "TaskListLaser.client.maxMessage")
     */
    protected $client5;

}

