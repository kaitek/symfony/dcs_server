<?php

namespace App\Entity;

use App\Model\OperationAuthorizationAnswer as MOperationAuthorizationAnswer;
use Doctrine\ORM\Mapping as ORM;

/**
 * OperationAuthorizationAnswer
 *
 * @ORM\Table(name="operation_authorization_answers",indexes={@ORM\Index(name="idx__operation_authorization_answers__masterId", columns={"masterid"})})
 * @ORM\Entity(repositoryClass="App\Repository\OperationAuthorizationAnswerRepository")
 */
class OperationAuthorizationAnswer extends MOperationAuthorizationAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="masterId", type="integer")
     */
    protected $masterId;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected $question;

    /**
     * @var int
     *
     * @ORM\Column(name="answer", type="integer")
     */
    protected $answer;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    protected $description;

}

