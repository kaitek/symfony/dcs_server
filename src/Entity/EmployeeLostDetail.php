<?php

namespace App\Entity;

use App\Model\EmployeeLostDetail as MEmployeeLostDetail;
use Doctrine\ORM\Mapping as ORM;

/**
 * EmployeeLostDetail
 *
 * @ORM\Table(name="employee_lost_details",indexes={@ORM\Index(name="idx__employee_lost_details__day", columns={"day"}),@ORM\Index(name="idx__employee_lost_details__week", columns={"week"})})
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeLostDetailRepository")
 */
class EmployeeLostDetail extends MEmployeeLostDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50)
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date")
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50)
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="teamleader", type="string", length=50, nullable=true)
     */
    protected $teamleader;

    /**
     * @var int
     *
     * @ORM\Column(name="week", type="integer")
     */
    protected $week;

    /**
     * @var string
     *
     * @ORM\Column(name="employee", type="string", length=50)
     */
    protected $employee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime")
     */
    protected $finish;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer")
     */
    protected $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    protected $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="ismandatory", type="boolean", options={"default" : false})
     */
    protected $ismandatory=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isrework", type="boolean", options={"default" : false})
     */
    protected $isrework=false;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    protected $description;
}
