<?php

namespace App\Entity;

use App\Model\MouldDetail as MMouldDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MouldDetail
 *
 * @ORM\Table(name="mould_details",indexes={@ORM\Index(name="idx__mould_details__mould_mouldgroup_opcode", columns={"mould","mouldgroup","opcode"})})
 * @ORM\Entity(repositoryClass="App\Repository\MouldDetailRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MouldDetail extends MMouldDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mould", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldDetail.mould.maxMessage")
     * @Assert\NotBlank(message="MouldDetail.mould.not_blank")
     * @Assert\NotNull(message="MouldDetail.mould.not_blank")
     */
    protected $mould;

    /**
     * @var string
     *
     * @ORM\Column(name="mouldgroup", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldDetail.mouldgroup.maxMessage")
     * @Assert\NotBlank(message="MouldDetail.mouldgroup.not_blank")
     * @Assert\NotNull(message="MouldDetail.mouldgroup.not_blank")
     */
    protected $mouldgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="opcode", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldDetail.opcode.maxMessage")
     * @Assert\NotBlank(message="MouldDetail.opcode.not_blank")
     * @Assert\NotNull(message="MouldDetail.opcode.not_blank")
     */
    protected $opcode;

    /**
     * @var int
     *
     * @ORM\Column(name="opnumber", type="integer")
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="MouldDetail.opnumber.not_blank")
     * @Assert\NotNull(message="MouldDetail.opnumber.not_blank")
     */
    protected $opnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60, maxMessage = "MouldDetail.opname.maxMessage")
     * @Assert\NotBlank(message="MouldDetail.opname.not_blank")
     * @Assert\NotNull(message="MouldDetail.opname.not_blank")
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="leafmask", type="string", length=50, options={"comment":"kalıbın çıktı sayısı, ilk değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldDetail.leafmask.maxMessage")
     * @Assert\NotBlank(message="MouldDetail.leafmask.not_blank")
     * @Assert\NotNull(message="MouldDetail.leafmask.not_blank")
     */
    protected $leafmask="01";
    
    /**
     * @var string
     *
     * @ORM\Column(name="leafmaskcurrent", type="string", length=50, options={"comment":"kalıbın çıktı sayısı şimdiki değer"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MouldDetail.leafmaskcurrent.maxMessage")
     * @Assert\NotBlank(message="MouldDetail.leafmaskcurrent.not_blank")
     * @Assert\NotNull(message="MouldDetail.leafmaskcurrent.not_blank")
     */
    protected $leafmaskcurrent="01";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="MouldDetail.start.not_blank")
     * @Assert\NotNull(message="MouldDetail.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

}

