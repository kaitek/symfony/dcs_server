<?php

namespace App\Entity;

use App\Model\MaterialUnit as MMaterialUnit;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MaterialUnit
 *
 * @ORM\Table(name="material_units")
 * @ORM\Entity(repositoryClass="App\Repository\MaterialUnitRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MaterialUnit extends MMaterialUnit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "MaterialUnit.code.maxMessage")
     * @Assert\NotBlank(message="MaterialUnit.code.not_blank")
     * @Assert\NotNull(message="MaterialUnit.code.not_blank")
     */
    protected $code;

}

