<?php

namespace App\Entity;

use App\Model\ClientGroupDetail as MClientGroupDetail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientGroupDetail
 *
 * @ORM\Table(name="client_group_details",indexes={@ORM\Index(name="idx__client_group_details__clientgroup", columns={"clientgroup"}),@ORM\Index(name="idx__client_group_details__clientgroupcode", columns={"clientgroupcode"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientGroupDetailRepository")
 */
class ClientGroupDetail extends MClientGroupDetail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clientgroup", type="string", length=50, options={"comment":"clientgroup.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientGroupDetail.clientgroup.maxMessage")
     * @Assert\NotBlank(message="ClientGroupDetail.clientgroup.not_blank")
     * @Assert\NotNull(message="ClientGroupDetail.clientgroup.not_blank")
     */
    protected $clientgroup;

    /**
     * @var string
     *
     * @ORM\Column(name="clientgroupcode", type="string", length=50, options={"comment":"clientgroup.groupcode"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientGroupDetail.clientgroupcode.maxMessage")
     * @Assert\NotBlank(message="ClientGroupDetail.clientgroupcode.not_blank")
     * @Assert\NotNull(message="ClientGroupDetail.clientgroupcode.not_blank")
     */
    protected $clientgroupcode;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"client.code"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientGroupDetail.client.maxMessage")
     * @Assert\NotBlank(message="ClientGroupDetail.client.not_blank")
     * @Assert\NotNull(message="ClientGroupDetail.client.not_blank")
     */
    protected $client;

}

