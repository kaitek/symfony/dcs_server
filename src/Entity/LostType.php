<?php

namespace App\Entity;

use App\Model\LostType as MLostType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LostType
 *
 * @ORM\Table(name="lost_types")
 * @ORM\Entity(repositoryClass="App\Repository\LostTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class LostType extends MLostType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostType.code.maxMessage")
     * @Assert\NotBlank(message="LostType.code.not_blank")
     * @Assert\NotNull(message="LostType.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="extcode", type="string", length=50, nullable=true, options={"comment":"dışarı aktarımda kullanılacak olan kod"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostType.extcode.maxMessage")
     */
    protected $extcode;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "LostType.description.maxMessage")
     */
    protected $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="isemployeerequired", type="boolean", options={"comment":"duruşu başlatırken operatör girilmesi gerekli mi"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.isemployeerequired.not_null")
     */
    protected $isemployeerequired=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isexpertemployeerequired", type="boolean", options={"comment":"duruşu başlatırken-bitirirken yetkili sicil girilmesi gerekli mi","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.isexpertemployeerequired.not_null")
     */
    protected $isexpertemployeerequired=false;

    /**
     * @var int
     *
     * @ORM\Column(name="time", type="integer", nullable=true,  options={"comment":"duruşu bitirirken belirtilen süreden fazla ise yetkili sicil bilgisi istenecek"})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $time=0;

    /**
     * @var bool
     *
     * @ORM\Column(name="isoperationnumberrequired", type="boolean", options={"comment":"kalıp arıza kayıplarında hat bazlı çalışan yerlerde arızanın olduğu kalıp bilgisi sorulması flag değişkeni"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.isoperationnumberrequired.not_null")
     */
    protected $isoperationnumberrequired=false;

    /**
     * @var string
     *
     * @ORM\Column(name="ioevent", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "LostType.ioevent.maxMessage")
     */
    protected $ioevent;

    /**
     * @var bool
     *
     * @ORM\Column(name="closenoemployee", type="boolean", options={"comment":"duruş devam ederken operatörlerin hepsi çıkarsa cihazı iş yok veya görevde kimse yok konumuna getir","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.closenoemployee.not_null")
     */
    protected $closenoemployee=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="islisted", type="boolean", options={"comment":"cihaz üzerinden kaybın seçilebilir olmasını belirtir","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.islisted.not_null")
     */
    protected $islisted=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isclient", type="boolean", options={"comment":"cihaz kayıplarında kaybın seçilebilir olmasını belirtir","default" : "true"})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.isclient.not_null")
     */
    protected $isclient=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isemployee", type="boolean", options={"comment":"operatör kayıplarında kaybın seçilebilir olmasını belirtir","default" : true})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.isemployee.not_null")
     */
    protected $isemployee=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isshortlost", type="boolean", options={"comment":"oee raporunda kısa duruş olarak gruplanmasını belirtir","default" : true})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.isshortlost.not_null")
     */
    protected $isshortlost=true;

    /**
     * @var bool
     *
     * @ORM\Column(name="issourcelookup", type="boolean", options={"comment":"b matris için kayba sebep olan istasyon bakılıp bakılmamasını belirtir","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotNull(message="LostType.isshortlost.not_null")
     */
    protected $issourcelookup=false;

    /**
     * @var int
     *
     * @ORM\Column(name="listorder", type="integer", nullable=true,  options={"comment":"cihazdaki listelemenin sıralama değeri","default" : 0})
     * @Assert\Type(
     *      type="integer",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $listorder=0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank(message="LostType.start.not_blank")
     * @Assert\NotNull(message="LostType.start.not_blank")
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finish", type="datetime", nullable=true)
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $finish;

    /**
     * @var string
     *
     * @ORM\Column(name="authorizeloststart", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $authorizeloststart;

    /**
     * @var string
     *
     * @ORM\Column(name="authorizelostfinish", type="string", length=50, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $authorizelostfinish;

    /**
     * @var bool
     *
     * @ORM\Column(name="isdescriptionrequired", type="boolean", nullable=true, options={"comment":"açıklama girilmesi gerekli mi","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isdescriptionrequired=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isopenallports", type="boolean", nullable=true, options={"comment":"bütün çıkış portlarını pasif hale getirir","default" : false})
     * @Assert\Type(
     *      type="bool",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isopenallports=false;
}
