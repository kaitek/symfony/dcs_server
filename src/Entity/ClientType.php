<?php

namespace App\Entity;

use App\Model\ClientType as MClientType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClientType
 *
 * @ORM\Table(name="client_types")
 * @ORM\Entity(repositoryClass="App\Repository\ClientTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ClientType extends MClientType
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "ClientType.code.maxMessage")
     * @Assert\NotBlank(message="ClientType.code.not_blank")
     * @Assert\NotNull(message="ClientType.code.not_blank")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 255, maxMessage = "ClientType.description.maxMessage")
     */
    protected $description;
    
}

