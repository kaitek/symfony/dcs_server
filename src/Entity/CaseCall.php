<?php

namespace App\Entity;

use App\Model\CaseCall as MCaseCall;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CaseCall
 *
 * @ORM\Table(name="case_calls",indexes={@ORM\Index(name="idx__case_calls__day_jobrotation", columns={"day","jobrotation"}),@ORM\Index(name="idx__case_calls__state", columns={"state"}),@ORM\Index(name="idx__case_calls__opname", columns={"opname"}),@ORM\Index(name="idx__case_calls__casemovementid", columns={"casemovementid"})})
 * @ORM\Entity(repositoryClass="App\Repository\CaseCallRepository")
 */
class CaseCall extends MCaseCall
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=50, options={"comment":"istasyon"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "CaseCall.client.maxMessage")
     * @Assert\NotBlank(message="CaseCall.client.not_blank")
     * @Assert\NotNull(message="CaseCall.client.not_blank")
     */
    protected $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", options={"comment":"2016-01-01 formatında gün değeri"})
     * @Assert\Type(
     *      type="datetime",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\NotBlank()
     */
    protected $day;

    /**
     * @var string
     *
     * @ORM\Column(name="jobrotation", type="string", length=50, options={"comment":"07:30 şeklinde vardiya başlangıç bilgisi"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     * @Assert\NotBlank()
     */
    protected $jobrotation;

    /**
     * @var string
     *
     * @ORM\Column(name="erprefnumber", type="string", length=50)
     */
    protected $erprefnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="opname", type="string", length=60, nullable=true, options={"comment":"producttree.name alanı değeri"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 60)
     */
    protected $opname;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=50, options={"comment":"Durum"})
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50, maxMessage = "CaseCall.state.maxMessage")
     * @Assert\NotBlank(message="CaseCall.state.not_blank")
     * @Assert\NotNull(message="CaseCall.state.not_blank")
     */
    protected $state;

    /**
     * @var int
     *
     * @ORM\Column(name="casemovementId", type="integer", nullable=true)
     */
    protected $casemovementId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calltime", type="datetime", options={"comment":"Çağrı zamanı"})
     */
    protected $calltime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="starttime", type="datetime", nullable=true, options={"comment":"Taşıma görevi oluşma zamanı"})
     */
    protected $starttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assignmenttime", type="datetime", nullable=true, options={"comment":"Taşıma görevi atanma zamanı"})
     */
    protected $assignmenttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actualfinishtime", type="datetime", nullable=true, options={"comment":"Taşıma görevi kapanma zamanı"})
     */
    protected $actualfinishtime;

}

