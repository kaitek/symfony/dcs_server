<?php

namespace App\Repository;

use App\Entity\QualityRejectionType;

/**
 * QualityRejectionTypeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class QualityRejectionTypeRepository extends \Doctrine\ORM\EntityRepository
{
}
