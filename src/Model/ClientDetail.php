<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * ClientDetail
 */
abstract class ClientDetail extends BaseSync implements BaseSyncInterface, ClientDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $clienttype;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $iotype;
    
    /**
     * @var integer
     */
    protected $portnumber;
    
    /**
     * @var string
     */
    protected $ioevent;

    /**
     * @var string
     */
    protected $triggertype;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ClientDetail
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ClientDetail
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set clienttype
     *
     * @param string $clienttype
     *
     * @return ClientDetail
     */
    public function setClienttype($clienttype)
    {
        $this->clienttype = $clienttype;

        return $this;
    }

    /**
     * Get clienttype
     *
     * @return string
     */
    public function getClienttype()
    {
        return $this->clienttype;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set iotype
     *
     * @param string $iotype
     *
     * @return ClientDetail
     */
    public function setIotype($iotype)
    {
        $this->iotype = $iotype;

        return $this;
    }

    /**
     * Get iotype
     *
     * @return string
     */
    public function getIotype()
    {
        return $this->iotype;
    }

    /**
     * Set portnumber
     *
     * @param integer $portnumber
     *
     * @return ClientDetail
     */
    public function setPortnumber($portnumber) {
        $this->portnumber = $portnumber;
    }
    
    /**
     * Get portnumber
     *
     * @return integer
     */
    public function getPortnumber() {
        return $this->portnumber;
    }
        
    /**
     * Set ioevent
     *
     * @param string $ioevent
     *
     * @return ClientDetail
     */
    public function setIOEvent($ioevent)
    {
        $this->ioevent = $ioevent;

        return $this;
    }

    /**
     * Get ioevent
     *
     * @return string
     */
    public function getIOEvent()
    {
        return $this->ioevent;
    }

    /**
     * Set triggertype
     *
     * @param string $triggertype
     *
     * @return ClientDetail
     */
    public function setTriggertype($triggertype)
    {
        $this->triggertype = $triggertype;

        return $this;
    }

    /**
     * Get triggertype
     *
     * @return string
     */
    public function getTriggertype()
    {
        return $this->triggertype;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientDetail
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientDetail
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
    

}

