<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * Employee
 */
abstract class Employee extends BaseSync implements BaseSyncInterface, EmployeeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var boolean
     */
    protected $isexpert;

    /**
     * @var boolean
     */
    protected $isoperator;

    /**
     * @var boolean
     */
    protected $ismaintenance;

    /**
     * @var boolean
     */
    protected $ismould;

    /**
     * @var boolean
     */
    protected $isquality;

    /**
     * @var string
     */
    protected $jobrotationteam;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var string
     */
    protected $client;
    
    /**
     * @var string
     */
    protected $tasklist;
    
    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var \DateTime
     */
    protected $lastseen;

    /**
     * @var string
     */
    protected $strlastseen;

    /**
     * @var string
     */
    protected $teamleader;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Employee
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Employee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set secret
     *
     * @param string $secret
     *
     * @return Employee
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set isexpert
     *
     * @param boolean $isexpert
     *
     * @return Employee
     */
    public function setIsexpert($isexpert)
    {
        $this->isexpert = $isexpert;

        return $this;
    }

    /**
     * Get isexpert
     *
     * @return boolean
     */
    public function getIsexpert()
    {
        return $this->isexpert;
    }

    /**
     * Set isoperator
     *
     * @param boolean $isoperator
     *
     * @return Employee
     */
    public function setIsoperator($isoperator)
    {
        $this->isoperator = $isoperator;

        return $this;
    }

    /**
     * Get isoperator
     *
     * @return boolean
     */
    public function getIsoperator()
    {
        return $this->isoperator;
    }

    /**
     * Set ismaintenance
     *
     * @param boolean $ismaintenance
     *
     * @return Employee
     */
    public function setIsmaintenance($ismaintenance)
    {
        $this->ismaintenance = $ismaintenance;

        return $this;
    }

    /**
     * Get ismaintenance
     *
     * @return boolean
     */
    public function getIsmaintenance()
    {
        return $this->ismaintenance;
    }

    /**
     * Set ismould
     *
     * @param boolean $ismould
     *
     * @return Employee
     */
    public function setIsmould($ismould)
    {
        $this->ismould = $ismould;

        return $this;
    }

    /**
     * Get ismould
     *
     * @return boolean
     */
    public function getIsmould()
    {
        return $this->ismould;
    }

    /**
     * Set isquality
     *
     * @param boolean $isquality
     *
     * @return Employee
     */
    public function setIsquality($isquality)
    {
        $this->isquality = $isquality;

        return $this;
    }

    /**
     * Get isquality
     *
     * @return boolean
     */
    public function getIsquality()
    {
        return $this->isquality;
    }

    /**
     * Set jobrotationteam
     *
     * @param string $jobrotationteam
     *
     * @return Employee
     */
    public function setJobrotationteam($jobrotationteam)
    {
        $this->jobrotationteam = $jobrotationteam;

        return $this;
    }

    /**
     * Get jobrotationteam
     *
     * @return string
     */
    public function getJobrotationteam()
    {
        return $this->jobrotationteam;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Employee
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return Employee
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
    
    /**
     * Set client
     *
     * @param string $client
     *
     * @return Employee
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     * Set tasklist
     *
     * @param string $tasklist
     *
     * @return Employee
     */
    public function setTasklist($tasklist)
    {
        $this->tasklist = $tasklist;

        return $this;
    }

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist()
    {
        return $this->tasklist;
    }
    
    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return Employee
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return Employee
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set lastseen
     *
     * @param \DateTime $lastseen
     *
     * @return Employee
     */
    public function setLastseen($lastseen)
    {
        $this->lastseen = $lastseen;

        return $this;
    }

    /**
     * Get lastseen
     *
     * @return \DateTime
     */
    public function getLastseen()
    {
        return $this->lastseen;
    }

    /**
     * Set strlastseen
     *
     * @param string $strlastseen
     *
     * @return Employee
     */
    public function setStrlastseen($strlastseen)
    {
        $this->strlastseen = $strlastseen;

        return $this;
    }

    /**
     * Get strlastseen
     *
     * @return string
     */
    public function getStrlastseen()
    {
        return $this->strlastseen;
    }

    /**
     * Set teamleader
     *
     * @param string $teamleader
     *
     * @return Employee
     */
    public function setTeamleader($teamleader)
    {
        $this->teamleader = $teamleader;

        return $this;
    }

    /**
     * Get teamleader
     *
     * @return string
     */
    public function getTeamleader()
    {
        return $this->teamleader;
    }
}

