<?php

namespace App\Model;

/**
 * MPStockInterface
 */
interface MPStockInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set stockcode
     *
     * @param string $stockcode
     *
     * @return MPStockInterface
     */
    public function setStockcode($stockcode);

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode();

    /**
     * Set qerp
     *
     * @param decimal $qerp
     *
     * @return MPStockInterface
     */
    public function setQerp($qerp);

    /**
     * Get qerp
     *
     * @return decimal
     */
    public function getQerp();

    /**
     * Set qreserved
     *
     * @param decimal $qreserved
     *
     * @return MPStockInterface
     */
    public function setQreserved($qreserved);

    /**
     * Get qreserved
     *
     * @return decimal
     */
    public function getQreserved();

    /**
     * Set qconsumed
     *
     * @param decimal $qconsumed
     *
     * @return MPStockInterface
     */
    public function setQconsumed($qconsumed);

    /**
     * Get qconsumed
     *
     * @return decimal
     */
    public function getQconsumed();

    /**
     * Set qstock
     *
     * @param decimal $qstock
     *
     * @return MPStockInterface
     */
    public function setQstock($qstock);

    /**
     * Get qstock
     *
     * @return decimal
     */
    public function getQstock();

    /**
     * Set lastupdate
     *
     * @param \DateTime $lastupdate
     *
     * @return MPStockInterface
     */
    public function setLastupdate($lastupdate);

    /**
     * Get lastupdate
     *
     * @return \DateTime
     */
    public function getLastupdate();
}
