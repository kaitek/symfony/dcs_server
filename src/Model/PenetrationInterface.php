<?php
namespace App\Model;

/**
 * PenetrationInterface
 */
interface PenetrationInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set opname
     *
     * @param string $opname
     * @return PenetrationInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();
    
    /**
     * Set timecreate
     *
     * @param \DateTime $timecreate
     * @return PenetrationInterface
     */
    public function setTimecreate($timecreate);

    /**
     * Get timecreate
     *
     * @return \DateTime 
     */
    public function getTimecreate();

    /**
     * Set timeview
     *
     * @param \DateTime $timeview
     * @return PenetrationInterface
     */
    public function setTimeview($timeview);

    /**
     * Get timeview
     *
     * @return \DateTime 
     */
    public function getTimeview();

    /**
     * Set title
     *
     * @param string $title
     * @return PenetrationInterface
     */
    public function setTitle($title);

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle();

    /**
     * Set message
     *
     * @param string $message
     * @return PenetrationInterface
     */
    public function setMessage($message);

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage();

}
