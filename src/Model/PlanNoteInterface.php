<?php
namespace App\Model;

/**
 * PlanNoteInterface
 */
interface PlanNoteInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return PlanNoteInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PlanNoteInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set recordorder
     *
     * @param integer $recordorder
     *
     * @return PlanNoteInterface
     */
    public function setRecordorder($recordorder);

    /**
     * Get recordorder
     *
     * @return integer
     */
    public function getRecordorder();

    /**
     * Set description
     *
     * @param string $description
     * @return PlanNoteInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

}
