<?php
namespace App\Model;

/**
 * QualityRejectionTypeInterface
 */
interface QualityRejectionTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return QualityRejectionTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();
    
    /**
     * Set description
     *
     * @param string $description
     *
     * @return QualityRejectionTypeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();
   
}
