<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base as ModelBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Event\LifecycleEventArgs;
use DateTime;

/**
 *
 */
abstract class BaseSync extends ModelBase implements BaseSyncInterface
{
    public function __construct()
    {
        return parent::__construct();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="record_id", type="string", length=50)
     * @Assert\Type(
     *      type="string",
     *      message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max = 50)
     */
    protected $recordId;

    /**
     * @ORM\PrePersist
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        $table = $em->getClassMetadata(get_class($entity))->getTableName();
        //$conn = $em->getConnection();
        //$now=\DateTime::createFromFormat('U.u', microtime(true));
        //$_d=$now->format('Y-m-d H:i:s.u');
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));

        $_d = $d->format("Y-m-d H:i:s.u");
        $this->recordId = $table."-".$_d;
        if (strlen($this->recordId) > 50) {
            $this->recordId = substr($this->recordId, strlen($this->recordId) - 50, 50);
        }
    }

    /**
     * @ORM\PostPersist
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        $table = $em->getClassMetadata(get_class($entity))->getTableName();
        $conn = $em->getConnection();
        //$now=\DateTime::createFromFormat('U.u', microtime(true));
        //$_d=$now->format('Y-m-d H:i:s.u');
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));

        $_d = $d->format("Y-m-d H:i:s.u");

        try {
            // $u_logs = $conn->fetchAll('SELECT * FROM _sync_updatelogs where "tableName"=?', array($table));
            // if (count($u_logs)>0) {
            //     $conn->update('_sync_updatelogs', array('"updatedAt"' => $_d,'"lastUpdate"' => $_d,'"strlastupdate"' => $d->format("Y-m-d H:i:s")), array('id' => $u_logs[0]["id"]));
            // } else {
            //     $conn->insert('_sync_updatelogs', array('"tableName"' => $table, '"createdAt"' => $_d, '"updatedAt"' => $_d,'"lastUpdate"' => $_d,'"strlastupdate"' => $d->format("Y-m-d H:i:s")));
            //     $_data=array("tableName"=>'_sync_updatelogs',"procType"=>"insert","data"=>array("tableName" => $table, "lastUpdate" => $_d,"createdAt" => $_d,"updatedAt" => $_d,"strlastupdate" => $d->format("Y-m-d H:i:s")));
            //     $this->messagesInsert($conn, $_data, $_d);
            // }
            try {
                //if ($_SERVER['HTTP_HOST']=="192.168.1.10"||$_SERVER['HTTP_HOST']=="192.168.1.40"||$_SERVER['HTTP_HOST']=="127.0.0.1") {
                $u_logs2 = $conn->fetchAll('SELECT * FROM __sync_updates where "tablename"=?', array($table));
                if (count($u_logs2) > 0) {
                    $conn->update('__sync_updates', array('"lastupdate"' => $d->format("Y-m-d H:i:s")), array('id' => $u_logs2[0]["id"]));
                } else {
                    $__data = array("tablename" => $table, "lastupdate" => $d->format("Y-m-d H:i:s"));
                    $conn->insert('__sync_updates', $__data);
                    $_data = array("tableName" => '__sync_updates',"procType" => "insert","data" => $__data);
                    $this->messagesInsert($conn, $_data, $_d);
                }
                //}
            } catch (\Throwable $th) {
                //throw $th;
            }
            $_tmp = array();
            $nlist = array('createuserId','updateuserId','deleteuserId','deleted','version','id');
            foreach ($entity as $key => $value) {
                if (in_array($key, $nlist) == false) {
                    if ($value instanceof DateTime || getType($value) == "object") {
                        $o = new \ReflectionObject($value);
                        //$p = $o->getProperty('date');
                        //if($p){
                        $value = $value->format('Y-m-d H:i:s');
                        //}
                    }
                    if ($key == "recordId") {
                        $_tmp["record_id"] = $value;
                    } elseif ($key == "created") {
                        $_tmp['createdAt'] = $value;
                    } elseif ($key == "updated") {
                        $_tmp['updatedAt'] = $value;
                    } else {
                        $_tmp[$key] = $value;
                    }
                }
            }
            $_data = array("tableName" => $table,"procType" => "insert","data" => $_tmp);
            $this->messagesInsert($conn, $_data, $_d);
        } catch (Exception $e) {
            // Rollback the failed transaction attempt
            //throw $e;
            return $this->msgError($e->getMessage());
        }
    }

    /**
     * @ORM\PostUpdate
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        $table = $em->getClassMetadata(get_class($entity))->getTableName();
        $conn = $em->getConnection();
        //$now=\DateTime::createFromFormat('U.u', microtime(true));
        //$_d=$now->format('Y-m-d H:i:s.u');
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));

        $_d = $d->format("Y-m-d H:i:s.u");
        try {
            //$u_logs = $conn->fetchAll('SELECT * FROM _sync_updatelogs where "tableName"=?', array($table));
            //if (count($u_logs)>0) {
            //    $conn->update('_sync_updatelogs', array('"updatedAt"' => $_d,'"lastUpdate"' => $_d,'"strlastupdate"' => $d->format("Y-m-d H:i:s")), array('id' => $u_logs[0]["id"]));
            //} else {
            //    $conn->insert('_sync_updatelogs', array('"tableName"' => $table, '"createdAt"' => $_d, '"updatedAt"' => $_d,'"lastUpdate"' => $_d,'"strlastupdate"' => $d->format("Y-m-d H:i:s")));
            //}
            //if ($_SERVER['HTTP_HOST']=="192.168.1.10"||$_SERVER['HTTP_HOST']=="192.168.1.40"||$_SERVER['HTTP_HOST']=="127.0.0.1") {
            $u_logs2 = $conn->fetchAll('SELECT * FROM __sync_updates where "tablename"=?', array($table));
            if (count($u_logs2) > 0) {
                $conn->update('__sync_updates', array('"lastupdate"' => $d->format("Y-m-d H:i:s")), array('id' => $u_logs2[0]["id"]));
            } else {
                $__data = array("tablename" => $table, "lastupdate" => $d->format("Y-m-d H:i:s"));
                $conn->insert('__sync_updates', $__data);
                $_data = array("tableName" => '__sync_updates',"procType" => "insert","data" => $__data);
                $this->messagesInsert($conn, $_data, $_d);
            }
            //}
            $_type = "update";
            $_rec_id = "";
            $_tmp = array();
            $nlist = array('createuserId','created','updateuserId','deleteuserId','deleted','version','id');
            foreach ($entity as $key => $value) {
                if (in_array($key, $nlist) == false) {
                    if ($value instanceof DateTime || getType($value) == "object") {
                        $o = new \ReflectionObject($value);
                        //$p = $o->getProperty('date');
                        //if($p){
                        $value = $value->format('Y-m-d H:i:s');
                        //}
                    }
                    if ($key == "recordId") {
                        $_rec_id = $value;
                        $_tmp["record_id"] = $value;
                    } else {
                        $_tmp[$key] = $value;
                    }
                } else {
                    if ($key == "deleteuserId" && $value != "") {
                        $_type = "delete";
                    }
                }
            }
            if ($_type == "delete") {
                $_tmp["updated"] = $_d;
            }
            $_data = array("tableName" => $table,"procType" => $_type,"id" => (isset($_tmp["id"]) ? $_tmp["id"] : null),"record_id" => $_rec_id,"data" => $_tmp);
            $this->messagesInsert($conn, $_data, $_d);
        } catch (Exception $e) {
            // Rollback the failed transaction attempt
            //throw $e;
            return $this->msgError($e->getMessage());
        }
    }

    /**
     * @ORM\PostRemove
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        //$entity = $args->getEntity();
        //$em = $args->getEntityManager();
    }

    private function messagesInsert($conn, $_data, $_d)
    {
        $w = '';
        $arr = array(true);
        $exclude = array('client_lost_details','client_production_details','client_production_signals','control_answers','task_imports','task_reject_details');
        if (in_array($_data['tableName'], $exclude) == true) {
            return;
        }
        $nlist = array('kanban_operations','task_cases'/*,'task_imports','task_lists'*/);
        //if($_data['tableName']==='kanban_operations')
        if (in_array($_data['tableName'], $nlist) == true || ($_data['tableName'] == 'reworks' && $_data['data']['client'] != '' && $_data['data']['client'] != null)) {
            $w .= ' and code=? ';
            $arr[] = $_data['data']['client'];
        }
        //if (($_data['procType']=='insert'||$_data['procType']=='update')&&$_data["tableName"]!=="__sync_updates") {
        //    $w.=' and connected=? ';
        //    $arr[]=true;
        //}
        if (($_data['tableName'] == 'task_list_lasers' || $_data['tableName'] == 'task_list_laser_details')) {
            $w .= ' and workflow=? ';
            $arr[] = 'Lazer Kesim';
        }
        if (($_data['tableName'] == 'task_lists' || $_data['tableName'] == 'reworks')) {
            $w .= ' and workflow<>? ';
            $arr[] = 'Lazer Kesim';
        }
        $data = json_encode($_data);
        $priority = 999;
        if ($_data['tableName'] == 'employees') {
            $priority = 1;
        }
        $tables = array("task_lists", "task_list_lasers", "moulds", "mould_details", "mould_groups");
        if (in_array($_data['tableName'], $tables)) {
            $priority = 10;
        }
        //if ($_data['tableName']=='task_lists'||$_data['tableName']=='task_list_lasers') {
        //    $priority=10;
        //}
        //$clients = $conn->fetchAll('SELECT * FROM clients where uuid is not null and "isactive"=? '.$w, $arr);
        //foreach ($clients as $row) {
        //    $conn->insert('_sync_messages', array('ip' => (($row["code"] !== null && $row["code"] !== '' ? $row["code"] : $row["ip"]).'_server'),'uuid' => $row["uuid"], 'ack' => 'true','queued' => 0, 'type' => 'sync_message', 'data' => $data, '"createdAt"' => $_d, '"updatedAt"' => $_d));
        //}
        $clients = $conn->fetchAll("SELECT * FROM clients where uuid is null and workflow<>'Gölge Cihaz' and isactive=? ".$w, $arr);//and connected=true and workflow<>'Gölge Cihaz'
        foreach ($clients as $row) {
            if ($_data["tableName"] !== '_sync_updatelogs' && $_data["tableName"] !== '__sync_updates' && $_data["tableName"] !== 'task_case_controls') {
                try {
                    //if ($_SERVER['HTTP_HOST']=="192.168.1.10"||$_SERVER['HTTP_HOST']=="192.168.1.40"||$_SERVER['HTTP_HOST']=="127.0.0.1") {
                    if (isset($_data["data"]["createdAt"])) {
                        $_data["data"]["created_at"] = $_data["data"]["createdAt"];
                        unset($_data["data"]["createdAt"]);
                    }
                    if (isset($_data["data"]["updatedAt"])) {
                        $_data["data"]["updated_at"] = $_data["data"]["updatedAt"];
                        unset($_data["data"]["updatedAt"]);
                    }
                    if (isset($_data["data"]["updated"])) {
                        $_data["data"]["updated_at"] = $_data["data"]["updated"];
                        unset($_data["data"]["updated"]);
                    }
                    if (!isset($_data["data"]["created_at"])) {
                        $_data["data"]["created_at"] = $_data["data"]["updated_at"];
                    }
                    $mt = "record_add";
                    $arrInfo = array("tableName" => $_data["tableName"]);
                    if ($_data['procType'] == 'update') {
                        $mt = "record_update";
                        $arrInfo["fieldName"] = "record_id";
                        $arrInfo["fieldValue"] = $_data["data"]["record_id"];
                    }
                    if ($_data['procType'] == 'delete') {
                        $mt = "record_delete";
                        $arrInfo["fieldName"] = "record_id";
                        $arrInfo["fieldValue"] = $_data["data"]["record_id"];
                    }
                    $conn->insert('__sync_messages', array('queuename' => ($row["code"].'_server'), 'priority' => $priority, 'messagetype' => $mt, 'status' => 'wait_for_send', 'time' => $_d, 'data' => json_encode($_data["data"]),'info' => json_encode($arrInfo) ));
                    //}
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }
        }
    }

    public function setRecordId($recordId)
    {
        $this->recordId = $recordId;
    }

    public function getRecordId()
    {
        return $this->recordId;
    }
}
