<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * ProductTree
 */
abstract class ProductTree extends BaseSync implements BaseSyncInterface, ProductTreeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $parent;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var integer
     */
    protected $number;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $materialtype;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $stockcode;

    /**
     * @var string
     */
    protected $stockname;

    /**
     * @var decimal
     */
    protected $tpp;

    /**
     * @var decimal
     */
    protected $quantity;

    /**
     * @var string
     */
    protected $unit_quantity;

    /**
     * @var integer
     */
    protected $opcounter;

    /**
     * @var string
     */
    protected $pack;

    /**
     * @var decimal
     */
    protected $packcapacity;

    /**
     * @var string
     */
    protected $unit_packcapacity;

    /**
     * @var integer
     */
    protected $oporder;

    /**
     * @var integer
     */
    protected $preptime;

    /**
     * @var boolean
     */
    protected $qualitycontrolrequire;

    /**
     * @var boolean
     */
    protected $task;

    /**
     * @var string
     */
    protected $feeder;

    /**
     * @var string
     */
    protected $carrierfeeder;

    /**
     * @var string
     */
    protected $drainer;

    /**
     * @var string
     */
    protected $carrierdrainer;

    /**
     * @var string
     */
    protected $goodsplanner;

    /**
     * @var integer
     */
    protected $productionmultiplier;

    /**
     * @var decimal
     */
    protected $intervalmultiplier;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var boolean
     */
    protected $delivery;

    /**
     * @var string
     */
    protected $locationsource;

    /**
     * @var string
     */
    protected $locationdestination;

    /**
     * @var integer
     */
    protected $empcount;

    /**
     * @var boolean
     */
    protected $isdefault;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var integer
     */
    protected $notificationtime;

    /**
     * @var integer
     */
    protected $lotcount;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent
     *
     * @param string $parent
     *
     * @return ProductTree
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ProductTree
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return ProductTree
     */
    public function setNumber($number)
    {
        $this->number = $number!==null ? intval($number) : null;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductTree
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set materialtype
     *
     * @param string $materialtype
     *
     * @return ProductTree
     */
    public function setMaterialtype($materialtype)
    {
        $this->materialtype = $materialtype;

        return $this;
    }

    /**
     * Get materialtype
     *
     * @return string
     */
    public function getMaterialtype()
    {
        return $this->materialtype;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductTree
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set stockcode
     *
     * @param string $stockcode
     *
     * @return ProductTree
     */
    public function setStockcode($stockcode)
    {
        $this->stockcode = $stockcode;

        return $this;
    }

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode()
    {
        return $this->stockcode;
    }

    /**
     * Set stockname
     *
     * @param string $stockname
     *
     * @return ProductTree
     */
    public function setStockname($stockname)
    {
        $this->stockname = $stockname;

        return $this;
    }

    /**
     * Get stockname
     *
     * @return string
     */
    public function getStockname()
    {
        return $this->stockname;
    }

    /**
     * Set tpp
     *
     * @param decimal $tpp
     *
     * @return ProductTree
     */
    public function setTpp($tpp)
    {
        $this->tpp = $tpp;

        return $this;
    }

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp()
    {
        return $this->tpp;
    }

    /**
     * Set quantity
     *
     * @param decimal $quantity
     *
     * @return ProductTree
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return decimal
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unit_quantity
     *
     * @param string $unit_quantity
     *
     * @return ProductTree
     */
    public function setUnit_quantity($unit_quantity)
    {
        $this->unit_quantity = $unit_quantity;

        return $this;
    }

    /**
     * Get unit_quantity
     *
     * @return string
     */
    public function getUnit_quantity()
    {
        return $this->unit_quantity;
    }

    /**
     * Set opcounter
     *
     * @param integer $opcounter
     *
     * @return ProductTree
     */
    public function setOpcounter($opcounter)
    {
        $this->opcounter = $opcounter!==null ? intval($opcounter) : null;

        return $this;
    }

    /**
     * Get opcounter
     *
     * @return integer
     */
    public function getOpcounter()
    {
        return $this->opcounter;
    }

    /**
     * Set pack
     *
     * @param string $pack
     *
     * @return ProductTree
     */
    public function setPack($pack)
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * Get pack
     *
     * @return string
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * Set packcapacity
     *
     * @param decimal $packcapacity
     *
     * @return ProductTree
     */
    public function setPackcapacity($packcapacity)
    {
        $this->packcapacity = $packcapacity;

        return $this;
    }

    /**
     * Get packcapacity
     *
     * @return decimal
     */
    public function getPackcapacity()
    {
        return $this->packcapacity;
    }

    /**
     * Set unit_packcapacity
     *
     * @param string $unit_packcapacity
     *
     * @return ProductTree
     */
    public function setUnit_packcapacity($unit_packcapacity)
    {
        $this->unit_packcapacity = $unit_packcapacity;

        return $this;
    }

    /**
     * Get unit_packcapacity
     *
     * @return string
     */
    public function getUnit_packcapacity()
    {
        return $this->unit_packcapacity;
    }

    /**
     * Set oporder
     *
     * @param integer $oporder
     *
     * @return ProductTree
     */
    public function setOporder($oporder)
    {
        $this->oporder = $oporder!==null ? intval($oporder) : null;

        return $this;
    }

    /**
     * Get oporder
     *
     * @return integer
     */
    public function getOporder()
    {
        return $this->oporder;
    }

    /**
     * Set preptime
     *
     * @param integer $preptime
     *
     * @return ProductTree
     */
    public function setPreptime($preptime)
    {
        $this->preptime = $preptime!==null ? intval($preptime) : null;

        return $this;
    }

    /**
     * Get preptime
     *
     * @return integer
     */
    public function getPreptime()
    {
        return $this->preptime;
    }

    /**
     * Set qualitycontrolrequire
     *
     * @param boolean $qualitycontrolrequire
     *
     * @return ProductTree
     */
    public function setQualitycontrolrequire($qualitycontrolrequire)
    {
        $this->qualitycontrolrequire = $qualitycontrolrequire;

        return $this;
    }

    /**
     * Get qualitycontrolrequire
     *
     * @return boolean
     */
    public function getQualitycontrolrequire()
    {
        return $this->qualitycontrolrequire;
    }

    /**
     * Set task
     *
     * @param boolean $task
     *
     * @return ProductTree
     */
    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return boolean
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set feeder
     *
     * @param string $feeder
     *
     * @return ProductTree
     */
    public function setFeeder($feeder)
    {
        $this->feeder = $feeder;

        return $this;
    }

    /**
     * Get feeder
     *
     * @return string
     */
    public function getFeeder()
    {
        return $this->feeder;
    }

    /**
     * Set carrierfeeder
     *
     * @param string $carrierfeeder
     *
     * @return ProductTree
     */
    public function setCarrierfeeder($carrierfeeder)
    {
        $this->carrierfeeder = $carrierfeeder;

        return $this;
    }

    /**
     * Get carrierfeeder
     *
     * @return string
     */
    public function getCarrierfeeder()
    {
        return $this->carrierfeeder;
    }

    /**
     * Set drainer
     *
     * @param string $drainer
     *
     * @return ProductTree
     */
    public function setDrainer($drainer)
    {
        $this->drainer = $drainer;

        return $this;
    }

    /**
     * Get drainer
     *
     * @return string
     */
    public function getDrainer()
    {
        return $this->drainer;
    }

    /**
     * Set carrierdrainer
     *
     * @param string $carrierdrainer
     *
     * @return ProductTree
     */
    public function setCarrierdrainer($carrierdrainer)
    {
        $this->carrierdrainer = $carrierdrainer;

        return $this;
    }

    /**
     * Get carrierdrainer
     *
     * @return string
     */
    public function getCarrierdrainer()
    {
        return $this->carrierdrainer;
    }

    /**
     * Set goodsplanner
     *
     * @param string $goodsplanner
     *
     * @return ProductTree
     */
    public function setGoodsplanner($goodsplanner)
    {
        $this->goodsplanner = $goodsplanner;

        return $this;
    }

    /**
     * Get goodsplanner
     *
     * @return string
     */
    public function getGoodsplanner()
    {
        return $this->goodsplanner;
    }

    /**
     * Set productionmultiplier
     *
     * @param integer $productionmultiplier
     *
     * @return ProductTree
     */
    public function setProductionmultiplier($productionmultiplier)
    {
        $this->productionmultiplier = $productionmultiplier!==null ? intval($productionmultiplier) : null;

        return $this;
    }

    /**
     * Get productionmultiplier
     *
     * @return integer
     */
    public function getProductionmultiplier()
    {
        return $this->productionmultiplier;
    }

    /**
     * Set intervalmultiplier
     *
     * @param decimal $intervalmultiplier
     *
     * @return ProductTree
     */
    public function setIntervalmultiplier($intervalmultiplier)
    {
        $this->intervalmultiplier = $intervalmultiplier!==null ? ($intervalmultiplier) : null;

        return $this;
    }

    /**
     * Get intervalmultiplier
     *
     * @return decimal
     */
    public function getIntervalmultiplier()
    {
        return $this->intervalmultiplier;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ProductTree
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ProductTree
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set delivery
     *
     * @param boolean $delivery
     *
     * @return ProductTree
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return boolean
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Get the value of locationsource
     *
     * @return  string
     */
    public function getLocationsource()
    {
        return $this->locationsource;
    }

    /**
     * Set the value of locationsource
     *
     * @param  string  $locationsource
     *
     * @return ProductTree
     */
    public function setLocationsource($locationsource)
    {
        $this->locationsource = $locationsource;

        return $this;
    }

    /**
     * Get the value of locationdestination
     *
     * @return  string
     */
    public function getLocationdestination()
    {
        return $this->locationdestination;
    }

    /**
     * Set the value of locationdestination
     *
     * @param  string  $locationdestination
     *
     * @return ProductTree
     */
    public function setLocationdestination($locationdestination)
    {
        $this->locationdestination = $locationdestination;

        return $this;
    }

    /**
     * Set empcount
     *
     * @param integer $empcount
     *
     * @return ProductTree
     */
    public function setEmpcount($empcount)
    {
        $this->empcount = $empcount!==null ? intval($empcount) : null;

        return $this;
    }

    /**
     * Get empcount
     *
     * @return integer
     */
    public function getEmpcount()
    {
        return $this->empcount;
    }

    /**
     * Set isdefault
     *
     * @param boolean $isdefault
     *
     * @return ProductTree
     */
    public function setIsdefault($isdefault)
    {
        $this->isdefault = $isdefault;

        return $this;
    }

    /**
     * Get isdefault
     *
     * @return boolean
     */
    public function getIsdefault()
    {
        return $this->isdefault;
    }

    /**
     * Get the value of client
     *
     * @return  string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set the value of client
     *
     * @param  string  $client
     *
     * @return ProductTree
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Set notificationtime
     *
     * @param integer $notificationtime
     *
     * @return ProductTree
     */
    public function setNotificationtime($notificationtime)
    {
        $this->notificationtime = $notificationtime!==null ? intval($notificationtime) : null;

        return $this;
    }

    /**
     * Get notificationtime
     *
     * @return integer
     */
    public function getNotificationtime()
    {
        return $this->notificationtime;
    }

    /**
     * Set lotcount
     *
     * @param integer $lotcount
     *
     * @return ProductTree
     */
    public function setLotcount($lotcount)
    {
        $this->lotcount = $lotcount!==null ? intval($lotcount) : null;

        return $this;
    }

    /**
     * Get lotcount
     *
     * @return integer
     */
    public function getLotcount()
    {
        return $this->lotcount;
    }
}
