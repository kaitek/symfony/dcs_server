<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * LostGroupDetail
 */
abstract class LostGroupDetail extends Base implements BaseInterface, LostGroupDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $lostgroup;

    /**
     * @var string
     */
    protected $lostgroupcode;

    /**
     * @var string
     */
    protected $losttype;

    /**
     * @var string
     */
    protected $color;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lostgroup
     *
     * @param string $lostgroup
     *
     * @return LostGroupDetail
     */
    public function setLostgroup($lostgroup)
    {
        $this->lostgroup = $lostgroup;

        return $this;
    }

    /**
     * Get lostgroup
     *
     * @return string
     */
    public function getLostgroup()
    {
        return $this->lostgroup;
    }

    /**
     * Set lostgroupcode
     *
     * @param string $lostgroupcode
     *
     * @return LostGroupDetail
     */
    public function setLostgroupcode($lostgroupcode)
    {
        $this->lostgroupcode = $lostgroupcode;

        return $this;
    }

    /**
     * Get lostgroupcode
     *
     * @return string
     */
    public function getLostgroupcode()
    {
        return $this->lostgroupcode;
    }

    /**
     * Set losttype
     *
     * @param string $losttype
     *
     * @return LostGroupDetail
     */
    public function setLosttype($losttype)
    {
        $this->losttype = $losttype;

        return $this;
    }

    /**
     * Get losttype
     *
     * @return string
     */
    public function getLosttype()
    {
        return $this->losttype;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return LostGroupDetail
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}

