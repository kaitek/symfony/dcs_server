<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * ClientLostDetail
 */
abstract class ClientLostDetail extends BaseSync implements BaseSyncInterface, ClientLostDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $losttype;

    /**
     * @var string
     */
    protected $faulttype;

    /**
     * @var string
     */
    protected $faultdescription;

    /**
     * @var string
     */
    protected $faultgroupcode;

    /**
     * @var string
     */
    protected $faultdevice;

    /**
     * @var string
     */
    protected $descriptionlost;

    /**
     * @var string
     */
    protected $tasklist;

    /**
     * @var string
     */
    protected $mould;

    /**
     * @var string
     */
    protected $mouldgroup;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $opdescription;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var bool
     */
    protected $taskfromerp;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var decimal
     */
    protected $energy;

    /**
     * @var string
     */
    protected $sourceclient;

    /**
     * @var string
     */
    protected $sourcelosttype;

    /**
     * @var string
     */
    protected $sourcedescriptionlost;

    /**
     * @var bool
     */
    protected $isexported;

    /**
     * @var integer
     */
    protected $opsayi;

    /**
     * @var integer
     */
    protected $refsayi;

    /**
     * @var string
     */
    protected $serialnumber;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ClientLostDetail
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientLostDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return ClientLostDetail
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return ClientLostDetail
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set losttype
     *
     * @param string $losttype
     *
     * @return ClientLostDetail
     */
    public function setLosttype($losttype)
    {
        $this->losttype = $losttype;

        return $this;
    }

    /**
     * Get losttype
     *
     * @return string
     */
    public function getLosttype()
    {
        return $this->losttype;
    }

    /**
     * Set tasklist
     *
     * @param string $tasklist
     *
     * @return ClientLostDetail
     */
    public function setTasklist($tasklist)
    {
        $this->tasklist = $tasklist;

        return $this;
    }

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist()
    {
        return $this->tasklist;
    }

    /**
     * Set faulttype
     *
     * @param string $faulttype
     *
     * @return ClientLostDetail
     */
    public function setFaulttype($faulttype)
    {
        $this->faulttype = $faulttype;
    }

    /**
     * Get faulttype
     *
     * @return string
     */
    public function getFaulttype()
    {
        return $this->faulttype;
    }

    /**
     * Set faultdescription
     *
     * @param string $faultdescription
     *
     * @return ClientLostDetail
     */
    public function setFaultdescription($faultdescription)
    {
        $this->faultdescription = $faultdescription;
    }

    /**
     * Get faultdescription
     *
     * @return string
     */
    public function getFaultdescription()
    {
        return $this->faultdescription;
    }

    /**
     * Set faultgroupcode
     *
     * @param string $faultgroupcode
     *
     * @return ClientLostDetail
     */
    public function setFaultgroupcode($faultgroupcode)
    {
        $this->faultgroupcode = $faultgroupcode;
    }

    /**
     * Get faultgroupcode
     *
     * @return string
     */
    public function getFaultgroupcode()
    {
        return $this->faultgroupcode;
    }

    /**
     * Set faultdevice
     *
     * @param string $faultdevice
     *
     * @return ClientLostDetail
     */
    public function setFaultdevice($faultdevice)
    {
        $this->faultdevice = $faultdevice;
    }

    /**
     * Get faultdevice
     *
     * @return string
     */
    public function getFaultdevice()
    {
        return $this->faultdevice;
    }

    /**
     * Set descriptionlost
     *
     * @param string $descriptionlost
     *
     * @return ClientLostDetail
     */
    public function setDescriptionLost($descriptionlost)
    {
        $this->descriptionlost = $descriptionlost;

        return $this;
    }

    /**
     * Get descriptionlost
     *
     * @return string
     */
    public function getDescriptionLost()
    {
        return $this->descriptionlost;
    }

    /**
     * Set mould
     *
     * @param string $mould
     *
     * @return ClientLostDetail
     */
    public function setMould($mould)
    {
        $this->mould = $mould;

        return $this;
    }

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould()
    {
        return $this->mould;
    }

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     *
     * @return ClientLostDetail
     */
    public function setMouldgroup($mouldgroup)
    {
        $this->mouldgroup = $mouldgroup;

        return $this;
    }

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup()
    {
        return $this->mouldgroup;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return ClientLostDetail
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return ClientLostDetail
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ClientLostDetail
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return ClientLostDetail
     */
    public function setOpdescription($opdescription)
    {
        $this->opdescription = $opdescription;
    }

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription()
    {
        return $this->opdescription;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return ClientLostDetail
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ClientLostDetail
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;
    }

    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return ClientLostDetail
     */
    public function setTaskfromerp($taskfromerp)
    {
        $this->taskfromerp = $taskfromerp;

        return $this;
    }

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp()
    {
        return $this->taskfromerp;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientLostDetail
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientLostDetail
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set energy
     *
     * @param decimal $energy
     *
     * @return ClientLostDetail
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;
    }

    /**
     * Get energy
     *
     * @return decimal
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set sourceclient
     *
     * @param string $sourceclient
     *
     * @return ClientLostDetail
     */
    public function setSourceclient($sourceclient)
    {
        $this->sourceclient = $sourceclient;

        return $this;
    }

    /**
     * Get sourceclient
     *
     * @return string
     */
    public function getSourceclient()
    {
        return $this->sourceclient;
    }

    /**
     * Set sourcelosttype
     *
     * @param string $sourcelosttype
     *
     * @return ClientLostDetail
     */
    public function setSourcelosttype($sourcelosttype)
    {
        $this->sourcelosttype = $sourcelosttype;

        return $this;
    }

    /**
     * Get sourcelosttype
     *
     * @return string
     */
    public function getSourcelosttype()
    {
        return $this->sourcelosttype;
    }

    /**
     * Set sourcedescriptionlost
     *
     * @param string $sourcedescriptionlost
     *
     * @return ClientLostDetail
     */
    public function setSourcedescriptionlost($sourcedescriptionlost)
    {
        $this->sourcedescriptionlost = $sourcedescriptionlost;

        return $this;
    }

    /**
     * Get sourcedescriptionlost
     *
     * @return string
     */
    public function getSourcedescriptionlost()
    {
        return $this->sourcedescriptionlost;
    }

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ClientLostDetail
     */
    public function setIsexported($isexported)
    {
        $this->isexported = $isexported;

        return $this;
    }

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported()
    {
        return $this->isexported;
    }

    /**
     * Set opsayi
     *
     * @param integer $opsayi
     *
     * @return ClientLostDetail
     */
    public function setOpsayi($opsayi)
    {
        $this->opsayi = $opsayi;

        return $this;
    }

    /**
     * Get opsayi
     *
     * @return integer
     */
    public function getOpsayi()
    {
        return $this->opsayi;
    }

    /**
     * Set refsayi
     *
     * @param integer $refsayi
     *
     * @return ClientLostDetail
     */
    public function setRefsayi($refsayi)
    {
        $this->refsayi = $refsayi;

        return $this;
    }

    /**
     * Get refsayi
     *
     * @return integer
     */
    public function getRefsayi()
    {
        return $this->refsayi;
    }

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return ClientLostDetail
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;
    }

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }
}
