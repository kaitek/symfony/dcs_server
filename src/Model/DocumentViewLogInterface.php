<?php
namespace App\Model;

/**
 * DocumentViewLogInterface
 */
interface DocumentViewLogInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DocumentViewLogInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set path
     *
     * @param string $path
     *
     * @return DocumentViewLogInterface
     */
    public function setPath($path);

    /**
     * Get path
     *
     * @return string
     */
    public function getPath();

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return DocumentViewLogInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return DocumentViewLogInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return DocumentViewLogInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set currentversion
     *
     * @param integer $currentversion
     *
     * @return DocumentViewLogInterface
     */
    public function setCurrentversion($currentversion);

    /**
     * Get currentversion
     *
     * @return integer
     */
    public function getCurrentversion();

    /**
     * Set client
     *
     * @param string $client
     *
     * @return DocumentViewLogInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return DocumentViewLogInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return DocumentViewLogInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return DocumentViewLogInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return DocumentViewLogInterface
     */
    public function setErprefnumber($erprefnumber);
    
    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return DocumentViewLogInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime();
}
