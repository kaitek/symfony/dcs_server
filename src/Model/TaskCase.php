<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * TaskCase
 */
abstract class TaskCase extends BaseSync implements BaseSyncInterface, TaskCaseInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var integer
     */
    protected $movementdetail;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $casetype;
    
    /**
     * @var string
     */
    protected $casenumber;
        
    /**
     * @var string
     */
    protected $caselot;
    
    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $stockcode;

    /**
     * @var string
     */
    protected $stockname;

    /**
     * @var decimal
     */
    protected $quantityused;

    /**
     * @var string
     */
    protected $casename;

    /**
     * @var decimal
     */
    protected $packcapacity;

    /**
     * @var string
     */
    protected $feeder;

    /**
     * @var integer
     */
    protected $preptime;

    /**
     * @var decimal
     */
    protected $quantityremaining;

    /**
     * @var decimal
     */
    protected $quantitydeliver;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var \DateTime
     */
    protected $deliverytime;

    /**
     * @var string
     */
    protected $finishtype;

    /**
     * @var integer
     */
    protected $conveyor;

    /**
     * @var string
     */
    protected $goodsplanner;

    /**
     * @var string
     */
    protected $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return TaskCase
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set movementdetail
     *
     * @param integer $movementdetail
     *
     * @return TaskCase
     */
    public function setMovementdetail($movementdetail)
    {
        $this->movementdetail = $movementdetail;

        return $this;
    }

    /**
     * Get movementdetail
     *
     * @return integer
     */
    public function getMovementdetail()
    {
        return $this->movementdetail;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return TaskCase
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set casetype
     *
     * @param string $casetype
     *
     * @return TaskCase
     */
    public function setCasetype($casetype)
    {
        $this->casetype = $casetype;

        return $this;
    }

    /**
     * Get casetype
     *
     * @return string
     */
    public function getCasetype()
    {
        return $this->casetype;
    }
    
    /**
     * Set casenumber
     *
     * @param string $casenumber
     *
     * @return TaskCase
     */
    public function setCasenumber($casenumber)
    {
        $this->casenumber = $casenumber;

        return $this;
    }

    /**
     * Get casenumber
     *
     * @return string
     */
    public function getCasenumber()
    {
        return $this->casenumber;
    }

    /**
     * Set caselot
     *
     * @param string $caselot
     *
     * @return TaskCase
     */
    public function setCaselot($caselot)
    {
        $this->caselot = $caselot;

        return $this;
    }

    /**
     * Get caselot
     *
     * @return string
     */
    public function getCaselot()
    {
        return $this->caselot;
    }
    
    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return TaskCase
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return TaskCase
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskCase
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set stockcode
     *
     * @param string $stockcode
     *
     * @return TaskCase
     */
    public function setStockcode($stockcode)
    {
        $this->stockcode = $stockcode;

        return $this;
    }

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode()
    {
        return $this->stockcode;
    }

    /**
     * Set stockname
     *
     * @param string $stockname
     *
     * @return TaskCase
     */
    public function setStockname($stockname)
    {
        $this->stockname = $stockname;

        return $this;
    }

    /**
     * Get stockname
     *
     * @return string
     */
    public function getStockname()
    {
        return $this->stockname;
    }

    /**
     * Set quantityused
     *
     * @param decimal $quantityused
     *
     * @return TaskCase
     */
    public function setQuantityused($quantityused)
    {
        $this->quantityused = $quantityused;

        return $this;
    }

    /**
     * Get quantityused
     *
     * @return decimal
     */
    public function getQuantityused()
    {
        return $this->quantityused;
    }

    /**
     * Set casename
     *
     * @param string $casename
     *
     * @return TaskCase
     */
    public function setCasename($casename)
    {
        $this->casename = $casename;

        return $this;
    }

    /**
     * Get casename
     *
     * @return string
     */
    public function getCasename()
    {
        return $this->casename;
    }

    /**
     * Set packcapacity
     *
     * @param decimal $packcapacity
     *
     * @return TaskCase
     */
    public function setPackcapacity($packcapacity)
    {
        $this->packcapacity = $packcapacity;

        return $this;
    }

    /**
     * Get packcapacity
     *
     * @return decimal
     */
    public function getPackcapacity()
    {
        return $this->packcapacity;
    }

    /**
     * Set feeder
     *
     * @param string $feeder
     *
     * @return TaskCase
     */
    public function setFeeder($feeder)
    {
        $this->feeder = $feeder;

        return $this;
    }

    /**
     * Get feeder
     *
     * @return string
     */
    public function getFeeder()
    {
        return $this->feeder;
    }

    /**
     * Set preptime
     *
     * @param integer $preptime
     *
     * @return TaskCase
     */
    public function setPreptime($preptime)
    {
        $this->preptime = $preptime;

        return $this;
    }

    /**
     * Get preptime
     *
     * @return integer
     */
    public function getPreptime()
    {
        return $this->preptime;
    }

    /**
     * Set quantityremaining
     *
     * @param decimal $quantityremaining
     *
     * @return TaskCase
     */
    public function setQuantityremaining($quantityremaining)
    {
        $this->quantityremaining = $quantityremaining;

        return $this;
    }

    /**
     * Get quantityremaining
     *
     * @return decimal
     */
    public function getQuantityremaining()
    {
        return $this->quantityremaining;
    }

    /**
     * Set quantitydeliver
     *
     * @param decimal $quantitydeliver
     *
     * @return TaskCase
     */
    public function setQuantitydeliver($quantitydeliver)
    {
        $this->quantitydeliver = $quantitydeliver;

        return $this;
    }

    /**
     * Get quantitydeliver
     *
     * @return decimal
     */
    public function getQuantitydeliver()
    {
        return $this->quantitydeliver;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return TaskCase
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return TaskCase
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set deliverytime
     *
     * @param \DateTime $deliverytime
     *
     * @return TaskCase
     */
    public function setDeliverytime($deliverytime)
    {
        $this->deliverytime = $deliverytime;

        return $this;
    }

    /**
     * Get deliverytime
     *
     * @return \DateTime
     */
    public function getDeliverytime()
    {
        return $this->deliverytime;
    }

    /**
     * Set finishtype
     *
     * @param string $finishtype
     *
     * @return TaskCase
     */
    public function setFinishtype($finishtype)
    {
        $this->finishtype = $finishtype;

        return $this;
    }

    /**
     * Get finishtype
     *
     * @return string
     */
    public function getFinishtype()
    {
        return $this->finishtype;
    }

    /**
     * Set conveyor
     *
     * @param integer $conveyor
     *
     * @return TaskCase
     */
    public function setConveyor($conveyor)
    {
        $this->conveyor = $conveyor;

        return $this;
    }

    /**
     * Get conveyor
     *
     * @return integer
     */
    public function getConveyor()
    {
        return $this->conveyor;
    }

    /**
     * Set goodsplanner
     *
     * @param string $goodsplanner
     *
     * @return TaskCase
     */
    public function setGoodsplanner($goodsplanner)
    {
        $this->goodsplanner = $goodsplanner;

        return $this;
    }

    /**
     * Get goodsplanner
     *
     * @return string
     */
    public function getGoodsplanner()
    {
        return $this->goodsplanner;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskCase
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

}

