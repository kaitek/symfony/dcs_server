<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * ClientProductionDetail
 */
abstract class ClientProductionDetail extends BaseSync implements BaseSyncInterface, ClientProductionDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $tasklist;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $mould;

    /**
     * @var string
     */
    protected $mouldgroup;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $opdescription;

    /**
     * @var string
     */
    protected $leafmask;

    /**
     * @var string
     */
    protected $leafmaskcurrent;

    /**
     * @var integer
     */
    protected $production;

    /**
     * @var integer
     */
    protected $productioncurrent;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var integer
     */
    protected $gap;

    /**
     * @var bool
     */
    protected $taskfromerp;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var decimal
     */
    protected $energy;

    /**
     * @var string
     */
    protected $taskfinishtype;

    /**
     * @var string
     */
    protected $taskfinishdescription;

    /**
     * @var decimal
      */
    protected $calculatedtpp;

    /**
     * @var bool
     */
    protected $isexported;

    /**
     * @var integer
     */
    protected $opsayi;

    /**
     * @var integer
     */
    protected $refsayi;

    /**
     * @var integer
     */
    protected $losttime;

    /**
     * @var string
     */
    protected $serialnumber;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ClientProductionDetail
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientProductionDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return ClientProductionDetail
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return ClientProductionDetail
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set tasklist
     *
     * @param string $tasklist
     *
     * @return ClientProductionDetail
     */
    public function setTasklist($tasklist)
    {
        $this->tasklist = $tasklist;

        return $this;
    }

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist()
    {
        return $this->tasklist;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return ClientProductionDetail
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set mould
     *
     * @param string $mould
     *
     * @return ClientProductionDetail
     */
    public function setMould($mould)
    {
        $this->mould = $mould;

        return $this;
    }

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould()
    {
        return $this->mould;
    }

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     *
     * @return ClientProductionDetail
     */
    public function setMouldgroup($mouldgroup)
    {
        $this->mouldgroup = $mouldgroup;

        return $this;
    }

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup()
    {
        return $this->mouldgroup;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return ClientProductionDetail
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return ClientProductionDetail
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ClientProductionDetail
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return ClientProductionDetail
     */
    public function setOpdescription($opdescription)
    {
        $this->opdescription = $opdescription;
    }

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription()
    {
        return $this->opdescription;
    }

    /**
     * Set leafmask
     *
     * @param string $leafmask
     *
     * @return ClientProductionDetail
     */
    public function setLeafmask($leafmask)
    {
        $this->leafmask = $leafmask;

        return $this;
    }

    /**
     * Get leafmask
     *
     * @return string
     */
    public function getLeafmask()
    {
        return $this->leafmask;
    }

    /**
     * Set leafmaskcurrent
     *
     * @param string $leafmaskcurrent
     *
     * @return ClientProductionDetail
     */
    public function setLeafmaskcurrent($leafmaskcurrent)
    {
        $this->leafmaskcurrent = $leafmaskcurrent;

        return $this;
    }

    /**
     * Get leafmaskcurrent
     *
     * @return string
     */
    public function getLeafmaskcurrent()
    {
        return $this->leafmaskcurrent;
    }

    /**
     * Set production
     *
     * @param integer $production
     *
     * @return ClientProductionDetail
     */
    public function setProduction($production)
    {
        $this->production = $production;

        return $this;
    }

    /**
     * Get production
     *
     * @return integer
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * Set productioncurrent
     *
     * @param integer $productioncurrent
     *
     * @return ClientProductionDetail
     */
    public function setProductioncurrent($productioncurrent)
    {
        $this->productioncurrent = $productioncurrent;

        return $this;
    }

    /**
     * Get productioncurrent
     *
     * @return integer
     */
    public function getProductioncurrent()
    {
        return $this->productioncurrent;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return ClientProductionDetail
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ClientProductionDetail
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;
    }

    /**
     * Set gap
     *
     * @param integer $gap
     *
     * @return ClientProductionDetail
     */
    public function setGap($gap)
    {
        $this->gap = $gap;

        return $this;
    }

    /**
     * Get gap
     *
     * @return int
     */
    public function getGap()
    {
        return $this->gap;
    }

    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return ClientProductionDetail
     */
    public function setTaskfromerp($taskfromerp)
    {
        $this->taskfromerp = $taskfromerp;

        return $this;
    }

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp()
    {
        return $this->taskfromerp;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientProductionDetail
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientProductionDetail
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set energy
     *
     * @param decimal $energy
     *
     * @return ClientProductionDetail
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;
    }

    /**
     * Get energy
     *
     * @return decimal
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set taskfinishtype
     *
     * @param string $taskfinishtype
     *
     * @return ClientProductionDetail
     */
    public function setTaskfinishtype($taskfinishtype)
    {
        $this->taskfinishtype = $taskfinishtype;
    }

    /**
     * Get taskfinishtype
     *
     * @return string
     */
    public function getTaskfinishtype()
    {
        return $this->taskfinishtype;
    }

    /**
     * Set taskfinishdescription
     *
     * @param string $taskfinishdescription
     *
     * @return ClientProductionDetail
     */
    public function setTaskfinishdescription($taskfinishdescription)
    {
        $this->taskfinishdescription = $taskfinishdescription;
    }

    /**
     * Get taskfinishdescription
     *
     * @return string
     */
    public function getTaskfinishdescription()
    {
        return $this->taskfinishdescription;
    }

    /**
     * Set calculatedtpp
     *
     * @param decimal $calculatedtpp
     *
     * @return ClientProductionDetail
     */
    public function setCalculatedtpp($calculatedtpp)
    {
        $this->calculatedtpp = $calculatedtpp;
    }

    /**
     * Get calculatedtpp
     *
     * @return decimal
     */
    public function getCalculatedtpp()
    {
        return $this->calculatedtpp;
    }

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ClientProductionDetail
     */
    public function setIsexported($isexported)
    {
        $this->isexported = $isexported;

        return $this;
    }

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported()
    {
        return $this->isexported;
    }

    /**
     * Set opsayi
     *
     * @param integer $opsayi
     *
     * @return ClientProductionDetail
     */
    public function setOpsayi($opsayi)
    {
        $this->opsayi = $opsayi;

        return $this;
    }

    /**
     * Get opsayi
     *
     * @return integer
     */
    public function getOpsayi()
    {
        return $this->opsayi;
    }

    /**
     * Set refsayi
     *
     * @param integer $refsayi
     *
     * @return ClientProductionDetail
     */
    public function setRefsayi($refsayi)
    {
        $this->refsayi = $refsayi;

        return $this;
    }

    /**
     * Get refsayi
     *
     * @return integer
     */
    public function getRefsayi()
    {
        return $this->refsayi;
    }

    /**
     * Set losttime
     *
     * @param integer $losttime
     *
     * @return ClientProductionDetail
     */
    public function setLosttime($losttime)
    {
        $this->losttime = $losttime;

        return $this;
    }

    /**
     * Get losttime
     *
     * @return integer
     */
    public function getLosttime()
    {
        return $this->losttime;
    }

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return ClientProductionDetail
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;
    }

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }
}
