<?php
namespace App\Model;

/**
 * ClientOEEDetailInterface
 */
interface ClientOEEDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientOEEDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ClientOEEDetailInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return ClientOEEDetailInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set kul
     *
     * @param string $kul
     * @return ClientOEEDetailInterface
     */
    public function setKul($kul);

    /**
     * Get kul
     *
     * @return string 
     */
    public function getKul();

    /**
     * Set per
     *
     * @param string $per
     * @return ClientOEEDetailInterface
     */
    public function setPer($per);

    /**
     * Get per
     *
     * @return string 
     */
    public function getPer();

    /**
     * Set kal
     *
     * @param string $kal
     * @return ClientOEEDetailInterface
     */
    public function setKal($kal);

    /**
     * Get kal
     *
     * @return string 
     */
    public function getKal();

    /**
     * Set oee
     *
     * @param string $oee
     * @return ClientOEEDetailInterface
     */
    public function setOee($oee);

    /**
     * Get oee
     *
     * @return string 
     */
    public function getOee();
    
}
