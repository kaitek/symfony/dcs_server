<?php
namespace App\Model;

/**
 * TaskCaseControlInterface
 */
interface TaskCaseControlInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskCaseControlInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return TaskCaseControlInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set packcapacity
     *
     * @param decimal $packcapacity
     *
     * @return TaskCaseControlInterface
     */
    public function setPackcapacity($packcapacity);

    /**
     * Get packcapacity
     *
     * @return decimal
     */
    public function getPackcapacity();

    /**
     * Set quantityremaining
     *
     * @param decimal $quantityremaining
     *
     * @return TaskCaseControlInterface
     */
    public function setQuantityremaining($quantityremaining);

    /**
     * Get quantityremaining
     *
     * @return decimal
     */
    public function getQuantityremaining();

    /**
     * Set clients
     *
     * @param string $clients
     *
     * @return TaskCaseControlInterface
     */
    public function setClients($clients);

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients();

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return TaskCaseControlInterface
     */
    public function setFilename($filename);

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename();
    
}
