<?php
namespace App\Model;

/**
 * MouldDetailInterface
 */
interface MouldDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set mould
     *
     * @param string $mould
     * @return MouldDetailInterface
     */
    public function setMould($mould);

    /**
     * Get mould
     *
     * @return string 
     */
    public function getMould();

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     * @return MouldDetailInterface
     */
    public function setMouldgroup($mouldgroup);

    /**
     * Get mouldgroup
     *
     * @return string 
     */
    public function getMouldgroup();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return MouldDetailInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string 
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return MouldDetailInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer 
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return MouldDetailInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set leafmask
     *
     * @param string $leafmask
     * @return MouldDetailInterface
     */
    public function setLeafmask($leafmask);

    /**
     * Get leafmask
     *
     * @return string 
     */
    public function getLeafmask();

    /**
     * Get leafmaskcurrent
     *
     * @return string
     */
    function getLeafmaskcurrent();

    /**
     * Set leafmaskcurrent
     *
     * @param string $leafmaskcurrent
     *
     * @return MouldDetailInterface
     */
    function setLeafmaskcurrent($leafmaskcurrent);

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return MouldDetailInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return MouldDetailInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
}
