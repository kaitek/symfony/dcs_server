<?php
namespace App\Model;

/**
 * ClientDetailInterface
 */
interface ClientDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return ClientDetailInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set description
     *
     * @param string $description
     * @return ClientDetailInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set clienttype
     *
     * @param string $clienttype
     * @return ClientDetailInterface
     */
    public function setClienttype($clienttype);

    /**
     * Get clienttype
     *
     * @return string 
     */
    public function getClienttype();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set iotype
     *
     * @param string $iotype
     * @return ClientDetailInterface
     */
    public function setIotype($iotype);

    /**
     * Get iotype
     *
     * @return string 
     */
    public function getIotype();

    /**
     * Set portnumber
     *
     * @param integer $portnumber
     *
     * @return ClientDetailInterface
     */
    public function setPortnumber($portnumber);
    
    /**
     * Get portnumber
     *
     * @return integer
     */
    public function getPortnumber();
    
    /**
     * Set ioevent
     *
     * @param string $ioevent
     * @return ClientDetailInterface
     */
    public function setIOEvent($ioevent);

    /**
     * Get ioevent
     *
     * @return string 
     */
    public function getIOEvent();

    /**
     * Set triggertype
     *
     * @param string $triggertype
     *
     * @return ClientDetailInterface
     */
    public function setTriggertype($triggertype);

    /**
     * Get triggertype
     *
     * @return string
     */
    public function getTriggertype();
    
    /**
     * Set start
     *
     * @param \DateTime $start
     * @return ClientDetailInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return ClientDetailInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
}
