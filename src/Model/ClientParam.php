<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * ClientParam
 */
abstract class ClientParam extends BaseSync implements BaseSyncInterface, ClientParamInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $value;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientParam
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ClientParam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ClientParam
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
