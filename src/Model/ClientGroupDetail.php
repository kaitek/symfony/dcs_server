<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * ClientGroupDetail
 */
abstract class ClientGroupDetail extends Base implements BaseInterface, ClientGroupDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $clientgroup;

    /**
     * @var string
     */
    protected $clientgroupcode;

    /**
     * @var string
     */
    protected $client;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientgroup
     *
     * @param string $clientgroup
     *
     * @return ClientGroupDetail
     */
    public function setClientgroup($clientgroup)
    {
        $this->clientgroup = $clientgroup;

        return $this;
    }

    /**
     * Get clientgroup
     *
     * @return string
     */
    public function getClientgroup()
    {
        return $this->clientgroup;
    }

    /**
     * Set clientgroupcode
     *
     * @param string $clientgroupcode
     *
     * @return ClientGroupDetail
     */
    public function setClientgroupcode($clientgroupcode)
    {
        $this->clientgroupcode = $clientgroupcode;

        return $this;
    }

    /**
     * Get clientgroupcode
     *
     * @return string
     */
    public function getClientgroupcode()
    {
        return $this->clientgroupcode;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientGroupDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }
}

