<?php
namespace App\Model;

/**
 * ClientDisconnectInterface
 */
interface ClientDisconnectInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientDisconnectInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientDisconnectInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();
    
}
