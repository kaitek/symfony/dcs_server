<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * Overtime
 */
abstract class Overtime extends BaseSync implements BaseSyncInterface, OvertimeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $clients;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $repeattype;

    /**
     * @var string
     */
    protected $days;

    /**
     * @var \DateTime
     */
    protected $startday;

    /**
     * @var string
     */
    protected $starttime;

    /**
     * @var \DateTime
     */
    protected $finishday;

    /**
     * @var string
     */
    protected $finishtime;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clients
     *
     * @param string $clients
     *
     * @return Overtime
     */
    public function setClients($clients)
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Overtime
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set repeattype
     *
     * @param string $repeattype
     *
     * @return Overtime
     */
    public function setRepeattype($repeattype)
    {
        $this->repeattype = $repeattype;

        return $this;
    }

    /**
     * Get repeattype
     *
     * @return string
     */
    public function getRepeattype()
    {
        return $this->repeattype;
    }

    /**
     * Set days
     *
     * @param string $days
     *
     * @return Overtime
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return string
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Set startday
     *
     * @param \DateTime $startday
     *
     * @return Overtime
     */
    public function setStartday($startday)
    {
        $this->startday = $startday;

        return $this;
    }

    /**
     * Get startday
     *
     * @return \DateTime
     */
    public function getStartday()
    {
        return $this->startday;
    }

    /**
     * Set starttime
     *
     * @param string $starttime
     *
     * @return Overtime
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return string
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set finishday
     *
     * @param \DateTime $finishday
     *
     * @return Overtime
     */
    public function setFinishday($finishday)
    {
        $this->finishday = $finishday;

        return $this;
    }

    /**
     * Get finishday
     *
     * @return \DateTime
     */
    public function getFinishday()
    {
        return $this->finishday;
    }

    /**
     * Set finishtime
     *
     * @param string $finishtime
     *
     * @return Overtime
     */
    public function setFinishtime($finishtime)
    {
        $this->finishtime = $finishtime;

        return $this;
    }

    /**
     * Get finishtime
     *
     * @return string
     */
    public function getFinishtime()
    {
        return $this->finishtime;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Overtime
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return Overtime
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
}

