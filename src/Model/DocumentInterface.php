<?php
namespace App\Model;

/**
 * DocumentInterface
 */
interface DocumentInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DocumentInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set path
     *
     * @param string $path
     *
     * @return DocumentInterface
     */
    public function setPath($path);

    /**
     * Get path
     *
     * @return string
     */
    public function getPath();

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return DocumentInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return DocumentInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return DocumentInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set currentversion
     *
     * @param integer $currentversion
     *
     * @return DocumentInterface
     */
    public function setCurrentversion($currentversion);

    /**
     * Get currentversion
     *
     * @return integer
     */
    public function getCurrentversion();

    /**
     * Set description
     *
     * @param string $description
     *
     * @return DocumentInterface
     */
    public function setDescription($description);
    
    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set islot
     *
     * @param boolean $islot
     *
     * @return DocumentInterface
     */
    public function setIslot($islot);

    /**
     * Get islot
     *
     * @return boolean
     */
    public function getIslot();

    /**
     * Set lastupdate
     *
     * @param \DateTime $lastupdate
     *
     * @return DocumentInterface
     */
    public function setLastupdate($lastupdate);

    /**
     * Get lastupdate
     *
     * @return \DateTime
     */
    public function getLastupdate();
    
}
