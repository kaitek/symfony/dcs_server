<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * LostGroup
 */
abstract class LostGroup extends Base implements BaseInterface, LostGroupInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $groupcode;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return LostGroup
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
     /**
     * Get groupcode
     *
     * @return string
     */
    function getGroupcode() {
        return $this->groupcode;
    }

    /**
     * Set groupcode
     *
     * @param string $groupcode
     *
     * @return LostGroup
     */
    function setGroupcode($groupcode) {
        $this->groupcode = $groupcode;
    }
    
}

