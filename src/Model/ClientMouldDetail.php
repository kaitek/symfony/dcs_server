<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * ClientMouldDetail
 */
abstract class ClientMouldDetail extends BaseSync implements BaseSyncInterface, ClientMouldDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $clientmouldgroup;

    /**
     * @var string
     */
    protected $mouldgroup;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientMouldDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set clientmouldgroup
     *
     * @param string $clientmouldgroup
     *
     * @return ClientMouldDetail
     */
    public function setClientmouldgroup($clientmouldgroup)
    {
        $this->clientmouldgroup = $clientmouldgroup;

        return $this;
    }

    /**
     * Get clientmouldgroup
     *
     * @return string
     */
    public function getClientmouldgroup()
    {
        return $this->clientmouldgroup;
    }

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     *
     * @return ClientMouldDetail
     */
    public function setMouldgroup($mouldgroup)
    {
        $this->mouldgroup = $mouldgroup;

        return $this;
    }

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup()
    {
        return $this->mouldgroup;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientMouldDetail
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientMouldDetail
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
}

