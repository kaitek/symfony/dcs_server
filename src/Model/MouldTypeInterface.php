<?php
namespace App\Model;

/**
 * MouldTypeInterface
 */
interface MouldTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return MouldTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();
    
}
