<?php
namespace App\Model;

/**
 * CaseMovementDetailInterface
 */
interface CaseMovementDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set casemovementId
     *
     * @param integer $casemovementId
     * @return CaseMovementDetailInterface
     */
    public function setCasemovementId($casemovementId);

    /**
     * Get casemovementId
     *
     * @return integer 
     */
    public function getCasemovementId();

    /**
     * Set carrier
     *
     * @param string $carrier
     * @return CaseMovementDetailInterface
     */
    public function setCarrier($carrier);

    /**
     * Get carrier
     *
     * @return string 
     */
    public function getCarrier();

    /**
     * Set assignmenttime
     *
     * @param \DateTime $assignmenttime
     * @return CaseMovementDetailInterface
     */
    public function setAssignmenttime($assignmenttime);

    /**
     * Get assignmenttime
     *
     * @return \DateTime 
     */
    public function getAssignmenttime();

    /**
     * Set canceltime
     *
     * @param \DateTime $canceltime
     * @return CaseMovementDetailInterface
     */
    public function setCanceltime($canceltime);

    /**
     * Get canceltime
     *
     * @return \DateTime 
     */
    public function getCanceltime();

    /**
     * Set donetime
     *
     * @param \DateTime $donetime
     * @return CaseMovementDetailInterface
     */
    public function setDonetime($donetime);

    /**
     * Get donetime
     *
     * @return \DateTime 
     */
    public function getDonetime();

    /**
     * Set description
     *
     * @param string $description
     * @return CaseMovementDetailInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set empcode
     *
     * @param string $empcode
     * @return CaseMovementDetailInterface
     */
    public function setEmpcode($empcode);

    /**
     * Get empcode
     *
     * @return string 
     */
    public function getEmpcode();

    /**
     * Set empname
     *
     * @param string $empname
     * @return CaseMovementDetailInterface
     */
    public function setEmpname($empname);

    /**
     * Get empname
     *
     * @return string 
     */
    public function getEmpname();
    
}
