<?php

namespace App\Model;
use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * CaseChangeTime
 */
abstract class CaseChangeTime extends Base implements BaseInterface, CaseChangeTimeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $casename;

    /**
     * @var integer
     */
    protected $changetime;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set casename
     *
     * @param string $casename
     *
     * @return CaseChangeTime
     */
    public function setCasename($casename)
    {
        $this->casename = $casename;

        return $this;
    }

    /**
     * Get casename
     *
     * @return string
     */
    public function getCasename()
    {
        return $this->casename;
    }

    /**
     * Set changetime
     *
     * @param integer $changetime
     *
     * @return CaseChangeTime
     */
    public function setChangetime($changetime)
    {
        $this->changetime = $changetime;

        return $this;
    }

    /**
     * Get changetime
     *
     * @return integer
     */
    public function getChangetime()
    {
        return $this->changetime;
    }

}

