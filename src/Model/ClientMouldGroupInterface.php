<?php
namespace App\Model;

/**
 * ClientMouldGroupInterface
 */
interface ClientMouldGroupInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return ClientMouldGroupInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientMouldGroupInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set productionmultiplier
     *
     * @param integer $productionmultiplier
     * @return ClientMouldGroupInterface
     */
    public function setProductionmultiplier($productionmultiplier);

    /**
     * Get productionmultiplier
     *
     * @return integer 
     */
    public function getProductionmultiplier();

    /**
     * Set intervalmultiplier
     *
     * @param decimal $intervalmultiplier
     * @return ClientMouldGroupInterface
     */
    public function setIntervalmultiplier($intervalmultiplier);

    /**
     * Get intervalmultiplier
     *
     * @return decimal 
     */
    public function getIntervalmultiplier();

    /**
     * Set setup
     *
     * @param integer $setup
     * @return ClientMouldGroupInterface
     */
    public function setSetup($setup);

    /**
     * Get setup
     *
     * @return integer 
     */
    public function getSetup();

    /**
     * Set cycletime
     *
     * @param decimal $cycletime
     * @return ClientMouldGroupInterface
     */
    public function setCycletime($cycletime);

    /**
     * Get cycletime
     *
     * @return decimal 
     */
    public function getCycletime();

    /**
     * Set lotcount
     *
     * @param integer $lotcount
     * @return ClientMouldGroupInterface
     */
    public function setLotcount($lotcount);

    /**
     * Get lotcount
     *
     * @return integer 
     */
    public function getLotcount();

    /**
     * Set operatorcount
     *
     * @param integer $operatorcount
     *
     * @return ClientMouldGroupInterface
     */
    public function setOperatorcount($operatorcount);

    /**
     * Get operatorcount
     *
     * @return integer
     */
    public function getOperatorcount();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return ClientMouldGroupInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return ClientMouldGroupInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    /**
     * Set isdefault
     *
     * @param boolean $isdefault
     *
     * @return ClientMouldGroupInterface
     */
    public function setIsdefault($isdefault);
    
    /**
     * Get isdefault
     *
     * @return boolean
     */
    public function getIsdefault();
    
}
