<?php
namespace App\Model;

/**
 * CaseLocationInterface
 */
interface CaseLocationInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set stockcode
     *
     * @param string $stockcode
     * @return CaseLocationInterface
     */
    public function setStockcode($stockcode);

    /**
     * Get stockcode
     *
     * @return string 
     */
    public function getStockcode();

    /**
     * Set location
     *
     * @param string $location
     * @return CaseLocationInterface
     */
    public function setLocation($location);

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation();

}
