<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * ClientMouldGroup
 */
abstract class ClientMouldGroup extends BaseSync implements BaseSyncInterface, ClientMouldGroupInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var integer
     */
    protected $productionmultiplier;

    /**
     * @var decimal
     */
    protected $intervalmultiplier;

    /**
     * @var integer
     */
    protected $setup;

    /**
     * @var decimal
     */
    protected $cycletime;

    /**
     * @var integer
     */
    protected $lotcount;
    
    /**
     * @var integer
     */
    protected $operatorcount;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var bool
     */
    protected $isdefault=false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ClientMouldGroup
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientMouldGroup
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set productionmultiplier
     *
     * @param integer $productionmultiplier
     *
     * @return ClientMouldGroup
     */
    public function setProductionmultiplier($productionmultiplier)
    {
        $this->productionmultiplier = $productionmultiplier!==null?intval($productionmultiplier):null;

        return $this;
    }

    /**
     * Get productionmultiplier
     *
     * @return integer
     */
    public function getProductionmultiplier()
    {
        return $this->productionmultiplier;
    }

    /**
     * Set intervalmultiplier
     *
     * @param decimal $intervalmultiplier
     *
     * @return ClientMouldGroup
     */
    public function setIntervalmultiplier($intervalmultiplier)
    {
        $this->intervalmultiplier = $intervalmultiplier!==null?($intervalmultiplier):null;

        return $this;
    }

    /**
     * Get intervalmultiplier
     *
     * @return decimal
     */
    public function getIntervalmultiplier()
    {
        return $this->intervalmultiplier;
    }

    /**
     * Set setup
     *
     * @param integer $setup
     *
     * @return ClientMouldGroup
     */
    public function setSetup($setup)
    {
        $this->setup = $setup!==null?intval($setup):null;

        return $this;
    }

    /**
     * Get setup
     *
     * @return integer
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * Set cycletime
     *
     * @param decimal $cycletime
     *
     * @return ClientMouldGroup
     */
    public function setCycletime($cycletime)
    {
        $this->cycletime = $cycletime;

        return $this;
    }

    /**
     * Get cycletime
     *
     * @return decimal
     */
    public function getCycletime()
    {
        return $this->cycletime;
    }

    /**
     * Set lotcount
     *
     * @param integer $lotcount
     *
     * @return ClientMouldGroup
     */
    public function setLotcount($lotcount)
    {
        $this->lotcount = $lotcount!==null?intval($lotcount):null;

        return $this;
    }

    /**
     * Get lotcount
     *
     * @return integer
     */
    public function getLotcount()
    {
        return $this->lotcount;
    }

    
    /**
     * Set operatorcount
     *
     * @param integer $operatorcount
     *
     * @return ClientMouldGroup
     */
    public function setOperatorcount($operatorcount)
    {
        $this->operatorcount = $operatorcount!==null?intval($operatorcount):null;

        return $this;
    }

    /**
     * Get operatorcount
     *
     * @return integer
     */
    public function getOperatorcount()
    {
        return $this->operatorcount;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientMouldGroup
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientMouldGroup
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set isdefault
     *
     * @param boolean $isdefault
     *
     * @return ClientMouldGroup
     */
    public function setIsdefault($isdefault) {
        $this->isdefault = $isdefault;
    }
    
    /**
     * Get isdefault
     *
     * @return boolean
     */
    public function getIsdefault() {
        return $this->isdefault;
    }
}

