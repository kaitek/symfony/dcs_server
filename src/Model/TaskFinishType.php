<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * TaskFinishType
 */
abstract class TaskFinishType extends BaseSync implements BaseSyncInterface, TaskFinishTypeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var boolean
     */
    protected $recreate = 0;

    /**
     * @var integer
     */
    protected $orderfield;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskFinishType
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set recreate
     *
     * @param boolean $recreate
     *
     * @return TaskFinishType
     */
    public function setRecreate($recreate)
    {
        $this->recreate = $recreate;

        return $this;
    }

    /**
     * Get recreate
     *
     * @return boolean
     */
    public function getRecreate()
    {
        return $this->recreate;
    }

    /**
     * Set orderfield
     *
     * @param integer $orderfield
     *
     * @return TaskFinishType
     */
    public function setOrderfield($orderfield)
    {
        $this->orderfield = $orderfield;

        return $this;
    }

    /**
     * Get orderfield
     *
     * @return integer
     */
    public function getOrderfield()
    {
        return $this->orderfield;
    }
}
