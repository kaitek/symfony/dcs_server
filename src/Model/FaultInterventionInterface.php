<?php

namespace App\Model;

/**
 * FaultInterventionInterface
 */
interface FaultInterventionInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set clients
     *
     * @param string $clients
     * @return FaultInterventionInterface
     */
    public function setClients($clients);

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients();

    /**
     * Set faultinterventiontype
     *
     * @param string $faultinterventiontype
     * @return FaultInterventionInterface
     */
    public function setFaultinterventiontype($faultinterventiontype);

    /**
     * Get faultinterventiontype
     *
     * @return string
     */
    public function getFaultinterventiontype();

    /**
     * Set name
     *
     * @param string $name
     * @return FaultInterventionInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set interventions
     *
     * @param string $interventions
     * @return FaultInterventionInterface
     */
    public function setInterventions($interventions);

    /**
     * Get interventions
     *
     * @return string
     */
    public function getInterventions();

    /**
     * Set controlname
     *
     * @param string $controlname
     * @return FaultInterventionInterface
     */
    public function setControlname($controlname);

    /**
     * Get controlname
     *
     * @return string
     */
    public function getControlname();

    /**
     * Set controlofficers
     *
     * @param string $controlofficers
     * @return FaultInterventionInterface
     */
    public function setControlofficers($controlofficers);

    /**
     * Get controlofficers
     *
     * @return string
     */
    public function getControlofficers();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return FaultInterventionInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return FaultInterventionInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Set answertype
     *
     * @param string $answertype
     * @return FaultInterventionInterface
     */
    public function setAnswertype($answertype);

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype();

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return FaultInterventionInterface
     */
    public function setValuerequire($valuerequire);

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire();

    /**
     * Set valuemin
     *
     * @param string $valuemin
     * @return FaultInterventionInterface
     */
    public function setValuemin($valuemin);

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin();

    /**
     * Set valuemax
     *
     * @param string $valuemax
     * @return FaultInterventionInterface
     */
    public function setValuemax($valuemax);

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax();
    /**
     * Get id
     *
     * @return integer
     */

    /**
    * Set mailto
    *
    * @param string $mailto
    * @return FaultInterventionInterface
    */
    public function setMailto($mailto);

    /**
     * Get mailto
     *
     * @return string
     */
    public function getMailto();

    /**
     * Set isallowna
     *
     * @param boolean $isallowna
     *
     * @return FaultInterventionInterface
     */
    public function setIsallowna($isallowna);

    /**
     * Get isallowna
     *
     * @return boolean
     */
    public function getIsallowna();
}
