<?php
namespace App\Model;

/**
 * ClientMouldDetailInterface
 */
interface ClientMouldDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientMouldDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set clientmouldgroup
     *
     * @param string $clientmouldgroup
     * @return ClientMouldDetailInterface
     */
    public function setClientmouldgroup($clientmouldgroup);

    /**
     * Get clientmouldgroup
     *
     * @return string 
     */
    public function getClientmouldgroup();

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     * @return ClientMouldDetailInterface
     */
    public function setMouldgroup($mouldgroup);

    /**
     * Get mouldgroup
     *
     * @return string 
     */
    public function getMouldgroup();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return ClientMouldDetailInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return ClientMouldDetailInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
}
