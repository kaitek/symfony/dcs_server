<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * MouldGroup
 */
abstract class MouldGroup extends BaseSync implements BaseSyncInterface, MouldGroupInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $mould;

    /**
     * @var integer
     */
    protected $productionmultiplier;

    /**
     * @var decimal
     */
    protected $intervalmultiplier;

    /**
     * @var integer
     */
    protected $counter;

    /**
     * @var integer
     */
    protected $setup;

    /**
     * @var decimal
     */
    protected $cycletime;

    /**
     * @var integer
     */
    protected $lotcount;

    /**
     * @var string
     */
    protected $program;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return MouldGroup
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set mould
     *
     * @param string $mould
     *
     * @return MouldGroup
     */
    public function setMould($mould)
    {
        $this->mould = $mould;

        return $this;
    }

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould()
    {
        return $this->mould;
    }

    /**
     * Set productionmultiplier
     *
     * @param integer $productionmultiplier
     *
     * @return MouldGroup
     */
    public function setProductionmultiplier($productionmultiplier)
    {
        $this->productionmultiplier = $productionmultiplier!==null?intval($productionmultiplier):null;

        return $this;
    }

    /**
     * Get productionmultiplier
     *
     * @return integer
     */
    public function getProductionmultiplier()
    {
        return $this->productionmultiplier;
    }

    /**
     * Set intervalmultiplier
     *
     * @param decimal $intervalmultiplier
     *
     * @return MouldGroup
     */
    public function setIntervalmultiplier($intervalmultiplier)
    {
        $this->intervalmultiplier = $intervalmultiplier!==null?($intervalmultiplier):null;

        return $this;
    }

    /**
     * Get intervalmultiplier
     *
     * @return decimal
     */
    public function getIntervalmultiplier()
    {
        return $this->intervalmultiplier;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     *
     * @return MouldGroup
     */
    public function setCounter($counter)
    {
        $this->counter = $counter!==null?intval($counter):null;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Set setup
     *
     * @param integer $setup
     *
     * @return MouldGroup
     */
    public function setSetup($setup)
    {
        $this->setup = $setup!==null?intval($setup):null;

        return $this;
    }

    /**
     * Get setup
     *
     * @return integer
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * Set cycletime
     *
     * @param decimal $cycletime
     *
     * @return MouldGroup
     */
    public function setCycletime($cycletime)
    {
        $this->cycletime = $cycletime;

        return $this;
    }

    /**
     * Get cycletime
     *
     * @return decimal
     */
    public function getCycletime()
    {
        return $this->cycletime;
    }

    /**
     * Set lotcount
     *
     * @param integer $lotcount
     *
     * @return MouldGroup
     */
    public function setLotcount($lotcount)
    {
        $this->lotcount = $lotcount!==null?intval($lotcount):null;

        return $this;
    }

    /**
     * Get lotcount
     *
     * @return integer
     */
    public function getLotcount()
    {
        return $this->lotcount;
    }

    /**
     * Set program
     *
     * @param string $program
     *
     * @return MouldGroup
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return string
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return MouldGroup
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return MouldGroup
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
}

