<?php
namespace App\Model;

/**
 * IOEventInterface
 */
interface IOEventInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return IOEventInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set iotype
     *
     * @param string $iotype
     * @return IOEventInterface
     */
    public function setIotype($iotype);

    /**
     * Get iotype
     *
     * @return string 
     */
    public function getIotype();
    
}
