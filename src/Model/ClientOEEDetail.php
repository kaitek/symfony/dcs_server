<?php

namespace App\Model;

/**
 * ClientOEEDetail
 */
abstract class ClientOEEDetail implements ClientOEEDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $kul;

    /**
     * @var string
     */
    protected $per;

    /**
     * @var string
     */
    protected $kal;

    /**
     * @var string
     */
    protected $oee;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientOEEDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return ClientOEEDetail
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return ClientOEEDetail
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set kul
     *
     * @param string $kul
     *
     * @return ClientOEEDetail
     */
    public function setKul($kul)
    {
        $this->kul = $kul;

        return $this;
    }

    /**
     * Get kul
     *
     * @return string
     */
    public function getKul()
    {
        return $this->kul;
    }

    /**
     * Set per
     *
     * @param string $per
     *
     * @return ClientOEEDetail
     */
    public function setPer($per)
    {
        $this->per = $per;

        return $this;
    }

    /**
     * Get per
     *
     * @return string
     */
    public function getPer()
    {
        return $this->per;
    }

    /**
     * Set kal
     *
     * @param string $kal
     *
     * @return ClientOEEDetail
     */
    public function setKal($kal)
    {
        $this->kal = $kal;

        return $this;
    }

    /**
     * Get kal
     *
     * @return string
     */
    public function getKal()
    {
        return $this->kal;
    }

    /**
     * Set oee
     *
     * @param string $oee
     *
     * @return ClientOEEDetail
     */
    public function setOee($oee)
    {
        $this->oee = $oee;

        return $this;
    }

    /**
     * Get oee
     *
     * @return string
     */
    public function getOee()
    {
        return $this->oee;
    }
}

