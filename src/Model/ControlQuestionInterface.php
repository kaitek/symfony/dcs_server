<?php

namespace App\Model;

/**
 * ControlQuestionInterface
 */
interface ControlQuestionInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ControlQuestionInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ControlQuestionInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set question
     *
     * @param string $question
     * @return ControlQuestionInterface
     */
    public function setQuestion($question);

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion();

    /**
     * Set answertype
     *
     * @param string $answertype
     * @return ControlQuestionInterface
     */
    public function setAnswertype($answertype);

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype();

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return ControlQuestionInterface
     */
    public function setValuerequire($valuerequire);

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire();

    /**
     * Set valuemin
     *
     * @param string $valuemin
     * @return ControlQuestionInterface
     */
    public function setValuemin($valuemin);

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin();

    /**
     * Set valuemax
     *
     * @param string $valuemax
     * @return ControlQuestionInterface
     */
    public function setValuemax($valuemax);

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax();
    /**
     * Get id
     *
     * @return integer
     */

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ControlQuestionInterface
     */
    public function setDocumentnumber($documentnumber);

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber();

    /**
     * Set clients
     *
     * @param string $clients
     *
     * @return ControlQuestionInterface
     */
    public function setClients($clients);

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients();

    /**
     * Set path
     *
     * @param string $path
     *
     * @return ControlQuestionInterface
     */
    public function setPath($path);

    /**
     * Get path
     *
     * @return string
     */
    public function getPath();

    /**
     * Set isallowna
     *
     * @param boolean $isallowna
     *
     * @return ControlQuestionInterface
     */
    public function setIsallowna($isallowna);

    /**
     * Get isallowna
     *
     * @return boolean
     */
    public function getIsallowna();
}
