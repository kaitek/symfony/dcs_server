<?php
namespace App\Model;

/**
 * CaseMovementInterface
 */
interface CaseMovementInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set carrier
     *
     * @param string $carrier
     * @return CaseMovementInterface
     */
    public function setCarrier($carrier);

    /**
     * Get carrier
     *
     * @return string 
     */
    public function getCarrier();

    /**
     * Set code
     *
     * @param string $code
     * @return CaseMovementInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set stockcode
     *
     * @param string $stockcode
     * @return CaseMovementInterface
     */
    public function setStockcode($stockcode);

    /**
     * Get stockcode
     *
     * @return string 
     */
    public function getStockcode();

    /**
     * Set number
     *
     * @param integer $number
     * @return CaseMovementInterface
     */
    public function setNumber($number);

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber();

    /**
     * Set casename
     *
     * @param string $casename
     * @return CaseMovementInterface
     */
    public function setCasename($casename);

    /**
     * Get casename
     *
     * @return string 
     */
    public function getCasename();

    /**
     * Set locationsource
     *
     * @param string $locationsource
     * @return CaseMovementInterface
     */
    public function setLocationsource($locationsource);

    /**
     * Get locationsource
     *
     * @return string 
     */
    public function getLocationsource();

    /**
     * Set locationdestination
     *
     * @param string $locationdestination
     * @return CaseMovementInterface
     */
    public function setLocationdestination($locationdestination);

    /**
     * Get locationdestination
     *
     * @return string 
     */
    public function getLocationdestination();

    /**
     * Set tasktype
     *
     * @param string $tasktype
     * @return CaseMovementInterface
     */
    public function setTasktype($tasktype);

    /**
     * Get tasktype
     *
     * @return string 
     */
    public function getTasktype();

    /**
     * Set starttime
     *
     * @param \DateTime $starttime
     * @return CaseMovementInterface
     */
    public function setStarttime($starttime);

    /**
     * Get starttime
     *
     * @return \DateTime 
     */
    public function getStarttime();

    /**
     * Set targettime
     *
     * @param \DateTime $targettime
     * @return CaseMovementInterface
     */
    public function setTargettime($targettime);

    /**
     * Get targettime
     *
     * @return \DateTime 
     */
    public function getTargettime();

    /**
     * Set assignmenttime
     *
     * @param \DateTime $assignmenttime
     * @return CaseMovementInterface
     */
    public function setAssignmenttime($assignmenttime);

    /**
     * Get assignmenttime
     *
     * @return \DateTime 
     */
    public function getAssignmenttime();

    /**
     * Set actualfinishtime
     *
     * @param \DateTime $actualfinishtime
     * @return CaseMovementInterface
     */
    public function setActualfinishtime($actualfinishtime);

    /**
     * Get actualfinishtime
     *
     * @return \DateTime 
     */
    public function getActualfinishtime();

    /**
     * Set status
     *
     * @param string $status
     * @return CaseMovementInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus();

    /**
     * Set client
     *
     * @param string $client
     * @return CaseMovementInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return CaseMovementInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string 
     */
    public function getErprefnumber();

    /**
     * Set quantitynecessary
     *
     * @param decimal $quantitynecessary
     * @return CaseMovementInterface
     */
    public function setQuantitynecessary($quantitynecessary);

    /**
     * Get quantitynecessary
     *
     * @return decimal 
     */
    public function getQuantitynecessary();

    /**
     * Set quantityready
     *
     * @param decimal $quantityready
     * @return CaseMovementInterface
     */
    public function setQuantityready($quantityready);

    /**
     * Get quantityready
     *
     * @return decimal 
     */
    public function getQuantityready();

    /**
     * Set quantitydelivered
     *
     * @param decimal $quantitydelivered
     * @return CaseMovementInterface
     */
    public function setQuantitydelivered($quantitydelivered);

    /**
     * Get quantitydelivered
     *
     * @return decimal 
     */
    public function getQuantitydelivered();

    /**
     * Set empcode
     *
     * @param string $empcode
     * @return CaseMovementInterface
     */
    public function setEmpcode($empcode);

    /**
     * Get empcode
     *
     * @return string 
     */
    public function getEmpcode();

    /**
     * Set empname
     *
     * @param string $empname
     * @return CaseMovementInterface
     */
    public function setEmpname($empname);

    /**
     * Get empname
     *
     * @return string 
     */
    public function getEmpname();

    /**
     * Set locationsourceinfo
     *
     * @param string $locationsourceinfo
     * @return CaseMovementInterface
     */
    public function setLocationsourceinfo($locationsourceinfo);

    /**
     * Get locationsourceinfo
     *
     * @return string 
     */
    public function getLocationsourceinfo();

    /**
     * Set locationdestinationinfo
     *
     * @param string $locationdestinationinfo
     * @return CaseMovementInterface
     */
    public function setLocationdestinationinfo($locationdestinationinfo);

    /**
     * Get locationdestinationinfo
     *
     * @return string 
     */
    public function getLocationdestinationinfo();

    /**
     * Set filepath
     *
     * @param string $filepath
     * @return CaseMovementInterface
     */
    public function setFilepath($filepath);

    /**
     * Get filepath
     *
     * @return string 
     */
    public function getFilepath();

    /**
     * Set estimatewaitfinishtime
     *
     * @param \DateTime $estimatewaitfinishtime
     * @return CaseMovementInterface
     */
    public function setEstimatewaitfinishtime($estimatewaitfinishtime);

    /**
     * Get estimatewaitfinishtime
     *
     * @return \DateTime 
     */
    public function getEstimatewaitfinishtime();

    /**
     * Set position
     *
     * @param string $position
     * @return CaseMovementInterface
     */
    public function setPosition($position);

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition();

    /**
     * Set carriertype
     *
     * @param string $carriertype
     * @return CaseMovementInterface
     */
    public function setCarriertype($carriertype);

    /**
     * Get carriertype
     *
     * @return string 
     */
    public function getCarriertype();

    /**
     * Set breaktimeseconds
     *
     * @param integer $breaktimeseconds
     * @return CaseMovementInterface
     */
    public function setBreaktimeseconds($breaktimeseconds);

    /**
     * Get breaktimeseconds
     *
     * @return integer 
     */
    public function getBreaktimeseconds();
    
    /**
     * Set carrierfeeder
     *
     * @param string $carrierfeeder
     * @return CaseMovementInterface
     */
    public function setCarrierfeeder($carrierfeeder);

    /**
     * Get carrierfeeder
     *
     * @return string 
     */
    public function getCarrierfeeder();

    /**
     * Set carrierdrainer
     *
     * @param string $carrierdrainer
     * @return CaseMovementInterface
     */
    public function setCarrierdrainer($carrierdrainer);

    /**
     * Get carrierdrainer
     *
     * @return string 
     */
    public function getCarrierdrainer();

    /**
     * Get casenumbersource
     *
     * @return string
     */
    public function getcasenumbersource();
    
    /**
     * Set casenumbersource
     *
     * @param string $casenumbersource
     *
     * @return CaseMovementInterface
     */
    public function setCasenumbersource($casenumbersource);

    /**
     * Set casenumberdestination
     *
     * @param string $casenumberdestination
     *
     * @return CaseMovementInterface
     */
    public function setCasenumberdestination($casenumberdestination);

    /**
     * Get casenumberdestination
     *
     * @return string
     */
    public function getCasenumberdestination();
}
