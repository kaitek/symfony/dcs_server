<?php
namespace App\Model;

/**
 * LostGroupInterface
 */
interface LostGroupInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return LostGroupInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();
    
     /**
     * Get groupcode
     *
     * @return string
     */
    function getGroupcode();

    /**
     * Set groupcode
     *
     * @param string $groupcode
     *
     * @return LostGroupInterface
     */
    function setGroupcode($groupcode);

   
}
