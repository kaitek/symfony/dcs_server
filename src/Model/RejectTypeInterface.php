<?php
namespace App\Model;

/**
 * RejectTypeInterface
 */
interface RejectTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return RejectTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set extcode
     *
     * @param string $extcode
     *
     * @return RejectTypeInterface
     */
    public function setExtcode($extcode);

    /**
     * Get extcode
     *
     * @return string
     */
    public function getExtcode();

    /**
     * Set description
     *
     * @param string $description
     * @return RejectTypeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();
    
    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return RejectTypeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return RejectTypeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Set islisted
     *
     * @param boolean $islisted
     *
     * @return RejectTypeInterface
     */
    public function setIslisted($islisted);
    
    /**
     * Get islisted
     *
     * @return boolean
     */
    public function getIslisted();

    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return RejectTypeInterface
     */
    public function setListorder($listorder);

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder();

    /**
     * Set isoeeaffect
     *
     * @param boolean $isoeeaffect
     *
     * @return RejectTypeInterface
     */
    public function setIsoeeaffect($isoeeaffect);
    
    /**
     * Get isoeeaffect
     *
     * @return boolean
     */
    public function getIsoeeaffect();

    /**
     * Set isproductionaffect
     *
     * @param boolean $isproductionaffect
     *
     * @return RejectTypeInterface
     */
    public function setIsproductionaffect($isproductionaffect);
    
    /**
     * Get isproductionaffect
     *
     * @return boolean
     */
    public function getIsproductionaffect();
    
}
