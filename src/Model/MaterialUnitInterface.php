<?php
namespace App\Model;

/**
 * MaterialUnitInterface
 */
interface MaterialUnitInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return MaterialUnitInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();
    
}
