<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * JobRotationWorkPeople
 */
abstract class JobRotationWorkPeople extends Base implements BaseInterface, JobRotationWorkPeopleInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $jobrotationteam;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $beginval;

    /**
     * @var string
     */
    protected $endval;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return JobRotationWorkPeople
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return JobRotationWorkPeople
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set jobrotationteam
     *
     * @param string $jobrotationteam
     *
     * @return JobRotationWorkPeople
     */
    public function setJobrotationteam($jobrotationteam)
    {
        $this->jobrotationteam = $jobrotationteam;

        return $this;
    }

    /**
     * Get jobrotationteam
     *
     * @return string
     */
    public function getJobrotationteam()
    {
        return $this->jobrotationteam;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return JobRotationWorkPeople
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set beginval
     *
     * @param string $beginval
     *
     * @return JobRotationWorkPeople
     */
    public function setBeginval($beginval)
    {
        $this->beginval = $beginval;

        return $this;
    }

    /**
     * Get beginval
     *
     * @return string
     */
    public function getBeginval()
    {
        return $this->beginval;
    }

    /**
     * Set endval
     *
     * @param string $endval
     *
     * @return JobRotationWorkPeople
     */
    public function setEndval($endval)
    {
        $this->endval = $endval;

        return $this;
    }

    /**
     * Get endval
     *
     * @return string
     */
    public function getEndval()
    {
        return $this->endval;
    }

}

