<?php
namespace App\Model;

/**
 * EmployeeInterface
 */
interface EmployeeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return EmployeeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set name
     *
     * @param string $name
     * @return EmployeeInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string 
     */
    public function getName();

    /**
     * Set secret
     *
     * @param string $secret
     * @return EmployeeInterface
     */
    public function setSecret($secret);

    /**
     * Get secret
     *
     * @return string 
     */
    public function getSecret();

    /**
     * Set isexpert
     *
     * @param boolean $isexpert
     * @return EmployeeInterface
     */
    public function setIsexpert($isexpert);

    /**
     * Get isexpert
     *
     * @return boolean 
     */
    public function getIsexpert();

    /**
     * Set isoperator
     *
     * @param boolean $isoperator
     * @return EmployeeInterface
     */
    public function setIsoperator($isoperator);

    /**
     * Get isoperator
     *
     * @return boolean 
     */
    public function getIsoperator();

    /**
     * Set ismaintenance
     *
     * @param boolean $ismaintenance
     * @return EmployeeInterface
     */
    public function setIsmaintenance($ismaintenance);

    /**
     * Get ismaintenance
     *
     * @return boolean 
     */
    public function getIsmaintenance();

    /**
     * Set ismould
     *
     * @param boolean $ismould
     * @return EmployeeInterface
     */
    public function setIsmould($ismould);

    /**
     * Get ismould
     *
     * @return boolean 
     */
    public function getIsmould();

    /**
     * Set isquality
     *
     * @param boolean $isquality
     * @return EmployeeInterface
     */
    public function setIsquality($isquality);

    /**
     * Get isquality
     *
     * @return boolean 
     */
    public function getIsquality();

    /**
     * Set jobrotationteam
     *
     * @param string $jobrotationteam
     * @return EmployeeInterface
     */
    public function setJobrotationteam($jobrotationteam);

    /**
     * Get jobrotationteam
     *
     * @return string 
     */
    public function getJobrotationteam();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return EmployeeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return EmployeeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
    /**
     * Set client
     *
     * @param string $client
     *
     * @return EmployeeInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();
    
    /**
     * Set tasklist
     *
     * @param string $tasklist
     *
     * @return EmployeeInterface
     */
    public function setTasklist($tasklist);

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist();
    
    /**
     * Set day
     *
     * @param \DateTime $day
     * @return EmployeeInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return EmployeeInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set lastseen
     *
     * @param \DateTime $lastseen
     *
     * @return EmployeeInterface
     */
    public function setLastseen($lastseen);

    /**
     * Get lastseen
     *
     * @return \DateTime
     */
    public function getLastseen();

    /**
     * Set strlastseen
     *
     * @param string $strlastseen
     *
     * @return EmployeeInterface
     */
    public function setStrlastseen($strlastseen);

    /**
     * Get strlastseen
     *
     * @return string
     */
    public function getStrlastseen();

    /**
     * Set teamleader
     *
     * @param string $teamleader
     *
     * @return EmployeeInterface
     */
    public function setTeamleader($teamleader);

    /**
     * Get teamleader
     *
     * @return string
     */
    public function getTeamleader();
}
