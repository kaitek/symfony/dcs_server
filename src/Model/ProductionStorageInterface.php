<?php
namespace App\Model;

/**
 * ProductionStorageInterface
 */
interface ProductionStorageInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set casemovementidIn
     *
     * @param integer $casemovementidIn
     * @return ProductionStorageInterface
     */
    public function setCasemovementidIn($casemovementidIn);

    /**
     * Get casemovementidIn
     *
     * @return integer 
     */
    public function getCasemovementidIn();

    /**
     * Set casemovementidOut
     *
     * @param integer $casemovementidOut
     * @return ProductionStorageInterface
     */
    public function setCasemovementidOut($casemovementidOut);

    /**
     * Get casemovementidOut
     *
     * @return integer 
     */
    public function getCasemovementidOut();

    /**
     * Set code
     *
     * @param string $code
     * @return ProductionStorageInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set number
     *
     * @param integer $number
     * @return ProductionStorageInterface
     */
    public function setNumber($number);

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber();

    /**
     * Set stockcode
     *
     * @param string $stockcode
     * @return ProductionStorageInterface
     */
    public function setStockcode($stockcode);

    /**
     * Get stockcode
     *
     * @return string 
     */
    public function getStockcode();

    /**
     * Set casename
     *
     * @param string $casename
     * @return ProductionStorageInterface
     */
    public function setCasename($casename);

    /**
     * Get casename
     *
     * @return string 
     */
    public function getCasename();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return ProductionStorageInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return ProductionStorageInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return ProductionStorageInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string 
     */
    public function getErprefnumber();

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return ProductionStorageInterface
     */
    public function setQuantity($quantity);

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity();
}
