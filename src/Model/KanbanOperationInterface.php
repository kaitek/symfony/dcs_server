<?php
namespace App\Model;

/**
 * KanbanOperationInterface
 */
interface KanbanOperationInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return KanbanOperationInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set product
     *
     * @param string $product
     * @return KanbanOperationInterface
     */
    public function setProduct($product);

    /**
     * Get product
     *
     * @return string 
     */
    public function getProduct();

    /**
     * Set boxcount
     *
     * @param integer $boxcount
     * @return KanbanOperationInterface
     */
    public function setBoxcount($boxcount);

    /**
     * Get boxcount
     *
     * @return integer 
     */
    public function getBoxcount();

    /**
     * Set minboxcount
     *
     * @param integer $minboxcount
     * @return KanbanOperationInterface
     */
    public function setMinboxcount($minboxcount);

    /**
     * Get minboxcount
     *
     * @return integer 
     */
    public function getMinboxcount();

    /**
     * Set maxboxcount
     *
     * @param integer $maxboxcount
     * @return KanbanOperationInterface
     */
    public function setMaxboxcount($maxboxcount);

    /**
     * Get maxboxcount
     *
     * @return integer 
     */
    public function getMaxboxcount();

    /**
     * Set currentboxcount
     *
     * @param integer $currentboxcount
     * @return KanbanOperationInterface
     */
    public function setCurrentboxcount($currentboxcount);

    /**
     * Get currentboxcount
     *
     * @return integer 
     */
    public function getCurrentboxcount();

    /**
     * Set packaging
     *
     * @param integer $packaging
     * @return KanbanOperationInterface
     */
    public function setPackaging($packaging);

    /**
     * Get packaging
     *
     * @return integer 
     */
    public function getPackaging();

    /**
     * Set hourlydemand
     *
     * @param integer $hourlydemand
     * @return KanbanOperationInterface
     */
    public function setHourlydemand($hourlydemand);

    /**
     * Get hourlydemand
     *
     * @return integer 
     */
    public function getHourlydemand();
   
}
