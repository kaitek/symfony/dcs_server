<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * KanbanOperation
 */
abstract class KanbanOperation extends BaseSync implements BaseSyncInterface, KanbanOperationInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $product;

    /**
     * @var integer
     */
    protected $boxcount;

    /**
     * @var integer
     */
    protected $minboxcount;

    /**
     * @var integer
     */
    protected $maxboxcount;

    /**
     * @var integer
     */
    protected $currentboxcount;

    /**
     * @var integer
     */
    protected $packaging;

    /**
     * @var integer
     */
    protected $hourlydemand;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return KanbanOperation
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set product
     *
     * @param string $product
     *
     * @return KanbanOperation
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set boxcount
     *
     * @param integer $boxcount
     *
     * @return KanbanOperation
     */
    public function setBoxcount($boxcount)
    {
        $this->boxcount = $boxcount;

        return $this;
    }

    /**
     * Get boxcount
     *
     * @return integer
     */
    public function getBoxcount()
    {
        return $this->boxcount;
    }

    /**
     * Set minboxcount
     *
     * @param integer $minboxcount
     *
     * @return KanbanOperation
     */
    public function setMinboxcount($minboxcount)
    {
        $this->minboxcount = $minboxcount;

        return $this;
    }

    /**
     * Get minboxcount
     *
     * @return integer
     */
    public function getMinboxcount()
    {
        return $this->minboxcount;
    }

    /**
     * Set maxboxcount
     *
     * @param integer $maxboxcount
     *
     * @return KanbanOperation
     */
    public function setMaxboxcount($maxboxcount)
    {
        $this->maxboxcount = $maxboxcount;

        return $this;
    }

    /**
     * Get maxboxcount
     *
     * @return integer
     */
    public function getMaxboxcount()
    {
        return $this->maxboxcount;
    }

    /**
     * Set currentboxcount
     *
     * @param integer $currentboxcount
     *
     * @return KanbanOperation
     */
    public function setCurrentboxcount($currentboxcount)
    {
        $this->currentboxcount = $currentboxcount;

        return $this;
    }

    /**
     * Get currentboxcount
     *
     * @return integer
     */
    public function getCurrentboxcount()
    {
        return $this->currentboxcount;
    }

    /**
     * Set packaging
     *
     * @param integer $packaging
     *
     * @return KanbanOperation
     */
    public function setPackaging($packaging)
    {
        $this->packaging = $packaging;

        return $this;
    }

    /**
     * Get packaging
     *
     * @return integer
     */
    public function getPackaging()
    {
        return $this->packaging;
    }

    /**
     * Set hourlydemand
     *
     * @param integer $hourlydemand
     *
     * @return KanbanOperation
     */
    public function setHourlydemand($hourlydemand)
    {
        $this->hourlydemand = $hourlydemand;

        return $this;
    }

    /**
     * Get hourlydemand
     *
     * @return integer
     */
    public function getHourlydemand()
    {
        return $this->hourlydemand;
    }
}

