<?php
namespace App\Model;

/**
 * CaseLabelInterface
 */
interface CaseLabelInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return CaseLabelInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string 
     */
    public function getErprefnumber();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return CaseLabelInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime();

    /**
     * Set status
     *
     * @param string $status
     * @return CaseLabelInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus();

    /**
     * Set canceldescription
     *
     * @param string $canceldescription
     * @return CaseLabelInterface
     */
    public function setCanceldescription($canceldescription);

    /**
     * Get canceldescription
     *
     * @return string 
     */
    public function getCanceldescription();

    /**
     * Set canceltime
     *
     * @param \DateTime $canceltime
     * @return CaseLabelInterface
     */
    public function setCanceltime($canceltime);

    /**
     * Get canceltime
     *
     * @return \DateTime 
     */
    public function getCanceltime();

    /**
     * Set cancelusername
     *
     * @param string $cancelusername
     * @return CaseLabelInterface
     */
    public function setCancelusername($cancelusername);

    /**
     * Get cancelusername
     *
     * @return string 
     */
    public function getCancelusername();

    /**
     * Set synced
     *
     * @param integer $synced
     *
     * @return CaseLabelInterface
     */
    public function setSynced($synced);

    /**
     * Get synced
     *
     * @return integer
     */
    public function getSynced();
    
}
