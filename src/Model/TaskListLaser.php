<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * TaskListLaser
 */
abstract class TaskListLaser extends BaseSync implements BaseSyncInterface, TaskListLaserInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var integer
     */
    protected $production;

    /**
     * @var string
     */
    protected $tfdescription;

    /**
     * @var \DateTime
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var string
     */
    protected $client1;

    /**
     * @var string
     */
    protected $client2;

    /**
     * @var string
     */
    protected $client3;

    /**
     * @var string
     */
    protected $client4;

    /**
     * @var string
     */
    protected $client5;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskListLaser
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TaskListLaser
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set production
     *
     * @param integer $production
     *
     * @return TaskListLaser
     */
    public function setProduction($production)
    {
        $this->production = $production!==null?intval($production):null;

        return $this;
    }

    /**
     * Get production
     *
     * @return integer
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * Set tfdescription
     *
     * @param string $tfdescription
     *
     * @return TaskListLaser
     */
    public function setTfdescription($tfdescription) {
        $this->tfdescription = $tfdescription;
    }
    
    /**
     * Get tfdescription
     *
     * @return string
     */
    public function getTfdescription() {
        return $this->tfdescription;
    }

    /**
     * Set plannedstart
     *
     * @param \DateTime $plannedstart
     *
     * @return TaskListLaser
     */
    public function setPlannedstart($plannedstart)
    {
        $this->plannedstart = $plannedstart;

        return $this;
    }

    /**
     * Get plannedstart
     *
     * @return \DateTime
     */
    public function getPlannedstart()
    {
        return $this->plannedstart;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return TaskListLaser
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return TaskListLaser
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Get the value of client1
     *
     * @return  string
     */ 
    public function getClient1()
    {
        return $this->client1;
    }

    /**
     * Set the value of client1
     *
     * @param  string  $client1
     *
     * @return TaskListLaser
     */ 
    public function setClient1($client1)
    {
        $this->client1 = $client1;

        return $this;
    }

    /**
     * Get the value of client2
     *
     * @return  string
     */ 
    public function getClient2()
    {
        return $this->client2;
    }

    /**
     * Set the value of client2
     *
     * @param  string  $client2
     *
     * @return TaskListLaser
     */ 
    public function setClient2($client2)
    {
        $this->client2 = $client2;

        return $this;
    }

    /**
     * Get the value of client3
     *
     * @return  string
     */ 
    public function getClient3()
    {
        return $this->client3;
    }

    /**
     * Set the value of client3
     *
     * @param  string  $client3
     *
     * @return TaskListLaser
     */ 
    public function setClient3($client3)
    {
        $this->client3 = $client3;

        return $this;
    }

    /**
     * Get the value of client4
     *
     * @return  string
     */ 
    public function getClient4()
    {
        return $this->client4;
    }

    /**
     * Set the value of client4
     *
     * @param  string  $client4
     *
     * @return TaskListLaser
     */ 
    public function setClient4($client4)
    {
        $this->client4 = $client4;

        return $this;
    }

    /**
     * Get the value of client5
     *
     * @return  string
     */ 
    public function getClient5()
    {
        return $this->client5;
    }

    /**
     * Set the value of client5
     *
     * @param  string  $client5
     *
     * @return TaskListLaser
     */ 
    public function setClient5($client5)
    {
        $this->client5 = $client5;

        return $this;
    }
}

