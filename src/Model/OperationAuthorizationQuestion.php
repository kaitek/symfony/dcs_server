<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * OperationAuthorizationQuestion
 */
abstract class OperationAuthorizationQuestion extends Base implements BaseInterface, OperationAuthorizationQuestionInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $question;

    /**
     * @var integer
     */
    protected $answertype;

    /**
     * @var integer
     */
    protected $answermin;

    /**
     * @var integer
     */
    protected $answermax;

    /**
     * @var boolean
     */
    protected $ismandatory;

    /**
     * @var integer
     */
    protected $score;

    /**
     * @var integer
     */
    protected $level;

    /**
     * @var integer
     */
    protected $listorder;

    /**
     * @var boolean
     */
    protected $isdescriptionrequire;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return OperationAuthorizationQuestion
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return OperationAuthorizationQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answertype
     *
     * @param integer $answertype
     *
     * @return OperationAuthorizationQuestion
     */
    public function setAnswertype($answertype)
    {
        $this->answertype = $answertype;

        return $this;
    }

    /**
     * Get answertype
     *
     * @return integer
     */
    public function getAnswertype()
    {
        return $this->answertype;
    }

    /**
     * Set answermin
     *
     * @param integer $answermin
     *
     * @return OperationAuthorizationQuestion
     */
    public function setAnswermin($answermin)
    {
        $this->answermin = $answermin;

        return $this;
    }

    /**
     * Get answermin
     *
     * @return integer
     */
    public function getAnswermin()
    {
        return $this->answermin;
    }

    /**
     * Set answermax
     *
     * @param integer $answermax
     *
     * @return OperationAuthorizationQuestion
     */
    public function setAnswermax($answermax)
    {
        $this->answermax = $answermax;

        return $this;
    }

    /**
     * Get answermax
     *
     * @return integer
     */
    public function getAnswermax()
    {
        return $this->answermax;
    }

    /**
     * Set ismandatory
     *
     * @param boolean $ismandatory
     *
     * @return OperationAuthorizationQuestion
     */
    public function setIsmandatory($ismandatory)
    {
        $this->ismandatory = $ismandatory;

        return $this;
    }

    /**
     * Get ismandatory
     *
     * @return boolean
     */
    public function getIsmandatory()
    {
        return $this->ismandatory;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return OperationAuthorizationQuestion
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return OperationAuthorizationQuestion
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return OperationAuthorizationQuestion
     */
    public function setListorder($listorder)
    {
        $this->listorder = $listorder;

        return $this;
    }

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder()
    {
        return $this->listorder;
    }

    /**
     * Set isdescriptionrequire
     *
     * @param boolean $isdescriptionrequire
     *
     * @return OperationAuthorizationQuestion
     */
    public function setIsdescriptionrequire($isdescriptionrequire)
    {
        $this->isdescriptionrequire = $isdescriptionrequire;

        return $this;
    }

    /**
     * Get isdescriptionrequire
     *
     * @return boolean
     */
    public function getIsdescriptionrequire()
    {
        return $this->isdescriptionrequire;
    }
}

