<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * Rework
 */
abstract class Rework extends BaseSync implements BaseSyncInterface, ReworkInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $reworktype;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $opdescription;

    /**
     * @var string
     */
    protected $tpp;

    /**
     * @var boolean
     */
    protected $islisted;
    /**
     * @var boolean
     */
    protected $ismandatory;  

    /**
     * @var integer
     */
    protected $listorder;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return Rework
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set reworktype
     *
     * @param string $reworktype
     *
     * @return Rework
     */
    public function setReworktype($reworktype)
    {
        $this->reworktype = $reworktype;

        return $this;
    }

    /**
     * Get reworktype
     *
     * @return string
     */
    public function getReworktype()
    {
        return $this->reworktype;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Rework
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return Rework
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return Rework
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return Rework
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return Rework
     */
    public function setOpdescription($opdescription)
    {
        $this->opdescription = $opdescription;

        return $this;
    }

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription()
    {
        return $this->opdescription;
    }

    /**
     * Set tpp
     *
     * @param string $tpp
     *
     * @return Rework
     */
    public function setTpp($tpp)
    {
        $this->tpp = $tpp;

        return $this;
    }

    /**
     * Get tpp
     *
     * @return string
     */
    public function getTpp()
    {
        return $this->tpp;
    }

    /**
     * Set islisted
     *
     * @param boolean $islisted
     *
     * @return Rework
     */
    public function setIslisted($islisted)
    {
        $this->islisted = $islisted;

        return $this;
    }

    /**
     * Get islisted
     *
     * @return boolean
     */
    public function getIslisted()
    {
        return $this->islisted;
    }
    
    /**
     * Set ismandatory
     *
     * @param boolean $ismandatory
     *
     * @return Rework
     */
    public function setIsmandatory($ismandatory)
    {
        $this->ismandatory = $ismandatory;

        return $this;
    }

    /**
     * Get ismandatory
     *
     * @return boolean
     */
    public function getIsmandatory()
    {
        return $this->ismandatory;
    }
    
    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return Rework
     */
    public function setListorder($listorder)
    {
        $this->listorder = $listorder;

        return $this;
    }

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder()
    {
        return $this->listorder;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Rework
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return Rework
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
}

