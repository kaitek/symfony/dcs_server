<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * Stock
 */
abstract class Stock extends BaseSync implements BaseSyncInterface, StockInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;
    
    /**
     * @var string
     */
    protected $opcode;
    
    /**
     * @var decimal
     */
    protected $packcapacity;

    /**
     * @var decimal
     */
    protected $quantityremaining;

    /**
     * @var string
     */
    protected $clients;

    /**
     * @var string
     */
    protected $filename;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Stock
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return Stock
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set packcapacity
     *
     * @param decimal $packcapacity
     *
     * @return Stock
     */
    public function setPackcapacity($packcapacity)
    {
        $this->packcapacity = $packcapacity;

        return $this;
    }

    /**
     * Get packcapacity
     *
     * @return decimal
     */
    public function getPackcapacity()
    {
        return $this->packcapacity;
    }

    /**
     * Set quantityremaining
     *
     * @param decimal $quantityremaining
     *
     * @return Stock
     */
    public function setQuantityremaining($quantityremaining)
    {
        $this->quantityremaining = $quantityremaining;

        return $this;
    }

    /**
     * Get quantityremaining
     *
     * @return decimal
     */
    public function getQuantityremaining()
    {
        return $this->quantityremaining;
    }

    /**
     * Set clients
     *
     * @param string $clients
     *
     * @return Stock
     */
    public function setClients($clients)
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Stock
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }
}
