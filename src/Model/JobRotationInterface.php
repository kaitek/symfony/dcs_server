<?php
namespace App\Model;

/**
 * JobRotationInterface
 */
interface JobRotationInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return JobRotationInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set beginval
     *
     * @param string $beginval
     * @return JobRotationInterface
     */
    public function setBeginval($beginval);

    /**
     * Get beginval
     *
     * @return string 
     */
    public function getBeginval();

    /**
     * Set endval
     *
     * @param string $endval
     * @return JobRotationInterface
     */
    public function setEndval($endval);

    /**
     * Get endval
     *
     * @return string 
     */
    public function getEndval();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return JobRotationInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return JobRotationInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    
}
