<?php
namespace App\Model;

/**
 * CaseChangeTimeInterface
 */
interface CaseChangeTimeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set casename
     *
     * @param string $casename
     * @return CaseChangeTimeInterface
     */
    public function setCasename($casename);

    /**
     * Get casename
     *
     * @return string 
     */
    public function getCasename();

    /**
     * Set changetime
     *
     * @param integer $changetime
     * @return CaseChangeTimeInterface
     */
    public function setChangetime($changetime);

    /**
     * Get changetime
     *
     * @return integer 
     */
    public function getChangetime();

}
