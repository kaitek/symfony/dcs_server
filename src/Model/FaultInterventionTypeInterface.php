<?php

namespace App\Model;

/**
 * FaultInterventionTypeInterface
 */
interface FaultInterventionTypeInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return FaultInterventionTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set description
     *
     * @param string $description
     *
     * @return FaultInterventionTypeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();
}
