<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * ActivityControlAnswer
 */
abstract class ActivityControlAnswer extends Base implements BaseInterface, ActivityControlAnswerInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $documentnumber;

    /**
     * @var string
     */
    protected $question;

    /**
     * @var string
     */
    protected $answertype;

    /**
     * @var string
     */
    protected $valuerequire;

    /**
     * @var string
     */
    protected $valuemin;

    /**
     * @var string
     */
    protected $valuemax;

    /**
     * @var string
     */
    protected $answer;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var bool
     */
    protected $isexported;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ActivityControlAnswer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ActivityControlAnswer
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return ActivityControlAnswer
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return ActivityControlAnswer
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ActivityControlAnswer
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ActivityControlAnswer
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return ActivityControlAnswer
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return ActivityControlAnswer
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ActivityControlAnswer
     */
    public function setDocumentnumber($documentnumber)
    {
        $this->documentnumber = $documentnumber;

        return $this;
    }

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber()
    {
        return $this->documentnumber;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return ActivityControlAnswer
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answertype
     *
     * @param string $answertype
     *
     * @return ActivityControlAnswer
     */
    public function setAnswertype($answertype)
    {
        $this->answertype = $answertype;

        return $this;
    }

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype()
    {
        return $this->answertype;
    }

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return ActivityControlAnswer
     */
    public function setValuerequire($valuerequire)
    {
        $this->valuerequire = $valuerequire;

        return $this;
    }

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire()
    {
        return $this->valuerequire;
    }

    /**
     * Set valuemin
     *
     * @param string $valuemin
     *
     * @return ActivityControlAnswer
     */
    public function setValuemin($valuemin)
    {
        $this->valuemin = $valuemin;

        return $this;
    }

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin()
    {
        return $this->valuemin;
    }

    /**
     * Set valuemax
     *
     * @param string $valuemax
     *
     * @return ActivityControlAnswer
     */
    public function setValuemax($valuemax)
    {
        $this->valuemax = $valuemax;

        return $this;
    }

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax()
    {
        return $this->valuemax;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return ActivityControlAnswer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ActivityControlAnswer
     */
    public function setDescription($description) {
        $this->description = $description;
    }
    
    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ActivityControlAnswer
     */
    public function setIsexported($isexported)
    {
        $this->isexported = $isexported;

        return $this;
    }

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported()
    {
        return $this->isexported;
    }
}

