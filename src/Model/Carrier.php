<?php

namespace App\Model;

/**
 * Carrier
 */
abstract class Carrier implements CarrierInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $uuid;

    /**
     * @var boolean
     */
    protected $connected;

    /**
     * @var string
     */
    protected $workflow;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var integer
     */
    protected $pincode;

    /**
     * @var string
     */
    protected $versionapp;

    /**
     * @var string
     */
    protected $versionshell;

    /**
     * @var string
     */
    protected $computername;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Carrier
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Carrier
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Carrier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return Carrier
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set connected
     *
     * @param boolean $connected
     *
     * @return Carrier
     */
    public function setConnected($connected)
    {
        $this->connected = $connected;

        return $this;
    }

    /**
     * Get connected
     *
     * @return boolean
     */
    public function getConnected()
    {
        return $this->connected;
    }

    /**
     * Set workflow
     *
     * @param string $workflow
     *
     * @return Carrier
     */
    public function setWorkflow($workflow)
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * Get workflow
     *
     * @return string
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Carrier
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set pincode
     *
     * @param integer $pincode
     *
     * @return Carrier
     */
    public function setPincode($pincode)
    {
        $this->pincode = $pincode;

        return $this;
    }

    /**
     * Get pincode
     *
     * @return integer
     */
    public function getPincode()
    {
        return $this->pincode;
    }

    /**
     * Set versionapp
     *
     * @param string $versionapp
     *
     * @return Carrier
     */
    public function setVersionapp($versionapp)
    {
        $this->versionapp = $versionapp;

        return $this;
    }

    /**
     * Get versionapp
     *
     * @return string
     */
    public function getVersionapp()
    {
        return $this->versionapp;
    }

    /**
     * Set versionshell
     *
     * @param string $versionshell
     *
     * @return Carrier
     */
    public function setVersionshell($versionshell)
    {
        $this->versionshell = $versionshell;

        return $this;
    }

    /**
     * Get versionshell
     *
     * @return string
     */
    public function getVersionshell()
    {
        return $this->versionshell;
    }

    /**
     * Set computername
     *
     * @param string $computername
     *
     * @return Carrier
     */
    public function setComputername($computername)
    {
        $this->computername = $computername;

        return $this;
    }

    /**
     * Get computername
     *
     * @return string
     */
    public function getComputername()
    {
        return $this->computername;
    }
}

