<?php
namespace App\Model;

/**
 * OperationAuthorizationQuestionInterface
 */
interface OperationAuthorizationQuestionInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set opname
     *
     * @param string $opname
     * @return OperationAuthorizationQuestionInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set question
     *
     * @param string $question
     * @return OperationAuthorizationQuestionInterface
     */
    public function setQuestion($question);

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion();

    /**
     * Set answertype
     *
     * @param integer $answertype
     * @return OperationAuthorizationQuestionInterface
     */
    public function setAnswertype($answertype);

    /**
     * Get answertype
     *
     * @return integer 
     */
    public function getAnswertype();

    /**
     * Set answermin
     *
     * @param integer $answermin
     * @return OperationAuthorizationQuestionInterface
     */
    public function setAnswermin($answermin);

    /**
     * Get answermin
     *
     * @return integer 
     */
    public function getAnswermin();

    /**
     * Set answermax
     *
     * @param integer $answermax
     * @return OperationAuthorizationQuestionInterface
     */
    public function setAnswermax($answermax);

    /**
     * Get answermax
     *
     * @return integer 
     */
    public function getAnswermax();

    /**
     * Set ismandatory
     *
     * @param boolean $ismandatory
     * @return OperationAuthorizationQuestionInterface
     */
    public function setIsmandatory($ismandatory);

    /**
     * Get ismandatory
     *
     * @return boolean 
     */
    public function getIsmandatory();

    /**
     * Set score
     *
     * @param integer $score
     * @return OperationAuthorizationQuestionInterface
     */
    public function setScore($score);

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore();

    /**
     * Set level
     *
     * @param integer $level
     * @return OperationAuthorizationQuestionInterface
     */
    public function setLevel($level);

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel();

    /**
     * Set listorder
     *
     * @param integer $listorder
     * @return OperationAuthorizationQuestionInterface
     */
    public function setListorder($listorder);

    /**
     * Get listorder
     *
     * @return integer 
     */
    public function getListorder();

    /**
     * Set isdescriptionrequire
     *
     * @param boolean $isdescriptionrequire
     * @return OperationAuthorizationQuestionInterface
     */
    public function setIsdescriptionrequire($isdescriptionrequire);

    /**
     * Get isdescriptionrequire
     *
     * @return boolean 
     */
    public function getIsdescriptionrequire();
    
}
