<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * ClientGroup
 */
abstract class ClientGroup extends Base implements BaseInterface, ClientGroupInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $groupcode;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ClientGroup
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
     /**
     * Get groupcode
     *
     * @return string
     */
    function getGroupcode() {
        return $this->groupcode;
    }

    /**
     * Set groupcode
     *
     * @param string $groupcode
     *
     * @return ClientGroup
     */
    function setGroupcode($groupcode) {
        $this->groupcode = $groupcode;
    }
    
}

