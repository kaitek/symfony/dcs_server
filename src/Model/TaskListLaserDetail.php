<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * TaskListLaserDetail
 */
abstract class TaskListLaserDetail extends BaseSync implements BaseSyncInterface, TaskListLaserDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $tasklistlaser;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var integer
     */
    protected $leafmask;

    /**
     * @var integer
     */
    protected $productcount;
    
    /**
     * @var integer
     */
    protected $productdonecount;
    
    /**
     * @var integer
     */
    protected $productdoneactivity;
    
    /**
     * @var integer
     */
    protected $productdonejobrotation;
    
    /**
     * @var integer
     */
    protected $scrapactivity;
    
    /**
     * @var integer
     */
    protected $scrapjobrotation;
    
    /**
     * @var integer
     */
    protected $scrappart;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tasklistlaser
     *
     * @param string $tasklistlaser
     *
     * @return TaskListLaserDetail
     */
    public function setTasklistlaser($tasklistlaser)
    {
        $this->tasklistlaser = $tasklistlaser;

        return $this;
    }

    /**
     * Get tasklistlaser
     *
     * @return string
     */
    public function getTasklistlaser()
    {
        return $this->tasklistlaser;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return TaskListLaserDetail
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return TaskListLaserDetail
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber!==null?intval($opnumber):null;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskListLaserDetail
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return TaskListLaserDetail
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set leafmask
     *
     * @param integer $leafmask
     *
     * @return TaskListLaserDetail
     */
    public function setLeafmask($leafmask)
    {
        $this->leafmask = $leafmask;

        return $this;
    }

    /**
     * Get leafmask
     *
     * @return integer
     */
    public function getLeafmask()
    {
        return $this->leafmask;
    }
    
    /**
     * Set productcount
     *
     * @param integer $productcount
     *
     * @return TaskListLaserDetail
     */
    public function setProductcount($productcount)
    {
        $this->productcount = $productcount;

        return $this;
    }

    /**
     * Get productcount
     *
     * @return integer
     */
    public function getProductcount()
    {
        return $this->productcount;
    }

    /**
     * Set productdonecount
     *
     * @param integer $productdonecount
     *
     * @return TaskListLaserDetail
     */
    public function setProductdonecount($productdonecount)
    {
        $this->productdonecount = $productdonecount;

        return $this;
    }

    /**
     * Get productdonecount
     *
     * @return int
     */
    public function getProductdonecount()
    {
        return $this->productdonecount;
    }
    
    /**
     * Set productdoneactivity
     *
     * @param integer $productdoneactivity
     *
     * @return TaskListLaserDetail
     */
    public function setProductdoneactivity($productdoneactivity) {
        $this->productdoneactivity = $productdoneactivity;
    }
    
    /**
     * Get productdoneactivity
     *
     * @return int
     */
    public function getProductdoneactivity() {
        return $this->productdoneactivity;
    }

    /**
     * Set productdonejobrotation
     *
     * @param integer $productdonejobrotation
     *
     * @return TaskListLaserDetail
     */
    public function setProductdonejobrotation($productdonejobrotation) {
        $this->productdonejobrotation = $productdonejobrotation;
    }
    
    /**
     * Get productdonejobrotation
     *
     * @return int
     */
    public function getProductdonejobrotation() {
        return $this->productdonejobrotation;
    }

    /**
     * Set scrapactivity
     *
     * @param integer $scrapactivity
     *
     * @return TaskListLaserDetail
     */
    public function setScrapactivity($scrapactivity) {
        $this->scrapactivity = $scrapactivity;
    }
    
    /**
     * Get scrapactivity
     *
     * @return int
     */
    public function getScrapactivity() {
        return $this->scrapactivity;
    }

    /**
     * Set scrapjobrotation
     *
     * @param integer $scrapjobrotation
     *
     * @return TaskListLaserDetail
     */
    public function setScrapjobrotation($scrapjobrotation) {
        $this->scrapjobrotation = $scrapjobrotation;
    }
    
    /**
     * Get scrapjobrotation
     *
     * @return int
     */
    public function getScrapjobrotation() {
        return $this->scrapjobrotation;
    }

    /**
     * Set scrappart
     *
     * @param integer $scrappart
     *
     * @return TaskListLaserDetail
     */
    public function setScrappart($scrappart) {
        $this->scrappart = $scrappart;
    }
    
    /**
     * Get scrappart
     *
     * @return int
     */
    public function getScrappart() {
        return $this->scrappart;
    }
}

