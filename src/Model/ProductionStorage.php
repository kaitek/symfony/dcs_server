<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * ProductionStorage
 */
abstract class ProductionStorage extends Base implements BaseInterface, ProductionStorageInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $casemovementidIn;

    /**
     * @var integer
     */
    protected $casemovementidOut;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var integer
     */
    protected $number;

    /**
     * @var string
     */
    protected $stockcode;

    /**
     * @var string
     */
    protected $casename;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var integer
     */
    protected $quantity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set casemovementidIn
     *
     * @param integer $casemovementidIn
     *
     * @return ProductionStorage
     */
    public function setCasemovementidIn($casemovementidIn)
    {
        $this->casemovementidIn = $casemovementidIn;

        return $this;
    }

    /**
     * Get casemovementidIn
     *
     * @return integer
     */
    public function getCasemovementidIn()
    {
        return $this->casemovementidIn;
    }

    /**
     * Set casemovementidOut
     *
     * @param integer $casemovementidOut
     *
     * @return ProductionStorage
     */
    public function setCasemovementidOut($casemovementidOut)
    {
        $this->casemovementidOut = $casemovementidOut;

        return $this;
    }

    /**
     * Get casemovementidOut
     *
     * @return integer
     */
    public function getCasemovementidOut()
    {
        return $this->casemovementidOut;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ProductionStorage
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return ProductionStorage
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set stockcode
     *
     * @param string $stockcode
     *
     * @return ProductionStorage
     */
    public function setStockcode($stockcode)
    {
        $this->stockcode = $stockcode;

        return $this;
    }

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode()
    {
        return $this->stockcode;
    }

    /**
     * Set casename
     *
     * @param string $casename
     *
     * @return ProductionStorage
     */
    public function setCasename($casename)
    {
        $this->casename = $casename;

        return $this;
    }

    /**
     * Get casename
     *
     * @return string
     */
    public function getCasename()
    {
        return $this->casename;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ProductionStorage
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ProductionStorage
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ProductionStorage
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductionStorage
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
