<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * CaseLabel
 */
abstract class CaseLabel extends BaseSync implements BaseSyncInterface, CaseLabelInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $canceldescription;

    /**
     * @var \DateTime
     */
    protected $canceltime;

    /**
     * @var string
     */
    protected $cancelusername;

    /**
     * @var integer
     */
    protected $synced;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return CaseLabel
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return CaseLabel
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return CaseLabel
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set canceldescription
     *
     * @param string $canceldescription
     *
     * @return CaseLabel
     */
    public function setCanceldescription($canceldescription)
    {
        $this->canceldescription = $canceldescription;

        return $this;
    }

    /**
     * Get canceldescription
     *
     * @return string
     */
    public function getCanceldescription()
    {
        return $this->canceldescription;
    }

    /**
     * Set canceltime
     *
     * @param \DateTime $canceltime
     *
     * @return CaseLabel
     */
    public function setCanceltime($canceltime)
    {
        $this->canceltime = $canceltime;

        return $this;
    }

    /**
     * Get canceltime
     *
     * @return \DateTime
     */
    public function getCanceltime()
    {
        return $this->canceltime;
    }

    /**
     * Set cancelusername
     *
     * @param string $cancelusername
     *
     * @return CaseLabel
     */
    public function setCancelusername($cancelusername)
    {
        $this->cancelusername = $cancelusername;

        return $this;
    }

    /**
     * Get cancelusername
     *
     * @return string
     */
    public function getCancelusername()
    {
        return $this->cancelusername;
    }

    /**
     * Set synced
     *
     * @param integer $synced
     *
     * @return CaseLabel
     */
    public function setSynced($synced)
    {
        $this->synced = $synced;

        return $this;
    }

    /**
     * Get synced
     *
     * @return integer
     */
    public function getSynced()
    {
        return $this->synced;
    }
}

