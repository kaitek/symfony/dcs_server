<?php
namespace App\Model;

/**
 * TaskListLaserInterface
 */
interface TaskListLaserInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return TaskListLaserInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set description
     *
     * @param string $description
     * @return TaskListLaserInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set production
     *
     * @param integer $production
     * @return TaskListLaserInterface
     */
    public function setProduction($production);

    /**
     * Get production
     *
     * @return integer 
     */
    public function getProduction();

    /**
     * Set tfdescription
     *
     * @param string $tfdescription
     *
     * @return TaskListLaserInterface
     */
    public function setTfdescription($tfdescription);
    
    /**
     * Get tfdescription
     *
     * @return string
     */
    public function getTfdescription();

    /**
     * Set plannedstart
     *
     * @param \DateTime $plannedstart
     *
     * @return TaskListLaserInterface
     */
    public function setPlannedstart($plannedstart);

    /**
     * Get plannedstart
     *
     * @return \DateTime
     */
    public function getPlannedstart();
    
    /**
     * Set start
     *
     * @param \DateTime $start
     * @return TaskListLaserInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return TaskListLaserInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    /**
     * Get the value of client1
     *
     * @return  string
     */ 
    public function getClient1();

    /**
     * Set the value of client1
     *
     * @param  string  $client1
     *
     * @return TaskListLaserInterface
     */ 
    public function setClient1($client1);

    /**
     * Get the value of client2
     *
     * @return  string
     */ 
    public function getClient2();

    /**
     * Set the value of client2
     *
     * @param  string  $client2
     *
     * @return TaskListLaserInterface
     */ 
    public function setClient2($client2);

    /**
     * Get the value of client3
     *
     * @return  string
     */ 
    public function getClient3();

    /**
     * Set the value of client3
     *
     * @param  string  $client3
     *
     * @return TaskListLaserInterface
     */ 
    public function setClient3($client3);

    /**
     * Get the value of client4
     *
     * @return  string
     */ 
    public function getClient4();

    /**
     * Set the value of client4
     *
     * @param  string  $client4
     *
     * @return TaskListLaserInterface
     */ 
    public function setClient4($client4);

    /**
     * Get the value of client5
     *
     * @return  string
     */ 
    public function getClient5();

    /**
     * Set the value of client5
     *
     * @param  string  $client5
     *
     * @return TaskListLaserInterface
     */ 
    public function setClient5($client5);
    
}
