<?php

namespace App\Model;

/**
 * CaseMovement
 */
abstract class CaseMovement implements CaseMovementInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $carrier;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $stockcode;

    /**
     * @var integer
     */
    protected $number;

    /**
     * @var string
     */
    protected $casename;

    /**
     * @var string
     */
    protected $locationsource;

    /**
     * @var string
     */
    protected $locationdestination;

    /**
     * @var string
     */
    protected $tasktype;

    /**
     * @var \DateTime
     */
    protected $starttime;

    /**
     * @var \DateTime
     */
    protected $targettime;

    /**
     * @var \DateTime
     */
    protected $assignmenttime;

    /**
     * @var \DateTime
     */
    protected $actualfinishtime;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var decimal
     */
    protected $quantitynecessary;

    /**
     * @var decimal
     */
    protected $quantityready;

    /**
     * @var decimal
     */
    protected $quantitydelivered;

    /**
     * @var string
     */
    protected $empcode;

    /**
     * @var string
     */
    protected $empname;

    /**
     * @var string
     */
    protected $locationsourceinfo;

    /**
     * @var string
     */
    protected $locationdestinationinfo;

    /**
     * @var string
     */
    protected $filepath;

    /**
     * @var \DateTime
     */
    protected $estimatewaitfinishtime;

    /**
     * @var string
     */
    protected $position;

    /**
     * @var string
     */
    protected $carriertype;

    /**
     * @var integer
     */
    protected $breaktimeseconds;

    /**
     * @var string
     */
    protected $carrierfeeder;

    /**
     * @var string
     */
    protected $carrierdrainer;

    /**
     * @var string
     */
    protected $casenumbersource;

    /**
     * @var string
     */
    protected $casenumberdestination;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carrier
     *
     * @param string $carrier
     *
     * @return CaseMovement
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return CaseMovement
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set stockcode
     *
     * @param string $stockcode
     *
     * @return CaseMovement
     */
    public function setStockcode($stockcode)
    {
        $this->stockcode = $stockcode;

        return $this;
    }

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode()
    {
        return $this->stockcode;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return CaseMovement
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set casename
     *
     * @param string $casename
     *
     * @return CaseMovement
     */
    public function setCasename($casename)
    {
        $this->casename = $casename;

        return $this;
    }

    /**
     * Get casename
     *
     * @return string
     */
    public function getCasename()
    {
        return $this->casename;
    }

    /**
     * Set locationsource
     *
     * @param string $locationsource
     *
     * @return CaseMovement
     */
    public function setLocationsource($locationsource)
    {
        $this->locationsource = $locationsource;

        return $this;
    }

    /**
     * Get locationsource
     *
     * @return string
     */
    public function getLocationsource()
    {
        return $this->locationsource;
    }

    /**
     * Set locationdestination
     *
     * @param string $locationdestination
     *
     * @return CaseMovement
     */
    public function setLocationdestination($locationdestination)
    {
        $this->locationdestination = $locationdestination;

        return $this;
    }

    /**
     * Get locationdestination
     *
     * @return string
     */
    public function getLocationdestination()
    {
        return $this->locationdestination;
    }

    /**
     * Set tasktype
     *
     * @param string $tasktype
     *
     * @return CaseMovement
     */
    public function setTasktype($tasktype)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype
     *
     * @return string
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set starttime
     *
     * @param \DateTime $starttime
     *
     * @return CaseMovement
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return \DateTime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set targettime
     *
     * @param \DateTime $targettime
     *
     * @return CaseMovement
     */
    public function setTargettime($targettime)
    {
        $this->targettime = $targettime;

        return $this;
    }

    /**
     * Get targettime
     *
     * @return \DateTime
     */
    public function getTargettime()
    {
        return $this->targettime;
    }

    /**
     * Set assignmenttime
     *
     * @param \DateTime $assignmenttime
     *
     * @return CaseMovement
     */
    public function setAssignmenttime($assignmenttime)
    {
        $this->assignmenttime = $assignmenttime;

        return $this;
    }

    /**
     * Get assignmenttime
     *
     * @return \DateTime
     */
    public function getAssignmenttime()
    {
        return $this->assignmenttime;
    }

    /**
     * Set actualfinishtime
     *
     * @param \DateTime $actualfinishtime
     *
     * @return CaseMovement
     */
    public function setActualfinishtime($actualfinishtime)
    {
        $this->actualfinishtime = $actualfinishtime;

        return $this;
    }

    /**
     * Get actualfinishtime
     *
     * @return \DateTime
     */
    public function getActualfinishtime()
    {
        return $this->actualfinishtime;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return CaseMovement
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return CaseMovement
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return CaseMovement
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set quantitynecessary
     *
     * @param decimal $quantitynecessary
     *
     * @return CaseMovement
     */
    public function setQuantitynecessary($quantitynecessary)
    {
        $this->quantitynecessary = $quantitynecessary;

        return $this;
    }

    /**
     * Get quantitynecessary
     *
     * @return decimal
     */
    public function getQuantitynecessary()
    {
        return $this->quantitynecessary;
    }

    /**
     * Set quantityready
     *
     * @param decimal $quantityready
     *
     * @return CaseMovement
     */
    public function setQuantityready($quantityready)
    {
        $this->quantityready = $quantityready;

        return $this;
    }

    /**
     * Get quantityready
     *
     * @return decimal
     */
    public function getQuantityready()
    {
        return $this->quantityready;
    }

    /**
     * Set quantitydelivered
     *
     * @param decimal $quantitydelivered
     *
     * @return CaseMovement
     */
    public function setQuantitydelivered($quantitydelivered)
    {
        $this->quantitydelivered = $quantitydelivered;

        return $this;
    }

    /**
     * Get quantitydelivered
     *
     * @return decimal
     */
    public function getQuantitydelivered()
    {
        return $this->quantitydelivered;
    }

    /**
     * Set empcode
     *
     * @param string $empcode
     *
     * @return CaseMovement
     */
    public function setEmpcode($empcode)
    {
        $this->empcode = $empcode;

        return $this;
    }

    /**
     * Get empcode
     *
     * @return string
     */
    public function getEmpcode()
    {
        return $this->empcode;
    }

    /**
     * Set empname
     *
     * @param string $empname
     *
     * @return CaseMovement
     */
    public function setEmpname($empname)
    {
        $this->empname = $empname;

        return $this;
    }

    /**
     * Get empname
     *
     * @return string
     */
    public function getEmpname()
    {
        return $this->empname;
    }

    /**
     * Set locationsourceinfo
     *
     * @param string $locationsourceinfo
     *
     * @return CaseMovement
     */
    public function setLocationsourceinfo($locationsourceinfo)
    {
        $this->locationsourceinfo = $locationsourceinfo;

        return $this;
    }

    /**
     * Get locationsourceinfo
     *
     * @return string
     */
    public function getLocationsourceinfo()
    {
        return $this->locationsourceinfo;
    }

    /**
     * Set locationdestinationinfo
     *
     * @param string $locationdestinationinfo
     *
     * @return CaseMovement
     */
    public function setLocationdestinationinfo($locationdestinationinfo)
    {
        $this->locationdestinationinfo = $locationdestinationinfo;

        return $this;
    }

    /**
     * Get locationdestinationinfo
     *
     * @return string
     */
    public function getLocationdestinationinfo()
    {
        return $this->locationdestinationinfo;
    }

    /**
     * Set filepath
     *
     * @param string $filepath
     *
     * @return CaseMovement
     */
    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;

        return $this;
    }

    /**
     * Get filepath
     *
     * @return string
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    /**
     * Set estimatewaitfinishtime
     *
     * @param \DateTime $estimatewaitfinishtime
     *
     * @return CaseMovement
     */
    public function setEstimatewaitfinishtime($estimatewaitfinishtime)
    {
        $this->estimatewaitfinishtime = $estimatewaitfinishtime;

        return $this;
    }

    /**
     * Get estimatewaitfinishtime
     *
     * @return \DateTime
     */
    public function getEstimatewaitfinishtime()
    {
        return $this->estimatewaitfinishtime;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return CaseMovement
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set carriertype
     *
     * @param string $carriertype
     *
     * @return CaseMovement
     */
    public function setCarriertype($carriertype)
    {
        $this->carriertype = $carriertype;

        return $this;
    }

    /**
     * Get carriertype
     *
     * @return string
     */
    public function getCarriertype()
    {
        return $this->carriertype;
    }

    /**
     * Set breaktimeseconds
     *
     * @param integer $breaktimeseconds
     *
     * @return CaseMovement
     */
    public function setBreaktimeseconds($breaktimeseconds)
    {
        $this->breaktimeseconds = $breaktimeseconds;

        return $this;
    }

    /**
     * Get breaktimeseconds
     *
     * @return integer
     */
    public function getBreaktimeseconds()
    {
        return $this->breaktimeseconds;
    }

    /**
     * Set carrierfeeder
     *
     * @param string $carrierfeeder
     *
     * @return CaseMovement
     */
    public function setCarrierfeeder($carrierfeeder)
    {
        $this->carrierfeeder = $carrierfeeder;

        return $this;
    }

    /**
     * Get carrierfeeder
     *
     * @return string
     */
    public function getCarrierfeeder()
    {
        return $this->carrierfeeder;
    }

    /**
     * Set carrierdrainer
     *
     * @param string $carrierdrainer
     *
     * @return CaseMovement
     */
    public function setCarrierdrainer($carrierdrainer)
    {
        $this->carrierdrainer = $carrierdrainer;

        return $this;
    }

    /**
     * Get carrierdrainer
     *
     * @return string
     */
    public function getCarrierdrainer()
    {
        return $this->carrierdrainer;
    }

    /**
     * Get casenumbersource
     *
     * @return string
     */
    public function getcasenumbersource()
    {
        return $this->casenumbersource;
    }

    /**
     * Set casenumbersource
     *
     * @param string $casenumbersource
     *
     * @return CaseMovement
     */
    public function setCasenumbersource($casenumbersource)
    {
        $this->casenumbersource = $casenumbersource;

        return $this;
    }

    /**
     * Set casenumberdestination
     *
     * @param string $casenumberdestination
     *
     * @return CaseMovement
     */
    public function setCasenumberdestination($casenumberdestination)
    {
        $this->casenumberdestination = $casenumberdestination;

        return $this;
    }

    /**
     * Get casenumberdestination
     *
     * @return string
     */
    public function getCasenumberdestination()
    {
        return $this->casenumberdestination;
    }
}

