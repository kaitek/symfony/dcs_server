<?php
namespace App\Model;

/**
 * LostGroupDetailInterface
 */
interface LostGroupDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set lostgroup
     *
     * @param string $lostgroup
     * @return LostGroupDetailInterface
     */
    public function setLostgroup($lostgroup);

    /**
     * Get lostgroup
     *
     * @return string 
     */
    public function getLostgroup();

    /**
     * Set lostgroupcode
     *
     * @param string $lostgroupcode
     * @return LostGroupDetailInterface
     */
    public function setLostgroupcode($lostgroupcode);

    /**
     * Get lostgroupcode
     *
     * @return string 
     */
    public function getLostgroupcode();

    /**
     * Set losttype
     *
     * @param string $losttype
     * @return LostGroupDetailInterface
     */
    public function setLosttype($losttype);

    /**
     * Get losttype
     *
     * @return string 
     */
    public function getLosttype();

    /**
     * Set color
     *
     * @param string $color
     *
     * @return LostGroupDetailInterface
     */
    public function setColor($color);

    /**
     * Get color
     *
     * @return string
     */
    public function getColor();
}
