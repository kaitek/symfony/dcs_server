<?php
namespace App\Model;

/**
 * OvertimeInterface
 */
interface OvertimeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set clients
     *
     * @param string $clients
     * @return OvertimeInterface
     */
    public function setClients($clients);

    /**
     * Get clients
     *
     * @return string 
     */
    public function getClients();

    /**
     * Set name
     *
     * @param string $name
     * @return OvertimeInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string 
     */
    public function getName();

    /**
     * Set repeattype
     *
     * @param string $repeattype
     * @return OvertimeInterface
     */
    public function setRepeattype($repeattype);

    /**
     * Get repeattype
     *
     * @return string 
     */
    public function getRepeattype();

    /**
     * Set days
     *
     * @param string $days
     * @return OvertimeInterface
     */
    public function setDays($days);

    /**
     * Get days
     *
     * @return string 
     */
    public function getDays();

    /**
     * Set startday
     *
     * @param \DateTime $startday
     * @return OvertimeInterface
     */
    public function setStartday($startday);

    /**
     * Get startday
     *
     * @return \DateTime 
     */
    public function getStartday();

    /**
     * Set starttime
     *
     * @param string $starttime
     * @return OvertimeInterface
     */
    public function setStarttime($starttime);

    /**
     * Get starttime
     *
     * @return string 
     */
    public function getStarttime();

    /**
     * Set finishday
     *
     * @param \DateTime $finishday
     * @return OvertimeInterface
     */
    public function setFinishday($finishday);

    /**
     * Get finishday
     *
     * @return \DateTime 
     */
    public function getFinishday();

    /**
     * Set finishtime
     *
     * @param string $finishtime
     * @return OvertimeInterface
     */
    public function setFinishtime($finishtime);

    /**
     * Get finishtime
     *
     * @return string 
     */
    public function getFinishtime();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return OvertimeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return OvertimeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();
    
}
