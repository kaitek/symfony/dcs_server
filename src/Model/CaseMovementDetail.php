<?php

namespace App\Model;

/**
 * CaseMovementDetail
 */
abstract class CaseMovementDetail implements CaseMovementDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $casemovementId;

    /**
     * @var string
     */
    protected $carrier;

    /**
     * @var \DateTime
     */
    protected $assignmenttime;

    /**
     * @var \DateTime
     */
    protected $canceltime;

    /**
     * @var \DateTime
     */
    protected $donetime;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $empcode;

    /**
     * @var string
     */
    protected $empname;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set casemovementId
     *
     * @param integer $casemovementId
     *
     * @return CaseMovementDetail
     */
    public function setCasemovementId($casemovementId)
    {
        $this->casemovementId = $casemovementId;

        return $this;
    }

    /**
     * Get casemovementId
     *
     * @return integer
     */
    public function getCasemovementId()
    {
        return $this->casemovementId;
    }

    /**
     * Set carrier
     *
     * @param string $carrier
     *
     * @return CaseMovementDetail
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set assignmenttime
     *
     * @param \DateTime $assignmenttime
     *
     * @return CaseMovementDetail
     */
    public function setAssignmenttime($assignmenttime)
    {
        $this->assignmenttime = $assignmenttime;

        return $this;
    }

    /**
     * Get assignmenttime
     *
     * @return \DateTime
     */
    public function getAssignmenttime()
    {
        return $this->assignmenttime;
    }

    /**
     * Set canceltime
     *
     * @param \DateTime $canceltime
     *
     * @return CaseMovementDetail
     */
    public function setCanceltime($canceltime)
    {
        $this->canceltime = $canceltime;

        return $this;
    }

    /**
     * Get canceltime
     *
     * @return \DateTime
     */
    public function getCanceltime()
    {
        return $this->canceltime;
    }

    /**
     * Set donetime
     *
     * @param \DateTime $donetime
     *
     * @return CaseMovementDetail
     */
    public function setDonetime($donetime)
    {
        $this->donetime = $donetime;

        return $this;
    }

    /**
     * Get donetime
     *
     * @return \DateTime
     */
    public function getDonetime()
    {
        return $this->donetime;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CaseMovementDetail
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set empcode
     *
     * @param string $empcode
     *
     * @return CaseMovementDetail
     */
    public function setEmpcode($empcode)
    {
        $this->empcode = $empcode;

        return $this;
    }

    /**
     * Get empcode
     *
     * @return string
     */
    public function getEmpcode()
    {
        return $this->empcode;
    }

    /**
     * Set empname
     *
     * @param string $empname
     *
     * @return CaseMovementDetail
     */
    public function setEmpname($empname)
    {
        $this->empname = $empname;

        return $this;
    }

    /**
     * Get empname
     *
     * @return string
     */
    public function getEmpname()
    {
        return $this->empname;
    }
}

