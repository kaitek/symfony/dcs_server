<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * CaseCall
 */
abstract class CaseCall extends Base implements BaseInterface, CaseCallInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $state;

    /**
     * @var integer
     */
    protected $casemovementId;

    /**
     * @var \DateTime
     */
    protected $calltime;

    /**
     * @var \DateTime
     */
    protected $starttime;

    /**
     * @var \DateTime
     */
    protected $assignmenttime;

    /**
     * @var \DateTime
     */
    protected $actualfinishtime;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return CaseCall
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return CaseCall
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return CaseCall
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber() {
        return $this->erprefnumber;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return CaseCall
     */
    public function setErprefnumber($erprefnumber) {
        $this->erprefnumber = $erprefnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return CaseCall
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return CaseCall
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set casemovementId
     *
     * @param integer $casemovementId
     *
     * @return CaseCall
     */
    public function setCasemovementId($casemovementId)
    {
        $this->casemovementId = $casemovementId;

        return $this;
    }

    /**
     * Get casemovementId
     *
     * @return integer
     */
    public function getCasemovementId()
    {
        return $this->casemovementId;
    }

    /**
     * Set calltime
     *
     * @param \DateTime $calltime
     *
     * @return CaseCall
     */
    public function setCalltime($calltime)
    {
        $this->calltime = $calltime;

        return $this;
    }

    /**
     * Get calltime
     *
     * @return \DateTime
     */
    public function getCalltime()
    {
        return $this->calltime;
    }

    /**
     * Set starttime
     *
     * @param \DateTime $starttime
     *
     * @return CaseCall
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return \DateTime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set assignmenttime
     *
     * @param \DateTime $assignmenttime
     *
     * @return CaseCall
     */
    public function setAssignmenttime($assignmenttime)
    {
        $this->assignmenttime = $assignmenttime;

        return $this;
    }

    /**
     * Get assignmenttime
     *
     * @return \DateTime
     */
    public function getAssignmenttime()
    {
        return $this->assignmenttime;
    }

    /**
     * Set actualfinishtime
     *
     * @param \DateTime $actualfinishtime
     *
     * @return CaseCall
     */
    public function setActualfinishtime($actualfinishtime)
    {
        $this->actualfinishtime = $actualfinishtime;

        return $this;
    }

    /**
     * Get actualfinishtime
     *
     * @return \DateTime
     */
    public function getActualfinishtime()
    {
        return $this->actualfinishtime;
    }
}

