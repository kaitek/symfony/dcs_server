<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * Mould
 */
abstract class Mould extends BaseSync implements BaseSyncInterface, MouldInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var integer
     */
    protected $counter;

    /**
     * @var string
     */
    protected $mouldtype;
    
    /**
     * @var string
     */
    protected $serialnumber;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var string
     */
    protected $client1;

    /**
     * @var string
     */
    protected $client2;

    /**
     * @var string
     */
    protected $client3;

    /**
     * @var string
     */
    protected $client4;

    /**
     * @var string
     */
    protected $client5;

    /**
     * @var \DateTime
     */
    protected $bantime;

    /**
     * @var bool
     */
    protected $issensormould=false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Mould
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Mould
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     *
     * @return Mould
     */
    public function setCounter($counter)
    {
        $this->counter = $counter!==null?intval($counter):null;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Set mouldtype
     *
     * @param string $mouldtype
     *
     * @return Mould
     */
    public function setMouldtype($mouldtype)
    {
        $this->mouldtype = $mouldtype;

        return $this;
    }

    /**
     * Get mouldtype
     *
     * @return string
     */
    public function getMouldtype()
    {
        return $this->mouldtype;
    }
    
    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return Mould
     */
    public function setSerialnumber($serialnumber) {
        $this->serialnumber = $serialnumber;
    }

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber() {
        return $this->serialnumber;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Mould
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return Mould
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Get the value of client1
     *
     * @return  string
     */ 
    public function getClient1()
    {
        return $this->client1;
    }

    /**
     * Set the value of client1
     *
     * @param  string  $client1
     *
     * @return Mould
     */ 
    public function setClient1($client1)
    {
        $this->client1 = $client1;

        return $this;
    }

    /**
     * Get the value of client2
     *
     * @return  string
     */ 
    public function getClient2()
    {
        return $this->client2;
    }

    /**
     * Set the value of client2
     *
     * @param  string  $client2
     *
     * @return Mould
     */ 
    public function setClient2($client2)
    {
        $this->client2 = $client2;

        return $this;
    }

    /**
     * Get the value of client3
     *
     * @return  string
     */ 
    public function getClient3()
    {
        return $this->client3;
    }

    /**
     * Set the value of client3
     *
     * @param  string  $client3
     *
     * @return Mould
     */ 
    public function setClient3($client3)
    {
        $this->client3 = $client3;

        return $this;
    }

    /**
     * Get the value of client4
     *
     * @return  string
     */ 
    public function getClient4()
    {
        return $this->client4;
    }

    /**
     * Set the value of client4
     *
     * @param  string  $client4
     *
     * @return Mould
     */ 
    public function setClient4($client4)
    {
        $this->client4 = $client4;

        return $this;
    }

    /**
     * Get the value of client5
     *
     * @return  string
     */ 
    public function getClient5()
    {
        return $this->client5;
    }

    /**
     * Set the value of client5
     *
     * @param  string  $client5
     *
     * @return Mould
     */ 
    public function setClient5($client5)
    {
        $this->client5 = $client5;

        return $this;
    }

    /**
     * Set bantime
     *
     * @param \DateTime $bantime
     *
     * @return Mould
     */
    public function setBantime($bantime)
    {
        $this->bantime = $bantime;

        return $this;
    }

    /**
     * Get bantime
     *
     * @return \DateTime
     */
    public function getBantime()
    {
        return $this->bantime;
    }

    /**
     * Set issensormould
     *
     * @param boolean $issensormould
     *
     * @return Mould
     */
    public function setIssensormould($issensormould)
    {
        $this->issensormould = $issensormould;

        return $this;
    }

    /**
     * Get issensormould
     *
     * @return boolean
     */
    public function getIssensormould()
    {
        return $this->issensormould;
    }
}

