<?php
namespace App\Model;

/**
 * ClientParamInterface
 */
interface ClientParamInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientParamInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set name
     *
     * @param string $name
     * @return ClientParamInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string 
     */
    public function getName();

    /**
     * Set value
     *
     * @param string $value
     * @return ClientParamInterface
     */
    public function setValue($value);

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue();
}
