<?php

namespace App\Model;

/**
 * ClientLostDetailInterface
 */
interface ClientLostDetailInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ClientLostDetailInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientLostDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ClientLostDetailInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return ClientLostDetailInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Set losttype
     *
     * @param string $losttype
     *
     * @return ClientLostDetailInterface
     */
    public function setLosttype($losttype);

    /**
     * Get losttype
     *
     * @return string
     */
    public function getLosttype();

    /**
     * Set faulttype
     *
     * @param string $faulttype
     *
     * @return ClientLostDetailInterface
     */
    public function setFaulttype($faulttype);

    /**
     * Get faulttype
     *
     * @return string
     */
    public function getFaulttype();

    /**
     * Set faultdescription
     *
     * @param string $faultdescription
     *
     * @return ClientLostDetailInterface
     */
    public function setFaultdescription($faultdescription);

    /**
     * Get faultdescription
     *
     * @return string
     */
    public function getFaultdescription();

    /**
     * Set faultgroupcode
     *
     * @param string $faultgroupcode
     *
     * @return ClientLostDetailInterface
     */
    public function setFaultgroupcode($faultgroupcode);

    /**
     * Get faultgroupcode
     *
     * @return string
     */
    public function getFaultgroupcode();

    /**
     * Set faultdevice
     *
     * @param string $faultdevice
     *
     * @return ClientLostDetailInterface
     */
    public function setFaultdevice($faultdevice);

    /**
     * Get faultdevice
     *
     * @return string
     */
    public function getFaultdevice();

    /**
     * Set descriptionlost
     *
     * @param string $descriptionlost
     *
     * @return ClientLostDetailInterface
     */
    public function setDescriptionLost($descriptionlost);

    /**
     * Get descriptionlost
     *
     * @return string
     */
    public function getDescriptionLost();

    /**
     * Set tasklist
     *
     * @param string $tasklist
     * @return ClientLostDetailInterface
     */
    public function setTasklist($tasklist);

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist();

    /**
     * Set mould
     *
     * @param string $mould
     * @return ClientLostDetailInterface
     */
    public function setMould($mould);

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould();

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     * @return ClientLostDetailInterface
     */
    public function setMouldgroup($mouldgroup);

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return ClientLostDetailInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return ClientLostDetailInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ClientLostDetailInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return ClientLostDetailInterface
     */
    public function setOpdescription($opdescription);

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription();

    /**
     * Set employee
     *
     * @param string $employee
     * @return ClientLostDetailInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string
     */

    public function getEmployee();
    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return ClientLostDetailInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return ClientLostDetailInterface
     */
    public function setTaskfromerp($taskfromerp);

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientLostDetailInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientLostDetailInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Set energy
     *
     * @param decimal $energy
     *
     * @return ClientLostDetailInterface
     */
    public function setEnergy($energy);

    /**
     * Get energy
     *
     * @return decimal
     */
    public function getEnergy();

    /**
     * Set sourceclient
     *
     * @param string $sourceclient
     *
     * @return ClientLostDetailInterface
     */
    public function setSourceclient($sourceclient);

    /**
     * Get sourceclient
     *
     * @return string
     */
    public function getSourceclient();

    /**
     * Set sourcelosttype
     *
     * @param string $sourcelosttype
     *
     * @return ClientLostDetailInterface
     */
    public function setSourcelosttype($sourcelosttype);

    /**
     * Get sourcelosttype
     *
     * @return string
     */
    public function getSourcelosttype();

    /**
     * Set sourcedescriptionlost
     *
     * @param string $sourcedescriptionlost
     *
     * @return ClientLostDetailInterface
     */
    public function setSourcedescriptionlost($sourcedescriptionlost);

    /**
     * Get sourcedescriptionlost
     *
     * @return string
     */
    public function getSourcedescriptionlost();

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ClientLostDetailInterface
     */
    public function setIsexported($isexported);

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported();

    /**
     * Set opsayi
     *
     * @param integer $opsayi
     *
     * @return ClientLostDetailInterface
     */
    public function setOpsayi($opsayi);

    /**
     * Get opsayi
     *
     * @return integer
     */
    public function getOpsayi();

    /**
     * Set refsayi
     *
     * @param integer $refsayi
     *
     * @return ClientLostDetailInterface
     */
    public function setRefsayi($refsayi);

    /**
     * Get refsayi
     *
     * @return integer
     */
    public function getRefsayi();

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return ClientLostDetailInterface
     */
    public function setSerialnumber($serialnumber);

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber();
}
