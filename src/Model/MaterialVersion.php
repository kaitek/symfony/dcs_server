<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * MaterialVersion
 */
abstract class MaterialVersion extends BaseSync implements BaseSyncInterface, MaterialVersionInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $masterName;

    /**
     * @var string
     */
    protected $versionName;

    /**
     * @var string
     */
    protected $fullName;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set masterName
     *
     * @param string $masterName
     *
     * @return MaterialVersion
     */
    public function setMasterName($masterName)
    {
        $this->masterName = $masterName;

        return $this;
    }

    /**
     * Get masterName
     *
     * @return string
     */
    public function getMasterName()
    {
        return $this->masterName;
    }

    /**
     * Set versionName
     *
     * @param string $versionName
     *
     * @return MaterialVersion
     */
    public function setVersionName($versionName)
    {
        $this->versionName = $versionName;

        return $this;
    }

    /**
     * Get versionName
     *
     * @return string
     */
    public function getVersionName()
    {
        return $this->versionName;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return MaterialVersion
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return MaterialVersion
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return MaterialVersion
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
}

