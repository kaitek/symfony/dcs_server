<?php

namespace App\Model;

/**
 * ProductTreeInterface
 */
interface ProductTreeInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set parent
     *
     * @param string $parent
     * @return ProductTreeInterface
     */
    public function setParent($parent);

    /**
     * Get parent
     *
     * @return string
     */
    public function getParent();

    /**
     * Set code
     *
     * @param string $code
     * @return ProductTreeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set number
     *
     * @param integer $number
     * @return ProductTreeInterface
     */
    public function setNumber($number);

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber();

    /**
     * Set description
     *
     * @param string $description
     * @return ProductTreeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set materialtype
     *
     * @param string $materialtype
     * @return ProductTreeInterface
     */
    public function setMaterialtype($materialtype);

    /**
     * Get materialtype
     *
     * @return string
     */
    public function getMaterialtype();

    /**
     * Set name
     *
     * @param string $name
     * @return ProductTreeInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set stockcode
     *
     * @param string $stockcode
     * @return ProductTreeInterface
     */
    public function setStockcode($stockcode);

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode();

    /**
     * Set stockname
     *
     * @param string $stockname
     * @return ProductTreeInterface
     */
    public function setStockname($stockname);

    /**
     * Get stockname
     *
     * @return string
     */
    public function getStockname();

    /**
     * Set tpp
     *
     * @param decimal $tpp
     * @return ProductTreeInterface
     */
    public function setTpp($tpp);

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp();

    /**
     * Set quantity
     *
     * @param decimal $quantity
     * @return ProductTreeInterface
     */
    public function setQuantity($quantity);

    /**
     * Get quantity
     *
     * @return decimal
     */
    public function getQuantity();

    /**
     * Set unitQuantity
     *
     * @param string $unitQuantity
     * @return ProductTreeInterface
     */
    public function setUnit_quantity($unitQuantity);

    /**
     * Get unitQuantity
     *
     * @return string
     */
    public function getUnit_quantity();

    /**
     * Set opcounter
     *
     * @param integer $opcounter
     * @return ProductTreeInterface
     */
    public function setOpcounter($opcounter);

    /**
     * Get opcounter
     *
     * @return integer
     */
    public function getOpcounter();

    /**
     * Set pack
     *
     * @param string $pack
     * @return ProductTreeInterface
     */
    public function setPack($pack);

    /**
     * Get pack
     *
     * @return string
     */
    public function getPack();

    /**
     * Set packcapacity
     *
     * @param decimal $packcapacity
     * @return ProductTreeInterface
     */
    public function setPackcapacity($packcapacity);

    /**
     * Get packcapacity
     *
     * @return decimal
     */
    public function getPackcapacity();

    /**
     * Set unit_packcapacity
     *
     * @param string $unit_packcapacity
     * @return ProductTreeInterface
     */
    public function setUnit_packcapacity($unit_packcapacity);

    /**
     * Get unit_packcapacity
     *
     * @return string
     */
    public function getUnit_packcapacity();

    /**
     * Set oporder
     *
     * @param integer $oporder
     * @return ProductTreeInterface
     */
    public function setOporder($oporder);

    /**
     * Get oporder
     *
     * @return integer
     */
    public function getOporder();

    /**
     * Set preptime
     *
     * @param integer $preptime
     * @return ProductTreeInterface
     */
    public function setPreptime($preptime);

    /**
     * Get preptime
     *
     * @return integer
     */
    public function getPreptime();

    /**
     * Set qualitycontrolrequire
     *
     * @param boolean $qualitycontrolrequire
     * @return ProductTreeInterface
     */
    public function setQualitycontrolrequire($qualitycontrolrequire);

    /**
     * Get qualitycontrolrequire
     *
     * @return boolean
     */
    public function getQualitycontrolrequire();

    /**
     * Set task
     *
     * @param boolean $task
     * @return ProductTreeInterface
     */
    public function setTask($task);

    /**
     * Get task
     *
     * @return boolean
     */
    public function getTask();

    /**
     * Set feeder
     *
     * @param string $feeder
     * @return ProductTreeInterface
     */
    public function setFeeder($feeder);

    /**
     * Get feeder
     *
     * @return string
     */
    public function getFeeder();

    /**
     * Set carrierfeeder
     *
     * @param string $carrierfeeder
     * @return ProductTreeInterface
     */
    public function setCarrierfeeder($carrierfeeder);

    /**
     * Get carrierfeeder
     *
     * @return string
     */
    public function getCarrierfeeder();

    /**
     * Set drainer
     *
     * @param string $drainer
     * @return ProductTreeInterface
     */
    public function setDrainer($drainer);

    /**
     * Get drainer
     *
     * @return string
     */
    public function getDrainer();

    /**
     * Set carrierdrainer
     *
     * @param string $carrierdrainer
     * @return ProductTreeInterface
     */
    public function setCarrierdrainer($carrierdrainer);

    /**
     * Get carrierdrainer
     *
     * @return string
     */
    public function getCarrierdrainer();

    /**
     * Set goodsplanner
     *
     * @param string $goodsplanner
     * @return ProductTreeInterface
     */
    public function setGoodsplanner($goodsplanner);

    /**
     * Get goodsplanner
     *
     * @return string
     */
    public function getGoodsplanner();

    /**
     * Set productionmultiplier
     *
     * @param integer $productionmultiplier
     * @return ProductTreeInterface
     */
    public function setProductionmultiplier($productionmultiplier);

    /**
     * Get productionmultiplier
     *
     * @return integer
     */
    public function getProductionmultiplier();

    /**
     * Set intervalmultiplier
     *
     * @param decimal $intervalmultiplier
     * @return ProductTreeInterface
     */
    public function setIntervalmultiplier($intervalmultiplier);

    /**
     * Get intervalmultiplier
     *
     * @return decimal
     */
    public function getIntervalmultiplier();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return ProductTreeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return ProductTreeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Set delivery
     *
     * @param boolean $delivery
     *
     * @return ProductTreeInterface
     */
    public function setDelivery($delivery);

    /**
     * Get delivery
     *
     * @return boolean
     */
    public function getDelivery();

    /**
     * Get the value of locationsource
     *
     * @return  string
     */
    public function getLocationsource();

    /**
     * Set the value of locationsource
     *
     * @param  string  $locationsource
     *
     * @return ProductTreeInterface
     */
    public function setLocationsource($locationsource);

    /**
     * Get the value of locationdestination
     *
     * @return  string
     */
    public function getLocationdestination();

    /**
     * Set the value of locationdestination
     *
     * @param  string  $locationdestination
     *
     * @return ProductTreeInterface
     */
    public function setLocationdestination($locationdestination);

    /**
     * Set empcount
     *
     * @param integer $empcount
     *
     * @return ProductTreeInterface
     */
    public function setEmpcount($empcount);

    /**
     * Get empcount
     *
     * @return integer
     */
    public function getEmpcount();

    /**
     * Set isdefault
     *
     * @param boolean $isdefault
     *
     * @return ProductTreeInterface
     */
    public function setIsdefault($isdefault);

    /**
     * Get isdefault
     *
     * @return boolean
     */
    public function getIsdefault();

    /**
     * Get the value of client
     *
     * @return  string
     */
    public function getClient();

    /**
     * Set the value of client
     *
     * @param  string  $client
     *
     * @return ProductTreeInterface
     */
    public function setClient($client);

    /**
     * Set notificationtime
     *
     * @param integer $notificationtime
     *
     * @return ProductTreeInterface
     */
    public function setNotificationtime($notificationtime);

    /**
     * Get notificationtime
     *
     * @return integer
     */
    public function getNotificationtime();

    /**
     * Set lotcount
     *
     * @param integer $lotcount
     *
     * @return ProductTreeInterface
     */
    public function setLotcount($lotcount);

    /**
     * Get lotcount
     *
     * @return integer
     */
    public function getLotcount();
}
