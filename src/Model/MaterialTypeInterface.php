<?php
namespace App\Model;

/**
 * MaterialTypeInterface
 */
interface MaterialTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return MaterialTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set value
     *
     * @param string $value
     * @return MaterialTypeInterface
     */
    public function setValue($value);

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue();
    
}
