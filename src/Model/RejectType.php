<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * RejectType
 */
abstract class RejectType extends BaseSync implements BaseSyncInterface, RejectTypeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $extcode;

    /**
     * @var string
     */
    protected $description;
    
    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var boolean
     */
    protected $islisted;
    
    /**
     * @var integer
     */
    protected $listorder;

    /**
     * @var boolean
     */
    protected $isoeeaffect;

    /**
     * @var boolean
     */
    protected $isproductionaffect;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return RejectType
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set extcode
     *
     * @param string $extcode
     *
     * @return RejectType
     */
    public function setExtcode($extcode)
    {
        $this->extcode = $extcode;

        return $this;
    }

    /**
     * Get extcode
     *
     * @return string
     */
    public function getExtcode()
    {
        return $this->extcode;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return RejectType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return RejectType
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return RejectType
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set islisted
     *
     * @param boolean $islisted
     *
     * @return RejectType
     */
    public function setIslisted($islisted) {
        $this->islisted = $islisted;
    }
    
    /**
     * Get islisted
     *
     * @return boolean
     */
    public function getIslisted() {
        return $this->islisted;
    }
    
    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return RejectType
     */
    public function setListorder($listorder)
    {
        $this->listorder = $listorder;

        return $this;
    }

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder()
    {
        return $this->listorder;
    }

    /**
     * Set isoeeaffect
     *
     * @param boolean $isoeeaffect
     *
     * @return RejectType
     */
    public function setIsoeeaffect($isoeeaffect) {
        $this->isoeeaffect = $isoeeaffect;
    }
    
    /**
     * Get isoeeaffect
     *
     * @return boolean
     */
    public function getIsoeeaffect() {
        return $this->isoeeaffect;
    }

    /**
     * Set isproductionaffect
     *
     * @param boolean $isproductionaffect
     *
     * @return RejectType
     */
    public function setIsproductionaffect($isproductionaffect) {
        $this->isproductionaffect = $isproductionaffect;
    }
    
    /**
     * Get isproductionaffect
     *
     * @return boolean
     */
    public function getIsproductionaffect() {
        return $this->isproductionaffect;
    }
}

