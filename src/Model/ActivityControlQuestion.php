<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * ActivityControlQuestion
 */
abstract class ActivityControlQuestion extends Base implements BaseInterface, ActivityControlQuestionInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $documentnumber;

    /**
     * @var string
     */
    protected $question;

    /**
     * @var string
     */
    protected $answertype;

    /**
     * @var string
     */
    protected $valuerequire;

    /**
     * @var string
     */
    protected $valuemin;

    /**
     * @var string
     */
    protected $valuemax;

    /**
     * @var string
     */
    protected $period;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ActivityControlQuestion
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ActivityControlQuestion
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ActivityControlQuestion
     */
    public function setDocumentnumber($documentnumber)
    {
        $this->documentnumber = $documentnumber;

        return $this;
    }

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber()
    {
        return $this->documentnumber;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return ActivityControlQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answertype
     *
     * @param string $answertype
     *
     * @return ActivityControlQuestion
     */
    public function setAnswertype($answertype)
    {
        $this->answertype = $answertype;

        return $this;
    }

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype()
    {
        return $this->answertype;
    }

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return ActivityControlQuestion
     */
    public function setValuerequire($valuerequire)
    {
        $this->valuerequire = $valuerequire;

        return $this;
    }

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire()
    {
        return $this->valuerequire;
    }

    /**
     * Set valuemin
     *
     * @param string $valuemin
     *
     * @return ActivityControlQuestion
     */
    public function setValuemin($valuemin)
    {
        $this->valuemin = $valuemin;

        return $this;
    }

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin()
    {
        return $this->valuemin;
    }

    /**
     * Set valuemax
     *
     * @param string $valuemax
     *
     * @return ActivityControlQuestion
     */
    public function setValuemax($valuemax)
    {
        $this->valuemax = $valuemax;

        return $this;
    }

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax()
    {
        return $this->valuemax;
    }

    /**
     * Set period
     *
     * @param string $period
     *
     * @return ActivityControlQuestion
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ActivityControlQuestion
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ActivityControlQuestion
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
}

