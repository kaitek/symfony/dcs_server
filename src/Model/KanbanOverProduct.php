<?php

namespace App\Model;

/**
 * KanbanOverProduct
 */
abstract class KanbanOverProduct implements KanbanOverProductInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $product;

    /**
     * @var integer
     */
    protected $boxcount;

    /**
     * @var integer
     */
    protected $currentboxcount;

    /**
     * @var \DateTime
     */
    protected $time;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return KanbanOverProduct
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set product
     *
     * @param string $product
     *
     * @return KanbanOverProduct
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set boxcount
     *
     * @param integer $boxcount
     *
     * @return KanbanOverProduct
     */
    public function setBoxcount($boxcount)
    {
        $this->boxcount = $boxcount;

        return $this;
    }

    /**
     * Get boxcount
     *
     * @return integer
     */
    public function getBoxcount()
    {
        return $this->boxcount;
    }

    /**
     * Set currentboxcount
     *
     * @param integer $currentboxcount
     *
     * @return KanbanOverProduct
     */
    public function setCurrentboxcount($currentboxcount)
    {
        $this->currentboxcount = $currentboxcount;

        return $this;
    }

    /**
     * Get currentboxcount
     *
     * @return integer
     */
    public function getCurrentboxcount()
    {
        return $this->currentboxcount;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return KanbanOverProduct
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }
}

