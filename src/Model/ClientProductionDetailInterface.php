<?php

namespace App\Model;

/**
 * ClientProductionDetailInterface
 */
interface ClientProductionDetailInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ClientProductionDetailInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientProductionDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ClientProductionDetailInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return ClientProductionDetailInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Set tasklist
     *
     * @param string $tasklist
     * @return ClientProductionDetailInterface
     */
    public function setTasklist($tasklist);

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return ClientProductionDetailInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime();

    /**
     * Set mould
     *
     * @param string $mould
     * @return ClientProductionDetailInterface
     */
    public function setMould($mould);

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould();

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     * @return ClientProductionDetailInterface
     */
    public function setMouldgroup($mouldgroup);

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return ClientProductionDetailInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return ClientProductionDetailInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ClientProductionDetailInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return ClientProductionDetailInterface
     */
    public function setOpdescription($opdescription);

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription();

    /**
     * Set leafmask
     *
     * @param string $leafmask
     * @return ClientProductionDetailInterface
     */
    public function setLeafmask($leafmask);

    /**
     * Get leafmask
     *
     * @return string
     */
    public function getLeafmask();

    /**
     * Set leafmaskcurrent
     *
     * @param string $leafmaskcurrent
     * @return ClientProductionDetailInterface
     */
    public function setLeafmaskcurrent($leafmaskcurrent);

    /**
     * Get leafmaskcurrent
     *
     * @return string
     */
    public function getLeafmaskcurrent();

    /**
     * Set production
     *
     * @param integer $production
     * @return ClientProductionDetailInterface
     */
    public function setProduction($production);

    /**
     * Get production
     *
     * @return integer
     */
    public function getProduction();

    /**
     * Set productioncurrent
     *
     * @param integer $productioncurrent
     * @return ClientProductionDetailInterface
     */
    public function setProductioncurrent($productioncurrent);

    /**
     * Get productioncurrent
     *
     * @return integer
     */
    public function getProductioncurrent();

    /**
     * Set employee
     *
     * @param string $employee
     * @return ClientProductionDetailInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string
     */

    public function getEmployee();
    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return ClientProductionDetailInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Set gap
     *
     * @param integer $gap
     *
     * @return ClientProductionDetailInterface
     */
    public function setGap($gap);

    /**
     * Get gap
     *
     * @return int
     */
    public function getGap();

    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return ClientProductionDetailInterface
     */
    public function setTaskfromerp($taskfromerp);

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ClientProductionDetailInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ClientProductionDetailInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Set energy
     *
     * @param decimal $energy
     *
     * @return ClientProductionDetailInterface
     */
    public function setEnergy($energy);

    /**
     * Get energy
     *
     * @return decimal
     */
    public function getEnergy();

    /**
     * Set taskfinishtype
     *
     * @param string $taskfinishtype
     *
     * @return ClientProductionDetailInterface
     */
    public function setTaskfinishtype($taskfinishtype);

    /**
     * Get taskfinishtype
     *
     * @return string
     */
    public function getTaskfinishtype();

    /**
     * Set taskfinishdescription
     *
     * @param string $taskfinishdescription
     *
     * @return ClientProductionDetailInterface
     */
    public function setTaskfinishdescription($taskfinishdescription);

    /**
     * Get taskfinishdescription
     *
     * @return string
     */
    public function getTaskfinishdescription();

    /**
     * Set calculatedtpp
     *
     * @param decimal $calculatedtpp
     *
     * @return ClientProductionDetailInterface
     */
    public function setCalculatedtpp($calculatedtpp);

    /**
     * Get calculatedtpp
     *
     * @return decimal
     */
    public function getCalculatedtpp();

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ClientProductionDetailInterface
     */
    public function setIsexported($isexported);

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported();

    /**
     * Set opsayi
     *
     * @param integer $opsayi
     *
     * @return ClientProductionDetailInterface
     */
    public function setOpsayi($opsayi);

    /**
     * Get opsayi
     *
     * @return integer
     */
    public function getOpsayi();

    /**
     * Set refsayi
     *
     * @param integer $refsayi
     *
     * @return ClientProductionDetailInterface
     */
    public function setRefsayi($refsayi);

    /**
     * Get refsayi
     *
     * @return integer
     */
    public function getRefsayi();

    /**
     * Set losttime
     *
     * @param integer $losttime
     *
     * @return ClientProductionDetailInterface
     */
    public function setLosttime($losttime);

    /**
     * Get losttime
     *
     * @return integer
     */
    public function getLosttime();

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return ClientProductionDetailInterface
     */
    public function setSerialnumber($serialnumber);

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber();
}
