<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * MouldDetail
 */
abstract class MouldDetail extends BaseSync implements BaseSyncInterface, MouldDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $mould;

    /**
     * @var string
     */
    protected $mouldgroup;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $leafmask;
    
    /**
     * @var string
     */
    protected $leafmaskcurrent;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mould
     *
     * @param string $mould
     *
     * @return MouldDetail
     */
    public function setMould($mould)
    {
        $this->mould = $mould;

        return $this;
    }

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould()
    {
        return $this->mould;
    }

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     *
     * @return MouldDetail
     */
    public function setMouldgroup($mouldgroup)
    {
        $this->mouldgroup = $mouldgroup;

        return $this;
    }

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup()
    {
        return $this->mouldgroup;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return MouldDetail
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return MouldDetail
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber!==null?intval($opnumber):null;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return MouldDetail
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set leafmask
     *
     * @param string $leafmask
     *
     * @return MouldDetail
     */
    public function setLeafmask($leafmask)
    {
        $this->leafmask = $leafmask;

        return $this;
    }

    /**
     * Get leafmask
     *
     * @return string
     */
    public function getLeafmask()
    {
        return $this->leafmask;
    }
    
    /**
     * Get leafmaskcurrent
     *
     * @return string
     */
    function getLeafmaskcurrent() {
        return $this->leafmaskcurrent;
    }

    /**
     * Set leafmaskcurrent
     *
     * @param string $leafmaskcurrent
     *
     * @return MouldDetail
     */
    function setLeafmaskcurrent($leafmaskcurrent) {
        $this->leafmaskcurrent = $leafmaskcurrent;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return MouldDetail
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return MouldDetail
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
}

