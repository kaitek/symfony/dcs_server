<?php
namespace App\Model;

/**
 * CaseCallInterface
 */
interface CaseCallInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     *
     * @return CaseCallInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return CaseCallInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return CaseCallInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return CaseCallInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return CaseCallInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set state
     *
     * @param string $state
     *
     * @return CaseCallInterface
     */
    public function setState($state);

    /**
     * Get state
     *
     * @return string
     */
    public function getState();

    /**
     * Set casemovementId
     *
     * @param integer $casemovementId
     *
     * @return CaseCallInterface
     */
    public function setCasemovementId($casemovementId);

    /**
     * Get casemovementId
     *
     * @return integer
     */
    public function getCasemovementId();

    /**
     * Set calltime
     *
     * @param \DateTime $calltime
     *
     * @return CaseCallInterface
     */
    public function setCalltime($calltime);

    /**
     * Get calltime
     *
     * @return \DateTime
     */
    public function getCalltime();

    /**
     * Set starttime
     *
     * @param \DateTime $starttime
     *
     * @return CaseCallInterface
     */
    public function setStarttime($starttime);

    /**
     * Get starttime
     *
     * @return \DateTime
     */
    public function getStarttime();

    /**
     * Set assignmenttime
     *
     * @param \DateTime $assignmenttime
     *
     * @return CaseCallInterface
     */
    public function setAssignmenttime($assignmenttime);

    /**
     * Get assignmenttime
     *
     * @return \DateTime
     */
    public function getAssignmenttime();

    /**
     * Set actualfinishtime
     *
     * @param \DateTime $actualfinishtime
     *
     * @return CaseCallInterface
     */
    public function setActualfinishtime($actualfinishtime);

    /**
     * Get actualfinishtime
     *
     * @return \DateTime
     */
    public function getActualfinishtime();
}
