<?php

namespace App\Model;

/**
 * TaskListLaserDetailInterface
 */
interface TaskListLaserDetailInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set tasklistlaser
     *
     * @param string $tasklistlaser
     * @return TaskListLaserDetailInterface
     */
    public function setTasklistlaser($tasklistlaser);

    /**
     * Get tasklistlaser
     *
     * @return string
     */
    public function getTasklistlaser();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return TaskListLaserDetailInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return TaskListLaserDetailInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return TaskListLaserDetailInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set leafmask
     *
     * @param integer $leafmask
     * @return TaskListLaserDetailInterface
     */
    public function setLeafmask($leafmask);

    /**
     * Get leafmask
     *
     * @return integer
     */
    public function getLeafmask();

    /**
     * Set productcount
     *
     * @param integer $productcount
     *
     * @return TaskListLaserDetailInterface
     */
    public function setProductcount($productcount);

    /**
     * Get productcount
     *
     * @return integer
     */
    public function getProductcount();


}
