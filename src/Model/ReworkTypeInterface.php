<?php
namespace App\Model;

/**
 * ReworkTypeInterface
 */
interface ReworkTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return ReworkTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set description
     *
     * @param string $description
     * @return ReworkTypeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();
    
    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ReworkTypeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ReworkTypeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();
    
}
