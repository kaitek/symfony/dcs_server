<?php

namespace App\Model;

/**
 * CarrierEmptySession
 */
abstract class CarrierEmptySession implements CarrierEmptySessionInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $carrier;

    /**
     * @var string
     */
    protected $empcode;

    /**
     * @var string
     */
    protected $empname;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carrier
     *
     * @param string $carrier
     *
     * @return CarrierEmptySession
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set empcode
     *
     * @param string $empcode
     *
     * @return CarrierEmptySession
     */
    public function setEmpcode($empcode)
    {
        $this->empcode = $empcode;

        return $this;
    }

    /**
     * Get empcode
     *
     * @return string
     */
    public function getEmpcode()
    {
        return $this->empcode;
    }

    /**
     * Set empname
     *
     * @param string $empname
     *
     * @return CarrierEmptySession
     */
    public function setEmpname($empname)
    {
        $this->empname = $empname;

        return $this;
    }

    /**
     * Get empname
     *
     * @return string
     */
    public function getEmpname()
    {
        return $this->empname;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return CarrierEmptySession
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return CarrierEmptySession
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return CarrierEmptySession
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return CarrierEmptySession
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }
}

