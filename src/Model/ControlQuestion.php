<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * ControlQuestion
 */
abstract class ControlQuestion extends BaseSync implements BaseSyncInterface, ControlQuestionInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $question;

    /**
     * @var string
     */
    protected $answertype;

    /**
     * @var string
     */
    protected $valuerequire;

    /**
     * @var string
     */
    protected $valuemin;

    /**
     * @var string
     */
    protected $valuemax;

    /**
     * @var string
     */
    protected $documentnumber;

    /**
     * @var string
     */
    protected $clients;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var bool
     */
    protected $isallowna;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ControlQuestion
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ControlQuestion
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return ControlQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answertype
     *
     * @param string $answertype
     *
     * @return ControlQuestion
     */
    public function setAnswertype($answertype)
    {
        $this->answertype = $answertype;

        return $this;
    }

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype()
    {
        return $this->answertype;
    }

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return ControlQuestion
     */
    public function setValuerequire($valuerequire)
    {
        $this->valuerequire = $valuerequire;

        return $this;
    }

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire()
    {
        return $this->valuerequire;
    }

    /**
     * Set valuemin
     *
     * @param string $valuemin
     *
     * @return ControlQuestion
     */
    public function setValuemin($valuemin)
    {
        $this->valuemin = $valuemin;

        return $this;
    }

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin()
    {
        return $this->valuemin;
    }

    /**
     * Set valuemax
     *
     * @param string $valuemax
     *
     * @return ControlQuestion
     */
    public function setValuemax($valuemax)
    {
        $this->valuemax = $valuemax;

        return $this;
    }

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax()
    {
        return $this->valuemax;
    }

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ControlQuestion
     */
    public function setDocumentnumber($documentnumber)
    {
        $this->documentnumber = $documentnumber;

        return $this;
    }

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber()
    {
        return $this->documentnumber;
    }

    /**
     * Set clients
     *
     * @param string $clients
     *
     * @return ControlQuestion
     */
    public function setClients($clients)
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return ControlQuestion
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set isallowna
     *
     * @param boolean $isallowna
     *
     * @return ControlQuestion
     */
    public function setIsallowna($isallowna)
    {
        $this->isallowna = $isallowna;

        return $this;
    }

    /**
     * Get isallowna
     *
     * @return boolean
     */
    public function getIsallowna()
    {
        return $this->isallowna;
    }
}
