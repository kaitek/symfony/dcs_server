<?php

namespace App\Model;

/**
 * TaskSession
 */
abstract class TaskSession implements TaskSessionInterface
{
    /**
     * @var integer
     */
    protected $id;
    
    /**
     * @var string
     */
    protected $idx;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;
    
    /**
     * @var string
     */
    protected $opname;
    
    /**
     * @var string
     */
    protected $opdescription;

    /**
     * @var integer
     */
    protected $productcount;
    
    /**
     * @var integer
     */
    protected $productdonecount;
    
    /**
     * @var integer
     */
    protected $productdoneactivity;
      
    /**
     * @var integer
     */
    protected $scrapactivity;
    
    /**
     * @var integer
     */
    protected $scrappart;

    /**
     * @var \DateTime
     */
    protected $deadline;

    /**
     * @var string
     */
    protected $client;
    
    /**
     * @var string
     */
    protected $type;

    /**
     * @var decimal
     */
    protected $tpp;

    /**
     * @var bool
     */
    protected $taskfromerp;
    
    /**
     * @var \DateTime
     */
    protected $start;
    
    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var string
     */
    protected $taskfinishtype;

    /**
     * @var string
     */
    protected $finishdescription;

    /**
     * @var \DateTime
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     */
    protected $plannedfinish;

    /**
     * @var string
     */
    protected $resourceid;

    /**
     * @var string
     */
    protected $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idx
     *
     * @param string $idx
     *
     * @return TaskSession
     */
    public function setIdx($idx)
    {
        $this->idx = $idx;

        return $this;
    }

    /**
     * Get idx
     *
     * @return string
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskSession
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return TaskSession
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return TaskSession
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return TaskSession
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskSession
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }
    
    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return TaskSession
     */
    public function setOpdescription($opdescription)
    {
        $this->opdescription = $opdescription;
    }
    
    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription()
    {
        return $this->opdescription;
    }
    
    /**
     * Set productcount
     *
     * @param integer $productcount
     *
     * @return TaskSession
     */
    public function setProductcount($productcount)
    {
        $this->productcount = $productcount;

        return $this;
    }

    /**
     * Get productcount
     *
     * @return integer
     */
    public function getProductcount()
    {
        return $this->productcount;
    }

    /**
     * Set productdonecount
     *
     * @param integer $productdonecount
     *
     * @return TaskSession
     */
    public function setProductdonecount($productdonecount)
    {
        $this->productdonecount = $productdonecount;

        return $this;
    }

    /**
     * Get productdonecount
     *
     * @return int
     */
    public function getProductdonecount()
    {
        return $this->productdonecount;
    }
    
    /**
     * Set productdoneactivity
     *
     * @param integer $productdoneactivity
     *
     * @return TaskSession
     */
    public function setProductdoneactivity($productdoneactivity)
    {
        $this->productdoneactivity = $productdoneactivity;
    }
    
    /**
     * Get productdoneactivity
     *
     * @return int
     */
    public function getProductdoneactivity()
    {
        return $this->productdoneactivity;
    }

    /**
     * Set scrapactivity
     *
     * @param integer $scrapactivity
     *
     * @return TaskSession
     */
    public function setScrapactivity($scrapactivity)
    {
        $this->scrapactivity = $scrapactivity;
    }
    
    /**
     * Get scrapactivity
     *
     * @return int
     */
    public function getScrapactivity()
    {
        return $this->scrapactivity;
    }

    /**
     * Set scrappart
     *
     * @param integer $scrappart
     *
     * @return TaskSession
     */
    public function setScrappart($scrappart)
    {
        $this->scrappart = $scrappart;
    }
    
    /**
     * Get scrappart
     *
     * @return int
     */
    public function getScrappart()
    {
        return $this->scrappart;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return TaskSession
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return TaskSession
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     * Set type
     *
     * @param string $type
     *
     * @return TaskSession
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set tpp
     *
     * @param decimal $tpp
     *
     * @return TaskSession
     */
    public function setTpp($tpp)
    {
        $this->tpp = $tpp;

        return $this;
    }

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp()
    {
        return $this->tpp;
    }
    
    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return TaskSession
     */
    public function setTaskfromerp($taskfromerp)
    {
        $this->taskfromerp = $taskfromerp;

        return $this;
    }

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp()
    {
        return $this->taskfromerp;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return TaskSession
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }
    
    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return TaskSession
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set taskfinishtype
     *
     * @param string $taskfinishtype
     *
     * @return TaskSession
     */
    public function setTaskfinishtype($taskfinishtype)
    {
        $this->taskfinishtype = $taskfinishtype;
    }
    
    /**
     * Get taskfinishtype
     *
     * @return string
     */
    public function getTaskfinishtype()
    {
        return $this->taskfinishtype;
    }

    /**
     * Set finishdescription
     *
     * @param string $finishdescription
     *
     * @return TaskSession
     */
    public function setFinishdescription($finishdescription)
    {
        $this->finishdescription = $finishdescription;
    }
    
    /**
     * Get finishdescription
     *
     * @return string
     */
    public function getFinishdescription()
    {
        return $this->finishdescription;
    }

    /**
     * Set plannedstart
     *
     * @param \DateTime $plannedstart
     *
     * @return TaskSession
     */
    public function setPlannedstart($plannedstart)
    {
        $this->plannedstart = $plannedstart;

        return $this;
    }

    /**
     * Get plannedstart
     *
     * @return \DateTime
     */
    public function getPlannedstart()
    {
        return $this->plannedstart;
    }

    /**
     * Set plannedfinish
     *
     * @param \DateTime $plannedfinish
     *
     * @return TaskSession
     */
    public function setPlannedfinish($plannedfinish)
    {
        $this->plannedfinish = $plannedfinish;

        return $this;
    }

    /**
     * Get plannedfinish
     *
     * @return \DateTime
     */
    public function getPlannedfinish()
    {
        return $this->plannedfinish;
    }

    /**
     * Set resourceid
     *
     * @param string $resourceid
     *
     * @return TaskSession
     */
    public function setResourceid($resourceid)
    {
        $this->resourceid = $resourceid;

        return $this;
    }

    /**
     * Get resourceid
     *
     * @return string
     */
    public function getResourceid()
    {
        return $this->resourceid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskSession
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
