<?php
namespace App\Model;

/**
 * ActivityControlAnswerInterface
 */
interface ActivityControlAnswerInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ActivityControlAnswerInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string 
     */
    public function getType();

    /**
     * Set client
     *
     * @param string $client
     * @return ActivityControlAnswerInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ActivityControlAnswerInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return ActivityControlAnswerInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ActivityControlAnswerInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ActivityControlAnswerInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set employee
     *
     * @param string $employee
     * @return ActivityControlAnswerInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return ActivityControlAnswerInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime();

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ActivityControlAnswerInterface
     */
    public function setDocumentnumber($documentnumber);

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber();

    /**
     * Set question
     *
     * @param string $question
     * @return ActivityControlAnswerInterface
     */
    public function setQuestion($question);

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion();

    /**
     * Set answertype
     *
     * @param string $answertype
     * @return ActivityControlAnswerInterface
     */
    public function setAnswertype($answertype);

    /**
     * Get answertype
     *
     * @return string 
     */
    public function getAnswertype();

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     * @return ActivityControlAnswerInterface
     */
    public function setValuerequire($valuerequire);

    /**
     * Get valuerequire
     *
     * @return string 
     */
    public function getValuerequire();

    /**
     * Set valuemin
     *
     * @param string $valuemin
     * @return ActivityControlAnswerInterface
     */
    public function setValuemin($valuemin);

    /**
     * Get valuemin
     *
     * @return string 
     */
    public function getValuemin();

    /**
     * Set valuemax
     *
     * @param string $valuemax
     * @return ActivityControlAnswerInterface
     */
    public function setValuemax($valuemax);

    /**
     * Get valuemax
     *
     * @return string 
     */
    public function getValuemax();

    /**
     * Set answer
     *
     * @param string $answer
     * @return ActivityControlAnswerInterface
     */
    public function setAnswer($answer);

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer();

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ActivityControlAnswerInterface
     */
    public function setDescription($description);
    
    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ActivityControlAnswerInterface
     */
    public function setIsexported($isexported);

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported();
    
}
