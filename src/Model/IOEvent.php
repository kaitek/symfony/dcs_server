<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * IOEvent
 */
abstract class IOEvent extends BaseSync implements BaseSyncInterface, IOEventInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $iotype;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return IOEvent
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set iotype
     *
     * @param string $iotype
     *
     * @return IOEvent
     */
    public function setIotype($iotype)
    {
        $this->iotype = $iotype;

        return $this;
    }

    /**
     * Get iotype
     *
     * @return string
     */
    public function getIotype()
    {
        return $this->iotype;
    }

}

