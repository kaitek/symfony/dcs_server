<?php
namespace App\Model;

/**
 * LostTypeInterface
 */
interface LostTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return LostTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set extcode
     *
     * @param string $extcode
     *
     * @return LostTypeInterface
     */
    public function setExtcode($extcode);

    /**
     * Get extcode
     *
     * @return string
     */
    public function getExtcode();

    /**
     * Set description
     *
     * @param string $description
     * @return LostTypeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set isemployeerequired
     *
     * @param boolean $isemployeerequired
     * @return LostTypeInterface
     */
    public function setIsemployeerequired($isemployeerequired);

    /**
     * Get isemployeerequired
     *
     * @return boolean 
     */
    public function getIsemployeerequired();

    /**
     * Set isexpertemployeerequired
     *
     * @param boolean $isexpertemployeerequired
     * @return LostTypeInterface
     */
    public function setIsexpertemployeerequired($isexpertemployeerequired);

    /**
     * Get isexpertemployeerequired
     *
     * @return boolean 
     */
    public function getIsexpertemployeerequired();

    /**
     * Set time
     *
     * @param integer $time
     * @return LostTypeInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return integer 
     */
    public function getTime();

    /**
     * Set isoperationnumberrequired
     *
     * @param boolean $isoperationnumberrequired
     * @return LostTypeInterface
     */
    public function setIsoperationnumberrequired($isoperationnumberrequired);

    /**
     * Get isoperationnumberrequired
     *
     * @return boolean 
     */
    public function getIsoperationnumberrequired();
    
    /**
     * Set ioevent
     *
     * @param string $ioevent
     *
     * @return LostTypeInterface
     */
    public function setIOEvent($ioevent);

    /**
     * Get ioevent
     *
     * @return string
     */
    public function getIOEvent();
    
    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return LostTypeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return LostTypeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();
    
    /**
     * Set closenoemployee
     *
     * @param boolean $closenoemployee
     *
     * @return LostTypeInterface
     */
    public function setClosenoemployee($closenoemployee);
    
    /**
     * Get closenoemployee
     *
     * @return boolean
     */
    public function getClosenoemployee();
    
    /**
     * Set islisted
     *
     * @param boolean $islisted
     *
     * @return LostTypeInterface
     */
    public function setIslisted($islisted);
    
    /**
     * Get islisted
     *
     * @return boolean
     */
    public function getIslisted();

    /**
     * Set isclient
     *
     * @param boolean $isclient
     *
     * @return LostTypeInterface
     */
    public function setIsclient($isclient);
    
    /**
     * Get isclient
     *
     * @return boolean
     */
    public function getIsclient();

    /**
     * Set isemployee
     *
     * @param boolean $isemployee
     *
     * @return LostTypeInterface
     */
    public function setIsemployee($isemployee);
    
    /**
     * Get isemployee
     *
     * @return boolean
     */
    public function getIsemployee();

    /**
     * Set isshortlost
     *
     * @param boolean $isshortlost
     *
     * @return LostTypeInterface
     */
    public function setIsshortlost($isshortlost);
    
    /**
     * Get isshortlost
     *
     * @return boolean
     */
    public function getIsshortlost();

    /**
     * Set issourcelookup
     *
     * @param boolean $issourcelookup
     *
     * @return LostTypeInterface
     */
    public function setIssourcelookup($issourcelookup);
    
    /**
     * Get issourcelookup
     *
     * @return boolean
     */
    public function getIssourcelookup();

    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return LostTypeInterface
     */
    public function setListorder($listorder);

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder();
    
    /**
     * Set authorizeloststart
     *
     * @param string $authorizeloststart
     *
     * @return LostTypeInterface
     */
    public function setAuthorizeloststart($authorizeloststart);

    /**
     * Get authorizeloststart
     *
     * @return string
     */
    public function getAuthorizeloststart();

    /**
     * Set authorizelostfinish
     *
     * @param string $authorizelostfinish
     *
     * @return LostTypeInterface
     */
    public function setAuthorizelostfinish($authorizelostfinish);

    /**
     * Get authorizelostfinish
     *
     * @return string
     */
    public function getAuthorizelostfinish();

    /**
     * Set isdescriptionrequired
     *
     * @param boolean $isdescriptionrequired
     *
     * @return LostTypeInterface
     */
    public function setIsdescriptionrequired($isdescriptionrequired);

    /**
     * Get isdescriptionrequired
     *
     * @return boolean
     */
    public function getIsdescriptionrequired();

    /**
     * Set isopenallports
     *
     * @param boolean $isopenallports
     *
     * @return LostTypeInterface
     */
    public function setIsopenallports($isopenallports);

    /**
     * Get isopenallports
     *
     * @return boolean
     */
    public function getIsopenallports();
}
