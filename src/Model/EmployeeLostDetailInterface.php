<?php
namespace App\Model;

/**
 * EmployeeLostDetailInterface
 */
interface EmployeeLostDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return EmployeeLostDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return EmployeeLostDetailInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return EmployeeLostDetailInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set teamleader
     *
     * @param string $teamleader
     *
     * @return EmployeeLostDetailInterface
     */
    public function setTeamleader($teamleader);

    /**
     * Get teamleader
     *
     * @return string
     */
    public function getTeamleader();

    /**
     * Set week
     *
     * @param integer $week
     * @return EmployeeLostDetailInterface
     */
    public function setWeek($week);

    /**
     * Get week
     *
     * @return integer 
     */
    public function getWeek();

    /**
     * Set employee
     *
     * @param string $employee
     * @return EmployeeLostDetailInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return EmployeeLostDetailInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return EmployeeLostDetailInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    /**
     * Set duration
     *
     * @param integer $duration
     * @return EmployeeLostDetailInterface
     */
    public function setDuration($duration);

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration();

    /**
     * Set status
     *
     * @param string $status
     * @return EmployeeLostDetailInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus();

    /**
     * Set ismandatory
     *
     * @param boolean $ismandatory
     * @return EmployeeLostDetailInterface
     */
    public function setIsmandatory($ismandatory);

    /**
     * Get ismandatory
     *
     * @return boolean 
     */
    public function getIsmandatory();

    /**
     * Set isrework
     *
     * @param boolean $isrework
     * @return EmployeeLostDetailInterface
     */
    public function setIsrework($isrework);

    /**
     * Get isrework
     *
     * @return boolean 
     */
    public function getIsrework();

    /**
     * Set description
     *
     * @param string $description
     * @return EmployeeLostDetailInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();
    
}
