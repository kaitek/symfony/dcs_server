<?php
namespace App\Model;

/**
 * MovementTaskTypeInterface
 */
interface MovementTaskTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set tasktype
     *
     * @param string $tasktype
     * @return MovementTaskTypeInterface
     */
    public function setTasktype($tasktype);

    /**
     * Get tasktype
     *
     * @return string 
     */
    public function getTasktype();

    /**
     * Set description
     *
     * @param string $description
     * @return MovementTaskTypeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set label
     *
     * @param string $label
     * @return MovementTaskTypeInterface
     */
    public function setLabel($label);

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel();
    
}
