<?php
namespace App\Model;

/**
 * JobRotationEmployeeInterface
 */
interface JobRotationEmployeeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return JobRotationEmployeeInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set week
     *
     * @param integer $week
     * @return JobRotationEmployeeInterface
     */
    public function setWeek($week);

    /**
     * Get week
     *
     * @return integer 
     */
    public function getWeek();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return JobRotationEmployeeInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set jobrotationteam
     *
     * @param string $jobrotationteam
     * @return JobRotationEmployeeInterface
     */
    public function setJobrotationteam($jobrotationteam);

    /**
     * Get jobrotationteam
     *
     * @return string 
     */
    public function getJobrotationteam();

    /**
     * Set employee
     *
     * @param string $employee
     * @return JobRotationEmployeeInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set isovertime
     *
     * @param boolean $isovertime
     * @return JobRotationEmployeeInterface
     */
    public function setIsovertime($isovertime);

    /**
     * Get isovertime
     *
     * @return boolean 
     */
    public function getIsovertime();

    /**
     * Set beginval
     *
     * @param string $beginval
     * @return JobRotationEmployeeInterface
     */
    public function setBeginval($beginval);

    /**
     * Get beginval
     *
     * @return string 
     */
    public function getBeginval();

    /**
     * Set endval
     *
     * @param string $endval
     * @return JobRotationEmployeeInterface
     */
    public function setEndval($endval);

    /**
     * Get endval
     *
     * @return string 
     */
    public function getEndval();

    /**
     * Set worktime
     *
     * @param integer $worktime
     * @return JobRotationEmployeeInterface
     */
    public function setWorktime($worktime);

    /**
     * Get worktime
     *
     * @return integer 
     */
    public function getWorktime();

    /**
     * Set breaktime
     *
     * @param integer $breaktime
     * @return JobRotationEmployeeInterface
     */
    public function setBreaktime($breaktime);

    /**
     * Get breaktime
     *
     * @return integer 
     */
    public function getBreaktime();
}
