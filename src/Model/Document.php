<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * Document
 */
abstract class Document extends BaseSync implements BaseSyncInterface, DocumentInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var integer
     */
    protected $currentversion;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var bool
     */
    protected $islot;

    /**
     * @var \DateTime
     */
    protected $lastupdate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Document
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return Document
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return Document
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return Document
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set currentversion
     *
     * @param integer $currentversion
     *
     * @return Document
     */
    public function setCurrentversion($currentversion)
    {
        $this->currentversion = $currentversion;

        return $this;
    }

    /**
     * Get currentversion
     *
     * @return integer
     */
    public function getCurrentversion()
    {
        return $this->currentversion;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Document
     */
    public function setDescription($description) {
        $this->description = $description;
    }
    
    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set islot
     *
     * @param boolean $islot
     *
     * @return Document
     */
    public function setIslot($islot)
    {
        $this->islot = $islot;

        return $this;
    }

    /**
     * Get islot
     *
     * @return boolean
     */
    public function getIslot()
    {
        return $this->islot;
    }

    /**
     * Set lastupdate
     *
     * @param \DateTime $lastupdate
     *
     * @return Document
     */
    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    /**
     * Get lastupdate
     *
     * @return \DateTime
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }
}

