<?php
namespace App\Model;

/**
 * EmployeeEfficiencyInterface
 */
interface EmployeeEfficiencyInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return EmployeeEfficiencyInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return EmployeeEfficiencyInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Set teamleader
     *
     * @param string $teamleader
     *
     * @return EmployeeEfficiencyInterface
     */
    public function setTeamleader($teamleader);

    /**
     * Get teamleader
     *
     * @return string
     */
    public function getTeamleader();

    /**
     * Set week
     *
     * @param integer $week
     * @return EmployeeEfficiencyInterface
     */
    public function setWeek($week);

    /**
     * Get week
     *
     * @return integer 
     */
    public function getWeek();

    /**
     * Set employee
     *
     * @param string $employee
     * @return EmployeeEfficiencyInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set isovertime
     *
     * @param boolean $isovertime
     * @return EmployeeEfficiencyInterface
     */
    public function setIsovertime($isovertime);

    /**
     * Get isovertime
     *
     * @return boolean 
     */
    public function getIsovertime();

    /**
     * Set fulltime
     *
     * @param string $fulltime
     * @return EmployeeEfficiencyInterface
     */
    public function setFulltime($fulltime);

    /**
     * Get fulltime
     *
     * @return string 
     */
    public function getFulltime();

    /**
     * Set breaktime
     *
     * @param string $breaktime
     * @return EmployeeEfficiencyInterface
     */
    public function setBreaktime($breaktime);

    /**
     * Get breaktime
     *
     * @return string 
     */
    public function getBreaktime();

    /**
     * Set worktime
     *
     * @param string $worktime
     * @return EmployeeEfficiencyInterface
     */
    public function setWorktime($worktime);

    /**
     * Get worktime
     *
     * @return string 
     */
    public function getWorktime();

    /**
     * Set optime
     *
     * @param string $optime
     * @return EmployeeEfficiencyInterface
     */
    public function setOptime($optime);

    /**
     * Get optime
     *
     * @return string 
     */
    public function getOptime();

    /**
     * Set discontinuity
     *
     * @param string $discontinuity
     *
     * @return EmployeeEfficiency
     */
    public function setDiscontinuity($discontinuity);

    /**
     * Get discontinuity
     *
     * @return string
     */
    public function getDiscontinuity();

    /**
     * Set losttime
     *
     * @param string $losttime
     * @return EmployeeEfficiencyInterface
     */
    public function setLosttime($losttime);

    /**
     * Get losttime
     *
     * @return string 
     */
    public function getLosttime();

    /**
     * Set reworkoptime
     *
     * @param string $reworkoptime
     * @return EmployeeEfficiencyInterface
     */
    public function setReworkoptime($reworkoptime);

    /**
     * Get reworkoptime
     *
     * @return string 
     */
    public function getReworkoptime();

    /**
     * Set mreworkduration
     *
     * @param string $mreworkduration
     * @return EmployeeEfficiencyInterface
     */
    public function setMreworkduration($mreworkduration);

    /**
     * Get mreworkduration
     *
     * @return string 
     */
    public function getMreworkduration();

    /**
     * Set mlosttime
     *
     * @param string $mlosttime
     * @return EmployeeEfficiencyInterface
     */
    public function setMlosttime($mlosttime);

    /**
     * Get mlosttime
     *
     * @return string 
     */
    public function getMlosttime();

    /**
     * Set rlosttime
     *
     * @param string $rlosttime
     * @return EmployeeEfficiencyInterface
     */
    public function setRlosttime($rlosttime);

    /**
     * Get rlosttime
     *
     * @return string 
     */
    public function getRlosttime();

    /**
     * Set rmlosttime
     *
     * @param string $rmlosttime
     * @return EmployeeEfficiencyInterface
     */
    public function setRmlosttime($rmlosttime);

    /**
     * Get rmlosttime
     *
     * @return string 
     */
    public function getRmlosttime();

    /**
     * Set worktimewithoutlost
     *
     * @param string $worktimewithoutlost
     * @return EmployeeEfficiencyInterface
     */
    public function setWorktimewithoutlost($worktimewithoutlost);

    /**
     * Get worktimewithoutlost
     *
     * @return string 
     */
    public function getWorktimewithoutlost();

    /**
     * Set efficiency
     *
     * @param string $efficiency
     * @return EmployeeEfficiencyInterface
     */
    public function setEfficiency($efficiency);

    /**
     * Get efficiency
     *
     * @return string 
     */
    public function getEfficiency();

    /**
     * Set tempo
     *
     * @param string $tempo
     * @return EmployeeEfficiencyInterface
     */
    public function setTempo($tempo);

    /**
     * Get tempo
     *
     * @return string 
     */
    public function getTempo();
    
}
