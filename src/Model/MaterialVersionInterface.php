<?php
namespace App\Model;

/**
 * MaterialVersionInterface
 */
interface MaterialVersionInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set masterName
     *
     * @param string $masterName
     * @return MaterialVersionInterface
     */
    public function setMasterName($masterName);

    /**
     * Get masterName
     *
     * @return string 
     */
    public function getMasterName();

    /**
     * Set versionName
     *
     * @param string $versionName
     * @return MaterialVersionInterface
     */
    public function setVersionName($versionName);

    /**
     * Get versionName
     *
     * @return string 
     */
    public function getVersionName();

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return MaterialVersionInterface
     */
    public function setFullName($fullName);

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return MaterialVersionInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return MaterialVersionInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
}
