<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * TaskImport
 */
abstract class TaskImport extends Base implements BaseInterface, TaskImportInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $opdescription;

    /**
     * @var integer
     */
    protected $productcount;

    /**
     * @var \DateTime
     */
    protected $deadline;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var decimal
     */
    protected $tpp;

    /**
     * @var string
     */
    protected $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskImport
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return TaskImport
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return TaskImport
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return TaskImport
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber !== null ? intval($opnumber) : null;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskImport
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return TaskImport
     */
    public function setOpdescription($opdescription)
    {
        $this->opdescription = $opdescription;
    }

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription()
    {
        return $this->opdescription;
    }

    /**
     * Set productcount
     *
     * @param integer $productcount
     *
     * @return TaskImport
     */
    public function setProductcount($productcount)
    {
        $this->productcount = $productcount !== null ? intval($productcount) : null;

        return $this;
    }

    /**
     * Get productcount
     *
     * @return integer
     */
    public function getProductcount()
    {
        return $this->productcount;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return TaskImport
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return TaskImport
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return TaskImport
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set tpp
     *
     * @param decimal $tpp
     *
     * @return TaskImport
     */
    public function setTpp($tpp)
    {
        $this->tpp = $tpp;

        return $this;
    }

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp()
    {
        return $this->tpp;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskImport
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

}
