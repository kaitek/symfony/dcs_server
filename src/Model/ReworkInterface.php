<?php
namespace App\Model;

/**
 * ReworkInterface
 */
interface ReworkInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return ReworkInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set reworktype
     *
     * @param string $reworktype
     * @return ReworkInterface
     */
    public function setReworktype($reworktype);

    /**
     * Get reworktype
     *
     * @return string 
     */
    public function getReworktype();

    /**
     * Set code
     *
     * @param string $code
     * @return ReworkInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return ReworkInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string 
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return ReworkInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer 
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ReworkInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set opdescription
     *
     * @param string $opdescription
     * @return ReworkInterface
     */
    public function setOpdescription($opdescription);

    /**
     * Get opdescription
     *
     * @return string 
     */
    public function getOpdescription();

    /**
     * Set tpp
     *
     * @param string $tpp
     * @return ReworkInterface
     */
    public function setTpp($tpp);

    /**
     * Get tpp
     *
     * @return string 
     */
    public function getTpp();

    /**
     * Set islisted
     *
     * @param boolean $islisted
     * @return ReworkInterface
     */
    public function setIslisted($islisted);

    /**
     * Get islisted
     *
     * @return boolean 
     */
    public function getIslisted();

    /**
     * Set ismandatory
     *
     * @param boolean $ismandatory
     *
     * @return Rework
     */
    public function setIsmandatory($ismandatory);

    /**
     * Get ismandatory
     *
     * @return boolean
     */
    public function getIsmandatory();

    /**
     * Set listorder
     *
     * @param integer $listorder
     * @return ReworkInterface
     */
    public function setListorder($listorder);

    /**
     * Get listorder
     *
     * @return integer 
     */
    public function getListorder();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return ReworkInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return ReworkInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
}
