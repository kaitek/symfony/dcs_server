<?php

namespace App\Model;

/**
 * MovementTaskTypeCarrier
 */
abstract class MovementTaskTypeCarrier implements MovementTaskTypeCarrierInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $tasktype;

    /**
     * @var string
     */
    protected $carrier;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tasktype
     *
     * @param string $tasktype
     *
     * @return MovementTaskTypeCarrier
     */
    public function setTasktype($tasktype)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype
     *
     * @return string
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set carrier
     *
     * @param string $carrier
     *
     * @return MovementTaskTypeCarrier
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }
}

