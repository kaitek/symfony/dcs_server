<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * JobRotation
 */
abstract class JobRotation extends BaseSync implements BaseSyncInterface, JobRotationInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $beginval;

    /**
     * @var string
     */
    protected $endval;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return JobRotation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set beginval
     *
     * @param string $beginval
     *
     * @return JobRotation
     */
    public function setBeginval($beginval)
    {
        $this->beginval = $beginval;

        return $this;
    }

    /**
     * Get beginval
     *
     * @return string
     */
    public function getBeginval()
    {
        return $this->beginval;
    }

    /**
     * Set endval
     *
     * @param string $endval
     *
     * @return JobRotation
     */
    public function setEndval($endval)
    {
        $this->endval = $endval;

        return $this;
    }

    /**
     * Get endval
     *
     * @return string
     */
    public function getEndval()
    {
        return $this->endval;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return JobRotation
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return JobRotation
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    
}

