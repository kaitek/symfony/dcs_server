<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * TaskRejectDetail
 */
abstract class TaskRejectDetail extends Base implements BaseInterface, TaskRejectDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $rejecttype;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $materialunit;

    /**
     * @var string
     */
    protected $tasklist;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $mould;

    /**
     * @var string
     */
    protected $mouldgroup;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
    * @var string
    */
    protected $opdescription;

    /**
     * @var integer
     */
    protected $quantityreject;

    /**
     * @var integer
     */
    protected $quantityretouch;

    /**
     * @var integer
     */
    protected $quantityscrap;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var bool
     */
    protected $isexported;

    /**
     * @var string
     */
    protected $importfilename;

    /**
     * @var string
     */
    protected $description;

    /**
    * @var boolean
    */
    protected $ispreprocess;

    /**
     * @var json
     */
    protected $materials;

    /**
     * @var string
     */
    protected $rejectlot;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return TaskRejectDetail
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set rejecttype
     *
     * @param string $rejecttype
     *
     * @return TaskRejectDetail
     */
    public function setRejecttype($rejecttype)
    {
        $this->rejecttype = $rejecttype;

        return $this;
    }

    /**
     * Get rejecttype
     *
     * @return string
     */
    public function getRejecttype()
    {
        return $this->rejecttype;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return TaskRejectDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return TaskRejectDetail
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return TaskRejectDetail
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set materialunit
     *
     * @param string $materialunit
     *
     * @return TaskRejectDetail
     */
    public function setMaterialunit($materialunit)
    {
        $this->materialunit = $materialunit;
    }

    /**
     * Get materialunit
     *
     * @return string
     */
    public function getMaterialunit()
    {
        return $this->materialunit;
    }

    /**
     * Set tasklist
     *
     * @param string $tasklist
     *
     * @return TaskRejectDetail
     */
    public function setTasklist($tasklist)
    {
        $this->tasklist = $tasklist;

        return $this;
    }

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist()
    {
        return $this->tasklist;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return TaskRejectDetail
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set mould
     *
     * @param string $mould
     *
     * @return TaskRejectDetail
     */
    public function setMould($mould)
    {
        $this->mould = $mould;

        return $this;
    }

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould()
    {
        return $this->mould;
    }

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     *
     * @return TaskRejectDetail
     */
    public function setMouldgroup($mouldgroup)
    {
        $this->mouldgroup = $mouldgroup;

        return $this;
    }

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup()
    {
        return $this->mouldgroup;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return TaskRejectDetail
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return TaskRejectDetail
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskRejectDetail
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
    * Set opdescription
    *
    * @param string $opdescription
    *
    * @return TaskRejectDetail
    */
    public function setOpdescription($opdescription)
    {
        $this->opdescription = $opdescription;
    }

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription()
    {
        return $this->opdescription;
    }

    /**
     * Set quantityreject
     *
     * @param integer $quantityreject
     *
     * @return TaskRejectDetail
     */
    public function setQuantityReject($quantityreject)
    {
        $this->quantityreject = $quantityreject;

        return $this;
    }

    /**
     * Get quantityreject
     *
     * @return int
     */
    public function getQuantityReject()
    {
        return $this->quantityreject;
    }

    /**
     * Set quantityretouch
     *
     * @param integer $quantityretouch
     *
     * @return TaskRejectDetail
     */
    public function setQuantityRetouch($quantityretouch)
    {
        $this->quantityretouch = $quantityretouch;

        return $this;
    }

    /**
     * Get quantityretouch
     *
     * @return int
     */
    public function getQuantityRetouch()
    {
        return $this->quantityretouch;
    }

    /**
     * Set quantityscrap
     *
     * @param integer $quantityscrap
     *
     * @return TaskRejectDetail
     */
    public function setQuantityScrap($quantityscrap)
    {
        $this->quantityscrap = $quantityscrap;

        return $this;
    }

    /**
     * Get quantityscrap
     *
     * @return int
     */
    public function getQuantityScrap()
    {
        return $this->quantityscrap;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return TaskRejectDetail
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return TaskRejectDetail
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;
    }

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return TaskRejectDetail
     */
    public function setIsexported($isexported)
    {
        $this->isexported = $isexported;

        return $this;
    }

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported()
    {
        return $this->isexported;
    }

    /**
     * Set importfilename
     *
     * @param string $importfilename
     *
     * @return TaskRejectDetail
     */
    public function setImportfilename($importfilename)
    {
        $this->importfilename = $importfilename;

        return $this;
    }

    /**
     * Get importfilename
     *
     * @return string
     */
    public function getImportfilename()
    {
        return $this->importfilename;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TaskRejectDetail
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set ispreprocess
     *
     * @param boolean $ispreprocess
     *
     * @return TaskRejectDetail
     */
    public function setIspreprocess($ispreprocess)
    {
        $this->ispreprocess = $ispreprocess;

        return $this;
    }

    /**
     * Get ispreprocess
     *
     * @return boolean
     */
    public function getIspreprocess()
    {
        return $this->ispreprocess;
    }

    /**
     * Set materials
     *
     * @param boolean $materials
     *
     * @return TaskRejectDetail
     */
    public function setMaterials($materials)
    {
        $this->materials = $materials;

        return $this;
    }

    /**
     * Get materials
     *
     * @return boolean
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    /**
     * Set rejectlot
     *
     * @param string $rejectlot
     *
     * @return TaskRejectDetail
     */
    public function setRejectlot($rejectlot)
    {
        $this->rejectlot = $rejectlot;

        return $this;
    }

    /**
     * Get rejectlot
     *
     * @return string
     */
    public function getRejectlot()
    {
        return $this->rejectlot;
    }
}
