<?php

namespace App\Model;

/**
 * TaskListInterface
 */
interface TaskListInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set idx
     *
     * @param string $idx
     *
     * @return TaskListInterface
     */
    public function setIdx($idx);

    /**
     * Get idx
     *
     * @return string
     */
    public function getIdx();

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskListInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return TaskListInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return TaskListInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return TaskListInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskListInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return TaskListInterface
     */
    public function setOpdescription($opdescription);

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription();

    /**
     * Set productcount
     *
     * @param integer $productcount
     * @return TaskListInterface
     */
    public function setProductcount($productcount);

    /**
     * Get productcount
     *
     * @return integer
     */
    public function getProductcount();

    /**
     * Set productdonecount
     *
     * @param integer $productdonecount
     *
     * @return TaskListInterface
     */
    public function setProductdonecount($productdonecount);

    /**
     * Get productdonecount
     *
     * @return int
     */
    public function getProductdonecount();

    /**
     * Set productdoneactivity
     *
     * @param integer $productdoneactivity
     *
     * @return TaskListInterface
     */
    public function setProductdoneactivity($productdoneactivity);

    /**
     * Get productdoneactivity
     *
     * @return int
     */
    public function getProductdoneactivity();

    /**
     * Set productdonejobrotation
     *
     * @param integer $productdonejobrotation
     *
     * @return TaskListInterface
     */
    public function setProductdonejobrotation($productdonejobrotation);

    /**
     * Get productdonejobrotation
     *
     * @return int
     */
    public function getProductdonejobrotation();

    /**
     * Set scrapactivity
     *
     * @param integer $scrapactivity
     *
     * @return TaskListInterface
     */
    public function setScrapactivity($scrapactivity);

    /**
     * Get scrapactivity
     *
     * @return int
     */
    public function getScrapactivity();

    /**
     * Set scrapjobrotation
     *
     * @param integer $scrapjobrotation
     *
     * @return TaskListInterface
     */
    public function setScrapjobrotation($scrapjobrotation);

    /**
     * Get scrapjobrotation
     *
     * @return int
     */
    public function getScrapjobrotation();

    /**
     * Set scrappart
     *
     * @param integer $scrappart
     *
     * @return TaskListInterface
     */
    public function setScrappart($scrappart);

    /**
     * Get scrappart
     *
     * @return int
     */
    public function getScrappart();

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     * @return TaskListInterface
     */
    public function setDeadline($deadline);

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline();

    /**
     * Set client
     *
     * @param string $client
     * @return TaskListInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set type
     *
     * @param string $type
     * @return TaskListInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set tpp
     *
     * @param decimal $tpp
     *
     * @return TaskListInterface
     */
    public function setTpp($tpp);

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp();

    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return TaskListInterface
     */
    public function setTaskfromerp($taskfromerp);

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp();

    /**
     * Set tfddescription
     *
     * @param string $tfddescription
     *
     * @return TaskListInterface
     */
    public function setTfddescription($tfddescription);

    /**
     * Get tfddescription
     *
     * @return string
     */
    public function getTfddescription();


    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return TaskListInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return TaskListInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Set plannedstart
     *
     * @param \DateTime $plannedstart
     *
     * @return TaskListInterface
     */
    public function setPlannedstart($plannedstart);

    /**
     * Get plannedstart
     *
     * @return \DateTime
     */
    public function getPlannedstart();

    /**
     * Set resourceid
     *
     * @param string $resourceid
     *
     * @return TaskListInterface
     */
    public function setResourceid($resourceid);

    /**
     * Get resourceid
     *
     * @return string
     */
    public function getResourceid();

    /**
     * Set plannedfinish
     *
     * @param \DateTime $plannedfinish
     *
     * @return TaskListInterface
     */
    public function setPlannedfinish($plannedfinish);

    /**
     * Get plannedfinish
     *
     * @return \DateTime
     */
    public function getPlannedfinish();

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return TaskListInterface
     */
    public function setDuration($duration);

    /**
     * Get duration
     *
     * @return int
     */
    public function getDuration();

    /**
     * Set offtime
     *
     * @param integer $offtime
     *
     * @return TaskListInterface
     */
    public function setOfftime($offtime);

    /**
     * Get offtime
     *
     * @return int
     */
    public function getOfftime();

    /**
     * Set totaltime
     *
     * @param integer $totaltime
     *
     * @return TaskListInterface
     */
    public function setTotaltime($totaltime);

    /**
     * Get totaltime
     *
     * @return int
     */
    public function getTotaltime();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskListInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set mouldaddress
     *
     * @param string $mouldaddress
     *
     * @return TaskListInterface
     */
    public function setMouldaddress($mouldaddress);

    /**
     * Get mouldaddress
     *
     * @return string
     */
    public function getMouldaddress();

    /**
     * Set taskinfo
     *
     * @param boolean $taskinfo
     *
     * @return TaskListInterface
     */
    public function setTaskinfo($taskinfo);

    /**
     * Get taskinfo
     *
     * @return boolean
     */
    public function getTaskinfo();

}
