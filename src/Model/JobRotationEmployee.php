<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * JobRotationEmployee
 */
abstract class JobRotationEmployee extends Base implements BaseInterface, JobRotationEmployeeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var integer
     */
    protected $week;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $jobrotationteam;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var boolean
     */
    protected $isovertime;

    /**
     * @var string
     */
    protected $beginval;

    /**
     * @var string
     */
    protected $endval;

    /**
     * @var integer
     */
    protected $worktime;

    /**
     * @var integer
     */
    protected $breaktime;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return JobRotationEmployee
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set week
     *
     * @param integer $week
     *
     * @return JobRotationEmployee
     */
    public function setWeek($week)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return integer
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return JobRotationEmployee
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set jobrotationteam
     *
     * @param string $jobrotationteam
     *
     * @return JobRotationEmployee
     */
    public function setJobrotationteam($jobrotationteam)
    {
        $this->jobrotationteam = $jobrotationteam;

        return $this;
    }

    /**
     * Get jobrotationteam
     *
     * @return string
     */
    public function getJobrotationteam()
    {
        return $this->jobrotationteam;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return JobRotationEmployee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set isovertime
     *
     * @param boolean $isovertime
     *
     * @return JobRotationEmployee
     */
    public function setIsovertime($isovertime)
    {
        $this->isovertime = $isovertime;

        return $this;
    }

    /**
     * Get isovertime
     *
     * @return boolean
     */
    public function getIsovertime()
    {
        return $this->isovertime;
    }

    /**
     * Set beginval
     *
     * @param string $beginval
     *
     * @return JobRotationEmployee
     */
    public function setBeginval($beginval)
    {
        $this->beginval = $beginval;

        return $this;
    }

    /**
     * Get beginval
     *
     * @return string
     */
    public function getBeginval()
    {
        return $this->beginval;
    }

    /**
     * Set endval
     *
     * @param string $endval
     *
     * @return JobRotationEmployee
     */
    public function setEndval($endval)
    {
        $this->endval = $endval;

        return $this;
    }

    /**
     * Get endval
     *
     * @return string
     */
    public function getEndval()
    {
        return $this->endval;
    }

    /**
     * Set worktime
     *
     * @param integer $worktime
     *
     * @return JobRotationEmployee
     */
    public function setWorktime($worktime)
    {
        $this->worktime = $worktime;

        return $this;
    }

    /**
     * Get worktime
     *
     * @return integer
     */
    public function getWorktime()
    {
        return $this->worktime;
    }

    /**
     * Set breaktime
     *
     * @param integer $breaktime
     *
     * @return JobRotationEmployee
     */
    public function setBreaktime($breaktime)
    {
        $this->breaktime = $breaktime;

        return $this;
    }

    /**
     * Get breaktime
     *
     * @return integer
     */
    public function getBreaktime()
    {
        return $this->breaktime;
    }
}

