<?php
namespace App\Model;

/**
 * TaskSessionInterface
 */
interface TaskSessionInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set idx
     *
     * @param string $idx
     *
     * @return TaskSessionInterface
     */
    public function setIdx($idx);

    /**
     * Get idx
     *
     * @return string
     */
    public function getIdx();

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskSessionInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();
    
    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return TaskSessionInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return TaskSessionInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return TaskSessionInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskSessionInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();
    
    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return TaskSessionInterface
     */
    public function setOpdescription($opdescription);
    
    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription();
    
    /**
     * Set productcount
     *
     * @param integer $productcount
     * @return TaskSessionInterface
     */
    public function setProductcount($productcount);

    /**
     * Get productcount
     *
     * @return integer
     */
    public function getProductcount();
    
    /**
     * Set productdonecount
     *
     * @param integer $productdonecount
     *
     * @return TaskSessionInterface
     */
    public function setProductdonecount($productdonecount);

    /**
     * Get productdonecount
     *
     * @return int
     */
    public function getProductdonecount();
    
    /**
     * Set productdoneactivity
     *
     * @param integer $productdoneactivity
     *
     * @return TaskSessionInterface
     */
    public function setProductdoneactivity($productdoneactivity);
    
    /**
     * Get productdoneactivity
     *
     * @return int
     */
    public function getProductdoneactivity();

    /**
     * Set scrapactivity
     *
     * @param integer $scrapactivity
     *
     * @return TaskSessionInterface
     */
    public function setScrapactivity($scrapactivity);
    
    /**
     * Get scrapactivity
     *
     * @return int
     */
    public function getScrapactivity();

    /**
     * Set scrappart
     *
     * @param integer $scrappart
     *
     * @return TaskSessionInterface
     */
    public function setScrappart($scrappart);

    /**
     * Get scrappart
     *
     * @return int
     */
    public function getScrappart();

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     * @return TaskSessionInterface
     */
    public function setDeadline($deadline);

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline();

    /**
     * Set client
     *
     * @param string $client
     * @return TaskSessionInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();
    
    /**
     * Set type
     *
     * @param string $type
     * @return TaskSessionInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();
    
    /**
     * Set tpp
     *
     * @param decimal $tpp
     *
     * @return TaskSessionInterface
     */
    public function setTpp($tpp);

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp();
    
    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return TaskSessionInterface
     */
    public function setTaskfromerp($taskfromerp);

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return TaskSessionInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();
    
    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return TaskSessionInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Set taskfinishtype
     *
     * @param string $taskfinishtype
     *
     * @return TaskSessionInterface
     */
    public function setTaskfinishtype($taskfinishtype);
    
    /**
     * Get taskfinishtype
     *
     * @return string
     */
    public function getTaskfinishtype();

    /**
     * Set finishdescription
     *
     * @param string $finishdescription
     *
     * @return TaskSessionInterface
     */
    public function setFinishdescription($finishdescription);
    
    /**
     * Get finishdescription
     *
     * @return string
     */
    public function getFinishdescription();

    /**
     * Set plannedstart
     *
     * @param \DateTime $plannedstart
     *
     * @return TaskSessionInterface
     */
    public function setPlannedstart($plannedstart);

    /**
     * Get plannedstart
     *
     * @return \DateTime
     */
    public function getPlannedstart();

    /**
     * Set plannedfinish
     *
     * @param \DateTime $plannedfinish
     *
     * @return TaskSessionInterface
     */
    public function setPlannedfinish($plannedfinish);

    /**
     * Get plannedfinish
     *
     * @return \DateTime
     */
    public function getPlannedfinish();

    /**
     * Set resourceid
     *
     * @param string $resourceid
     *
     * @return TaskSessionInterface
     */
    public function setResourceid($resourceid);

    /**
     * Get resourceid
     *
     * @return string
     */
    public function getResourceid();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskSessionInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus();
}
