<?php
namespace App\Model;

/**
 * MouldInterface
 */
interface MouldInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return MouldInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set description
     *
     * @param string $description
     * @return MouldInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set counter
     *
     * @param integer $counter
     * @return MouldInterface
     */
    public function setCounter($counter);

    /**
     * Get counter
     *
     * @return integer 
     */
    public function getCounter();

    /**
     * Set mouldtype
     *
     * @param string $mouldtype
     * @return MouldInterface
     */
    public function setMouldtype($mouldtype);

    /**
     * Get mouldtype
     *
     * @return string 
     */
    public function getMouldtype();

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return MouldInterface
     */
    public function setSerialnumber($serialnumber);

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber();
    
    /**
     * Set start
     *
     * @param \DateTime $start
     * @return MouldInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return MouldInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    /**
     * Get the value of client1
     *
     * @return  string
     */ 
    public function getClient1();

    /**
     * Set the value of client1
     *
     * @param  string  $client1
     *
     * @return MouldInterface
     */ 
    public function setClient1($client1);

    /**
     * Get the value of client2
     *
     * @return  string
     */ 
    public function getClient2();

    /**
     * Set the value of client2
     *
     * @param  string  $client2
     *
     * @return MouldInterface
     */ 
    public function setClient2($client2);

    /**
     * Get the value of client3
     *
     * @return  string
     */ 
    public function getClient3();

    /**
     * Set the value of client3
     *
     * @param  string  $client3
     *
     * @return MouldInterface
     */ 
    public function setClient3($client3);

    /**
     * Get the value of client4
     *
     * @return  string
     */ 
    public function getClient4();

    /**
     * Set the value of client4
     *
     * @param  string  $client4
     *
     * @return MouldInterface
     */ 
    public function setClient4($client4);

    /**
     * Get the value of client5
     *
     * @return  string
     */ 
    public function getClient5();

    /**
     * Set the value of client5
     *
     * @param  string  $client5
     *
     * @return MouldInterface
     */ 
    public function setClient5($client5);

    /**
     * Set bantime
     *
     * @param \DateTime $bantime
     *
     * @return MouldInterface
     */
    public function setBantime($bantime);

    /**
     * Get bantime
     *
     * @return \DateTime
     */
    public function getBantime();

    /**
     * Set issensormould
     *
     * @param boolean $issensormould
     *
     * @return MouldInterface
     */
    public function setIssensormould($issensormould);

    /**
     * Get issensormould
     *
     * @return boolean
     */
    public function getIssensormould();
    
}
