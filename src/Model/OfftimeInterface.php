<?php
namespace App\Model;

/**
 * OfftimeInterface
 */
interface OfftimeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set clients
     *
     * @param string $clients
     * @return OfftimeInterface
     */
    public function setClients($clients);

    /**
     * Get clients
     *
     * @return string 
     */
    public function getClients();

    /**
     * Set losttype
     *
     * @param string $losttype
     * @return OfftimeInterface
     */
    public function setLosttype($losttype);

    /**
     * Get losttype
     *
     * @return string 
     */
    public function getLosttype();

    /**
     * Set name
     *
     * @param string $name
     * @return OfftimeInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string 
     */
    public function getName();

    /**
     * Set repeattype
     *
     * @param string $repeattype
     * @return OfftimeInterface
     */
    public function setRepeattype($repeattype);

    /**
     * Get repeattype
     *
     * @return string 
     */
    public function getRepeattype();

    /**
     * Set days
     *
     * @param string $days
     * @return OfftimeInterface
     */
    public function setDays($days);

    /**
     * Get days
     *
     * @return string 
     */
    public function getDays();

    /**
     * Set startday
     *
     * @param \DateTime $startday
     * @return OfftimeInterface
     */
    public function setStartday($startday);

    /**
     * Get startday
     *
     * @return \DateTime 
     */
    public function getStartday();

    /**
     * Set starttime
     *
     * @param string $starttime
     * @return OfftimeInterface
     */
    public function setStarttime($starttime);

    /**
     * Get starttime
     *
     * @return string 
     */
    public function getStarttime();

    /**
     * Set finishday
     *
     * @param \DateTime $finishday
     * @return OfftimeInterface
     */
    public function setFinishday($finishday);

    /**
     * Get finishday
     *
     * @return \DateTime 
     */
    public function getFinishday();

    /**
     * Set finishtime
     *
     * @param string $finishtime
     * @return OfftimeInterface
     */
    public function setFinishtime($finishtime);

    /**
     * Get finishtime
     *
     * @return string 
     */
    public function getFinishtime();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return OfftimeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return OfftimeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();

    /**
     * Get the value of clearonovertime
     *
     * @return  boolean
     */ 
    public function getClearonovertime();

    /**
     * Set the value of clearonovertime
     *
     * @param  boolean  $clearonovertime
     *
     * @return OfftimeInterface
     */ 
    public function setClearonovertime($clearonovertime);
    
}
