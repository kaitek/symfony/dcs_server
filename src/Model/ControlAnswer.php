<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * ControlAnswer
 */
abstract class ControlAnswer extends Base implements BaseInterface, ControlAnswerInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $question;

    /**
     * @var string
     */
    protected $answertype;

    /**
     * @var string
     */
    protected $valuerequire;

    /**
     * @var string
     */
    protected $valuemin;

    /**
     * @var string
     */
    protected $valuemax;

    /**
     * @var string
     */
    protected $answer;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var bool
     */
    protected $isexported;

    /**
     * @var string
     */
    protected $documentnumber;

    /**
     * @var bool
     */
    protected $isallowna;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ControlAnswer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ControlAnswer
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return ControlAnswer
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return ControlAnswer
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ControlAnswer
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ControlAnswer
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return ControlAnswer
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return ControlAnswer
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return ControlAnswer
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answertype
     *
     * @param string $answertype
     *
     * @return ControlAnswer
     */
    public function setAnswertype($answertype)
    {
        $this->answertype = $answertype;

        return $this;
    }

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype()
    {
        return $this->answertype;
    }

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return ControlAnswer
     */
    public function setValuerequire($valuerequire)
    {
        $this->valuerequire = $valuerequire;

        return $this;
    }

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire()
    {
        return $this->valuerequire;
    }

    /**
     * Set valuemin
     *
     * @param string $valuemin
     *
     * @return ControlAnswer
     */
    public function setValuemin($valuemin)
    {
        $this->valuemin = $valuemin;

        return $this;
    }

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin()
    {
        return $this->valuemin;
    }

    /**
     * Set valuemax
     *
     * @param string $valuemax
     *
     * @return ControlAnswer
     */
    public function setValuemax($valuemax)
    {
        $this->valuemax = $valuemax;

        return $this;
    }

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax()
    {
        return $this->valuemax;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return ControlAnswer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ControlAnswer
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ControlAnswer
     */
    public function setIsexported($isexported)
    {
        $this->isexported = $isexported;

        return $this;
    }

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported()
    {
        return $this->isexported;
    }

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ControlAnswer
     */
    public function setDocumentnumber($documentnumber)
    {
        $this->documentnumber = $documentnumber;

        return $this;
    }

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber()
    {
        return $this->documentnumber;
    }

    /**
     * Set isallowna
     *
     * @param boolean $isallowna
     *
     * @return ControlAnswer
     */
    public function setIsallowna($isallowna)
    {
        $this->isallowna = $isallowna;

        return $this;
    }

    /**
     * Get isallowna
     *
     * @return boolean
     */
    public function getIsallowna()
    {
        return $this->isallowna;
    }
}
