<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * FaultIntervention
 */
abstract class FaultIntervention extends BaseSync implements BaseSyncInterface, FaultInterventionInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $clients;

    /**
     * @var string
     */
    protected $faultinterventiontype;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $interventions;

    /**
     * @var string
     */
    protected $controlname;

    /**
     * @var string
     */
    protected $controlofficers;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var string
     */
    protected $answertype;

    /**
     * @var string
     */
    protected $valuerequire;

    /**
     * @var string
     */
    protected $valuemin;

    /**
     * @var string
     */
    protected $valuemax;

    /**
     * @var string
     */
    protected $mailto;

    /**
     * @var bool
     */
    protected $isallowna;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clients
     *
     * @param string $clients
     *
     * @return FaultIntervention
     */
    public function setClients($clients)
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set faultinterventiontype
     *
     * @param string $faultinterventiontype
     *
     * @return FaultIntervention
     */
    public function setFaultinterventiontype($faultinterventiontype)
    {
        $this->faultinterventiontype = $faultinterventiontype;

        return $this;
    }

    /**
     * Get faultinterventiontype
     *
     * @return string
     */
    public function getFaultinterventiontype()
    {
        return $this->faultinterventiontype;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FaultIntervention
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set interventions
     *
     * @param string $interventions
     *
     * @return FaultIntervention
     */
    public function setInterventions($interventions)
    {
        $this->interventions = $interventions;

        return $this;
    }

    /**
     * Get interventions
     *
     * @return string
     */
    public function getInterventions()
    {
        return $this->interventions;
    }

    /**
     * Set controlname
     *
     * @param string $controlname
     *
     * @return FaultIntervention
     */
    public function setControlname($controlname)
    {
        $this->controlname = $controlname;

        return $this;
    }

    /**
     * Get controlname
     *
     * @return string
     */
    public function getControlname()
    {
        return $this->controlname;
    }

    /**
     * Set controlofficers
     *
     * @param string $controlofficers
     *
     * @return FaultIntervention
     */
    public function setControlofficers($controlofficers)
    {
        $this->controlofficers = $controlofficers;

        return $this;
    }

    /**
     * Get controlofficers
     *
     * @return string
     */
    public function getControlofficers()
    {
        return $this->controlofficers;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return FaultIntervention
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return FaultIntervention
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set answertype
     *
     * @param string $answertype
     *
     * @return FaultIntervention
     */
    public function setAnswertype($answertype)
    {
        $this->answertype = $answertype;

        return $this;
    }

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype()
    {
        return $this->answertype;
    }

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return FaultIntervention
     */
    public function setValuerequire($valuerequire)
    {
        $this->valuerequire = $valuerequire;

        return $this;
    }

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire()
    {
        return $this->valuerequire;
    }

    /**
     * Set valuemin
     *
     * @param string $valuemin
     *
     * @return FaultIntervention
     */
    public function setValuemin($valuemin)
    {
        $this->valuemin = $valuemin;

        return $this;
    }

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin()
    {
        return $this->valuemin;
    }

    /**
     * Set valuemax
     *
     * @param string $valuemax
     *
     * @return FaultIntervention
     */
    public function setValuemax($valuemax)
    {
        $this->valuemax = $valuemax;

        return $this;
    }

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax()
    {
        return $this->valuemax;
    }

    /**
     * Set mailto
     *
     * @param string $mailto
     *
     * @return FaultIntervention
     */
    public function setMailto($mailto)
    {
        $this->mailto = $mailto;

        return $this;
    }

    /**
     * Get mailto
     *
     * @return string
     */
    public function getMailto()
    {
        return $this->mailto;
    }

    /**
     * Set isallowna
     *
     * @param boolean $isallowna
     *
     * @return FaultIntervention
     */
    public function setIsallowna($isallowna)
    {
        $this->isallowna = $isallowna;

        return $this;
    }

    /**
     * Get isallowna
     *
     * @return boolean
     */
    public function getIsallowna()
    {
        return $this->isallowna;
    }
}
