<?php
namespace App\Model;

/**
 * MouldGroupInterface
 */
interface MouldGroupInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return MouldGroupInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set mould
     *
     * @param string $mould
     * @return MouldGroupInterface
     */
    public function setMould($mould);

    /**
     * Get mould
     *
     * @return string 
     */
    public function getMould();

    /**
     * Set productionmultiplier
     *
     * @param integer $productionmultiplier
     * @return MouldGroupInterface
     */
    public function setProductionmultiplier($productionmultiplier);

    /**
     * Get productionmultiplier
     *
     * @return integer 
     */
    public function getProductionmultiplier();

    /**
     * Set intervalmultiplier
     *
     * @param decimal $intervalmultiplier
     * @return MouldGroupInterface
     */
    public function setIntervalmultiplier($intervalmultiplier);

    /**
     * Get intervalmultiplier
     *
     * @return decimal 
     */
    public function getIntervalmultiplier();

    /**
     * Set counter
     *
     * @param integer $counter
     * @return MouldGroupInterface
     */
    public function setCounter($counter);

    /**
     * Get counter
     *
     * @return integer 
     */
    public function getCounter();

    /**
     * Set setup
     *
     * @param integer $setup
     * @return MouldGroupInterface
     */
    public function setSetup($setup);

    /**
     * Get setup
     *
     * @return integer 
     */
    public function getSetup();

    /**
     * Set cycletime
     *
     * @param decimal $cycletime
     * @return MouldGroupInterface
     */
    public function setCycletime($cycletime);

    /**
     * Get cycletime
     *
     * @return decimal 
     */
    public function getCycletime();

    /**
     * Set lotcount
     *
     * @param integer $lotcount
     * @return MouldGroupInterface
     */
    public function setLotcount($lotcount);

    /**
     * Get lotcount
     *
     * @return integer 
     */
    public function getLotcount();

    /**
     * Set program
     *
     * @param string $program
     *
     * @return MouldGroupInterface
     */
    public function setProgram($program);

    /**
     * Get program
     *
     * @return string
     */
    public function getProgram();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return MouldGroupInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return MouldGroupInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
}
