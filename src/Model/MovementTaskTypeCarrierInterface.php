<?php
namespace App\Model;

/**
 * MovementTaskTypeCarrierInterface
 */
interface MovementTaskTypeCarrierInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set tasktype
     *
     * @param string $tasktype
     * @return MovementTaskTypeCarrierInterface
     */
    public function setTasktype($tasktype);

    /**
     * Get tasktype
     *
     * @return string 
     */
    public function getTasktype();

    /**
     * Set carrier
     *
     * @param string $carrier
     * @return MovementTaskTypeCarrierInterface
     */
    public function setCarrier($carrier);

    /**
     * Get carrier
     *
     * @return string 
     */
    public function getCarrier();
    
}
