<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * Penetration
 */
abstract class Penetration extends Base implements BaseInterface, PenetrationInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var \DateTime
     */
    protected $timecreate;

    /**
     * @var \DateTime
     */
    protected $timeview;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return Penetration
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set timecreate
     *
     * @param \DateTime $timecreate
     *
     * @return Penetration
     */
    public function setTimecreate($timecreate)
    {
        $this->timecreate = $timecreate;

        return $this;
    }

    /**
     * Get timecreate
     *
     * @return \DateTime
     */
    public function getTimecreate()
    {
        return $this->timecreate;
    }

    /**
     * Set timeview
     *
     * @param \DateTime $timeview
     *
     * @return Penetration
     */
    public function setTimeview($timeview)
    {
        $this->timeview = $timeview;

        return $this;
    }

    /**
     * Get timeview
     *
     * @return \DateTime
     */
    public function getTimeview()
    {
        return $this->timeview;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Penetration
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Penetration
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}

