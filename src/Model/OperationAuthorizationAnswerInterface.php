<?php
namespace App\Model;

/**
 * OperationAuthorizationAnswerInterface
 */
interface OperationAuthorizationAnswerInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set masterId
     *
     * @param integer $masterId
     * @return OperationAuthorizationAnswerInterface
     */
    public function setMasterId($masterId);

    /**
     * Get masterId
     *
     * @return integer 
     */
    public function getMasterId();

    /**
     * Set opname
     *
     * @param string $opname
     * @return OperationAuthorizationAnswerInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set employee
     *
     * @param string $employee
     * @return OperationAuthorizationAnswerInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set question
     *
     * @param string $question
     * @return OperationAuthorizationAnswerInterface
     */
    public function setQuestion($question);

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion();

    /**
     * Set answer
     *
     * @param integer $answer
     * @return OperationAuthorizationAnswerInterface
     */
    public function setAnswer($answer);

    /**
     * Get answer
     *
     * @return integer 
     */
    public function getAnswer();

    /**
     * Set description
     *
     * @param string $description
     * @return OperationAuthorizationAnswerInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();
    
}
