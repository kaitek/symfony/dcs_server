<?php

namespace App\Model;

/**
 * EmployeeLostDetail
 */
abstract class EmployeeLostDetail implements EmployeeLostDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $teamleader;

    /**
     * @var integer
     */
    protected $week;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var integer
     */
    protected $duration;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var boolean
     */
    protected $ismandatory;

    /**
     * @var boolean
     */
    protected $isrework;

    /**
     * @var string
     */
    protected $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return EmployeeLostDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return EmployeeLostDetail
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return EmployeeLostDetail
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set teamleader
     *
     * @param string $teamleader
     *
     * @return EmployeeLostDetail
     */
    public function setTeamleader($teamleader)
    {
        $this->teamleader = $teamleader;

        return $this;
    }

    /**
     * Get teamleader
     *
     * @return string
     */
    public function getTeamleader()
    {
        return $this->teamleader;
    }

    /**
     * Set week
     *
     * @param integer $week
     *
     * @return EmployeeLostDetail
     */
    public function setWeek($week)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return integer
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return EmployeeLostDetail
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return EmployeeLostDetail
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return EmployeeLostDetail
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return EmployeeLostDetail
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return EmployeeLostDetail
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set ismandatory
     *
     * @param boolean $ismandatory
     *
     * @return EmployeeLostDetail
     */
    public function setIsmandatory($ismandatory)
    {
        $this->ismandatory = $ismandatory;

        return $this;
    }

    /**
     * Get ismandatory
     *
     * @return boolean
     */
    public function getIsmandatory()
    {
        return $this->ismandatory;
    }

    /**
     * Set isrework
     *
     * @param boolean $isrework
     *
     * @return EmployeeLostDetail
     */
    public function setIsrework($isrework)
    {
        $this->isrework = $isrework;

        return $this;
    }

    /**
     * Get isrework
     *
     * @return boolean
     */
    public function getIsrework()
    {
        return $this->isrework;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return EmployeeLostDetail
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}

