<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * Client
 */
abstract class Client extends BaseSync implements BaseSyncInterface, ClientInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $uuid;

    /**
     * @var boolean
     */
    protected $isactive;

    /**
     * @var integer
     */
    protected $ganttorder;

    /**
     * @var string
     */
    protected $clientweight;

    /**
     * @var boolean
     */
    protected $connected;

    /**
     * @var string
     */
    protected $status;
    
    /**
     * @var \DateTime
     */
    protected $statustime;

    /**
     * @var string
     */
    protected $info;

    /**
     * @var integer
     */
    protected $watchorder;

    /**
     * @var boolean
     */
    protected $materialpreparation;

    /**
     * @var string
     */
    protected $workflow;

    /**
     * @var string
     */
    protected $ip;
    
    /**
     * @var integer
     */
    protected $pincode;
    
    /**
     * @var string
     */
    protected $versionapp;
    
    /**
     * @var string
     */
    protected $versionshell;
    
    /**
     * @var string
     */
    protected $computername;

    /**
     * @var \DateTime
     */
    protected $lastprodstarttime;
    
    /**
     * @var \DateTime
     */
    protected $lastprodtime;

    /**
     * @var integer
     */
    protected $inputready;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Client
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Client
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return Client
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     *
     * @return Client
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set ganttorder
     *
     * @param integer $ganttorder
     *
     * @return Client
     */
    public function setGanttorder($ganttorder)
    {
        $this->ganttorder = $ganttorder;

        return $this;
    }

    /**
     * Get ganttorder
     *
     * @return integer
     */
    public function getGanttorder()
    {
        return $this->ganttorder;
    }

    /**
     * Set clientweight
     *
     * @param string $clientweight
     *
     * @return Client
     */
    public function setClientweight($clientweight)
    {
        $this->clientweight = $clientweight;

        return $this;
    }

    /**
     * Get clientweight
     *
     * @return string
     */
    public function getClientweight()
    {
        return $this->clientweight;
    }

    /**
     * Set connected
     *
     * @param boolean $connected
     *
     * @return Client
     */
    public function setConnected($connected)
    {
        $this->connected = $connected;

        return $this;
    }

    /**
     * Get connected
     *
     * @return boolean
     */
    public function getConnected()
    {
        return $this->connected;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Client
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statustime
     *
     * @param \DateTime $statustime
     *
     * @return Client
     */
    public function setStatustime(\DateTime $statustime) {
        $this->statustime = $statustime;
    }
    
    /**
     * Get statustime
     *
     * @return \DateTime
     */
    public function getStatustime() {
        return $this->statustime;
    }
    
    /**
     * Set info
     *
     * @param string $info
     *
     * @return Client
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set watchorder
     *
     * @param integer $watchorder
     *
     * @return Client
     */
    public function setWatchorder($watchorder)
    {
        $this->watchorder = $watchorder;

        return $this;
    }

    /**
     * Get watchorder
     *
     * @return integer
     */
    public function getWatchorder()
    {
        return $this->watchorder;
    }

    /**
     * Set materialpreparation
     *
     * @param boolean $materialpreparation
     *
     * @return Client
     */
    public function setMaterialpreparation($materialpreparation)
    {
        $this->materialpreparation = $materialpreparation;

        return $this;
    }

    /**
     * Get materialpreparation
     *
     * @return boolean
     */
    public function getMaterialpreparation()
    {
        return $this->materialpreparation;
    }

    /**
     * Set workflow
     *
     * @param string $workflow
     *
     * @return Client
     */
    public function setWorkflow($workflow)
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * Get workflow
     *
     * @return string
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp() {
        return $this->ip;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Client
     */
    public function setIp($ip) {
        $this->ip = $ip;
    }
    
    /**
     * Set pincode
     *
     * @param integer $pincode
     *
     * @return Client
     */
    public function setPincode($pincode) {
        $this->pincode = $pincode;
    }
    
    /**
     * Get pincode
     *
     * @return integer
     */
    public function getPincode() {
        return $this->pincode;
    }

    /**
     * Set versionapp
     *
     * @param string $versionapp
     *
     * @return Client
     */
    public function setVersionapp($versionapp) {
        $this->versionapp = $versionapp;
    }
    
    /**
     * Get versionapp
     *
     * @return string
     */
    public function getVersionapp() {
        return $this->versionapp;
    }

    /**
     * Set versionshell
     *
     * @param string $versionshell
     *
     * @return Client
     */
    public function setVersionshell($versionshell) {
        $this->versionshell = $versionshell;
    }
    
    /**
     * Get versionshell
     *
     * @return string
     */
    public function getVersionshell() {
        return $this->versionshell;
    }

     /**
     * Set computername
     *
     * @param string $computername
     *
     * @return Client
     */
    public function setComputername($computername) {
        $this->computername = $computername;
    }
    
    /**
     * Get computername
     *
     * @return string
     */
    public function getComputername() {
        return $this->computername;
    }

    /**
     * Set lastprodstarttime
     *
     * @param \DateTime $lastprodstarttime
     *
     * @return Client
     */
    public function setLastprodstarttime(\DateTime $lastprodstarttime) {
        $this->lastprodstarttime = $lastprodstarttime;
    }

    /**
     * Get lastprodstarttime
     *
     * @return \DateTime
     */
    public function getLastprodstarttime() {
        return $this->lastprodstarttime;
    }

    /**
     * Set lastprodtime
     *
     * @param \DateTime $lastprodtime
     *
     * @return Client
     */
    public function setLastprodtime(\DateTime $lastprodtime) {
        $this->lastprodtime = $lastprodtime;
    }

    /**
     * Get lastprodtime
     *
     * @return \DateTime
     */
    public function getLastprodtime() {
        return $this->lastprodtime;
    }

    /**
     * Set inputready
     *
     * @param integer $inputready
     *
     * @return Client
     */
    public function setInputready($inputready) {
        $this->inputready = $inputready;
    }
    
    /**
     * Get inputready
     *
     * @return integer
     */
    public function getInputready() {
        return $this->inputready;
    }

}

