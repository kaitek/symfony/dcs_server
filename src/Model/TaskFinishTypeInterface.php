<?php
namespace App\Model;

/**
 * TaskFinishTypeInterface
 */
interface TaskFinishTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return TaskFinishTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set recreate
     *
     * @param boolean $recreate
     * @return TaskFinishTypeInterface
     */
    public function setRecreate($recreate);

    /**
     * Get recreate
     *
     * @return boolean 
     */
    public function getRecreate();

    /**
     * Set orderfield
     *
     * @param integer $orderfield
     * @return TaskFinishTypeInterface
     */
    public function setOrderfield($orderfield);

    /**
     * Get orderfield
     *
     * @return integer 
     */
    public function getOrderfield();
}
