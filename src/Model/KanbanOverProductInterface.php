<?php
namespace App\Model;

/**
 * KanbanOverProductInterface
 */
interface KanbanOverProductInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return KanbanOverProductInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set product
     *
     * @param string $product
     * @return KanbanOverProductInterface
     */
    public function setProduct($product);

    /**
     * Get product
     *
     * @return string 
     */
    public function getProduct();

    /**
     * Set boxcount
     *
     * @param integer $boxcount
     * @return KanbanOverProductInterface
     */
    public function setBoxcount($boxcount);

    /**
     * Get boxcount
     *
     * @return integer 
     */
    public function getBoxcount();

    /**
     * Set currentboxcount
     *
     * @param integer $currentboxcount
     * @return KanbanOverProductInterface
     */
    public function setCurrentboxcount($currentboxcount);

    /**
     * Get currentboxcount
     *
     * @return integer 
     */
    public function getCurrentboxcount();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return KanbanOverProductInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime();
    
}
