<?php

namespace App\Model;

/**
 * OperationAuthorizationDeviceLog
 */
abstract class OperationAuthorizationDeviceLog implements OperationAuthorizationDeviceLogInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $authemployee;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var boolean
     */
    protected $ismailsend;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return OperationAuthorizationDeviceLog
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return OperationAuthorizationDeviceLog
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return OperationAuthorizationDeviceLog
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return OperationAuthorizationDeviceLog
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set authemployee
     *
     * @param string $authemployee
     *
     * @return OperationAuthorizationDeviceLog
     */
    public function setAuthemployee($authemployee)
    {
        $this->authemployee = $authemployee;

        return $this;
    }

    /**
     * Get authemployee
     *
     * @return string
     */
    public function getAuthemployee()
    {
        return $this->authemployee;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return OperationAuthorizationDeviceLog
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set ismailsend
     *
     * @param boolean $ismailsend
     *
     * @return OperationAuthorizationDeviceLog
     */
    public function setIsmailsend($ismailsend)
    {
        $this->ismailsend = $ismailsend;

        return $this;
    }

    /**
     * Get ismailsend
     *
     * @return boolean
     */
    public function getIsmailsend()
    {
        return $this->ismailsend;
    }
}

