<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * LostType
 */
abstract class LostType extends BaseSync implements BaseSyncInterface, LostTypeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $extcode;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $isemployeerequired;

    /**
     * @var boolean
     */
    protected $isexpertemployeerequired;

    /**
     * @var integer
     */
    protected $time;

    /**
     * @var boolean
     */
    protected $isoperationnumberrequired;

    /**
     * @var string
     */
    protected $ioevent;
    
    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;
    
    /**
     * @var boolean
     */
    protected $closenoemployee;
    
    /**
     * @var boolean
     */
    protected $islisted;

    /**
     * @var boolean
     */
    protected $isclient;

    /**
     * @var boolean
     */
    protected $isemployee;

    /**
     * @var boolean
     */
    protected $isshortlost;

    /**
     * @var boolean
     */
    protected $issourcelookup;
    
    /**
     * @var integer
     */
    protected $listorder;

    /**
     * @var string
     */
    protected $authorizeloststart;

    /**
     * @var string
     */
    protected $authorizelostfinish;

    /**
     * @var boolean
     */
    protected $isdescriptionrequired;

    /**
     * @var boolean
     */
    protected $isopenallports;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return LostType
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set extcode
     *
     * @param string $extcode
     *
     * @return LostType
     */
    public function setExtcode($extcode)
    {
        $this->extcode = $extcode;

        return $this;
    }

    /**
     * Get extcode
     *
     * @return string
     */
    public function getExtcode()
    {
        return $this->extcode;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return LostType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isemployeerequired
     *
     * @param boolean $isemployeerequired
     *
     * @return LostType
     */
    public function setIsemployeerequired($isemployeerequired)
    {
        $this->isemployeerequired = $isemployeerequired;

        return $this;
    }

    /**
     * Get isemployeerequired
     *
     * @return boolean
     */
    public function getIsemployeerequired()
    {
        return $this->isemployeerequired;
    }

    /**
     * Set isexpertemployeerequired
     *
     * @param boolean $isexpertemployeerequired
     *
     * @return LostType
     */
    public function setIsexpertemployeerequired($isexpertemployeerequired)
    {
        $this->isexpertemployeerequired = $isexpertemployeerequired;

        return $this;
    }

    /**
     * Get isexpertemployeerequired
     *
     * @return boolean
     */
    public function getIsexpertemployeerequired()
    {
        return $this->isexpertemployeerequired;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return LostType
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set isoperationnumberrequired
     *
     * @param boolean $isoperationnumberrequired
     *
     * @return LostType
     */
    public function setIsoperationnumberrequired($isoperationnumberrequired)
    {
        $this->isoperationnumberrequired = $isoperationnumberrequired;

        return $this;
    }

    /**
     * Get isoperationnumberrequired
     *
     * @return boolean
     */
    public function getIsoperationnumberrequired()
    {
        return $this->isoperationnumberrequired;
    }
    
    /**
     * Set ioevent
     *
     * @param string $ioevent
     *
     * @return LostType
     */
    public function setIOEvent($ioevent)
    {
        $this->ioevent = $ioevent;

        return $this;
    }

    /**
     * Get ioevent
     *
     * @return string
     */
    public function getIOEvent()
    {
        return $this->ioevent;
    }
    
    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return LostType
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return LostType
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }
    
    /**
     * Set closenoemployee
     *
     * @param boolean $closenoemployee
     *
     * @return LostType
     */
    public function setClosenoemployee($closenoemployee) {
        $this->closenoemployee = $closenoemployee;
    }
    
    /**
     * Get closenoemployee
     *
     * @return boolean
     */
    public function getClosenoemployee() {
        return $this->closenoemployee;
    }
    
    /**
     * Set islisted
     *
     * @param boolean $islisted
     *
     * @return LostType
     */
    public function setIslisted($islisted) {
        $this->islisted = $islisted;
    }
    
    /**
     * Get islisted
     *
     * @return boolean
     */
    public function getIslisted() {
        return $this->islisted;
    }

    /**
     * Set isclient
     *
     * @param boolean $isclient
     *
     * @return LostType
     */
    public function setIsclient($isclient) {
        $this->isclient = $isclient;
    }
    
    /**
     * Get isclient
     *
     * @return boolean
     */
    public function getIsclient() {
        return $this->isclient;
    }

    /**
     * Set isemployee
     *
     * @param boolean $isemployee
     *
     * @return LostType
     */
    public function setIsemployee($isemployee) {
        $this->isemployee = $isemployee;
    }
    
    /**
     * Get isemployee
     *
     * @return boolean
     */
    public function getIsemployee() {
        return $this->isemployee;
    }

    /**
     * Set isshortlost
     *
     * @param boolean $isshortlost
     *
     * @return LostType
     */
    public function setIsshortlost($isshortlost) {
        $this->isshortlost = $isshortlost;
    }
    
    /**
     * Get isshortlost
     *
     * @return boolean
     */
    public function getIsshortlost() {
        return $this->isshortlost;
    }

    /**
     * Set issourcelookup
     *
     * @param boolean $issourcelookup
     *
     * @return LostType
     */
    public function setIssourcelookup($issourcelookup) {
        $this->issourcelookup = $issourcelookup;
    }
    
    /**
     * Get issourcelookup
     *
     * @return boolean
     */
    public function getIssourcelookup() {
        return $this->issourcelookup;
    }
    
    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return LostType
     */
    public function setListorder($listorder)
    {
        $this->listorder = $listorder;

        return $this;
    }

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder()
    {
        return $this->listorder;
    }

    /**
     * Set authorizeloststart
     *
     * @param string $authorizeloststart
     *
     * @return LostType
     */
    public function setAuthorizeloststart($authorizeloststart)
    {
        $this->authorizeloststart = $authorizeloststart;

        return $this;
    }

    /**
     * Get authorizeloststart
     *
     * @return string
     */
    public function getAuthorizeloststart()
    {
        return $this->authorizeloststart;
    }

    /**
     * Set authorizelostfinish
     *
     * @param string $authorizelostfinish
     *
     * @return LostType
     */
    public function setAuthorizelostfinish($authorizelostfinish)
    {
        $this->authorizelostfinish = $authorizelostfinish;

        return $this;
    }

    /**
     * Get authorizelostfinish
     *
     * @return string
     */
    public function getAuthorizelostfinish()
    {
        return $this->authorizelostfinish;
    }

    /**
     * Set isdescriptionrequired
     *
     * @param boolean $isdescriptionrequired
     *
     * @return LostType
     */
    public function setIsdescriptionrequired($isdescriptionrequired)
    {
        $this->isdescriptionrequired = $isdescriptionrequired;

        return $this;
    }

    /**
     * Get isdescriptionrequired
     *
     * @return boolean
     */
    public function getIsdescriptionrequired()
    {
        return $this->isdescriptionrequired;
    }

    /**
     * Set isopenallports
     *
     * @param boolean $isopenallports
     *
     * @return LostType
     */
    public function setIsopenallports($isopenallports)
    {
        $this->isopenallports = $isopenallports;

        return $this;
    }

    /**
     * Get isopenallports
     *
     * @return boolean
     */
    public function getIsopenallports()
    {
        return $this->isopenallports;
    }
}

