<?php
namespace App\Model;

/**
 * TaskCaseInterface
 */
interface TaskCaseInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     *
     * @return TaskCaseInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set movementdetail
     *
     * @param integer $movementdetail
     * @return TaskCaseInterface
     */
    public function setMovementdetail($movementdetail);

    /**
     * Get movementdetail
     *
     * @return integer 
     */
    public function getMovementdetail();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return TaskCaseInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string 
     */
    public function getErprefnumber();

    /**
     * Set casetype
     *
     * @param string $casetype
     * @return TaskCaseInterface
     */
    public function setCasetype($casetype);

    /**
     * Get casetype
     *
     * @return string 
     */
    public function getCasetype();
    
    /**
     * Set casenumber
     *
     * @param string $casenumber
     *
     * @return TaskCaseInterface
     */
    public function setCasenumber($casenumber);

    /**
     * Get casenumber
     *
     * @return string
     */
    public function getCasenumber();
    
    /**
     * Set caselot
     *
     * @param string $caselot
     *
     * @return TaskCaseInterface
     */
    public function setCaselot($caselot);

    /**
     * Get caselot
     *
     * @return string
     */
    public function getCaselot();


    /**
     * Set opcode
     *
     * @param string $opcode
     * @return TaskCaseInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string 
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return TaskCaseInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer 
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return TaskCaseInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set stockcode
     *
     * @param string $stockcode
     * @return TaskCaseInterface
     */
    public function setStockcode($stockcode);

    /**
     * Get stockcode
     *
     * @return string 
     */
    public function getStockcode();

    /**
     * Set stockname
     *
     * @param string $stockname
     * @return TaskCaseInterface
     */
    public function setStockname($stockname);

    /**
     * Get stockname
     *
     * @return string 
     */
    public function getStockname();

    /**
     * Set quantityused
     *
     * @param decimal $quantityused
     * @return TaskCaseInterface
     */
    public function setQuantityused($quantityused);

    /**
     * Get quantityused
     *
     * @return decimal 
     */
    public function getQuantityused();

    /**
     * Set casename
     *
     * @param string $casename
     * @return TaskCaseInterface
     */
    public function setCasename($casename);

    /**
     * Get casename
     *
     * @return string 
     */
    public function getCasename();

    /**
     * Set packcapacity
     *
     * @param decimal $packcapacity
     * @return TaskCaseInterface
     */
    public function setPackcapacity($packcapacity);

    /**
     * Get packcapacity
     *
     * @return decimal 
     */
    public function getPackcapacity();

    /**
     * Set feeder
     *
     * @param string $feeder
     * @return TaskCaseInterface
     */
    public function setFeeder($feeder);

    /**
     * Get feeder
     *
     * @return string 
     */
    public function getFeeder();

    /**
     * Set preptime
     *
     * @param integer $preptime
     * @return TaskCaseInterface
     */
    public function setPreptime($preptime);

    /**
     * Get preptime
     *
     * @return integer 
     */
    public function getPreptime();

    /**
     * Set quantityremaining
     *
     * @param decimal $quantityremaining
     * @return TaskCaseInterface
     */
    public function setQuantityremaining($quantityremaining);

    /**
     * Get quantityremaining
     *
     * @return decimal 
     */
    public function getQuantityremaining();

    /**
     * Set quantitydeliver
     *
     * @param decimal $quantitydeliver
     * @return TaskCaseInterface
     */
    public function setQuantitydeliver($quantitydeliver);

    /**
     * Get quantitydeliver
     *
     * @return decimal 
     */
    public function getQuantitydeliver();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return TaskCaseInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return TaskCaseInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    /**
     * Set deliverytime
     *
     * @param \DateTime $deliverytime
     *
     * @return TaskCaseInterface
     */
    public function setDeliverytime($deliverytime);

    /**
     * Get deliverytime
     *
     * @return \DateTime
     */
    public function getDeliverytime();

    /**
     * Set finishtype
     *
     * @param string $finishtype
     * @return TaskCaseInterface
     */
    public function setFinishtype($finishtype);

    /**
     * Get finishtype
     *
     * @return string 
     */
    public function getFinishtype();

    /**
     * Set conveyor
     *
     * @param integer $conveyor
     * @return TaskCaseInterface
     */
    public function setConveyor($conveyor);

    /**
     * Get conveyor
     *
     * @return integer 
     */
    public function getConveyor();

    /**
     * Set goodsplanner
     *
     * @param string $goodsplanner
     * @return TaskCaseInterface
     */
    public function setGoodsplanner($goodsplanner);

    /**
     * Get goodsplanner
     *
     * @return string 
     */
    public function getGoodsplanner();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskCaseInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus();
    
}
