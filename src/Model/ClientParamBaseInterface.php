<?php
namespace App\Model;

/**
 * ClientParamBaseInterface
 */
interface ClientParamBaseInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set optiongroup
     *
     * @param string $optiongroup
     * @return ClientParamBaseInterface
     */
    public function setOptiongroup($optiongroup);

    /**
     * Get optiongroup
     *
     * @return string 
     */
    public function getOptiongroup();

    /**
     * Set name
     *
     * @param string $name
     * @return ClientParamBaseInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string 
     */
    public function getName();

    /**
     * Set label
     *
     * @param string $label
     * @return ClientParamBaseInterface
     */
    public function setLabel($label);

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel();

    /**
     * Set optiontype
     *
     * @param string $optiontype
     * @return ClientParamBaseInterface
     */
    public function setOptiontype($optiontype);

    /**
     * Get optiontype
     *
     * @return string 
     */
    public function getOptiontype();

    /**
     * Set options
     *
     * @param string $options
     * @return ClientParamBaseInterface
     */
    public function setOptions($options);

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions();
    
}
