<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * ProcessDetail
 */
abstract class ProcessDetail extends Base implements BaseInterface, ProcessDetailInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $client;
    
    /**
     * @var string
     */
    protected $tasklist;

    /**
     * @var string
     */
    protected $mould;

    /**
     * @var string
     */
    protected $mouldgroup;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;
    
    /**
     * @var string
     */
    protected $opdescription;

    /**
     * @var string
     */
    protected $leafmask;

    /**
     * @var string
     */
    protected $leafmaskcurrent;

    /**
     * @var integer
     */
    protected $production;

    /**
     * @var integer
     */
    protected $productioncurrent;
    
    /**
     * @var integer
     */
    protected $reject;
    
    /**
     * @var integer
     */
    protected $retouch;
    
    /**
     * @var integer
     */
    protected $scrap;
    
    /**
     * @var integer
     */
    protected $cycle;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $losttype;
    
    /**
     * @var integer
     */
    protected $time;
       
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ProcessDetail
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return ProcessDetail
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return ProcessDetail
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ProcessDetail
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     * Set tasklist
     *
     * @param string $tasklist
     *
     * @return ProcessDetail
     */
    public function setTasklist($tasklist)
    {
        $this->tasklist = $tasklist;

        return $this;
    }

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist()
    {
        return $this->tasklist;
    }

    /**
     * Set mould
     *
     * @param string $mould
     *
     * @return ProcessDetail
     */
    public function setMould($mould)
    {
        $this->mould = $mould;

        return $this;
    }

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould()
    {
        return $this->mould;
    }

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     *
     * @return ProcessDetail
     */
    public function setMouldgroup($mouldgroup)
    {
        $this->mouldgroup = $mouldgroup;

        return $this;
    }

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup()
    {
        return $this->mouldgroup;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return ProcessDetail
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return ProcessDetail
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ProcessDetail
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return ProcessDetail
     */
    public function setOpdescription($opdescription) {
        $this->opdescription = $opdescription;
    }
    
    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription() {
        return $this->opdescription;
    }
    
    /**
     * Set leafmask
     *
     * @param string $leafmask
     *
     * @return ProcessDetail
     */
    public function setLeafmask($leafmask)
    {
        $this->leafmask = $leafmask;

        return $this;
    }

    /**
     * Get leafmask
     *
     * @return string
     */
    public function getLeafmask()
    {
        return $this->leafmask;
    }

    /**
     * Set leafmaskcurrent
     *
     * @param string $leafmaskcurrent
     *
     * @return ProcessDetail
     */
    public function setLeafmaskcurrent($leafmaskcurrent)
    {
        $this->leafmaskcurrent = $leafmaskcurrent;

        return $this;
    }

    /**
     * Get leafmaskcurrent
     *
     * @return string
     */
    public function getLeafmaskcurrent()
    {
        return $this->leafmaskcurrent;
    }

    /**
     * Set production
     *
     * @param integer $production
     *
     * @return ProcessDetail
     */
    public function setProduction($production)
    {
        $this->production = $production;

        return $this;
    }

    /**
     * Get production
     *
     * @return integer
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * Set productioncurrent
     *
     * @param integer $productioncurrent
     *
     * @return ProcessDetail
     */
    public function setProductioncurrent($productioncurrent)
    {
        $this->productioncurrent = $productioncurrent;

        return $this;
    }

    /**
     * Get productioncurrent
     *
     * @return integer
     */
    public function getProductioncurrent()
    {
        return $this->productioncurrent;
    }

    /**
     * Set reject
     *
     * @param integer $reject
     *
     * @return ProcessDetail
     */
    public function setReject($reject) {
        $this->reject = $reject;
    }
    
    /**
     * Get reject
     *
     * @return int
     */
    public function getReject() {
        return $this->reject;
    }

    /**
     * Set retouch
     *
     * @param integer $retouch
     *
     * @return ProcessDetail
     */
    public function setRetouch($retouch) {
        $this->retouch = $retouch;
    }
    
    /**
     * Get retouch
     *
     * @return int
     */
    public function getRetouch() {
        return $this->retouch;
    }
    
    /**
     * Set scrap
     *
     * @param integer $scrap
     *
     * @return ProcessDetail
     */
    public function setScrap($scrap) {
        $this->scrap = $scrap;
    }

    /**
     * Get scrap
     *
     * @return int
     */
    public function getScrap() {
        return $this->scrap;
    }

    
    /**
     * Set cycle
     *
     * @param integer $cycle
     *
     * @return ProcessDetail
     */
    public function setCycle($cycle) {
        $this->cycle = $cycle;
    }

    /**
     * Get cycle
     *
     * @return integer
     */
    public function getCycle() {
        return $this->cycle;
    }
    
    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return ProcessDetail
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }
    
    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber() {
        return $this->erprefnumber;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ProcessDetail
     */
    public function setErprefnumber($erprefnumber) {
        $this->erprefnumber = $erprefnumber;
    }
    
    /**
     * Set losttype
     *
     * @param string $losttype
     *
     * @return ProcessDetail
     */
    public function setLostType($losttype)
    {
        $this->losttype = $losttype;

        return $this;
    }

    /**
     * Get losttype
     *
     * @return string
     */
    public function getLostType()
    {
        return $this->losttype;
    }
    
    /**
     * Set time
     *
     * @param integer $time
     *
     * @return ProcessDetail
     */
    public function setTime($time) {
        $this->time = $time;
    }

    /**
     * Get time
     *
     * @return int
     */
    public function getTime() {
        return $this->time;
    }
        
}

