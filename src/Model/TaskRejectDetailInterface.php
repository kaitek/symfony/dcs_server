<?php

namespace App\Model;

/**
 * TaskRejectDetailInterface
 */
interface TaskRejectDetailInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return TaskRejectDetailInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set rejecttype
     *
     * @param string $rejecttype
     *
     * @return TaskRejectDetail
     */
    public function setRejecttype($rejecttype);

    /**
     * Get rejecttype
     *
     * @return string
     */
    public function getRejecttype();

    /**
     * Set client
     *
     * @param string $client
     * @return TaskRejectDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return TaskRejectDetailInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return TaskRejectDetailInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Set materialunit
     *
     * @param string $materialunit
     *
     * @return TaskRejectDetailInterface
     */
    public function setMaterialunit($materialunit);

    /**
     * Get materialunit
     *
     * @return string
     */
    public function getMaterialunit();

    /**
     * Set tasklist
     *
     * @param string $tasklist
     * @return TaskRejectDetailInterface
     */
    public function setTasklist($tasklist);

    /**
     * Get tasklist
     *
     * @return string
     */
    public function getTasklist();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return TaskRejectDetailInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime();

    /**
     * Set mould
     *
     * @param string $mould
     * @return TaskRejectDetailInterface
     */
    public function setMould($mould);

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould();

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     * @return TaskRejectDetailInterface
     */
    public function setMouldgroup($mouldgroup);

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return TaskRejectDetailInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return TaskRejectDetailInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return TaskRejectDetailInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return TaskRejectDetailInterface
     */
    public function setOpdescription($opdescription);

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription();

    /**
     * Set quantityreject
     *
     * @param integer $quantityreject
     *
     * @return TaskRejectDetail
     */
    public function setQuantityReject($quantityreject);

    /**
     * Get quantityreject
     *
     * @return int
     */
    public function getQuantityReject();

    /**
     * Set quantityretouch
     *
     * @param integer $quantityretouch
     *
     * @return TaskRejectDetail
     */
    public function setQuantityRetouch($quantityretouch);

    /**
     * Get quantityretouch
     *
     * @return int
     */
    public function getQuantityRetouch();

    /**
     * Set quantityscrap
     *
     * @param integer $quantityscrap
     *
     * @return TaskRejectDetail
     */
    public function setQuantityScrap($quantityscrap);

    /**
     * Get quantityscrap
     *
     * @return int
     */
    public function getQuantityScrap();

    /**
     * Set employee
     *
     * @param string $employee
     * @return TaskRejectDetailInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string
     */

    public function getEmployee();
    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return TaskRejectDetailInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return TaskRejectDetailInterface
     */
    public function setIsexported($isexported);

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported();

    /**
     * Set importfilename
     *
     * @param string $importfilename
     *
     * @return TaskRejectDetailInterface
     */
    public function setImportfilename($importfilename);

    /**
     * Get importfilename
     *
     * @return string
     */
    public function getImportfilename();

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TaskRejectDetailInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set materials
     *
     * @param boolean $materials
     *
     * @return TaskRejectDetailInterface
     */
    public function setMaterials($materials);

    /**
     * Get materials
     *
     * @return boolean
     */
    public function getMaterials();

    /**
    * Set rejectlot
    *
    * @param string $rejectlot
    *
    * @return TaskRejectDetailInterface
    */
    public function setRejectlot($rejectlot);

    /**
     * Get rejectlot
     *
     * @return string
     */
    public function getRejectlot();
}
