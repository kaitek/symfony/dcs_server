<?php
namespace App\Model;

/**
 * CarrierInterface
 */
interface CarrierInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return CarrierInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CarrierInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set description
     *
     * @param string $description
     * @return CarrierInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return CarrierInterface
     */
    public function setUuid($uuid);

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid();

    /**
     * Set connected
     *
     * @param boolean $connected
     * @return CarrierInterface
     */
    public function setConnected($connected);

    /**
     * Get connected
     *
     * @return boolean 
     */
    public function getConnected();

    /**
     * Set workflow
     *
     * @param string $workflow
     * @return CarrierInterface
     */
    public function setWorkflow($workflow);

    /**
     * Get workflow
     *
     * @return string 
     */
    public function getWorkflow();

    /**
     * Set ip
     *
     * @param string $ip
     * @return CarrierInterface
     */
    public function setIp($ip);

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp();

    /**
     * Set pincode
     *
     * @param integer $pincode
     * @return CarrierInterface
     */
    public function setPincode($pincode);

    /**
     * Get pincode
     *
     * @return integer 
     */
    public function getPincode();

    /**
     * Set versionapp
     *
     * @param string $versionapp
     * @return CarrierInterface
     */
    public function setVersionapp($versionapp);

    /**
     * Get versionapp
     *
     * @return string 
     */
    public function getVersionapp();

    /**
     * Set versionshell
     *
     * @param string $versionshell
     * @return CarrierInterface
     */
    public function setVersionshell($versionshell);

    /**
     * Get versionshell
     *
     * @return string 
     */
    public function getVersionshell();

    /**
     * Set computername
     *
     * @param string $computername
     * @return CarrierInterface
     */
    public function setComputername($computername);

    /**
     * Get computername
     *
     * @return string 
     */
    public function getComputername();
    
}
