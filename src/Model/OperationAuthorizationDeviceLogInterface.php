<?php
namespace App\Model;

/**
 * OperationAuthorizationDeviceLogInterface
 */
interface OperationAuthorizationDeviceLogInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return OperationAuthorizationDeviceLogInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime();

    /**
     * Set employee
     *
     * @param string $employee
     * @return OperationAuthorizationDeviceLogInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set client
     *
     * @param string $client
     * @return OperationAuthorizationDeviceLogInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set opname
     *
     * @param string $opname
     * @return OperationAuthorizationDeviceLogInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set authemployee
     *
     * @param string $authemployee
     * @return OperationAuthorizationDeviceLogInterface
     */
    public function setAuthemployee($authemployee);

    /**
     * Get authemployee
     *
     * @return string 
     */
    public function getAuthemployee();

    /**
     * Set type
     *
     * @param string $type
     * @return OperationAuthorizationDeviceLogInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string 
     */
    public function getType();

    /**
     * Set ismailsend
     *
     * @param boolean $ismailsend
     * @return OperationAuthorizationDeviceLogInterface
     */
    public function setIsmailsend($ismailsend);

    /**
     * Get ismailsend
     *
     * @return boolean 
     */
    public function getIsmailsend();
    
}
