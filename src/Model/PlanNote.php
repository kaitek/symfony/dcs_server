<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * PlanNote
 */
abstract class PlanNote extends Base implements BaseInterface, PlanNoteInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var integer
     */
    protected $recordorder;

    /**
     * @var string
     */
    protected $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return PlanNote
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PlanNote
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set recordorder
     *
     * @param integer $recordorder
     *
     * @return PlanNote
     */
    public function setRecordorder($recordorder)
    {
        $this->recordorder = $recordorder;

        return $this;
    }

    /**
     * Get recordorder
     *
     * @return integer
     */
    public function getRecordorder()
    {
        return $this->recordorder;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PlanNote
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}

