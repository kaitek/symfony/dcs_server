<?php

namespace App\Model;

/**
 * MovementTaskType
 */
abstract class MovementTaskType implements MovementTaskTypeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $tasktype;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $label;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tasktype
     *
     * @param string $tasktype
     *
     * @return MovementTaskType
     */
    public function setTasktype($tasktype)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype
     *
     * @return string
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return MovementTaskType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return MovementTaskType
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
}

