<?php
namespace App\Model;

/**
 * OperationAuthorizationInterface
 */
interface OperationAuthorizationInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set employee
     *
     * @param string $employee
     * @return OperationAuthorizationInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set opname
     *
     * @param string $opname
     * @return OperationAuthorizationInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set l1
     *
     * @param integer $l1
     * @return OperationAuthorizationInterface
     */
    public function setL1($l1);

    /**
     * Get l1
     *
     * @return integer 
     */
    public function getL1();

    /**
     * Set l2
     *
     * @param integer $l2
     * @return OperationAuthorizationInterface
     */
    public function setL2($l2);

    /**
     * Get l2
     *
     * @return integer 
     */
    public function getL2();

    /**
     * Set l3
     *
     * @param integer $l3
     * @return OperationAuthorizationInterface
     */
    public function setL3($l3);

    /**
     * Get l3
     *
     * @return integer 
     */
    public function getL3();

    /**
     * Set l4
     *
     * @param integer $l4
     * @return OperationAuthorizationInterface
     */
    public function setL4($l4);

    /**
     * Get l4
     *
     * @return integer 
     */
    public function getL4();

    /**
     * Set l5
     *
     * @param integer $l5
     * @return OperationAuthorizationInterface
     */
    public function setL5($l5);

    /**
     * Get l5
     *
     * @return integer 
     */
    public function getL5();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return OperationAuthorizationInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime();
    
}
