<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

/**
 * OperationAuthorization
 */
abstract class OperationAuthorization extends Base implements BaseInterface, OperationAuthorizationInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var integer
     */
    protected $l1;

    /**
     * @var integer
     */
    protected $l2;

    /**
     * @var integer
     */
    protected $l3;

    /**
     * @var integer
     */
    protected $l4;

    /**
     * @var integer
     */
    protected $l5;

    /**
     * @var \DateTime
     */
    protected $time;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return OperationAuthorization
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return OperationAuthorization
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set l1
     *
     * @param integer $l1
     *
     * @return OperationAuthorization
     */
    public function setL1($l1)
    {
        $this->l1 = $l1;

        return $this;
    }

    /**
     * Get l1
     *
     * @return integer
     */
    public function getL1()
    {
        return $this->l1;
    }

    /**
     * Set l2
     *
     * @param integer $l2
     *
     * @return OperationAuthorization
     */
    public function setL2($l2)
    {
        $this->l2 = $l2;

        return $this;
    }

    /**
     * Get l2
     *
     * @return integer
     */
    public function getL2()
    {
        return $this->l2;
    }

    /**
     * Set l3
     *
     * @param integer $l3
     *
     * @return OperationAuthorization
     */
    public function setL3($l3)
    {
        $this->l3 = $l3;

        return $this;
    }

    /**
     * Get l3
     *
     * @return integer
     */
    public function getL3()
    {
        return $this->l3;
    }

    /**
     * Set l4
     *
     * @param integer $l4
     *
     * @return OperationAuthorization
     */
    public function setL4($l4)
    {
        $this->l4 = $l4;

        return $this;
    }

    /**
     * Get l4
     *
     * @return integer
     */
    public function getL4()
    {
        return $this->l4;
    }

    /**
     * Set l5
     *
     * @param integer $l5
     *
     * @return OperationAuthorization
     */
    public function setL5($l5)
    {
        $this->l5 = $l5;

        return $this;
    }

    /**
     * Get l5
     *
     * @return integer
     */
    public function getL5()
    {
        return $this->l5;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return OperationAuthorization
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }
}

