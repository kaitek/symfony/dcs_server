<?php

namespace App\Model;

/**
 * MPStock
 */
abstract class MPStock implements MPStockInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $stockcode;

    /**
     * @var decimal
     */
    protected $qerp;

    /**
     * @var decimal
     */
    protected $qreserved;

    /**
     * @var decimal
     */
    protected $qconsumed;

    /**
     * @var decimal
     */
    protected $qstock;

    /**
     * @var \DateTime
     */
    protected $lastupdate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stockcode
     *
     * @param string $stockcode
     *
     * @return MPStock
     */
    public function setStockcode($stockcode)
    {
        $this->stockcode = $stockcode;

        return $this;
    }

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode()
    {
        return $this->stockcode;
    }

    /**
     * Set qerp
     *
     * @param decimal $qerp
     *
     * @return MPStock
     */
    public function setQerp($qerp)
    {
        $this->qerp = $qerp;

        return $this;
    }

    /**
     * Get qerp
     *
     * @return decimal
     */
    public function getQerp()
    {
        return $this->qerp;
    }

    /**
     * Set qreserved
     *
     * @param decimal $qreserved
     *
     * @return MPStock
     */
    public function setQreserved($qreserved)
    {
        $this->qreserved = $qreserved;

        return $this;
    }

    /**
     * Get qreserved
     *
     * @return decimal
     */
    public function getQreserved()
    {
        return $this->qreserved;
    }

    /**
     * Set qconsumed
     *
     * @param decimal $qconsumed
     *
     * @return MPStock
     */
    public function setQconsumed($qconsumed)
    {
        $this->qconsumed = $qconsumed;

        return $this;
    }

    /**
     * Get qconsumed
     *
     * @return decimal
     */
    public function getQconsumed()
    {
        return $this->qconsumed;
    }

    /**
     * Set qstock
     *
     * @param decimal $qstock
     *
     * @return MPStock
     */
    public function setQstock($qstock)
    {
        $this->qstock = $qstock;

        return $this;
    }

    /**
     * Get qstock
     *
     * @return decimal
     */
    public function getQstock()
    {
        return $this->qstock;
    }

    /**
     * Set lastupdate
     *
     * @param \DateTime $lastupdate
     *
     * @return MPStock
     */
    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    /**
     * Get lastupdate
     *
     * @return \DateTime
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }
}
