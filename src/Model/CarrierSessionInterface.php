<?php
namespace App\Model;

/**
 * CarrierSessionInterface
 */
interface CarrierSessionInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set carrier
     *
     * @param string $carrier
     * @return CarrierSessionInterface
     */
    public function setCarrier($carrier);

    /**
     * Get carrier
     *
     * @return string 
     */
    public function getCarrier();

    /**
     * Set empcode
     *
     * @param string $empcode
     * @return CarrierSessionInterface
     */
    public function setEmpcode($empcode);

    /**
     * Get empcode
     *
     * @return string 
     */
    public function getEmpcode();

    /**
     * Set empname
     *
     * @param string $empname
     * @return CarrierSessionInterface
     */
    public function setEmpname($empname);

    /**
     * Get empname
     *
     * @return string 
     */
    public function getEmpname();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return CarrierSessionInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return CarrierSessionInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return CarrierSessionInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return CarrierSessionInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();
    
}
