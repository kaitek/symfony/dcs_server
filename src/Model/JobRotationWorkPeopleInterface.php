<?php
namespace App\Model;

/**
 * JobRotationWorkPeopleInterface
 */
interface JobRotationWorkPeopleInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return JobRotationWorkPeopleInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return JobRotationWorkPeopleInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set jobrotationteam
     *
     * @param string $jobrotationteam
     * @return JobRotationWorkPeopleInterface
     */
    public function setJobrotationteam($jobrotationteam);

    /**
     * Get jobrotationteam
     *
     * @return string 
     */
    public function getJobrotationteam();

    /**
     * Set employee
     *
     * @param string $employee
     * @return JobRotationWorkPeopleInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set beginval
     *
     * @param string $beginval
     * @return JobRotationWorkPeopleInterface
     */
    public function setBeginval($beginval);

    /**
     * Get beginval
     *
     * @return string 
     */
    public function getBeginval();

    /**
     * Set endval
     *
     * @param string $endval
     * @return JobRotationWorkPeopleInterface
     */
    public function setEndval($endval);

    /**
     * Get endval
     *
     * @return string 
     */
    public function getEndval();

}
