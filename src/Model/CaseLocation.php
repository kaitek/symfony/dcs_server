<?php

namespace App\Model;

use Kaitek\Bundle\FrameworkBundle\Model\Base;
use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;
/**
 * CaseLocation
 */
abstract class CaseLocation extends Base implements BaseInterface, CaseLocationInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $stockcode;

    /**
     * @var string
     */
    protected $location;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stockcode
     *
     * @param string $stockcode
     *
     * @return CaseLocation
     */
    public function setStockcode($stockcode)
    {
        $this->stockcode = $stockcode;

        return $this;
    }

    /**
     * Get stockcode
     *
     * @return string
     */
    public function getStockcode()
    {
        return $this->stockcode;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return CaseLocation
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

}

