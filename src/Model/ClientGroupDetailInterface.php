<?php
namespace App\Model;

/**
 * ClientGroupDetailInterface
 */
interface ClientGroupDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set clientgroup
     *
     * @param string $clientgroup
     * @return ClientGroupDetailInterface
     */
    public function setClientgroup($clientgroup);

    /**
     * Get clientgroup
     *
     * @return string 
     */
    public function getClientgroup();

    /**
     * Set clientgroupcode
     *
     * @param string $clientgroupcode
     * @return ClientGroupDetailInterface
     */
    public function setClientgroupcode($clientgroupcode);

    /**
     * Get clientgroupcode
     *
     * @return string 
     */
    public function getClientgroupcode();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientGroupDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();
}
