<?php
namespace App\Model;

/**
 * ProcessDetailInterface
 */
interface ProcessDetailInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ProcessDetailInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string 
     */
    public function getType();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ProcessDetailInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return ProcessDetailInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set client
     *
     * @param string $client
     * @return ProcessDetailInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();
    
    /**
     * Set tasklist
     *
     * @param string $tasklist
     * @return ProcessDetailInterface
     */
    public function setTasklist($tasklist);

    /**
     * Get tasklist
     *
     * @return string 
     */
    public function getTasklist();

    /**
     * Set mould
     *
     * @param string $mould
     * @return ProcessDetailInterface
     */
    public function setMould($mould);

    /**
     * Get mould
     *
     * @return string 
     */
    public function getMould();

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     * @return ProcessDetailInterface
     */
    public function setMouldgroup($mouldgroup);

    /**
     * Get mouldgroup
     *
     * @return string 
     */
    public function getMouldgroup();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return ProcessDetailInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string 
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return ProcessDetailInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer 
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ProcessDetailInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return ProcessDetailInterface
     */
    public function setOpdescription($opdescription);
    
    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription();
    
    /**
     * Set leafmask
     *
     * @param string $leafmask
     * @return ProcessDetailInterface
     */
    public function setLeafmask($leafmask);

    /**
     * Get leafmask
     *
     * @return string 
     */
    public function getLeafmask();

    /**
     * Set leafmaskcurrent
     *
     * @param string $leafmaskcurrent
     * @return ProcessDetailInterface
     */
    public function setLeafmaskcurrent($leafmaskcurrent);

    /**
     * Get leafmaskcurrent
     *
     * @return string 
     */
    public function getLeafmaskcurrent();

    /**
     * Set production
     *
     * @param integer $production
     * @return ProcessDetailInterface
     */
    public function setProduction($production);

    /**
     * Get production
     *
     * @return integer 
     */
    public function getProduction();

    /**
     * Set productioncurrent
     *
     * @param integer $productioncurrent
     * @return ProcessDetailInterface
     */
    public function setProductioncurrent($productioncurrent);

    /**
     * Get productioncurrent
     *
     * @return integer 
     */
    public function getProductioncurrent();

    /**
     * Set reject
     *
     * @param integer $reject
     *
     * @return ProcessDetailInterface
     */
    public function setReject($reject);
    
    /**
     * Get reject
     *
     * @return int
     */
    public function getReject();

    /**
     * Set retouch
     *
     * @param integer $retouch
     *
     * @return ProcessDetailInterface
     */
    public function setRetouch($retouch);
    
    /**
     * Get retouch
     *
     * @return int
     */
    public function getRetouch();
    
    /**
     * Set scrap
     *
     * @param integer $scrap
     *
     * @return ProcessDetailInterface
     */
    public function setScrap($scrap);

    /**
     * Get scrap
     *
     * @return int
     */
    public function getScrap();
    
    /**
     * Set cycle
     *
     * @param integer $cycle
     *
     * @return ProcessDetailInterface
     */
    public function setCycle($cycle);

    /**
     * Get cycle
     *
     * @return integer
     */
    function getCycle();
    
    /**
     * Set employee
     *
     * @param string $employee
     * @return ProcessDetailInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();
    
    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return ProcessDetailInterface
     */
    public function setErprefnumber($erprefnumber);
    
    /**
     * Set losttype
     *
     * @param string $losttype
     * @return ProcessDetailInterface
     */
    public function setLostType($losttype);

    /**
     * Get losttype
     *
     * @return string
     */
    public function getLostType();
    
    /**
     * Set time
     *
     * @param integer $time
     *
     * @return ProcessDetail
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return int
     */
    public function getTime();
        
}
