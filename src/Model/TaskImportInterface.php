<?php
namespace App\Model;

/**
 * TaskImportInterface
 */
interface TaskImportInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskImportInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return TaskImportInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string 
     */
    public function getErprefnumber();

    /**
     * Set opcode
     *
     * @param string $opcode
     * @return TaskImportInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string 
     */
    public function getOpcode();

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     * @return TaskImportInterface
     */
    public function setOpnumber($opnumber);

    /**
     * Get opnumber
     *
     * @return integer 
     */
    public function getOpnumber();

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskImportInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();
    
    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return TaskImportInterface
     */
    public function setOpdescription($opdescription);
    
    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription();

    /**
     * Set productcount
     *
     * @param integer $productcount
     * @return TaskImportInterface
     */
    public function setProductcount($productcount);

    /**
     * Get productcount
     *
     * @return integer 
     */
    public function getProductcount();

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     * @return TaskImportInterface
     */
    public function setDeadline($deadline);

    /**
     * Get deadline
     *
     * @return \DateTime 
     */
    public function getDeadline();

    /**
     * Set client
     *
     * @param string $client
     * @return TaskImportInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();
    
    /**
     * Set type
     *
     * @param string $type
     * @return TaskImportInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string 
     */
    public function getType();

    /**
     * Set tpp
     *
     * @param decimal $tpp
     *
     * @return TaskImportInterface
     */
    public function setTpp($tpp);

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskImportInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus();

}
