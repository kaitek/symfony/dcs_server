<?php

namespace App\Model;

/**
 * ClientProductionSignalInterface
 */
interface ClientProductionSignalInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ClientProductionSignalInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set client
     *
     * @param string $client
     * @return ClientProductionSignalInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ClientProductionSignalInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return ClientProductionSignalInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Set port
     *
     * @param integer $port
     *
     * @return ClientProductionSignalInterface
     */
    public function setPort($port);

    /**
     * Get port
     *
     * @return integer
     */
    public function getPort();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return ClientProductionSignalInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime();

    /**
     * Set mould
     *
     * @param string $mould
     * @return ClientProductionSignalInterface
     */
    public function setMould($mould);

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould();

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     * @return ClientProductionSignalInterface
     */
    public function setMouldgroup($mouldgroup);

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ClientProductionSignalInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set production
     *
     * @param integer $production
     * @return ClientProductionSignalInterface
     */
    public function setProduction($production);

    /**
     * Get production
     *
     * @return integer
     */
    public function getProduction();

    /**
     * Set productioncurrent
     *
     * @param integer $productioncurrent
     * @return ClientProductionSignalInterface
     */
    public function setProductioncurrent($productioncurrent);

    /**
     * Get productioncurrent
     *
     * @return integer
     */
    public function getProductioncurrent();

    /**
     * Set employee
     *
     * @param string $employee
     * @return ClientProductionSignalInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string
     */

    public function getEmployee();
    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     * @return ClientProductionSignalInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Set gap
     *
     * @param integer $gap
     *
     * @return ClientProductionSignalInterface
     */
    public function setGap($gap);

    /**
     * Get gap
     *
     * @return int
     */
    public function getGap();

    /**
     * Set energy
     *
     * @param decimal $energy
     *
     * @return ClientProductionSignalInterface
     */
    public function setEnergy($energy);

    /**
     * Get energy
     *
     * @return decimal
     */
    public function getEnergy();

    /**
     * Set cycletime
     *
     * @param decimal $cycletime
     *
     * @return ClientProductionSignalInterface
     */
    public function setCycletime($cycletime);

    /**
     * Get cycletime
     *
     * @return decimal
     */
    public function getCycletime();

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ClientProductionSignalInterface
     */
    public function setIsexported($isexported);

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported();

    /**
     * Set opsayi
     *
     * @param integer $opsayi
     *
     * @return ClientProductionSignalInterface
     */
    public function setOpsayi($opsayi);

    /**
     * Get opsayi
     *
     * @return integer
     */
    public function getOpsayi();

    /**
     * Set refsayi
     *
     * @param integer $refsayi
     *
     * @return ClientProductionSignalInterface
     */
    public function setRefsayi($refsayi);

    /**
     * Get refsayi
     *
     * @return integer
     */
    public function getRefsayi();

    /**
     * Set losttime
     *
     * @param integer $losttime
     *
     * @return ClientProductionSignalInterface
     */
    public function setLosttime($losttime);

    /**
     * Get losttime
     *
     * @return integer
     */
    public function getLosttime();

    /**
     * Set signalcount
     *
     * @param integer $signalcount
     *
     * @return ClientProductionSignalInterface
     */
    public function setSignalcount($signalcount);

    /**
     * Get signalcount
     *
     * @return integer
     */
    public function getSignalcount();

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return ClientProductionSignalInterface
     */
    public function setSerialnumber($serialnumber);

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber();
}
