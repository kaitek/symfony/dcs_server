<?php
namespace App\Model;

/**
 * ClientInterface
 */
interface ClientInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return ClientInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set description
     *
     * @param string $description
     * @return ClientInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return ClientInterface
     */
    public function setUuid($uuid);

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid();

    /**
     * Set isactive
     *
     * @param boolean $isactive
     * @return ClientInterface
     */
    public function setIsactive($isactive);

    /**
     * Get isactive
     *
     * @return boolean 
     */
    public function getIsactive();

    /**
     * Set ganttorder
     *
     * @param integer $ganttorder
     * @return ClientInterface
     */
    public function setGanttorder($ganttorder);

    /**
     * Get ganttorder
     *
     * @return integer 
     */
    public function getGanttorder();

    /**
     * Set clientweight
     *
     * @param string $clientweight
     * @return ClientInterface
     */
    public function setClientweight($clientweight);

    /**
     * Get clientweight
     *
     * @return string 
     */
    public function getClientweight();

    /**
     * Set connected
     *
     * @param boolean $connected
     * @return ClientInterface
     */
    public function setConnected($connected);

    /**
     * Get connected
     *
     * @return boolean 
     */
    public function getConnected();

    /**
     * Set status
     *
     * @param string $status
     * @return ClientInterface
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus();
    
    /**
     * Set statustime
     *
     * @param \DateTime $statustime
     *
     * @return ClientInterface
     */
    public function setStatustime(\DateTime $statustime);
    
    /**
     * Get statustime
     *
     * @return \DateTime
     */
    public function getStatustime();

    /**
     * Set info
     *
     * @param string $info
     * @return ClientInterface
     */
    public function setInfo($info);

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo();

    /**
     * Set watchorder
     *
     * @param integer $watchorder
     * @return ClientInterface
     */
    public function setWatchorder($watchorder);

    /**
     * Get watchorder
     *
     * @return integer 
     */
    public function getWatchorder();

    /**
     * Set materialpreparation
     *
     * @param boolean $materialpreparation
     * @return ClientInterface
     */
    public function setMaterialpreparation($materialpreparation);

    /**
     * Get materialpreparation
     *
     * @return boolean 
     */
    public function getMaterialpreparation();

    /**
     * Set workflow
     *
     * @param integer $workflow
     * @return ClientInterface
     */
    public function setWorkflow($workflow);

    /**
     * Get workflow
     *
     * @return string
     */
    public function getWorkflow();
 
    /**
     * Get ip
     *
     * @return string
     */
    public function getIp();

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return ClientInterface
     */
    public function setIp($ip);
    
    /**
     * Set pincode
     *
     * @param integer $pincode
     *
     * @return ClientInterface
     */
    public function setPincode($pincode);
    
    /**
     * Get pincode
     *
     * @return integer
     */
    public function getPincode();
    
    /**
     * Set versionapp
     *
     * @param string $versionapp
     *
     * @return ClientInterface
     */
    public function setVersionapp($versionapp);
    
    /**
     * Get versionapp
     *
     * @return string
     */
    public function getVersionapp();

    /**
     * Set versionshell
     *
     * @param string $versionshell
     *
     * @return ClientInterface
     */
    public function setVersionshell($versionshell);
    
    /**
     * Get versionshell
     *
     * @return string
     */
    public function getVersionshell();

     /**
     * Set computername
     *
     * @param string $computername
     *
     * @return ClientInterface
     */
    public function setComputername($computername);
    
    /**
     * Get computername
     *
     * @return string
     */
    public function getComputername();
    
    /**
     * Set lastprodstarttime
     *
     * @param \DateTime $lastprodstarttime
     *
     * @return ClientInterface
     */
    public function setLastprodstarttime(\DateTime $lastprodstarttime);

    /**
     * Get lastprodstarttime
     *
     * @return \DateTime
     */
    public function getLastprodstarttime();

    /**
     * Set lastprodtime
     *
     * @param \DateTime $lastprodtime
     *
     * @return ClientInterface
     */
    public function setLastprodtime(\DateTime $lastprodtime);

    /**
     * Get lastprodtime
     *
     * @return \DateTime
     */
    public function getLastprodtime();

    /**
     * Set inputready
     *
     * @param integer $inputready
     *
     * @return ClientInterface
     */
    public function setInputready($inputready);
    
    /**
     * Get inputready
     *
     * @return integer
     */
    public function getInputready();
}
