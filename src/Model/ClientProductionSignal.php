<?php

namespace App\Model;

//use Kaitek\Bundle\FrameworkBundle\Model\Base;
//use Kaitek\Bundle\FrameworkBundle\Model\BaseInterface;

//abstract class ClientProductionSignal extends Base implements BaseInterface, ClientProductionSignalInterface
/**
 * ClientProductionSignal
 */
abstract class ClientProductionSignal implements ClientProductionSignalInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var int
     */
    protected $port;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $mould;

    /**
     * @var string
     */
    protected $mouldgroup;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var integer
     */
    protected $production;

    /**
     * @var integer
     */
    protected $productioncurrent;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var integer
     */
    protected $gap;

    /**
     * @var decimal
     */
    protected $energy;

    /**
     * @var decimal
      */
    protected $cycletime;

    /**
     * @var bool
     */
    protected $isexported;

    /**
     * @var integer
     */
    protected $opsayi;

    /**
     * @var integer
     */
    protected $refsayi;

    /**
     * @var integer
     */
    protected $losttime;

    /**
     * @var integer
     */
    protected $signalcount;

    /**
     * @var string
     */
    protected $serialnumber;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ClientProductionSignal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientProductionSignal
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return ClientProductionSignal
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return ClientProductionSignal
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set port
     *
     * @param integer $port
     *
     * @return ClientProductionSignal
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Get port
     *
     * @return integer
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return ClientProductionSignal
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set mould
     *
     * @param string $mould
     *
     * @return ClientProductionSignal
     */
    public function setMould($mould)
    {
        $this->mould = $mould;

        return $this;
    }

    /**
     * Get mould
     *
     * @return string
     */
    public function getMould()
    {
        return $this->mould;
    }

    /**
     * Set mouldgroup
     *
     * @param string $mouldgroup
     *
     * @return ClientProductionSignal
     */
    public function setMouldgroup($mouldgroup)
    {
        $this->mouldgroup = $mouldgroup;

        return $this;
    }

    /**
     * Get mouldgroup
     *
     * @return string
     */
    public function getMouldgroup()
    {
        return $this->mouldgroup;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return ClientProductionSignal
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set production
     *
     * @param integer $production
     *
     * @return ClientProductionSignal
     */
    public function setProduction($production)
    {
        $this->production = $production;

        return $this;
    }

    /**
     * Get production
     *
     * @return integer
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * Set productioncurrent
     *
     * @param integer $productioncurrent
     *
     * @return ClientProductionSignal
     */
    public function setProductioncurrent($productioncurrent)
    {
        $this->productioncurrent = $productioncurrent;

        return $this;
    }

    /**
     * Get productioncurrent
     *
     * @return integer
     */
    public function getProductioncurrent()
    {
        return $this->productioncurrent;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return ClientProductionSignal
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ClientProductionSignal
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;
    }

    /**
     * Set gap
     *
     * @param integer $gap
     *
     * @return ClientProductionSignal
     */
    public function setGap($gap)
    {
        $this->gap = $gap;

        return $this;
    }

    /**
     * Get gap
     *
     * @return int
     */
    public function getGap()
    {
        return $this->gap;
    }

    /**
     * Set energy
     *
     * @param decimal $energy
     *
     * @return ClientProductionSignal
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;
    }

    /**
     * Get energy
     *
     * @return decimal
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set cycletime
     *
     * @param decimal $cycletime
     *
     * @return ClientProductionSignal
     */
    public function setCycletime($cycletime)
    {
        $this->cycletime = $cycletime;
    }

    /**
     * Get cycletime
     *
     * @return decimal
     */
    public function getCycletime()
    {
        return $this->cycletime;
    }

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ClientProductionSignal
     */
    public function setIsexported($isexported)
    {
        $this->isexported = $isexported;

        return $this;
    }

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported()
    {
        return $this->isexported;
    }

    /**
     * Set opsayi
     *
     * @param integer $opsayi
     *
     * @return ClientProductionSignal
     */
    public function setOpsayi($opsayi)
    {
        $this->opsayi = $opsayi;

        return $this;
    }

    /**
     * Get opsayi
     *
     * @return integer
     */
    public function getOpsayi()
    {
        return $this->opsayi;
    }

    /**
     * Set refsayi
     *
     * @param integer $refsayi
     *
     * @return ClientProductionSignal
     */
    public function setRefsayi($refsayi)
    {
        $this->refsayi = $refsayi;

        return $this;
    }

    /**
     * Get refsayi
     *
     * @return integer
     */
    public function getRefsayi()
    {
        return $this->refsayi;
    }

    /**
     * Set losttime
     *
     * @param integer $losttime
     *
     * @return ClientProductionSignal
     */
    public function setLosttime($losttime)
    {
        $this->losttime = $losttime;

        return $this;
    }

    /**
     * Get losttime
     *
     * @return integer
     */
    public function getLosttime()
    {
        return $this->losttime;
    }

    /**
     * Set signalcount
     *
     * @param integer $signalcount
     *
     * @return ClientProductionSignal
     */
    public function setSignalcount($signalcount)
    {
        $this->signalcount = $signalcount;

        return $this;
    }

    /**
     * Get signalcount
     *
     * @return int
     */
    public function getSignalcount()
    {
        return $this->signalcount;
    }

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return ClientProductionSignal
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;
    }

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }
}
