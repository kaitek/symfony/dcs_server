<?php
namespace App\Model;

/**
 * ClientGroupInterface
 */
interface ClientGroupInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return ClientGroupInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();
    
     /**
     * Get groupcode
     *
     * @return string
     */
    function getGroupcode();

    /**
     * Set groupcode
     *
     * @param string $groupcode
     *
     * @return ClientGroupInterface
     */
    function setGroupcode($groupcode);

   
}
