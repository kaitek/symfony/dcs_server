<?php
namespace App\Model;

/**
 * WorkOutSystemInterface
 */
interface WorkOutSystemInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set client
     *
     * @param string $client
     * @return WorkOutSystemInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return WorkOutSystemInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return WorkOutSystemInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string 
     */
    public function getJobrotation();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return WorkOutSystemInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime();

    /**
     * Set description
     *
     * @param string $description
     * @return WorkOutSystemInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set employee
     *
     * @param string $employee
     * @return WorkOutSystemInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee();

    /**
     * Set opnames
     *
     * @param string $opnames
     * @return WorkOutSystemInterface
     */
    public function setOpnames($opnames);

    /**
     * Get opnames
     *
     * @return string 
     */
    public function getOpnames();
    
}
