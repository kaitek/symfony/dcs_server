<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;

/**
 * TaskList
 */
abstract class TaskList extends BaseSync implements BaseSyncInterface, TaskListInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $idx;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $erprefnumber;

    /**
     * @var string
     */
    protected $opcode;

    /**
     * @var integer
     */
    protected $opnumber;

    /**
     * @var string
     */
    protected $opname;

    /**
     * @var string
     */
    protected $opdescription;

    /**
     * @var integer
     */
    protected $productcount;

    /**
     * @var integer
     */
    protected $productdonecount;

    /**
     * @var integer
     */
    protected $productdoneactivity;

    /**
     * @var integer
     */
    protected $productdonejobrotation;

    /**
     * @var integer
     */
    protected $scrapactivity;

    /**
     * @var integer
     */
    protected $scrapjobrotation;

    /**
     * @var integer
     */
    protected $scrappart;

    /**
     * @var \DateTime
     */
    protected $deadline;

    /**
     * @var string
     */
    protected $client;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var decimal
     */
    protected $tpp;

    /**
     * @var bool
     */
    protected $taskfromerp;

    /**
     * @var string
     */
    protected $tfddescription;

    /**
     * @var \DateTime
     */
    protected $start;

    /**
     * @var \DateTime
     */
    protected $finish;

    /**
     * @var \DateTime
     */
    protected $plannedstart;

    /**
     * @var \DateTime
     */
    protected $plannedfinish;

    /**
     * @var integer
     */
    protected $duration;

    /**
     * @var integer
     */
    protected $offtime;

    /**
     * @var integer
     */
    protected $totaltime;

    /**
     * @var string
     */
    protected $resourceid;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $mouldaddress;

    /**
     * @var json
     */
    protected $taskinfo;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idx
     *
     * @param string $idx
     *
     * @return TaskList
     */
    public function setIdx($idx)
    {
        $this->idx = $idx;

        return $this;
    }

    /**
     * Get idx
     *
     * @return string
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TaskList
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return TaskList
     */
    public function setErprefnumber($erprefnumber)
    {
        $this->erprefnumber = $erprefnumber;

        return $this;
    }

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber()
    {
        return $this->erprefnumber;
    }

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return TaskList
     */
    public function setOpcode($opcode)
    {
        $this->opcode = $opcode;

        return $this;
    }

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode()
    {
        return $this->opcode;
    }

    /**
     * Set opnumber
     *
     * @param integer $opnumber
     *
     * @return TaskList
     */
    public function setOpnumber($opnumber)
    {
        $this->opnumber = $opnumber;

        return $this;
    }

    /**
     * Get opnumber
     *
     * @return integer
     */
    public function getOpnumber()
    {
        return $this->opnumber;
    }

    /**
     * Set opname
     *
     * @param string $opname
     *
     * @return TaskList
     */
    public function setOpname($opname)
    {
        $this->opname = $opname;

        return $this;
    }

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname()
    {
        return $this->opname;
    }

    /**
     * Set opdescription
     *
     * @param string $opdescription
     *
     * @return TaskList
     */
    public function setOpdescription($opdescription)
    {
        $this->opdescription = $opdescription;
    }

    /**
     * Get opdescription
     *
     * @return string
     */
    public function getOpdescription()
    {
        return $this->opdescription;
    }

    /**
     * Set productcount
     *
     * @param integer $productcount
     *
     * @return TaskList
     */
    public function setProductcount($productcount)
    {
        $this->productcount = $productcount;

        return $this;
    }

    /**
     * Get productcount
     *
     * @return integer
     */
    public function getProductcount()
    {
        return $this->productcount;
    }

    /**
     * Set productdonecount
     *
     * @param integer $productdonecount
     *
     * @return TaskList
     */
    public function setProductdonecount($productdonecount)
    {
        $this->productdonecount = $productdonecount;

        return $this;
    }

    /**
     * Get productdonecount
     *
     * @return int
     */
    public function getProductdonecount()
    {
        return $this->productdonecount;
    }

    /**
     * Set productdoneactivity
     *
     * @param integer $productdoneactivity
     *
     * @return TaskList
     */
    public function setProductdoneactivity($productdoneactivity)
    {
        $this->productdoneactivity = $productdoneactivity;
    }

    /**
     * Get productdoneactivity
     *
     * @return int
     */
    public function getProductdoneactivity()
    {
        return $this->productdoneactivity;
    }

    /**
     * Set productdonejobrotation
     *
     * @param integer $productdonejobrotation
     *
     * @return TaskList
     */
    public function setProductdonejobrotation($productdonejobrotation)
    {
        $this->productdonejobrotation = $productdonejobrotation;
    }

    /**
     * Get productdonejobrotation
     *
     * @return int
     */
    public function getProductdonejobrotation()
    {
        return $this->productdonejobrotation;
    }

    /**
     * Set scrapactivity
     *
     * @param integer $scrapactivity
     *
     * @return TaskList
     */
    public function setScrapactivity($scrapactivity)
    {
        $this->scrapactivity = $scrapactivity;
    }

    /**
     * Get scrapactivity
     *
     * @return int
     */
    public function getScrapactivity()
    {
        return $this->scrapactivity;
    }

    /**
     * Set scrapjobrotation
     *
     * @param integer $scrapjobrotation
     *
     * @return TaskList
     */
    public function setScrapjobrotation($scrapjobrotation)
    {
        $this->scrapjobrotation = $scrapjobrotation;
    }

    /**
     * Get scrapjobrotation
     *
     * @return int
     */
    public function getScrapjobrotation()
    {
        return $this->scrapjobrotation;
    }

    /**
     * Set scrappart
     *
     * @param integer $scrappart
     *
     * @return TaskList
     */
    public function setScrappart($scrappart)
    {
        $this->scrappart = $scrappart;
    }

    /**
     * Get scrappart
     *
     * @return int
     */
    public function getScrappart()
    {
        return $this->scrappart;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return TaskList
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return TaskList
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return TaskList
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set tpp
     *
     * @param decimal $tpp
     *
     * @return TaskList
     */
    public function setTpp($tpp)
    {
        $this->tpp = $tpp;

        return $this;
    }

    /**
     * Get tpp
     *
     * @return decimal
     */
    public function getTpp()
    {
        return $this->tpp;
    }

    /**
     * Set taskfromerp
     *
     * @param boolean $taskfromerp
     *
     * @return TaskList
     */
    public function setTaskfromerp($taskfromerp)
    {
        $this->taskfromerp = $taskfromerp;

        return $this;
    }

    /**
     * Get taskfromerp
     *
     * @return boolean
     */
    public function getTaskfromerp()
    {
        return $this->taskfromerp;
    }

    /**
     * Set tfddescription
     *
     * @param string $tfddescription
     *
     * @return TaskList
     */
    public function setTfddescription($tfddescription)
    {
        $this->tfddescription = $tfddescription;
    }

    /**
     * Get tfddescription
     *
     * @return string
     */
    public function getTfddescription()
    {
        return $this->tfddescription;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return TaskList
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return TaskList
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set plannedstart
     *
     * @param \DateTime $plannedstart
     *
     * @return TaskList
     */
    public function setPlannedstart($plannedstart)
    {
        $this->plannedstart = $plannedstart;

        return $this;
    }

    /**
     * Get plannedstart
     *
     * @return \DateTime
     */
    public function getPlannedstart()
    {
        return $this->plannedstart;
    }

    /**
     * Set resourceid
     *
     * @param string $resourceid
     *
     * @return TaskList
     */
    public function setResourceid($resourceid)
    {
        $this->resourceid = $resourceid;

        return $this;
    }

    /**
     * Get resourceid
     *
     * @return string
     */
    public function getResourceid()
    {
        return $this->resourceid;
    }

    /**
     * Set plannedfinish
     *
     * @param \DateTime $plannedfinish
     *
     * @return TaskList
     */
    public function setPlannedfinish($plannedfinish)
    {
        $this->plannedfinish = $plannedfinish;

        return $this;
    }

    /**
     * Get plannedfinish
     *
     * @return \DateTime
     */
    public function getPlannedfinish()
    {
        return $this->plannedfinish;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return TaskList
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * Get duration
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set offtime
     *
     * @param integer $offtime
     *
     * @return TaskList
     */
    public function setOfftime($offtime)
    {
        $this->offtime = $offtime;
    }

    /**
     * Get offtime
     *
     * @return int
     */
    public function getOfftime()
    {
        return $this->offtime;
    }

    /**
     * Set totaltime
     *
     * @param integer $totaltime
     *
     * @return TaskList
     */
    public function setTotaltime($totaltime)
    {
        $this->totaltime = $totaltime;
    }

    /**
     * Get totaltime
     *
     * @return int
     */
    public function getTotaltime()
    {
        return $this->totaltime;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TaskList
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set mouldaddress
     *
     * @param string $mouldaddress
     *
     * @return TaskList
     */
    public function setMouldaddress($mouldaddress)
    {
        $this->mouldaddress = $mouldaddress;

        return $this;
    }

    /**
     * Get mouldaddress
     *
     * @return string
     */
    public function getMouldaddress()
    {
        return $this->mouldaddress;
    }

    /**
     * Set taskinfo
     *
     * @param boolean $taskinfo
     *
     * @return TaskList
     */
    public function setTaskinfo($taskinfo)
    {
        $this->taskinfo = $taskinfo;

        return $this;
    }

    /**
     * Get taskinfo
     *
     * @return boolean
     */
    public function getTaskinfo()
    {
        return $this->taskinfo;
    }
}
