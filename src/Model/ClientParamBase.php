<?php

namespace App\Model;

use App\Model\BaseSync;
use App\Model\BaseSyncInterface;
/**
 * ClientParamBase
 */
abstract class ClientParamBase extends BaseSync implements BaseSyncInterface, ClientParamBaseInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $optiongroup;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $optiontype;

    /**
     * @var string
     */
    protected $options;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optiongroup
     *
     * @param string $optiongroup
     *
     * @return ClientParamBase
     */
    public function setOptiongroup($optiongroup)
    {
        $this->optiongroup = $optiongroup;

        return $this;
    }

    /**
     * Get optiongroup
     *
     * @return string
     */
    public function getOptiongroup()
    {
        return $this->optiongroup;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ClientParamBase
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ClientParamBase
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set optiontype
     *
     * @param string $optiontype
     *
     * @return ClientParamBase
     */
    public function setOptiontype($optiontype)
    {
        $this->optiontype = $optiontype;

        return $this;
    }

    /**
     * Get optiontype
     *
     * @return string
     */
    public function getOptiontype()
    {
        return $this->optiontype;
    }

    /**
     * Set options
     *
     * @param string $options
     *
     * @return ClientParamBase
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }
}

