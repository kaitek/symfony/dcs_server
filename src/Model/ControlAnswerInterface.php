<?php

namespace App\Model;

/**
 * ControlAnswerInterface
 */
interface ControlAnswerInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ControlAnswerInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set client
     *
     * @param string $client
     * @return ControlAnswerInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string
     */
    public function getClient();

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ControlAnswerInterface
     */
    public function setDay($day);

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay();

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     * @return ControlAnswerInterface
     */
    public function setJobrotation($jobrotation);

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ControlAnswerInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string
     */
    public function getOpname();

    /**
     * Set erprefnumber
     *
     * @param string $erprefnumber
     *
     * @return ControlAnswerInterface
     */
    public function setErprefnumber($erprefnumber);

    /**
     * Get erprefnumber
     *
     * @return string
     */
    public function getErprefnumber();

    /**
     * Set employee
     *
     * @param string $employee
     * @return ControlAnswerInterface
     */
    public function setEmployee($employee);

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee();

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return ControlAnswerInterface
     */
    public function setTime($time);

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime();

    /**
     * Set question
     *
     * @param string $question
     * @return ControlAnswerInterface
     */
    public function setQuestion($question);

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion();

    /**
     * Set answertype
     *
     * @param string $answertype
     * @return ControlAnswerInterface
     */
    public function setAnswertype($answertype);

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype();

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     * @return ControlAnswerInterface
     */
    public function setValuerequire($valuerequire);

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire();

    /**
     * Set valuemin
     *
     * @param string $valuemin
     * @return ControlAnswerInterface
     */
    public function setValuemin($valuemin);

    /**
     * Get valuemin
     *
     * @return string
     */
    public function getValuemin();

    /**
     * Set valuemax
     *
     * @param string $valuemax
     * @return ControlAnswerInterface
     */
    public function setValuemax($valuemax);

    /**
     * Get valuemax
     *
     * @return string
     */
    public function getValuemax();

    /**
     * Set answer
     *
     * @param string $answer
     * @return ControlAnswerInterface
     */
    public function setAnswer($answer);

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer();

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ControlAnswerInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set isexported
     *
     * @param boolean $isexported
     *
     * @return ControlAnswerInterface
     */
    public function setIsexported($isexported);

    /**
     * Get isexported
     *
     * @return boolean
     */
    public function getIsexported();

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ControlAnswerInterface
     */
    public function setDocumentnumber($documentnumber);

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber();

    /**
     * Set isallowna
     *
     * @param boolean $isallowna
     *
     * @return ControlAnswerInterface
     */
    public function setIsallowna($isallowna);

    /**
     * Get isallowna
     *
     * @return boolean
     */
    public function getIsallowna();
}
