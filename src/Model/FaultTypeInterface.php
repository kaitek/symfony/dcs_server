<?php
namespace App\Model;

/**
 * FaultTypeInterface
 */
interface FaultTypeInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     * @return FaultTypeInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode();

    /**
     * Set description
     *
     * @param string $description
     * @return FaultTypeInterface
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription();

    /**
     * Set client
     *
     * @param string $client
     * @return FaultTypeInterface
     */
    public function setClient($client);

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient();

    /**
     * Set faultgroupcode
     *
     * @param string $faultgroupcode
     * @return FaultTypeInterface
     */
    public function setFaultgroupcode($faultgroupcode);

    /**
     * Get faultgroupcode
     *
     * @return string 
     */
    public function getFaultgroupcode();

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return FaultTypeInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     * @return FaultTypeInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime 
     */
    public function getFinish();
    
}
