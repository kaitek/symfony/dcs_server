<?php
namespace App\Model;

/**
 * ActivityControlQuestionInterface
 */
interface ActivityControlQuestionInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return ActivityControlQuestionInterface
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string 
     */
    public function getType();

    /**
     * Set opname
     *
     * @param string $opname
     * @return ActivityControlQuestionInterface
     */
    public function setOpname($opname);

    /**
     * Get opname
     *
     * @return string 
     */
    public function getOpname();

    /**
     * Set documentnumber
     *
     * @param string $documentnumber
     *
     * @return ActivityControlQuestionInterface
     */
    public function setDocumentnumber($documentnumber);

    /**
     * Get documentnumber
     *
     * @return string
     */
    public function getDocumentnumber();

    /**
     * Set question
     *
     * @param string $question
     * @return ActivityControlQuestionInterface
     */
    public function setQuestion($question);

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion();

    /**
     * Set answertype
     *
     * @param string $answertype
     * @return ActivityControlQuestionInterface
     */
    public function setAnswertype($answertype);

    /**
     * Get answertype
     *
     * @return string 
     */
    public function getAnswertype();

    /**
     * Set valuerequire
     *
     * @param string $valuerequire
     *
     * @return ActivityControlQuestionInterface
     */
    public function setValuerequire($valuerequire);

    /**
     * Get valuerequire
     *
     * @return string
     */
    public function getValuerequire();

    /**
     * Set valuemin
     *
     * @param string $valuemin
     * @return ActivityControlQuestionInterface
     */
    public function setValuemin($valuemin);

    /**
     * Get valuemin
     *
     * @return string 
     */
    public function getValuemin();

    /**
     * Set valuemax
     *
     * @param string $valuemax
     * @return ActivityControlQuestionInterface
     */
    public function setValuemax($valuemax);

    /**
     * Get valuemax
     *
     * @return string 
     */
    public function getValuemax();
    /**
     * Get id
     *
     * @return integer 
     */

    /**
     * Set period
     *
     * @param string $period
     *
     * @return ActivityControlQuestionInterface
     */
    public function setPeriod($period);

    /**
     * Get period
     *
     * @return string
     */
    public function getPeriod();

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return ActivityControlQuestionInterface
     */
    public function setStart($start);

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * Set finish
     *
     * @param \DateTime $finish
     *
     * @return ActivityControlQuestionInterface
     */
    public function setFinish($finish);

    /**
     * Get finish
     *
     * @return \DateTime
     */
    public function getFinish();
    
}
