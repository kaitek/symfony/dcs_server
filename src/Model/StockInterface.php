<?php
namespace App\Model;

/**
 * StockInterface
 */
interface StockInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set code
     *
     * @param string $code
     *
     * @return StockInterface
     */
    public function setCode($code);

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set opcode
     *
     * @param string $opcode
     *
     * @return StockInterface
     */
    public function setOpcode($opcode);

    /**
     * Get opcode
     *
     * @return string
     */
    public function getOpcode();

    /**
     * Set packcapacity
     *
     * @param decimal $packcapacity
     *
     * @return StockInterface
     */
    public function setPackcapacity($packcapacity);

    /**
     * Get packcapacity
     *
     * @return decimal
     */
    public function getPackcapacity();

    /**
     * Set quantityremaining
     *
     * @param decimal $quantityremaining
     *
     * @return StockInterface
     */
    public function setQuantityremaining($quantityremaining);

    /**
     * Get quantityremaining
     *
     * @return decimal
     */
    public function getQuantityremaining();

    /**
     * Set clients
     *
     * @param string $clients
     *
     * @return StockInterface
     */
    public function setClients($clients);

    /**
     * Get clients
     *
     * @return string
     */
    public function getClients();

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return StockInterface
     */
    public function setFilename($filename);

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename();
}
