<?php

namespace App\Model;

/**
 * EmployeeEfficiency
 */
abstract class EmployeeEfficiency implements EmployeeEfficiencyInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $day;

    /**
     * @var string
     */
    protected $jobrotation;

    /**
     * @var string
     */
    protected $teamleader;

    /**
     * @var integer
     */
    protected $week;

    /**
     * @var string
     */
    protected $employee;

    /**
     * @var boolean
     */
    protected $isovertime;

    /**
     * @var string
     */
    protected $fulltime;

    /**
     * @var string
     */
    protected $breaktime;

    /**
     * @var string
     */
    protected $worktime;

    /**
     * @var string
     */
    protected $optime;

    /**
     * @var string
     */
    protected $discontinuity;

    /**
     * @var string
     */
    protected $losttime;

    /**
     * @var string
     */
    protected $reworkoptime;

    /**
     * @var string
     */
    protected $mreworkduration;

    /**
     * @var string
     */
    protected $mlosttime;

    /**
     * @var string
     */
    protected $rlosttime;

    /**
     * @var string
     */
    protected $rmlosttime;

    /**
     * @var string
     */
    protected $worktimewithoutlost;

    /**
     * @var string
     */
    protected $efficiency;

    /**
     * @var string
     */
    protected $tempo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return EmployeeEfficiency
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set jobrotation
     *
     * @param string $jobrotation
     *
     * @return EmployeeEfficiency
     */
    public function setJobrotation($jobrotation)
    {
        $this->jobrotation = $jobrotation;

        return $this;
    }

    /**
     * Get jobrotation
     *
     * @return string
     */
    public function getJobrotation()
    {
        return $this->jobrotation;
    }

    /**
     * Set teamleader
     *
     * @param string $teamleader
     *
     * @return EmployeeEfficiency
     */
    public function setTeamleader($teamleader)
    {
        $this->teamleader = $teamleader;

        return $this;
    }

    /**
     * Get teamleader
     *
     * @return string
     */
    public function getTeamleader()
    {
        return $this->teamleader;
    }

    /**
     * Set week
     *
     * @param integer $week
     *
     * @return EmployeeEfficiency
     */
    public function setWeek($week)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return integer
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set employee
     *
     * @param string $employee
     *
     * @return EmployeeEfficiency
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set isovertime
     *
     * @param boolean $isovertime
     *
     * @return EmployeeEfficiency
     */
    public function setIsovertime($isovertime)
    {
        $this->isovertime = $isovertime;

        return $this;
    }

    /**
     * Get isovertime
     *
     * @return boolean
     */
    public function getIsovertime()
    {
        return $this->isovertime;
    }

    /**
     * Set fulltime
     *
     * @param string $fulltime
     *
     * @return EmployeeEfficiency
     */
    public function setFulltime($fulltime)
    {
        $this->fulltime = $fulltime;

        return $this;
    }

    /**
     * Get fulltime
     *
     * @return string
     */
    public function getFulltime()
    {
        return $this->fulltime;
    }

    /**
     * Set breaktime
     *
     * @param string $breaktime
     *
     * @return EmployeeEfficiency
     */
    public function setBreaktime($breaktime)
    {
        $this->breaktime = $breaktime;

        return $this;
    }

    /**
     * Get breaktime
     *
     * @return string
     */
    public function getBreaktime()
    {
        return $this->breaktime;
    }

    /**
     * Set worktime
     *
     * @param string $worktime
     *
     * @return EmployeeEfficiency
     */
    public function setWorktime($worktime)
    {
        $this->worktime = $worktime;

        return $this;
    }

    /**
     * Get worktime
     *
     * @return string
     */
    public function getWorktime()
    {
        return $this->worktime;
    }

    /**
     * Set optime
     *
     * @param string $optime
     *
     * @return EmployeeEfficiency
     */
    public function setOptime($optime)
    {
        $this->optime = $optime;

        return $this;
    }

    /**
     * Get optime
     *
     * @return string
     */
    public function getOptime()
    {
        return $this->optime;
    }

    /**
     * Set discontinuity
     *
     * @param string $discontinuity
     *
     * @return EmployeeEfficiency
     */
    public function setDiscontinuity($discontinuity)
    {
        $this->discontinuity = $discontinuity;

        return $this;
    }

    /**
     * Get discontinuity
     *
     * @return string
     */
    public function getDiscontinuity()
    {
        return $this->discontinuity;
    }

    /**
     * Set losttime
     *
     * @param string $losttime
     *
     * @return EmployeeEfficiency
     */
    public function setLosttime($losttime)
    {
        $this->losttime = $losttime;

        return $this;
    }

    /**
     * Get losttime
     *
     * @return string
     */
    public function getLosttime()
    {
        return $this->losttime;
    }

    /**
     * Set reworkoptime
     *
     * @param string $reworkoptime
     *
     * @return EmployeeEfficiency
     */
    public function setReworkoptime($reworkoptime)
    {
        $this->reworkoptime = $reworkoptime;

        return $this;
    }

    /**
     * Get reworkoptime
     *
     * @return string
     */
    public function getReworkoptime()
    {
        return $this->reworkoptime;
    }

    /**
     * Set mreworkduration
     *
     * @param string $mreworkduration
     *
     * @return EmployeeEfficiency
     */
    public function setMreworkduration($mreworkduration)
    {
        $this->mreworkduration = $mreworkduration;

        return $this;
    }

    /**
     * Get mreworkduration
     *
     * @return string
     */
    public function getMreworkduration()
    {
        return $this->mreworkduration;
    }

    /**
     * Set mlosttime
     *
     * @param string $mlosttime
     *
     * @return EmployeeEfficiency
     */
    public function setMlosttime($mlosttime)
    {
        $this->mlosttime = $mlosttime;

        return $this;
    }

    /**
     * Get mlosttime
     *
     * @return string
     */
    public function getMlosttime()
    {
        return $this->mlosttime;
    }

    /**
     * Set rlosttime
     *
     * @param string $rlosttime
     *
     * @return EmployeeEfficiency
     */
    public function setRlosttime($rlosttime)
    {
        $this->rlosttime = $rlosttime;

        return $this;
    }

    /**
     * Get rlosttime
     *
     * @return string
     */
    public function getRlosttime()
    {
        return $this->rlosttime;
    }

    /**
     * Set rmlosttime
     *
     * @param string $rmlosttime
     *
     * @return EmployeeEfficiency
     */
    public function setRmlosttime($rmlosttime)
    {
        $this->rmlosttime = $rmlosttime;

        return $this;
    }

    /**
     * Get rmlosttime
     *
     * @return string
     */
    public function getRmlosttime()
    {
        return $this->rmlosttime;
    }

    /**
     * Set worktimewithoutlost
     *
     * @param string $worktimewithoutlost
     *
     * @return EmployeeEfficiency
     */
    public function setWorktimewithoutlost($worktimewithoutlost)
    {
        $this->worktimewithoutlost = $worktimewithoutlost;

        return $this;
    }

    /**
     * Get worktimewithoutlost
     *
     * @return string
     */
    public function getWorktimewithoutlost()
    {
        return $this->worktimewithoutlost;
    }

    /**
     * Set efficiency
     *
     * @param string $efficiency
     *
     * @return EmployeeEfficiency
     */
    public function setEfficiency($efficiency)
    {
        $this->efficiency = $efficiency;

        return $this;
    }

    /**
     * Get efficiency
     *
     * @return string
     */
    public function getEfficiency()
    {
        return $this->efficiency;
    }

    /**
     * Set tempo
     *
     * @param string $tempo
     *
     * @return EmployeeEfficiency
     */
    public function setTempo($tempo)
    {
        $this->tempo = $tempo;

        return $this;
    }

    /**
     * Get tempo
     *
     * @return string
     */
    public function getTempo()
    {
        return $this->tempo;
    }
}

