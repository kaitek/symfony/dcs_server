<?php
  $nodeport=$_GET['nodeport'];
	$node=$_SERVER['HTTP_HOST'].":".$nodeport;
	function get_web_page( $url )
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_POST		   => true,
			CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
			CURLOPT_SSL_VERIFYHOST => false
		);
		$headers = [
			'Cache-Control: no-cache',
			'Connection: keep-alive',
			'content-length: 0',
		];
		
		$ch      = curl_init( $url );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	$ret= get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/Watch/getClientData");
	if($ret['errno']!=0){
		var_dump($ret);
	}else{
		try{
			$j_ret=json_decode($ret['content']);
			if(is_object($j_ret)){
				$str="";
				//echo($j_ret);
			}else{
				echo($ret['content']);
			}
		}catch (Exception $e) {
			echo($ret['content']);
		}
	}
?>
<!DOCTYPE html>
<html lang="tr">
	<head>
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='cache-control' content='no-cache'>
		<title>Verimot</title>
		<link rel="stylesheet" type="text/css" href="../../lib/km/dist/km.min.css"/>
		<script src="../../lib/km/dist/km.js?i=1"></script>
		<style>
			.flex-container {
			display: flex;
			flex-direction: column;
			}
			
			.flex-container>div {
			margin: 1px;
			text-align: center;
			font-size: 20px;
			}

			.flex-container-wrap {
			display: flex;
			flex-wrap: wrap;
			align-items: flex-start;
			flex-shrink: 0;
			}
			.flex-container-wrap > div {
			margin: 5px;
			flex-grow: 0;
			}

			.group-header {
				text-align: left;
				margin:0px;
				margin-left: 5px;
			}
		</style>
	</head>
	<body style="-moz-user-select:none;-khtml-user-select: none;overflow-x: hidden;padding: 0px;margin: 0px;" unselectable="on">
		<script src='https://<?=$node?>/socket.io/socket.io.js'></script>
		<script type='text/javascript'>
			$(document).on({
				"contextmenu": function(e) {
					e.preventDefault();
					//alert("contextmenu");
				},
				"mousedown": function(e) { 
					
				},
				"mouseup": function(e) { 
					//alert("mouseup"+e.button);
				}
			});
			$(function() {
				var d={};
				var myVar = setInterval(myTimer, 30000);

				function myTimer() {
					//console.log('myTimer');
					var url='https://<?=$_SERVER['HTTP_HOST'].$_SERVER['BASE']?>/tr/Watch/getWorkshopValues';
					$.post( url, function( data ) {
						//$( ".result" ).html( data );
						//console.log(data);
						var _device_count=0;
						var _total_tempo=0;
						for(var i=0;i<app.devices.length;i++){
							var _device=app.devices[i];
							if(_device.workflow!=='El İşçiliği'&&_device.status&&_device.status.indexOf('KİMSE YOK')==-1&&_device.status.indexOf('İŞ YOK')==-1){
								_device_count++;
								if(_device.tempo){
									_total_tempo+=_device.tempo;
								}
							}
						}
						if(data.case_labels){
							app.watch.setCaseLabels(data.case_labels);
							$('.erpref',app.watch.renderTo).removeClass('waitmaterial');
							for(var i=0;i<data.case_labels.length;i++){
								var item=data.case_labels[i];
								$(".erpref:contains('"+item.erprefnumber+"')",app.watch.renderTo).addClass('waitmaterial');
							}
							// console.warn(data.case_labels);
						}
						if(data.poka_yoke){
							app.watch.setPokaYoke(data.poka_yoke);
						}
						if(data.workshop){
							//console.log(data);
							if($('.row-header').length==0){
								$( ".cnt-watch" ).prepend('<div class="flex-container-row row-header"></div>' );
							}
							$('.row-header').html('<p class="group-header">Vardiyada Planlanan:'+data.workshop.planlanan+' Çalışan:'+data.workshop.calisan+' Boşta:'+data.workshop.bosta+((_device_count>0&&_total_tempo>0)?' Atölye Verimi:%'+Math.round(_total_tempo/_device_count,2):'')+'</p>');
						}
					});
				}

				function myStopFunction() {
					clearInterval(myVar);
				}
				window.onbeforeunload = function(event){
					if(d && d.socket)
						d.socket.disconnect();
					myStopFunction();
				};
				try{
					d.socket=io.connect('<?=$node?>');
					window.app = new km.app({});
					app.groups=[];
					app.devices=[];
					app.watch=new km.watch({parent:app,renderTo:app.getBody()});
					myTimer();
					d.socket.on('connecting', function () {
						console.log('connecting');
					});

					d.socket.on('connect', function(s){
						console.log('connect');
					});
					
					d.socket.on('reconnecting', function () {
						//console.log('reconnecting');
					});
					
					d.socket.on('reconnect', function () {
						//console.log('reconnect');
					});
					
					d.socket.on('reconnect_failed', function () {
						//console.log('reconnect_failed');
					});
					
					d.socket.on('disconnect', function () {
						console.log('disconnect');
					});

					d.socket.on('cn_js_watch', function(obj){
						var t=this;
						//console.log(obj);
						var event=obj.event;
						switch(event){
						case "device_connect":
							var _c_i=km.getIndex(app.devices,"ip",obj.ip);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"connect"});
							//}else{
							//	obj.renderTo=t.cntWatch;
							//	t.devices.push(new km.watch_device(obj));
							}
							break;
						case "device_disconnect":
							var _c_i=km.getIndex(app.devices,"ip",obj.ip);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"disconnect"});
							}
							break;
						case "device_io_info":
							var _c_i=km.getIndex(app.devices,"ip",obj.ip);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"device_io_info",type:obj.data.type});
							}
							break;
						case "watch_info":
							//console.log(obj);
							var _c_i=km.getIndex(app.devices,"code",obj.data.client);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								if (typeof Object.assign != 'function') {
									km.apply(cl,obj.data);
								}else{
									Object.assign(cl,obj.data);
								}
								cl.trigger("statuschange",{event:"watch_info"});
							}
							break;
						case "watch_input_signal":
							var _c_i=km.getIndex(app.devices,"code",obj.data.client);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"watch_input_signal",item:obj.data.item,processVariables:obj.data.processVariables});
							}
							break;
						case "watch_calc_tempo":
							var _c_i=km.getIndex(app.devices,"code",obj.data.client);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"watch_calc_tempo",item:obj.data});
							}
							break;
						case "updateApp":
							var _c_i=km.getIndex(app.devices,"ip",obj.ip);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"updateApp",item:obj.data});
							}
							break;
						case "lockpc":
							var _c_i=km.getIndex(app.devices,"ip",obj.ip);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"lockpc",item:obj.data});
							}
							break;
						case "unlockpc":
							var _c_i=km.getIndex(app.devices,"ip",obj.ip);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:"unlockpc",item:obj.data});
							}
							break;
						case 'calculated_oee':
							for(var i=0;i<obj.data.length;i++){
								var _c_i=km.getIndex(app.devices,"code",obj.data[i].client);
								if(_c_i>-1){
									var cl=app.devices[_c_i];
									cl.trigger("statuschange",{event:"calculated_oee",item:obj.data[i]});
								}
							}
							break;
						default:
							var _c_i=km.getIndex(app.devices,"code",obj.data.client);
							if(_c_i>-1){
								var cl=app.devices[_c_i];
								cl.trigger("statuschange",{event:event,item:obj.data});
							}
						break;
					}
					});

					<?php
						if(is_object($j_ret)){
							echo("app.result=(".json_encode($j_ret).");");
						}
					?>
					if(app.result){
						/*
						for(var i=0;i<3;i++){
							$('<div class="flex-container-row row-'+i+'"><p class="group-header">Sabit '+i+'</p></div><div class="flex-container-wrap d-row-'+i+'"></div>').appendTo(app.watch.cntWatch);
						}
						app.result.groups["Sabit 1"].items["03.06.0002"].renderTo=$('.d-row-0',app.watch.cntWatch);
												app.result.groups["Sabit 1"].items["03.06.0002"].self=app;
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));

						app.result.groups["Sabit 1"].items["03.06.0002"].renderTo=$('.d-row-1',app.watch.cntWatch);
												app.result.groups["Sabit 1"].items["03.06.0002"].self=app;
												app.devices.push(new km.watch_device(app.result.groups["Sabit 1"].items["03.06.0002"]));
						*/
						var i=0;
						for (var group in app.result.groups) { 
							$('<div class="flex-container-row row-'+i+'"><p class="group-header">'+group+'</p></div><div class="flex-container-wrap d-row-'+i+'"></div>').appendTo(app.watch.cntWatch);
							for(var item in app.result.groups[group].items){
								app.result.groups[group].items[item].renderTo=$('.d-row-'+i,app.watch.cntWatch);
								app.result.groups[group].items[item].self=app;
								app.devices.push(new km.watch_device(app.result.groups[group].items[item]));
							}
							i++;
						}
						d.socket.emit('js_cn_watch', {event:"getDataAllClients"});
					}
				}
				catch(err)
				{
					console.log(err);
					//alert(err);
					//location.reload();
				}	
			});	
		</script>
	</body>
</html>