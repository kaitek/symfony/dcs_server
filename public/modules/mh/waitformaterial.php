<?php
	header("Refresh:30");
	error_reporting(E_ALL);
	function get_web_page( $url )
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
			CURLOPT_SSL_VERIFYHOST => false
		);

		$headers = [
			'Cache-Control: no-cache',
			'Connection: keep-alive',
			'content-length: 0',
		];
		
		$ch      = curl_init( $url );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	$ret= get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/MaterialPrepare/WaitForMaterial/");
	if($ret['errno']!=0){
		var_dump($ret);
	}else{
		try{
			$j_ret=json_decode($ret['content']);
			if(is_object($j_ret)){
				$str="";
				//echo($j_ret->errno);
				foreach($j_ret->data as $row){
					//mjd($row);
					$str=$str."<tr unselectable=\"on\" style=\"".($row->islemtip=='ERP STOK YOK'?'color:red;':'')."\">"
						//."<td unselectable=\"on\"><strong unselectable=\"on\">".$row->carrier."</strong></td>"
						."<td unselectable=\"on\"><strong unselectable=\"on\">".$row->code."-".$row->number."</strong></td>"
						."<td unselectable=\"on\">".$row->islemtip."</td>"
						."<td unselectable=\"on\">".($row->tasktype=='GRV001'?$row->casename:$row->stockcode)."</td>"
						."<td unselectable=\"on\">".$row->erprefnumber."</td>"
						//."<td unselectable=\"on\">".($row->locationsourceinfo?$row->locationsourceinfo:$row->locationsource)."</td>"
						//."<td unselectable=\"on\">".($row->locationdestinationinfo?$row->locationdestinationinfo:$row->locationdestination)."</td>"
						."<td unselectable=\"on\">".$row->starttime."</td>"
						//."<td unselectable=\"on\">".$row->empname."</td>"
						."</tr>";
				}
			}else{
				echo($ret['content']);
			}
		}catch (Exception $e) {
			echo($ret['content']);
		}
	}
?>
<html lang="tr">
	<head>
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='cache-control' content='no-cache'>
		<title>Verimot</title>
		<link href="../main/inc/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="../main/inc/html5shiv.min.js"></script>
		<script src="../main/inc/respond.min.js"></script>
		<![endif]-->
	</head>
	<body style="-moz-user-select:none;-khtml-user-select: none;overflow-x: hidden;padding: 0px;margin: 0px;" unselectable="on">
		<table id='table' class='table' unselectable='on' style='font-size:20px; margin-bottom: 0px;'>
			<thead unselectable='on'>
				<tr unselectable='on'>
					<th unselectable='on'>Operasyon</th>
					<th unselectable='on'>İşlem Tipi</th>
					<th unselectable='on'>Malzeme</th>
					<th unselectable='on'>KD No</th>
					<th unselectable='on'>Zaman</th>
					<!--<th unselectable='on'>Görevli</th>-->
				</tr>
			</thead>
			<tbody unselectable='on'>
				
			</tbody>
		</table>
		<script src="../main/inc/jquery.min.js"></script>
		<script src="../main/inc/jquery.browser.min.js"></script>
		<script src="../main/inc/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		<script type='text/javascript'>
			$(document).on({
				"contextmenu": function(e) {
					e.preventDefault();
					//alert("contextmenu");
				},
				"mousedown": function(e) { 
					
				},
				"mouseup": function(e) { 
					//alert("mouseup"+e.button);
				}
			});
			try{
				$.find('tbody')[0].innerHTML='<?=$str?>';
			}
			catch(err)
			{
				location.reload();
			}			
		</script>
	</body>
</html>