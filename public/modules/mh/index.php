<?php
	require_once("../debug.php");
	header("Refresh:30");
	error_reporting(E_ALL);
	function get_web_page( $url )
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
			CURLOPT_SSL_VERIFYHOST => false
		);

		$headers = [
			'Cache-Control: no-cache',
			'Connection: keep-alive',
			'content-length: 0',
		];
		
		$ch      = curl_init( $url );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	$ret= get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/MaterialPrepare/PrepareData");
	if($ret['errno']!=0){
		var_dump($ret);
	}else{
		try{
			$j_ret=json_decode($ret['content']);
			if(is_object($j_ret)){
				mjd($j_ret,1);
			}else{
				//echo($ret['content']);
				mjd($ret['content'],1);
			}
		}catch (Exception $e) {
			echo($ret['content']);
		}
	}
?>