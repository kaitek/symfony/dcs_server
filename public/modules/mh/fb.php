<?php
header("Refresh:60");
error_reporting(E_ALL);
function get_web_page($url)
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
        CURLOPT_SSL_VERIFYHOST => false
    );

    $headers = [
        'Cache-Control: no-cache',
        'Connection: keep-alive',
        'content-length: 0',
    ];

    $ch      = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $err     = curl_errno($ch);
    $errmsg  = curl_error($ch);
    $header  = curl_getinfo($ch);
    curl_close($ch);

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}
$ret = get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/MaterialPrepare/ForkliftBekleme");
if ($ret['errno'] != 0) {
    var_dump($ret);
} else {
    try {
        //$arr_source_losttype = array(
        //	"SUNDURMA BÖLGESİNE PARÇA GÖTÜRME/GELME"
        //	,"YARIMAMUL/MAMUL ALMAYA GİTME"
        //	,"YARIMAMUL/MAMUL BÖLGESİNDE PARÇA ARAMA"
        //	,"KASA ARAMA"
        //	,"BARKOD ÇIKARTMA"
        //	,"YARI MAMUL ALANINDAN BULAMAMA"
        //	,"SUPER MARKETTE BULAMAMA"
        //	,"BASKA YERDE GÖREVLİ"
        //	,"İHTİYAC MOLASI"
        //	,"KASA BULUNMAMASI"
        //	,"MAKAS BÖLGESİNDE GÖREVLİ"
        //	,"SAC İNDİRMEDE GÖREVLİ"
        //	,"YENİ PRESHANEDE GÖREVLİ"
        //	,"KALIP TAŞIMADA GÖREVLİ"
        //);
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/ClientLostDetail/sourcelosttype/";
        //$str_source_losttype = '<select size=10 style="font-size:22px">';
        //foreach ($arr_source_losttype as $item) {
        //    $str_source_losttype .= '<option value="'.$item.'">'.$item.'</option>';
        //}
        //$str_source_losttype .= '</select>';
        $rows = '';
        $j_ret = json_decode($ret['content']);
        if (is_object($j_ret)) {
            $str = "";
            //echo($j_ret->errno);
            $rows = $j_ret->data;
            foreach ($j_ret->data as $row) {
                //mjd($row);
                $str = $str."<tr unselectable=\"on\" onclick=gorevkapat(".$row->id.")>"
                    ."<td unselectable=\"on\"><strong unselectable=\"on\">".$row->client."</strong></td>"
                    ."<td unselectable=\"on\"><strong unselectable=\"on\">".$row->start."</strong></td>"
                    ."<td unselectable=\"on\"><strong unselectable=\"on\">".$row->losttype."</td>"
                    ."<td unselectable=\"on\"><strong unselectable=\"on\">".$row->opname."</td>"
                    //."<td unselectable=\"on\"><strong unselectable=\"on\">".(in_array($row->sourcelosttype, $arr_source_losttype) ? $row->sourcelosttype : "")."</strong></td>"
                    ."</tr>";
            }
        } else {
            echo($ret['content']);
        }
    } catch (Exception $e) {
        echo($ret['content']);
    }
}

?>
<html lang="tr">
	<head>
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='cache-control' content='no-cache'>
		<title>Verimot</title>
		<link href="../main/inc/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="../../lib/Sentry/polyfill.min.js?features=Promise"></script>
		<script src="../../lib/Sentry/bundle.min.js" ></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="../main/inc/html5shiv.min.js"></script>
		<script src="../main/inc/respond.min.js"></script>
		<![endif]-->
	</head>
	<body style="-moz-user-select:none;-khtml-user-select: none;overflow-x: hidden;padding: 0px;margin: 0px;" unselectable="on">
		<table id='table' class='table' unselectable='on' style='font-size:20px; margin-bottom: 0px;'>
			<thead unselectable='on'>
				<tr unselectable='on'>
					<th unselectable='on'>İstasyon</th>
					<th unselectable='on'>Başlangıç</th>
					<th unselectable='on'>Kayıp Sebebi</th>
					<th unselectable='on'>Operasyon</th>
				</tr>
			</thead>
			<tbody unselectable='on'>
				
			</tbody>
		</table>
		<button id="btn_task_done" type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmTaskDone" style="display:none;">
			Launch demo modal
		</button>
		<!-- Modal -->
		<div class="modal fade" id="confirmTaskDone" tabindex="-1" role="dialog" aria-labelledby="confirmTaskDoneTitle" aria-hidden="true" >
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle" style="font-size:20px;"><center>Kayıp Sebebi</center></h5>
				</div>
				<div class="modal-body" style="font-size:16px;">
					...
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Vazgeç</button>
					<button type="button" class="btn btn-primary" id="confirm-task-done">Kaydet</button>
				</div>
				</div>
			</div>
		</div>
		<script src="../main/inc/jquery.min.js"></script>
		<script src="../main/inc/jquery.browser.min.js"></script>
		<script src="../main/inc/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		<script type='text/javascript'>
			//Sentry.init({
      //          dsn: 'http://11c0528d52054012bf608d6ccbf17021@sentry.kaitek.com.tr/9'
                //,integrations: [new MyAwesomeIntegration()]
			//});
			var initMyApp = function(){
				$(document).on({
					"contextmenu": function(e) {
						e.preventDefault();
						//alert("contextmenu");
					},
					"mousedown": function(e) { 
						
					},
					"mouseup": function(e) { 
						//alert("mouseup"+e.button);
					}
				});

				var d={};
				window.d=d;
				d.selectedRow=false;
				d.records=<?=json_encode($rows)?>;
				window.gorevkapat=function(idx){
					if(d.records){
						var row=false;
						for(var i=0;i<d.records.length;i++){
							if(!row){
								if(d.records[i].id==idx){
									row=d.records[i];
									break;
								}
							}
						}
						if(row!==false){
							d.selectedRow=row;
							//console.log(row);
							// $('.modal-body')[0].innerHTML='<_? echo($str_source_losttype) ?_>';
							$('.modal-body')[0].innerHTML=row.client+' istasyonundaki '+row.losttype+' kaybını sonlandırmak istediğinize emin misiniz?';
							$('#btn_task_done').click();
						}
					}else{
						d.selectedRow=false;
					}
				};
				$('#confirm-task-done').on('click', function(event) {
						var $button = $(event.target); // The clicked button
						//var options=$('option','.modal-body');
						//var selectedoption=false;
						//for(var i=0;i<options.length;i++){
						//	if(!selectedoption){
						//		if(options[i].selected){
						//			selectedoption=options[i].value;
						//			break;
						//		}
						//	}
						//}
						//if(selectedoption!==false&&d.selectedRow!==false){
						if(d.selectedRow!==false){
							$('#confirm-task-done')[0].disabled=true;
							$.ajax({
								url: '<?echo($url)?>'+d.selectedRow.id,
								method: "POST",
								//data: JSON.stringify({sourcelosttype:selectedoption}),
								data: JSON.stringify(d.selectedRow),
								headers: { 
									"Content-Type": "application/json",
									"X-HTTP-Method-Override": 'PUT'
								},
								dataType: 'json',
								success: function(data, textStatus, jqXHR) {
									d.selectedRow=false;
								},
								error: function(jqXHR, textStatus, errorThrown) {
									//mj.message({
									//	title: mj.lng.glb.error,
									//	msg: jqXHR.responseJSON.message + "<br>" + jqXHR.responseJSON.file,
									//	type: "error",
									//	html: true
									//});
									d.selectedRow=false;
									setTimeout(() => {
										$('#btn_task_done').click();
										location.reload();
									}, 3000);
								},
								complete: function(jqXHR, textStatus) {
									d.selectedRow=false;
									setTimeout(() => {
										$('#btn_task_done').click();
										location.reload();
									}, 3000);
								}
							});
						}
					});
				try{
					$.find('tbody')[0].innerHTML='<?=$str?>';
				}
				catch(err)
				{
					location.reload();
				}
			}
			initMyApp();
		</script>
	</body>
</html>