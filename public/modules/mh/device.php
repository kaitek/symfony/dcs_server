<?php
    $id=$_GET['id'];
    $nodeport=$_GET['nodeport'];
	$node=$_SERVER['HTTP_HOST'].":".$nodeport;
	$name='('.$id.')';
    error_reporting(0);
	function get_web_page( $url )
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
			CURLOPT_SSL_VERIFYHOST => false
		);

		$headers = [
			'Cache-Control: no-cache',
			'Connection: keep-alive',
			'content-length: 0',
		];
		
		$ch      = curl_init( $url );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	$ret= get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/MaterialPrepare/DeviceData/".$id);
	if($ret['errno']!=0){
		var_dump($ret);
	}else{
		$j_ret=json_decode($ret['content']);
		$name='('.$id.')-'.$j_ret->name;
		/*echo("<pre>".date('d.m.Y H:i:s')."</pre>");
        if($j_ret->message!=='OK'){
            echo("<br>");
            echo("<pre>".$j_ret->message."</pre>");
        }*/
	}
?>
<!DOCTYPE html>
<html lang="tr">
    <head>
        <meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='cache-control' content='no-cache'>
		<title>Verimot</title>
		<link href="../main/inc/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="../../lib/Sentry/polyfill.min.js?features=Promise"></script>
		<script src="../../lib/Sentry/bundle.min.js" ></script>
    </head>
    <body style="-moz-user-select:none;-khtml-user-select: none;overflow-x: hidden;padding: 0px;margin: 0px;" unselectable="on">
        <?php if($j_ret->message!=='OK'){?>
            <div class="alert alert-success">
                <center><strong><?php echo('Cihaz No:'.$j_ret->deviceId."<br>".date('d.m.Y H:i:s'))?></strong></center>
            </div>
            <div class="alert alert-info">
            <center><strong><?php echo($j_ret->message)?></strong></center>
            </div>
        <?php }else{?>
        <div id='connection' unselectable='on'
			style='display:block;position:absolute;top:0;left:0;-moz-opacity: 0.8;opacity:.80;filter: alpha(opacity=80);width:100%;height:100%;background-color:#000;z-index:1000; font-size: 100px;'>
			<div unselectable='on' 
				style='display:block;width:1400px;height:220px;position:absolute;top:50%;left:50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);background-color:#000;z-index:1000;color:red;'>
				<center unselectable='on'>Bağlantı Kesilmiş</center>
			</div>
		</div>
		<div id='logout' unselectable='on'
			style='display:none;position:absolute;top:0;left:0;-moz-opacity: 0.8;opacity:.80;filter: alpha(opacity=80);width:100%;height:100%;background-color:#000;z-index:1000; font-size: 100px;'>
			<div unselectable='on' 
				style='display:block;width:1400px;height:220px;position:absolute;top:50%;left:50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);background-color:#000;z-index:1000;color:red;'>
				<center unselectable='on'>Oturum Açık Değil</center>
			</div>
		</div>
		<div id='msg' unselectable='on'
			style='display:none;position:absolute;top:0;left:0;-moz-opacity: 0.8;opacity:.80;filter: alpha(opacity=80);width:100%;height:100%;background-color:#000;z-index:900; font-size: 80px;'>
			<div unselectable='on' 
				style='display:block;width:1800px;height:420px;position:absolute;top:50%;left:50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);background-color:#000;z-index:1000;color:red;'>
				<center unselectable='on'></center>
			</div>
		</div>
		<table id='table' class='table' unselectable='on' style='font-size:30px;'>
			<thead unselectable='on'>
				<tr unselectable='on'>
					<th class='info' id='infoleft'><center unselectable='on'></center></th>
					<th colspan=3 class='info' id='info' unselectable='on'><center unselectable='on'><?=$name?></center></th>
					<th class='info' id='info2' unselectable='on'><center unselectable='on'></center></th>
					<th class='info' id='inforight'><center unselectable='on'></center></th>
				</tr>
				<tr unselectable='on'>
					<th unselectable='on'>Malzeme</th>
					<th unselectable='on'>İşlem Tipi</th>
					<th unselectable='on'>Çıkış</th>
					<th unselectable='on'>Varış</th>
					<th unselectable='on'>Çağrı</th>
					<th unselectable='on'>Kalan</th>
				</tr>
			</thead>
			<tbody unselectable='on'>
				
			</tbody>
		</table>
		<script src="../main/inc/jquery.min.js"></script>
		<script src="../main/inc/jquery.browser.min.js"></script>
		<script src="../main/inc/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		<script src="../main/inc/jquery.contextMenu.js" type="text/javascript"></script>
		<!--<link href="../main/inc/jquery.contextMenu.css" rel="stylesheet" type="text/css" />-->
		<style>
			.contextMenu {
				position: absolute;
				width: 250px;
				z-index: 99999;
				border: solid 1px #CCC;
				background: #EEE;
				padding: 0px;
				margin: 0px;
				display: none;
			}

			.contextMenu LI {
				list-style: none;
				padding: 0px;
				margin: 0px;
			}

			.contextMenu A {
				color: #333;
				text-decoration: none;
				display: block;
				line-height: 50px;
				height: 50px;
				background-position: 6px center;
				background-repeat: no-repeat;
				outline: none;
				padding: 1px 5px;
				padding-left: 28px;
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 20px;
				font-weight:bold;
			}

			.contextMenu LI.hover A {
				color: #FFF;
				background-color: #3399FF;
			}

			.contextMenu LI.disabled A {
				color: #AAA;
				cursor: default;
			}

			.contextMenu LI.hover.disabled A {
				background-color: transparent;
			}

			.contextMenu LI.separator {
				border-top: solid 1px #CCC;
			}
		</style>
		<script src="http://<?=$node?>/socket.io/socket.io.js"></script>
		<script type='text/javascript'>
			Sentry.init({
                dsn: 'http://11c0528d52054012bf608d6ccbf17021@sentry.kaitek.com.tr/9'
                //,integrations: [new MyAwesomeIntegration()]
			});
			Sentry.configureScope(function(scope){
				scope.setTag('ip','<?=$_SERVER['REMOTE_ADDR']?>');
				scope.setTag('name','<?=$name?>');
			});
			//Sentry.configureScope(scope => {
			//	//scope.setTag('ip',t.ip);
			//	scope.setTag('name','<?=$name?>');
			//	//scope.setTag('version',t.versionApp);
			//	//scope.setTag('build',t.buildNumber);
			//	//scope.setUser({ ip: t.cfg_dcs_server_IP });
			//});
			var initMyApp = function(){
				$(document).on({
					"contextmenu": function(e) {
						e.preventDefault();
						//alert("contextmenu");
					},
					"mousedown": function(e) { 
						
					},
					"mouseup": function(e) { 
						//alert("mouseup"+e.button);
					}
				});
				var d={};
				//window.d=d;
				d.clientIp='<?=$_SERVER['REMOTE_ADDR']?>';
				d.id='<?=$id?>';
				d.connection=$('#connection');
				d.logout=$('#logout');
				d.info=$('#info');
				d.info2=$('#info2');
				d.infoleft=$('#infoleft');
				d.inforight=$('#inforight');
				d.table=$('#table');
				d.msg=$('#msg');
				d.timerfn=false;
				d.timerfnmsg=false;
				d.timemsg=3;
				d.pingCounter=0;
				d.timerping=false;
				d._islemzamanasimi=1200;
				d.islemzamanasimi=d._islemzamanasimi;
				window.onbeforeunload = function(event){
					if(d && d.socket)
						d.socket.disconnect();
				};
				try{
					d.fnexternal=function(action){
						if(typeof window.external.fromjs!=='undefined'){
							try
							{
								if(action!=='dcs_beep_error'){
									if(d.timerping!==false){
										clearTimeout(d.timerping);
										d.timerping=false;
									}
									if(d.timerfn){
										clearTimeout(d.timerfn);
										d.timerfn=false;
									}
								}
								window.external.fromjs(action);
							}
							catch(err)
							{
								Sentry.captureException(err);
								if(action!=='dcs_beep_error'){
									if(d.timerping!==false){
										clearTimeout(d.timerping);
										d.timerping=false;
									}
									if(d.timerfn){
										clearTimeout(d.timerfn);
										d.timerfn=false;
									}
									alert(err);
									//location.reload();
								}
							}
						}else{
							//alert(3);
						}
					};
					d.fnrefresh=function(){
						d.fnexternal('cm_refresh');
					};
					d.temizle=function(){
						$.find('tbody')[0].innerHTML='';
						$('center',d.info)[0].innerHTML='<?=$name?>';
						d.logout.show();
						d.islemzamanasimi=-1;
						$('center',d.inforight)[0].innerHTML='';
					};
					
					d.socket=io.connect('<?=$node?>');
					
					d.socket.on('connecting', function () {
						//console.log('connecting');
						d.logout.hide();
						d.connection.show();
					});
					
					d.setinfo2=function(){
						$('center',d.info2)[0].innerHTML='P:'+d.pingCounter;
					};

					d.fnping=function(){
						d.pingCounter+=1;
						d.setinfo2();
						d.socket.emit('route',{'className':'forklift','methodName':'ping','id':'<?=$id?>'});
						d.timerping=setTimeout(d.fnpingcb,5000);
					};

					d.fnpingcb=function(){
						//d.fnrefresh0();
					};

					d.socket.on('forklift_ping',function(obj){
						if(d.timerping!==false){
							d.pingCounter-=1;
							d.setinfo2();
							clearTimeout(d.timerping);
							d.timerping=false;
						}
						setTimeout(d.fnping,5000);
					});
					
					d.socket.on('connect', function(s){
						//console.log('connect');
						d.socket.emit('setUser','<?=$id?>');
						d.connection.hide();
						d.logout.show();
						d.socket.emit('route',{'className':'forklift','methodName':'oturumkontrol','id':'<?=$id?>'});
						setTimeout(d.fnping,5000);
					});

					d.socket.on('connect_error', function(s){
						//console.log('connect');
						//Sentry.captureMessage({
						//	message: 'connect_error',
						//	data : JSON.parse(JSON.stringify({id:d.id,ip:d.clientIp,node:'<?=$node?>'})),ip:d.clientIp
						//});
						//d.fnrefresh0();
					});

					d.socket.on('connect_timeout', function(s){
						//console.log('connect');
						//Sentry.captureMessage({
						//	message: 'connect_timeout',
						//	data : JSON.parse(JSON.stringify({id:d.id,ip:d.clientIp,node:'<?=$node?>'})),ip:d.clientIp
						//});
						//d.fnrefresh0();
					});
					
					d.socket.on('reconnecting', function () {
						//console.log('reconnecting');
						d.logout.hide();
						d.connection.show();
					});
					
					d.socket.on('reconnect', function () {
						//console.log('reconnect');
						d.connection.hide();
						d.logout.show();
					});
					
					d.socket.on('reconnect_error', function () {
						//console.log('reconnect_failed');
						//Sentry.captureMessage({
						//	message: 'reconnect_error',
						//	data : JSON.parse(JSON.stringify({id:d.id,ip:d.clientIp,node:'<?=$node?>'})),ip:d.clientIp
						//});
						d.connection.show();
						//d.fnrefresh0();
					});

					d.socket.on('reconnect_failed', function () {
						//console.log('reconnect_failed');
						//Sentry.captureMessage({
						//	message: 'reconnect_failed',
						//	data : JSON.parse(JSON.stringify({id:d.id,ip:d.clientIp,node:'<?=$node?>'})),ip:d.clientIp
						//});
						d.connection.show();
						//d.fnrefresh0();
					});
					
					d.socket.on('disconnect', function () {
						//console.log('disconnect');
						//Sentry.captureMessage({
						//	message: 'disconnect',
						//	data : JSON.parse(JSON.stringify({id:d.id,ip:d.clientIp,node:'<?=$node?>'})),ip:d.clientIp
						//});
						$.find('tbody')[0].innerHTML='';
						d.logout.hide();
						d.connection.show();
						if(d.timerping!==false){
							clearTimeout(d.timerping);
							d.timerping=false;
						}
						//d.fnrefresh0();
					});
					
					d.fn1=function(){
						var el=$($.find('tbody')).find('tr');
						for(var i=0;i<el.length;i++){
							if(el[i].cells.length>0){
								var fsn=false;
								var html=el[i].cells[el[i].cells.length-1].innerHTML;
								var arrsn=html.split(':');
								//console.log(html);
								//console.dir(arrsn);
								if(arrsn.length==3){
									fsn=arrsn[0].indexOf('-')>-1;
									var sn=(fsn?-1:1)*((fsn?-1:1)*parseInt(arrsn[0])*3600+(parseInt(arrsn[1]))*60+(parseInt(arrsn[2])));
									//console.log(sn);
									//sn=sn>1?sn-1:0;
									sn=sn-1;
									if(/*sn>=0&&*/sn<180){
										if($(el[i]).hasClass('danger')==false) $(el[i]).addClass('danger');
										if($(el[i]).hasClass('warning')==true) $(el[i]).removeClass('warning');
									}
									if(sn>=180&&sn<300){
										if($(el[i]).hasClass('warning')==false) $(el[i]).addClass('warning');
									}
									if(sn<0){
										sn=sn*-1;
										fsn=true;
									}
									var h=(sn>=3600?Math.floor(sn / 3600):0);
									var m=(sn>=60?Math.floor((sn - (h * 3600)) / 60):0);
									var s=sn - (h * 3600) - (m * 60);
									var snhtml=('00'+h).substr(-2)+':'+('00'+m).substr(-2)+':'+('00'+s).substr(-2);
									//console.log((fsn?'-':'')+snhtml);
									el[i].cells[el[i].cells.length-1].innerHTML=(fsn?'-':'')+snhtml;
								}
								
							}
						}
						if(d.islemzamanasimi>0){
							d.islemzamanasimi=d.islemzamanasimi-1;
							$('center',d.inforight)[0].innerHTML=('00'+Math.floor(d.islemzamanasimi/60)).substr(-2)+':'+('00'+d.islemzamanasimi%60).substr(-2);
							if(d.islemzamanasimi==0){
								d.temizle();
								d.socket.emit('route',{'className':'forklift','methodName':'islemzamanasimi','id':'<?=$id?>'});
							}
						}else{
							$('center',d.inforight)[0].innerHTML='';
						}
						d.timerfn = setTimeout(d.fn1,1000);
					};
					
					d.socket.on('forklift_oturumkontrol',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_oturumkontrol',
						//	data : JSON.parse(JSON.stringify(data)),
						//	id:d.id,
						//	ip:d.clientIp
						//});
						//console.log('forklift_oturumkontrol');
						if(data.status==='ok'){
							$('center',d.info)[0].innerHTML='<?=$name?>';
							if(data.message=='oturumvar'){
								d.logout.hide();
								$('center',d.info)[0].innerHTML='<?=$name?> '+data.sicil+' - '+data.isim.toUpperCase();
								d.socket.emit('route',{'className':'forklift','methodName':'tasimagorevilistele','id':'<?=$id?>'});
							}
						}
					});
					
					d.socket.on('forklift_oturumacildi',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_oturumacildi',
						//	data : JSON.parse(JSON.stringify(data)),
						//	id:d.id,ip:d.clientIp
						//});
						//console.log('dcs_oturumacildi');
						d.logout.hide();
						d.islemzamanasimi=d._islemzamanasimi;
						d.socket.emit('route',{'className':'forklift','methodName':'oturumkontrol','id':'<?=$id?>'});
						//oturum açıldığında ekranda mesaj yazılması kontrol edilecek
						var msg=decodeURI(data.message);
						if(typeof msg!=='undefined'){
							(d.msg.find("center"))[0].innerHTML="Bilgi<br>"+msg;
							d.timemsg=2;
							d.msg.show();
							d.fnexternal("dcs_beep_error");
							if(d.timerfnmsg){
								clearTimeout(d.timerfnmsg);
								d.timerfnmsg=false;
							}
							d.timerfnmsg = setTimeout(d.fn2,1);
						}
					});
					
					d.socket.on('forklift_oturumkapandi',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_oturumkapandi',
						//	data : JSON.parse(JSON.stringify(data)),
						//	id:d.id,ip:d.clientIp
						//});
						//console.log('dcs_oturumkapandi');
						d.temizle();
						d.fnexternal("dcs_beep_error");
					});
					
					d.socket.on('forklift_islemzamanasimi',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_islemzamanasimi',
						//	data : JSON.parse(JSON.stringify(data)),
						//	id:d.id,ip:d.clientIp
						//});
						//console.log('dcs_islemzamanasimi');
						d.socket.emit('route',{'className':'forklift','methodName':'oturumkapandi','id':'<?=$id?>'});
					});
					
					d.socket.on('forklift_tasimagoreviacildi',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_tasimagoreviacildi',
						//	data : JSON.parse(JSON.stringify({data:data,id:d.id,node:'<?=$node?>'})),ip:d.clientIp
						//});
						//console.log('dcs_tasimagoreviacildi');
						if(d.islemzamanasimi==-1)d.islemzamanasimi=d._islemzamanasimi;
						//console.dir({'className':'forklift','methodName':'tasimagorevilistele','id':'<?=$id?>'});
						d.socket.emit('route',{'className':'forklift','methodName':'tasimagorevilistele','id':'<?=$id?>'});
					});
					
					d.socket.on('forklift_tasimagorevikapandi',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_tasimagorevikapandi',
						//	data : JSON.parse(JSON.stringify({data:data,id:d.id,node:'<?=$node?>'})),ip:d.clientIp
						//});
						//console.log('dcs_tasimagorevikapandi');
						d.islemzamanasimi=d._islemzamanasimi;
						d.socket.emit('route',{'className':'forklift','methodName':'tasimagorevilistele','id':'<?=$id?>'});
					});
					
					d.fn2=function(){
						if(d.timemsg>0){
							d.timemsg=d.timemsg-1;
							d.timerfnmsg = setTimeout(d.fn2,1000);
						}else{
							clearTimeout(d.timerfnmsg);
							d.timerfnmsg=false;
							if(d.flag_islogout_display){
								d.logout.css('display','block');
							}
							d.msg.hide();
							
						}
					};
					
					d.socket.on('forklift_gorevkapathata',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_gorevkapathata',
						//	data : JSON.parse(JSON.stringify({data:data,id:d.id,node:'<?=$node?>'})),ip:d.clientIp
						//});
						//console.log('forklift_gorevkapathata');
						var msg=decodeURI(data.message);
						//console.log(msg);
						(d.msg.find("center"))[0].innerHTML="Hata Oluştu<br>"+msg;
						d.timemsg=3;
						d.msg.show();
						if(d.timerfnmsg){
							clearTimeout(d.timerfnmsg);
							d.timerfnmsg=false;
						}
						d.timerfnmsg = setTimeout(d.fn2,1);
					});
					
					d.socket.on('forklift_hata',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_hata',
						//	data : JSON.parse(JSON.stringify({data:data,id:d.id,node:'<?=$node?>'})),ip:d.clientIp
						//});
						//console.log('forklift_gorevkapathata');
						d.flag_islogout_display=d.logout.css('display')=='block';
						var msg=decodeURI(data.message);
						//console.log(msg);
						//alert(msg);
						(d.msg.find("center"))[0].innerHTML="Hata Oluştu<br>"+msg;
						d.timemsg=5;
						if(d.flag_islogout_display){
							d.logout.css('display','none');
						}
						d.msg.show();
						d.fnexternal("dcs_beep_error");
						if(d.timerfnmsg){
							clearTimeout(d.timerfnmsg);
							d.timerfnmsg=false;
						}
						d.timerfnmsg = setTimeout(d.fn2,1);
					});
					
					d.socket.on('forklift_tasimagorevilistele',function(data){
						//Sentry.captureMessage({
						//	message: 'forklift_tasimagorevilistele',
						//	data : JSON.parse(JSON.stringify({data:data,id:d.id,node:'<?=$node?>'})),ip:d.clientIp
						//});
						if(d.timerfn){
							clearTimeout(d.timerfn);
							d.timerfn=false;
						}
						var str='';
						if(data.length>0){
							var _d = data.records;
							if(_d.length==0){
								d.islemzamanasimi=-1;
								$('center',d.inforight)[0].innerHTML='';
							}
							for(var i=0;i<_d.length;i++){
								//console.dir(_d[i]);
								//str=str+'<tr unselectable="on"><td unselectable="on"><strong unselectable="on">'+_d[i]['bilesen']+'</strong><br/><kbd unselectable="on">'+_d[i]['kasa']+'</kbd></td><td unselectable="on">'+_d[i]['islemtip']+'</td><td unselectable="on">'+_d[i]['lokasyoncikis']+'</td><td unselectable="on">'+_d[i]['lokasyonvaris']+'</td><td unselectable="on">'+_d[i]['cagrizamani']+'</td><td unselectable="on">'+_d[i]['kalansure']+'</td></tr>';
								str=str+'<tr unselectable="on"><td unselectable="on"><strong unselectable="on">'+_d[i]['bilesen2']+'</strong></td><td unselectable="on">'+_d[i]['islemtip']+'</td><td unselectable="on">'+_d[i]['locationsourceinfo']+'</td><td unselectable="on"><strong unselectable="on">'+(_d[i]['locationdestinationinfo']!==null?_d[i]['locationdestinationinfo']:_d[i]['locationdestination'])+'</strong></td><td unselectable="on">'+_d[i]['cagrizamani']+'</td><td unselectable="on">'+_d[i]['kalansure']+'</td></tr>';
								//console.log(_d[i]['kalansure']);
							}
							d.timerfn = setTimeout(d.fn1,1);
						}else{
							d.islemzamanasimi=-1;
							$('center',d.inforight)[0].innerHTML='';
						}
						$.find('tbody')[0].innerHTML=str;
					});
					
					d.socket.on('dcs_pagerefresh',function(data){
						d.fnrefresh();
					});
					
					d.socket.on('dcs_changestation',function(data){
						d.fnexternal("dcs_changestation");
					});

					d.socket.on('error',function(error){
						//Sentry.captureMessage({
						//	message: 'forklift_error',
						//	data : JSON.parse(JSON.stringify({error:error,id:d.id,node:'<?=$node?>'})),ip:d.clientIp
						//});
						//d.fnrefresh();
					});
				}
				catch(err)
				{
					if(d.timerfn){
						clearTimeout(d.timerfn);
						d.timerfn=false;
					}
					Sentry.captureException(err);
					alert(err);
					//location.reload();
				}
				
				function netCall(type,data){	
					//alert(data);
					switch (type){
						case 'disconnect':
							if(d && d.socket){
								d.socket.disconnect();
							}
							d.fnexternal("cb_disconnect");
							break;
						case 'msg':
							switch (data){
								case 'disconnect':
									if(d && d.socket){
										d.socket.disconnect();
									}
									d.fnexternal("cb_disconnect");
									break;
							}
							break;
						case 'pildurumu':
							var _d=data.split('|');
							if(_d.length==2){
								$('center',d.infoleft)[0].innerHTML="<font color='"+(_d[0]==1?'green':'red')+"'>Pil : %"+_d[1]+"</font>";
							}
							break;
					}
					
				};
				d.fnexternal("cb_pageload");
						
				$("body").contextMenu({
					menu: 'myMenu'
				},
					function(action, el, pos) {
						d.fnexternal(action);
					/*alert(
						'Action: ' + action + '\n\n' +
						'Element ID: ' + $(el).attr('id') + '\n\n' + 
						'X: ' + pos.x + '  Y: ' + pos.y + ' (relative to element)\n\n' + 
						'X: ' + pos.docX + '  Y: ' + pos.docY+ ' (relative to document)'
						);*/
				});
			}
			//Sentry.context(function () {
			initMyApp();
			//});	
			
		</script>
		<ul id="myMenu" class="contextMenu">
			<!--<li class="sys"><a href="#cm_sys">Sistem Ayarları</a></li>-->
			<li class="refresh separator"><a href="#cm_refresh">Yenile</a></li>
		</ul>
        <?php }?>
        <script type='text/javascript'>
            <?php if($j_ret->message!=='OK'){?>
                setTimeout(function(){
                    location.reload();
                }, 1000);
            <?php }?>
        </script>
    </body>
</html>