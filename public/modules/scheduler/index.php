<!-- https://192.168.1.40/kaynak3/modules/scheduler/index.php?adamsayi=4&gunsayi=4&periyot=30&dbg=1 -->
<?php
    require_once("../debug.php");
$adamsayi=isset($_GET['adamsayi']) ? $_GET['adamsayi'] : 5;
$gunsayi=isset($_GET['gunsayi']) ? $_GET['gunsayi'] : 5;
$periyot=isset($_GET['periyot']) ? $_GET['periyot'] : 10;
$dbg=isset($_GET['dbg']) ? $_GET['dbg'] : 0;
$url="https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/Scheduler/getTasksServer/$periyot/$gunsayi/$adamsayi";
?>
<!DOCTYPE html>
<html lang="tr">
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='cache-control' content='no-cache'>
		<title>Verimot</title>
		<link rel="stylesheet" type="text/css" href="../../lib/myjui/mj.css"/>
		<script src="../../lib/myjui/mj-debug.js?i=3"></script>
		<script src="../../lib/myjui/myjui/language/tr.js"></script>
		<style>
			.offtime {
				background: #ffc107;
				border:1px solid;
			}
			.day {
				white-space: pre;
			}
			.work {
				background: #8bc34a;
				border:1px solid;
				border-radius: 5px;
				white-space: pre;
			}
			.free {
				border: none;
			}
			.border {
				border:1px solid;
			}
			.hide {
				display:none;
			}
			.selected {
				background: yellow;
			}
			#loading-indicator {
				position: fixed;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background: url('../../lib/ajax.gif') center center no-repeat rgb(27,27,27);
				z-index: 999999;
				display: none;
			}
		</style>
    </head>
    <body style="-moz-user-select:none;-khtml-user-select: none;overflow: hidden;padding: 0px;margin: 0px;" unselectable="on">
		<div id="loading-indicator"></div>
		<div id="container" style="width: 100%;height: 100%;overflow: auto;">
			<table id="tbl_main" style="border:none;font-family: verdana;font-size: 10px;z-index:0;">
				<thead>
					<tr>
						<td rowspan="3" style="border:1px solid;text-align:center;">İstasyon</td>
						<td rowspan="3" style="border:1px solid;text-align:center;">Ekipman</td>
					</tr>
					<tr>
						
					</tr>
					<tr>
						
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
			<table id="tbl_top_left" style="border:none;font-family: verdana;font-size: 10px;position:absolute;top:0px;left:0px;z-index:100;display:none;background: white;">
				<thead>
					<tr>
						<td id="station" rowspan="3" style="border:1px solid;text-align:center;color: black;height:48px;">İstasyon</td>
						<td id="equipment" rowspan="3" style="border:1px solid;text-align:center;color: black;height:48px;">Ekipman</td>
					</tr>
					<tr>
						
					</tr>
					<tr>
						
					</tr>
				</thead>				
			</table>
			<table id="tbl_main_rows_shadow" style="border:none;font-family: verdana;font-size: 10px;position:absolute;top:54px;left:0px;z-index:10;display:none;background: white;">
				<tbody>
				
				</tbody>
			</table>
			<table id="tbl_main_colums_shadow" style="border:none;font-family: verdana;font-size: 10px;position:absolute;top:0px;left:0px;z-index:10;display:none;background: white;">
				<thead>
					<tr>
						
					</tr>
					<tr>
						
					</tr>
					<tr>
						
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
		<script type='text/javascript'>
			$(function() {
				let _dbg_=parseInt('<?=$dbg?>');
				let thead_tr=$("#tbl_main").find("thead").find("tr");
				let thead_tr_days=$(thead_tr[0]);
				let thead_tr_hours=$(thead_tr[1]);
				let thead_tr_opcount=$(thead_tr[2]);
				let thead_tr_shadow=$("#tbl_main_colums_shadow").find("thead").find("tr");
				let thead_tr_days_shadow=$(thead_tr_shadow[0]);
				let thead_tr_hours_shadow=$(thead_tr_shadow[1]);
				let thead_tr_opcount_shadow=$(thead_tr_shadow[2]);
				let tbody=$("#tbl_main").find("tbody");
				let tbody_shadow=$("#tbl_main_rows_shadow").find("tbody");
				const wait = ms => new Promise((resolve) => setTimeout(resolve, ms));
				async function render_clients(cd) {
					//console.log('render_clients');
					for(let j=0;j<cd.length;j++){
						let row=cd[j];
						let cnt_client=tbody.find("tr:last");
						let str_client=row.client.split('.').join('');
						let str_code='';

						let arr_codes=row.codes.split('|');
						str_code=arr_codes[0].split('.').join('');
						tbody.append('<tr id="tb_tr_clients_'+str_client+'_'+str_code+'" class="clients client_e_'+str_client+' client_'+str_client+'_'+str_code+'">'+
							'<td id="tb_td_clients_'+str_client+'" class="client td-fixed" style="border:1px solid;" rowspan="'+(row.ekipmansayisi)+'">'+row.client+'</td>'+
							'<td id="tb_td_clients_'+str_code+'" class="clientdetail td-fixed" style="border:1px solid;">'+arr_codes[0]+'</td>'+
							'</tr>');
						tbody_shadow.append('<tr id="tb_tr_clients_'+str_client+'_'+str_code+'" class="clients client_e_'+str_client+' client_'+str_client+'_'+str_code+'">'+
							'<td id="tb_td_clients_'+str_client+'" class="client " style="border:1px solid;" rowspan="'+(row.ekipmansayisi)+'">'+row.client+'</td>'+
							'<td id="tb_td_clients_'+str_code+'" class="clientdetail " style="border:1px solid;">'+arr_codes[0]+'</td>'+
							'</tr>');
						for(let i=1;i<arr_codes.length;i++){
							let item=arr_codes[i];
							str_code=item.split('.').join('');
							tbody.append('<tr id="tb_tr_clients_'+str_client+'_'+str_code+'" class="clients client_e_'+str_client+' client_'+str_client+'_'+str_code+'">'+
							'<td id="tb_td_clients_'+str_code+'" class="clientdetail td-fixed" style="border:1px solid;">'+item+'</td>'+
							'</tr>');
							tbody_shadow.append('<tr id="tb_tr_clients_'+str_client+'_'+str_code+'" class="clients client_e_'+str_client+' client_'+str_client+'_'+str_code+'">'+
							'<td id="tb_td_clients_'+str_code+'" class="clientdetail" style="border:1px solid;">'+item+'</td>'+
							'</tr>');
						}
					}
				}
				async function render_timeline(obj) {
					let x=new Date(obj.firsttime);
					for(let i=0;i<(obj.dayscount*24*60/obj.period);i++){
						let str_day=x.toLocaleDateString().split('/').reverse().join('').split('.').reverse().join('');
						let str_day_short=x.toLocaleDateString().replace('/20','.');
						let str_time5=x.toLocaleTimeString().substr(0,5);
						let str_time4=str_time5.replace(':','');
						thead_tr_hours.append('<td id="th_tr_hours_'+str_day+str_time4+'" class="hour tr-fixed" style="border:1px solid;">'+x.toLocaleTimeString().substr(0,5)+'</td>');
						thead_tr_hours_shadow.append('<td id="th_tr_hours_'+str_day+str_time4+'" class="hour" style="border:1px solid;">'+x.toLocaleTimeString().substr(0,5)+'</td>');
						thead_tr_opcount.append('<td id="th_tr_opcount_'+str_day+str_time4+'" class="opcount tr-fixed" style="border:1px solid;text-align: right;">0</td>');
						thead_tr_opcount_shadow.append('<td id="th_tr_opcount_'+str_day+str_time4+'" class="opcount" style="border:1px solid;text-align: right;">0</td>');
						let _day=thead_tr_days.find('.day:last');
						let _day_shadow=thead_tr_days_shadow.find('.day:last');
						if(_day.length===0||(_day.length>0&&_day.text()!==str_day_short)){
							thead_tr_days.append('<td id="th_tr_days_'+str_day+'" class="day tr-fixed" style="border:1px solid;">'+str_day_short+'</td>');
							thead_tr_days_shadow.append('<td id="th_tr_days_'+str_day+'" class="day" style="border:1px solid;">'+str_day_short+'</td>');
						}else{
							if(_day.text()==str_day_short){
								_day[0].colSpan++;
								_day_shadow[0].colSpan++;
							}
						}
						x.setMilliseconds(x.getMilliseconds()+(1000* ((obj.period/60)*3600) ));
					}
					if(_dbg_==1){
						$('.free').addClass('border');
						//console.info('Toplam süre: '+sure+' (ms) Dizilim Süresi: '+d.time_render_tasks+' (ms) Toplam İş Sayısı: '+d.data.tasks.length+' Yerleştirilen İş Sayısı: '+(task_counter>d.data.tasks.length?d.data.tasks.length:task_counter));
						//console.log(d);
					}
				}
				const render_boxes_client = async function(obj,idx) {
					let x=new Date(obj.firsttime);
					for(let i=0;i<(obj.dayscount*24*60/obj.period);i++){
						let str_day=x.toLocaleDateString().split('/').reverse().join('').split('.').reverse().join('');
						let str_day_short=x.toLocaleDateString().replace('/20','.');
						let str_time5=x.toLocaleTimeString().substr(0,5);
						let str_time4=str_time5.replace(':','');
						let row=d.client_details[idx];
						let str_client=row.client.split('.').join('');
						let _cls='free';
						let _title='';
						for(l=0;l<obj.offtimes.length;l++){
							let row_offtime=obj.offtimes[l];
							if(row_offtime.clients.indexOf(row.client)>-1
							&&((row_offtime.repeattype!=='day'&&row_offtime.startday.split('-').join('')==str_day) || (row_offtime.repeattype==='day'&&row_offtime.days.indexOf(d.days[x.getDay()])>-1) )
							){
								let vi_start=parseInt(row_offtime.starttime.split(':').join(''));
								let vi_finish=parseInt(row_offtime.finishtime.split(':').join(''));
								if(parseInt(str_time4)>=vi_start && parseInt(str_time4)<vi_finish){
									_cls='offtime';
									_title='title="'+row_offtime.losttype+' '+row_offtime.starttime+'-'+row_offtime.finishtime+'"';
								}
							}
						}
						let arr_codes=row.codes.split('|');
						for(let k=0;k<arr_codes.length;k++){
							let item=arr_codes[k];
							str_code=item.split('.').join('');
							$('#tb_tr_clients_'+str_client+'_'+str_code).append('<td id="tb_tr_clients_hours_'+str_day+str_time4+'" class="client_equipment_hour client_hour_'+str_client+'_'+str_day+str_time4+' '+_cls+'" '+_title+' colspan=1 ></td>');
						}
						x.setMilliseconds(x.getMilliseconds()+(1000* ((obj.period/60)*3600) ));
					}
					await wait(0);
					return idx;
				}
				async function render_boxes_2(obj,idx) {
					const fragment = await render_boxes_client(obj,idx);
					if(idx<d.client_details.length-1){
						return await render_boxes_2(obj,idx+1);
					}else{
						if(_dbg_==1){
							$('.free').addClass('border');
							//console.info('Toplam süre: '+sure+' (ms) Dizilim Süresi: '+d.time_render_tasks+' (ms) Toplam İş Sayısı: '+d.data.tasks.length+' Yerleştirilen İş Sayısı: '+(task_counter>d.data.tasks.length?d.data.tasks.length:task_counter));
							//console.log(d);
						}
						return idx;
					}
				}
				const render_task = async function(data,idx){
					let obj=data[idx];
					if(obj.event==='equipment_render_add'){
						let boxes_equipment=$('.'+obj.rowcls).find('td.client_equipment_hour');
						if(boxes_equipment.length>0){
							for(let i=0;i<boxes_equipment.length;i++){
								let el_rem_idx=$(boxes_equipment[i]);
								if(el_rem_idx.hasClass(obj.boxcls)){
									el_rem_idx.text(obj.title);
									el_rem_idx.attr('title',obj.title);
									el_rem_idx.addClass('work');
									el_rem_idx.addClass(obj.erprefnumber);
									el_rem_idx.removeClass('free');
									$(boxes_equipment[i])[0].colSpan=obj.colspan;
									for(let j=1;j<obj.colspan;j++){
										$(boxes_equipment[i+j]).remove();
									}
									break;
								}
							}
						}
					}
					if(obj.event==='equipment_render_edit'){
						let boxes_equipment_edit=$('.'+obj.rowcls).find('td.'+obj.boxcls);
						boxes_equipment_edit.text(obj.title.split('new_line').join('\n'));
						boxes_equipment_edit.attr('title',obj.title.split('new_line').join('\n'));
						boxes_equipment_edit.addClass(obj.erprefnumber);
					}
					await wait(0);
					return idx;
				}
				async function render_tasks(data,idx){
					const fragment = await render_task(data,idx);
					if(idx<data.length-1){
						return await render_tasks(data,idx+1);
					}else{
						return idx;
					}
				}
				async function render_opcount(data){
					Object.keys(data).forEach(function(key_op) {
						let _item_op=data[key_op];
						$('#th_tr_opcount_'+_item_op.id,$("#tbl_main")).text(_item_op.title);
						$('#th_tr_opcount_'+_item_op.id,$("#tbl_main_colums_shadow")).text(_item_op.title);
					});
				}
				let store = new mj.store({
					method: 'POST'
					,url : '<?=$url?>'
					,params : {}
				});
				let d={
					days : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
					,client_details:[]
					,timeline:[]
					,boxes:[]
				};
				window.d=d;
				$('#loading-indicator').show();
				store.on('load', function(){
					let t=this;
					t._x=0;
					t._y=54;
					$('#loading-indicator').hide();
					let _data=this.data;
					d.client_details=_data.client_details;
					render_clients(d.client_details)
					.then(()=>{
						if (_data.firsttime) {
							return render_timeline(_data);
						}else{
							alert("Görev bulunamadı, dizilim yapılamıyor");
							return;
						}
					}).then(()=>{
						if (_data.firsttime) {
							return render_boxes_2(_data,0);
						}
						return null;
					}).then(()=>{
						if(_data.tasks.length>0){
							return render_tasks(_data.tasks,0);
						}
						return null;
					}).then(()=>{
						if(_data.tasks.length>0){
							return render_opcount(_data.render_opcount);
						}
						return null;
					}).then(()=>{
						if(_data.tasks.length>0){
							$('#tbl_main tr td.work').click(function(){
								//var cid = $(this).attr('id');
								//alert(cid);
								//var cval = $(this).text();
								//alert(cval);
								$('#tbl_main tr td.selected').removeClass('selected');
								let arr_cl=$(this)[0].classList;
								for(let j=(_dbg_==1?4:3);j<arr_cl.length;j++){
									$('#tbl_main tr td.'+arr_cl[j]).addClass('selected');
								}
							});
							let elements_fixed_rows=$('.td-fixed');
							if(elements_fixed_rows.length>0){
								let _x=0;
								for(let i=0;i<elements_fixed_rows.length;i++){
									let item=$(elements_fixed_rows[i]);
									if(i===0){
										_x+=item.width();
										$('#station',$("#tbl_top_left")).css({ "min-width": item.width()+"px" });
									}
									if(i===1){
										_x+=item.width();
										$('#equipment',$("#tbl_top_left")).css({ "min-width": item.width()+"px" });
									}
									$('#'+item[0].id,$("#tbl_main_rows_shadow")).height(item.height());
								}
								t._x=_x+12;
								$("#tbl_main_colums_shadow").css("left", _x+12);
							}
							let elements_fixed_columns=$('.tr-fixed');
							if(elements_fixed_columns.length>0){
								let _x=0;
								for(let i=0;i<elements_fixed_columns.length;i++){
									let item=$(elements_fixed_columns[i]);
									$('#'+item[0].id,$("#tbl_main_colums_shadow")).css({ "min-width": item.width()+"px" });
								}
							}

							var position_x = $('#container').scrollLeft(); 
							var position_y = $('#container').scrollTop(); 
							$('#container').scroll(function() {
								var scroll_x = $('#container').scrollLeft();
								var scroll_y = $('#container').scrollTop();
								if(scroll_x===position_x){
									$("#tbl_main_rows_shadow").css("top", t._y-scroll_y);
									position_y = scroll_y;
								}else{
									if(scroll_y===position_y){
										$("#tbl_main_colums_shadow").css("left", t._x-scroll_x);
										position_x = scroll_x;
									}
								}
								
							});
							$("#tbl_top_left").show();
							$("#tbl_main_rows_shadow").show();
							$("#tbl_main_colums_shadow").show();
							if(_dbg_==1){
								console.info('Dizilim Süresi: '+_data.duration+' (sn) ');
								console.log(_data);
							}else{
								alert('Dizilim Süresi: '+_data.duration+' (sn) ');
							}
						}
					});
				});
				store.load();
			});	
				
		</script>
    </body>
</html>