<?php
	//https://192.168.1.40/kaynak3/modules/scheduler/index-stream.php?adamsayi=4&gunsayi=4&periyot=30&dbg=1
	require_once("../debug.php");
	$adamsayi=isset($_GET['adamsayi'])?$_GET['adamsayi']:5;
	$gunsayi=isset($_GET['gunsayi'])?$_GET['gunsayi']:5;
	$periyot=isset($_GET['periyot'])?$_GET['periyot']:10;
	$dbg=isset($_GET['dbg'])?$_GET['dbg']:0;
	$url="https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/Scheduler/getTasksStream/$periyot/$gunsayi/$adamsayi";
?>
<!DOCTYPE html>
<html lang="tr">
    <head>
        <meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='cache-control' content='no-cache'>
		<title>Verimot</title>
		<link rel="stylesheet" type="text/css" href="../../lib/myjui/mj.css"/>
		<script src="../../lib/myjui/mj-debug.js?i=3"></script>
		<script src="../../lib/myjui/myjui/language/tr.js"></script>
		<style>
			.opcountover {
				background: red;
			}
			.offtime {
				background: #ffc107;
				border:1px solid;
			}
			.day {
				white-space: pre;
			}
			.work {
				background: #8bc34a;
				border:1px solid;
				white-space: pre;
			}
			.free {
				border: none;
			}
			.border {
				border:1px solid;
			}
			.hide {
				display:none;
			}
		</style>
    </head>
    <body style="-moz-user-select:none;-khtml-user-select: none;overflow-x: hidden;padding: 0px;margin: 0px;" unselectable="on">
		<div style="width: 100%;height: 100%;overflow: auto;">
			<table id="tbl_main" style="border:none;font-family: verdana;font-size: 10px;">
				<thead>
					<tr>
						<td rowspan="3" style="border:1px solid;text-align:center;">İstasyon</td>
						<td rowspan="3" style="border:1px solid;text-align:center;">Ekipman</td>
					</tr>
					<tr>
						
					</tr>
					<tr>
						
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
		<script type='text/javascript'>

			$(function() {
				let _dbg_=parseInt('<?=$dbg?>');
				let thead_tr=$("#tbl_main").find("thead").find("tr");
				let thead_tr_days=$(thead_tr[0]);
				let thead_tr_hours=$(thead_tr[1]);
				let thead_tr_opcount=$(thead_tr[2]);
				let tbody=$("#tbl_main").find("tbody");
				async function render_clients(cd) {
					//console.log('render_clients');
					for(let j=0;j<cd.length;j++){
						let row=cd[j];
						let cnt_client=tbody.find("tr:last");
						let str_client=row.client.split('.').join('');
						let str_code='';

						let arr_codes=row.codes.split('|');
						str_code=arr_codes[0].split('.').join('');
						tbody.append('<tr id="tb_tr_clients_'+str_client+'_'+str_code+'" class="clients client_e_'+str_client+' client_'+str_client+'_'+str_code+'">'+
							'<td id="tb_td_clients_'+str_client+'" class="client" style="border:1px solid;" rowspan="'+(row.ekipmansayisi)+'">'+row.client+'</td>'+
							'<td id="tb_td_clients_'+str_code+'" class="clientdetail" style="border:1px solid;">'+arr_codes[0]+'</td>'+
							'</tr>');
						for(let i=1;i<arr_codes.length;i++){
							let item=arr_codes[i];
							str_code=item.split('.').join('');
							tbody.append('<tr id="tb_tr_clients_'+str_client+'_'+str_code+'" class="clients client_e_'+str_client+' client_'+str_client+'_'+str_code+'">'+
							'<td id="tb_td_clients_'+str_code+'" class="clientdetail" style="border:1px solid;">'+item+'</td>'+
							'</tr>');
						}
					}
				}
				async function render_timeline(tl) {
					//console.log('render_timeline');
					Object.keys(tl.days).forEach(function(key_d) {
						let _item_day=tl.days[key_d];
						thead_tr_days.append('<td id="th_tr_days_'+_item_day.id+'" class="day" style="border:1px solid;" colspan="'+_item_day.cs+'">'+_item_day.title+'</td>');
					});
					Object.keys(tl.hours).forEach(function(key_h) {
						let _item_hour=tl.hours[key_h];
						thead_tr_hours.append('<td id="th_tr_hours_'+_item_hour.id+'" class="hour" style="border:1px solid;">'+_item_hour.title+'</td>');
					});
					Object.keys(tl.opcount).forEach(function(key_o) {
						let _item_opcount=tl.opcount[key_o];
						thead_tr_opcount.append('<td id="th_tr_opcount_'+_item_opcount.id+'" class="opcount" style="border:1px solid;text-align: right;">'+_item_opcount.title+'</td>');
					});
				}
				async function render_boxes(boxes) {
					//console.log('render_boxes');
					Object.keys(boxes).forEach(function(key_b) {
						let _item_boxes=boxes[key_b];
						let arr_equipments=_item_boxes.equipments.split('|');
						for(let j=0;j<arr_equipments.length;j++){
							let _item_equipment=arr_equipments[j];
							for(let i=0;i<_item_boxes._timeline.length;i++){
								let _box_item=_item_boxes._timeline[i];
								$('#tb_tr_clients_'+_item_boxes.client+'_'+_item_equipment).append('<td id="tb_tr_clients_hours_'+_box_item.id+'" class="client_equipment_hour client_hour_'+_box_item.cls+'" '+_box_item.title+' colspan=1 ></td>');
							}
						}
						//console.log(_item_boxes);
					});
					if(_dbg_==1){
						$('.free').addClass('border');
						//console.info('Toplam süre: '+sure+' (ms) Dizilim Süresi: '+d.time_render_tasks+' (ms) Toplam İş Sayısı: '+d.data.tasks.length+' Yerleştirilen İş Sayısı: '+(task_counter>d.data.tasks.length?d.data.tasks.length:task_counter));
						//console.log(d);
					}
				}
				async function render_boxes_2(obj) {
					let x=new Date(obj.firsttime);
					for(let i=0;i<(obj.dayscount*24*60/obj.period);i++){
						let str_day=x.toLocaleDateString().split('/').reverse().join('');
						let str_day_short=x.toLocaleDateString().replace('.20','.');
						let str_time5=x.toLocaleTimeString().substr(0,5);
						let str_time4=str_time5.replace(':','');
						thead_tr_hours.append('<td id="th_tr_hours_'+str_day+str_time4+'" class="hour" style="border:1px solid;">'+x.toLocaleTimeString().substr(0,5)+'</td>');
						thead_tr_opcount.append('<td id="th_tr_opcount_'+str_day+str_time4+'" class="opcount" style="border:1px solid;text-align: right;">0</td>');
						let _day=thead_tr_days.find('.day:last');
						if(_day.length===0||(_day.length>0&&_day.text()!==str_day_short)){
							thead_tr_days.append('<td id="th_tr_days_'+str_day+'" class="day" style="border:1px solid;">'+str_day_short+'</td>');
						}else{
							if(_day.text()==str_day_short){
								_day[0].colSpan++;
							}
						}
						for(let j=0;j<d.client_details.length;j++){
							let row=d.client_details[j];
							let str_client=row.client.split('.').join('');
							let _cls='free';
							let _title='';
							for(l=0;l<obj.offtimes.length;l++){
								let row_offtime=obj.offtimes[l];
								if(row_offtime.clients.indexOf(row.client)>-1
								&&((row_offtime.repeattype!=='day') || (row_offtime.repeattype==='day'&&row_offtime.days.indexOf(d.days[x.getDay()])>-1) )
								){
									let vi_start=parseInt(row_offtime.starttime.split(':').join(''));
									let vi_finish=parseInt(row_offtime.finishtime.split(':').join(''));
									if(parseInt(str_time4)>=vi_start && parseInt(str_time4)<vi_finish){
										_cls='offtime';
										_title='title="'+row_offtime.losttype+' '+row_offtime.starttime+'-'+row_offtime.finishtime+'"';
									}
								}
							}
							let arr_codes=row.codes.split('|');
							for(let k=0;k<arr_codes.length;k++){
								let item=arr_codes[k];
								str_code=item.split('.').join('');
								$('#tb_tr_clients_'+str_client+'_'+str_code).append('<td id="tb_tr_clients_hours_'+str_day+str_time4+'" class="client_equipment_hour client_hour_'+str_client+'_'+str_day+str_time4+' '+_cls+'" style="" '+_title+' colspan=1 ></td>');
							}
						}
						x.setMilliseconds(x.getMilliseconds()+(1000* ((obj.period/60)*3600) ));
					}
					if(_dbg_==1){
						$('.free').addClass('border');
						//console.info('Toplam süre: '+sure+' (ms) Dizilim Süresi: '+d.time_render_tasks+' (ms) Toplam İş Sayısı: '+d.data.tasks.length+' Yerleştirilen İş Sayısı: '+(task_counter>d.data.tasks.length?d.data.tasks.length:task_counter));
						//console.log(d);
					}
				}
				let startStreaming=function(lastResponseLength) {
					xhr = new XMLHttpRequest();
					xhr.open('POST', '<?=$url?>', true);
					xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					let _output='';
					xhr.onprogress = function(e) {
						var response = e.currentTarget.response;
						var output = lastResponseLength === false
							? response
							: response.substring(lastResponseLength);
						lastResponseLength = response.length;
						try {
							let obj=JSON.parse(_output+output);
							if (_dbg_==1) {
								console.log(obj);
							}
							_output='';
							switch (obj.event) {
								case 'render_clients':
									d.client_details=obj.data;
									render_clients(obj.data);
									break;
								case 'render_timeline':
									d.timeline.push(obj.data);
									render_timeline(obj.data);
									break;
								case 'render_boxes':
									d.boxes.push(obj.data);
									render_boxes(obj.data);
									break;
								case 'render_boxes_2':
									render_boxes_2(obj.data);
									break;
								case 'equipment_render_add':
									let boxes_equipment=$('.'+obj.rowcls).find('td.client_equipment_hour');
									if(boxes_equipment.length>0){
										for(let i=0;i<boxes_equipment.length;i++){
											let el_rem_idx=$(boxes_equipment[i]);
											if(el_rem_idx.hasClass(obj.boxcls)){
												el_rem_idx.text(obj.title);
												el_rem_idx.attr('title',obj.title);
												el_rem_idx.addClass('work');
												el_rem_idx.removeClass('free');
												$(boxes_equipment[i])[0].colSpan=obj.colspan;
												for(let j=1;j<obj.colspan;j++){
													$(boxes_equipment[i+j]).remove();
												}
												break;
											}
										}
									}
									break;
								case 'equipment_render_edit':
									let boxes_equipment_edit=$('.'+obj.rowcls).find('td.'+obj.boxcls);
									boxes_equipment_edit.text(obj.title.split('new_line').join('\n'));
									boxes_equipment_edit.attr('title',obj.title.split('new_line').join('\n'));
									break;
								case 'render_opcount':
									Object.keys(obj.data).forEach(function(key_op) {
										let _item_op=obj.data[key_op];
										$('#th_tr_opcount_'+_item_op.id).text(_item_op.title);
									});
									break;
								default:
									break;
							}
						} catch (error) {
							console.log(output);
							console.warn(error);
							_output+=output;
						}
					};
	
					xhr.onreadystatechange = function() {
						if (xhr.readyState == 4) {
							console.log('Completed');
						}
					};
	
					xhr.send();
				};
				var lastResponseLength = false;
				let d={
					days : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
					,client_details:[]
					,timeline:[]
					,boxes:[]
				};
				window.d=d;
				startStreaming(lastResponseLength);
			});	
		</script>
    </body>
</html>