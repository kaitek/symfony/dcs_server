<?php
	//https://192.168.1.40/kaynak3/modules/scheduler/index.2.php?adamsayi=4&gunsayi=4&periyot=30&dbg=1
	require_once("../debug.php");
	$adamsayi=isset($_GET['adamsayi'])?$_GET['adamsayi']:5;
	$gunsayi=isset($_GET['gunsayi'])?$_GET['gunsayi']:5;
	$periyot=isset($_GET['periyot'])?$_GET['periyot']:10;
	$dbg=isset($_GET['dbg'])?$_GET['dbg']:0;
	function get_web_page( $url )
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_POST		   => true,
			CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
			CURLOPT_SSL_VERIFYHOST => false
		);
		$headers = [
			'Cache-Control: no-cache',
			'Connection: keep-alive',
			'content-length: 0',
		];
		
		$ch      = curl_init( $url );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	$ret= get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/Scheduler/getTasks/$periyot/$gunsayi");
	if($ret['errno']!=0){
		var_dump($ret);
	}else{
		try{
			$j_ret=json_decode($ret['content']);
			if(is_object($j_ret)){
				//mjd($j_ret);
			}else{
				//echo($ret['content']);
				//mjd($ret['content']);
			}
		}catch (Exception $e) {
			echo($ret['content']);
		}
	}
?>
<!DOCTYPE html>
<html lang="tr">
    <head>
        <meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='cache-control' content='no-cache'>
		<title>Verimot</title>
		<link rel="stylesheet" type="text/css" href="../../lib/myjui/mj.css"/>
		<script src="../../lib/myjui/mj-debug.js?i=3"></script>
		<script src="../../lib/myjui/myjui/language/tr.js"></script>
		<style>
			.offtime {
				background: #ffc107;
				border:1px solid;
			}
			.day {
				white-space: pre;
			}
			.work {
				background: #8bc34a;
				border:1px solid;
				border-radius: 5px;
				white-space: pre;
			}
			.free {
				border: none;
			}
			.border {
				border:1px solid;
			}
			.hide {
				display:none;
			}
			.selected {
				background: yellow;
			}
		</style>
    </head>
    <body style="-moz-user-select:none;-khtml-user-select: none;overflow-x: hidden;padding: 0px;margin: 0px;" unselectable="on">
		<div style="width: 100%;height: 100%;overflow: auto;">
			<table id="tbl_main" style="border:none;font-family: verdana;font-size: 10px;">
				<thead>
					<tr>
						<td rowspan="3" style="border:1px solid;text-align:center;">İstasyon</td>
						<td rowspan="3" style="border:1px solid;text-align:center;">Ekipman</td>
					</tr>
					<tr>
						
					</tr>
					<tr>
						
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
		<script type='text/javascript'>

			$(function() {
				let _dbg_=parseInt('<?=$dbg?>');
				let _dizilim=function(){
					const wait = ms => new Promise((resolve) => setTimeout(resolve, ms));
					let task_counter=0;
					let control_pre_task=function(opid){
						for(let i=0;i<d.data.tasks.length;i++){
							if(d.data.tasks[i].opid==opid&&typeof d.data.tasks[i].__start__!=='undefined'){
								return d.data.tasks[i];
							}
						}
						return false;
					};
					let equipment_render=function(task,str_client,str_code,colspan,boxcount){
						let boxes_equipment=$('.client_'+str_client+'_'+str_code).find('td.client_equipment_hour');
						if(boxes_equipment.length>0){
							let str_attr=task.erprefnumber+'\n'+task.opname+'\n'+task.productcount+'\n'+boxcount;
							let first_index=-1;
							for(let i=0;i<boxes_equipment.length;i++){
								let el_rem_idx=$(boxes_equipment[i]);
								if(el_rem_idx.hasClass('client_hour_'+str_client+'_'+task.__start__)){
									first_index=i;
									break;
								}
							}
							let el_equipment=$(boxes_equipment[first_index]);
							if(el_equipment.hasClass('free')){
								el_equipment.text(task.erprefnumber+' '+task.opname+' '+task.productcount+(_dbg_==1?' ('+boxcount+')':''));
								el_equipment.attr('title',str_attr);
								el_equipment.addClass('work');
								el_equipment.removeClass('free');
								for(let i=1;i<colspan;i++){
									let el_rem=$(boxes_equipment[first_index+i]);
									$(boxes_equipment[first_index]).addClass(el_rem[0].classList[1]);
									el_rem.remove();
								}
								$(boxes_equipment[first_index])[0].colSpan=colspan;
							}else{
								el_equipment.text(el_equipment.text()+'\n'+ task.erprefnumber+' '+task.opname+' '+task.productcount+(_dbg_==1?' ('+boxcount+')':''));
							}
							//console.log(str_client,str_code,task.erprefnumber,task.opname);
							task.done=true;
							task_counter++;
						}	
					};
					let task_render=function(task, str_client, boxes, adamsayi, index){
						//console.log(task.erprefnumber,'---',index);
						let boxcount=Math.ceil(task.sure/d.data.period);
						let counter=0;
						let j;
						for(j=index;j<boxes.length;j++){
							if(counter<boxcount){
								let elc=$(boxes[j]);
								let __start__=elc[0].id.replace('tb_tr_client_hours_','');
								if(!elc.hasClass('offtime')){
									let el_opcount=$('#'+elc[0].id.replace('tb_tr_client_hours','th_tr_opcount'));
									if(el_opcount.length>0){
										if(!elc.hasClass('work')&&parseInt(el_opcount.text())<adamsayi){
											if(counter<boxcount){
												if(el_opcount.length>0){
													++counter;
													if(typeof task.__start__==='undefined'){
														task.__start__=__start__;
													}
													//if(client_detail_length>1){//robotta ilk koyulan iş sonrası sadece 1 personel arttırımı için
													//	let tmp_classname=elc[0].id.replace('tb_tr_client_hours','client_hour_'+str_client);
													//	let tmp_row_count=$('.'+tmp_classname+'.work');
													//	if(tmp_row_count.length==0){
													//		el_opcount.text(parseInt(el_opcount.text())+1);
													//	}
													//}else{
													el_opcount.text(parseInt(el_opcount.text())+1);
													//}
													elc.addClass('work');
													elc.removeClass('free');
												}
											}
										}else{
											break;
										}
									}
								}else{
									if(j==index){
										break;
									}
								}
							}else{
								for(let i=index+1;i<j;i++){
									let el_rem=$(boxes[i]);
									$(boxes[index]).addClass(el_rem[0].classList[1]);
									el_rem.remove();
								}
								$(boxes[index])[0].colSpan=j-index;
								if(typeof task.groupedtask==='undefined'){
									let arr_clientdetail=task.clientdetail.split('|');
									for(let cj=0;cj<arr_clientdetail.length;cj++){
										let tcd=arr_clientdetail[cj];
										let str_code=tcd.split('.').join('');
										equipment_render(task,str_client,str_code,j-index,boxcount);
									}
								}else{
									for(let i=0;i<d.data.tasks.length;i++){
										let _row_task=d.data.tasks[i];
										if(_row_task.groupedtask===task.groupedtask){
											_row_task.__start__=task.__start__;
											let arr_clientdetail=_row_task.clientdetail.split('|');
											for(let cj=0;cj<arr_clientdetail.length;cj++){
												let tcd=arr_clientdetail[cj];
												let str_code=tcd.split('.').join('');
												equipment_render(_row_task,str_client,str_code,j-index,boxcount);
											}
										}
									}
								}
								return;
							}
						}
						if(counter>0){
							//yarıda kalan varsa temizle
							delete task.__start__;
							for(let i=index;i<j;i++){
								let el=$(boxes[i]);
								if(el.hasClass('work')){
									let el_opcount=$('#'+el[0].id.replace('tb_tr_client_hours','th_tr_opcount'));
									if(el_opcount.length>0){
										el_opcount.text(parseInt(el_opcount.text())-1);
										el.addClass('free');
										el.removeClass('work');
										el.text('');
										el.removeAttr( "title" );
									}
								}
							}
						}
						if(j<boxes.length){
							return task_render(task, str_client, boxes, adamsayi, j+1);
						}
					};
					let d={
						days : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
					};
					d.adamsayi=parseInt('<?=$adamsayi?>')
					window.d=d;
					try{
						d.data=JSON.parse('<?=json_encode($j_ret)?>');
						let thead_tr=$("#tbl_main").find("thead").find("tr");
						let thead_tr_days=$(thead_tr[0]);
						let thead_tr_hours=$(thead_tr[1]);
						let thead_tr_opcount=$(thead_tr[2]);
						let tbody=$("#tbl_main").find("tbody");
						
						function StopWatch() {
							this.startTime = new Date();
							this.endTime = null;
							
							this.stop = function() {
								this.endTime = new Date();
							}

							this.reset = function() {
								this.startTime = new Date();
							}
							
							this.duration = function() {
								if( this.endTime == null ) {
									this.stop();
								}
								return this.endTime-this.startTime;
							}
						}
						async function render_clients() {
							for(let j=0;j<d.data.client_details.length;j++){
								let row=d.data.client_details[j];
								let cnt_client=tbody.find("tr:last");
								let str_client=row.client.split('.').join('');
								let str_code='';
								//robotlarda fikstür dışında 1 satır ilave ediliyor
								tbody.append('<tr id="tb_tr_clients_'+str_client+'" class="clients client_'+str_client+'">'+
									'<td id="tb_td_clients_'+str_client+'" class="client" style="border:1px solid;" rowspan="'+(row.ekipmansayisi+1)+'">'+row.client+'</td>'+
									'<td class="clientdetail td_client_row" style="border:1px solid;"></td>'+
									'</tr>');
								let arr_codes=row.codes.split('|')
								for(let i=0;i<arr_codes.length;i++){
									let item=arr_codes[i];
									str_code=item.split('.').join('');
									tbody.append('<tr id="tb_tr_clients_'+str_client+'_'+str_code+'" class="clients client_e_'+str_client+' client_'+str_client+'_'+str_code+'">'+
									'<td id="tb_td_clients_'+str_code+'" class="clientdetail" style="border:1px solid;">'+item+'</td>'+
									'</tr>');
								}
							}
						}
						async function render_boxes() {
							let x=new Date(d.data.firsttime);
							for(let i=0;i<(d.data.dayscount*24*60/d.data.period);i++){
								//let x=new Date(d.data.firsttime);
								//x.setMilliseconds(x.getMilliseconds()+(1000* i*((d.data.period/60)*3600) ));
								let str_day=x.toLocaleDateString().split('/').reverse().join('');
								let str_day_short=x.toLocaleDateString().replace('.20','.');
								let str_time5=x.toLocaleTimeString().substr(0,5);
								let str_time4=str_time5.replace(':','');
								thead_tr_hours.append('<td id="th_tr_hours_'+str_day+str_time4+'" class="hour" style="border:1px solid;">'+x.toLocaleTimeString().substr(0,5)+'</td>');
								thead_tr_opcount.append('<td id="th_tr_opcount_'+str_day+str_time4+'" class="opcount" style="border:1px solid;text-align: right;">0</td>');
								let _day=thead_tr_days.find('.day:last');
								if(_day.length===0||(_day.length>0&&_day.text()!==str_day_short)){
									thead_tr_days.append('<td id="th_tr_days_'+str_day+'" class="day" style="border:1px solid;">'+str_day_short+'</td>');
								}else{
									if(_day.text()==str_day_short){
										_day[0].colSpan++;
									}
								}
								
								for(let j=0;j<d.data.client_details.length;j++){
									let row=d.data.client_details[j];
									let str_client=row.client.split('.').join('');
									let _cls='free';
									let _title='';
									for(l=0;l<d.data.offtimes.length;l++){
										let row_offtime=d.data.offtimes[l];
										if(row_offtime.clients.indexOf(row.client)>-1
										&&((row_offtime.repeattype!=='day') || (row_offtime.repeattype==='day'&&row_offtime.days.indexOf(d.days[x.getDay()])>-1) )
										){
											let vi_start=parseInt(row_offtime.starttime.split(':').join(''));
											let vi_finish=parseInt(row_offtime.finishtime.split(':').join(''));
											if(parseInt(str_time4)>=vi_start && parseInt(str_time4)<vi_finish){
												_cls='offtime';
												_title='title="'+row_offtime.losttype+' '+row_offtime.starttime+'-'+row_offtime.finishtime+'"';
											}
										}
									}
									let arr_codes=row.codes.split('|');
									for(let k=0;k<arr_codes.length;k++){
										let item=arr_codes[k];
										str_code=item.split('.').join('');
										
										
										$('#tb_tr_clients_'+str_client+'_'+str_code).append('<td id="tb_tr_clients_hours_'+str_day+str_time4+'" class="client_equipment_hour client_hour_'+str_client+'_'+str_day+str_time4+' '+_cls+'" style="" '+_title+' colspan=1 ></td>');
										if(k==0){
											$('#tb_tr_clients_'+str_client).append('<td id="tb_tr_client_hours_'+str_day+str_time4+'" class="client_hour client_hour_'+str_client+'_'+str_day+str_time4+' '+_cls+'" style="" '+_title+' colspan=1 ></td>');
										}
									}
								}
								x.setMilliseconds(x.getMilliseconds()+(1000* ((d.data.period/60)*3600) ));
							}
						}
						async function task_preprocess() {
							for(let i=0;i<d.data.tasks.length;i++){
								let task=d.data.tasks[i];
								if(typeof task.groupedtask==='undefined'){
									let arr_task_group=[];
									if(typeof d.data.client_mould_details[task.client]!=='undefined'){
										arr_task_group.push(i);
										let _cmd_=d.data.client_mould_details[task.client];
										for(let j=0;j<_cmd_.length;j++){
											let _row_cmd=_cmd_[j];
											let _row_arr_opname=_row_cmd.opname.split('|');
											let _flag_opname=true;
											for(let k=0;k<_row_arr_opname.length;k++){
												let _item_opname=_row_arr_opname[k];
												if(_item_opname==task.opname){
													continue;
												}
												let _task_found=false;
												for(let l=0;l<d.data.tasks.length;l++){
													let _item_task=d.data.tasks[l];
													if(typeof _item_task.groupedtask!=='undefined'
														||_task_found!==false
														||_item_task.opname!==_item_opname
														||_item_task.client!==task.client
														||_item_task.termintarihi!==task.termintarihi
														||_item_task.productcount!==task.productcount
													){
														continue;
													}else{
														arr_task_group.push(l);
														_task_found=true;
														break;
													}
												}
												if(!_task_found){
													_flag_opname=false;
												}
											}
											if(_flag_opname){
												for(m=0;m<arr_task_group.length;m++){
													let idx=arr_task_group[m];
													d.data.tasks[idx].groupedtask=arr_task_group[0];
													d.data.tasks[idx].tpp=_row_cmd.tpp;
													d.data.tasks[idx].sure=Math.ceil((parseInt(_row_cmd.tpp)*task.productcount)/60);
												}
												break;
											}
										}
									}
								}
							}
							return;
						}
						async function render_tasks() {
							for(let i=0;i<d.data.tasks.length;i++){
								let task=d.data.tasks[i];
								if(typeof task.__start__==='undefined'){
									let idx=0;
									let pre_task=false;
									if(task.onkosul!=='0'){
										pre_task=control_pre_task(task.onkosul);
									}
									//@todo: birden fazla istasyonda yapılabilme durumunda, hangisinde daha erkene yerleştirilebiliyor ise ona yerleşim yapılmalı
									let str_client=task.client.split('.').join('');
									let arr_clientdetail=task.clientdetail.split('|');
									task.sure=task.sure/arr_clientdetail.length;
									let boxes=$('.client_'+str_client).find('td.client_hour');
									if (boxes.length>0) {
										if(pre_task!==false){
											for(j=0;j<boxes.length;j++){
												let el=$(boxes[j]);
												let __start__=el[0].id.replace('tb_tr_client_hours_','');
												if(pre_task.__start__===__start__){
													idx=j;
													break;
												}
											}
										}
										task_render(task, str_client, boxes, d.adamsayi, idx);
									}
								}
							}
						}
						let timer_all=new StopWatch();
						let timer_render_clients=new StopWatch();
						let timer_render_boxes=false;
						let timer_task_preprocess=false;
						let timer_render_tasks=false;
						render_clients()
						.then(()=>{
							d.time_render_clients=timer_render_clients.duration();
							timer_render_boxes=new StopWatch();
							async function f() {
								let promise = new Promise((resolve, reject) => {
									setTimeout(() => {
										render_boxes().then(()=>{
											resolve();
										});
									}, 10)
								});
								let result = await promise;
							}
							return f();
						}).then(()=>{
							d.time_render_boxes=timer_render_boxes.duration();
							timer_task_preprocess=new StopWatch();
							async function f() {
								let promise = new Promise((resolve, reject) => {
									setTimeout(() => {
										task_preprocess().then(()=>{
											resolve();
										});
									}, 5)
								});
								let result = await promise;
							}
							return f();
						}).then(()=>{
							d.timer_task_preprocess=timer_task_preprocess.duration();
							timer_render_tasks=new StopWatch();
							async function f() {
								let promise = new Promise((resolve, reject) => {
									setTimeout(() => {
										render_tasks().then(()=>{
											resolve();
										});
									}, 10)
								});
								let result = await promise;
							}
							return f();
						}).then(()=>{
							d.time_render_tasks=timer_render_tasks.duration();
							let sure=timer_all.duration();
							if(_dbg_==1){
								$('.free').addClass('border');
								console.info('Toplam süre: '+sure+' (ms) Dizilim Süresi: '+d.time_render_tasks+' (ms) Toplam İş Sayısı: '+d.data.tasks.length+' Yerleştirilen İş Sayısı: '+(task_counter>d.data.tasks.length?d.data.tasks.length:task_counter));
								console.log(d);
							}
							if(_dbg_==0){
								//$('.td_client_row').addClass('hide');
								//$('.client_hour').addClass('hide');
								alert('Dizilim bitti!\nDizilim Süresi '+d.time_render_tasks+' (ms)\nToplam İş Sayısı: '+d.data.tasks.length+'\nYerleştirilen İş Sayısı: '+(task_counter>d.data.tasks.length?d.data.tasks.length:task_counter));
							}
						});
					}
					catch(err)
					{
						console.log(err);
						//alert(err);
						//location.reload();
					}
				}
				_dizilim();
			});	
		</script>
    </body>
</html>