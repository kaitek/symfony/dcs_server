mj.ReportView = function(config){
	mj.ReportView.superclass.constructor.call(this, config);
};

mj.ReportView.prototype = {
	_className : 'mj.ReportView',
	_url : false,
	status : '',
	elements : false,
	checkFilterValues : true,
	filterValues:false,
	customFilter:false,
	curPage : 0,
	totalPage : 0,
	filter : false,
	reportingType : 0,
	btnEventFlag : 0,
	loc : eval(decodeURIComponent(location.search.replace('?',''))),
	request : function (s){
		var xhr = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest();
		var bComplete = false;
		try{
			if (s.method == "GET")
			{
				xhr.open(s.method, s.url+"?"+s.params, true);
				s.params = "";
			}
			else
			{
				xhr.open(s.method, s.url, true);
				//xhr.setRequestHeader("Method", "POST "+s.url+" HTTP/1.1");
				xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.setRequestHeader("Accept", "text/javascript, application/javascript");
			}
			xhr.onreadystatechange = function (){
				if (xhr.readyState == 4 && !bComplete)
				{
					bComplete = true;
					var str = xhr.responseText.replace(s.callback,"");
					if(s.cb != null) s.cb(s.eval?eval(str):str);
				}
			};
			//var str = this.escape(JSON.stringify(s.params)/*.toJSONString()*/);
			str = '';
			for ( var item in s.params ){
				if(item=='toJSONString') continue;
				str = str + '&' + item + '=' + this.escape(s.params[item]);
			}
			//var data = 'jsoncallback='+s.callback+'&data='+str;
			var data = 'jsoncallback='+s.callback+str;
			xhr.send(data);
		}catch(e) {
			//alert(e);
			return false;
		}
		return true;
	},
	init : function(){
		var t = this;
		t.ifrmp = $(mj.NE(t.elements.main, {tag:'iframe',id:'myiFrm',src:location.href+'libs/mjreport/reportPrint.php',style:'display:block;left:10px;position:relative;z-index:1;'}));
		t.filterValues={};
		t.status = 'init';
		var _e = t.elements = {
			mask : $(mj.NE(t.renderTo, {cls:'rprmask',style:"display:none;position:absolute;top:0;left:0;-moz-opacity: 0.5;opacity:.50;filter: alpha(opacity=50);width:100%;height:100%;background-color:#000;z-index:1000;"})),//t.$("main"),
			main : $(mj.NE(t.renderTo, {cls:'rprmain',style:'width:100%;height: 100%;position:absolute;top:0px;left:0px;overflow:hidden;#user-select: none;#-moz-user-select: none;#-khtml-user-select: none;font-family:tahoma;font-size:8pt'}))//t.$("main"),
		};
		_e.menu = $(mj.NE(t.elements.main, {cls:'rprmenu',style:'width:100%;height: 30px;background:#a2a8aD;position:absolute;top:0px;left:0px;z-index:20;font-family:tahoma;font-size:8pt'}));
		_e.report = $(mj.NE(t.elements.main, {cls:'rprreport',style:'background : #eee;width:100%;position:absolute;top:30px;left:0px;bottom:0px;overflow:auto;user-select: none;-moz-user-select: none;-khtml-user-select: none;z-index:20;font-family:tahoma;font-size:8pt'}));
		
		_e.menu1 = $(mj.NE(t.elements.menu, {style:'float:left;left:10px;position:relative;'}));
		_e.menu2 = $(mj.NE(t.elements.menu, {style:''}));
		_e.menu21 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		_e.menu22 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu22 , {tag:'span',style:'float: left; padding: 8px 2px 8px 8px;',html:'&nbsp;Sayfa&nbsp;'}));
		_e.menu23 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		_e.menu24 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu24 , {tag:'span',style:'float: left; padding: 8px 2px 8px 4px;',html:'/'}));
		_e.menu25 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		/*_e.menu26 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu26 , {tag:'span',style:'float: left; padding: 8px 2px 8px 8px;',html:'&nbsp;İlk&nbsp;'}));
		_e.menu27 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		_e.menu28 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu28 , {tag:'span',style:'float: left; padding: 8px 2px 8px 8px;',html:'&nbsp;sayfayı yaz&nbsp;'}));*/
		_e.menu29 = $(mj.NE(_e.menu2 , {style:'float:left;left:15px;position:relative;'}));
		
		_e.top = {
			btnRefresh : $(mj.NE(_e.menu1, {tag:'input',type:'button',cls:'btnRefresh',style:'width: 50px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'Yenile'}))//t.$('btnRefresh'),
			,selectPageSize : $(mj.NE(_e.menu1, {tag:'select',cls:'selectPageSize',style:'position:relative;top:4px;font-family:tahoma;font-size:8pt',html:"<option value='a4'>A4</option><option value='a3'>A3</option>"}))
			,selectPageOrientation : $(mj.NE(_e.menu1, {tag:'select',cls:'selectPageOrientation',style:'position:relative;top:4px;font-family:tahoma;font-size:8pt',html:"<option value='v'>Dikey</option><option value='h'>Yatay</option>"}))
			,btnFirstPage : $(mj.NE(_e.menu21, {tag:'input',type:'button',cls:'btnFirstPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'|<',title:'İlk Sayfa'}))//t.$('btnFirstPage'),
			,btnPrevPage : $(mj.NE(_e.menu21, {tag:'input',type:'button',cls:'btnPrevPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'<',title:'Önceki Sayfa'}))//t.$('btnPrevPage'),
			,currentPageNumber : $(mj.NE(_e.menu23, {tag:'input',cls:'currentPageNumber',style:'text-align:right;top:4px;position:relative;font-family:tahoma;font-size:8pt',size:"3",value:"",maxlength:"3"}))//t.$('currentPageNumber'),
			,totalPageNumber : $(mj.NE(_e.menu24, {tag:'span',cls:'totalPageNumber',style:'float: left; padding: 8px 4px 4px 2px;font-family:tahoma;font-size:8pt'}))//t.$('totalPageNumber')
			,btnNextPage : $(mj.NE(_e.menu25, {tag:'input',type:'button',cls:'btnNextPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'>',title:'Sonraki Sayfa'}))//t.$('btnNextPage'),
			,btnLastPage : $(mj.NE(_e.menu25, {tag:'input',type:'button',cls:'btnLastPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'>|',title:'Son Sayfa'}))//t.$('btnLastPage'),
			//printFinishPage : $(mj.NE(_e.menu27, {tag:'input',cls:'printFinishPage',style:'text-align:right;top:4px;position:relative;font-family:tahoma;font-size:8pt',size:"3",value:"",maxlength:"3"}))//t.$('printFinishPage'),
			//selectprintStyle : t.$('selectprintStyle'),
			//DOSPrintDevices : t.$('DOSPrintDevices'),
			,btnPrint : $(mj.NE(_e.menu29, {tag:'input',type:'button',cls:'btnPrint',style:'top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'Yazdır'}))//t.$('btnPrint')
		};
		//add event listeners
		t.elements.menu.find('input:button').each(function(){
			var _x=$(this);
			_x.click(function(e){
				e.stopPropagation();
				t.btnclick(t,_x);
			});
		});
		t.elements.menu.find('input:text').each(function(){
			var _x=$(this);
			_x.change(function(e){
				e.stopPropagation();
				t.currentPageNumberOnchange(t,e);
			});
		});
		t.elements.menu.find('select').each(function(){
			var _x=$(this);
			_x.change(function(e){
				e.stopPropagation();
				t.itemchange(t,_x);
			});
		});
		/*t.Event.add(t.elements.top.btnRefresh, "click", t.btnRefreshOnclick, t);
		t.Event.add(t.elements.top.selectPageSize, "change", t.selectPageSizeChange, t);
		t.Event.add(t.elements.top.selectPageOrientation, "change", t.selectPageOrientationChange, t);
		t.Event.add(t.elements.top.btnFirstPage, "click", t.btnFirstPageOnclick, t);
		t.Event.add(t.elements.top.btnPrevPage, "click", t.btnPrevPageOnclick, t);
		t.Event.add(t.elements.top.currentPageNumber, "change", t.currentPageNumberOnchange, t);
		t.Event.add(t.elements.top.btnNextPage, "click", t.btnNextPageOnclick, t);
		t.Event.add(t.elements.top.btnLastPage, "click", t.btnLastPageOnclick, t);
		t.Event.add(t.elements.top.btnPrint, "click", t.btnPrintOnclick, t);*/
		t.setPageValues();
		t.btnRefreshOnclick(t);
	},
	btnclick:function(t,obj){
		//console.log(obj.context.className);
		var cn=typeof obj.context=='undefined'?obj[0].className:obj.context.className;
		if(cn=='btnRefresh'){
			t.btnRefreshOnclick(t);
		}else if(cn=='btnFirstPage'){
			t.btnFirstPageOnclick(t);
		}else if(cn=='btnPrevPage'){
			t.btnPrevPageOnclick(t);
		}else if(cn=='btnNextPage'){
			t.btnNextPageOnclick(t);
		}else if(cn=='btnLastPage'){
			t.btnLastPageOnclick(t);
		}else if(cn=='btnPrint'){
			t.btnPrintOnclick(t);
		}
	},
	itemchange:function(t,obj){
		//console.log(obj.context.className);
		var cn=typeof obj.context=='undefined'?obj[0].className:obj.context.className;
		if(cn=='selectPageSize'){
			t.selectPageSizeChange(t);
		}else if(cn=='selectPageOrientation'){
			t.selectPageOrientationChange(t);
		}
	},
	enabler : function(el){
		//typeof(el)=='string'?this.$(el).removeAttribute('disabled'):el.removeAttribute('disabled');
		el[0].removeAttribute('disabled');
	},
	disabler : function(el){
		//typeof(el)=='string'?this.$(el).setAttribute('disabled','true'):el.setAttribute('disabled','true');
		el[0].setAttribute('disabled','true');
	},
	hide : function(el){
		//typeof(el)=='string'?this.$(el).style.display = 'none':el.style.display = 'none';
		el[0].style.display = 'none';
	},
	show : function(el){
		//typeof(el)=='string'?this.$(el).style.display = 'block':el.style.display = 'block';
		el[0].style.display = 'block';
	},
	getSendObjectDefaults : function(){
		return {
			url : this._url,
			method : 'POST',
			callback : 'jcb'+new Date().getTime(),
			eval : true
		};
	},
	escape : function(str){
		str = str ? str : '';
		str = str.toString().replace(/\+/g,'<|p|>');
		str = str.toString().replace(/\%/g,'<|_|>');
		return encodeURIComponent ? encodeURIComponent(str) : escape(str);
	},
	setButtonstatus : function(type){
		this.status = type;
		for(var x in this.elements.top){
			if(x == 'toJSONString') continue;
			if(type == 'first')
				(x != 'btnRefresh')?this.disabler(this.elements.top[x]):this.enabler(this.elements.top[x]);
			else
				this.enabler(this.elements.top[x]);
			// if(x == 'chkShowData' || x == 'chkShowFilters'){
				// this.$(x).checked = false;
				// this.$(x).value = 0;
			// }
		}	
	},
	setPageValues : function(){
		var t = this;
		t.elements.top.currentPageNumber[0].value=t.curPage;
		t.elements.top.totalPageNumber[0].innerHTML=t.totalPage;	
	},
	getPageProperties : function(){
		var t = this;
		var o={};
		if(t.elements.top.selectPageSize[0].value=='a4')
			if(t.elements.top.selectPageOrientation[0].value=='v'){
				o.width = '210mm';
				o.height = '297mm';
			}else{
				o.width = '297mm';
				o.height = '210mm';
			}
		else
			if(t.elements.top.selectPageOrientation[0].value=='v'){
				o.width = '297mm';
				o.height = '420mm';
			}else{
				o.width = '420mm';
				o.height = '297mm';
			}
		return o;
	},
	changePageProperties : function(){
		var o = this.getPageProperties();
		var el = this.elements.report[0].childNodes[0];
		if(el){
			el.style.width = o.width;
			el.style.height = o.height;
		}
	},
	createReportDiv : function(){
		var t = this;
		var o = t.getPageProperties();
		t.elements.report[0].innerHTML = '<table align="center" class="tblReport" style="width: '+o.width+'; height: '+o.height+'; background: none repeat scroll 0% 0% white;"><tr valign="top"><td></td></tr></table>';
	},
	loader : function(){
		var t = this;
		var o = t.getSendObjectDefaults();
		o.params = {className:'mjReportDS',methodName:'getReportData',file:t.loc.file,id:t.id,currentPageNumber:t.curPage.toString(),reportingType : t.reportingType,customFilter:t.customFilter,filter:JSON.stringify(t.filter)};
		o.cb = function(xh){
			t.createReportDiv();
			if(xh.result=='nok'){
				alert(xh.msg);
			}
			if(xh.result=='ok'){
				t.setButtonstatus('list');
				t.elements.report[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].innerHTML = xh.html;
				t.curPage = parseInt(xh.currentPageNumber);
				t.totalPage = parseInt(xh.totalPageNumber)
				t.setPageValues();
			}
			t.hide(t.elements.mask);
		};
		t.request(o);
		t.setButtonstatus('first');
	},
	go2page : function(pageNumber){
		this.show(this.elements.mask);
		pageNumber = isNaN(parseInt(pageNumber))?1:parseInt(pageNumber);
		if(pageNumber < 1)
			pageNumber = 1;
		if(pageNumber > this.totalPage)
			pageNumber = this.totalPage;
		this.curPage = pageNumber;
		this.setPageValues();
		this.loader();
	},
	btnRefreshOnclick : function(sc,e){
		var t = this;
		t.trigger('refresh', t);
		t.elements.report[0].innerHTML = '';
		t.setButtonstatus('first');
	},
	selectPageSizeChange : function(sc,e){
		sc.changePageProperties();
	},
	selectPageOrientationChange : function(sc,e){
		sc.changePageProperties();
	},
	btnFirstPageOnclick : function(sc,e){
		if(this.curPage > 1)
			this.go2page(1);
	},
	btnPrevPageOnclick : function(sc,e){
		if(this.curPage > 1)
			this.go2page(--this.curPage);
	},
	currentPageNumberOnchange : function(sc,e){
		var re = /^\d{1,5}?$/;
		var pageNumber = e.target.value;
		if(pageNumber != '') { 
			if(regs = pageNumber.match(re)) { 
				sc.go2page(pageNumber);
			}else { 
				e.target.value = sc.curPage;  
			} 
		}
	},
	btnNextPageOnclick : function(sc,e){
		if(this.totalPage > this.curPage)
			this.go2page(++this.curPage);
	},
	btnLastPageOnclick : function(sc,e){
		if(this.totalPage > this.curPage)
			this.go2page(this.totalPage);
	},
	btnPrintOnclick : function(sc,e){
		var t = this;
		//t.show(t.elements.mask);
		//t.ifrm = $(mj.NE(t.elements.main, {tag:'iframe',id:'myiFrm',src:location.href+'libs/mjreport/reportPrint.php',style:'display:block;left:10px;position:relative;z-index:1;'}));
		//window._varPrint = t;
		//t.checkIframeLoadingPrint();
		t.ifrmp[0].contentWindow.setContent(t.elements.report[0].innerHTML);
		t.ifrmp[0].contentWindow.focus();
		t.ifrmp[0].contentWindow.print();
		t.ifrmp[0].contentWindow.setContent('');
	},
	btnListOnClick : function(e){
		var t = this;
		t.checkFilterValues = true;
		if(t.checkFilterValues){
			t.show(t.elements.mask);
			switch (t.reportingType) {
				case 'screen':
					t.loader();
				break;
				case 'excel-0':
				case 'excel-1':
				case 'pdf':
					t.ifrm = $(mj.NE(t.elements.main, {	tag:'iframe',
														id:'myiFrm',
														src:t._url+'?className=mjReportDS&methodName=getReportData&file='+t.escape(t.loc.file)+'&id='+t.id+'&currentPageNumber='+t.curPage+'&reportingType='+t.reportingType+'&customFilter='+t.customFilter+'&filter='+t.escape(JSON.stringify(t.filter)),
														style:'display:none;left:10px;position:relative;z-index:1;'
														}));
					window._varPrintList = t;
					t.checkIframeLoading();
				break;
			}	
		}
	},
	checkIframeLoading : function(){
		var t = this;
		if (t.ifrm[0].contentWindow && t.ifrm[0].contentWindow.document && t.ifrm[0].contentWindow.document.readyState == 'complete') {
			//t.enabler(t.elements.btnList);
			t.hide(t.elements.mask);
			//t.elements.main[0].removeChild(t.ifrm[0]);
			t.ifrm = false;
			window._varPrintList = false;
			return;
		}
		window.setTimeout("_varPrintList.checkIframeLoading()", 100);      
	}/*,
	checkIframeLoadingPrint : function(){
		var t = _varPrint;
		if(t.ifrm[0].contentWindow && t.ifrm[0].contentWindow.document && t.ifrm[0].contentWindow.document.readyState == 'complete'){
			t.ifrm[0].contentWindow.setContent(t.elements.report[0].innerHTML);
			t.ifrm[0].contentWindow.focus();
			t.ifrm[0].contentWindow.print();
			t.hide(t.elements.mask);
			t.elements.main[0].removeChild(t.ifrm[0]);
			t.ifrm = false;
			window._varPrint = false;	
			return;
		}
		window.setTimeout("_varPrint.checkIframeLoadingPrint();", 100);
	}*/
};
mj.extend(mj.ReportView, mj.component);