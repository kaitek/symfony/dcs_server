/*classHelper = function(scope){
	this.scope = scope;
	var c = function() {
		var init = arguments.callee.prototype.init;
		if (init) { init.apply(this,arguments); }
	};
	c.implement = function(parent) {
		for (var p in parent.prototype) { this.prototype[p] = parent.prototype[p]; }
		return this;
	};
	c.extend = function(parent) {
		var tmp = function(){};
		tmp.prototype = parent.prototype;
		this.prototype = new tmp();
		this.prototype.constructor = this;
		return this;
	};
	c.prototype.bind = function(fnc) { return fnc.bind(this); };
	c.prototype.dispatch = function(type, data) {
		var obj = {
			type:type,
			target:this,
			timeStamp:(new Date()).getTime(),
			data:data
		}
		var tocall = [];
		var list = this.scope.Event._byName[type];
		for (var id in list) {
			var item = list[id];
			if (!item[0] || item[0] == this) { tocall.push(item[2]); }
		}
		var len = tocall.length;
		for (var i=0;i<len;i++) { tocall[i](obj); }
	}
	return c;
};

domHelper = function(scope){
	this.scope = scope;
};
domHelper.prototype = {
	elm:function(name, opts) {
		var elm = document.createElement(name);
		for (var p in opts) {
			var val = opts[p];
			if (p == "class") { p = "className"; }
			if (p in elm) { elm[p] = val; }
		}
		this.scope.Style.set(elm, opts);
		return elm;
	},
	text:function(str) { return document.createTextNode(str); },
	clear:function(node) { while (node.firstChild) {node.removeChild(node.firstChild);} },
	pos:function(elm) { // relative to _viewport_ 
		var cur = this.scope.$(elm);
		var html = cur.ownerDocument.documentElement;
		var parent = cur.parentNode;
		var x = y = 0;
		if (cur == html) { return [x,y]; }
		while (1) {
			if (this.scope.Style.get(cur,"position") == "fixed") {
				x += cur.offsetLeft;
				y += cur.offsetTop;
				return [x,y];
			}
			
			if (this.scope.browser.opera && (parent == html || this.scope.Style.get(cur,"display") != "block")) { } else {
				x -= parent.scrollLeft;
				y -= parent.scrollTop;
			}
			if (parent == cur.offsetParent || cur.parentNode == html) {
				x += cur.offsetLeft;
				y += cur.offsetTop;
				cur = parent;
			}
			
			if (parent == html) { return [x,y]; }
			parent = parent.parentNode;
		}
	},
	scroll:function() { 
		var x = document.documentElement.scrollLeft || document.body.scrollLeft || 0;
		var y = document.documentElement.scrollTop || document.body.scrollTop || 0;
		return [x,y];
	},
	win:function() {
		var node = (document.compatMode == "CSS1Compat" ? document.documentElement : document.body);
		if (this.scope.browser.opera && parseFloat(navigator.appVersion) < 9.5) { node = document.body; }
		var x = node.clientWidth;
		var y = node.clientHeight;
		return [x,y];
	},
	hasClass:function(node, className) {
		var cn = this.scope.$(node).className;
		var arr = (cn ? cn.split(" ") : []);
		return (arr.indexOf(className) != -1);
	},
	addClass:function(node,className) {
		if (this.scope.DOM.hasClass(node, className)) { return; }
		var cn = this.scope.$(node).className;
		var arr = (cn ? cn.split(" ") : []);
		arr.push(className);
		this.scope.$(node).className = arr.join(" ");
	},
	removeClass:function(node, className) {
		if (!this.scope.DOM.hasClass(node, className)) { return; }
		var cn = this.scope.$(node).className;
		var arr = (cn ? cn.split(" ") : []);
		var arr = arr.filter(function($){ return $ != className; });
		this.scope.$(node).className = arr.join(" ");
	},
	append:function() {
		if (arguments.length == 1) {
			var arr = arguments[0];
			var root = this.scope.$(arr[0]);
			for (var i=1;i<arr.length;i++) { root.appendChild(this.scope.$(arr[i])); }
		} else for (var i=0;i<arguments.length;i++) { this.scope.DOM.append(arguments[i]); }
	}
}
styleHelper = function(scope){
	this.scope = scope;
};
styleHelper.prototype = {
	get:function(elm, prop) {
		if (document.defaultView && document.defaultView.getComputedStyle) {
			try {
				var cs = elm.ownerDocument.defaultView.getComputedStyle(elm,"");
			} catch(e) {
				return false;
			}
			if (!cs) { return false; }
			return cs[prop];
		} else {
			return elm.currentStyle[prop];
		}
	},
	set:function(elm, obj) {
		for (var p in obj) { 
			var val = obj[p];
			if (p == "opacity" && this.scope.browser.ie) {
				p = "filter";
				val = "alpha(opacity="+Math.round(100*val)+")";
				elm.style.zoom = 1;
			} else if (p == "float") {
				p = (this.scope.browser.ie ? "styleFloat" : "cssFloat");
			}
			if (p in elm.style) { elm.style[p] = val; }
		}
	}
};

*/
eventHelper = function(scope){
	this.scope = scope;
};
eventHelper.prototype = {
	_id:0,
	_byName:{},
	_byID:{},
	bind : function (fn,scope){
		var args = Array.prototype.slice.call(arguments, 1); 
		return function() { 
			return fn.apply(scope, args.concat(Array.prototype.slice.call(arguments))); 
		}
	},
	add:function(elm,event,cb,sc) {
		var id = this.scope.Event._id++;
		//var element = this.scope.$(elm);
		var element = elm[0];
		var fnc = this.bind(cb,sc||this.scope);
		if (element) {
			if (element.addEventListener) {
				element.addEventListener(event,fnc,false);
			} else if (element.attachEvent) {
				fnc = function() { return cb.apply(elm,arguments); }
				element.attachEvent("on"+event,fnc);
			}
		}
		if (!(event in this.scope.Event._byName)) { this.scope.Event._byName[event] = {}; }
		var obj = this.scope.Event._byName[event];
		obj[id] = [element,event,fnc];
		this.scope.Event._byID[id] = obj;
		return id;
	},
	remove:function(id) {
		var obj = this.scope.Event._byID[id];
		if (!obj) { return; }
		var e = obj[id];
		var elm = e[0];
		if (elm) {
			if (elm.removeEventListener) {
				elm.removeEventListener(e[1],e[2],false);
			} else if (elm.detachEvent) {
				elm.detachEvent("on"+e[1],e[2]);
			}
		}
		delete this.scope.Event._byID[id];
		delete obj[id];
	},
	stop:function(e) { e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true; },
	prevent:function(e) { e.preventDefault ? e.preventDefault() : e.returnValue = false; },
	target:function(e) { return e.target || e.srcElement; }
};

ReportViewer = function(config){
	mj.apply(this, config);
	this.init();
};
ReportViewer.prototype = {
	_className : 'ReportViewer',
	_url : false,//location.protocol+'//'+location.hostname+'/'+location.pathname,
	status : '',
	elements : false,
	browser : {
		opera:!!window.opera,
		ie:!!document.attachEvent && !window.opera,
		gecko:!!document.getAnonymousElementByAttribute,
		webkit:!!navigator.userAgent.match(/webkit/i),
		khtml:!!navigator.userAgent.match(/khtml/i) || !!navigator.userAgent.match(/konqueror/i)
	},
	Event : false,
	checkFilterValues : true,
	filterValues:false,
	curPage : 0,
	totalPage : 0,
	filter : false,
	reportingType : 0,
	btnEventFlag : 0,
	loc : eval(decodeURIComponent(location.search.replace('?',''))),
	request : function (s){
		var xhr = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest();
		var bComplete = false;
		try{
			if (s.method == "GET")
			{
				xhr.open(s.method, s.url+"?"+s.params, true);
				s.params = "";
			}
			else
			{
				xhr.open(s.method, s.url, true);
				//xhr.setRequestHeader("Method", "POST "+s.url+" HTTP/1.1");
				xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.setRequestHeader("Accept", "text/javascript, application/javascript");
			}
			xhr.onreadystatechange = function (){
				if (xhr.readyState == 4 && !bComplete)
				{
					bComplete = true;
					var str = xhr.responseText.replace(s.callback,"");
					if(s.cb != null) s.cb(s.eval?eval(str):str);
				}
			};
			//var str = this.escape(JSON.stringify(s.params)/*.toJSONString()*/);
			str = '';
			for ( var item in s.params ){
				if(item=='toJSONString') continue;
				str = str + '&' + item + '=' + this.escape(s.params[item]);
			}
			//var data = 'jsoncallback='+s.callback+'&data='+str;
			var data = 'jsoncallback='+s.callback+str;
			xhr.send(data);
		}catch(e) {
			//alert(e);
			return false;
		}
		return true;
	},
	init : function(){
		var t = this;
		t.ifrmp = $(mj.NE(t.elements.main, {tag:'iframe',id:'myiFrm',src:location.href+'libs/mjreport/reportPrint.php',style:'display:block;left:10px;position:relative;z-index:1;'}));
		t.filterValues={};
		t.Event = new eventHelper(t);
		t.status = 'init';
		var _e = t.elements = {
			mask : $(mj.NE(t.renderTo, {cls:'rprmask',style:"display:none;position:absolute;top:0;left:0;-moz-opacity: 0.5;opacity:.50;filter: alpha(opacity=50);width:100%;height:100%;background-color:#000;z-index:1000;"})),//t.$("main"),
			main : $(mj.NE(t.renderTo, {cls:'rprmain',style:'width:100%;height: 100%;position:absolute;top:0px;left:0px;overflow:hidden;#user-select: none;#-moz-user-select: none;#-khtml-user-select: none;font-family:tahoma;font-size:8pt'}))//t.$("main"),
		};
		_e.menu = $(mj.NE(t.elements.main, {cls:'rprmenu',style:'width:100%;height: 30px;background:#a2a8aD;position:absolute;top:0px;left:0px;z-index:20;font-family:tahoma;font-size:8pt'}));
		_e.report = $(mj.NE(t.elements.main, {cls:'rprreport',style:'background : #eee;width:100%;position:absolute;top:30px;left:0px;bottom:0px;overflow:auto;user-select: none;-moz-user-select: none;-khtml-user-select: none;z-index:20;font-family:tahoma;font-size:8pt'}));
		
		_e.menu1 = $(mj.NE(t.elements.menu, {style:'float:left;left:10px;position:relative;'}));
		_e.menu2 = $(mj.NE(t.elements.menu, {style:''}));
		_e.menu21 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		_e.menu22 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu22 , {tag:'span',style:'float: left; padding: 8px 2px 8px 8px;',html:'&nbsp;Sayfa&nbsp;'}));
		_e.menu23 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		_e.menu24 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu24 , {tag:'span',style:'float: left; padding: 8px 2px 8px 4px;',html:'/'}));
		_e.menu25 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		/*_e.menu26 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu26 , {tag:'span',style:'float: left; padding: 8px 2px 8px 8px;',html:'&nbsp;İlk&nbsp;'}));
		_e.menu27 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		_e.menu28 = $(mj.NE(_e.menu2 , {style:'float:left;left:10px;position:relative;'}));
		$(mj.NE(_e.menu28 , {tag:'span',style:'float: left; padding: 8px 2px 8px 8px;',html:'&nbsp;sayfayı yaz&nbsp;'}));*/
		_e.menu29 = $(mj.NE(_e.menu2 , {style:'float:left;left:15px;position:relative;'}));
		
		_e.top = {
			btnRefresh : $(mj.NE(_e.menu1, {tag:'input',type:'button',cls:'btnRefresh',style:'width: 50px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'Yenile'})),//t.$('btnRefresh'),
			selectPageSize : $(mj.NE(_e.menu1, {tag:'select',cls:'selectPageSize',style:'position:relative;top:4px;font-family:tahoma;font-size:8pt',html:"<option value='a4'>A4</option><option value='a3'>A3</option>"})),
			selectPageOrientation : $(mj.NE(_e.menu1, {tag:'select',cls:'selectPageOrientation',style:'position:relative;top:4px;font-family:tahoma;font-size:8pt',html:"<option value='v'>Dikey</option><option value='h'>Yatay</option>"})),
			btnFirstPage : $(mj.NE(_e.menu21, {tag:'input',type:'button',cls:'btnFirstPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'|<',title:'İlk Sayfa'})),//t.$('btnFirstPage'),
			btnPrevPage : $(mj.NE(_e.menu21, {tag:'input',type:'button',cls:'btnPrevPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'<',title:'Önceki Sayfa'})),//t.$('btnPrevPage'),
			currentPageNumber : $(mj.NE(_e.menu23, {tag:'input',cls:'currentPageNumber',style:'text-align:right;top:4px;position:relative;font-family:tahoma;font-size:8pt',size:"3",value:"",maxlength:"3"})),//t.$('currentPageNumber'),
			totalPageNumber : $(mj.NE(_e.menu24, {tag:'span',cls:'totalPageNumber',style:'float: left; padding: 8px 4px 4px 2px;font-family:tahoma;font-size:8pt'})),//t.$('totalPageNumber'),
			btnNextPage : $(mj.NE(_e.menu25, {tag:'input',type:'button',cls:'btnNextPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'>',title:'Sonraki Sayfa'})),//t.$('btnNextPage'),
			btnLastPage : $(mj.NE(_e.menu25, {tag:'input',type:'button',cls:'btnLastPage',style:'width: 30px;top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'>|',title:'Son Sayfa'})),//t.$('btnLastPage'),
			//printFinishPage : $(mj.NE(_e.menu27, {tag:'input',cls:'printFinishPage',style:'text-align:right;top:4px;position:relative;font-family:tahoma;font-size:8pt',size:"3",value:"",maxlength:"3"})),//t.$('printFinishPage'),
			//selectprintStyle : t.$('selectprintStyle'),
			//DOSPrintDevices : t.$('DOSPrintDevices'),
			btnPrint : $(mj.NE(_e.menu29, {tag:'input',type:'button',cls:'btnPrint',style:'top:4px;position:relative;font-family:tahoma;font-size:8pt',value:'Yazdır'}))//t.$('btnPrint')
		};
		//add event listeners
		//t.elements.top.btnRefresh.click($.proxy(t.btnRefreshOnclick,t));
		t.Event.add(t.elements.top.btnRefresh, "click", t.btnRefreshOnclick, t);
		t.Event.add(t.elements.top.selectPageSize, "change", t.selectPageSizeChange, t);
		t.Event.add(t.elements.top.selectPageOrientation, "change", t.selectPageOrientationChange, t);
		t.Event.add(t.elements.top.btnFirstPage, "click", t.btnFirstPageOnclick, t);
		t.Event.add(t.elements.top.btnPrevPage, "click", t.btnPrevPageOnclick, t);
		t.Event.add(t.elements.top.currentPageNumber, "change", t.currentPageNumberOnchange, t);
		t.Event.add(t.elements.top.btnNextPage, "click", t.btnNextPageOnclick, t);
		t.Event.add(t.elements.top.btnLastPage, "click", t.btnLastPageOnclick, t);
		t.Event.add(t.elements.top.btnPrint, "click", t.btnPrintOnclick, t);

		t.setPageValues();
		t.btnRefreshOnclick(t);
	},
	enabler : function(el){
		//typeof(el)=='string'?this.$(el).removeAttribute('disabled'):el.removeAttribute('disabled');
		el[0].removeAttribute('disabled');
	},
	disabler : function(el){
		//typeof(el)=='string'?this.$(el).setAttribute('disabled','true'):el.setAttribute('disabled','true');
		el[0].setAttribute('disabled','true');
	},
	hide : function(el){
		//typeof(el)=='string'?this.$(el).style.display = 'none':el.style.display = 'none';
		el[0].style.display = 'none';
	},
	show : function(el){
		//typeof(el)=='string'?this.$(el).style.display = 'block':el.style.display = 'block';
		el[0].style.display = 'block';
	},
	getSendObjectDefaults : function(){
		return {
			url : this._url,
			method : 'POST',
			callback : 'jcb'+new Date().getTime(),
			eval : true
		};
	},
	escape : function(str){
		str = str ? str : '';
		str = str.toString().replace(/\+/g,'<|p|>');
		str = str.toString().replace(/\%/g,'<|_|>');
		return encodeURIComponent ? encodeURIComponent(str) : escape(str);
	},
	setButtonstatus : function(type){
		this.status = type;
		for(var x in this.elements.top){
			if(x == 'toJSONString') continue;
			if(type == 'first')
				(x != 'btnRefresh')?this.disabler(this.elements.top[x]):this.enabler(this.elements.top[x]);
			else
				this.enabler(this.elements.top[x]);
			// if(x == 'chkShowData' || x == 'chkShowFilters'){
				// this.$(x).checked = false;
				// this.$(x).value = 0;
			// }
		}	
	},
	checkDate : function(field) { 
		var allowBlank = false; 
		//var minYear = 1902; 
		//var maxYear = (new Date()).getFullYear(); 
		var errorMsg = ""; 
		//var re = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
		var re = /^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
		if(field.value != '') { 
			if(regs = field.value.match(re)) { 
				// if(regs[1] < 1 || regs[1] > 31) { 
					// errorMsg = "Invalid value for day: " + regs[1]; 
				// } else if(regs[2] < 1 || regs[2] > 12) { 
					// errorMsg = "Invalid value for month: " + regs[2]; 
				// } else if(regs[3] < minYear || regs[3] > maxYear) { 
					// errorMsg = "Invalid value for year: " + regs[3] + " - must be between " + minYear + " and " + maxYear; 
				// } 
			} else { 
				errorMsg = "Geçersiz tarih : " + field.value; 
			} 
		} else if(!allowBlank) { 
			errorMsg = "Tarih boş olamaz!"; 
		} 
		if(errorMsg != "") { 
			alert(errorMsg); 
			this.checkFilterValues = false;
			field.focus(); 
			field.select();
			return false; 
		} 
		return true; 
	},
	checkNumber : function(field){
		var allowBlank = false;
		var errorMsg = ""; 
		var re = /^\d{1,5}(\,\d{1,2})?$/;
		if(field.value != '') { 
			if(regs = field.value.match(re)) { 
			
			}else { 
				errorMsg = "Geçersiz değer : " + field.value; 
			} 
		}else if(!allowBlank) { 
			errorMsg = "Değer boş olamaz!"; 
		} 
		if(errorMsg != "") { 
			alert(errorMsg); 
			this.checkFilterValues = false;
			field.focus(); 
			field.select();
			return false; 
		} 
		return true; 
	},
	getItemValue : function(item){
		if(!item.isdate&&!item.isnumber)
			return item.value;
		else
			if(item.isdate)
				return this.checkDate(item)?item.value:'';
			else
				return this.checkNumber(item)?item.value:'';
	},
	getFilterValues : function(){
		var t = this;
		var fBody = t.elements.report[0].childNodes[0].childNodes[1].childNodes;
		var obj = {};
		for(var i=0;i<fBody.length;i++){
			if(fBody[i].childNodes.length>1){
				var item = fBody[i].childNodes[1];
				for(var j=0;j<item.childNodes.length;j++){
					var citem = item.childNodes[j];
					citem.isdate = citem.attributes.getNamedItem('isdate')?citem.attributes.getNamedItem('isdate').nodeValue:false;
					citem.isnumber = citem.attributes.getNamedItem('isnumber')?citem.attributes.getNamedItem('isnumber').nodeValue:false;
					if(citem.type=='select-multiple'){
						var tmp = [];
						for(var k=0;k<citem.childNodes.length;k++)
							if(citem.childNodes[k].selected)
								tmp.push(citem.childNodes[k].value);
						obj[citem.id] = tmp.join(',');
					}else if(citem.isdate&&t.frmFilter.items.length>0){
						var val = parseInt(t.frmFilter.getField('Date_'+citem.id).value);
						t.filterValues['Date_'+citem.id]=val;
						var tval = t.frmFilter.getField('Time_'+citem.id).value.split(':');
						var x=new Date(val*1000);
						x.setHours(tval[0]);
						x.setMinutes(tval[1]);
						x.setSeconds(citem.id.indexOf('_2')>0?59:0);
						//val = val + parseInt(tval[0]*3600+tval[1]*60+(citem.id.indexOf('_2')>0?59:0));
						val=x.getTime()/1000;
						obj[citem.id] = val.toString();
					}else
						obj[citem.id] = t.getItemValue(citem);
				}
			}
		}
		return obj;
	},
	setPageValues : function(){
		var t = this;
		t.elements.top.currentPageNumber[0].value=t.curPage;
		t.elements.top.totalPageNumber[0].innerHTML=t.totalPage;	
	},
	getPageProperties : function(){
		var t = this;
		var o={};
		if(t.elements.top.selectPageSize[0].value=='a4')
			if(t.elements.top.selectPageOrientation[0].value=='v'){
				o.width = '210mm';
				o.height = '297mm';
			}else{
				o.width = '297mm';
				o.height = '210mm';
			}
		else
			if(t.elements.top.selectPageOrientation[0].value=='v'){
				o.width = '297mm';
				o.height = '420mm';
			}else{
				o.width = '420mm';
				o.height = '297mm';
			}
		return o;
	},
	changePageProperties : function(){
		var o = this.getPageProperties();
		var el = this.elements.report[0].childNodes[0];
		if(el){
			el.style.width = o.width;
			el.style.height = o.height;
		}
	},
	createReportDiv : function(){
		var t = this;
		var o = t.getPageProperties();
		t.elements.report[0].innerHTML = '<table align="center" class="tblReport" style="width: '+o.width+'; height: '+o.height+'; background: none repeat scroll 0% 0% white;"><tr valign="top"><td></td></tr></table>';
	},
	loader : function(){
		var t = this;
		var o = t.getSendObjectDefaults();
		o.params = {className:'mjReportDS',methodName:'getReportData',file:t.loc.file,id:t.id,currentPageNumber:t.curPage.toString(),reportingType : t.reportingType,filter:JSON.stringify(t.filter)};
		o.cb = function(xh){
			t.createReportDiv();
			if(xh.result=='nok'){
				alert(xh.msg);
			}
			if(xh.result=='ok'){
				t.setButtonstatus('list');
				t.elements.report[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].innerHTML = xh.html;
				t.curPage = parseInt(xh.currentPageNumber);
				t.totalPage = parseInt(xh.totalPageNumber)
				t.setPageValues();
			}
			t.hide(t.elements.mask);
		};
		t.request(o);
		t.setButtonstatus('first');
		t.disabler(t.elements.btnList);
	},
	go2page : function(pageNumber){
		this.show(this.elements.mask);
		pageNumber = isNaN(parseInt(pageNumber))?1:parseInt(pageNumber);
		if(pageNumber < 1)
			pageNumber = 1;
		if(pageNumber > this.totalPage)
			pageNumber = this.totalPage;
		this.curPage = pageNumber;
		this.setPageValues();
		this.loader();
	},
	btnRefreshOnclick : function(sc,e){
		var t = this;
		t.show(t.elements.mask);
		t.elements.report[0].innerHTML = '';
		var o = t.getSendObjectDefaults();
		o.params = {className:'mjReportDS',methodName:'getReportFilter',file:t.loc.file,id:t.id};
		o.cb = function(xh){
			if(xh.result=='nok'){
				alert(xh.msg);
			}
			if(xh.result=='ok'){
				t.elements.report[0].innerHTML = xh.html;
				var tblrf = $(t.renderTo).find('.tblReportFilter');
				var tblrffoot = tblrf.find('tfoot')[0];
				var tblrfbody = tblrf.find('tbody')[0];
				tblrffoot.childNodes[0].childNodes[0].innerHTML = "<input type='button' value='Çalıştır' style='border:activeborder 1px solid;margin-top: 3px;font-family:tahoma;font-size:8pt;'/><select onchange='' name='reportingType' style='font-family:tahoma;font-size:8pt;' class='reportingType'><option value='screen'>Ekran</option><option value='excel-0'>Excel (*.xls)</option><option value='excel-1'>Excel (*.xlsx)</option></select>"; 
				t.elements.btnList = tblrffoot.childNodes[0].childNodes[0].childNodes;
				if(!t.btnEventFlag){
					t.btnEventFlag = true;
				}else{
					t.Event.remove(t.btnEventFlag);
					t.btnEventFlag = 0;
				}
				t.btnEventFlag = t.Event.add($(tblrffoot.childNodes[0].childNodes[0].childNodes[0]), "click", t.btnListOnClick, t);
				t.curPage = 0;
				t.totalPage = 0
				
				if(t.frmFilter)
					t.frmFilter.destroy();
				var dates = $(tblrfbody).find('.date-pick');
				if(dates.length>0){
					var idx=-1;
					for(var i=0;i<tblrfbody.childNodes.length;i++){
						if(idx==-1)
							if($(tblrfbody.childNodes[i]).find('.date-pick').length>0) idx=i;
					}
					t.frmFilter = new mj.form({
						renderTo : tblrfbody.childNodes[idx].childNodes[1],
						store : [],
						items : []
					});
					for(var i=0;i<dates.length;i++){
						t.frmFilter.add(
							new mj.form.dateField({
								title : '',
								epoch : true,
								labelWidth : '0px',
								dataIndex : 'Date_'+dates[i].id,
								itemStyle : 'padding-left:0px;',
								width : 80,
								defaultYear:t.scope.yil
							})
						);
						var itemId = 'Date_'+dates[i].id;
						if(itemId.indexOf('1')>0){
							if(typeof t.filterValues['Date_'+dates[i].id]=='undefined')
								t.frmFilter.setValue(eval('a={"'+itemId+'":"01/01/'+t.scope.yil+'"}'));
							else
								t.frmFilter.setValue(eval('a={"'+itemId+'":"'+t.filterValues['Date_'+dates[i].id]+'"}'));
						}
						if(itemId.indexOf('2')>0){
							if(typeof t.filterValues['Date_'+dates[i].id]!='undefined')
								t.frmFilter.setValue(eval('a={"'+itemId+'":"'+t.filterValues['Date_'+dates[i].id]+'"}'));
						}
						t.frmFilter.add(
							new mj.form.timeField({
								title : '-',
								labelWidth : '10px',
								right : true,
								itemStyle : 'width:70px;',
								dataIndex : 'Time_'+dates[i].id,
								width : 50
							})
						);
						var itemId = 'Time_'+dates[i].id;
						if(itemId.indexOf('1')>0) t.frmFilter.setValue(eval('a={"'+itemId+'":"00:00"}'));
						if(itemId.indexOf('_1')>0) t.frmFilter.setValue(eval('a={"'+itemId+'":"00:00"}'));
						if(itemId.indexOf('2')>0) t.frmFilter.setValue(eval('a={"'+itemId+'":"00:00"}'));
						if(itemId.indexOf('_2')>0) t.frmFilter.setValue(eval('a={"'+itemId+'":"23:59"}'));
					}
				}
				t.setPageValues();
			}
			t.hide(t.elements.mask);
		};
		t.request(o);
		t.setButtonstatus('first');
	},
	selectPageSizeChange : function(sc,e){
		sc.changePageProperties();
	},
	selectPageOrientationChange : function(sc,e){
		sc.changePageProperties();
	},
	btnFirstPageOnclick : function(sc,e){
		if(this.curPage > 1)
			this.go2page(1);
	},
	btnPrevPageOnclick : function(sc,e){
		if(this.curPage > 1)
			this.go2page(--this.curPage);
	},
	currentPageNumberOnchange : function(sc,e){
		var re = /^\d{1,5}?$/;
		var pageNumber = e.target.value;
		if(pageNumber != '') { 
			if(regs = pageNumber.match(re)) { 
				sc.go2page(pageNumber);
			}else { 
				e.target.value = sc.curPage;  
			} 
		}
	},
	btnNextPageOnclick : function(sc,e){
		if(this.totalPage > this.curPage)
			this.go2page(++this.curPage);
	},
	btnLastPageOnclick : function(sc,e){
		if(this.totalPage > this.curPage)
			this.go2page(this.totalPage);
	},
	btnPrintOnclick : function(sc,e){
		var t = this;
		//t.show(t.elements.mask);
		//t.ifrm = $(mj.NE(t.elements.main, {tag:'iframe',id:'myiFrm',src:location.href+'libs/mjreport/reportPrint.php',style:'display:block;left:10px;position:relative;z-index:1;'}));
		//window._varPrint = t;
		//t.checkIframeLoadingPrint();
		t.ifrmp[0].contentWindow.setContent(t.elements.report[0].innerHTML);
		t.ifrmp[0].contentWindow.focus();
		t.ifrmp[0].contentWindow.print();
		t.ifrmp[0].contentWindow.setContent('');
	},
	btnListOnClick : function(e){
		var t = this;
		t.checkFilterValues = true;
		t.reportingType = $('.reportingType', t.elements.report[0])[0].value;
		t.filter = t.getFilterValues();
		if(t.checkFilterValues){
			t.show(t.elements.mask);
			switch (t.reportingType) {
				case 'screen':
					t.loader();
				break;
				case 'excel-0':
				case 'excel-1':
				case 'pdf':
					t.disabler(t.elements.btnList);
					t.ifrm = $(mj.NE(t.elements.main, {	tag:'iframe',
														id:'myiFrm',
														src:t._url+'?className=mjReportDS&methodName=getReportData&file='+t.escape(t.loc.file)+'&id='+t.id+'&currentPageNumber='+t.curPage+'&reportingType='+t.reportingType+'&filter='+t.escape(JSON.stringify(t.filter)),
														style:'display:none;left:10px;position:relative;z-index:1;'
														}));
					window._varPrintList = t;
					t.checkIframeLoading();
				break;
			}	
		}
	},
	checkIframeLoading : function(){
		var t = this;
		if (t.ifrm[0].contentWindow && t.ifrm[0].contentWindow.document && t.ifrm[0].contentWindow.document.readyState == 'complete') {
			t.enabler(t.elements.btnList);
			t.hide(t.elements.mask);
			//t.elements.main[0].removeChild(t.ifrm[0]);
			t.ifrm = false;
			window._varPrintList = false;
			return;
		}
		window.setTimeout("_varPrintList.checkIframeLoading()", 100);      
	}/*,
	checkIframeLoadingPrint : function(){
		var t = _varPrint;
		if(t.ifrm[0].contentWindow && t.ifrm[0].contentWindow.document && t.ifrm[0].contentWindow.document.readyState == 'complete'){
			t.ifrm[0].contentWindow.setContent(t.elements.report[0].innerHTML);
			t.ifrm[0].contentWindow.focus();
			t.ifrm[0].contentWindow.print();
			t.hide(t.elements.mask);
			t.elements.main[0].removeChild(t.ifrm[0]);
			t.ifrm = false;
			window._varPrint = false;	
			return;
		}
		window.setTimeout("_varPrint.checkIframeLoadingPrint();", 100);
	}*/
};

var reportViewModule = function(){
	var $1 = mj._renderingModule;
	$1.viewer = new ReportViewer({renderTo:$1.panels.main.getBody(),loc:$1.loc,_url:$1.url,id:$1.id,scope:$1});
	$1.win.on('beforeclose',function(){
		if($1.viewer.frmFilter)
			$1.viewer.frmFilter.destroy();
	});
};
$(function(){
	reportViewModule();
});