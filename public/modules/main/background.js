var cBackGround = function(){
	var t=['<div class="thumb-wrap"">',
	'<div class="thumb"><img src="{url}" title="{name}"></div>',
	'</div>'];
	var $1 = mj._renderingModule;
	var view3 = new mj.view({
		renderTo : $1.panels.main.getBody(),
		store : new mj.store({
			url : $1.url,
			params : {
				className : 'user',
				methodName : 'getImageView',
				myRoot : '../../',
				node : 'images/Desktop',
				fileTypes : '*',
				event : 'getImageView'
			}
		}),
		pbar : new mj.pager({limit:10,pos:'bottom'}),
		tpl : new mj.template(t),
		selector : 'div.thumb-wrap',
		multiSelect : true
	});
	view3.on('itemclick',function(sel){
		d.changeBackground(sel.selections[0].store.fileName);
	})
	view3.load();
};
$(function(){
	cBackGround();
});