baseObj = function(config){
	//mj.apply(this, config);
	//this.init();
	baseObj.superclass.constructor.call(this, config);
};
baseObj.prototype = {
	/*cacheParam : function(store){
		var p = store.params, cp = {};
		for(var i in p)
			if(typeof p[i] != 'function'&&i!='toJSONString')
				cp[i] = p[i];
		return cp;
	},
	setParam : function(store,params){
		store.params = {};
		return mj.apply(store.params,params);
	},
	deleteLoad : function(obj,event){
		var cp = obj.cacheParam(obj.stores.grid);
		mj.apply(obj.stores.grid.params,{methodName : (event?event:'deleteRecord'), table : obj.table, id : obj.grids.records.selectedRow && obj.grids.records.selectedRow.data ? parseInt(obj.grids.records.selectedRow.data.id) : 0,oldParams : encodeURIComponent(JSON.stringify(cp)), mId:obj.mId}); 
		obj.stores.grid.load();
		obj.setParam(obj.stores.grid,cp);
	},
	clearForms : function(_forms){
		if(!_forms)
			_forms = this.forms;
		for(var f in _forms)
			if(typeof this.forms[f]!='function' && typeof this.forms[f].clear == 'function')
				this.forms[f].clear();
		if(this.win && typeof this.win.setTitle == 'function')
			this.win.setTitle(this.winTitle);
	},
	clearPageBar : function(g){
		var pb = g.pbar.params, p = g.store.params;
		p.current = pb.current = 1;
		p.start = pb.start = 0;
	},
	setDetailStore : function(g,id){
		g.store.params.id = id;
		this.clearPageBar(g);
		g.load();
	},
	messageShow : function(obj,tit,mes){
		new mj.message({
			title : tit,
			msg : mes,
			wM : obj.win?obj.win.manager:d.vM,
			modal : true
		});
	},
	btnDisabler : function(){
		var btns = this.buttons;
		if(btns.editBtn&&typeof btns.editBtn.setDisable == 'function')
			btns.editBtn.setDisable();
		if(btns.deleteBtn&&typeof btns.deleteBtn.setDisable == 'function')
			btns.deleteBtn.setDisable();
	},
	onWindowBeforeClose : function(){
		return !this.ajaxActive;
	},
	bindFormLoad : function(){
		var t = this, formField = false;
		if(t.forms && t.forms.main){
			var form = t.forms.main;
			for(var i=0,l=form.items.length;i<l;i++)
				if((formField = form.items[i]).isKey){
					form.pkField = formField;
					formField.on('focus', function(field){t._onKeyFieldFocus(field);}, t);
					formField.on('blur', function(field){t._onKeyFieldBlur(field);}, t);
				}
			form.store.on('load', function(){
				if(form.store.isEmpty){
					t.createNewRecord();
					if(form._beforeLoadParams)
						form.setValue(form._beforeLoadParams);
				}
				form._beforeLoadParams = false;
			}, t);
		}
	},
	_onKeyFieldBlur : function(field){
		var t = this, form = field.form, val = field.getValue();
		//db kontrol et, kay�t varsa y�kle yoksa yeni kay�t moduna ge�
		if(val!='' && val!=field._oldValue)
			t.editRecord(field.getValue(), field.dataIndex);
	},
	_onKeyFieldFocus : function(field){
		var t = this, form = field.form;
		field._oldValue = field.getValue();
	},
	createNewRecord : function(ev){
		var t = this;
		if(t.win && typeof t.win.setTitle == 'function')
			t.win.setTitle(t.winTitle+' - ' +mj.lng.glb.addRecord);
		t.grids.records.clearSelectedRow();
		t.btnDisabler();
		t.recordMode = ev&&ev!=''?ev:'addRecord';
		t.forms.main.clear();
		//sor!!!
		//t.clearItems(t.forms.main.items);
		t.tabPanels.main.setActive(0);
	},
	_storeOnLoad : function(data){
		if(this != arguments.callee._oScope){
			return arguments.callee.apply(arguments.callee._oScope, arguments);
		};
		this.recordMode = data.data.length>0?'editRecord':'addRecord';
		if(this.win && typeof this.win.setTitle == 'function')
			this.win.setTitle(this.winTitle+' - ' +mj.lng.glb[this.recordMode]);
	},
	checkForm : function(f){
		var r = {}, fn = '', p = -1, ofn = '', rw = false, ret = true, raw = false, item , i=0, fItem;
		while(i<f.items.length && ret){
			fItem = f.items[i++];
			if(typeof fItem.notEmpty!='undefined'&&fItem.notEmpty==true){
				fn = fItem.dataIndex;
				p = fn.indexOf('_lookup_');
				ofn = p == -1?fn:fn.substr(0,p);
				item = f.getField.call(f, ofn);
				rw = raw || p>-1;
				if(rw)
					r[fn] = item.getElValue();
				else
					r[fn] = item.getValue();
				if(r[fn]==''||r[fn]==null){
					ret = false;
					if(typeof item.markInvalid=='function')
						item.markInvalid(item.invalidText);
					this.messageShow(this,mj.lng.glb.error,mj.lng.glb.formSaveWarning +'<br>'+ item.title +'<br>'+ mj.lng.glb.bosAlan);
				}else
					if(typeof item.clearInvalid=='function')
						item.clearInvalid();
			}
		};
		return ret;
	},
	editRecord : function(recordId, dataIndex){//dataIndex verilirse recordId kolonunda gelen de�er ID yerine dataIndex ile e�lenecek. �r: editRecord(65, 'sicil')
		var t = this, form = this.forms.main;
		if(typeof t.customEditHandler == 'function')
			t.customEditHandler.apply(this, [recordId, dataIndex]);
		else{
			form._beforeLoadParams = {};
			form._beforeLoadParams[dataIndex] = recordId;
			var q = {};
			q[t.table+'.'+dataIndex] = recordId;
			form.store.url = t.url;
			form.store.params = {
				className : t.stores.grid.params.className||'',
				methodName : t.stores.grid.params.methodName||'',
				event : t.formLoadEvent || t.stores.grid.params.event,
				table : t.table,
				q : encodeURIComponent(JSON.stringify(q))
			};
			form.load();
			form.store.onOnce('load',t._storeOnLoad);
		}
	},
	loadFilteredRecords:function(recordId, dataIndex,cb){
		var t = this, g = this.stores.grid;
		if(typeof g =="object"){
			var q = {};
			var _arrdi=dataIndex.split("|");
			var _arrrid=recordId.split("|");
			for(var i=0;i<_arrdi.length;i++)
				q[t.table+'.'+_arrdi[i]] = _arrrid[i];
			g.params.q=encodeURIComponent(JSON.stringify(q));
			g.load();
		}
	},
	_printPreviewLoadTrigger : function(store){
		store.params.limit = store._limitBackup;
		store.params.current = store._currentBackup;
		store.mon('load', this._printPreviewLoadTrigger);
		this._printPreview(this._printGrid);
	},
	_printLoadTrigger : function(){
		store.params.limit = store._limitBackup;
		store.params.current = store._currentBackup;
		store.mon('load', this._printLoadTrigger);
		this._print(this._printGrid);
	},
	printPreview : function(grid, fromPrintFn){
		if(!grid)
			grid = this.grids.records;
		if(parseInt(grid.store.recordCount)!=grid.store.data.length){
			grid.store._limitBackup = grid.store.params.limit;
			grid.store._currentBackup = grid.store.params.current;
			grid.store.params.limit = parseInt(grid.store.recordCount);
			grid.store.params.current = 1;
			this._printGrid = grid;
			grid.store.on('load', fromPrintFn ? this._printLoadTrigger : this._printPreviewLoadTrigger, this);
			grid.store.load();
		}else{
			var nw = mj.newWindow({title:'Verimot'}), nb = $(nw.document.body);
			$(nw.document.body.previousSibling).append('<link href="'+this.printCssPath+'" type="text/css" rel="stylesheet"/>');

			mj.NE(nb, {cls:'mj-heading-1', html:this.reportTitle});
			grid.copyContent(nb);
			return nw;
		}
	},
	print : function(){
		var t = this;
		var nw = t.printPreview(null, true);
		if(nw){
			nw.print();
			nw.close();
		}
	},
	_printPreview : function(grid){
		if(!grid)
			grid = this.grids.records;
		var nw = mj.newWindow({title:'Verimot'}), nb = $(nw.document.body);
		$(nw.document.body.previousSibling).append('<link href="'+this.printCssPath+'" type="text/css" rel="stylesheet"/>');

		mj.NE(nb, {cls:'mj-heading-1', html:this.reportTitle});
		grid.copyContent(nb);
		return nw;
	},
	_print : function(){
		var t = this;
		var nw = t.printPreview();
		nw.print();
		nw.close();	
	},
	
	_printLoadTriggerFromServer : function(store,data){
		store.mon('load', this._printLoadTriggerFromServer);
		var nw = mj.newWindow({title:'Kaitek'}), nb = $(nw.document.body);
		$(nw.document.body.previousSibling).append('<link href="'+this.printCssPath+'" type="text/css" rel="stylesheet"/>');

		clearTimeout(this.sto);
		mj.NE(nb, {cls:'mj-heading-1', html:this.reportTitle});
		nb.append(data.records);
		if(store.params.print==true&&data.records.length>0){
			nw.print();
			nw.close();	
		}
		if(data.records.length==0) nw.close();
		//console.dir(this);
		store.trigger('printok', store);
	},
	printFromServer:function(store,preview){
		store.on('load', this._printLoadTriggerFromServer, this, preview);
		store.load();
		var t=this;
		t.to=false;
		t.sto=setTimeout(function(){t.to=true;store.mon('load', t._printLoadTriggerFromServer);alert("Hata!Tekrar denenyiniz...");store.trigger('printok', store);},3000);
	},
	
	iframeLoader:function(src){
		var t=this;
		t.ifrm = $(mj.NE(t.win.getBody(), {	tag:'iframe',
									id:'myiFrm',
									src:t.url+'?'+src,
									style:'display:none;left:10px;position:relative;z-index:1;'
									}));
		window._varLoaderFrameScope = t;
		t.checkLoaderIframeLoading();
	},
	checkLoaderIframeLoading : function(){
		var t = this;
		if (t.ifrm[0].contentWindow && t.ifrm[0].contentWindow.document && t.ifrm[0].contentWindow.document.readyState == 'complete') {
			t.ifrm = false;
			window._varLoaderFrameScope = false;
			t.win.trigger('iframeloaded',t);
			return;
		}
		window.setTimeout("_varLoaderFrameScope.checkLoaderIframeLoading()", 100);      
	},
	buttons : false,
	forms : false,
	grids : false,
	panels : false,
	stores : false,
	tabPanels : false,
	tabs : false,
	windows : false,
	wins : false,
	formLoadEvent : false,
	affectRecord : false,
	customClose : false,*/
	gridLoaded: true,
	filter:false,
    recordsperpage: 25,
	windowButtons: false,
	init : function(){
		var t = this;
		t._event=false;
		t.windowButtons= {
			add: true,
			delete: true,
			edit: true,
			exit: false,
			filter:false,
			formSubmit: true,
			saveAsCopy: false,
			formCancel: true
		};
		if (t.scope && t.scope.win) {
			t.scope.win.obj = t;
			t.win = t.scope.win;
			t.winTitle = t.scope.win.title;
			//t.waitMask = t.win.waitMask;
		}
		t.stores = {};
		var routeName = t.modulename + '-showall';
		try {
			var route = Routing.getRoute(routeName);
			var data = {
				'pg': 1,
				'lm': t.recordsperpage
			};
			var url = Routing.generate(routeName, data);
			t.stores.main = new mj.store({
				url: url,
				method: route.methods[0],
				data: t.data,
				modulename:t.modulename,
				params:{},
				failure:function(a,b){
					//console.log(a);
					//console.log(b);
				}
			});
			t.stores.main.on('beforeload', t._onMainStoreBeforeLoad, t);
			t.stores.main.on('load', t._onMainStoreLoad, t);
		} catch (error) {
			console.log(error);
		}
        
		if (typeof t.storeConfig != "undefined") {
			for (_objParamStore in t.storeConfig) {
				var _itemStore=t.storeConfig[_objParamStore];
				//var _routeName = t.modulename + '-' + _itemStore.routeName;
				var _routeName = _itemStore.routeName;
				var _route = Routing.getRoute(_routeName);
				var _data = {
					'pg': 1,
					'lm': _itemStore.limit?_itemStore.limit:t.recordsperpage,
					'table':_itemStore.table,
					'fieldId':(_itemStore.fieldId?_itemStore.fieldId:'code'),
					'fieldDisplay':(_itemStore.fieldDisplay?_itemStore.fieldDisplay:'code')
				};
				if(_itemStore.filters){
					_data.filters=_itemStore.filters;
				}
				if(_itemStore.options){
					_data.options=JSON.stringify(_itemStore.options);
				}
				var _url = Routing.generate(_routeName, _data);
				t.stores[_objParamStore]=new mj.store({
					url: _url,
					method: _route.requirements._method,
					modulename:_itemStore.modulename?_itemStore.modulename:t.modulename,
					routename:_routeName,
					routedata:_data
				});
			}
		}

		t.tabPanels = {};
		t.tabPanels.main = new mj.tab({
			renderTo: t.scope,
			activeTab: t.gridLoaded ? 1 : 0,
			border: false,
			hideHeader: true,
			items: [{
				closable: false
			}, {
				closable: false
			}]
		});
		t.tabPanels.main.on('tabchange', t._onParentTabChange, t);

		t.tabs = {};
		t.tabs.main = {
			form: t.tabPanels.main.tabs[0],
			grid: t.tabPanels.main.tabs[1]
		};
		t.on('activate-tab-form', t._onActivateTabForm);
		t.on('activate-tab-grid', t._onActivateTabGrid);

		if (typeof t.gridConfig !== "undefined") {
			t._setGridConfig(t.gridConfig);
		}
		
		if (typeof t.formConfig !== "undefined") {
			t._setFormConfig(t.formConfig);
		}
		t.waitMask = $(mj.NE(t.win.getBody(),{
			cls:'mj-page-wait-mask mj-opacity-8',
			style:'display:none;width:100%;height:100%;',
			html:'<table width="100%" height="100%"><tr><td align="center" valign="middle"><img src="'+mj.glb.imagePath+'ajax-loader.gif"/><br/><br/><span class="mj-page-wait-title">'+mj.lng.glb.pleaseWait+'</span></td></tr></table>'
		}));
		t.waitMask.renderTo = t.win.getBody();
		t.win.addRelated(t.waitMask);
		t.win.on('afterresize',function(){
			t.tabPanels.main.doTab();
		});
		t.win.on('close',function(){
			if(t.windowButtons.filter){
				t.filter.win.destroy();
			}
			if(t.grids&&t.grids.main){
				t.grids.main.destroy();
			}
			if(t.forms&&t.forms.main){
				t.forms.main.destroy();
			}
		});
	},
	waitMaskResize : function(){
		var wm = this.waitMask;
		wm.css({'width':wm.renderTo.width(), 'height':wm.renderTo.height()});
	},
	waitMaskShow : function(){
		this.waitMask[0].style.display='block';
	},
	waitMaskHide : function(){
		this.waitMask[0].style.display='none';
	},
	_onActivateTabForm: function() {
	},
	_onActivateTabGrid: function() {
	},
	_onButtonAddClick: function() {
	this.newRecord();
	},
	_onButtonauditSwitchClick: function() {
		var t = this;
		if(t._auditShowDeleted){
				t._auditShowDeleted = false;
				t.buttons.auditSwitch.setIconCls('fa-trash');
				t.buttons.auditSwitch.setAlt(mj.lng.glb.auditShowDeleted);
		} else {
				t._auditShowDeleted = true;
				t.buttons.auditSwitch.setIconCls('fa-heart-o');
				t.buttons.auditSwitch.setAlt(mj.lng.glb.auditHideDeleted);
		}
	},
	_onButtonDeleteClick: function() {
			this.deleteRecord();
	},
	_onButtonEditClick: function() {
			this.editSelectedRecord();
	},
	_onButtonFilterClick: function() {
        this.filter.show();
	},
	_onButtonExitClick: function() {
			this.exit();
	},
	_onButtonFormCancelClick: function() {
			this.reset();
			this.activateTabGrid();
	},
	_onButtonFormSubmitClick: function() {
		this.save();
	},
	_onButtonFormSubmitAsCopyClick: function() {
		this.forms.main.setValue({"id":""});
		this.save();
	},
	_onButtonSaveAsXLSClick:function(){
		var t = this;
		if (t.modulename && typeof Routing !== 'undefined') {
			t.waitMaskShow();
			var routeName = t.modulename + '-add';
			var route = Routing.getRoute(routeName);
			var pagerParams = {'pg':1,'lm':5000};
			var url = Routing.generate(routeName, pagerParams);
			if (route && route.methods && route.methods.length>0 && route.methods[0]) {
				$.ajax({
					url: url,
					method: "POST",
					headers: { 
						"Content-Type": "application/json",
						"X-HTTP-Method-Override": route.methods[0] 
					},
					data: JSON.stringify(t.__data__),
					dataType: 'binary',
					processData: false,
					success: function(data, textStatus, xhr) {
						//t.activateTabGrid();
						if(textStatus==='success'){
							var filename = "";
							var disposition = xhr.getResponseHeader('Content-Disposition');
							if (disposition && disposition.indexOf('attachment') !== -1) {
								var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
								var matches = filenameRegex.exec(disposition);
								if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
							}
							var type = xhr.getResponseHeader('Content-Type');
							var blob = typeof File === 'function'
								? new File([data], filename, { type: type })
								: new Blob([data], { type: type });
							if (typeof window.navigator.msSaveBlob !== 'undefined') {
								// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
								window.navigator.msSaveBlob(blob, filename);
								t.waitMaskHide();
							} else {
								var URL = window.URL || window.webkitURL;
								var downloadUrl = URL.createObjectURL(blob);
								if (filename) {
									// use HTML5 a[download] attribute to specify filename
									var a = document.createElement("a");
									// safari doesn't support this yet
									if (typeof a.download === 'undefined') {
										window.location = downloadUrl;
										t.waitMaskHide();
									} else {
										a.href = downloadUrl;
										a.download = filename;
										document.body.appendChild(a);
										a.click();
										t.waitMaskHide();
									}
								} else {
									window.location = downloadUrl;
									t.waitMaskHide();
								}
								setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
							}
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						t.waitMaskHide();
						if(xhr.responseJSON&&xhr.responseJSON.message){
							mj.message({
								title: mj.lng.glb.error,
								msg: xhr.responseJSON.message,
								type: "error",
								html: true
							});
						}
					},
					complete: function(jqXHR, textStatus) {
						t.waitMaskHide();
					}
				});
			}
		}
	},
	_onFormStoreLoad: function() {
			var t = this;
			//-- Timeout vermeden activateTabForm yapıldığında t.stores.form için load listener'larının bir kısmı çalışmıyor.
			// 24.04.2017'de $.eachR içinde yapılan değişiklik sonrası setTimeout kapatıldı. Sorun devam ederse tekrar açılacak.
			//setTimeout(function() {
					t.activateTabForm();
			//}, 10);
	},
	_onMainGridRowClick: function(grid, rowIndex) {
			var t = this;
			if (t.buttons.edit) {
					t.buttons.edit.setEnable();
			}
			if (t.buttons.delete) {
					t.buttons.delete.setEnable();
			}
	},
	_onMainStoreBeforeLoad: function() {
	this.stores.main.params.menuId = this.id;
	this.waitMaskShow();
	},
	_onMainStoreLoad: function() {
	var t = this;
	t.trigger('mainstoreload', t);
	setTimeout(function() {
		if(t.buttons){
			if (t.buttons.edit) {
				t.buttons.edit.setDisable();
			}
			if (t.buttons.delete) {
				t.buttons.delete.setDisable();
			}
		}
					//if(t.extras && typeof t.extras['documentId'] == 'undefined' && typeof t.extras['recordId'] != 'undefined'){
					//    t.grids.main.selectRow(t.grids.main, 0, false);
					//    t.editSelectedRecord();
					//}
	}, 10);
	if(t._event!==false){
		if(t._event==='save'){
			t.activateTabGrid();
		}
	}
	this.waitMaskHide();
			return true;
	},
    _onParentTabChange: function(tab, activeItem){
        //var t = this;
        //if(activeItem == t.tabs.main.grid) {
        //    if (t.buttons.edit)
        //        t.buttons.add.show();
        //    if (t.buttons.delete)
        //        t.buttons.delete.show();
        //    if (t.buttons.edit)
        //        t.buttons.edit.show();
        //    if (t.buttons.exit)
        //        t.buttons.exit.show();
        //    if (t.buttons.pagerButtons)
        //        t.buttons.pagerButtons.show();
        //} else {
        //    if (t.buttons.edit)
        //        t.buttons.add.hide();
        //    if (t.buttons.delete)
        //        t.buttons.delete.hide();
        //    if (t.buttons.edit)
        //        t.buttons.edit.hide();
        //    if (t.buttons.exit)
        //        t.buttons.exit.hide();
        //    if (t.buttons.pagerButtons)
        //        t.buttons.pagerButtons.hide();
        //}
        //if(activeItem == t.tabs.main.form){
        //    if (t.buttons.formCancel)
        //        t.buttons.formCancel.show();
        //    if (t.buttons.formSubmit)
        //        t.buttons.formSubmit.show();
        //} else {
        //    if (t.buttons.formCancel)
        //        t.buttons.formCancel.hide();
        //    if (t.buttons.formSubmit)
        //        t.buttons.formSubmit.hide();
        //}
        ////setTimeout(function(){
        ////    t.win.body.perfectScrollbar('update');
        ////    // Bazı client'larda scrollbar update gerçekleşmemesi halinde alttaki süre arttırılabilir.
        ////}, 50);
    },
    _onReset: function(_forms, _data) {
        var t = this;
        if (typeof _data != "undefined") {
            t.stores.main.preloaded = true;
            t.stores.main.data = _data;
        }
        //t.stores.main.load();
	},
	_setGridConfig: function(gridConfig){
		const t = this;
		t.gridConfig=gridConfig;
		if( mj.getIndex(t.gridConfig.cm, 'dataIndex', 'version') === -1 ) {
			var lastCol = t.gridConfig.cm[t.gridConfig.cm.length-1];
			if(lastCol.hide && lastCol.header === ''){
				t.gridConfig.cm.pop();
			}
			t.gridConfig.cm.push({header: mj.lng.glb.revision, width: 25, dataIndex: 'version', hide: true});
			t.gridConfig.cm.push({header: '', width: 5, hide: true});
		}

		t.grids = {};
		t.grids.main = new mj.grid(mj.apply({
			win: t.win,
			renderTo: t.tabs.main.grid.getBody(),
			store: t.stores.main,
			entity: t.modulename,
			pbar: new mj.pager({
				pos: 'bottom',
				beforePageText: false,
				limit: 25
			}),
			fitToParent: true
		}, t.gridConfig));
		t.on('reset', t._onReset, t);

		t.grids.main.on('rowclick', t._onMainGridRowClick, t);

		t.grids.main.on('rowdblclick', t.editSelectedRecord, t);
	},
	_setFormConfig: function(formConfig) {
		const t = this;
		t.stores.form = new mj.store({
			params:{},
			failure:function(a,b){
				//console.log(a);
				//console.log(b);
			}
		});
		t.stores.form.on('load', t._onFormStoreLoad, t);

		t.forms = {};
		t.forms.main = new mj.form(mj.apply({
			renderTo: t.tabs.main.form.getBody(),
			modulename: t.modulename,
			store: t.stores.form
		}, formConfig));
		if(t.windowButtons!==false){
			t.initDefaultButtons();
		}
	},
    activateTabForm: function() {
        this.tabs.main.form.activate();
        this.trigger('activate-tab-form');
    },
    activateTabGrid: function() {
        this.tabs.main.grid.activate();
        this.trigger('activate-tab-grid');
    },
    deleteRecord: function() {
        var t = this,
            _f = this.forms;
        var selectedRecord = t.getSelectedRecord();
        if (selectedRecord !== false) {
			mj.message({
				title: mj.lng.glb.winTitleOnay,
				modal : true,
				msg: mj.lng.glb.delQuestion,
				buttons:['NO','YES'],
				cb:function(el,btn){
					if(btn=='YES'){
						var id = selectedRecord.id;
						var version = selectedRecord.version;
						var routeName = t.modulename + '-del';
						var route = Routing.getRoute(routeName);
						var pagerParams = (t.grids && t.grids.main && t.grids.main.pbar) ? {
							'pg': t.grids.main.pbar.current,
							'lm': t.grids.main.pbar.limit
						} : {};
						var url = Routing.generate(routeName, id ? mj.apply(pagerParams, {
							'id': id,
							'v': version
						}) : pagerParams);
						if (route && route.methods && route.methods.length>0 && route.methods[0]) {
							t.waitMaskShow();
							t._event='delete';
							var data = {'menuId': t.id};
							$.ajax({
								url: url,
								method: "POST",
								dataType: 'json',
								data: JSON.stringify(data),
								headers: { 
									"Content-Type": "application/json",
									"X-HTTP-Method-Override": route.methods[0] 
								},
								success: function(data, textStatus, jqXHR) {
									t.reset(_f, data);
								},
								error: function(jqXHR, textStatus, errorThrown) {
									t.waitMaskHide();
									mj.message({
										title: mj.lng.glb.error,
										msg: jqXHR.responseJSON.message + "<br>" + jqXHR.responseJSON.file,
										type: "error",
										html: true
									});
								},
								complete: function(jqXHR, textStatus) {
									//t.waitMaskHide();
								}
							});
						}
					}
					el.window.close();
				}
			});
        }
    },
    editData: function(data) {
        this.forms.main.store.preloaded = true;
        this.forms.main.store.data = data;
        this.forms.main.store.load();
    },
    editSelectedRecord: function() {
        var selectedRecord = this.getSelectedRecord();
        if (selectedRecord !== false)
            this.editData(selectedRecord);
    },
    exit: function() {
        this.win.close();
    },
    getSelectedRecord: function() {
        return (this.grids && this.grids.main && this.grids.main.selectedRow && this.grids.main.selectedRow.data) ? this.grids.main.selectedRow.data : false;
    },
    initDefaultButtons: function() {
        var t = this;
		t.buttons = {};
		var obj_btn_container;
		if (typeof t.gridConfig !== "undefined") {
			obj_btn_container=t.grids.main.pbar.tbar;
			if(mj.getIndex(t.grids.main.cm,'filter','single')>-1){
				t.windowButtons.filter=true;
			}else if(mj.getIndex(t.grids.main.cm,'filter','between')>-1){
				t.windowButtons.filter=true;
			}else{
				t.windowButtons.filter=false;
			}
		}else{
			obj_btn_container=t.win;
		}
        //if (t.audit)
        //    t.buttons.auditSwitch = obj_btn_container.addButton({
        //        alt: mj.lng.glb.auditShowDeleted,
        //        iconcls: 'fa-trash',
        //        cls: 'btn-sm left',
        //        scope: t,
        //        handler: t._onButtonauditSwitchClick
        //    });
        if (t.windowButtons.exit){
            t.buttons.exit = obj_btn_container.addButton({
                alt: mj.lng.glb.exit,
                iconcls: 'fa-sign-out',
                scope: t,
                handler: t._onButtonExitClick
			});
		}
        if (t.windowButtons.add){
			if(typeof obj_btn_container.addSplitter==='function'){
				obj_btn_container.addSplitter();
			}
            t.buttons.add = obj_btn_container.addButton({
                alt: mj.lng.glb.add,
                cls: 'btn-sm left',
                iconCls: 'mj-add',
                scope: t,
                handler: t._onButtonAddClick
			});
		}
        if (t.windowButtons.delete){
			t.buttons.delete = obj_btn_container.addButton({
                alt: mj.lng.glb.del,
                cls: 'btn-sm left',
                iconCls: 'mj-delete',
                disabled: true,
                scope: t,
                handler: t._onButtonDeleteClick
			});
		}
        if (t.windowButtons.edit){
            t.buttons.edit = obj_btn_container.addButton({
                alt: mj.lng.glb.edit,
                cls: 'btn-sm left',
                iconCls: 'mj-edit',
                disabled: true,
                scope: t,
                handler: t._onButtonEditClick
			});
		}
		if (t.windowButtons.filter){
			if(typeof obj_btn_container.addSplitter==='function'){
				obj_btn_container.addSplitter();
			}
            t.buttons.filter = obj_btn_container.addButton({
                alt: mj.lng.glb.filter,
                cls: 'btn-sm left',
                iconCls: 'mj-filter',
                scope: t,
                handler: t._onButtonFilterClick
			});
			t.filter = new mj.dataFilter({
				grid : t.grids.main,
				button : t.buttons.filter,
				height : 400,
				width : 500,
				routeName : t.modulename + '-showall',
				recordsperpage: t.recordsperpage
			});
		}
        if (t.windowButtons.formSubmit){
            t.buttons.formSubmit = t.forms.main.addButton({
                alt: mj.lng.glb.save,
                cls: 'btn-sm left',
                iconCls: 'mj-save',
                //invisible: true,
                scope: t,
                handler: t._onButtonFormSubmitClick
			});
		}
		if (t.windowButtons.saveAsCopy){
            t.buttons.formSubmit = t.forms.main.addButton({
                alt: mj.lng.glb.save,
                cls: 'btn-sm left',
                iconCls: 'mj-add',
                //invisible: true,
                scope: t,
                handler: t._onButtonFormSubmitAsCopyClick
			});
		}
        if (t.windowButtons.formCancel){
            t.buttons.formCancel = t.forms.main.addButton({
                alt: mj.lng.glb.cancel,
                cls: 'btn-sm left',
                iconCls: 'mj-list',
                //invisible: true,
                scope: t,
                handler: t._onButtonFormCancelClick
            });
		}
        //if (typeof t.gridConfig != "undefined")
        //    t.buttons.pagerButtons = $('.btn-pager', t.win.buttonCnt);
	},
	messageShow : function(obj,tit,mes){
		new mj.message({
			title : tit,
			msg : mes,
			wM : obj.win?obj.win.manager:d.vM,
			modal : true
		});
	},
    newRecord: function() {
        this.resetForms();
        this.activateTabForm();
    },
    reset: function(_forms, _data) {
        this.resetForms(_forms, _data);
		this.trigger('reset', _forms, _data);
		this.stores.main.load();
    },
    resetForms: function(_forms, _data) {
        var t = this;
        if (!_forms) {
            _forms = t.forms;
        }
        for (var f in _forms) {
            if (typeof t.forms[f] != 'undefined' && typeof t.forms[f] != 'function' && typeof t.forms[f].clear == 'function') {
                t.forms[f].clear();
            }
        }
        if (t.win && typeof t.win.setTitle == 'function') {
            t.win.setTitle(t.winTitle);
        }
    },
    save: function() {
        var t = this,
            _f = this.forms,
            f = this.forms.main;
        t.trigger('beforesave');
        if (f.modulename && typeof Routing !== 'undefined') {
            //if (f.validate()) {
                var id = f.getValue('id').id;
                var versionField = f.getField('version');
                var version = typeof versionField != "undefined" ? f.getValue('version').version : 0;
                var mode = id ? 'update' : 'add';
                var routeName = f.modulename + '-' + mode;
                var route = Routing.getRoute(routeName);
                var pagerParams = (t.grids && t.grids.main && t.grids.main.pbar) ? {
                    'pg': t.grids.main.pbar.current,
                    'lm': t.grids.main.pbar.limit
                } : {};
                var url = Routing.generate(routeName, id ? mj.apply(pagerParams, {
                    'id': id,
                    'v': version
                }) : pagerParams);
                var data = f.getValue();
                data.menuId = t.id;
                if (route && route.methods && route.methods.length>0 && route.methods[0]) {
					t.waitMaskShow();
					t._event='save';
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: JSON.stringify(data),
						headers: { 
							"Content-Type": "application/json",
							"X-HTTP-Method-Override": route.methods[0] 
						},
                        dataType: 'json',
                        success: function(data, textStatus, jqXHR) {
                            t.reset(_f, data);
							//t.activateTabGrid();
							//t.waitMaskHide();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
													t.waitMaskHide();
													mj.message({
														title: mj.lng.glb.error,
														msg: jqXHR.responseJSON.message + "<br>" + jqXHR.responseJSON.file,
														type: "error",
														html: true
													});
                        },
                        complete: function(jqXHR, textStatus) {
							//t.waitMaskHide();
                        }
                    });
                }
            //}
        }
	}
};
mj.extend(baseObj, mj.component);