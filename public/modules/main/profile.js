var profileModule = function(){
	var $1 = mj._renderingModule;

	$1.tabPanels.main = new mj.tab({
		renderTo : $1.panels.main.getBody(),
		activeTab : 0,
		border : false,
		tabWidth : 150,
		maxTitle : 20,
		items :[
			{
				title: mj.lng.titles.modules.profile.tab0,
				iconCls:'tabs',
				closable:false
			},{
				title: mj.lng.titles.modules.profile.tab1,
				iconCls:'tabs',
				closable:false
			}
		]
	});

	$1.tabs.main = {
		pb : $1.tabPanels.main.tabs[0],
		psw : $1.tabPanels.main.tabs[1]
	};
	
	$1.forms.pb=new mj.form({
		renderTo : $1.tabs.main.pb.getBody(),
		items : [
			new mj.form.numberField({
				title : mj.lng.glb.id,
				dataIndex : 'id',
				hidden : true,
				labelWidth : '130px',
				width : 150
			}),
			new mj.form.textField({
				title : mj.lng.glb.userName,
				dataIndex : 'loginId',
				labelWidth : '130px',
				width : 150
			}),
			new mj.form.textField({
				title : mj.lng.glb.name,
				dataIndex : 'name',
				labelWidth : '130px',
				width : 150
			}),
			new mj.form.textField({
				title : mj.lng.glb.surname,
				dataIndex : 'surName',
				labelWidth : '130px',
				width : 150
			}),
			new mj.form.mailField({
				title : mj.lng.glb.email,
				dataIndex : 'email',
				labelWidth : '130px',
				width : 250
			})
		],
		buttons : [
			{title:mj.lng.glb.close,handler:function(){$1.win.close();}},
			{title:mj.lng.glb.save,handler:function(){
				var f = $1.forms.pb;
				if(f.items.getIndex('invalid',true)==-1){
					var idx = f.items[0].getValue();
					f.submit({
						url : $1.url,
						params : {className : 'kkController', methodName : 'editRecord', table : $1.table, id : idx},
						success : function(data){
							$1.messageShow($1,mj.lng.glb.info,data.msg);
						},
						failure : function(data){
							$1.messageShow($1,mj.lng.glb.error,data.msg);
						},
						encoded : true
					});
				}else
					$1.messageShow($1,mj.lng.glb.error,mj.lng.glb.formSaveWarning);
			}}
		]
	});
	$1.forms.psw = new mj.form({
		renderTo : $1.tabs.main.psw.getBody(),
		items : [
			new mj.form.passField({
				title : mj.lng.glb.oldPass,
				regEx : /^(?=.*\d)(?=.*[a-z]).{4,15}$/,
				dataIndex : 'passOld',
				width : 200,
				labelWidth : '130px'
			}),
			new mj.form.passField({
				title : mj.lng.glb.newPass,
				regEx : /^(?=.*\d)(?=.*[a-z]).{4,15}$/,
				dataIndex : 'passNew',
				width : 200,
				labelWidth : '130px'
			}),
			new mj.form.passField({
				title : mj.lng.glb.newPassAgain,
				regEx : /^(?=.*\d)(?=.*[a-z]).{4,15}$/,
				dataIndex : 'passNewr',
				width : 200,
				labelWidth : '130px'
			})
		],
		buttons : [
			{title:mj.lng.glb.close,handler:function(){$1.win.close();}},
			{title:mj.lng.glb.save,handler:function(){
				var f = $1.forms.psw;
				if(f.items.getIndex('invalid',true)==-1){
					var same = (f.items[1].getValue()==f.items[2].getValue());
					if(!same)
						$1.messageShow($1,mj.lng.glb.error,mj.lng.glb.passNotMatch);
					else{
						var idx = $1.forms.pb.items[0].getValue();
						f.items[0].clearValidate();
						f.items[0].setValue(hex_md5(f.items[0].getValue()));
						
						f.items[1].clearValidate();
						f.items[1].setValue(hex_md5(f.items[1].getValue()));
						
						f.items[2].clearValidate();
						f.items[2].setValue(hex_md5(f.items[2].getValue()));
						
						f.submit({
							url : $1.url,
							params : {className: 'user', methodName	: 'changePasswordProfile', table : $1.table, id : idx},
							success : function(data){
								new mj.message({
									title : mj.lng.glb.info,
									msg : data.msg,
									modal : true,
									wM : $1.win.manager,
									cb : function(el,btn){
										f.items[0].setValue('');
										f.items[1].setValue('');
										f.items[2].setValue('');
										el.window.close();
									}
								});
							},
							failure : function(data){
								new mj.message({
									title : mj.lng.glb.error,
									msg : data.msg,
									modal : true,
									wM : $1.win.manager,
									cb : function(el,btn){
										f.items[0].setValue('');
										f.items[1].setValue('');
										f.items[2].setValue('');
										el.window.close();
									}
								});
							},
							encoded : true
						});
						f.items[0].setValue('');
						f.items[1].setValue('');
						f.items[2].setValue('');
						f.items[0].setOldValidate();
						f.items[1].setOldValidate();
						f.items[2].setOldValidate();
					}
				}else
					$1.messageShow($1,mj.lng.glb.error,mj.lng.glb.formSaveWarning);
			}}
		]
	});
	if(typeof $1.userInfo != 'undefined')
		for(var key in $1.userInfo){
			var idx = $1.forms.pb.items.getIndex('dataIndex',key);
			if(idx>-1)
				$1.forms.pb.items[idx].setValue($1.userInfo[key]);
		}
};
$(function(){
	profileModule();
});