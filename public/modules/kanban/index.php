<?php
header("Refresh:300");
error_reporting(E_ALL);
function get_web_page( $url )
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
        CURLOPT_SSL_VERIFYHOST => false
    );

    $headers = [
        'Cache-Control: no-cache',
        'Connection: keep-alive',
        'content-length: 0',
    ];
    
    $ch      = curl_init( $url );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}
$ret= get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/KanbanOperation/AllProducts");
if($ret['errno']!=0){
    var_dump($ret);
}else{
    try{
        $j_ret=json_decode($ret['content']);
        if(is_object($j_ret)){
            $str=json_encode($j_ret->data);
        }else{
            echo($ret['content']);
        }
    }catch (Exception $e) {
        echo($ret['content']);
    }
}
?>
<!DOCTYPE html>
<html lang="tr">
    <head>
        <title>DCS Server</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />

        <link rel="stylesheet" type="text/css" href="../../lib/km/dist/km.css?z=<?=time()?>"/>
        <script type="text/javascript" src="../../lib/km/dist/km.js?z=<?=time()?>"></script>
        <style type="text/css">
            html, body{
                padding:0px;margin:0;width:100%;height:100%;
            }
            .body{
                overflow-y: auto;
            }
            .km-kanban-2 {
                height: auto;
            }
            .km-kanban .products .scroll-wrapper {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                overflow: hidden;
                margin-bottom: 5px;
            }
            .km-kanban .products .scroll-wrapper .box-cnt.boxes {
                -webkit-box-flex: 1;
                -ms-flex: 1 0 58px;
                flex: 1 0 58px;
                margin-bottom: 5px;
            }
        </style>
        <script type='text/javascript'>
            var ex = function () {
                window.app = new km.app();
                app.initScreen = app.addScreen();
                app.initScreen.activate();

                var initSc = app.initScreen;

                var data=JSON.parse('<?=$str?>');
                for (var row in data) {
                    var item=data[row];
                    if(item.data.length>0){
                        window['kanban_'+item.client] = new km.kanban({
                            renderTo: initSc.getBody()
                            ,title: item.client
                        });
                        for(var i=0;i<item.data.length;i++){
                            var _title = item.data[i].product;
                            var _min = item.data[i].minboxcount;
                            var _max = item.data[i].maxboxcount;
                            var _cur = item.data[i].currentboxcount;
                            var _box = item.data[i].boxcount;
                            var _packaging = item.data[i].packaging;
                            var _hourlyDemand = item.data[i].hourlydemand;

                            var product = window['kanban_'+item.client].addProduct(_title, _min, _max, _cur, _box, _packaging, _hourlyDemand);
                            //$('.box', product.els.cntBoxes).click(function(){
                            //    product.setCurrent($(this).index());
                            //});
                        }
                    }
                    //console.log(item);
                }
                var k=$.find('.km-kanban');
                for(var i=0;i<k.length;i++){
                    $(k[i]).addClass('km-kanban-2');
                }
            };
            $(function () {
                ex();
            });
        </script>
    </head>
    <body>
    </body>
</html>