var ex = function () {
    window.app = new km.app();
    app.initScreen = app.addScreen();
    app.initScreen.activate();

    var initSc = app.initScreen;

    var cnt = $('<div style="position:absolute;right:15px;top:15px;"></div>').appendTo(initSc.getBody());
    //initSc.kaitekLogo = new km.logo({
    //    renderTo: cnt,
    //    type: 'kaitek'
    //})

    window.k = new km.kanban({
        renderTo: initSc.getBody()
        ,title: '03.08.0060'
    });

    var randomProductCodes = [
        '16.13319AA', '16.18A', '16.12051/0010AD', '16.12053', '16.12055AD'
    ];
    var pIdx = 0;//Math.floor(Math.random()*randomProductCodes.length);
    var getRandomNumber = function(min, max){
        return Math.floor((Math.random() * (max-min)) + min);
    };
    var addSampleProduct = function(total){
        var _title = randomProductCodes[pIdx++ /*% randomProductCodes.length*/];
        var _min = getRandomNumber(2, 6);
        var _max = getRandomNumber(8, total);
        var _cur = getRandomNumber(0, total);

        var _packaging = getRandomNumber(15, 50);
        var _hourlyDemand = getRandomNumber(20, 40);

        //var product = k.addProduct(_title, _min, _max, _cur, total, _packaging, _hourlyDemand);
        var product = k.addProduct(_title, 1, 5, 2, 7, 1, 10);
        $('.box', product.els.cntBoxes).click(function(){
            product.setCurrent($(this).index());
        });
    }
    for(var i=0; i<randomProductCodes.length; i++)
        addSampleProduct(Math.floor((Math.random() * 13) + 12));


};
$(function () {
    ex();
});
