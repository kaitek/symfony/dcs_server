watch2 = function(renderTo, data){
	var t=this;
	t.data = data;
	t.renderTo = renderTo;
	t.render();
};
watch2.prototype = {
	hide : function(){
		this.els.cnt.hide();
	},
	render : function(){
		var t=this, e = t.els = {};
		//if(t.data){
			e.cnt = $(
				'<div style="height:100%;position:relative;display:inline-block;"><table height="100%" class="data">'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;" class="istasyon-cnt"><span class="istasyon"></span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span class="surec"></span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span class="referans"></span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span class="font-size-0 kalan"></span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:30%;padding:0 20px;" class="v-none verim-cnt"><span class="verim"></span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:10%;padding:0 20px;"><span class="personel"></span></td></tr>'+
				'</table></div>'
			).appendTo(t.renderTo);
			e.istasyon = $('.istasyon',e.cnt);
			e.referans = $('.referans',e.cnt);		
			e.surec = $('.surec', e.cnt);
			e.kalan = $('.kalan', e.cnt);
			e.verim = $('.verim', e.cnt);
			e.verimParent = e.verim.parent();
			e.verimParentClass = "v-none";
			e.personel = $('.personel',e.cnt);
			t.refresh(t.data);
		//}
	},
	refresh : function(data){
		var t=this, e = t.els;
		if(data){
			t.data = data;
			e.istasyon.html(data.istasyon);
			if(data.color)
				e.cnt.css("background-color", data.color);
			else
				e.cnt.css("background-color", "transparent");
			e.referans.html(data.referans);
			e.kalan.html(data.kalan);
			e.surec.html(data.surec);
			data.verim = parseInt("0"+data.verim);
			e.verimParentClass = data.verim == 0 ? "v-none" : (data.verim<85 ? "v-0" : (data.verim>105 || data.verim<95 ? "v-1" : "v-2"));
			if(data.verim == 0)
				e.verim.html('<span class="v-none">&nbsp;</span>');
			else
				e.verim.html('<span class="'+e.verimParentClass+'">%'+data.verim+'</span>');
			e.personel.html(data.personel);
			t.show();
		}else
			t.hide();
	},
	show : function(){
		this.els.cnt.show();
	}
};
(function(){
	var module = {
		moduleId : 'screen2',
		refreshInterval : 20,
		page1duration : 15,
		refreshRound : 200,
		refreshRoundEnabled : true,
		els : {
			page0 : $('<div class="bg1" style="position:absolute;left:0;right:0;top:0;bottom:0;"><div class="bg1-shadow-right"></div></div>').appendTo('body')
			,page0_cnt1 : $('<div class="font-size-1 hb" style="float:left;color:#3399ff;width:22%;height:100%;">'+
				'<table height="100%" class="inset-text header">'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span style="opacity:0">İSTASYON</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span style="opacity:0">DURUM</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span style="opacity:0">REFERANS</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span style="opacity:0">KALAN</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:30%;padding:0 20px;"><span style="opacity:0">OEE</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:10%;padding:0 20px;"><span style="opacity:0">PERSONEL</span></td></tr>'+
				'</table>'+
				'</div>').appendTo('body')
			,page0_cnt2 : $('<div class="font-size-1 hb" style="display:block;overflow:hidden;width:78%;height:100%;position:relative;white-space: nowrap;">'+
					'<div class="bg1-shadow-left"></div>'+
					'<div class="data-cnt screen2" style="height:100%;"></div>'+
				'</div>').appendTo('body')
			,page1 : $('<div class="bg1 screen2" style="position:absolute;left:0;right:0;top:0;bottom:0;display:none;"><img style="position:absolute;right:20px;top:20px;" src="inc/images/verimot_logo.png"/>'+
					'<div style="width:100%;height:100%;display:table;"><div style="width:100%;height:100%;display:table-cell;vertical-align:middle;text-align:center;">'+
						'<table class="font-size-1" align="center" style="">'+
							'<tr><td align="right"><b>Atöyle OEE</b></td><td> : </td><td class="atolyeverim verim"></td></tr>'+
							'<tr><td align="right"><b>Hat OEE</b></td><td> : </td><td class="hatverim verim"></td></tr>'+
							'<tr><td align="right"><b>Vardiya Planlanan</b></td><td> : </td><td class="planlanan"></td></tr>'+
							'<tr><td align="right"><b>Çalışan</b></td><td> : </td><td class="calisan"></td></tr>'+
							'<tr><td align="right"><b>Boşta</b></td><td> : </td><td class="bosta"></td></tr>'+
						'</table>'+
					'</div></div>'+
				'</div>').appendTo('body')
		},
		render : function(){
			var t = this, e = t.els;
			e.page0.append(e.page0_cnt1).append(e.page0_cnt2);
			e.dataCnt = $('.data-cnt', e.page0_cnt2);
			e.atolyeverim = $('.atolyeverim', e.page1);
			e.hatverim = $('.hatverim', e.page1);
			e.planlanan = $('.planlanan', e.page1);
			e.calisan = $('.calisan', e.page1);
			e.bosta = $('.bosta', e.page1);
			t.stations = {};
			for(i=0;i<t._stations.length;i++){
				var sId = parseInt(t._stations[i]);
				t.stations[sId] = new watch2(e.dataCnt, t.data[sId]);
			}
			t.rendered = true;
			// t.open();
		},
		init : function(){
			var t = this, e = t.els;
			t._stations = window.stations.split(',');
			t.store = new mj.store({
				url : app.url
				,params : {
					className : 'androidWatch'
					,methodName : 'screen2'
					,stations : stations
				}
			});
			t.data = {};
			t.store.on('load', function(){
				t.storeOnLoad();
			});
			t.els.page0active = true;
			t.store.load();
			
		},
		open : function(){
			var t = this, e = t.els;
			$('.header span', e.page0_cnt1).each(function(i) {
				var t=$(this);
				setTimeout(function(){t.fadeTo(1000, 1);}, (i++) * 200);
			});
			t.currentAnimation = t.scrollToLeft;
			$('.header').click(function(){
				if(!t._debugging){
					t._debugging = true;
					e.dataCnt.stop().css('margin-left',0).parent().css('overflow-x', 'scroll');
				}else{
					t._debugging = false;
					e.dataCnt.css('margin-left',0).parent().css('overflow', 'hidden');
					t.scrollToLeft();
				}
			});
		},
		scrollToLeft : function(){
			var t=this;
			t.els.page0active = true;
			t.currentAnimation = t.scrollToLeft;
			t.els.dataCnt.animate({'marginLeft':-t.els.totalWidth+parseInt(t.els.dataCnt.width())},25*(t.els.totalWidth+parseInt(t.els.dataCnt.css('marginLeft'))), "linear", function(){
				t.scrollToRight();
			});
		},
		scrollToRight : function(){
			var t=this, e=t.els;
			t.els.page0active = true;
			t.currentAnimation = t.scrollToRight;
			e.dataCnt.animate({'marginLeft':0},25*(e.totalWidth-parseInt(e.dataCnt.css('marginLeft'))), "linear", function(){
				e.page0.fadeOut();
				t.els.page0active = false;
				t.els.page1.fadeIn(function(){
					setTimeout(function(){
						if(!t._refreshRound && t.refreshRoundEnabled)
							t._refreshRound = t.refreshRound;
						if(--t._refreshRound == 0 && t.refreshRoundEnabled)
							location.reload();
						else{
							e.page1.fadeOut();
							e.page0.fadeIn();
							t.scrollToLeft();
						}
					}, t.page1duration*1000);
				});
			});
		},
		storeOnLoad : function(){
			var t = this, e = t.els;
			var d = t.store.data[0], d1 = t.store.data[1], first = false;
			for(i=0;i<d.length;i++)
				t.data[parseInt(d[i].id)] = d[i];
			if(!t.rendered){
				t.render();
				t.open();
				first = true;
			}else{
				for(i=0;i<d.length;i++)
					t.stations[d[i].id].refresh(d[i]);
			}
			var verimClass = d1.atolyeverim == 0 ? "v-none" : (d1.atolyeverim<85 ? "v-0" : (d1.atolyeverim>105 || d1.atolyeverim<95 ? "v-1" : "v-2"));
			e.atolyeverim.html('<span class="'+verimClass+'">%'+d1.atolyeverim+'</span>');
			verimClass = d1.hatverim == 0 ? "v-none" : (d1.hatverim<85 ? "v-0" : (d1.hatverim>105 || d1.hatverim<95 ? "v-1" : "v-2"));
			e.hatverim.html('<span class="'+verimClass+'">%'+d1.hatverim+'</span>');
			e.planlanan.html(d1.planlanan);
			e.calisan.html(d1.calisan);
			e.bosta.html(d1.bosta);
			t.els.tables = $('.data', t.els.dataCnt);
			//t.els.tables.removeClass('odd').removeClass('even');
			if(t.els.page0active){
				t.els.totalWidth = 0;
				t.els.tables.each(function(i){
					var tbl = $(this);
					//tbl.addClass(i%2?'odd':'even');
					t.els.totalWidth += parseInt(tbl.width());
				});
			}
			if(first)
				t.currentAnimation();
			t.timer = setTimeout(function(){
				if(!t.stopped)
					t.store.load();
			}, t.refreshInterval*1000);
		}
	};
	module.init();
	app.modules.push(module);
})();