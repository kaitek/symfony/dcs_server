function Countdown(options) {
  var timer,
  instance = this,
  seconds = options.seconds || 10,
  updateStatus = options.onUpdateStatus || function () {},
  counterEnd = options.onCounterEnd || function () {};

  function decrementCounter() {
    updateStatus(seconds);
    // if (seconds === 0) {
      // counterEnd();
      // instance.stop();
    // }
    seconds--;
  }

  this.start = function () {
    clearInterval(timer);
    timer = 0;
    seconds = options.seconds;
    timer = setInterval(decrementCounter, 1000);
  };

  this.stop = function () {
    clearInterval(timer);
  };
};
app.sec_to_time = function (val) {
    var sec_num = parseInt(Math.abs(val), 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = (val<0 ? "-" : "")+hours+':'+minutes+':'+seconds;
    return time;
};
(function(){
	var module = {
		moduleId : 'screen3',
		refreshInterval : 30,
		page1duration : 30,
		page2duration : 30,
		refreshRound : 20,
		els : {
			page1 : $('<div class="screen3"><div class="bg1 verim-cnt" style="position:absolute;left:0;right:0;top:0;bottom:0;">'+
				'<div class="a0" style="position:absolute;top:15px;color:#ffffff;font-size:80px;">'+
					'<div style="background:#6bcaff;display:inline-block;padding:15px 30px 5px 30px;position:relative;" class="title"></div>'+
				'</div>'+
				'<div class="a0" style="position:absolute;top:15px;right:0;color:#ffffff;text-shadow:0 1px 2px #034883;font-size:80px;">'+
					'<div style="background:#034883;color:#ffffff;display:block;padding:15px 30px 5px 30px;position:relative;top:5px;float:right;text-align:right;" class="refno"></div>'+
				'</div>'+
				'<center><div class="hb" style="font-size:120px;max-width:1200px;padding-top:200px;">'+
					'<div style="display:inline-block;position:relative;">'+
						'<div class="a0" style="display:inline-block;position:relative;float:left;margin-right:50px;">'+
							'<div class="vt" style="font-size:60px;">HEDEFLENEN</div>'+
							'<div class="vbg planlanan" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:130px;"></div>'+
						'</div>'+
						'<div class="a0" style="display:inline-block;position:relative;float:left;">'+
							'<div class="vt" style="font-size:60px;">GERÇEKLEŞEN</div>'+
							'<div class="vbg uretim" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:130px;"></div>'+
						'</div>'+
					'</div>'+
					'<center><div class="a0" style="display:inline-block;position:relative;margin-top:50px;">'+
						'<div class="vt" style="font-size:60px;text-align:center;">VERİM</div>'+
						'<div class="vbg verim" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:130px;"></div>'+
					'</div></center>'+
				'</div></center>'+
				'<div class="a0 s-img">&nbsp;</div>'+
			'</div></div>').appendTo('body')
			,page2 : $('<div class="screen3" style="padding:1%;font-size:100px;display:none;color:#034883;position:absolute;width:100%;height:100%;">'+
				'<div class="personel a0"></div>'+
			'</div>').appendTo('body')
			,page3 : $('<div class="screen3"><div class="verim-cnt" style="position:absolute;left:0;right:0;top:0;bottom:0;color:#ffffff;">'+
				'<div class="bg3 vbg"></div>'+
				'<div class="a0" style="position:absolute;top:15px;">'+
					'<div style="display:inline-block;padding:15px 30px 5px 30px;position:relative;font-size:85px;" class="title3"></div>'+
					'<div style="display:block;padding:0 30px 5px 30px;position:relative;margin-top:-15px;font-size:75px;" class="refno3"></div>'+
				'</div>'+
				'<center><div class="vt1 a0 sure" style="font-size:270px;margin-top:20%;"></div></center>'+
			'</div></div>').appendTo('body')
		},
		render : function(){
			var t = this, e = t.els;
			window.e = e;
			e.verimCnt = $('.verim-cnt', e.page1);
			e.title = $('.title', e.page1);
			e.refno = $('.refno', e.page1);
			e.uretim = $('.uretim', e.page1);
			e.planlanan = $('.planlanan', e.page1);
			e.verim = $('.verim', e.page1);
			
			e.personel = $('.personel', e.page2);
			e.ayar = $('.ayar', e.page3);
			e.verimCnt3 = $('.verim-cnt', e.page3);
			e.title3 = $('.title3', e.page3);
			e.refno3 = $('.refno3', e.page3);
			e.sure = $('.sure', e.page3);
			t.rendered = true;
		},
		init : function(){
			var t = this, e = t.els;
			t.store = new mj.store({
				url : app.url
				,params : {
					className : 'androidWatch'
					,methodName : 'screen3'
					,station : station
				}
			});
			t.data = {};
			t.store.on('load', function(){
				if(!t.rendered)
					t.render();
				t.storeOnLoad();
			});
			t.store.load();
			
		},
		showPage1 : function(){
			var t = this, e = t.els;
			e.page2Active = false;
			e.page3Active = false;
			e.page1.css('display', 'block');
			e.personel.fadeTo(500, 0);
			$('.a0', e.page3).fadeTo(500, 0);
			setTimeout(function(){
				e.page2.css('display', 'none');
				e.page3.css('display', 'none');
			}, 500);
			$('.a0', e.page1).each(function(i) {
				var t=$(this);
				setTimeout(function(){t.fadeTo(1000, 1);}, (i++) * 200);
			});
			setTimeout(function(){
				t.showPage2();
			}, t.page1duration*1000);
		},
		showPage2 : function(){
			var t = this, e = t.els;
			e.page2Active = true;
			e.page3Active = false;
			$('.a0', e.page1).fadeTo(500, 0);
			setTimeout(function(){
				e.page1.css('display', 'none');
				e.page2.css('display', 'block');
				e.personel.fadeTo(500, 1);
				setTimeout(function(){
					t.showPage2Anim1();
				}, 500);
				setTimeout(function(){
					t.showPage1();
				}, t.page2duration*1000);
			}, 500);
		},
		showPage3 : function(){
			var t = this, e = t.els;
			e.page2Active = false;
			e.page3Active = true;
			$('.a0', e.page1).fadeTo(500, 0);
			e.personel.fadeTo(500, 0);
			setTimeout(function(){
				e.page1.css('display', 'none');
				e.page2.css('display', 'none');
				e.page3.css('display', 'block');
				$('.a0', e.page3).each(function(i) {
					var t=$(this);
					setTimeout(function(){t.fadeTo(750, 1);}, (i++) * 200);
				});
			}, 500);
		},
		showPage2Anim1 : function(){
			var t = this, e = t.els;
			t.page2anim = t.showPage2Anim1;
			var size = e.personel.height()-e.page2.height(), mt = parseInt(e.personel.css('marginTop'));
			//console.log("1 : size="+size+", mt="+mt+" sm="+(size+mt));
			if(size>0){
				e.personel.animate({'marginTop': -size}, (size+mt)*30, "linear", function(){
					t.showPage2Anim2();
				});
			}else if(mt<0){
				t.showPage2Anim2();
			}
		},
		showPage2Anim2 : function(){
			var t = this, e = t.els;
			t.page2anim = t.showPage2Anim2;
			var mt = parseInt(e.personel.css('marginTop'));
			//console.log("2 - "+mt);
			if(mt!=0){
				e.personel.animate({'marginTop': 0}, -mt*30, "linear", function(){
					t.showPage2Anim1();
				});
			}
		},
		storeOnLoad : function(){
			var t = this, e = t.els;
			var d3 = t.store.data[2];
			if(d3){
				d3 = d3[0];
				e.title3.html(d3.name);
				e.refno3.html(d3.description);
				
				e.sure.html(app.sec_to_time(d3.suresec));
				/*
				if(!e.counter)
					e.counter = new Countdown({  
						seconds:d3.suresec,  // number of seconds to count down
						onUpdateStatus: function(sec){
							e.sure.html(app.sec_to_time(sec));
						}, // callback for each second
						onCounterEnd: function(){
						} // final action
					});
				else
					e.counter.stop();
				e.counter.seconds = d3.suresec;
				e.counter.start();
				*/
				if(e.lastVerim)
					e.verimCnt3.removeClass(e.lastVerim);
				e.lastVerim = "v"+d3.renk;
				e.verimCnt3.addClass(e.lastVerim);
				if(!e.page3Active)
					t.showPage3();
			}else{
				var d1 = t.store.data[0][0];
				var d2 = t.store.data[1][0];
				if(e.lastVerim)
					e.verimCnt.removeClass(e.lastVerim);
				e.lastVerim = "v"+d1.renk;
				e.verimCnt.addClass(e.lastVerim);
				e.title.html(d1.title);
				//e.refno.html(d1.refno);
				e.refno.html(d1.refno+(d1.islem ? (d1.refno!='' ? '<br/>'+d1.islem : d1.islem) : ''));
				e.uretim.html(d1.uretim);
				e.planlanan.html(d1.planlanan);
				e.verim.html('% '+d1.verim);
				
				e.personel.html(d2.personel);
				if(e.page3Active || (typeof e.page3Active == 'undefined' && typeof e.page2Active == 'undefined' ))
					t.showPage1();
				else if(e.page2Active){
					e.personel.stop();
					t.page2anim();
				}
			}
			t.timer = setTimeout(function(){
				if(!t._refreshRound)
					t._refreshRound = t.refreshRound;
				if(--t._refreshRound == 0)
					location.reload();
				else{
					if(!t.stopped)
						t.store.load();
				}
			}, t.refreshInterval*1000);
		}
	};
	module.init();
	app.modules.push(module);
})();