function Countdown(options) {
  var timer, instance = this, seconds = options.seconds || 10, updateStatus = options.onUpdateStatus || function () {}, counterEnd = options.onCounterEnd || function () {};

  function decrementCounter() {
    updateStatus(seconds);
    seconds--;
  }

  this.start = function () {
    clearInterval(timer);
    timer = 0;
    seconds = options.seconds;
    timer = setInterval(decrementCounter, 1000);
  };

  this.stop = function () {
    clearInterval(timer);
  };
};
app.sec_to_time = function (val) {
    var sec_num = parseInt(Math.abs(val), 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = (val<0 ? "-" : "")+hours+':'+minutes+':'+seconds;
    return time;
};
(function(){
	window.w = $(window).width();

	window.sizes = {
		p1f1 : w>1280 ? 100 : 60
		,p1f2 : w>1280 ? 200 : 130
		,p1f3 : w>1280 ? 140 : 90
	};
	
	var module = {
		moduleId : 'screen3',
		refreshInterval : 30,
		page1duration : 30,
		page2duration : 30,
		refreshRound : 8,
		refreshPageEnabled : false,
		els : {
			page1 : $('<div class="screen3"><div class="bg1 verim-cnt" style="position:absolute;left:0;right:0;top:0;bottom:0;">'+
				'<div class="a0" style="position:absolute;top:15px;color:#ffffff;font-size:80px;">'+
					'<div style="background:#6bcaff;display:inline-block;padding:15px 30px 5px 30px;position:relative;" class="title"></div>'+
				'</div>'+
				'<div class="a0" style="position:absolute;top:15px;right:0;color:#ffffff;text-shadow:0 1px 2px #034883;font-size:70px;">'+
					'<div style="background:#034883;color:#ffffff;display:block;padding:15px 30px 5px 30px;position:relative;top:5px;float:right;text-align:right;" class="refno"></div>'+
				'</div>'+
				'<center><div class="hb" style="font-size:100px;max-width:1800px;padding-top:200px;">'+
					'<div style="display:inline-block;position:relative;">'+
						'<div class="a0 cntkul" style="display:inline-block;position:relative;float:left;margin-right:50px;">'+
							'<div class="vt" style="font-size:'+sizes.p1f1+'px;">KUL</div>'+
							'<div class="vbg" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:'+sizes.p1f2+'px;">'+
								'<span class="kul" style="line-height:150px;"></span><br/>'+
								//'<span class="planlanan-tpp" style="font-size:'+sizes.p1f3+'px;line-height: 120px;"></span>'+
							'</div>'+
						'</div>'+
						'<div class="a0 cntper" style="display:inline-block;position:relative;float:left;margin-right:50px;">'+
							'<div class="vt" style="font-size:'+sizes.p1f1+'px;">PER</div>'+
							'<div class="vbg" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:'+sizes.p1f2+'px;">'+
								'<span class="per" style="line-height:150px;"></span><br/>'+
								//'<span class="uretim-tpp" style="font-size:'+sizes.p1f3+'px;line-height: 120px;"></span>'+
							'</div>'+
						'</div>'+
						'<div class="a0 cntkal" style="display:inline-block;position:relative;float:left;margin-right:50px;">'+
							'<div class="vt" style="font-size:'+sizes.p1f1+'px;">KAL</div>'+
							'<div class="vbg" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:'+sizes.p1f2+'px;">'+
								'<span class="kal" style="line-height:150px;"></span><br/>'+
								//'<span class="uretim-tpp" style="font-size:'+sizes.p1f3+'px;line-height: 120px;"></span>'+
							'</div>'+
						'</div>'+
						'<div class="a0 cntoee" style="display:inline-block;position:relative;float:left;">'+
							'<div class="vt" style="font-size:'+sizes.p1f1+'px;">OEE</div>'+
							'<div class="vbg" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:'+sizes.p1f2+'px;">'+
								'<span class="oee" style="line-height:150px;"></span><br/>'+
							'</div>'+
						'</div>'+
					'</div>'+

					'<div style="display:inline-block;position:relative;">'+
						'<div class="a0 cnthedef" style="display:inline-block;position:relative;float:left;margin-right:50px;">'+
							'<div class="vt" style="font-size:'+sizes.p1f1+'px;">HEDEF (sn)</div>'+
							'<div class="vbg" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:'+sizes.p1f2+'px;">'+
								'<span class="hedef" style="line-height:150px;"></span><br/>'+
								//'<span class="uretim-tpp" style="font-size:'+sizes.p1f3+'px;line-height: 120px;"></span>'+
							'</div>'+
						'</div>'+
						'<div class="a0 cntgercek" style="display:inline-block;position:relative;float:left;">'+
							'<div class="vt" style="font-size:'+sizes.p1f1+'px;">GERÇEK (sn)</div>'+
							'<div class="vbg" style="color:#ffffff;padding:15px 30px 5px 30px;text-align:center;position:relative;font-size:'+sizes.p1f2+'px;">'+
								'<span class="gercek" style="line-height:150px;"></span><br/>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div></center>'+
				'<div class="a0 cntsurat"><div class="a0 s-img">&nbsp;</div></div>'+
			'</div></div>').appendTo('body')
			,page2 : $('<div class="screen3" style="padding:1%;font-size:100px;display:none;color:#034883;position:absolute;width:100%;height:100%;">'+
				'<div class="personel a0"></div>'+
			'</div>').appendTo('body')
			,page3 : $('<div class="screen3"><div class="verim-cnt" style="position:absolute;left:0;right:0;top:0;bottom:0;color:#ffffff;">'+
				'<div class="bg3 vbg"></div>'+
				'<div class="a0" style="position:absolute;top:15px;">'+
					'<div style="display:inline-block;padding:15px 30px 5px 30px;position:relative;font-size:85px;" class="title3"></div>'+
					'<div style="display:block;padding:0 30px 5px 30px;position:relative;margin-top:-15px;font-size:75px;" class="refno3"></div>'+
				'</div>'+
				'<center><div class="vt1 a0 sure" style="font-size:270px;margin-top:20%;"></div></center>'+
			'</div></div>').appendTo('body')
		},
		render : function(){
			var t = this, e = t.els;
			window.e = e;
			//e.verimCnt = $('.verim-cnt', e.page1);
			e.title = $('.title', e.page1);
			e.refno = $('.refno', e.page1);
			//e.uretim = $('.uretim', e.page1);
			//e.uretimTpp = $('.uretim-tpp', e.page1);
			//e.planlanan = $('.planlanan', e.page1);
			//e.planlananTpp = $('.planlanan-tpp', e.page1);
			//e.verim = $('.verim', e.page1);
			e.kul = $('.kul', e.page1);
			e.cntkul = $('.cntkul', e.page1);
			e.per = $('.per', e.page1);
			e.cntper = $('.cntper', e.page1);
			e.kal = $('.kal', e.page1);
			e.cntkal = $('.cntkal', e.page1);
			e.oee = $('.oee', e.page1);
			e.cntoee = $('.cntoee', e.page1);
			e.cnthedef = $('.cnthedef', e.page1);
			e.cntgercek = $('.cntgercek', e.page1);
			e.hedef = $('.hedef', e.page1);
			e.gercek = $('.gercek', e.page1);
			e.surat = $('.cntsurat', e.page1);
			e.personel = $('.personel', e.page2);
			e.ayar = $('.ayar', e.page3);
			e.verimCnt3 = $('.verim-cnt', e.page3);
			e.title3 = $('.title3', e.page3);
			e.refno3 = $('.refno3', e.page3);
			e.sure = $('.sure', e.page3);
			t.rendered = true;
		},
		init : function(){
			var t = this, e = t.els;
			var ua = navigator.userAgent.toLowerCase();
			t.isAndroid = !(window.inBrowser);
			//t.store = inBrowser ? new mj.store({
			//	url : app.url
			//	,params : {
			//		className : 'androidWatch'
			//		,methodName : 'screen3'
			//		,macId : window.macId
			//	}
			//}) : new mj.store();
			t.store = inBrowser ? new mj.store({
				method: 'POST'
				,url : app.url
				,params : {
					//className : 'androidWatch'
					//,methodName : 'screen2'
					pc : window.macId
					,isStore:true
				}
			}) : new mj.store();
			t.data = {};
			$(document).ajaxError(function(){
				t._refreshRound = t.refreshRound;
				if(!t.isAndroid)
					t.nextRequest();
			});
			t.store.on('load', function(){
				if(!t.rendered)
					t.render();
				t.storeOnLoad();
			});
			//if(!t.isAndroid)
			//	t.store.load();
			
		},
		showPage1 : function(){
			var t = this, e = t.els;
			e.page2Active = false;
			e.page3Active = false;
			e.page1.css('display', 'block');
			e.personel.fadeTo(500, 0);
			$('.a0', e.page3).fadeTo(500, 0);
			setTimeout(function(){
				e.page2.css('display', 'none');
				e.page3.css('display', 'none');
			}, 500);
			$('.a0', e.page1).each(function(i) {
				var t=$(this);
				setTimeout(function(){t.fadeTo(1000, 1);}, (i++) * 200);
			});
			setTimeout(function(){
				if(t.personelExists)
					t.showPage2();
				else
					t.showPage1();
			}, t.page1duration*1000);
		},
		showPage2 : function(){
			var t = this, e = t.els;
			e.page2Active = true;
			e.page3Active = false;
			$('.a0', e.page1).fadeTo(500, 0);
			setTimeout(function(){
				e.page1.css('display', 'none');
				e.page2.css('display', 'block');
				e.personel.fadeTo(500, 1);
				setTimeout(function(){
					t.showPage2Anim1();
				}, 500);
				setTimeout(function(){
					t.showPage1();
				}, t.page2duration*1000);
			}, 500);
		},
		showPage3 : function(){
			var t = this, e = t.els;
			e.page2Active = false;
			e.page3Active = true;
			$('.a0', e.page1).fadeTo(500, 0);
			e.personel.fadeTo(500, 0);
			setTimeout(function(){
				e.page1.css('display', 'none');
				e.page2.css('display', 'none');
				e.page3.css('display', 'block');
				$('.a0', e.page3).each(function(i) {
					var t=$(this);
					setTimeout(function(){t.fadeTo(750, 1);}, (i++) * 200);
				});
			}, 500);
		},
		showPage2Anim1 : function(){
			var t = this, e = t.els;
			t.page2anim = t.showPage2Anim1;
			var size = e.personel.height()-e.page2.height(), mt = parseInt(e.personel.css('marginTop'));
			if(size>0){
				e.personel.animate({'marginTop': -size}, (size+mt)*30, "linear", function(){
					t.showPage2Anim2();
				});
			}else if(mt<0){
				t.showPage2Anim2();
			}
		},
		showPage2Anim2 : function(){
			var t = this, e = t.els;
			t.page2anim = t.showPage2Anim2;
			var mt = parseInt(e.personel.css('marginTop'));
			if(mt!=0){
				e.personel.animate({'marginTop': 0}, -mt*30, "linear", function(){
					t.showPage2Anim1();
				});
			}
		},
		storeOnLoad : function(){
			var t = this, e = t.els;
			//console.log(t.store.data);
			var d3 = t.store.data[2];
			if(d3){
				d3 = d3[0];
				e.title3.html(d3.name);
				e.refno3.html(d3.description);
				
				e.sure.html(app.sec_to_time(d3.surestr));
				if(e.lastVerim)
					e.verimCnt3.removeClass(e.lastVerim);
				e.lastVerim = "v"+d3.renk;
				e.verimCnt3.addClass(e.lastVerim);
				if(!e.page3Active)
					t.showPage3();
			}else{
				var d1 = t.store.data[0][0];
				var d2 = t.store.data[1][0];
				if(e.last_kul){
					e.cntkul.removeClass(e.last_kul);
				}
				e.last_kul = "v"+d1.renk_kul;
				e.cntkul.addClass(e.last_kul);
				if(e.last_per){
					e.cntper.removeClass(e.last_per);
				}
				e.last_per = "v"+d1.renk_per;
				e.cntper.addClass(e.last_per);
				if(e.last_kal){
					e.cntkal.removeClass(e.last_kal);
				}
				e.last_kal = "v"+d1.renk_kal;
				e.cntkal.addClass(e.last_kal);
				if(e.last_oee){
					e.cntoee.removeClass(e.last_oee);
					e.surat.removeClass(e.last_oee);
				}
				e.last_oee = "v"+d1.renk_oee;
				e.cntoee.addClass(e.last_oee);
				e.surat.addClass(e.last_oee);
				//e.verimCnt.addClass(e.last_oee);
				e.title.html(d1.title);
				e.refno.html(d1.refno+(d1.islem ? (d1.refno!='' ? '<br/>'+d1.islem : d1.islem) : ''));
				//e.uretim.html(d1.uretim);
				//e.uretimTpp.html(d1.tdtpp2+(d1.tdtpp2 ? ' sn' : ''));
				//e.planlanan.html(d1.planlanan);
				//e.planlananTpp.html(d1.tpp+(d1.tpp ? ' sn' : ''));
				//e.verim.html('% '+d1.verim);
				e.kul.html(d1.kul);
				e.per.html(d1.per);
				e.kal.html(d1.kal);
				e.oee.html(d1.oee);
				e.hedef.html(d1.hedef);
				e.gercek.html(d1.gercek);
				if(parseFloat(d1.gercek)<parseFloat(d1.hedef)){
					e.cnthedef.addClass('v3');
					e.cntgercek.addClass('v3');
				}else{
					e.cnthedef.addClass('v1');
					e.cntgercek.addClass('v1');
				}
				t.personelExists = (d2.personel && d2.personel != "" && d2.personel != "<ul></ul>");
				e.personel.html(d2.personel);
				if(e.page3Active || (typeof e.page3Active == 'undefined' && typeof e.page2Active == 'undefined' ))
					t.showPage1();
				else if(e.page2Active){
					e.personel.stop();
					if(t.page2anim)t.page2anim();
				}
			}
			if(!t.isAndroid)
				t.nextRequest();
		},
		nextRequest : function(){
			var t=this;
			if(!t.isAndroid){
				t.timer = setTimeout(function(){
					if(!t._refreshRound)
						t._refreshRound = t.refreshRound;
					if(--t._refreshRound == 0 && t.refreshPageEnabled){
						$.get(app.url,{c:(new Date()).getTime()},function(d){
							location.reload();
						});
					}else{
						if(!t.stopped)
							t.store.load();
					}
				}, t.refreshInterval*1000);
			}
		}
	};
	module.init();
	app.modules.push(module);
	window.screenLoaded = true;
})();
window.loadData = function(data){
	try{
		window.m = app.modules[0];
		eval('m.store.data = data;');
		eval('m.store.load();');
		window.connectionAvailable();
	}catch(err){
		return "error";
	}
	return "ok";
};
window.connectionAvailable = function(){
	var m = app.modules[0];
	if(m.rendered)
		m.els.title.css("background", "#6bcaff");
};
window.connectionLost = function(){
	var m = app.modules[0];
	if(m.rendered)
		m.els.title.css("background", "#e80000");
};