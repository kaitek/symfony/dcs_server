watch1 = function(renderTo, data){
	var t=this;
	t.data = data;
	t.renderTo = renderTo;
	t.render();
};
watch1.prototype = {
	render : function(){
		var t=this, e = t.els = {};
		if(t.data){
			e.cnt = $(
				'<div class="inset" style="height:40%;display:table-cell;color:#00aec8;overflow:hidden;position:relative;background:#fff;">'+
					//'<div style="position:absolute;width:100%;height:100%;z-index:-1;">'+
					//	'<img src="inc/images/s'+t.data.stationtypeId+'.png" style="max-width:90%;max-height:70%;"/>'+
					//'</div>'+
					'<div style="width:100%;height:100%;position:relative;">'+
						(
							t.data.stationtypeId==5 ? 
							'<img class="img-station" src="inc/images/s'+t.data.stationtypeId+'.png" style="max-width:90%;max-height:80%;position:absolute;z-index:0;left:5%;bottom:5%;"/>'
							: ''
						)+
						'<div style="width:100%;position:relative;z-index:2;">'+
							'<div style="position:absolute;width:100%;height:100%;background:#00aec8;z-index:0;"></div>'+
							'<div style="padding:1% 5%;z-index:1;position:relative;color:#fff;" class="istasyon-cnt font-size-7 hb"></div>'+
						'</div>'+
						'<div style="position:absolute;z-index:2;top:10%;right:17%;text-align:center;" class="font-size-5">'+
							'<div>Kalan</div>'+
							'<div class="hb kalan-cnt"></div>'+
							'<div>OEE</div>'+
							'<div class="hb verim-cnt"></div>'+
						'</div>'+
						'<div style="max-width:100%;position:absolute;bottom:0;right:0;text-align:center;">'+
							'<div style="padding:1% 3%;z-index:1;position:relative;color:#00aec8;text-align:right;" class="personel-cnt font-size-6 hb"></div>'+
						'</div>'+
					'</div>'+
				'</div>'
			).appendTo(t.renderTo);
			e.istasyon = $('.istasyon-cnt',e.cnt);		
			e.kalan = $('.kalan-cnt', e.cnt);
			e.verim = $('.verim-cnt', e.cnt);
			e.personel = $('.personel-cnt',e.cnt);
			t.refresh(t.data);
		}else{
			e.cnt = $(
				'<div style="height:40%;display:table-cell;">&nbsp;</div>'
			).appendTo(t.renderTo);
		}
	},
	refresh : function(data){
		var t=this, e = t.els;
		t.data = data;
		e.istasyon.html(data.stationName);
		e.kalan.html(data.kalan);
		e.verim.html('%'+data.verim);
		e.personel.html(data.peradi);
	}
};
(function(){
	var module = {
		moduleId : 'screen1',
		refreshInterval : 20,
		els : {
			cnt : $('<div style="position:absolute;left:0;right:0;top:0;bottom:0;background:#292e33;"></div>').appendTo('body')
			,cnt1 : $('<div style="width:100%;height:40%;display:table;table-layout:fixed;border-spacing:15px;border-collapse:separate;position:absolute;top:-40%;"></div>').appendTo('body')
			,cnt2 : $('<div style="width:100%;height:20%;"></div>').appendTo('body')
			,cnt3 : $('<div style="width:100%;height:40%;display:table;table-layout:fixed;border-spacing:15px;border-collapse:separate;position:absolute;bottom:-40%;"></div>').appendTo('body')
		},
		render : function(){
			var t = this, e = t.els;
			e.cnt.append(e.cnt1).append(e.cnt2).append(e.cnt3);
			t.stations = {};
			for(i=0;i<t.row1.length;i++){
				var sId = parseInt(t.row1[i]);
				if(sId == 0)
					new watch1(e.cnt1);
				else
					t.stations[sId] = new watch1(e.cnt1, t.data[sId]);
			}
			for(i=0;i<t.row2.length;i++){
				var sId = t.row2[i];
				if(sId == 0)
					new watch1(e.cnt3);
				else{
					t.stations[sId] = new watch1(e.cnt3, t.data[sId]);
				}
			}
			t.rendered = true;
			if(t.row1.length>6)
				$('.img-station', e.cnt1).hide();
			if(t.row2.length>6)
				$('.img-station', e.cnt3).hide();
			t.open();
		},
		init : function(){
			var t = this, e = t.els, _stations = [];
			var s = stations.split('|');
			t.row1 = s[0].split(',');
			t.row2 = s[1].split(',');
			for(i=0;i<t.row1.length;i++)
				if(t.row1[i]>0)
					_stations.push(t.row1[i]);
			for(i=0;i<t.row2.length;i++)
				if(t.row2[i]>0)
					_stations.push(t.row2[i]);
			t.store = new mj.store({
				url : app.url
				,params : {
					className : 'androidWatch'
					,methodName : 'screen1'
					,stations : _stations.join(',')
				}
			});
			t.data = {};
			t.store.on('load', function(){
				t.storeOnLoad();
			});
			t.store.load();
		},
		open : function(){
			var t = this, e = t.els;
			e.cnt1.animate({top:0}, 600);
			e.cnt3.animate({bottom:0}, 600);
		},
		storeOnLoad : function(){
			var t = this;
			var d = t.store.data;
			for(i=0;i<d.length;i++)
				t.data[parseInt(d[i].id)] = d[i];
			if(!t.rendered){
				t.render();
				t.open();
			}else{
				for(i=0;i<d.length;i++)
					t.stations[d[i].id].refresh(d[i]);
			}
			
			t.timer = setTimeout(function(){
				if(!t.stopped)
					t.store.load();
			}, t.refreshInterval*1000);
		}
	};
	module.init();
	app.modules.push(module);
})();