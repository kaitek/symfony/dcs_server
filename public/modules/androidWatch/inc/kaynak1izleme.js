watch2 = function(renderTo, data){
	var t=this;
	t.data = data;
	t.renderTo = renderTo;
	t.render();
};
watch2.prototype = {
	hide : function(){
		this.els.cnt.hide();
	},
	render : function(){
		var t=this, e = t.els = {};
		e.cnt = $(
			'<div style="width:50%;height:100%;position:relative;display:inline-block;"><table width="100%" height="100%" class="data">'+
				'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;" class="istasyon-cnt"><span class="istasyon"></span></td></tr>'+
				'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span class="surec"></span></td></tr>'+
				'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span class="referans"></span></td></tr>'+
				'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span class="font-size-0 kalan"></span></td></tr>'+
				'<tr><td align="center" valign="middle" style="height:30%;padding:0 20px;" class="v-none verim-cnt"><span class="verim"></span></td></tr>'+
				'<tr><td align="center" valign="middle" style="height:10%;padding:0 20px;"><span class="personel"></span></td></tr>'+
			'</table></div>'
		).appendTo(t.renderTo);
		e.istasyon = $('.istasyon',e.cnt);
		e.referans = $('.referans',e.cnt);		
		e.surec = $('.surec', e.cnt);
		e.kalan = $('.kalan', e.cnt);
		e.verim = $('.verim', e.cnt);
		e.verimParent = e.verim.parent();
		e.verimParentClass = "v-none";
		e.personel = $('.personel',e.cnt);
		t.refresh(t.data);
	},
	refresh : function(data){
		var t=this, e = t.els;
		if(data){
			t.data = data;
			e.istasyon.html(data.istasyon);
			if(data.color)
				e.cnt.css("background-color", data.color);
			else
				e.cnt.css("background-color", "transparent");
			e.referans.html(data.referans);
			e.kalan.html(data.kalan);
			e.surec.html(data.surec);
			if(!data.verim || parseFloat(data.verim)==0)
				data.verim = 0;
			else
				data.verim = parseFloat("0"+data.verim);
			e.verimParentClass = data.verim == 0 ? "v-none" : (data.verim<=82 ? "v-0" : (data.verim>86 ? "v-2" : "v-1"));
			if(data.verim == 0)
				e.verim.html('<span class="v-none">&nbsp;</span>');
			else
				e.verim.html('<span class="'+e.verimParentClass+'">%'+data.verim+'</span>');
			e.personel.html(data.personel);
		}
	},
	show : function(){
		this.els.cnt.show();
	}
};
(function(){
	var module = {
		moduleId : 'screen2',
		refreshInterval : 20,
		timeoutSeconds : 10,
		pageduration : 5,
		page1duration : 5,
		refreshRound : 200,
		refreshRoundEnabled : false,
		els : {
			page0 : $('<div class="bg1" style="position:absolute;left:0;right:0;top:0;bottom:0;"></div>').appendTo('body')
			,page0_cnt1 : $('<div id="cntDbg" class="font-size-1 hb" style="float:left;color:#3399ff;width:19%;height:100%;">'+
				'<div id="dbg" style="display:none;position:absolute;right:0;bottom:0;left:0;top:0;font-size:15px;background-color:#fff;overflow-y:scroll;"></div>'+
				'<table height="100%" class="inset-text header">'+
					'<tr id="startDebug"><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span>İSTASYON</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span>DURUM</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span>REFERANS</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:15%;padding:0 20px;"><span>KALAN</span></td></tr>'+
					'<tr><td align="center" valign="middle" style="height:30%;padding:0 20px;"><span>OEE</span></td></tr>'+
					'<tr id="stopDebug"><td align="center" valign="middle" style="height:10%;padding:0 20px;"><span>PERSONEL</span></td></tr>'+
				'</table>'+
				'</div>').appendTo('body')
			,page0_cnt2 : $('<div class="font-size-1 hb" style="display:block;overflow:hidden;width:81%;height:100%;position:relative;white-space: nowrap;">'+
					'<div class="data-cnt screen2" style="height:100%;"></div>'+
				'</div>').appendTo('body')
			,page1 : $('<div class="bg1 screen2" style="position:absolute;left:0;right:0;top:0;bottom:0;display:none;"><img style="position:absolute;right:20px;top:20px;" src="inc/images/verimot_logo.png"/>'+
					'<div style="width:100%;height:100%;display:table;"><div style="width:100%;height:100%;display:table-cell;vertical-align:middle;text-align:center;">'+
						'<table class="font-size-1" align="center" style="margin-left:50px;">'+
							'<tr><td align="right"><b>Atöyle OEE</b></td><td> : </td><td class="atolyeverim verim"></td></tr>'+
							'<tr><td align="right"><b>Hat OEE</b></td><td> : </td><td class="hatverim verim"></td></tr>'+
							'<tr><td align="right"><b>Vardiya Planlanan</b></td><td> : </td><td class="planlanan"></td></tr>'+
							'<tr><td align="right"><b>Çalışan</b></td><td> : </td><td class="calisan"></td></tr>'+
							'<tr><td align="right"><b>Boşta</b></td><td> : </td><td class="bosta"></td></tr>'+
						'</table>'+
					'</div></div>'+
				'</div>').appendTo('body')
		},
		render : function(){
			var t = this, e = t.els;
			e.page0.append(e.page0_cnt1).append(e.page0_cnt2);
			e.dataCnt = $('.data-cnt', e.page0_cnt2);
			e.atolyeverim = $('.atolyeverim', e.page1);
			e.hatverim = $('.hatverim', e.page1);
			e.planlanan = $('.planlanan', e.page1);
			e.calisan = $('.calisan', e.page1);
			e.bosta = $('.bosta', e.page1);
			t.stations = {};
			for(i=0;i<t._stations.length;i++){
				var sId = parseFloat(t._stations[i]);
				t.stations[sId] = new watch2(e.dataCnt, t.data[sId]);
			}
			t.rendered = true;
		},
		init : function(){
			var t = this, e = t.els;
			t.store = inBrowser ? new mj.store({
				method: 'POST'
				,url : app.url
				,params : {
					//className : 'androidWatch'
					//,methodName : 'screen2'
					pc : window.macId
					,isStore:true
				}
			}) : new mj.store();
			t.data = {};
			t.store.on('load', function(){
				t.storeOnLoad();
			});
			t.store.on('error', function(){
				if(inBrowser)
					t.setNextRequest();
			});
			t.els.page0active = true;
			
		},
		next : function(){
			var t = this, e = t.els;
			if(t.visibleStations && t.visibleStations.length>0){
				if(!t.curIndex)
					t.curIndex = 0;
				for(i=0;i<t.visibleStations.length;i++){
					var sw = t.stations[parseFloat(t.visibleStations[i])];
					if(i==t.curIndex || i==t.curIndex+1)
						sw.show();
					else
						sw.hide();
				}
				if(t.curIndex<t.visibleStations.length-2){
					t.curIndex += 2;
					if(!t._debugging)
						setTimeout(function(){t.next();}, t.pageduration*1000);
				}else{
					t.curIndex = 0;
					setTimeout(function(){
						e.page0.fadeOut();
						t.els.page0active = false;
						t.els.page1.fadeIn(function(){
							setTimeout(function(){
								if(!t._refreshRound && t.refreshRoundEnabled)
									t._refreshRound = t.refreshRound;
								if(--t._refreshRound == 0 && t.refreshRoundEnabled)
									location.reload();
								else{
									e.page1.fadeOut();
									e.page0.fadeIn();
									t.next();
								}
							}, t.page1duration*1000);
						});
					}, t.pageduration*1000);
				}
			}else
				setTimeout(function(){t.next();}, 100);
		},
		open : function(){
			var t = this, e = t.els;
			$('#startDebug').click(function(){
				t._debugging = true;
				t.next();
			});
			$('#stopDebug').click(function(){
				t._debugging = false;
				t.next();
			});
		},
		scrollToLeft : function(){
			var t=this;
			t.els.page0active = true;
			t.currentAnimation = t.scrollToLeft;
			t.els.dataCnt.animate({'marginLeft':-t.els.totalWidth+parseInt(t.els.dataCnt.width())},25*(t.els.totalWidth+parseInt(t.els.dataCnt.css('marginLeft'))), "linear", function(){
				t.scrollToRight();
			});
		},
		scrollToRight : function(){
			var t=this, e=t.els;
			t.els.page0active = true;
			t.currentAnimation = t.scrollToRight;
			e.dataCnt.animate({'marginLeft':0},25*(e.totalWidth-parseInt(e.dataCnt.css('marginLeft'))), "linear", function(){
				e.page0.fadeOut();
				t.els.page0active = false;
				t.els.page1.fadeIn(function(){
					setTimeout(function(){
						if(!t._refreshRound && t.refreshRoundEnabled)
							t._refreshRound = t.refreshRound;
						if(--t._refreshRound == 0 && t.refreshRoundEnabled)
							location.reload();
						else{
							e.page1.fadeOut();
							e.page0.fadeIn();
							t.scrollToLeft();
						}
					}, t.page1duration*1000);
				});
			});
		},
		setNextRequest : function(){
			var t = this;
			t.timer = setTimeout(function(){
				if(t.timeOutChecker)
					clearTimeout(t.timeOutChecker);
				t.timeOutChecker = setTimeout(function(){
					if(t._timeoutChecking){
						connectionLost();
						t.setNextRequest();	
					}
				}, t.timeoutSeconds*1000);
				t._timeoutChecking = true;
				t.store.load();
			}, t.refreshInterval*1000);
		},
		storeOnLoad : function(){
			var t = this, e = t.els;
			t._timeoutChecking = false;
			connectionAvailable();
			if(inBrowser)
				t.setNextRequest();
			try{
				var d = t.store.data[0], d1 = t.store.data[1], first = false;
				for(i=0;i<d.length;i++){
					if(!t._stations){
						t._stations = [];
						t.visibleStations = [];
					}
					var idx=t._stations.indexOf(d[i].id);
					if(idx==-1){
						t.visibleStations.push(d[i].id);
						t._stations.push(d[i].id);
					}
					t.data[parseFloat(d[i].id)] = d[i];
				}
				if(!t.rendered){
					t.render();
					t.open();
					first = true;
				}else{
					for(i=0;i<d.length;i++){
						if(!t.stations[d[i].id])
							t.stations[d[i].id] = new watch2(e.dataCnt, d[i]);
						else
							t.stations[d[i].id].refresh(d[i]);
					}
					for(i in t.stations){
						var exists = false;
						for(j=0;j<d.length;j++)
							if(d[j].id == i){
								exists = true;
								break;
							}
						if(!exists){
							if(typeof(t.stations[i].hide)=='function'){
								t.visibleStations.splice(t.visibleStations.indexOf(i), 1);
							}
						}
					}
				}
				var verimClass = d1.atolyeverim == 0 ? "v-none" : (d1.atolyeverim<85 ? "v-0" : (d1.atolyeverim>105 || d1.atolyeverim<95 ? "v-1" : "v-2"));
				e.atolyeverim.html('<span class="'+verimClass+'">%'+d1.atolyeverim+'</span>');
				verimClass = d1.hatverim == 0 ? "v-none" : (d1.hatverim<85 ? "v-0" : (d1.hatverim>105 || d1.hatverim<95 ? "v-1" : "v-2"));
				e.hatverim.html('<span class="'+verimClass+'">%'+d1.hatverim+'</span>');
				e.planlanan.html(d1.planlanan);
				e.calisan.html(d1.calisan);
				e.bosta.html(d1.bosta);
				t.els.tables = $('.data', t.els.dataCnt);
				if(t.els.page0active){
					t.els.totalWidth = 0;
					t.els.tables.each(function(i){
						var tbl = $(this);
						t.els.totalWidth += parseInt(tbl.width());
					});
				}
				if(first)
					t.next();
			}catch(err){
				console.log(err);
				//$.post( "/kaynak2/inc/service3.php", { msg: JSON.stringify(t.store.data)} );
			}
		}
	};
	module.init();
	app.modules.push(module);
	window.screenLoaded = true;
})();
window.loadData = function(data){
	try{
		$('.header span').css("color", "#000000");
		window.m = app.modules[0];
		eval('m.store.data = data;');
		eval('m.store.load();');
		window.connectionAvailable();
	}catch(err){
		console.log(err);
		//$.post( "/kaynak2/inc/service3.php", { msg: JSON.stringify(data)} );
		return "error";
	}
	return "ok";
};
window.connectionAvailable = function(){
	$('.header span').css("color", "#3399ff");
};
window.connectionLost = function(){
	$('.header span').css("color", "red");
};
