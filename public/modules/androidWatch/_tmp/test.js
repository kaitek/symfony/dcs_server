(function(){
	var module = {
		moduleId : 'mSingle',
		els : {
			view : $('<div style="position:absolute;left:0;right:0;top:0;bottom:0;background:#e7e7e7;"></div>').appendTo('body'),
			cntStationName : $('<div style="position:absolute;left:0;top:50;background:#6d7987;padding:8pt 8pt 0 8pt;"></div>').appendTo('body'),
			lblStationCode : $('<div style="position:relative;left:0;top:10;color:#ffffff;font-size:36pt;">100-15</div>').appendTo('body'),
			lblStationName : $('<div style="position:relative;left:0;top:0;color:#ffffff;font-size:60pt;">1500 TON HATTI</div>').appendTo('body'),
			cntStatus : $('<div style="position:absolute;top:70px;right:0;background:#6d7987;padding:5pt;"></div>').appendTo('body'),
			lblStatus : $('<div style="position:relative;color:#ffffff;font-size:36pt;">ÇAY PAYDOSU</div>').appendTo('body')
			/*
			lblStatus : Titanium.UI.createLabel({
				text : 'ÇAY PAYDOSU',
				color : '#ffffff',
				top : 10,
				bottom : 10,
				left : 10,
				right : 30,
				font : {
					fontSize : 36,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			lblOperation : Titanium.UI.createLabel({
				text : '16.11002/0010AF-20',
				color : '#6d7987',
				//top : 220,
				zIndex : 0,
				top : 120,
				visible : false,
				left : 40,
				font : {
					fontSize : 38,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			cntHedeflenen0 : Titanium.UI.createView({
				layout:'vertical',
				height:'100%',
				opacity:1,
				width:'24%',
				zIndex:0
			}),
			cntHedeflenen1 : Titanium.UI.createView({
				layout:'absolute',
				backgroundColor:'#74a742',
				width:'100%',
				height:'47%',
				opacity:1,
				zIndex:0
			}),
			cntHedeflenen2 : Titanium.UI.createView({
				layout:'absolute',
				backgroundColor:'#4c7e1c',
				width:'100%',
				height:'47%',
				top:'4%',
				opacity:1,
				zIndex:0
			}),
			lblHedeflenen1 : Titanium.UI.createLabel({
				text : 'Hedeflenen',
				color : '#ffffff',
				font : {
					fontSize : 42,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			lblHedeflenen2 : Titanium.UI.createLabel({
				text : '918',
				color : '#ffffff',
				font : {
					fontSize : 58,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			cntGerceklesen0 : Titanium.UI.createView({
				layout:'vertical',
				height:'100%',
				opacity:1,
				left:'1%',
				width:'24%',
				zIndex:0
			}),
			cntGerceklesen1 : Titanium.UI.createView({
				layout:'absolute',
				backgroundColor:'#e48b25',
				width:'100%',
				height:'47%',
				opacity:1,
				zIndex:0
			}),
			cntGerceklesen2 : Titanium.UI.createView({
				layout:'absolute',
				backgroundColor:'#a3641d',
				width:'100%',
				height:'47%',
				top:'4%',
				opacity:1,
				zIndex:0
			}),
			lblGerceklesen1 : Titanium.UI.createLabel({
				text : 'Gerçekleşen',
				color : '#ffffff',
				font : {
					fontSize : 42,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			lblGerceklesen2 : Titanium.UI.createLabel({
				text : '599',
				color : '#ffffff',
				font : {
					fontSize : 58,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			cntVerim0 : Titanium.UI.createView({
				layout:'vertical',
				height:'100%',
				opacity:1,
				left:'1%',
				width:'24%',
				zIndex:0
			}),
			cntVerim1 : Titanium.UI.createView({
				layout:'absolute',
				backgroundColor:'#a59d88',
				width:'100%',
				height:'47%',
				opacity:1,
				zIndex:0
			}),
			cntVerim2 : Titanium.UI.createView({
				layout:'absolute',
				backgroundColor:'#7b7156',
				width:'100%',
				height:'47%',
				top:'4%',
				opacity:1,
				zIndex:0
			}),
			lblVerim1 : Titanium.UI.createLabel({
				text : 'Verim',
				color : '#ffffff',
				font : {
					fontSize : 42,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			lblVerim2 : Titanium.UI.createLabel({
				text : '65',
				color : '#ffffff',
				font : {
					fontSize : 58,
					fontFamily : 'Cuprum-Bold'
				}
			}),
			imgSmiley : Titanium.UI.createImageView({
				backgroundColor:'#74a742',
				top:0,
				left:'1%',
				width:'24%',
				height:'98%',
				image:'/assets/face_b0.png'
			}),
			cntDisplays0 : Titanium.UI.createView({
				layout:'horizontal',
				width:'95%',
				height:240,
				opacity:1,
				zIndex:0
			}), 
			cntDisplays : Titanium.UI.createView({
				layout:'absolute',
				left:0,
				right:0,
				top:260,
				bottom:70,
				opacity:0,
				zIndex:0
			}),
			cntMarquee : Titanium.UI.createScrollView({
				backgroundColor:'#6d7987',
				layout:'absolute',
				scrollType: 'horizontal',
				showHorizontalScrollIndicator: true,
				contentWidth: 'auto',
				left:0,
				right:0,
				bottom:-46,
				height:46,
				opacity:1,
				zIndex:0
			}),
			lblMarquee : Titanium.UI.createLabel({
			    text: 'OSMAN  BAYRAM, SERKAN  CEYHAN, BASRİ  YILMAZ, MURAT  YILMAZ, ŞENOL  DAĞDELEN, BARBAROS ŞİMŞEK, EMRE FERE, İLKER  ÇINARKES, YUSUF ÇAKIR, ERSİN  ARÇELİK, SERKAN AŞKIN, ADEM TUNAHAN YAVUZ, ALİ  ÇILDIR, AYHAN  ORAKÇI, BİNALİ  ÇAĞLAYAN',
			    color:'#ffffff',
			    visible:false,
			    left:Titanium.Platform.displayCaps.platformWidth,
			    top:5,
			    width:Ti.UI.SIZE,
			    height:36,
			    horizontalWrap:false,
				font : {
					fontSize : 36,
					fontFamily : 'Cuprum-Regular'
				}
			})*/
		},
		render : function(){
			var t = this, e = t.els;
			e.cntStationName.append(e.lblStationCode);
			e.cntStationName.append(e.lblStationName);
			e.view.append(e.cntStationName);

			e.cntStatus.append(e.lblStatus);
			e.view.append(e.cntStatus);
			/*
			
			e.view.add(e.lblOperation);
			
			e.cntHedeflenen1.add(e.lblHedeflenen1);
			e.cntHedeflenen2.add(e.lblHedeflenen2);
			e.cntHedeflenen0.add(e.cntHedeflenen1);
			e.cntHedeflenen0.add(e.cntHedeflenen2);
			e.cntDisplays0.add(e.cntHedeflenen0);
			
			e.cntGerceklesen1.add(e.lblGerceklesen1);
			e.cntGerceklesen2.add(e.lblGerceklesen2);
			e.cntGerceklesen0.add(e.cntGerceklesen1);
			e.cntGerceklesen0.add(e.cntGerceklesen2);
			e.cntDisplays0.add(e.cntGerceklesen0);
			
			e.cntVerim1.add(e.lblVerim1);
			e.cntVerim2.add(e.lblVerim2);
			e.cntVerim0.add(e.cntVerim1);
			e.cntVerim0.add(e.cntVerim2);
			e.cntDisplays0.add(e.cntVerim0);
			
			e.cntDisplays0.add(e.imgSmiley);
			e.cntDisplays.add(e.cntDisplays0);
			
			e.view.add(e.cntDisplays);
			
			e.cntMarquee.add(e.lblMarquee);
			e.view.add(e.cntMarquee);
			
			Ti.App.current.renderTo.add(e.view);
			*/
		},
		init : function(){
			var t = this, e = t.els;
			t.render();
			//t.open();
		},
		open : function(){
			var t = this, e = t.els;
			/*
			e.cntStationName.left = -e.cntStationName.size.width;
			e.cntStationName.visible = true;
			
			e.cntStatus.right = -e.cntStatus.size.width;
			e.cntStatus.visible = true;
			Ti.App.anim.viewPropertyAnimator.animate(e.cntStatus).x(Titanium.Platform.displayCaps.platformWidth-e.cntStatus.size.width).setDuration(400).setInterpolator(Ti.App.anim.LINEAR_INTERPOLATOR);
				
			
			Ti.App.anim.viewPropertyAnimator.animate(e.cntStationName).x(0).setDuration(300).setInterpolator(Ti.App.anim.LINEAR_INTERPOLATOR)
			.withEndAction(function(){
				
				e.lblOperation.visible = true;
				Ti.App.anim.viewPropertyAnimator.animate(e.lblOperation).y(220).setDuration(300).setInterpolator(Ti.App.anim.LINEAR_INTERPOLATOR);
			});
					
			e.lblMarquee.visible = true;
			e.lblMarquee.animate = function(){
				Ti.App.anim.viewPropertyAnimator.animate(e.lblMarquee).setDuration(1).x(Titanium.Platform.displayCaps.platformWidth)
				.withEndAction(function(){
					Ti.App.anim.viewPropertyAnimator.animate(e.lblMarquee)
					.setStartDelay(250)
					.setInterpolator(Ti.App.anim.LINEAR_INTERPOLATOR)
			        .setDuration((e.lblMarquee.size.width/Titanium.Platform.displayCaps.platformWidth)*10000)
			        .withEndAction(e.lblMarquee.animate)
			        .x(-1*(e.lblMarquee.size.width));
				});	
			};
			e.lblMarquee.animate();
			
			Ti.App.anim.viewPropertyAnimator.animate(e.cntDisplays).opacity(1).setDuration(500).setInterpolator(Ti.App.anim.LINEAR_INTERPOLATOR);
			Ti.App.anim.viewPropertyAnimator.animate(e.cntMarquee).yBy(-56).setDuration(300).setInterpolator(Ti.App.anim.LINEAR_INTERPOLATOR);
			*/
		},
		close : function(){
			
		}
	};
	module.init();
	//Ti.App.current.module = module;
})();