<?php
$store=$_GET['isStore'] ? $_GET['isStore'] : $_POST["isStore"];
$isapp=$_GET['isApp'] ? $_GET['isApp'] : $_POST["isApp"];
$pc=$_GET['pc'] ? $_GET['pc'] : $_POST['pc'];
function get_web_page($url)
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_POST		   => true,
        CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
        CURLOPT_SSL_VERIFYHOST => false
    );

    $headers = [
        'Cache-Control: no-cache',
        'Connection: keep-alive',
        'content-length: 0',
    ];

    $ch      = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $err     = curl_errno($ch);
    $errmsg  = curl_error($ch);
    $header  = curl_getinfo($ch);
    curl_close($ch);

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}
$ret= get_web_page("https://".$_SERVER['HTTP_HOST'].$_SERVER['BASE']."/tr/AndroidWatch/RefreshData/".$pc);
if($ret['errno']!=0) {
    var_dump($ret);
    die;
} else {
    $data=$ret['content'];
}
if(isset($store)) {
    if(isset($isapp)) {
        echo $data;
    } else {
        echo '{totalProperty:1,records:'.$data.'}';
    }
    die;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="tr" lang="tr">
<head>
<title>Verimot</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php
/*
<script type="text/javascript" src="../../libs/myjui/mj-debug.js.php"></script>
<script type="text/javascript" src="../../libs/myjui/myjui/language/tr-min.js"></script>
<script type="text/javascript" src="inc/app.js"></script>
<link rel="stylesheet" type="text/css" href="fonts.css" media="all" />
<link rel="stylesheet" type="text/css" href="style.css" media="all" />
*/
$s = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$s = "http://".substr($s, 0, strrpos($s, "/")+1);
?>
<style type"text/javascript">
@font-face {
    font-family: 'CuprumBold';
    src: url('<?=$s;?>inc/fonts/cuprum-bold.eot');
    src: url('<?=$s;?>inc/fonts/cuprum-bold.eot') format('embedded-opentype'),
         url('<?=$s;?>inc/fonts/cuprum-bold.woff') format('woff'),
         url('<?=$s;?>inc/fonts/cuprum-bold.ttf') format('truetype'),
         url('<?=$s;?>inc/fonts/cuprum-bold.svg#CuprumBold') format('svg');
}
@font-face {
    font-family: 'HelveticaNeueLTStd45Light';
    src: url('<?=$s;?>inc/fonts/helveticaneueltstd-lt_0.eot');
    src: url('<?=$s;?>inc/fonts/helveticaneueltstd-lt_0.eot') format('embedded-opentype'),
         url('<?=$s;?>inc/fonts/helveticaneueltstd-lt_0.woff') format('woff'),
         url('<?=$s;?>inc/fonts/helveticaneueltstd-lt_0.ttf') format('truetype'),
         url('<?=$s;?>inc/fonts/helveticaneueltstd-lt_0.svg#HelveticaNeueLTStd45Light') format('svg');
}
@font-face {
    font-family: 'HelveticaNeueLTStd97BlackCondensed';
    src: url('<?=$s;?>inc/fonts/helveticaneueltstd-blkcn_0.eot');
    src: url('<?=$s;?>inc/fonts/helveticaneueltstd-blkcn_0.eot') format('embedded-opentype'),
         url('<?=$s;?>inc/fonts/helveticaneueltstd-blkcn_0.woff') format('woff'),
         url('<?=$s;?>inc/fonts/helveticaneueltstd-blkcn_0.ttf') format('truetype'),
         url('<?=$s;?>inc/fonts/helveticaneueltstd-blkcn_0.svg#HelveticaNeueLTStd97BlackCondensed') format('svg');
}
<?php
echo file_get_contents("fonts.css")."\n";
echo file_get_contents("style.css")."\n";
?>
</style>
</head>
<body>
<script type="text/javascript">
<?php
// window.onerror = function(errorMsg, url, lineNumber) {
//  alert(url+':'+lineNumber+' - '+errorMsg);
// };
echo "window.macId = ".$pc.";\n";
//echo "window.inBrowser = ".(strpos($_SERVER["HTTP_USER_AGENT"], "KAITEK Mobile Device")===false ? "true" : "false").";\n";
echo "window.inBrowser = false;\n";
echo file_get_contents("../../lib/myjui/mj-debug.js")."\n";
echo file_get_contents("../../lib/myjui/myjui/language/tr-min.js")."\n";
echo file_get_contents("./inc/app.js")."\n";
if($_SERVER['HTTP_HOST']=='172.16.1.149'&&$_SERVER['BASE']=='/pres'||$_SERVER['HTTP_HOST']=='192.168.1.40') {
    echo file_get_contents("./inc/presizleme.js")."\n";
} else {
    echo file_get_contents("./inc/kaynak1izleme.js")."\n";
}
echo 'loadData('.$data.');';
?>
</script>
</body>
</html>