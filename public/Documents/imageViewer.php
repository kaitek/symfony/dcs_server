<?php
    $showDeviceHome=$_GET['showDeviceHome'];
    $hideName = array('.','..','.DS_Store','Thumbs.db');
    $path=$_GET['path'];
    $text=$_GET['text'];
    $arr_docs=array("docs"=>array(),"sop"=>array());
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            * {box-sizing: border-box}
            body {font-family: Verdana, sans-serif; margin:0}
            .mySlidesSop {display: none}
            .mySlidesDoc {display: none; height:800px;}
            img {vertical-align: middle;max-height: 100%; max-width: 100%; object-fit: contain;}

            /* Slideshow container */
            .slideshow-container {
            max-width: 1800px;
            max-height: 800px;
            position: relative;
            margin: auto;
            }
            .slideshow-container-home {
            max-width: 1100px;
            max-height: 650px;
            position: relative;
            margin: auto;
            background-size: 150px;
            }

            /* Next & previous buttons */
            .prev, .next {
            background-color: rgba(0,0,0,0.5);
            cursor: pointer;
            position: absolute;
            top: 60px;
            width: auto;
            padding: 16px;
            margin-top: -22px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
            right: 0;
            border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover, .next:hover {
            background-color: rgba(0,0,0,1);
            }

            /* Caption text */
            .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
            }

            /* Number text (1/3 etc) */
            .numbertext {
            color: red;
            font-size: 20px;
            font-weight: bold;
            padding: 8px 12px;
            position: absolute;
            top: 0;
            }

            /* The dots/bullets/indicators */
            .dot {
            cursor: pointer;
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            transition: background-color 0.6s ease;
            }

            .active, .dot:hover {
            background-color: #717171;
            }

            /* Fading animation */
            .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 1.5s;
            animation-name: fade;
            animation-duration: 1.5s;
            }

            @-webkit-keyframes fade {
            from {opacity: .4} 
            to {opacity: 1}
            }

            @keyframes fade {
            from {opacity: .4} 
            to {opacity: 1}
            }

            /* On smaller screens, decrease text size */
            @media only screen and (max-width: 300px) {
            .prev, .next,.text {font-size: 11px}
            }
            /* Style the tab */
            .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
            }

            /* Style the buttons inside the tab */
            .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
            }

            /* Change background color of buttons on hover */
            .tab button:hover {
            background-color: #ddd;
            }

            /* Create an active/current tablink class */
            .tab button.active {
            background-color: #ccc;
            }

            /* Style the tab content */
            .tabcontent {
            display: none;
            padding: 6px 12px;
            }
            /* Remove default bullets */
            ul, #myUL {
            list-style-type: none;
            }

            /* Remove margins and padding from the parent ul */
            #myUL {
            margin: 0;
            padding: 0;
            }

            /* Style the caret/arrow */
            .caret {
            cursor: pointer; 
            user-select: none; /* Prevent text selection */
            }

            /* Create the caret/arrow with a unicode, and style it */
            .caret::before {
            content: "\25B6";
            color: black;
            display: inline-block;
            margin-right: 6px;
            }

            /* Rotate the caret/arrow icon when clicked on (using JavaScript) */
            .caret-down::before {
            transform: rotate(90deg); 
            }

            /* Hide the nested list */
            .nested {
            display: none;
            }

            /* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
            .treeactive {
            display: block;
            }
        </style>
    </head>
    <body>
        <div class="<?=($showDeviceHome=="1"?'slideshow-container-home':'slideshow-container')?>">
            <?php
                $str_sop='';
                $containerStyle='max-width:1800px;height:950px;';
                if($showDeviceHome=="1"){
                    $containerStyle='width:1100px;height:650px;';
                }
                if($path!==''){
                    $str_sop.='<div class="" style=" '.$containerStyle.'">
                        <div class="numbertext">'.$text.'</div>
                        <center><img src="./'.$path.'" style=" '.$containerStyle.'"></center>
                    </div>';
                }else{
                    $str_sop.='Gösterilecek resim dosyası bulunamadı!';
                }
                echo $str_sop;
            ?>
        </div>
        <!-- The dots/circles -->
        <!--<div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span> 
            <span class="dot" onclick="currentSlide(2)"></span> 
            <span class="dot" onclick="currentSlide(3)"></span> 
        </div>-->
    </body>
</html> 
