<?php
$showDeviceHome=$_GET['showDeviceHome'];
//1-cihaz ana ekranı
//2-debug için yazıldı
//3-kontrol cevap ekranı
$hideName = array('.','..','.DS_Store','Thumbs.db');
$arr_docs=array("docs"=>array(),"sop"=>array());
if($showDeviceHome=="1"||$showDeviceHome=="2"||$showDeviceHome=="3") {
    if(file_exists('./ISG/')) {
        $files = scandir('./ISG/');
        foreach($files as $filename) {
            if(!in_array($filename, $hideName)&&!is_dir($filename)) {
                $arr_docs["isg"][]=array("path"=>'./ISG/',"name"=>$filename);
            }
        }
    }
    $opnames=$_GET['c'];
    $arr_opnames=explode('|', $opnames);
    foreach($arr_opnames as $opname) {
        $opname=urldecode(implode('_', explode('/', $opname)));
        $arr_opname=explode('-', $opname);
        if(count($arr_opname)>1) {
            $opnumber=array_pop($arr_opname);
        }
        $opcode=implode('-', $arr_opname);
        if($opcode!=='') {
            if(file_exists('./SOP/'.$opcode)) {
                if(file_exists('./SOP/'.$opcode.'/'.$opname)) {
                    $files = scandir('./SOP/'.$opcode.'/'.$opname);
                    foreach($files as $filename) {
                        if(!in_array($filename, $hideName)) {
                            $arr_docs["sop"][]=array("path"=>'./SOP/'.$opcode.'/'.$opname,"name"=>$filename);
                        }
                    }
                }
            }
        }
    }
} else {
    $opname=$_GET['c'];
    $opname=implode('_', explode('/', $opname));
    $arr_opname=explode('-', $opname);
    if(count($arr_opname)>1) {
        $opnumber=array_pop($arr_opname);
    }
    $opcode=implode('-', $arr_opname);
    if(file_exists('./MSDS')) {
        $arr_docs["docs"]['MSDS']=array("path"=>'./MSDS/',"name"=>'MSDS',"data"=>array());
        $files = scandir('./MSDS/');
        foreach($files as $filename) {
            if(!in_array($filename, $hideName)) {
                $pictures = scandir('./MSDS/'.$filename);
                $tmp=array();
                foreach($pictures as $picture) {
                    if(!in_array($picture, $hideName)) {
                        $tmp[]=array("path"=>'./MSDS/'.$filename,"name"=>$picture);
                    }
                }
                $arr_docs["docs"]['MSDS']["data"][]=array("path"=>'./MSDS/'.$filename,"name"=>$filename,"data"=>$tmp);
            }
        }
    }
    if($opcode!=='') {
        if(file_exists('./SOP/'.$opcode)) {
            if(file_exists('./SOP/'.$opcode.'/'.$opname)) {
                $files = scandir('./SOP/'.$opcode.'/'.$opname);
                foreach($files as $filename) {
                    if(!in_array($filename, $hideName)) {
                        $arr_docs["sop"][]=array("path"=>'./SOP/'.$opcode.'/'.$opname,"name"=>$filename);
                    }
                }
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            * {box-sizing: border-box}
            body {font-family: Verdana, sans-serif; margin:0}
            .mySlidesSop {display: none}
            .mySlidesDoc {display: none; height:800px;}
            img {vertical-align: middle;max-height: 100%; max-width: 100%; object-fit: contain;}

            /* Slideshow container */
            .slideshow-container {
            max-width: 1800px;
            max-height: 800px;
            position: relative;
            margin: auto;
            }
            .slideshow-container-home {
            max-width: 1100px;
            max-height: 650px;
            position: relative;
            margin: auto;
            background-size: 150px;
            }

            /* Next & previous buttons */
            .prev, .next {
            background-color: rgba(0,0,0,0.5);
            cursor: pointer;
            position: absolute;
            top: 60px;
            width: auto;
            padding: 16px;
            margin-top: -22px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
            right: 0;
            border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover, .next:hover {
            background-color: rgba(0,0,0,1);
            }

            /* Caption text */
            .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
            }

            /* Number text (1/3 etc) */
            .numbertext {
            color: red;
            font-size: 20px;
            font-weight: bold;
            padding: 8px 12px;
            position: absolute;
            top: 0;
            }

            /* The dots/bullets/indicators */
            .dot {
            cursor: pointer;
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            transition: background-color 0.6s ease;
            }

            .active, .dot:hover {
            background-color: #717171;
            }

            /* Fading animation */
            .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 1.5s;
            animation-name: fade;
            animation-duration: 1.5s;
            }

            @-webkit-keyframes fade {
            from {opacity: .4} 
            to {opacity: 1}
            }

            @keyframes fade {
            from {opacity: .4} 
            to {opacity: 1}
            }

            /* On smaller screens, decrease text size */
            @media only screen and (max-width: 300px) {
            .prev, .next,.text {font-size: 11px}
            }
            /* Style the tab */
            .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
            }

            /* Style the buttons inside the tab */
            .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
            }

            /* Change background color of buttons on hover */
            .tab button:hover {
            background-color: #ddd;
            }

            /* Create an active/current tablink class */
            .tab button.active {
            background-color: #ccc;
            }

            /* Style the tab content */
            .tabcontent {
            display: none;
            padding: 6px 12px;
            }
            /* Remove default bullets */
            ul, #myUL {
            list-style-type: none;
            }

            /* Remove margins and padding from the parent ul */
            #myUL {
            margin: 0;
            padding: 0;
            }

            /* Style the caret/arrow */
            .caret {
            cursor: pointer; 
            user-select: none; /* Prevent text selection */
            }

            /* Create the caret/arrow with a unicode, and style it */
            .caret::before {
            content: "\25B6";
            color: black;
            display: inline-block;
            margin-right: 6px;
            }

            /* Rotate the caret/arrow icon when clicked on (using JavaScript) */
            .caret-down::before {
            transform: rotate(90deg); 
            }

            /* Hide the nested list */
            .nested {
            display: none;
            }

            /* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
            .treeactive {
            display: block;
            }
        </style>
    </head>
    <body>
        <div class="<?=($showDeviceHome=="1" ? 'slideshow-container-home' : 'slideshow-container')?>">
            <?php
                $idx_isg=count($arr_docs["isg"]);
$idx_sop=count($arr_docs["sop"]);
$str_sop='';
$containerStyle='max-width:1800px;height:950px;';
if($showDeviceHome=="1") {
    $containerStyle='width:1100px;height:650px;';
}
if($idx_isg>0||$idx_sop>0) {
    $i=1;
    if($idx_isg>0) {
        foreach($arr_docs["isg"] as $row) {
            $str_sop.='<div class="mySlidesSop fade" style=" '.$containerStyle.'">
                                <div class="numbertext">'.$i++.' / '.($idx_isg+$idx_sop).'</div>
                                <center><img src="'.$row['path'].'/'.$row['name'].'?'.time().'" style=" '.$containerStyle.'"></center>
                            </div>';
        }
    }
    if($idx_sop>0) {
        foreach($arr_docs["sop"] as $row) {
            $str_sop.='<div class="mySlidesSop fade" style=" '.$containerStyle.'">
                                <div class="numbertext">'.$i++.' / '.($idx_isg+$idx_sop).'</div>
                                <center><img src="'.$row['path'].'/'.$row['name'].'?'.time().'" style=" '.$containerStyle.'"></center>
                            </div>';
        }
    }
    if($showDeviceHome!=="1") {
        $str_sop.='<a class="prev" onclick="plusSlides(-1)">&#10094;</a>';
        $str_sop.='<a class="next" onclick="plusSlides(1)">&#10095;</a>';
    }
} else {
    $str_sop.='Gösterilecek resim dosyası bulunamadı!';
}
if(count($arr_docs["docs"])>0) {
    $str_folder="";
    foreach($arr_docs["docs"] as $row) {
        $tmp_folder='';
        $tmp_slides='';
        if(count($row["data"])>0) {
            $tmp_folder.='<ul class="nested">';
            foreach($row["data"] as $sub_folder) {
                if(count($sub_folder["data"])>0) {
                    $tmp_folder.='<li><span class="caret subfolder">'.$sub_folder['name'].'</span></li>';
                    $i=1;
                    $tmp_slides.='<div id="mySlidesDoc-'.$sub_folder['name'].'" style="display:none;">';
                    foreach($sub_folder["data"] as $sub_item) {
                        $tmp_slides.='<div class="mySlidesDoc mySlidesDoc-'.$sub_folder['name'].' fade">
                                            <div class="numbertext">'.$i++.' / '.count($sub_folder["data"]).'</div>
                                            <center><img src="'.$sub_item['path'].'/'.$sub_item['name'].'?'.time().'" style="position:absolute;top:30px;left:0px;"></center>
                                        </div>';
                    }
                    $tmp_slides.='<a class="prev" onclick="plusSlides(-1)" style="position:absolute;left:0px;">&#10094;</a>';
                    $tmp_slides.='<a class="next" onclick="plusSlides(1)">&#10095;</a>';
                    $tmp_slides.='</div>';
                }
            }
            $tmp_folder.='</ul>';
            $str_folder.='<li><span class="caret">'.$row['name'].'</span>
                                '.$tmp_folder.'
                            </li>';
        } else {
            $str_folder.='<li>'.$row['name'].'</li>';
        }
    }
    $str='<div style="height:50px;width:100%;">
                        <center>
                            <div class="tab" style="width:195px;">
                                <button '.($idx_sop==0 ? 'disabled' : '').' class="tablinks '.($idx_sop>0 ? 'active' : '').'" onclick="openTab(event, \'sop\')">SOP</button>
                                <button class="tablinks '.($idx_sop==0 ? 'active' : '').'" onclick="openTab(event, \'docs\')">Dokümanlar</button>
                            </div>
                        </center>
                    </div>
                    '.($idx_sop>0 ? '
                    <div id="sop" class="tabcontent" style="display:block;">
                        '.$str_sop.'
                    </div>'
    : '').'

                    <div id="docs" class="tabcontent" style="display:'.($idx_sop==0 ? 'block' : 'none').';" >
                        <div class="filetree" style="width:300px;height:100%;float:left;">
                            <ul id="myUL">
                                '.$str_folder.'
                            </ul>
                        </div>
                        <div class="filebrowser" style="width:1500px;height:100%;position:relative;left:300px;">
                            '.$tmp_slides.'
                        </div>
                    </div>
                    ';
    echo $str;
} else {
    echo $str_sop;
}
?>
        </div>
        <!-- The dots/circles -->
        <!--<div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span> 
            <span class="dot" onclick="currentSlide(2)"></span> 
            <span class="dot" onclick="currentSlide(3)"></span> 
        </div>-->
        <script>
            var showDeviceHome='<?=$showDeviceHome?>';
            var slideIndex = 1;
            var activeSlideClass = 'mySlidesSop';
            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            function currentSlide(n) {
                showSlides(slideIndex = n);
            }
            function openTab(evt, tabName) {
                slideIndex=1;
                if(tabName=='sop'){
                    activeSlideClass = 'mySlidesSop';
                }else{
                    activeSlideClass = 'mySlidesDoc';
                }
                // Declare all variables
                var i, tabcontent, tablinks;

                // Get all elements with class="tabcontent" and hide them
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }

                // Get all elements with class="tablinks" and remove the class "active"
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }

                // Show the current tab, and add an "active" class to the button that opened the tab
                document.getElementById(tabName).style.display = "block";
                evt.currentTarget.className += " active";
            }
            function showSlides(n) {
                try {
                    var i;
                    var slides = document.getElementsByClassName(activeSlideClass);
                    if(slides.length>0){
                        //var dots = document.getElementsByClassName("dot");
                        //if (n > slides.length) {slideIndex = 1}    
                        //if (n < 1) {slideIndex = slides.length}
                        if (n > slides.length) {
                            if(showDeviceHome==1){
                                slideIndex = 1;
                            }else{
                                slideIndex = slides.length
                            }
                        }  
                        if (n < 1) {slideIndex = 1}
                        for (i = 0; i < slides.length; i++) {
                            slides[i].style.display = "none";  
                        }
                        //for (i = 0; i < dots.length; i++) {
                        //    dots[i].className = dots[i].className.replace(" active", "");
                        //}
                        slides[slideIndex-1].style.display = "block";  
                        //dots[slideIndex-1].className += " active";
                    }
                } catch (error) {
                    console.log(error);
                }
            }
            showSlides(slideIndex);
            var toggler = document.getElementsByClassName("caret");
            var i;
            for (i = 0; i < toggler.length; i++) {
                toggler[i].addEventListener("click", function() {
                    if(this.className=='caret'){
                        this.parentElement.querySelector(".nested").classList.toggle("treeactive");
                        this.classList.toggle("caret-down");
                    }else{
                        var parentSlide=document.getElementById(activeSlideClass.replace('mySlidesDoc ',''));
                        if(parentSlide){
                            parentSlide.style.display = "none";
                        }
                        var parentSlide=document.getElementsByClassName(activeSlideClass);
                        if(parentSlide.length>0){
                            for(var i=0;i<parentSlide.length;i++){
                                parentSlide[i].style.display = "none";
                            }
                        }
                        var currentSlide=document.getElementById('mySlidesDoc-'+this.innerHTML);
                        if(currentSlide){
                            currentSlide.style.display = "block";
                        }
                        activeSlideClass='mySlidesDoc mySlidesDoc-'+this.innerHTML;
                        slideIndex=1;
                        showSlides(slideIndex);
                    }
                });
            }
            if(showDeviceHome==1){
                setInterval(function(){
                    plusSlides(1);
                }, 10000);
            }
        </script>

    </body>
</html> 
