//!!! myjui/js/jquery/jquery-2.1.4.js
/*!
 * jQuery JavaScript Library v2.1.4
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-04-28T16:01Z
 */

(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Support: Firefox 18+
// Can't be in strict mode, several libs including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
//

var arr = [];

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var support = {};



var
	// Use the correct document accordingly with window argument (sandbox)
	document = window.document,

	version = "2.1.4",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android<4.1
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {
	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return just the one element from the set
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return all the elements in a clean array
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend({
	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray,

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {
		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		// adding 1 corrects loss of precision from parseFloat (#15100)
		return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
	},

	isPlainObject: function( obj ) {
		// Not plain objects:
		// - Any object or value whose internal [[Class]] property is not "[object Object]"
		// - DOM nodes
		// - window
		if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		if ( obj.constructor &&
				!hasOwn.call( obj.constructor.prototype, "isPrototypeOf" ) ) {
			return false;
		}

		// If the function hasn't returned already, we're confident that
		// |obj| is a plain object, created by {} or constructed with new Object
		return true;
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		// Support: Android<4.0, iOS<6 (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call(obj) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		var script,
			indirect = eval;

		code = jQuery.trim( code );

		if ( code ) {
			// If the code includes a valid, prologue position
			// strict mode pragma, execute code by injecting a
			// script tag into the document.
			if ( code.indexOf("use strict") === 1 ) {
				script = document.createElement("script");
				script.text = code;
				document.head.appendChild( script ).parentNode.removeChild( script );
			} else {
			// Otherwise, avoid the DOM node creation, insertion
			// and removal by using an indirect global eval
				indirect( code );
			}
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE9-11+
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	// args is for internal usage only
	each: function( obj, callback, args ) {
		var value,
			i = 0,
			length = obj.length,
			isArray = isArraylike( obj );

		if ( args ) {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			}

		// A special, fast, case for the most common use of each
		} else {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			}
		}

		return obj;
	},

	// Support: Android<4.1
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArraylike( Object(arr) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value,
			i = 0,
			length = elems.length,
			isArray = isArraylike( elems ),
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
});

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function isArraylike( obj ) {

	// Support: iOS 8.2 (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = "length" in obj && obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	if ( obj.nodeType === 1 && length ) {
		return true;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.2.0-pre
 * http://sizzlejs.com/
 *
 * Copyright 2008, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-12-16
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// http://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",
	// http://www.w3.org/TR/css3-syntax/#characters
	characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Loosely modeled on CSS identifier characters
	// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
	// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = characterEncoding.replace( "w", "w#" ),

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + characterEncoding + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + characterEncoding + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + characterEncoding + ")" ),
		"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
		"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var match, elem, m, nodeType,
		// QSA vars
		i, groups, old, nid, newContext, newSelector;

	if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
		setDocument( context );
	}

	context = context || document;
	results = results || [];
	nodeType = context.nodeType;

	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	if ( !seed && documentIsHTML ) {

		// Try to shortcut find operations when possible (e.g., not under DocumentFragment)
		if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {
			// Speed-up: Sizzle("#ID")
			if ( (m = match[1]) ) {
				if ( nodeType === 9 ) {
					elem = context.getElementById( m );
					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document (jQuery #6963)
					if ( elem && elem.parentNode ) {
						// Handle the case where IE, Opera, and Webkit return items
						// by name instead of ID
						if ( elem.id === m ) {
							results.push( elem );
							return results;
						}
					} else {
						return results;
					}
				} else {
					// Context is not a document
					if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
						contains( context, elem ) && elem.id === m ) {
						results.push( elem );
						return results;
					}
				}

			// Speed-up: Sizzle("TAG")
			} else if ( match[2] ) {
				push.apply( results, context.getElementsByTagName( selector ) );
				return results;

			// Speed-up: Sizzle(".CLASS")
			} else if ( (m = match[3]) && support.getElementsByClassName ) {
				push.apply( results, context.getElementsByClassName( m ) );
				return results;
			}
		}

		// QSA path
		if ( support.qsa && (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
			nid = old = expando;
			newContext = context;
			newSelector = nodeType !== 1 && selector;

			// qSA works strangely on Element-rooted queries
			// We can work around this by specifying an extra ID on the root
			// and working up from there (Thanks to Andrew Dupont for the technique)
			// IE 8 doesn't work on object elements
			if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
				groups = tokenize( selector );

				if ( (old = context.getAttribute("id")) ) {
					nid = old.replace( rescape, "\\$&" );
				} else {
					context.setAttribute( "id", nid );
				}
				nid = "[id='" + nid + "'] ";

				i = groups.length;
				while ( i-- ) {
					groups[i] = nid + toSelector( groups[i] );
				}
				newContext = rsibling.test( selector ) && testContext( context.parentNode ) || context;
				newSelector = groups.join(",");
			}

			if ( newSelector ) {
				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch(qsaError) {
				} finally {
					if ( !old ) {
						context.removeAttribute("id");
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = attrs.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, parent,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// If no document and documentElement is available, return
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Set our document
	document = doc;
	docElem = doc.documentElement;
	parent = doc.defaultView;

	// Support: IE>8
	// If iframe document is assigned to "document" variable and if iframe has been reloaded,
	// IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
	// IE6-8 do not support the defaultView property so parent will be undefined
	if ( parent && parent !== parent.top ) {
		// IE11 does not have attachEvent, so all must suffer
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", unloadHandler, false );
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Support tests
	---------------------------------------------------------------------- */
	documentIsHTML = !isXML( doc );

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( doc.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( doc.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !doc.getElementsByName || !doc.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var m = context.getElementById( id );
				// Check parentNode to catch when Blackberry 4.6 returns
				// nodes that are no longer in the document #6963
				return m && m.parentNode ? [ m ] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\f]' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( div.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.2+, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.7+
			if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibing-combinator selector` fails
			if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = doc.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully does not implement inclusive descendent
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === doc ? -1 :
				b === doc ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return doc;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, outerCache, node, diff, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {
							// Seek `elem` from a previously-cached index
							outerCache = parent[ expando ] || (parent[ expando ] = {});
							cache = outerCache[ type ] || [];
							nodeIndex = cache[0] === dirruns && cache[1];
							diff = cache[0] === dirruns && cache[2];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									outerCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						// Use previously-cached element index if available
						} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
							diff = cache[1];

						// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
						} else {
							// Use the same loop as above to seek `elem` from the start
							while ( (node = ++nodeIndex && node && node[ dir ] ||
								(diff = nodeIndex = 0) || start.pop()) ) {

								if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
									// Cache the index of each encountered element
									if ( useCache ) {
										(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
									}

									if ( node === elem ) {
										break;
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});
						if ( (oldCache = outerCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							outerCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context !== document && context;
			}

			// Add elements passing elementMatchers directly to results
			// Keep `i` a string if there are no elements so `matchedCount` will be "00" below
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// Apply set filters to unmatched elements
			matchedCount += i;
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is no seed and only one group
	if ( match.length === 1 ) {

		// Take a shortcut and set the context if the root selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				support.getById && context.nodeType === 9 && documentIsHTML &&
				Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		});

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		});

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) >= 0 ) !== not;
	});
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		}));
};

jQuery.fn.extend({
	find: function( selector ) {
		var i,
			len = this.length,
			ret = [],
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter(function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			}) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow(this, selector || [], false) );
	},
	not: function( selector ) {
		return this.pushStack( winnow(this, selector || [], true) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
});


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[0] === "<" && selector[ selector.length - 1 ] === ">" && selector.length >= 3 ) {
				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[1],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {
							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[2] );

					// Support: Blackberry 4.6
					// gEBID returns nodes no longer in the document (#6963)
					if ( elem && elem.parentNode ) {
						// Inject the element directly into the jQuery object
						this.length = 1;
						this[0] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || rootjQuery ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[0] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return typeof rootjQuery.ready !== "undefined" ?
				rootjQuery.ready( selector ) :
				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.extend({
	dir: function( elem, dir, until ) {
		var matched = [],
			truncate = until !== undefined;

		while ( (elem = elem[ dir ]) && elem.nodeType !== 9 ) {
			if ( elem.nodeType === 1 ) {
				if ( truncate && jQuery( elem ).is( until ) ) {
					break;
				}
				matched.push( elem );
			}
		}
		return matched;
	},

	sibling: function( n, elem ) {
		var matched = [];

		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				matched.push( n );
			}
		}

		return matched;
	}
});

jQuery.fn.extend({
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter(function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[i] ) ) {
					return true;
				}
			}
		});
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
				// Always skip document fragments
				if ( cur.nodeType < 11 && (pos ?
					pos.index(cur) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector(cur, selectors)) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.unique( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.unique(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter(selector)
		);
	}
});

function sibling( cur, dir ) {
	while ( (cur = cur[dir]) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return elem.contentDocument || jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {
			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.unique( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
});
var rnotwhite = (/\S+/g);



// String to Object options format cache
var optionsCache = {};

// Convert String-formatted options into Object-formatted ones and store in cache
function createOptions( options ) {
	var object = optionsCache[ options ] = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	});
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		( optionsCache[ options ] || createOptions( options ) ) :
		jQuery.extend( {}, options );

	var // Last fire value (for non-forgettable lists)
		memory,
		// Flag to know if list was already fired
		fired,
		// Flag to know if list is currently firing
		firing,
		// First callback to fire (used internally by add and fireWith)
		firingStart,
		// End of the loop when firing
		firingLength,
		// Index of currently firing callback (modified by remove if needed)
		firingIndex,
		// Actual callback list
		list = [],
		// Stack of fire calls for repeatable lists
		stack = !options.once && [],
		// Fire callbacks
		fire = function( data ) {
			memory = options.memory && data;
			fired = true;
			firingIndex = firingStart || 0;
			firingStart = 0;
			firingLength = list.length;
			firing = true;
			for ( ; list && firingIndex < firingLength; firingIndex++ ) {
				if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
					memory = false; // To prevent further calls using add
					break;
				}
			}
			firing = false;
			if ( list ) {
				if ( stack ) {
					if ( stack.length ) {
						fire( stack.shift() );
					}
				} else if ( memory ) {
					list = [];
				} else {
					self.disable();
				}
			}
		},
		// Actual Callbacks object
		self = {
			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {
					// First, we save the current length
					var start = list.length;
					(function add( args ) {
						jQuery.each( args, function( _, arg ) {
							var type = jQuery.type( arg );
							if ( type === "function" ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && type !== "string" ) {
								// Inspect recursively
								add( arg );
							}
						});
					})( arguments );
					// Do we need to add the callbacks to the
					// current firing batch?
					if ( firing ) {
						firingLength = list.length;
					// With memory, if we're not firing then
					// we should call right away
					} else if ( memory ) {
						firingStart = start;
						fire( memory );
					}
				}
				return this;
			},
			// Remove a callback from the list
			remove: function() {
				if ( list ) {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
							// Handle firing indexes
							if ( firing ) {
								if ( index <= firingLength ) {
									firingLength--;
								}
								if ( index <= firingIndex ) {
									firingIndex--;
								}
							}
						}
					});
				}
				return this;
			},
			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
			},
			// Remove all callbacks from the list
			empty: function() {
				list = [];
				firingLength = 0;
				return this;
			},
			// Have the list do nothing anymore
			disable: function() {
				list = stack = memory = undefined;
				return this;
			},
			// Is it disabled?
			disabled: function() {
				return !list;
			},
			// Lock the list in its current state
			lock: function() {
				stack = undefined;
				if ( !memory ) {
					self.disable();
				}
				return this;
			},
			// Is it locked?
			locked: function() {
				return !stack;
			},
			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( list && ( !fired || stack ) ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					if ( firing ) {
						stack.push( args );
					} else {
						fire( args );
					}
				}
				return this;
			},
			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},
			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend({

	Deferred: function( func ) {
		var tuples = [
				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks("memory") ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[1] ](function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.done( newDefer.resolve )
										.fail( newDefer.reject )
										.progress( newDefer.notify );
								} else {
									newDefer[ tuple[ 0 ] + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
								}
							});
						});
						fns = null;
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[1] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(function() {
					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[0] ] = function() {
				deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[0] + "With" ] = list.fireWith;
		});

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred. If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );
					} else if ( !( --remaining ) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// Add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject )
						.progress( updateFunc( i, progressContexts, progressValues ) );
				} else {
					--remaining;
				}
			}
		}

		// If we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
});


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend({
	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.triggerHandler ) {
			jQuery( document ).triggerHandler( "ready" );
			jQuery( document ).off( "ready" );
		}
	}
});

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed, false );
	window.removeEventListener( "load", completed, false );
	jQuery.ready();
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called after the browser event has already occurred.
		// We once tried to use readyState "interactive" here, but it caused issues like the one
		// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			setTimeout( jQuery.ready );

		} else {

			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed, false );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed, false );
		}
	}
	return readyList.promise( obj );
};

// Kick off the DOM ready check even if the user does not
jQuery.ready.promise();




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {
			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			len ? fn( elems[0], key ) : emptyGet;
};


/**
 * Determines whether an object can have data
 */
jQuery.acceptData = function( owner ) {
	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	/* jshint -W018 */
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};


function Data() {
	// Support: Android<4,
	// Old WebKit does not have Object.preventExtensions/freeze method,
	// return new empty object instead with no [[set]] accessor
	Object.defineProperty( this.cache = {}, 0, {
		get: function() {
			return {};
		}
	});

	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;
Data.accepts = jQuery.acceptData;

Data.prototype = {
	key: function( owner ) {
		// We can accept data for non-element nodes in modern browsers,
		// but we should not, see #8335.
		// Always return the key for a frozen object.
		if ( !Data.accepts( owner ) ) {
			return 0;
		}

		var descriptor = {},
			// Check if the owner object already has a cache key
			unlock = owner[ this.expando ];

		// If not, create one
		if ( !unlock ) {
			unlock = Data.uid++;

			// Secure it in a non-enumerable, non-writable property
			try {
				descriptor[ this.expando ] = { value: unlock };
				Object.defineProperties( owner, descriptor );

			// Support: Android<4
			// Fallback to a less secure definition
			} catch ( e ) {
				descriptor[ this.expando ] = unlock;
				jQuery.extend( owner, descriptor );
			}
		}

		// Ensure the cache object
		if ( !this.cache[ unlock ] ) {
			this.cache[ unlock ] = {};
		}

		return unlock;
	},
	set: function( owner, data, value ) {
		var prop,
			// There may be an unlock assigned to this node,
			// if there is no entry for this "owner", create one inline
			// and set the unlock as though an owner entry had always existed
			unlock = this.key( owner ),
			cache = this.cache[ unlock ];

		// Handle: [ owner, key, value ] args
		if ( typeof data === "string" ) {
			cache[ data ] = value;

		// Handle: [ owner, { properties } ] args
		} else {
			// Fresh assignments by object are shallow copied
			if ( jQuery.isEmptyObject( cache ) ) {
				jQuery.extend( this.cache[ unlock ], data );
			// Otherwise, copy the properties one-by-one to the cache object
			} else {
				for ( prop in data ) {
					cache[ prop ] = data[ prop ];
				}
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		// Either a valid cache is found, or will be created.
		// New caches will be created and the unlock returned,
		// allowing direct access to the newly created
		// empty data object. A valid owner object must be provided.
		var cache = this.cache[ this.key( owner ) ];

		return key === undefined ?
			cache : cache[ key ];
	},
	access: function( owner, key, value ) {
		var stored;
		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				((key && typeof key === "string") && value === undefined) ) {

			stored = this.get( owner, key );

			return stored !== undefined ?
				stored : this.get( owner, jQuery.camelCase(key) );
		}

		// [*]When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i, name, camel,
			unlock = this.key( owner ),
			cache = this.cache[ unlock ];

		if ( key === undefined ) {
			this.cache[ unlock ] = {};

		} else {
			// Support array or space separated string of keys
			if ( jQuery.isArray( key ) ) {
				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = key.concat( key.map( jQuery.camelCase ) );
			} else {
				camel = jQuery.camelCase( key );
				// Try the string as a key before any manipulation
				if ( key in cache ) {
					name = [ key, camel ];
				} else {
					// If a key with the spaces exists, use it.
					// Otherwise, create an array by matching non-whitespace
					name = camel;
					name = name in cache ?
						[ name ] : ( name.match( rnotwhite ) || [] );
				}
			}

			i = name.length;
			while ( i-- ) {
				delete cache[ name[ i ] ];
			}
		}
	},
	hasData: function( owner ) {
		return !jQuery.isEmptyObject(
			this.cache[ owner[ this.expando ] ] || {}
		);
	},
	discard: function( owner ) {
		if ( owner[ this.expando ] ) {
			delete this.cache[ owner[ this.expando ] ];
		}
	}
};
var data_priv = new Data();

var data_user = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /([A-Z])/g;

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :
					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch( e ) {}

			// Make sure we set the data so it isn't changed later
			data_user.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend({
	hasData: function( elem ) {
		return data_user.hasData( elem ) || data_priv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return data_user.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		data_user.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to data_priv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return data_priv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		data_priv.remove( elem, name );
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = data_user.get( elem );

				if ( elem.nodeType === 1 && !data_priv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE11+
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice(5) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					data_priv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				data_user.set( this, key );
			});
		}

		return access( this, function( value ) {
			var data,
				camelKey = jQuery.camelCase( key );

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {
				// Attempt to get data from the cache
				// with the key as-is
				data = data_user.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to get data from the cache
				// with the key camelized
				data = data_user.get( elem, camelKey );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, camelKey, undefined );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each(function() {
				// First, attempt to store a copy or reference of any
				// data that might've been store with a camelCased key.
				var data = data_user.get( this, camelKey );

				// For HTML5 data-* attribute interop, we have to
				// store property names with dashes in a camelCase form.
				// This might not apply to all properties...*
				data_user.set( this, camelKey, value );

				// *... In the case of properties that might _actually_
				// have dashes, we need to also store a copy of that
				// unchanged property.
				if ( key.indexOf("-") !== -1 && data !== undefined ) {
					data_user.set( this, key, value );
				}
			});
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each(function() {
			data_user.remove( this, key );
		});
	}
});


jQuery.extend({
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = data_priv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray( data ) ) {
					queue = data_priv.access( elem, type, jQuery.makeArray(data) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return data_priv.get( elem, key ) || data_priv.access( elem, key, {
			empty: jQuery.Callbacks("once memory").add(function() {
				data_priv.remove( elem, [ type + "queue", key ] );
			})
		});
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
			this :
			this.each(function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = data_priv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
});
var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {
		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
	};

var rcheckableType = (/^(?:checkbox|radio)$/i);



(function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Safari<=5.1
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Safari<=5.1, Android<4.2
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<=11+
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
})();
var strundefined = typeof undefined;



support.focusinBubbles = "onfocusin" in window;


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu)|click/,
	rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = data_priv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !(events = elemData.events) ) {
			events = elemData.events = {};
		}
		if ( !(eventHandle = elemData.handle) ) {
			eventHandle = elemData.handle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== strundefined && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !(handlers = events[ type ]) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle, false );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = data_priv.hasData( elem ) && data_priv.get( elem );

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			delete elemData.handle;
			data_priv.remove( elem, "events" );
		}
	},

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf(".") >= 0 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf(":") < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join(".");
		event.namespace_re = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === (elem.ownerDocument || document) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( data_priv.get( cur, "events" ) || {} )[ event.type ] && data_priv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
				jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, j, ret, matched, handleObj,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( data_priv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
							.apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( (event.result = ret) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, matches, sel, handleObj,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.disabled !== true || event.type !== "click" ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) >= 0 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, handlers: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
		}

		return handlerQueue;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
		filter: function( event, original ) {
			var eventDoc, doc, body,
				button = original.button;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: Cordova 2.5 (WebKit) (#13255)
		// All events should have a target; Cordova deviceready doesn't
		if ( !event.target ) {
			event.target = document;
		}

		// Support: Safari 6.0+, Chrome<28
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	special: {
		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {
			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {
			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	},

	simulate: function( type, elem, event, bubble ) {
		// Piggyback on a donor event to simulate a different one.
		// Fake originalEvent to avoid donor's stopPropagation, but if the
		// simulated event prevents default then we do the same on the donor.
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true,
				originalEvent: {}
			}
		);
		if ( bubble ) {
			jQuery.event.trigger( e, null, elem );
		} else {
			jQuery.event.dispatch.call( elem, e );
		}
		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle, false );
	}
};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&
				// Support: Android<4.0
				src.returnValue === false ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && e.preventDefault ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && e.stopPropagation ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && e.stopImmediatePropagation ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
// Support: Chrome 15+
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// Support: Firefox, Chrome, Safari
// Create "bubbling" focus and blur events
if ( !support.focusinBubbles ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
			};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = data_priv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				data_priv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = data_priv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					data_priv.remove( doc, fix );

				} else {
					data_priv.access( doc, fix, attaches );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
		var origFn, type;

		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				this.on( type, selector, data, types[ type ], one );
			}
			return this;
		}

		if ( data == null && fn == null ) {
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return this;
		}

		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return this.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		});
	},
	one: function( types, selector, data, fn ) {
		return this.on( types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		var elem = this[0];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
});


var
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
	rtagName = /<([\w:]+)/,
	rhtml = /<|&#?\w+;/,
	rnoInnerhtml = /<(?:script|style|link)/i,
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptType = /^$|\/(?:java|ecma)script/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

	// We have to close these tags to support XHTML (#13200)
	wrapMap = {

		// Support: IE9
		option: [ 1, "<select multiple='multiple'>", "</select>" ],

		thead: [ 1, "<table>", "</table>" ],
		col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

		_default: [ 0, "", "" ]
	};

// Support: IE9
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

// Support: 1.x compatibility
// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName("tbody")[0] ||
			elem.appendChild( elem.ownerDocument.createElement("tbody") ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute("type");
	}

	return elem;
}

// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		data_priv.set(
			elems[ i ], "globalEval", !refElements || data_priv.get( refElements[ i ], "globalEval" )
		);
	}
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( data_priv.hasData( src ) ) {
		pdataOld = data_priv.access( src );
		pdataCur = data_priv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( data_user.hasData( src ) ) {
		udataOld = data_user.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		data_user.set( dest, udataCur );
	}
}

function getAll( context, tag ) {
	var ret = context.getElementsByTagName ? context.getElementsByTagName( tag || "*" ) :
			context.querySelectorAll ? context.querySelectorAll( tag || "*" ) :
			[];

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], ret ) :
		ret;
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

jQuery.extend({
	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	buildFragment: function( elems, context, scripts, selection ) {
		var elem, tmp, tag, wrap, contains, j,
			fragment = context.createDocumentFragment(),
			nodes = [],
			i = 0,
			l = elems.length;

		for ( ; i < l; i++ ) {
			elem = elems[ i ];

			if ( elem || elem === 0 ) {

				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
					// Support: QtWebKit, PhantomJS
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );

				// Convert html into DOM nodes
				} else {
					tmp = tmp || fragment.appendChild( context.createElement("div") );

					// Deserialize a standard representation
					tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;
					tmp.innerHTML = wrap[ 1 ] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[ 2 ];

					// Descend through wrappers to the right content
					j = wrap[ 0 ];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}

					// Support: QtWebKit, PhantomJS
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, tmp.childNodes );

					// Remember the top-level container
					tmp = fragment.firstChild;

					// Ensure the created nodes are orphaned (#12392)
					tmp.textContent = "";
				}
			}
		}

		// Remove wrapper from fragment
		fragment.textContent = "";

		i = 0;
		while ( (elem = nodes[ i++ ]) ) {

			// #4087 - If origin and destination elements are the same, and this is
			// that element, do not do anything
			if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
				continue;
			}

			contains = jQuery.contains( elem.ownerDocument, elem );

			// Append to fragment
			tmp = getAll( fragment.appendChild( elem ), "script" );

			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}

			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( (elem = tmp[ j++ ]) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}

		return fragment;
	},

	cleanData: function( elems ) {
		var data, elem, type, key,
			special = jQuery.event.special,
			i = 0;

		for ( ; (elem = elems[ i ]) !== undefined; i++ ) {
			if ( jQuery.acceptData( elem ) ) {
				key = elem[ data_priv.expando ];

				if ( key && (data = data_priv.cache[ key ]) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}
					if ( data_priv.cache[ key ] ) {
						// Discard any remaining `private` data
						delete data_priv.cache[ key ];
					}
				}
			}
			// Discard any remaining `user` data
			delete data_user.cache[ elem[ data_user.expando ] ];
		}
	}
});

jQuery.fn.extend({
	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each(function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				});
		}, null, value, arguments.length );
	},

	append: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		});
	},

	before: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		});
	},

	after: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		});
	},

	remove: function( selector, keepData /* Internal Use Only */ ) {
		var elem,
			elems = selector ? jQuery.filter( selector, this ) : this,
			i = 0;

		for ( ; (elem = elems[i]) != null; i++ ) {
			if ( !keepData && elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem ) );
			}

			if ( elem.parentNode ) {
				if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
					setGlobalEval( getAll( elem, "script" ) );
				}
				elem.parentNode.removeChild( elem );
			}
		}

		return this;
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; (elem = this[i]) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map(function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = value.replace( rxhtmlTag, "<$1></$2>" );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var arg = arguments[ 0 ];

		// Make the changes, replacing each context element with the new content
		this.domManip( arguments, function( elem ) {
			arg = this.parentNode;

			jQuery.cleanData( getAll( this ) );

			if ( arg ) {
				arg.replaceChild( elem, this );
			}
		});

		// Force removal if there was no new content (e.g., from empty arguments)
		return arg && (arg.length || arg.nodeType) ? this : this.remove();
	},

	detach: function( selector ) {
		return this.remove( selector, true );
	},

	domManip: function( args, callback ) {

		// Flatten any nested arrays
		args = concat.apply( [], args );

		var fragment, first, scripts, hasScripts, node, doc,
			i = 0,
			l = this.length,
			set = this,
			iNoClone = l - 1,
			value = args[ 0 ],
			isFunction = jQuery.isFunction( value );

		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return this.each(function( index ) {
				var self = set.eq( index );
				if ( isFunction ) {
					args[ 0 ] = value.call( this, index, self.html() );
				}
				self.domManip( args, callback );
			});
		}

		if ( l ) {
			fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
			first = fragment.firstChild;

			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}

			if ( first ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;

				// Use the original fragment for the last item instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;

					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );

						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
							// Support: QtWebKit
							// jQuery.merge because push.apply(_, arraylike) throws
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}

					callback.call( this[ i ], node, i );
				}

				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;

					// Reenable scripts
					jQuery.map( scripts, restoreScript );

					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!data_priv.access( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

							if ( node.src ) {
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
							}
						}
					}
				}
			}
		}

		return this;
	}
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: QtWebKit
			// .get() because push.apply(_, arraylike) throws
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
});


var iframe,
	elemdisplay = {};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var style,
		elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		// getDefaultComputedStyle might be reliably used only on attached element
		display = window.getDefaultComputedStyle && ( style = window.getDefaultComputedStyle( elem[ 0 ] ) ) ?

			// Use of this method is a temporary fix (more like optimization) until something better comes along,
			// since it was removed from specification and supported only in FF
			style.display : jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = (iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = iframe[ 0 ].contentDocument;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}
var rmargin = (/^margin/);

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {
		// Support: IE<=11+, Firefox<=30+ (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		if ( elem.ownerDocument.defaultView.opener ) {
			return elem.ownerDocument.defaultView.getComputedStyle( elem, null );
		}

		return window.getComputedStyle( elem, null );
	};



function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,
		style = elem.style;

	computed = computed || getStyles( elem );

	// Support: IE9
	// getPropertyValue is only needed for .css('filter') (#12537)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];
	}

	if ( computed ) {

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// Support: iOS < 6
		// A tribute to the "awesome hack by Dean Edwards"
		// iOS < 6 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
		// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
		if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?
		// Support: IE
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {
	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {
				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return (this.get = hookFn).apply( this, arguments );
		}
	};
}


(function() {
	var pixelPositionVal, boxSizingReliableVal,
		docElem = document.documentElement,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	if ( !div.style ) {
		return;
	}

	// Support: IE9-11+
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;" +
		"position:absolute";
	container.appendChild( div );

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computePixelPositionAndBoxSizingReliable() {
		div.style.cssText =
			// Support: Firefox<29, Android 2.3
			// Vendor-prefix box-sizing
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;" +
			"box-sizing:border-box;display:block;margin-top:1%;top:1%;" +
			"border:1px;padding:1px;width:4px;position:absolute";
		div.innerHTML = "";
		docElem.appendChild( container );

		var divStyle = window.getComputedStyle( div, null );
		pixelPositionVal = divStyle.top !== "1%";
		boxSizingReliableVal = divStyle.width === "4px";

		docElem.removeChild( container );
	}

	// Support: node.js jsdom
	// Don't assume that getComputedStyle is a property of the global object
	if ( window.getComputedStyle ) {
		jQuery.extend( support, {
			pixelPosition: function() {

				// This test is executed only once but we still do memoizing
				// since we can use the boxSizingReliable pre-computing.
				// No need to check if the test was already performed, though.
				computePixelPositionAndBoxSizingReliable();
				return pixelPositionVal;
			},
			boxSizingReliable: function() {
				if ( boxSizingReliableVal == null ) {
					computePixelPositionAndBoxSizingReliable();
				}
				return boxSizingReliableVal;
			},
			reliableMarginRight: function() {

				// Support: Android 2.3
				// Check if div with explicit width and no margin-right incorrectly
				// gets computed margin-right based on width of container. (#3333)
				// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
				// This support function is only executed once so no memoizing is needed.
				var ret,
					marginDiv = div.appendChild( document.createElement( "div" ) );

				// Reset CSS: box-sizing; display; margin; border; padding
				marginDiv.style.cssText = div.style.cssText =
					// Support: Firefox<29, Android 2.3
					// Vendor-prefix box-sizing
					"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" +
					"box-sizing:content-box;display:block;margin:0;border:0;padding:0";
				marginDiv.style.marginRight = marginDiv.style.width = "0";
				div.style.width = "1px";
				docElem.appendChild( container );

				ret = !parseFloat( window.getComputedStyle( marginDiv, null ).marginRight );

				docElem.removeChild( container );
				div.removeChild( marginDiv );

				return ret;
			}
		});
	}
})();


// A method for quickly swapping in/out CSS properties to get correct calculations.
jQuery.swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var
	// Swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),
	rrelNum = new RegExp( "^([+-])=(" + pnum + ")", "i" ),

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( style, name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in style ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[0].toUpperCase() + name.slice(1),
		origName = name,
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in style ) {
			return name;
		}
	}

	return origName;
}

function setPositiveNumber( elem, value, subtract ) {
	var matches = rnumsplit.exec( value );
	return matches ?
		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?
		// If we already have the right measurement, avoid augmentation
		4 :
		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {
		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {
			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {
			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {
		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test(val) ) {
			return val;
		}

		// Check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox &&
			( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = data_priv.get( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {
			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = data_priv.access( elem, "olddisplay", defaultDisplay(elem.nodeName) );
			}
		} else {
			hidden = isHidden( elem );

			if ( display !== "none" || !hidden ) {
				data_priv.set( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

jQuery.extend({

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && (ret = rrelNum.exec( value )) ) {
				value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number, add 'px' to the (except for certain CSS properties)
			if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
				value += "px";
			}

			// Support: IE9-11+
			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {
				style[ name ] = value;
			}

		} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
		}
		return val;
	}
});

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) && elem.offsetWidth === 0 ?
					jQuery.swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					}) :
					getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var styles = extra && getStyles( elem );
			return setPositiveNumber( elem, value, extra ?
				augmentWidthOrHeight(
					elem,
					name,
					extra,
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				) : 0
			);
		}
	};
});

// Support: Android 2.3
jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			return jQuery.swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
});

jQuery.fn.extend({
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each(function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		});
	}
});


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || "swing";
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			if ( tween.elem[ tween.prop ] != null &&
				(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );
			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {
			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE9
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	}
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rfxnum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" ),
	rrun = /queueHooks$/,
	animationPrefilters = [ defaultPrefilter ],
	tweeners = {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value ),
				target = tween.cur(),
				parts = rfxnum.exec( value ),
				unit = parts && parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

				// Starting value computation is required for potential unit mismatches
				start = ( jQuery.cssNumber[ prop ] || unit !== "px" && +target ) &&
					rfxnum.exec( jQuery.css( tween.elem, prop ) ),
				scale = 1,
				maxIterations = 20;

			if ( start && start[ 3 ] !== unit ) {
				// Trust units reported by jQuery.css
				unit = unit || start[ 3 ];

				// Make sure we update the tween properties later on
				parts = parts || [];

				// Iteratively approximate from a nonzero starting point
				start = +target || 1;

				do {
					// If previous iteration zeroed out, double until we get *something*.
					// Use string for doubling so we don't accidentally see scale as unchanged below
					scale = scale || ".5";

					// Adjust and apply
					start = start / scale;
					jQuery.style( tween.elem, prop, start + unit );

				// Update scale, tolerating zero or NaN from tween.cur(),
				// break the loop if scale is unchanged or perfect, or if we've just had enough
				} while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
			}

			// Update tween properties
			if ( parts ) {
				start = tween.start = +start || +target || 0;
				tween.unit = unit;
				// If a +=/-= token was provided, we're doing a relative animation
				tween.end = parts[ 1 ] ?
					start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :
					+parts[ 2 ];
			}

			return tween;
		} ]
	};

// Animations created synchronously will run synchronously
function createFxNow() {
	setTimeout(function() {
		fxNow = undefined;
	});
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( (tween = collection[ index ].call( animation, prop, value )) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = data_priv.get( elem, "fxshow" );

	// Handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always(function() {
			// Ensure the complete handler is called before this completes
			anim.always(function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			});
		});
	}

	// Height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE9-10 do not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );

		// Test default display if display is currently "none"
		checkDisplay = display === "none" ?
			data_priv.get( elem, "olddisplay" ) || defaultDisplay( elem.nodeName ) : display;

		if ( checkDisplay === "inline" && jQuery.css( elem, "float" ) === "none" ) {
			style.display = "inline-block";
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always(function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		});
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );

		// Any non-fx value stops us from restoring the original display value
		} else {
			display = undefined;
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = data_priv.access( elem, "fxshow", {} );
		}

		// Store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done(function() {
				jQuery( elem ).hide();
			});
		}
		anim.done(function() {
			var prop;

			data_priv.remove( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		});
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}

	// If this is a noop like .hide().hide(), restore an overwritten display value
	} else if ( (display === "none" ? defaultDisplay( elem.nodeName ) : display) === "inline" ) {
		style.display = display;
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = animationPrefilters.length,
		deferred = jQuery.Deferred().always( function() {
			// Don't match elem in the :animated selector
			delete tick.elem;
		}),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
				// Support: Android 2.3
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ]);

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise({
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, { specialEasing: {} }, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,
					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		}),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		})
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.split(" ");
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			tweeners[ prop ] = tweeners[ prop ] || [];
			tweeners[ prop ].unshift( callback );
		}
	},

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			animationPrefilters.unshift( callback );
		} else {
			animationPrefilters.push( callback );
		}
	}
});

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
		opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend({
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate({ opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {
				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || data_priv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each(function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = data_priv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		});
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each(function() {
			var index,
				data = data_priv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		});
	}
});

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show"),
	slideUp: genFx("hide"),
	slideToggle: genFx("toggle"),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];
		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;

jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	clearInterval( timerId );
	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,
	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = setTimeout( next, time );
		hooks.stop = function() {
			clearTimeout( timeout );
		};
	});
};


(function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: iOS<=5.1, Android<=4.2+
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE<=11+
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: Android<=2.3
	// Options inside disabled selects are incorrectly marked as disabled
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE<=11+
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
})();


var nodeHook, boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend({
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	}
});

jQuery.extend({
	attr: function( elem, name, value ) {
		var hooks, ret,
			nType = elem.nodeType;

		// don't get/set attributes on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === strundefined ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );
		}

		if ( value !== undefined ) {

			if ( value === null ) {
				jQuery.removeAttr( elem, name );

			} else if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				elem.setAttribute( name, value + "" );
				return value;
			}

		} else if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
			return ret;

		} else {
			ret = jQuery.find.attr( elem, name );

			// Non-existent attributes return null, we normalize to undefined
			return ret == null ?
				undefined :
				ret;
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( (name = attrNames[i++]) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {
					// Set corresponding property to false
					elem[ propName ] = false;
				}

				elem.removeAttribute( name );
			}
		}
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					jQuery.nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	}
});

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle;
		if ( !isXML ) {
			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ name ];
			attrHandle[ name ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				name.toLowerCase() :
				null;
			attrHandle[ name ] = handle;
		}
		return ret;
	};
});




var rfocusable = /^(?:input|select|textarea|button)$/i;

jQuery.fn.extend({
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each(function() {
			delete this[ jQuery.propFix[ name ] || name ];
		});
	}
});

jQuery.extend({
	propFix: {
		"for": "htmlFor",
		"class": "className"
	},

	prop: function( elem, name, value ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		if ( notxml ) {
			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			return hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ?
				ret :
				( elem[ name ] = value );

		} else {
			return hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ?
				ret :
				elem[ name ];
		}
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {
				return elem.hasAttribute( "tabindex" ) || rfocusable.test( elem.nodeName ) || elem.href ?
					elem.tabIndex :
					-1;
			}
		}
	}
});

if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		}
	};
}

jQuery.each([
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
});




var rclass = /[\t\r\n\f]/g;

jQuery.fn.extend({
	addClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			proceed = typeof value === "string" && value,
			i = 0,
			len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call( this, j, this.className ) );
			});
		}

		if ( proceed ) {
			// The disjunction here is for better compressibility (see removeClass)
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					" "
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			proceed = arguments.length === 0 || typeof value === "string" && value,
			i = 0,
			len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call( this, j, this.className ) );
			});
		}
		if ( proceed ) {
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					""
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = value ? jQuery.trim( cur ) : "";
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
			});
		}

		return this.each(function() {
			if ( type === "string" ) {
				// Toggle individual class names
				var className,
					i = 0,
					self = jQuery( this ),
					classNames = value.match( rnotwhite ) || [];

				while ( (className = classNames[ i++ ]) ) {
					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( type === strundefined || type === "boolean" ) {
				if ( this.className ) {
					// store className if set
					data_priv.set( this, "__className__", this.className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				this.className = this.className || value === false ? "" : data_priv.get( this, "__className__" ) || "";
			}
		});
	},

	hasClass: function( selector ) {
		var className = " " + selector + " ",
			i = 0,
			l = this.length;
		for ( ; i < l; i++ ) {
			if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
				return true;
			}
		}

		return false;
	}
});




var rreturn = /\r/g;

jQuery.fn.extend({
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// Handle most common string cases
					ret.replace(rreturn, "") :
					// Handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each(function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
	}
});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :
					// Support: IE10-11+
					// option.text throws exceptions (#14686, #14858)
					jQuery.trim( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// IE6-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&
							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ? !option.disabled : option.getAttribute( "disabled" ) === null ) &&
							( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];
					if ( (option.selected = jQuery.inArray( option.value, values ) >= 0) ) {
						optionSet = true;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
});

// Radios and checkboxes getter/setter
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute("value") === null ? "on" : elem.value;
		};
	}
});




// Return jQuery for attributes-only inclusion


jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
});

jQuery.fn.extend({
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
	}
});


var nonce = jQuery.now();

var rquery = (/\?/);



// Support: Android 2.3
// Workaround failure to string-cast null input
jQuery.parseJSON = function( data ) {
	return JSON.parse( data + "" );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml, tmp;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE9
	try {
		tmp = new DOMParser();
		xml = tmp.parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,
	rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Document location
	ajaxLocation = window.location.href,

	// Segment location into parts
	ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {
			// For each dataType in the dataTypeExpression
			while ( (dataType = dataTypes[i++]) ) {
				// Prepend if requested
				if ( dataType[0] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

				// Otherwise append
				} else {
					(structure[ dataType ] = structure[ dataType ] || []).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		});
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},
		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

		// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {
								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s[ "throws" ] ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: ajaxLocation,
		type: "GET",
		isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,
			// URL without anti-cache param
			cacheURL,
			// Response headers
			responseHeadersString,
			responseHeaders,
			// timeout handle
			timeoutTimer,
			// Cross-domain detection vars
			parts,
			// To know if global events are to be dispatched
			fireGlobals,
			// Loop variable
			i,
			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
				jQuery( callbackContext ) :
				jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks("once memory"),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// The jqXHR state
			state = 0,
			// Default abort message
			strAbort = "canceled",
			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( (match = rheaders.exec( responseHeadersString )) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {
								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {
							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" )
			.replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when we have a protocol:host:port mismatch
		if ( s.crossDomain == null ) {
			parts = rurl.exec( s.url.toLowerCase() );
			s.crossDomain = !!( parts &&
				( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
					( parts[ 3 ] || ( parts[ 1 ] === "http:" ? "80" : "443" ) ) !==
						( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? "80" : "443" ) ) )
			);
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger("ajaxStart");
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
				s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}
			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = setTimeout(function() {
					jqXHR.abort("timeout");
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader("Last-Modified");
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader("etag");
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {
				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger("ajaxStop");
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		return jQuery.ajax({
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		});
	};
});


jQuery._evalUrl = function( url ) {
	return jQuery.ajax({
		url: url,
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	});
};


jQuery.fn.extend({
	wrapAll: function( html ) {
		var wrap;

		if ( jQuery.isFunction( html ) ) {
			return this.each(function( i ) {
				jQuery( this ).wrapAll( html.call(this, i) );
			});
		}

		if ( this[ 0 ] ) {

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function( i ) {
				jQuery( this ).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function() {
		return this.parent().each(function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		}).end();
	}
});


jQuery.expr.filters.hidden = function( elem ) {
	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	return elem.offsetWidth <= 0 && elem.offsetHeight <= 0;
};
jQuery.expr.filters.visible = function( elem ) {
	return !jQuery.expr.filters.hidden( elem );
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {
			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend({
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					}) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});


jQuery.ajaxSettings.xhr = function() {
	try {
		return new XMLHttpRequest();
	} catch( e ) {}
};

var xhrId = 0,
	xhrCallbacks = {},
	xhrSuccessStatus = {
		// file protocol always yields status code 0, assume 200
		0: 200,
		// Support: IE9
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

// Support: IE9
// Open requests must be manually aborted on unload (#5280)
// See https://support.microsoft.com/kb/2856746 for more info
if ( window.attachEvent ) {
	window.attachEvent( "onunload", function() {
		for ( var key in xhrCallbacks ) {
			xhrCallbacks[ key ]();
		}
	});
}

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport(function( options ) {
	var callback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr(),
					id = ++xhrId;

				xhr.open( options.type, options.url, options.async, options.username, options.password );

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers["X-Requested-With"] ) {
					headers["X-Requested-With"] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							delete xhrCallbacks[ id ];
							callback = xhr.onload = xhr.onerror = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {
								complete(
									// file: protocol always yields status 0; see #8605, #14207
									xhr.status,
									xhr.statusText
								);
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,
									// Support: IE9
									// Accessing binary-data responseText throws an exception
									// (#11426)
									typeof xhr.responseText === "string" ? {
										text: xhr.responseText
									} : undefined,
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				xhr.onerror = callback("error");

				// Create the abort callback
				callback = xhrCallbacks[ id ] = callback("abort");

				try {
					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {
					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /(?:java|ecma)script/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {
	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery("<script>").prop({
					async: true,
					charset: s.scriptCharset,
					src: s.url
				}).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always(function() {
			// Restore preexisting value
			window[ callbackName ] = overwritten;

			// Save back as free
			if ( s[ callbackName ] ) {
				// make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		});

		// Delegate to script
		return "script";
	}
});




// data: string of html
// context (optional): If specified, the fragment will be created in this context, defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[1] ) ];
	}

	parsed = jQuery.buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, type, response,
		self = this,
		off = url.indexOf(" ");

	if ( off >= 0 ) {
		selector = jQuery.trim( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax({
			url: url,

			// if "type" variable is undefined, then "GET" method will be used
			type: type,
			dataType: "html",
			data: params
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		}).complete( callback && function( jqXHR, status ) {
			self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
		});
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
});




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
		return elem === fn.elem;
	}).length;
};




var docElem = window.document.documentElement;

/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf("auto") > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {
			options = options.call( elem, i, curOffset );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend({
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each(function( i ) {
					jQuery.offset.setOffset( this, options, i );
				});
		}

		var docElem, win,
			elem = this[ 0 ],
			box = { top: 0, left: 0 },
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		// Support: BlackBerry 5, iOS 3 (original iPhone)
		// If we don't have gBCR, just use 0,0 rather than error
		if ( typeof elem.getBoundingClientRect !== strundefined ) {
			box = elem.getBoundingClientRect();
		}
		win = getWindow( doc );
		return {
			top: box.top + win.pageYOffset - docElem.clientTop,
			left: box.left + win.pageXOffset - docElem.clientLeft
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {
			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent || docElem;

			while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position" ) === "static" ) ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || docElem;
		});
	}
});

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : window.pageXOffset,
					top ? val : window.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

// Support: Safari<7+, Chrome<37+
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );
				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
});


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {
					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?
					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	});
});


// The number of elements contained in the matched element set
jQuery.fn.size = function() {
	return this.length;
};

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	});
}




var
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === strundefined ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;

}));

//!!! myjui/js/jquery/hotkeys/jquery.hotkeys.js
/******************************************************************************************************************************

 * @ Original idea by by Binny V A, Original version: 2.00.A 
 * @ http://www.openjs.com/scripts/events/keyboard_shortcuts/
 * @ Original License : BSD
 
 * @ jQuery Plugin by Tzury Bar Yochay 
        mail: tzury.by@gmail.com
        blog: evalinux.wordpress.com
        face: facebook.com/profile.php?id=513676303
        
        (c) Copyrights 2007
        
 * @ jQuery Plugin version Beta (0.0.2)
 * @ License: jQuery-License.
 
TODO:
    add queue support (as in gmail) e.g. 'x' then 'y', etc.
    add mouse + mouse wheel events.

USAGE:
    $.hotkeys.add('Ctrl+c', function(){ alert('copy anyone?');});
    $.hotkeys.add('Ctrl+c', {target:'div#editor', type:'keyup', propagate: true},function(){ alert('copy anyone?');});>
    $.hotkeys.remove('Ctrl+c'); 
    $.hotkeys.remove('Ctrl+c', {target:'div#editor', type:'keypress'}); 
    
******************************************************************************************************************************/
(function (jQuery){
    this.version = '(beta)(0.0.3)';
	this.all = {};
    this.special_keys = {
        27: 'esc', 9: 'tab', 32:'space', 13: 'return', 8:'backspace', 145: 'scroll', 20: 'capslock', 
        144: 'numlock', 19:'pause', 45:'insert', 36:'home', 46:'del',35:'end', 33: 'pageup', 
        34:'pagedown', 37:'left', 38:'up', 39:'right',40:'down', 112:'f1',113:'f2', 114:'f3', 
        115:'f4', 116:'f5', 117:'f6', 118:'f7', 119:'f8', 120:'f9', 121:'f10', 122:'f11', 123:'f12'};
        
    this.shift_nums = { "`":"~", "1":"!", "2":"@", "3":"#", "4":"$", "5":"%", "6":"^", "7":"&", 
        "8":"*", "9":"(", "0":")", "-":"_", "=":"+", ";":":", "'":"\"", ",":"<", 
        ".":">",  "/":"?",  "\\":"|" };
        
    this.add = function(combi, options, callback) {
        if (jQuery.isFunction(options)){
            callback = options;
            options = {};
        }
        var opt = {},
            defaults = {type: 'keydown', propagate: false, disableInInput: false, target: jQuery('html')[0], checkParent: true},
            that = this;
        opt = jQuery.extend( opt , defaults, options || {} );
        combi = combi.toLowerCase();        
        
        // inspect if keystroke matches
        var inspector = function(event) {
            event = jQuery.event.fix(event); // jQuery event normalization.
            var element = event.target;
            // @ TextNode -> nodeType == 3
            element = (element.nodeType==3) ? element.parentNode : element;
            
            if(opt['disableInInput']) { // Disable shortcut keys in Input, Textarea fields
                var target = jQuery(element);
                if( target.is("input") || target.is("textarea")){
                    return;
                }
            }
            var code = event.which,
                type = event.type,
                character = String.fromCharCode(code).toLowerCase(),
                special = that.special_keys[code],
                shift = event.shiftKey,
                ctrl = event.ctrlKey,
                alt= event.altKey,
                propagate = true, // default behaivour
                mapPoint = null;
            
            // in opera + safari, the event.target is unpredictable.
            // for example: 'keydown' might be associated with HtmlBodyElement 
            // or the element where you last clicked with your mouse.
            if (jQuery.browser.opera || jQuery.browser.safari || opt.checkParent){
                while (!that.all[element] && element.parentNode){
                    element = element.parentNode;
                }
            }
            
            var cbMap = that.all[element].events[type].callbackMap;
            if(!shift && !ctrl && !alt) { // No Modifiers
                mapPoint = cbMap[special] ||  cbMap[character]
			}
            // deals with combinaitons (alt|ctrl|shift+anything)
            else{
                var modif = '';
                if(alt) modif +='alt+';
                if(ctrl) modif+= 'ctrl+';
                if(shift) modif += 'shift+';
                // modifiers + special keys or modifiers + characters or modifiers + shift characters
                mapPoint = cbMap[modif+special] || cbMap[modif+character] || cbMap[modif+that.shift_nums[character]]
            }
            if (mapPoint){
                mapPoint.cb(event);
                if(!mapPoint.propagate) {
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
            }
		};        
        // first hook for this element
        if (!this.all[opt.target]){
            this.all[opt.target] = {events:{}};
        }
        if (!this.all[opt.target].events[opt.type]){
            this.all[opt.target].events[opt.type] = {callbackMap: {}}
            jQuery.event.add(opt.target, opt.type, inspector);
        }        
        this.all[opt.target].events[opt.type].callbackMap[combi] =  {cb: callback, propagate:opt.propagate};                
        return jQuery;
	};    
    this.remove = function(exp, opt) {
        opt = opt || {};
        target = opt.target || jQuery('html')[0];
        type = opt.type || 'keydown';
		exp = exp.toLowerCase();        
        delete this.all[target].events[type].callbackMap[exp]        
        return jQuery;
	};
    jQuery.hotkeys = this;
    return jQuery;    
})(jQuery);
/**
 *
 * jquery.binarytransport.js
 *
 * @description. jQuery ajax transport for making binary data type requests.
 * @version 1.0
 * @author Henry Algus <henryalgus@gmail.com>
 *
 */

(function($, undefined) {
    "use strict";

    // use this transport for "binary" data type
    $.ajaxTransport("+binary", function(options, originalOptions, jqXHR) {
        // check for conditions and support for blob / arraybuffer response type
        if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob))))) {
            return {
                // create new XMLHttpRequest
                send: function(headers, callback) {
                    // setup all variables
                    var xhr = new XMLHttpRequest(),
                        url = options.url,
                        type = options.type,
                        async = options.async || true,
                        // blob or arraybuffer. Default is blob
                        dataType = options.responseType || "blob",
                        data = options.data || null,
                        username = options.username || null,
                        password = options.password || null;

                    xhr.addEventListener('load', function() {
                        var data = {};
                        data[options.dataType] = xhr.response;
                        // make callback and send data
                        callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                    });
                    xhr.addEventListener('error', function() {
                        var data = {};
                        data[options.dataType] = xhr.response;
                        // make callback and send data
                        callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                    });

                    xhr.open(type, url, async, username, password);

                    // setup custom headers
                    for (var i in headers) {
                        xhr.setRequestHeader(i, headers[i]);
                    }

                    xhr.responseType = dataType;
                    xhr.send(data);
                },
                abort: function() {}
            };
        }
    });
})(window.jQuery);
//!!! myjui/js/jquery/dimensions/jquery.dimensions.js
/* Copyright (c) 2007 Paul Bakaus (paul.bakaus@googlemail.com) and Brandon Aaron (brandon.aaron@gmail.com || http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * $LastChangedDate: 2007-08-17 13:14:11 -0500 (Fri, 17 Aug 2007) $
 * $Rev: 2759 $
 *
 * Version: 1.1.2
 *
 * Requires: jQuery 1.1.3+
 */

(function($){

// store a copy of the core height and width methods
var height = $.fn.height,
    width  = $.fn.width;

$.fn.extend({
	/**
	 * If used on document, returns the document's height (innerHeight).
	 * If used on window, returns the viewport's (window) height.
	 * See core docs on height() to see what happens when used on an element.
	 *
	 * @example $("#testdiv").height()
	 * @result 200
	 *
	 * @example $(document).height()
	 * @result 800
	 *
	 * @example $(window).height()
	 * @result 400
	 *
	 * @name height
	 * @type Number
	 * @cat Plugins/Dimensions
	 */
	/* height: function() {
		if ( !this[0] ) error();
		if ( this[0] == window )
			if ( $.browser.opera || ($.browser.safari && parseInt($.browser.version) > 520) )
				return self.innerHeight - (($(document).height() > self.innerHeight) ? getScrollbarWidth() : 0);
			else if ( $.browser.safari )
				return self.innerHeight;
			else
                return $.boxModel && document.documentElement.clientHeight || document.body.clientHeight;
		
		if ( this[0] == document ) 
			return Math.max( ($.boxModel && document.documentElement.scrollHeight || document.body.scrollHeight), document.body.offsetHeight );
		
		return height.apply(this, arguments);
	}, */
	
	/**
	 * If used on document, returns the document's width (innerWidth).
	 * If used on window, returns the viewport's (window) width.
	 * See core docs on width() to see what happens when used on an element.
	 *
	 * @example $("#testdiv").width()
	 * @result 200
	 *
	 * @example $(document).width()
	 * @result 800
	 *
	 * @example $(window).width()
	 * @result 400
	 *
	 * @name width
	 * @type Number
	 * @cat Plugins/Dimensions
	 */
	/* width: function() {
		if (!this[0]) error();
		if ( this[0] == window )
			if ( $.browser.opera || ($.browser.safari && parseInt($.browser.version) > 520) )
				return self.innerWidth - (($(document).width() > self.innerWidth) ? getScrollbarWidth() : 0);
			else if ( $.browser.safari )
				return self.innerWidth;
			else
                return $.boxModel && document.documentElement.clientWidth || document.body.clientWidth;

		if ( this[0] == document )
			if ($.browser.mozilla) {
				// mozilla reports scrollWidth and offsetWidth as the same
				var scrollLeft = self.pageXOffset;
				self.scrollTo(99999999, self.pageYOffset);
				var scrollWidth = self.pageXOffset;
				self.scrollTo(scrollLeft, self.pageYOffset);
				return document.body.offsetWidth + scrollWidth;
			}
			else 
				return Math.max( (($.boxModel && !$.browser.safari) && document.documentElement.scrollWidth || document.body.scrollWidth), document.body.offsetWidth );

		return width.apply(this, arguments);
	}, */
	
	/**
	 * Gets the inner height (excludes the border and includes the padding) for the first matched element.
	 * If used on document, returns the document's height (innerHeight).
	 * If used on window, returns the viewport's (window) height.
	 *
	 * @example $("#testdiv").innerHeight()
	 * @result 210
	 *
	 * @name innerHeight
	 * @type Number
	 * @cat Plugins/Dimensions
	 */
	innerHeight: function() {
		if (!this[0]) error();
		return this[0] == window || this[0] == document ?
			this.height() :
			this.is(':visible') ?
				this[0].offsetHeight - num(this, 'borderTopWidth') - num(this, 'borderBottomWidth') :
				this.height() + num(this, 'paddingTop') + num(this, 'paddingBottom');
	},
	
	/**
	 * Gets the inner width (excludes the border and includes the padding) for the first matched element.
	 * If used on document, returns the document's width (innerWidth).
	 * If used on window, returns the viewport's (window) width.
	 *
	 * @example $("#testdiv").innerWidth()
	 * @result 210
	 *
	 * @name innerWidth
	 * @type Number
	 * @cat Plugins/Dimensions
	 */
	innerWidth: function() {
		if (!this[0]) error();
		return this[0] == window || this[0] == document ?
			this.width() :
			this.is(':visible') ?
				this[0].offsetWidth - num(this, 'borderLeftWidth') - num(this, 'borderRightWidth') :
				this.width() + num(this, 'paddingLeft') + num(this, 'paddingRight');
	},
	
	/**
	 * Gets the outer height (includes the border and padding) for the first matched element.
	 * If used on document, returns the document's height (innerHeight).
	 * If used on window, returns the viewport's (window) height.
	 *
	 * The margin can be included in the calculation by passing an options map with margin
	 * set to true.
	 *
	 * @example $("#testdiv").outerHeight()
	 * @result 220
	 *
	 * @example $("#testdiv").outerHeight({ margin: true })
	 * @result 240
	 *
	 * @name outerHeight
	 * @type Number
	 * @param Map options Optional settings to configure the way the outer height is calculated.
	 * @cat Plugins/Dimensions
	 */
	outerHeight: function(options) {
		if (!this[0]) error();
		options = $.extend({ margin: false }, options || {});
		return this[0] == window || this[0] == document ?
			this.height() :
			this.is(':visible') ?
				this[0].offsetHeight + (options.margin ? (num(this, 'marginTop') + num(this, 'marginBottom')) : 0) :
				this.height() 
					+ num(this,'borderTopWidth') + num(this, 'borderBottomWidth') 
					+ num(this, 'paddingTop') + num(this, 'paddingBottom')
					+ (options.margin ? (num(this, 'marginTop') + num(this, 'marginBottom')) : 0);
	},
	
	/**
	 * Gets the outer width (including the border and padding) for the first matched element.
	 * If used on document, returns the document's width (innerWidth).
	 * If used on window, returns the viewport's (window) width.
	 *
	 * The margin can be included in the calculation by passing an options map with margin
	 * set to true.
	 *
	 * @example $("#testdiv").outerWidth()
	 * @result 1000
	 *
	 * @example $("#testdiv").outerWidth({ margin: true })
	 * @result 1020
	 * 
	 * @name outerHeight
	 * @type Number
	 * @param Map options Optional settings to configure the way the outer width is calculated.
	 * @cat Plugins/Dimensions
	 */
	outerWidth: function(options) {
		if (!this[0]) error();
		options = $.extend({ margin: false }, options || {});
		return this[0] == window || this[0] == document ?
			this.width() :
			this.is(':visible') ?
				this[0].offsetWidth + (options.margin ? (num(this, 'marginLeft') + num(this, 'marginRight')) : 0) :
				this.width() 
					+ num(this, 'borderLeftWidth') + num(this, 'borderRightWidth') 
					+ num(this, 'paddingLeft') + num(this, 'paddingRight')
					+ (options.margin ? (num(this, 'marginLeft') + num(this, 'marginRight')) : 0);
	},
	
	/**
	 * Gets how many pixels the user has scrolled to the right (scrollLeft).
	 * Works on containers with overflow: auto and window/document.
	 *
	 * @example $(window).scrollLeft()
	 * @result 100
	 *
	 * @example $(document).scrollLeft()
	 * @result 100
	 * 
	 * @example $("#testdiv").scrollLeft()
	 * @result 100
	 *
	 * @name scrollLeft
	 * @type Number
	 * @cat Plugins/Dimensions
	 */
	/**
	 * Sets the scrollLeft property for each element and continues the chain.
	 * Works on containers with overflow: auto and window/document.
	 *
	 * @example $(window).scrollLeft(100).scrollLeft()
	 * @result 100
	 * 
	 * @example $(document).scrollLeft(100).scrollLeft()
	 * @result 100
	 *
	 * @example $("#testdiv").scrollLeft(100).scrollLeft()
	 * @result 100
	 *
	 * @name scrollLeft
	 * @param Number value A positive number representing the desired scrollLeft.
	 * @type jQuery
	 * @cat Plugins/Dimensions
	 */
	scrollLeft: function(val) {
		if (!this[0]) error();
		if ( val != undefined )
			// set the scroll left
			return this.each(function() {
				if (this == window || this == document)
					window.scrollTo( val, $(window).scrollTop() );
				else
					this.scrollLeft = val;
			});
		
		// return the scroll left offest in pixels
		if ( this[0] == window || this[0] == document )
			return self.pageXOffset ||
				$.boxModel && document.documentElement.scrollLeft ||
				document.body.scrollLeft;
				
		return this[0].scrollLeft;
	},
	
	/**
	 * Gets how many pixels the user has scrolled to the bottom (scrollTop).
	 * Works on containers with overflow: auto and window/document.
	 *
	 * @example $(window).scrollTop()
	 * @result 100
	 *
	 * @example $(document).scrollTop()
	 * @result 100
	 * 
	 * @example $("#testdiv").scrollTop()
	 * @result 100
	 *
	 * @name scrollTop
	 * @type Number
	 * @cat Plugins/Dimensions
	 */
	/**
	 * Sets the scrollTop property for each element and continues the chain.
	 * Works on containers with overflow: auto and window/document.
	 *
	 * @example $(window).scrollTop(100).scrollTop()
	 * @result 100
	 * 
	 * @example $(document).scrollTop(100).scrollTop()
	 * @result 100
	 *
	 * @example $("#testdiv").scrollTop(100).scrollTop()
	 * @result 100
	 *
	 * @name scrollTop
	 * @param Number value A positive number representing the desired scrollTop.
	 * @type jQuery
	 * @cat Plugins/Dimensions
	 */
	scrollTop: function(val) {
		if (!this[0]) error();
		if ( val != undefined )
			// set the scroll top
			return this.each(function() {
				if (this == window || this == document)
					window.scrollTo( $(window).scrollLeft(), val );
				else
					this.scrollTop = val;
			});
		
		// return the scroll top offset in pixels
		if ( this[0] == window || this[0] == document )
			return self.pageYOffset ||
				$.boxModel && document.documentElement.scrollTop ||
				document.body.scrollTop;

		return this[0].scrollTop;
	},
	
	/** 
	 * Gets the top and left positioned offset in pixels.
	 * The positioned offset is the offset between a positioned
	 * parent and the element itself.
	 *
	 * For accurate calculations make sure to use pixel values for margins, borders and padding.
	 *
	 * @example $("#testdiv").position()
	 * @result { top: 100, left: 100 }
	 *
	 * @example var position = {};
	 * $("#testdiv").position(position)
	 * @result position = { top: 100, left: 100 }
	 * 
	 * @name position
	 * @param Object returnObject Optional An object to store the return value in, so as not to break the chain. If passed in the
	 *                            chain will not be broken and the result will be assigned to this object.
	 * @type Object
	 * @cat Plugins/Dimensions
	 */
	position: function(returnObject) {
		return this.offset({ margin: false, scroll: false, relativeTo: this.offsetParent() }, returnObject);
	},
	
	/**
	 * Gets the location of the element in pixels from the top left corner of the viewport.
	 * The offset method takes an optional map of key value pairs to configure the way
	 * the offset is calculated. Here are the different options.
	 *
	 * (Boolean) margin - Should the margin of the element be included in the calculations? True by default.
	 * (Boolean) border - Should the border of the element be included in the calculations? False by default. 
	 * (Boolean) padding - Should the padding of the element be included in the calculations? False by default. 
	 * (Boolean) scroll - Should the scroll offsets of the parent elements be included in the calculations? True by default.
	 *                    When true it adds the total scroll offsets of all parents to the total offset and also adds two
	 *                    properties to the returned object, scrollTop and scrollLeft.
	 * (Boolean) lite - When true it will use the offsetLite method instead of the full-blown, slower offset method. False by default.
	 *                  Only use this when margins, borders and padding calculations don't matter.
	 * (HTML Element) relativeTo - This should be a parent of the element and should have position (like absolute or relative).
	 *                             It will retreive the offset relative to this parent element. By default it is the body element.
	 *
	 * Also an object can be passed as the second paramater to
	 * catch the value of the return and continue the chain.
	 *
	 * For accurate calculations make sure to use pixel values for margins, borders and padding.
	 * 
	 * Known issues:
	 *  - Issue: A div positioned relative or static without any content before it and its parent will report an offsetTop of 0 in Safari
	 *    Workaround: Place content before the relative div ... and set height and width to 0 and overflow to hidden
	 *
	 * @example $("#testdiv").offset()
	 * @result { top: 100, left: 100, scrollTop: 10, scrollLeft: 10 }
	 *
	 * @example $("#testdiv").offset({ scroll: false })
	 * @result { top: 90, left: 90 }
	 *
	 * @example var offset = {}
	 * $("#testdiv").offset({ scroll: false }, offset)
	 * @result offset = { top: 90, left: 90 }
	 *
	 * @name offset
	 * @param Map options Optional settings to configure the way the offset is calculated.
	 * @param Object returnObject An object to store the return value in, so as not to break the chain. If passed in the
	 *                            chain will not be broken and the result will be assigned to this object.
	 * @type Object
	 * @cat Plugins/Dimensions
	 */
	offset: function(options, returnObject) {
		if (!this[0]) error();
		var x = 0, y = 0, sl = 0, st = 0,
		    elem = this[0], parent = this[0], op, parPos, elemPos = $.css(elem, 'position'),
		    mo = $.browser.mozilla, ie = $.browser.msie, oa = $.browser.opera,
		    sf = $.browser.safari, sf3 = $.browser.safari && parseInt($.browser.version) > 520,
		    absparent = false, relparent = false, 
		    options = $.extend({ margin: true, border: false, padding: false, scroll: true, lite: false, relativeTo: document.body }, options || {});
		
		// Use offsetLite if lite option is true
		if (options.lite) return this.offsetLite(options, returnObject);
		// Get the HTMLElement if relativeTo is a jquery collection
		if (options.relativeTo.jquery) options.relativeTo = options.relativeTo[0];
		
		if (elem.tagName == 'BODY') {
			// Safari 2 is the only one to get offsetLeft and offsetTop properties of the body "correct"
			// Except they all mess up when the body is positioned absolute or relative
			x = elem.offsetLeft;
			y = elem.offsetTop;
			// Mozilla ignores margin and subtracts border from body element
			if (mo) {
				x += num(elem, 'marginLeft') + (num(elem, 'borderLeftWidth')*2);
				y += num(elem, 'marginTop')  + (num(elem, 'borderTopWidth') *2);
			} else
			// Opera ignores margin
			if (oa) {
				x += num(elem, 'marginLeft');
				y += num(elem, 'marginTop');
			} else
			// IE does not add the border in Standards Mode
			if ((ie && jQuery.boxModel)) {
				x += num(elem, 'borderLeftWidth');
				y += num(elem, 'borderTopWidth');
			} else
			// Safari 3 doesn't not include border or margin
			if (sf3) {
				x += num(elem, 'marginLeft') + num(elem, 'borderLeftWidth');
				y += num(elem, 'marginTop')  + num(elem, 'borderTopWidth');
			}
		} else {
			do {
				parPos = $.css(parent, 'position');
			
				x += parent.offsetLeft;
				y += parent.offsetTop;

				// Mozilla and IE do not add the border
				// Mozilla adds the border for table cells
				if ((mo && !parent.tagName.match(/^t[d|h]$/i)) || ie || sf3) {
					// add borders to offset
					x += num(parent, 'borderLeftWidth');
					y += num(parent, 'borderTopWidth');

					// Mozilla does not include the border on body if an element isn't positioned absolute and is without an absolute parent
					if (mo && parPos == 'absolute') absparent = true;
					// IE does not include the border on the body if an element is position static and without an absolute or relative parent
					if (ie && parPos == 'relative') relparent = true;
				}

				op = parent.offsetParent || document.body;
				if (options.scroll || mo) {
					do {
						if (options.scroll) {
							// get scroll offsets
							sl += parent.scrollLeft;
							st += parent.scrollTop;
						}
						
						// Opera sometimes incorrectly reports scroll offset for elements with display set to table-row or inline
						if (oa && ($.css(parent, 'display') || '').match(/table-row|inline/)) {
							sl = sl - ((parent.scrollLeft == parent.offsetLeft) ? parent.scrollLeft : 0);
							st = st - ((parent.scrollTop == parent.offsetTop) ? parent.scrollTop : 0);
						}
				
						// Mozilla does not add the border for a parent that has overflow set to anything but visible
						if (mo && parent != elem && $.css(parent, 'overflow') != 'visible') {
							x += num(parent, 'borderLeftWidth');
							y += num(parent, 'borderTopWidth');
						}
				
						parent = parent.parentNode;
					} while (parent != op);
				}
				parent = op;
				
				// exit the loop if we are at the relativeTo option but not if it is the body or html tag
				if (parent == options.relativeTo && !(parent.tagName == 'BODY' || parent.tagName == 'HTML'))  {
					// Mozilla does not add the border for a parent that has overflow set to anything but visible
					if (mo && parent != elem && $.css(parent, 'overflow') != 'visible') {
						x += num(parent, 'borderLeftWidth');
						y += num(parent, 'borderTopWidth');
					}
					// Safari 2 and opera includes border on positioned parents
					if ( ((sf && !sf3) || oa) && parPos != 'static' ) {
						x -= num(op, 'borderLeftWidth');
						y -= num(op, 'borderTopWidth');
					}
					break;
				}
				if (parent.tagName == 'BODY' || parent.tagName == 'HTML') {
					// Safari 2 and IE Standards Mode doesn't add the body margin for elments positioned with static or relative
					if (((sf && !sf3) || (ie && $.boxModel)) && elemPos != 'absolute' && elemPos != 'fixed') {
						x += num(parent, 'marginLeft');
						y += num(parent, 'marginTop');
					}
					// Safari 3 does not include the border on body
					// Mozilla does not include the border on body if an element isn't positioned absolute and is without an absolute parent
					// IE does not include the border on the body if an element is positioned static and without an absolute or relative parent
					if ( sf3 || (mo && !absparent && elemPos != 'fixed') || 
					     (ie && elemPos == 'static' && !relparent) ) {
						x += num(parent, 'borderLeftWidth');
						y += num(parent, 'borderTopWidth');
					}
					break; // Exit the loop
				}
			} while (parent);
		}

		var returnValue = handleOffsetReturn(elem, options, x, y, sl, st);

		if (returnObject) { $.extend(returnObject, returnValue); return this; }
		else              { return returnValue; }
	},
	/**
	 * Gets the location of the element in pixels from the top left corner of the viewport.
	 * This method is much faster than offset but not as accurate when borders and margins are
	 * on the element and/or its parents. This method can be invoked
	 * by setting the lite option to true in the offset method.
	 * The offsetLite method takes an optional map of key value pairs to configure the way
	 * the offset is calculated. Here are the different options.
	 *
	 * (Boolean) margin - Should the margin of the element be included in the calculations? True by default.
	 * (Boolean) border - Should the border of the element be included in the calculations? False by default. 
	 * (Boolean) padding - Should the padding of the element be included in the calcuations? False by default. 
	 * (Boolean) scroll - Sould the scroll offsets of the parent elements be included int he calculations? True by default.
	 *                    When true it adds the total scroll offsets of all parents to the total offset and also adds two
	 *                    properties to the returned object, scrollTop and scrollLeft.
	 * (HTML Element) relativeTo - This should be a parent of the element and should have position (like absolute or relative).
	 *                             It will retreive the offset relative to this parent element. By default it is the body element.
	 *
	 * @name offsetLite
	 * @param Map options Optional settings to configure the way the offset is calculated.
	 * @param Object returnObject An object to store the return value in, so as not to break the chain. If passed in the
	 *                            chain will not be broken and the result will be assigned to this object.
	 * @type Object
	 * @cat Plugins/Dimensions
	 */
	offsetLite: function(options, returnObject) {
		if (!this[0]) error();
		var x = 0, y = 0, sl = 0, st = 0, parent = this[0], offsetParent, 
		    options = $.extend({ margin: true, border: false, padding: false, scroll: true, relativeTo: document.body }, options || {});
				
		// Get the HTMLElement if relativeTo is a jquery collection
		if (options.relativeTo.jquery) options.relativeTo = options.relativeTo[0];
		
		do {
			x += parent.offsetLeft;
			y += parent.offsetTop;

			offsetParent = parent.offsetParent || document.body;
			if (options.scroll) {
				// get scroll offsets
				do {
					sl += parent.scrollLeft;
					st += parent.scrollTop;
					parent = parent.parentNode;
				} while(parent != offsetParent);
			}
			parent = offsetParent;
		} while (parent && parent.tagName != 'BODY' && parent.tagName != 'HTML' && parent != options.relativeTo);

		var returnValue = handleOffsetReturn(this[0], options, x, y, sl, st);

		if (returnObject) { $.extend(returnObject, returnValue); return this; }
		else              { return returnValue; }
	},
	
	/**
	 * Returns a jQuery collection with the positioned parent of 
	 * the first matched element. This is the first parent of 
	 * the element that has position (as in relative or absolute).
	 *
	 * @name offsetParent
	 * @type jQuery
	 * @cat Plugins/Dimensions
	 */
	offsetParent: function() {
		if (!this[0]) error();
		var offsetParent = this[0].offsetParent;
		while ( offsetParent && (offsetParent.tagName != 'BODY' && $.css(offsetParent, 'position') == 'static') )
			offsetParent = offsetParent.offsetParent;
		return $(offsetParent);
	}
});

/**
 * Throws an error message when no elements are in the jQuery collection
 * @private
 */
var error = function() {
	throw "Dimensions: jQuery collection is empty";
};

/**
 * Handles converting a CSS Style into an Integer.
 * @private
 */
var num = function(el, prop) {
	return parseInt($.css(el.jquery?el[0]:el,prop))||0;
};

/**
 * Handles the return value of the offset and offsetLite methods.
 * @private
 */
var handleOffsetReturn = function(elem, options, x, y, sl, st) {
	if ( !options.margin ) {
		x -= num(elem, 'marginLeft');
		y -= num(elem, 'marginTop');
	}

	// Safari and Opera do not add the border for the element
	if ( options.border && (($.browser.safari && parseInt($.browser.version) < 520) || $.browser.opera) ) {
		x += num(elem, 'borderLeftWidth');
		y += num(elem, 'borderTopWidth');
	} else if ( !options.border && !(($.browser.safari && parseInt($.browser.version) < 520) || $.browser.opera) ) {
		x -= num(elem, 'borderLeftWidth');
		y -= num(elem, 'borderTopWidth');
	}

	if ( options.padding ) {
		x += num(elem, 'paddingLeft');
		y += num(elem, 'paddingTop');
	}
	
	// do not include scroll offset on the element ... opera sometimes reports scroll offset as actual offset
	if ( options.scroll && (!$.browser.opera || elem.offsetLeft != elem.scrollLeft && elem.offsetTop != elem.scrollLeft) ) {
		sl -= elem.scrollLeft;
		st -= elem.scrollTop;
	}

	return options.scroll ? { top: y - st, left: x - sl, scrollTop:  st, scrollLeft: sl }
	                      : { top: y, left: x };
};

/**
 * Gets the width of the OS scrollbar
 * @private
 */
var scrollbarWidth = 0;
var getScrollbarWidth = function() {
	if (!scrollbarWidth) {
		var testEl = $('<div>')
				.css({
					width: 100,
					height: 100,
					overflow: 'auto',
					position: 'absolute',
					top: -1000,
					left: -1000
				})
				.appendTo('body');
		scrollbarWidth = 100 - testEl
			.append('<div>')
			.find('div')
				.css({
					width: '100%',
					height: 200
				})
				.width();
		testEl.remove();
	}
	return scrollbarWidth;
};

})(jQuery);
//!!! myjui/js/jquery/multifile/jquery.MultiFile.js
/*
 ### jQuery Multiple File Upload Plugin v1.48 - 2012-07-19 ###
 * Home: http://www.fyneworks.com/jquery/multiple-file-upload/
 * Code: http://code.google.com/p/jquery-multifile-plugin/
 *
	* Licensed under http://en.wikipedia.org/wiki/MIT_License
 ###
*/

/*# AVOID COLLISIONS #*/
;if(window.jQuery) (function($){
/*# AVOID COLLISIONS #*/
 
	// plugin initialization
	$.fn.MultiFile = function(options){
		if(this.length==0) return this; // quick fail
		
		// Handle API methods
		if(typeof arguments[0]=='string'){
			// Perform API methods on individual elements
			if(this.length>1){
				var args = arguments;
				return this.each(function(){
					$.fn.MultiFile.apply($(this), args);
    });
			};
			// Invoke API method handler
			$.fn.MultiFile[arguments[0]].apply(this, $.makeArray(arguments).slice(1) || []);
			// Quick exit...
			return this;
		};
		
		// Initialize options for this call
		var options = $.extend(
			{}/* new object */,
			$.fn.MultiFile.options/* default options */,
			options || {} /* just-in-time options */
		);
		
		// Empty Element Fix!!!
		// this code will automatically intercept native form submissions
		// and disable empty file elements
		$('form')
		.not('MultiFile-intercepted')
		.addClass('MultiFile-intercepted')
		.submit($.fn.MultiFile.disableEmpty);
		
		//### http://plugins.jquery.com/node/1363
		// utility method to integrate this plugin with others...
		if($.fn.MultiFile.options.autoIntercept){
			$.fn.MultiFile.intercept( $.fn.MultiFile.options.autoIntercept /* array of methods to intercept */ );
			$.fn.MultiFile.options.autoIntercept = null; /* only run this once */
		};
		
		// loop through each matched element
		this
		 .not('.MultiFile-applied')
			.addClass('MultiFile-applied')
		.each(function(){
			//#####################################################################
			// MAIN PLUGIN FUNCTIONALITY - START
			//#####################################################################
			
       // BUG 1251 FIX: http://plugins.jquery.com/project/comments/add/1251
       // variable group_count would repeat itself on multiple calls to the plugin.
       // this would cause a conflict with multiple elements
       // changes scope of variable to global so id will be unique over n calls
       window.MultiFile = (window.MultiFile || 0) + 1;
       var group_count = window.MultiFile;
       
       // Copy parent attributes - Thanks to Jonas Wagner
       // we will use this one to create new input elements
       var MultiFile = {e:this, E:$(this), clone:$(this).clone()};
       
       //===
       
       //# USE CONFIGURATION
       if(typeof options=='number') options = {max:options};
       var o = $.extend({},
        $.fn.MultiFile.options,
        options || {},
   					($.metadata? MultiFile.E.metadata(): ($.meta?MultiFile.E.data():null)) || {}, /* metadata options */
								{} /* internals */
       );
       // limit number of files that can be selected?
       if(!(o.max>0) /*IsNull(MultiFile.max)*/){
        o.max = MultiFile.E.attr('maxlength');
       };
							if(!(o.max>0) /*IsNull(MultiFile.max)*/){
								o.max = (String(MultiFile.e.className.match(/\b(max|limit)\-([0-9]+)\b/gi) || ['']).match(/[0-9]+/gi) || [''])[0];
								if(!(o.max>0)) o.max = -1;
								else           o.max = String(o.max).match(/[0-9]+/gi)[0];
							}
       o.max = new Number(o.max);
       // limit extensions?
       o.accept = o.accept || MultiFile.E.attr('accept') || '';
       if(!o.accept){
        o.accept = (MultiFile.e.className.match(/\b(accept\-[\w\|]+)\b/gi)) || '';
        o.accept = new String(o.accept).replace(/^(accept|ext)\-/i,'');
       };
       
       //===
       
       // APPLY CONFIGURATION
							$.extend(MultiFile, o || {});
       MultiFile.STRING = $.extend({},$.fn.MultiFile.options.STRING,MultiFile.STRING);
       
       //===
       
       //#########################################
       // PRIVATE PROPERTIES/METHODS
       $.extend(MultiFile, {
        n: 0, // How many elements are currently selected?
        slaves: [], files: [],
        instanceKey: MultiFile.e.id || 'MultiFile'+String(group_count), // Instance Key?
        generateID: function(z){ return MultiFile.instanceKey + (z>0 ?'_F'+String(z):''); },
        trigger: function(event, element){
         var handler = MultiFile[event], value = $(element).attr('value');
         if(handler){
          var returnValue = handler(element, value, MultiFile);
          if( returnValue!=null ) return returnValue;
         }
         return true;
        }
       });
       
       //===
       
       // Setup dynamic regular expression for extension validation
       // - thanks to John-Paul Bader: http://smyck.de/2006/08/11/javascript-dynamic-regular-expresions/
       if(String(MultiFile.accept).length>1){
								MultiFile.accept = MultiFile.accept.replace(/\W+/g,'|').replace(/^\W|\W$/g,'');
        MultiFile.rxAccept = new RegExp('\\.('+(MultiFile.accept?MultiFile.accept:'')+')$','gi');
       };
       
       //===
       
       // Create wrapper to hold our file list
       MultiFile.wrapID = MultiFile.instanceKey+'_wrap'; // Wrapper ID?
       MultiFile.E.wrap('<div class="MultiFile-wrap" id="'+MultiFile.wrapID+'"></div>');
       MultiFile.wrapper = $('#'+MultiFile.wrapID+'');
       
       //===
       
       // MultiFile MUST have a name - default: file1[], file2[], file3[]
       MultiFile.e.name = MultiFile.e.name || 'file'+ group_count +'[]';
       
       //===
       
							if(!MultiFile.list){
								// Create a wrapper for the list
								// * OPERA BUG: NO_MODIFICATION_ALLOWED_ERR ('list' is a read-only property)
								// this change allows us to keep the files in the order they were selected
								MultiFile.wrapper.append( '<div class="MultiFile-list" id="'+MultiFile.wrapID+'_list"></div>' );
								MultiFile.list = $('#'+MultiFile.wrapID+'_list');
							};
       MultiFile.list = $(MultiFile.list);
							
       //===
       
       // Bind a new element
       MultiFile.addSlave = function( slave, slave_count ){
								//if(window.console) console.log('MultiFile.addSlave',slave_count);
								
        // Keep track of how many elements have been displayed
        MultiFile.n++;
        // Add reference to master element
        slave.MultiFile = MultiFile;
								
								// BUG FIX: http://plugins.jquery.com/node/1495
								// Clear identifying properties from clones
								if(slave_count>0) slave.id = slave.name = '';
								
        // Define element's ID and name (upload components need this!)
        //slave.id = slave.id || MultiFile.generateID(slave_count);
								if(slave_count>0) slave.id = MultiFile.generateID(slave_count);
								//FIX for: http://code.google.com/p/jquery-multifile-plugin/issues/detail?id=23
        
        // 2008-Apr-29: New customizable naming convention (see url below)
        // http://groups.google.com/group/jquery-dev/browse_frm/thread/765c73e41b34f924#
        slave.name = String(MultiFile.namePattern
         /*master name*/.replace(/\$name/gi,$(MultiFile.clone).attr('name'))
         /*master id  */.replace(/\$id/gi,  $(MultiFile.clone).attr('id'))
         /*group count*/.replace(/\$g/gi,   group_count)//(group_count>0?group_count:''))
         /*slave count*/.replace(/\$i/gi,   slave_count)//(slave_count>0?slave_count:''))
        );
        
        // If we've reached maximum number, disable input slave
        if( (MultiFile.max > 0) && ((MultiFile.n-1) > (MultiFile.max)) )//{ // MultiFile.n Starts at 1, so subtract 1 to find true count
         slave.disabled = true;
        //};
        
        // Remember most recent slave
        MultiFile.current = MultiFile.slaves[slave_count] = slave;
        
								// We'll use jQuery from now on
								slave = $(slave);
        
        // Clear value
        slave.val('').attr('value','')[0].value = '';
        
								// Stop plugin initializing on slaves
								slave.addClass('MultiFile-applied');
								
        // Triggered when a file is selected
        slave.change(function(){
          //if(window.console) console.log('MultiFile.slave.change',slave_count);
 								 
          // Lose focus to stop IE7 firing onchange again
          $(this).blur();
          
          //# Trigger Event! onFileSelect
          if(!MultiFile.trigger('onFileSelect', this, MultiFile)) return false;
          //# End Event!
          
          //# Retrive value of selected file from element
          var ERROR = '', v = String(this.value || ''/*.attr('value)*/);
          
          // check extension
          if(MultiFile.accept && v && !v.match(MultiFile.rxAccept))//{
            ERROR = MultiFile.STRING.denied.replace('$ext', String(v.match(/\.\w{1,4}$/gi)));
           //}
          //};
          
          // Disallow duplicates
										for(var f in MultiFile.slaves)//{
           if(MultiFile.slaves[f] && MultiFile.slaves[f]!=this)//{
  										//console.log(MultiFile.slaves[f],MultiFile.slaves[f].value);
            if(MultiFile.slaves[f].value==v)//{
             ERROR = MultiFile.STRING.duplicate.replace('$file', v.match(/[^\/\\]+$/gi));
            //};
           //};
          //};
          
          // Create a new file input element
          var newEle = $(MultiFile.clone).clone();// Copy parent attributes - Thanks to Jonas Wagner
          //# Let's remember which input we've generated so
          // we can disable the empty ones before submission
          // See: http://plugins.jquery.com/node/1495
          newEle.addClass('MultiFile');
          
          // Handle error
          if(ERROR!=''){
            // Handle error
            MultiFile.error(ERROR);
												
            // 2007-06-24: BUG FIX - Thanks to Adrian Wróbel <adrian [dot] wrobel [at] gmail.com>
            // Ditch the trouble maker and add a fresh new element
            MultiFile.n--;
            MultiFile.addSlave(newEle[0], slave_count);
            slave.parent().prepend(newEle);
            slave.remove();
            return false;
          };
          
          // Hide this element (NB: display:none is evil!)
          $(this).css({ position:'absolute', top: '-3000px' });
          
          // Add new element to the form
          slave.after(newEle);
          
          // Update list
          MultiFile.addToList( this, slave_count );
          
          // Bind functionality
          MultiFile.addSlave( newEle[0], slave_count+1 );
          
          //# Trigger Event! afterFileSelect
          if(!MultiFile.trigger('afterFileSelect', this, MultiFile)) return false;
          //# End Event!
          
        }); // slave.change()
								
								// Save control to element
								$(slave).data('MultiFile', MultiFile);
								
       };// MultiFile.addSlave
       // Bind a new element
       
       
       
       // Add a new file to the list
       MultiFile.addToList = function( slave, slave_count ){
        //if(window.console) console.log('MultiFile.addToList',slave_count);
								
        //# Trigger Event! onFileAppend
        if(!MultiFile.trigger('onFileAppend', slave, MultiFile)) return false;
        //# End Event!
        
        // Create label elements
        var
         r = $('<div class="MultiFile-label"></div>'),
         v = String(slave.value || ''/*.attr('value)*/),
         a = $('<span class="MultiFile-title" title="'+MultiFile.STRING.selected.replace('$file', v)+'">'+MultiFile.STRING.file.replace('$file', v.match(/[^\/\\]+$/gi)[0])+'</span>'),
         b = $('<a class="MultiFile-remove" href="#'+MultiFile.wrapID+'">'+MultiFile.STRING.remove+'</a>');
        
        // Insert label
        MultiFile.list.append(
         r.append(b, ' ', a)
        );
        
        b
								.click(function(){
         
          //# Trigger Event! onFileRemove
          if(!MultiFile.trigger('onFileRemove', slave, MultiFile)) return false;
          //# End Event!
          
          MultiFile.n--;
          MultiFile.current.disabled = false;
          
          // Remove element, remove label, point to current
										MultiFile.slaves[slave_count] = null;
										$(slave).remove();
										$(this).parent().remove();
										
          // Show most current element again (move into view) and clear selection
          $(MultiFile.current).css({ position:'', top: '' });
										$(MultiFile.current).reset().val('').attr('value', '')[0].value = '';
          
          //# Trigger Event! afterFileRemove
          if(!MultiFile.trigger('afterFileRemove', slave, MultiFile)) return false;
          //# End Event!
										
          return false;
        });
        
        //# Trigger Event! afterFileAppend
        if(!MultiFile.trigger('afterFileAppend', slave, MultiFile)) return false;
        //# End Event!
        
       }; // MultiFile.addToList
       // Add element to selected files list
       
       
       
       // Bind functionality to the first element
       if(!MultiFile.MultiFile) MultiFile.addSlave(MultiFile.e, 0);
       
       // Increment control count
       //MultiFile.I++; // using window.MultiFile
       MultiFile.n++;
							
							// Save control to element
							MultiFile.E.data('MultiFile', MultiFile);
							

			//#####################################################################
			// MAIN PLUGIN FUNCTIONALITY - END
			//#####################################################################
		}); // each element
	};
	
	/*--------------------------------------------------------*/
	
	/*
		### Core functionality and API ###
	*/
	$.extend($.fn.MultiFile, {
  /**
   * This method removes all selected files
   *
   * Returns a jQuery collection of all affected elements.
   *
   * @name reset
   * @type jQuery
   * @cat Plugins/MultiFile
   * @author Diego A. (http://www.fyneworks.com/)
   *
   * @example $.fn.MultiFile.reset();
   */
  reset: function(){
			var settings = $(this).data('MultiFile');
			//if(settings) settings.wrapper.find('a.MultiFile-remove').click();
			if(settings) settings.list.find('a.MultiFile-remove').click();
   return $(this);
  },
  
  
  /**
   * This utility makes it easy to disable all 'empty' file elements in the document before submitting a form.
   * It marks the affected elements so they can be easily re-enabled after the form submission or validation.
   *
   * Returns a jQuery collection of all affected elements.
   *
   * @name disableEmpty
   * @type jQuery
   * @cat Plugins/MultiFile
   * @author Diego A. (http://www.fyneworks.com/)
   *
   * @example $.fn.MultiFile.disableEmpty();
   * @param String class (optional) A string specifying a class to be applied to all affected elements - Default: 'mfD'.
   */
  disableEmpty: function(klass){ klass = (typeof(klass)=='string'?klass:'')||'mfD';
   var o = [];
   $('input:file.MultiFile').each(function(){ if($(this).val()=='') o[o.length] = this; });
   return $(o).each(function(){ this.disabled = true }).addClass(klass);
  },
  
  
		/**
			* This method re-enables 'empty' file elements that were disabled (and marked) with the $.fn.MultiFile.disableEmpty method.
			*
			* Returns a jQuery collection of all affected elements.
			*
			* @name reEnableEmpty
			* @type jQuery
			* @cat Plugins/MultiFile
			* @author Diego A. (http://www.fyneworks.com/)
			*
			* @example $.fn.MultiFile.reEnableEmpty();
			* @param String klass (optional) A string specifying the class that was used to mark affected elements - Default: 'mfD'.
			*/
  reEnableEmpty: function(klass){ klass = (typeof(klass)=='string'?klass:'')||'mfD';
   return $('input:file.'+klass).removeClass(klass).each(function(){ this.disabled = false });
  },
  
  
		/**
			* This method will intercept other jQuery plugins and disable empty file input elements prior to form submission
			*
	
			* @name intercept
			* @cat Plugins/MultiFile
			* @author Diego A. (http://www.fyneworks.com/)
			*
			* @example $.fn.MultiFile.intercept();
			* @param Array methods (optional) Array of method names to be intercepted
			*/
  intercepted: {},
  intercept: function(methods, context, args){
   var method, value; args = args || [];
   if(args.constructor.toString().indexOf("Array")<0) args = [ args ];
   if(typeof(methods)=='function'){
    $.fn.MultiFile.disableEmpty();
    value = methods.apply(context || window, args);
				//SEE-http://code.google.com/p/jquery-multifile-plugin/issues/detail?id=27
				setTimeout(function(){ $.fn.MultiFile.reEnableEmpty() },1000);
    return value;
   };
   if(methods.constructor.toString().indexOf("Array")<0) methods = [methods];
   for(var i=0;i<methods.length;i++){
    method = methods[i]+''; // make sure that we have a STRING
    if(method) (function(method){ // make sure that method is ISOLATED for the interception
     $.fn.MultiFile.intercepted[method] = $.fn[method] || function(){};
     $.fn[method] = function(){
      $.fn.MultiFile.disableEmpty();
      value = $.fn.MultiFile.intercepted[method].apply(this, arguments);
						//SEE http://code.google.com/p/jquery-multifile-plugin/issues/detail?id=27
      setTimeout(function(){ $.fn.MultiFile.reEnableEmpty() },1000);
      return value;
     }; // interception
    })(method); // MAKE SURE THAT method IS ISOLATED for the interception
   };// for each method
  } // $.fn.MultiFile.intercept
		
 });
	
	/*--------------------------------------------------------*/
	
	/*
		### Default Settings ###
		eg.: You can override default control like this:
		$.fn.MultiFile.options.accept = 'gif|jpg';
	*/
	$.fn.MultiFile.options = { //$.extend($.fn.MultiFile, { options: {
		accept: '', // accepted file extensions
		max: -1,    // maximum number of selectable files
		
		// name to use for newly created elements
		namePattern: '$name', // same name by default (which creates an array)
         /*master name*/ // use $name
         /*master id  */ // use $id
         /*group count*/ // use $g
         /*slave count*/ // use $i
									/*other      */ // use any combination of he above, eg.: $name_file$i
		
		// STRING: collection lets you show messages in different languages
		STRING: {
			remove:'x',
			denied:'You cannot select a $ext file.\nTry again...',
			file:'$file',
			selected:'File selected: $file',
			duplicate:'This file has already been selected:\n$file'
		},
		
		// name of methods that should be automcatically intercepted so the plugin can disable
		// extra file elements that are empty before execution and automatically re-enable them afterwards
  autoIntercept: [ 'submit', 'ajaxSubmit', 'ajaxForm', 'validate', 'valid' /* array of methods to intercept */ ],
		
		// error handling function
		error: function(s){
			/*
			ERROR! blockUI is not currently working in IE
			if($.blockUI){
				$.blockUI({
					message: s.replace(/\n/gi,'<br/>'),
					css: { 
						border:'none', padding:'15px', size:'12.0pt',
						backgroundColor:'#900', color:'#fff',
						opacity:'.8','-webkit-border-radius': '10px','-moz-border-radius': '10px'
					}
				});
				window.setTimeout($.unblockUI, 2000);
			}
			else//{// save a byte!
			*/
			 alert(s);
			//}// save a byte!
		}
 }; //} });
	
	/*--------------------------------------------------------*/
	
	/*
		### Additional Methods ###
		Required functionality outside the plugin's scope
	*/
	
	// Native input reset method - because this alone doesn't always work: $(element).val('').attr('value', '')[0].value = '';
	$.fn.reset = function(){ return this.each(function(){ try{ this.reset(); }catch(e){} }); };
	
	/*--------------------------------------------------------*/
	
	/*
		### Default implementation ###
		The plugin will attach itself to file inputs
		with the class 'multi' when the page loads
	*/
	$(function(){
  //$("input:file.multi").MultiFile();
  $("input[type=file].multi").MultiFile();
 });
	
	
	
/*# AVOID COLLISIONS #*/
})(jQuery);
/*# AVOID COLLISIONS #*/

//!!! myjui/js/md5.js
/*
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.1 Copyright (C) Paul Johnston 1999 - 2002.
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
var b64pad  = ""; /* base-64 pad character. "=" for strict RFC compliance   */
var chrsz   = 8;  /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
function hex_md5(s){ return binl2hex(core_md5(str2binl(s), s.length * chrsz));}
function b64_md5(s){ return binl2b64(core_md5(str2binl(s), s.length * chrsz));}
function str_md5(s){ return binl2str(core_md5(str2binl(s), s.length * chrsz));}
function hex_hmac_md5(key, data) { return binl2hex(core_hmac_md5(key, data)); }
function b64_hmac_md5(key, data) { return binl2b64(core_hmac_md5(key, data)); }
function str_hmac_md5(key, data) { return binl2str(core_hmac_md5(key, data)); }

/*
 * Perform a simple self-test to see if the VM is working
 */
function md5_vm_test()
{
  return hex_md5("abc") == "900150983cd24fb0d6963f7d28e17f72";
}

/*
 * Calculate the MD5 of an array of little-endian words, and a bit length
 */
function core_md5(x, len)
{
  /* append padding */
  x[len >> 5] |= 0x80 << ((len) % 32);
  x[(((len + 64) >>> 9) << 4) + 14] = len;

  var a =  1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d =  271733878;

  for(var i = 0; i < x.length; i += 16)
  {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;

    a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
    d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
    c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
    b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
    a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
    d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
    c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
    b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
    a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
    d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
    c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
    b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
    a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
    d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
    c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
    b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);

    a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
    d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
    c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
    b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
    a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
    d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
    c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
    b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
    a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
    d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
    c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
    b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
    a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
    d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
    c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
    b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);

    a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
    d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
    c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
    b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
    a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
    d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
    c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
    b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
    a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
    d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
    c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
    b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
    a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
    d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
    c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
    b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);

    a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
    d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
    c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
    b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
    a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
    d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
    c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
    b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
    a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
    d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
    c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
    b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
    a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
    d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
    c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
    b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);

    a = safe_add(a, olda);
    b = safe_add(b, oldb);
    c = safe_add(c, oldc);
    d = safe_add(d, oldd);
  }
  return Array(a, b, c, d);

}

/*
 * These functions implement the four basic operations the algorithm uses.
 */
function md5_cmn(q, a, b, x, s, t)
{
  return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
}
function md5_ff(a, b, c, d, x, s, t)
{
  return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
}
function md5_gg(a, b, c, d, x, s, t)
{
  return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
}
function md5_hh(a, b, c, d, x, s, t)
{
  return md5_cmn(b ^ c ^ d, a, b, x, s, t);
}
function md5_ii(a, b, c, d, x, s, t)
{
  return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
}

/*
 * Calculate the HMAC-MD5, of a key and some data
 */
function core_hmac_md5(key, data)
{
  var bkey = str2binl(key);
  if(bkey.length > 16) bkey = core_md5(bkey, key.length * chrsz);

  var ipad = Array(16), opad = Array(16);
  for(var i = 0; i < 16; i++)
  {
    ipad[i] = bkey[i] ^ 0x36363636;
    opad[i] = bkey[i] ^ 0x5C5C5C5C;
  }

  var hash = core_md5(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
  return core_md5(opad.concat(hash), 512 + 128);
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bit_rol(num, cnt)
{
  return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * Convert a string to an array of little-endian words
 * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
 */
function str2binl(str)
{
  var bin = Array();
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < str.length * chrsz; i += chrsz)
    bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);
  return bin;
}

/*
 * Convert an array of little-endian words to a string
 */
function binl2str(bin)
{
  var str = "";
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < bin.length * 32; i += chrsz)
    str += String.fromCharCode((bin[i>>5] >>> (i % 32)) & mask);
  return str;
}

/*
 * Convert an array of little-endian words to a hex string.
 */
function binl2hex(binarray)
{
  var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i++)
  {
    str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) +
           hex_tab.charAt((binarray[i>>2] >> ((i%4)*8  )) & 0xF);
  }
  return str;
}

/*
 * Convert an array of little-endian words to a base-64 string
 */
function binl2b64(binarray)
{
  var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i += 3)
  {
    var triplet = (((binarray[i   >> 2] >> 8 * ( i   %4)) & 0xFF) << 16)
                | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 )
                |  ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
    for(var j = 0; j < 4; j++)
    {
      if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
      else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
    }
  }
  return str;
}

//!!! myjui/js/json2.js
/*
    http://www.JSON.org/json2.js
    2009-09-29

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html

    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.

    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.
*/

/*jslint evil: true, strict: false */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (!this.JSON) {
    this.JSON = {};
}

(function () {

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf()) ?
                   this.getUTCFullYear()   + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate())      + 'T' +
                 f(this.getUTCHours())     + ':' +
                 f(this.getUTCMinutes())   + ':' +
                 f(this.getUTCSeconds())   + 'Z' : null;
        };

        String.prototype.toJSON =
        Number.prototype.toJSON =
        Boolean.prototype.toJSON = function (key) {
            return this.valueOf();
        };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ?
            '"' + string.replace(escapable, function (a) {
                var c = meta[a];
                return typeof c === 'string' ? c :
                    '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            }) + '"' :
            '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0 ? '[]' :
                    gap ? '[\n' + gap +
                            partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                          '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    k = rep[i];
                    if (typeof k === 'string') {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0 ? '{}' :
                gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                        mind + '}' : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                     typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/.
test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ?
                    walk({'': j}, '') : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());

//!!! myjui/js/swfobject.js
/*	SWFObject v2.2 <http://code.google.com/p/swfobject/> 
	is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
var swfobject=function(){var D="undefined",r="object",S="Shockwave Flash",W="ShockwaveFlash.ShockwaveFlash",q="application/x-shockwave-flash",R="SWFObjectExprInst",x="onreadystatechange",O=window,j=document,t=navigator,T=false,U=[h],o=[],N=[],I=[],l,Q,E,B,J=false,a=false,n,G,m=true,M=function(){var aa=typeof j.getElementById!=D&&typeof j.getElementsByTagName!=D&&typeof j.createElement!=D,ah=t.userAgent.toLowerCase(),Y=t.platform.toLowerCase(),ae=Y?/win/.test(Y):/win/.test(ah),ac=Y?/mac/.test(Y):/mac/.test(ah),af=/webkit/.test(ah)?parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,X=!+"\v1",ag=[0,0,0],ab=null;if(typeof t.plugins!=D&&typeof t.plugins[S]==r){ab=t.plugins[S].description;if(ab&&!(typeof t.mimeTypes!=D&&t.mimeTypes[q]&&!t.mimeTypes[q].enabledPlugin)){T=true;X=false;ab=ab.replace(/^.*\s+(\S+\s+\S+$)/,"$1");ag[0]=parseInt(ab.replace(/^(.*)\..*$/,"$1"),10);ag[1]=parseInt(ab.replace(/^.*\.(.*)\s.*$/,"$1"),10);ag[2]=/[a-zA-Z]/.test(ab)?parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/,"$1"),10):0}}else{if(typeof O.ActiveXObject!=D){try{var ad=new ActiveXObject(W);if(ad){ab=ad.GetVariable("$version");if(ab){X=true;ab=ab.split(" ")[1].split(",");ag=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}}catch(Z){}}}return{w3:aa,pv:ag,wk:af,ie:X,win:ae,mac:ac}}(),k=function(){if(!M.w3){return}if((typeof j.readyState!=D&&j.readyState=="complete")||(typeof j.readyState==D&&(j.getElementsByTagName("body")[0]||j.body))){f()}if(!J){if(typeof j.addEventListener!=D){j.addEventListener("DOMContentLoaded",f,false)}if(M.ie&&M.win){j.attachEvent(x,function(){if(j.readyState=="complete"){j.detachEvent(x,arguments.callee);f()}});if(O==top){(function(){if(J){return}try{j.documentElement.doScroll("left")}catch(X){setTimeout(arguments.callee,0);return}f()})()}}if(M.wk){(function(){if(J){return}if(!/loaded|complete/.test(j.readyState)){setTimeout(arguments.callee,0);return}f()})()}s(f)}}();function f(){if(J){return}try{var Z=j.getElementsByTagName("body")[0].appendChild(C("span"));Z.parentNode.removeChild(Z)}catch(aa){return}J=true;var X=U.length;for(var Y=0;Y<X;Y++){U[Y]()}}function K(X){if(J){X()}else{U[U.length]=X}}function s(Y){if(typeof O.addEventListener!=D){O.addEventListener("load",Y,false)}else{if(typeof j.addEventListener!=D){j.addEventListener("load",Y,false)}else{if(typeof O.attachEvent!=D){i(O,"onload",Y)}else{if(typeof O.onload=="function"){var X=O.onload;O.onload=function(){X();Y()}}else{O.onload=Y}}}}}function h(){if(T){V()}else{H()}}function V(){var X=j.getElementsByTagName("body")[0];var aa=C(r);aa.setAttribute("type",q);var Z=X.appendChild(aa);if(Z){var Y=0;(function(){if(typeof Z.GetVariable!=D){var ab=Z.GetVariable("$version");if(ab){ab=ab.split(" ")[1].split(",");M.pv=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}else{if(Y<10){Y++;setTimeout(arguments.callee,10);return}}X.removeChild(aa);Z=null;H()})()}else{H()}}function H(){var ag=o.length;if(ag>0){for(var af=0;af<ag;af++){var Y=o[af].id;var ab=o[af].callbackFn;var aa={success:false,id:Y};if(M.pv[0]>0){var ae=c(Y);if(ae){if(F(o[af].swfVersion)&&!(M.wk&&M.wk<312)){w(Y,true);if(ab){aa.success=true;aa.ref=z(Y);ab(aa)}}else{if(o[af].expressInstall&&A()){var ai={};ai.data=o[af].expressInstall;ai.width=ae.getAttribute("width")||"0";ai.height=ae.getAttribute("height")||"0";if(ae.getAttribute("class")){ai.styleclass=ae.getAttribute("class")}if(ae.getAttribute("align")){ai.align=ae.getAttribute("align")}var ah={};var X=ae.getElementsByTagName("param");var ac=X.length;for(var ad=0;ad<ac;ad++){if(X[ad].getAttribute("name").toLowerCase()!="movie"){ah[X[ad].getAttribute("name")]=X[ad].getAttribute("value")}}P(ai,ah,Y,ab)}else{p(ae);if(ab){ab(aa)}}}}}else{w(Y,true);if(ab){var Z=z(Y);if(Z&&typeof Z.SetVariable!=D){aa.success=true;aa.ref=Z}ab(aa)}}}}}function z(aa){var X=null;var Y=c(aa);if(Y&&Y.nodeName=="OBJECT"){if(typeof Y.SetVariable!=D){X=Y}else{var Z=Y.getElementsByTagName(r)[0];if(Z){X=Z}}}return X}function A(){return !a&&F("6.0.65")&&(M.win||M.mac)&&!(M.wk&&M.wk<312)}function P(aa,ab,X,Z){a=true;E=Z||null;B={success:false,id:X};var ae=c(X);if(ae){if(ae.nodeName=="OBJECT"){l=g(ae);Q=null}else{l=ae;Q=X}aa.id=R;if(typeof aa.width==D||(!/%$/.test(aa.width)&&parseInt(aa.width,10)<310)){aa.width="310"}if(typeof aa.height==D||(!/%$/.test(aa.height)&&parseInt(aa.height,10)<137)){aa.height="137"}j.title=j.title.slice(0,47)+" - Flash Player Installation";var ad=M.ie&&M.win?"ActiveX":"PlugIn",ac="MMredirectURL="+O.location.toString().replace(/&/g,"%26")+"&MMplayerType="+ad+"&MMdoctitle="+j.title;if(typeof ab.flashvars!=D){ab.flashvars+="&"+ac}else{ab.flashvars=ac}if(M.ie&&M.win&&ae.readyState!=4){var Y=C("div");X+="SWFObjectNew";Y.setAttribute("id",X);ae.parentNode.insertBefore(Y,ae);ae.style.display="none";(function(){if(ae.readyState==4){ae.parentNode.removeChild(ae)}else{setTimeout(arguments.callee,10)}})()}u(aa,ab,X)}}function p(Y){if(M.ie&&M.win&&Y.readyState!=4){var X=C("div");Y.parentNode.insertBefore(X,Y);X.parentNode.replaceChild(g(Y),X);Y.style.display="none";(function(){if(Y.readyState==4){Y.parentNode.removeChild(Y)}else{setTimeout(arguments.callee,10)}})()}else{Y.parentNode.replaceChild(g(Y),Y)}}function g(ab){var aa=C("div");if(M.win&&M.ie){aa.innerHTML=ab.innerHTML}else{var Y=ab.getElementsByTagName(r)[0];if(Y){var ad=Y.childNodes;if(ad){var X=ad.length;for(var Z=0;Z<X;Z++){if(!(ad[Z].nodeType==1&&ad[Z].nodeName=="PARAM")&&!(ad[Z].nodeType==8)){aa.appendChild(ad[Z].cloneNode(true))}}}}}return aa}function u(ai,ag,Y){var X,aa=c(Y);if(M.wk&&M.wk<312){return X}if(aa){if(typeof ai.id==D){ai.id=Y}if(M.ie&&M.win){var ah="";for(var ae in ai){if(ai[ae]!=Object.prototype[ae]){if(ae.toLowerCase()=="data"){ag.movie=ai[ae]}else{if(ae.toLowerCase()=="styleclass"){ah+=' class="'+ai[ae]+'"'}else{if(ae.toLowerCase()!="classid"){ah+=" "+ae+'="'+ai[ae]+'"'}}}}}var af="";for(var ad in ag){if(ag[ad]!=Object.prototype[ad]){af+='<param name="'+ad+'" value="'+ag[ad]+'" />'}}aa.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+ah+">"+af+"</object>";N[N.length]=ai.id;X=c(ai.id)}else{var Z=C(r);Z.setAttribute("type",q);for(var ac in ai){if(ai[ac]!=Object.prototype[ac]){if(ac.toLowerCase()=="styleclass"){Z.setAttribute("class",ai[ac])}else{if(ac.toLowerCase()!="classid"){Z.setAttribute(ac,ai[ac])}}}}for(var ab in ag){if(ag[ab]!=Object.prototype[ab]&&ab.toLowerCase()!="movie"){e(Z,ab,ag[ab])}}aa.parentNode.replaceChild(Z,aa);X=Z}}return X}function e(Z,X,Y){var aa=C("param");aa.setAttribute("name",X);aa.setAttribute("value",Y);Z.appendChild(aa)}function y(Y){var X=c(Y);if(X&&X.nodeName=="OBJECT"){if(M.ie&&M.win){X.style.display="none";(function(){if(X.readyState==4){b(Y)}else{setTimeout(arguments.callee,10)}})()}else{X.parentNode.removeChild(X)}}}function b(Z){var Y=c(Z);if(Y){for(var X in Y){if(typeof Y[X]=="function"){Y[X]=null}}Y.parentNode.removeChild(Y)}}function c(Z){var X=null;try{X=j.getElementById(Z)}catch(Y){}return X}function C(X){return j.createElement(X)}function i(Z,X,Y){Z.attachEvent(X,Y);I[I.length]=[Z,X,Y]}function F(Z){var Y=M.pv,X=Z.split(".");X[0]=parseInt(X[0],10);X[1]=parseInt(X[1],10)||0;X[2]=parseInt(X[2],10)||0;return(Y[0]>X[0]||(Y[0]==X[0]&&Y[1]>X[1])||(Y[0]==X[0]&&Y[1]==X[1]&&Y[2]>=X[2]))?true:false}function v(ac,Y,ad,ab){if(M.ie&&M.mac){return}var aa=j.getElementsByTagName("head")[0];if(!aa){return}var X=(ad&&typeof ad=="string")?ad:"screen";if(ab){n=null;G=null}if(!n||G!=X){var Z=C("style");Z.setAttribute("type","text/css");Z.setAttribute("media",X);n=aa.appendChild(Z);if(M.ie&&M.win&&typeof j.styleSheets!=D&&j.styleSheets.length>0){n=j.styleSheets[j.styleSheets.length-1]}G=X}if(M.ie&&M.win){if(n&&typeof n.addRule==r){n.addRule(ac,Y)}}else{if(n&&typeof j.createTextNode!=D){n.appendChild(j.createTextNode(ac+" {"+Y+"}"))}}}function w(Z,X){if(!m){return}var Y=X?"visible":"hidden";if(J&&c(Z)){c(Z).style.visibility=Y}else{v("#"+Z,"visibility:"+Y)}}function L(Y){var Z=/[\\\"<>\.;]/;var X=Z.exec(Y)!=null;return X&&typeof encodeURIComponent!=D?encodeURIComponent(Y):Y}var d=function(){if(M.ie&&M.win){window.attachEvent("onunload",function(){var ac=I.length;for(var ab=0;ab<ac;ab++){I[ab][0].detachEvent(I[ab][1],I[ab][2])}var Z=N.length;for(var aa=0;aa<Z;aa++){y(N[aa])}for(var Y in M){M[Y]=null}M=null;for(var X in swfobject){swfobject[X]=null}swfobject=null})}}();return{registerObject:function(ab,X,aa,Z){if(M.w3&&ab&&X){var Y={};Y.id=ab;Y.swfVersion=X;Y.expressInstall=aa;Y.callbackFn=Z;o[o.length]=Y;w(ab,false)}else{if(Z){Z({success:false,id:ab})}}},getObjectById:function(X){if(M.w3){return z(X)}},embedSWF:function(ab,ah,ae,ag,Y,aa,Z,ad,af,ac){var X={success:false,id:ah};if(M.w3&&!(M.wk&&M.wk<312)&&ab&&ah&&ae&&ag&&Y){w(ah,false);K(function(){ae+="";ag+="";var aj={};if(af&&typeof af===r){for(var al in af){aj[al]=af[al]}}aj.data=ab;aj.width=ae;aj.height=ag;var am={};if(ad&&typeof ad===r){for(var ak in ad){am[ak]=ad[ak]}}if(Z&&typeof Z===r){for(var ai in Z){if(typeof am.flashvars!=D){am.flashvars+="&"+ai+"="+Z[ai]}else{am.flashvars=ai+"="+Z[ai]}}}if(F(Y)){var an=u(aj,am,ah);if(aj.id==ah){w(ah,true)}X.success=true;X.ref=an}else{if(aa&&A()){aj.data=aa;P(aj,am,ah,ac);return}else{w(ah,true)}}if(ac){ac(X)}})}else{if(ac){ac(X)}}},switchOffAutoHideShow:function(){m=false},ua:M,getFlashPlayerVersion:function(){return{major:M.pv[0],minor:M.pv[1],release:M.pv[2]}},hasFlashPlayerVersion:F,createSWF:function(Z,Y,X){if(M.w3){return u(Z,Y,X)}else{return undefined}},showExpressInstall:function(Z,aa,X,Y){if(M.w3&&A()){P(Z,aa,X,Y)}},removeSWF:function(X){if(M.w3){y(X)}},createCSS:function(aa,Z,Y,X){if(M.w3){v(aa,Z,Y,X)}},addDomLoadEvent:K,addLoadEvent:s,getQueryParamValue:function(aa){var Z=j.location.search||j.location.hash;if(Z){if(/\?/.test(Z)){Z=Z.split("?")[1]}if(aa==null){return L(Z)}var Y=Z.split("&");for(var X=0;X<Y.length;X++){if(Y[X].substring(0,Y[X].indexOf("="))==aa){return L(Y[X].substring((Y[X].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(a){var X=c(R);if(X&&l){X.parentNode.replaceChild(l,X);if(Q){w(Q,true);if(M.ie&&M.win){l.style.display="block"}}if(E){E(B)}}a=false}}}}();
//!!! myjui/js/css_browser_selector.js
/*
CSS Browser Selector v0.2.9
Rafael Lima (http://rafael.adm.br)
http://rafael.adm.br/css_browser_selector
License: http://creativecommons.org/licenses/by/2.5/
Contributors: http://rafael.adm.br/css_browser_selector#contributors
*/
var css_browser_selector = function() {var ua=navigator.userAgent.toLowerCase(),is=function(t){return ua.indexOf(t) != -1;},h=document.getElementsByTagName('html')[0],b=(!(/opera|webtv/i.test(ua))&&/msie\s(\d)/.test(ua))?('ie ie'+RegExp.$1):is('firefox/2')?'gecko ff2':is('firefox/3')?'gecko ff3':is('gecko/')?'gecko':is('opera/9')?'opera opera9':/opera\s(\d)/.test(ua)?'opera opera'+RegExp.$1:is('konqueror')?'konqueror':is('chrome')?'chrome webkit safari':is('applewebkit/')?'webkit safari':is('mozilla/')?'gecko':'',os=(is('x11')||is('linux'))?' linux':is('mac')?' mac':is('win')?' win':'';b+= (navigator.userAgent.toLowerCase().indexOf('adobeair')>-1 ? ' adobeair' : '');var c=b+os+' js'; h.className += h.className?' '+c:c;}();
//!!! myjui/source/base/jquerybridge.js
mj={version:'0.8 alpha'};
var userAgent = navigator.userAgent.toLowerCase();
$.browser = {
	version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [])[1],
	safari: /webkit/.test( userAgent ),
	opera: /opera/.test( userAgent ),
	msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
	mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
};
$(function(){
	mj.bd = $(document.body);
	window.onresize = function(){
		var l = mj._windowResizeListeners;
		if(l){
			return $(l).eachR(function(){
				if(this.scope.componentClass)
					return this.fn.apply(this.scope);
				else
					return null;
			});
		}
	};
	if($.browser.msie)
		mj.bd.addClass('mj-ie mj-ie'+parseInt($.browser.version));
});
$.fn.hasParent = function(parent){
	var fp;
	$(this[0]).parents().each(function(){
		if(this===parent)
			fp = this;
	});
	return fp;
};
$.fn.hasAbsoluteParent = function(){
	var fp;
	$(this[0]).parents().each(function(){
		var t = $(this);
		if(t.css && (t.css('position')=='absolute' ||t.css('position')=='relative'))
			fp = t;
	});
	return fp;
};
$.eachR = function( obj, fn, args ) {
	if ( obj.length == undefined )
		for ( var i in obj )
			fn.apply( obj[i], args || [i, obj[i]] );
	else
		for ( var i = 0, ol = obj.length; i < ol; i++ )
			if ( fn.apply( obj[i], args || [i, obj[i]] ) === false )
				return false;
	return obj;
};
$.fn.eachR = function( fn, args ) {
	return $.eachR( this, fn, args );
};
$.fn.position = function(ret){
	return this.offset({scroll:false}, ret);
};
$.fn.swapClass = function(c1, c2) {
	return this.each(function() {
		var $this = $(this);
		if ( $this.hasClass(c1) )
			$this.removeClass(c1).addClass(c2);
		else if ( $this.hasClass(c2) )
			$this.removeClass(c2).addClass(c1);
	});
};
$.fn.replaceclass = function(c1, c2) {
	return this.each(function() {
		var $this = $(this);
		if ( $this.hasClass(c1) )
			$this.removeClass(c1).addClass(c2);
	});
};
/*
$.fn.kkresizewidth = function(width, dont){
	if(width>0){
		width = parseInt(width);
		_oWidth = parseInt(this[0].style.width);
		if(width != _oWidth){
			this[0].style.width = parseInt(width) + 'px';
			if(dont!==true)
				this.trigger('kkresize');
		}
	}		
	return this;
};
$.fn.kkresizeheight = function(height, dont){
	if(height>0){
		height = parseInt(height);
		_oHeight = parseInt(this[0].style.height);
		if(height != _oHeight){
			this[0].style.height = parseInt(height) + 'px';
			if(dont!==true)
				this.trigger('kkresize');
		}
	}
	return this;
};
*/
$.fn.extend({
	load: function( url, params, callback ) {
		if ( jQuery.isFunction( url ) )
			return this.bind("load", url);

		var off = url.indexOf(" ");
		if ( off >= 0 ) {
			var selector = url.slice(off, url.length);
			url = url.slice(0, off);
		}

		callback = callback || function(){};

		// Default to a GET request
		var type = "GET";

		// If the second parameter was provided
		if ( params )
			// If it's a function
			if ( jQuery.isFunction( params ) ) {
				// We assume that it's the callback
				callback = params;
				params = null;

			// Otherwise, build a param string
			} else {
				params = jQuery.param( params );
				type = "POST";
			}

		var self = this;

		// Request the remote document
		jQuery.ajax({
			url: url,
			type: type,
			data: params,
			complete: function(res, status){
				// If successful, inject the HTML into all the matched elements
				if ( status == "success" || status == "notmodified" )
					// See if a selector was specified
					var ja = jQuery.active;
					self.html( selector ?
						// Create a dummy div to hold the results
						jQuery("<div/>")
							// inject the contents of the document in, removing the scripts
							// to avoid any 'Permission Denied' errors in IE
							.append(res.responseText.replace(/<script(.|\s)*?\/script>/g, ""))

							// Locate the specified elements
							.find(selector) :

						// If not, just inject the full result
						res.responseText );
					jQuery.active = ja;
				// Add delay to account for Safari's delay in globalEval
				setTimeout(function(){
					self.each( callback, [res.responseText, status, res] );
				}, 13);
			}
		});
		return this;
	}
});	
//!!! myjui/source/base/base.js
/** 
* @fileoverview Bu dosya kütüphane genelinde kullanılmakta olan genel sabit tanımlamaları ve metodları içerir
* @hazırlayan kk
* @sürüm 0.1
*/
/**
 * global mj objesi. UI'nin tamamında kullanılacak global değişkenlerin tutulduğu obje.
 * @name mj.glb
 */
mj.glb = {
	blankImage : 'http://oguzhan-PC/mj/res/myjui/images/spacer.gif',
	modalIndex : 500,
	menuDelay : 300,
	messageWidth : 300,
	messageHeight : 150,
	imagePath : '',
	//blankImage : 'http://myjui.com/spacer.gif',
	//editorSettings : {mjPath.php'ye alındı.
	//	root : 'res/openwysiwyg/'
	//},
	views : []
};
/**
 * Tuş değerlerinin tutulduğu obje
 * @name mj.keys
 */
mj.keys = {
	BACKSPACE : 8,
	TAB : 9,
	RETURN : 13,
	ENTER : 13,
	SHIFT : 16,
	CONTROL : 17,
	ESC : 27,
	SPACE : 32,
	PAGEUP : 33,
	PAGEDOWN : 34,
	END : 35,
	HOME : 36,
	LEFT : 37,
	UP : 38,
	RIGHT : 39,
	DOWN : 40,
	DELETE : 46,
	F5 : 116,
	/**
	 * Basılan tuşun özel tuş olması durumunda true döndüren fonksiyon
	 * @name mj.keys.isSpecial
	 * @function
	 * @type {Function}
	 * @param {Object} e Event objesi
	 * @return {Boolean} Özel tuş olması durumunda true döndürür
	 */
	isSpecial : function(e){
		var k = e.keyCode;
		return (e.type == 'keypress' && e.ctrlKey) || k == 9 || k == 13  || k == 40 || k == 27 ||
		(k == 16) || (k == 17) ||
		(k >= 18 && k <= 20) ||
		(k >= 33 && k <= 35) ||
		(k >= 36 && k <= 39) ||
		(k >= 44 && k <= 45);
	}	
};
/**
 * Verilen genişlik ve yükseklikte boşluk oluşturur
 * @name mj.insertSpacer
 * @function
 * @type {Function}
 * @param {Integer} width Genişlik değeri
 * @param {Integer} height Yükseklik değeri
 * @return {HTMLElement} parametrelerinde aldığı boyutta img döndürür
 */
mj.insertSpacer = function(width,height){
	width = width ? width : 1;
	height = height ? height : 1;
	return '<img style="width:'+width+'px;height:'+height+'px;" src="'+mj.glb.blankImage+'"/>';
};
/**
 * document.getElementById metodunun kısa çağırım şekli
 * @name mj.get
 * @function
 * @type {Function}
 * @param {String} id Aranan HTML elemanının id değeri
 * @return {HTMLElement} 
 */
mj.get = function(id){
	return document.getElementById(id);
};
/**
 * HTTP istekleri çalıştırılırken <i>'Yükleniyor...'</i> imgesini göstermek için kullanılan metod
 * @name mj.loaderShow
 * @function
 * @type {Function}
  */
mj.loaderShow = function(){
	if(!mj.loading)
		mj.loading = $(mj.NE(mj.bd, {cls:'mj-loader', html:mj.lng.glb.loadingText}));
	mj.loading.show();
};
/**
 * HTTP istekleri sonuçlandığında <i>'Yükleniyor...'</i> imgesini gizlemek için kullanılan metod
 * @name mj.loaderHide
 * @function
 * @type {Function}
  */
mj.loaderHide = function(){
	if(mj.loading)
		mj.loading.hide();
};
/**
 * Bu metod bir elemanın içeriğini güncellemek için kullanılır. İstek süresince <i>'Yükleniyor...'</i> imgesi görüntülenir.
 * @example
var container = mj.get('container');
mj.load(container, 'page.php?id=1&type=a');
 * @example
var container = mj.get('container');
var cb = function(responseText, status, response){
	alert(responseText);
};
mj.load(container, {
	url : 'page.php',
	params : {
		id : 1,
		type : 'a'
	},
	callback : cb
});
 * @name mj.load
 * @function
 * @type {Function}
 * @param {HTMLElement} cnt İçeriği güncellenecek olan eleman.
 * @param {Object/String} config Güncelleme parametreleridir. String olarak verildiğinde parametrenin istek yapılacak sayfa adresi olduğu varsayılır.
 * @config {String} url İstek yapılacak sayfa adresi
 * @config {Object} params İstek yapılacak sayfaya gönderilecek olan parametreler
 * @config {Function} callback İstek sonuçlandığında tetiklenecek olan fonksiyon
  */
mj.load=function(cnt,config){
	if((typeof cnt == 'object') && !config){
		config = cnt;
		cnt = mj.HNE();
	}
	var c = (typeof config == 'object') ? config : {url:config};
	var url = c.url, params = c.params, callback = c.callback;
	var cb = function(responseText, status, response){
		if(typeof callback=='function') callback(responseText, status, response);
		mj.loaderHide();
	};
	if(c.dataType=='script')
		$.ajax(c);
	else
	$(cnt).load(url, params, cb);
};
mj.getString = function(config){
	var cnt = $(mj.NE(mj.bd, {cls:'mj-invisible'}));
	var c = (typeof config == 'object') ? config : {url:config};
	var url = c.url, params = c.params, callback = c.callback;
	var cb = function(responseText){
		if(typeof callback=='function') callback(responseText);
		mj.loaderHide();
		cnt.remove();
	};
	cnt.load(url, params, cb);
};
/**
	* @param {Object} source kaynak nesne
	* @param {Object} target hedef nesne
	* @param {String} subKey alt nesnelerin tutulduğu property adı
	var tmpObj = this.cloneObject(source, target ,subKey);
*/
mj.cloneObject = function(source, subKey, target){
	if(!subKey)
		subKey = 'data';
	if(!target)
		target = {};
	for(var i in source)
		if(typeof source[i] != 'function'){
			var c = {};
			if(typeof source[i] !== 'object' || source[i] == null || source[i] == undefined)
				c = source[i];
			else
				for(var j in source[i]){
					if(typeof source[i][j] != 'function')
						if(j!=subKey)
							c[j] = source[i][j];
						else{
							c[j] = source[i][j] instanceof Array?[]:{};
							mj.cloneObject(source[i][j], subKey, c[j]);
						}
				}
			target[i] = c;
		}
	return target;
};

/**
 * config parametresi ile verilen nesnenin tüm özelliklerini obj parametresi ile verilen nesneye kopyalar
 * @name mj.apply
 * @function
 * @type {Function}
 * @param {Object} obj Hedef nesne
 * @param {Object} config Kaynak nesne
 * @return {Object} obj parametresi ile verilen nesne
 */
mj.apply=function(obj,config){
	if(obj&&config&&typeof config=='object')
		for(var p in config)
			obj[p]=config[p];
	return obj;
};
/**
 * config parametresi ile verilen nesnenin özelliklerinden obj parametresi ile verilen nesnede tanımlı olmayanları kopyalar
 * @name mj.applyIf
 * @function
 * @type {Function}
 * @param {Object} obj Hedef nesne
 * @param {Object} config Kaynak nesne
 * @return {Object} obj parametresi ile verilen nesne
 */
mj.applyIf = function(obj,config){
	if(typeof obj!=='object')
		obj = {};
	if(config)
		for(var p in config)
			if(typeof obj[p]=="undefined")
				obj[p]=config[p];
	return obj;
};
/**
 * kaynak sınıftan bir başka sınıf türetmeye yarar
 * @name mj.extend
 * @function
 * @type {Function}
 * @param {Object} sc Kaynak sınıf
 * @param {Object} bc Hedef sınıf
 */
mj.extend = function(sc, bc) {
	var fn = function() {};
	fn.prototype = bc.prototype;
	var scpb = sc.prototype;
	var scp = sc.prototype = new fn();
	scp.constructor = sc;
	if(bc.prototype.constructor == Object.prototype.constructor)
		bc.prototype.constructor = bc;
	mj.apply(scp, scpb);
	sc.superclass = bc.prototype;
};
/**
 * Sıralı olarak artan bir sayısal değer döndürür. Oluşturulan nesnelere id ataması yaparken kullanılıyor
 * @name mj.genId
 * @example
var newEl = mj.NE(document.body, {id: 'kk-'+mj.genId()});
 * @function
 * @type {Function}
 * @return {Integer} Oluşturulan sayısal değer
 */
mj.idCounter = 0;
mj.genId = function(prefix){
	return ((prefix?prefix:'')+(mj.idCounter++));
};
/**
 * DOM üzerinde yeni html elemanları oluşturur.
 * @name mj.NE
 * @function
 * @type {Function}
 * @param {HTMLElement} [container] Yeni elemanın içine oluşturulacağı hedef nesne. Varsayılan değeri <i>document.body</i> olarak ayarlanmıştır.
 * @param {HTMLElement} [config] Yeni elemanın özellikleri. Bu parametre içerisinde aşağıda belirtilenler dışında verilen özellikler
 varsa bunlar yeni elemanın özellikleri(attribute) olarak atanır<br>
 * @example
mj.NE(document.body, {tag:'span', html:'My Text', myattribute:'myvalue'});
<br><pre class="comment">Yukarıdaki kod sonucunda döküman içerisinde<xmp><span myattribute="myvalue">My Text</span></xmp>elemanı oluşur</pre>
 * @config {String} tag Elemanın HTML etiketi. Varsayılan değeri : <i>'div'</i>
 * @config {String} cls Elemanın stil sınıfı 
 * @config {String} html Elemanın içine eklenecek olan html ifadesi
 * @config {Array} children Bu parametre ile dizi halinde gönderilecek olan config nesneleri de oluşturularak yeni nesneye dahil edilir
 * @return {HTMLElement} Oluşturulan HTML elemanı
 */
mj.NE = function(container,config){
	config = !config ? {tag:'div'} : (config.tag ? config : mj.apply(config,{tag:'div'}));
	if(!config.draggable)
		config.draggable = 'false';
	var _c = container ? $(container) : $(document.body), chld = config.children;
	if(typeof config!=='string'){
		var attr = '';
		for(var item in config)
			if(typeof config[item]!=='function' && item!=='cls' && item!=='html' && item!=='tag' && item!=='children' && config[item])
				attr += ' ' + item +'="' + config[item] + '" ';
		var endtag = '</' + config.tag + '>', closetag = '>';
		if(config.tag=='input' || config.tag=='img'){
			endtag = '/>';
			closetag = '';
		}
		config = '<' + config.tag + (config.cls ? (' class="' + config.cls + '" ') : '') + attr + closetag + (config.html ? config.html : '') + endtag;
	}
	_c.append(config);
	var _ch = _c.children();
	if($.browser.msie)
		_ch = _c[0].lastChild;
	else
		_ch = _ch[_ch.length-1];
	if(chld)
		$(chld).each(function(){
			return mj.NE(_ch, this);
		});
	return _ch;
};
mj.HNE = function(){
	if(!mj.hiddenContainer)
		mj.hiddenContainer = mj.NE(mj.bd,{
			cls : 'mj-invisible'
		});
	return mj.NE(mj.hiddenContainer);
};
/**
 * Bir dizi içerisindeki nesnelerin <i>key</i> parametresinde belirtilen özelliği aranan 
 değere eşit olan ilk nesnenin sırasını verir. Eşleşen nesne olmazsa -1 sonucu döner. Kısa kullanım karşılığı : <b>mj.getIndex</b>
 * @example
var arr = [
	{id:0, name : 'a'},
	{id:1, name : 'b'},
	{id:2, name : 'c'},
	{id:3, name : 'b'}
];
var i = mj.getIndex(arr, 'name', 'b'); // =1
 * @name mj.getArrayElementIndex
 * @function
 * @type {Function}
 * @param {Array} array Nesneleri barındıran dizi
 * @param {String} key Kıyaslama kriteri
 * @param {Object/String/Number, vb...} keyValue Aranan değer
 * @return {Integer} Aranan elemanın sıra değeri. Değer bulunamaması durumunda -1
 */
mj.getIndex = mj.getArrayElementIndex = function(array,key,keyValue){
	if(typeof array.length!='undefined')
		for(var i = 0, len = array.length; i < len; i++){
			if(array[i][key]==keyValue)	return i;
		}
	else
		for(var i in array)
			if(typeof array[i]!='function')
				if(array[i][key]==keyValue)	return i;
	return -1;
};
/**
 * <i>consol</i> üzerinde <i>msg</i> parametresi ile aldığı mesajı yazdırır.
 * @example 
 mj.log('mesaj');
 * @name mj.log
 * @function
 * @type {Function}
 * @param {String} msg Konsolda yazdırılması istenen mesaj
 */
mj.log = function(msg){
	window.console && console.log && console.log(msg);
	window.air && window.air.Introspector && window.air.Introspector.Console.log(msg);
};

/**
 * <i>str</i> parametresi ile aldığı metin içinden <i>+</i> - <i><|p|></i> ve <i>%</i> - <i><|_|></i> değişimini yapar escape eder.
 * @name mj.escape
 * @function
 * @type {Function}
 * @param {String} str Escape edilecek metin
 * @return {String} Escape edilmiş string.
 */
mj.escape = function(str){
	str = str ? str : '';
	str = str.toString().replace(/\+/g,'<|p|>');
	str = str.toString().replace(/\%/g,'<|_|>');
	return encodeURIComponent ? encodeURIComponent(str) : escape(str);
};
/**
 * Ana renderer fonksiyonu
 * @name mj.renderer
 * @function
 * @type {Function}
 */
mj.renderer = function(){
	var trimRe = /^\s+|\s+$/g;
	return{
		bool : function(){
			return function(v){
				return mj.renderer.booleanRenderer(v);  
			};
		},
		/**
		 * <i>v</i> parametresi ile aldığı değeri <i>true</i> olması durumunda <i>Evet</i> değilse <i>Hayır</i> olarak değiştirir. 
		 * @name booleanRenderer
		 * @function
		 * @type {Function}
		 * @param {String} v Render edilecek değer
		 * @memberOf  mj.renderer
		 * @return {String} <i>Evet</i> veya <i>Hayır</i>.
		 */
		booleanRenderer : function(v){
			return v=='1' ? mj.lng.titles.buttons.yes : mj.lng.titles.buttons.no;
		},
		/**
		 * clearZero. 
		 * @name clearZero
		 * @function
		 * @type {Function}
		 * @param {Float} val Render edilecek değer
		 * @memberOf  mj.renderer
		 * @return {String} <i>Para birimi türünde değer</i> veya <i>0,00</i>.
		 */
		check : function(){
			return function(val){
				//val = parseInt(val);
				return '<center><div class="mj-checkbox '+(val&&!!val===true?'mj-checkbox-checked':'')+' '+(val==-1?'mj-item-disabled':'')+'" style="height:16px;float:none;"></div></center>';
			};
		},
		clearZero : function(val){
			if(val){
				val = val.toString().replace('.',',').split(',');
				if(!val[1]) return val;
				val[1] = val[1].replace('00000000','');
				for(var i=val[1].length;i>0;i--){if(val[1][i-1]!='0'){val[1]=val[1].substr(0,i);break;}}
				return val[0]+(val[1].length>0 ? ',' + val[1] : '');
			}else
				return '0,00';
		},
		color : function(val){
			return '<div style="background: '+val+' none repeat scroll 0%;"><center><img src="'+mj.glb.blankImage+'" style="height:20px;width:100%;" /></center></div>'
		},
		/**
		 * <i>v</i> parametresiyle aldığı değeri <i>format</i> parametresinde belirtilen formatta değeri formatlayacak geri döndürür. 
		 * @name dateRenderer
		 * @function
		 * @type {Function}
		 * @param {String} v Formatlanacak değer
		 * @param {String} format Tarih formatı tipi
		 * @memberOf  mj.renderer
		 * @return {String} Formatlanmış tarih stringi.
		 */
		dateRenderer : function(v, format){
			if(!v){
				return "";
			}
			if(!(v instanceof Date)){
				if (/^\d{8}$/.test(v)){
					v=new Date(Date.parse(v.substring(0,4)+'/'+v.substring(4,6)+'/'+v.substring(6,8)));
				//}else if(/^\d{4}\-\d{2}\-\d{2}\s{1}\d{2}\:\d{2}\:\d{2}/.test(v)){
				//	v = new Date(Date.parse(v.substring(5,7)+'/'+v.substring(8,10)+'/'+v.substring(0,4)));
				}else if(typeof v == 'number')
					v = new Date(v*1000);
				else if(typeof v == 'string' && new Date(parseInt(v)*1000) instanceof Date)
					v = new Date(parseInt(v)*1000);
				else
					v=new Date(Date.parse(v));
					//v=new Date(Date.parseDate(v,format));
			}
			return (isNaN(v.getDay()))?'&#160;':v.formatDate(format || "d/m/Y");
		},
		/**
		 * <i>format</i> parametresinde belirtilen formatta değeri formatlayacak fonksiyonu geri döndürür. 
		 * @name date
		 * @function
		 * @type {Function}
		 * @param {String} format Tarih formatı
		 * @memberOf  mj.renderer
		 * @return {Function} <i>dateRenderer</i> fonksiyonu.
		 */
		date : function(format){
			return function(v){
				return mj.renderer.dateRenderer(v, format);  
			};
		},
		right : function(val){
			return '<div style="text-align:right">'+val+'</div>';
		},
		timeRenderer : function(v, format){
			if(!v){
				return "";
			}
			if(!(v instanceof Date)){
				//if(/^\d{2}\-\d{2}\-\d{4}\s{1}\d{2}\:\d{2}\:\d{2}/.test(v)){
				//	v = new Date(Date.parse(v.substring(5,7)+'/'+v.substring(8,10)+'/'+v.substring(0,4)+' '+v.substring(11)));
				if(typeof v == 'number')
					v = new Date(v*1000);
				else{
					v=new Date(Date.parse(v));
					//v=new Date(Date.parseDate(v,format));
				}
			}
			return (isNaN(v.getDay()))?'&#160;':v.formatDate(format || "d/m/Y H:i:s");
		},
		time : function(format){
			return function(v){
				return mj.renderer.timeRenderer(v, format);  
			};
		}
	}
}();
mj.applyIf(Array.prototype, {
    /**
     * Gönderilen objenin dizi içinde olup olmadığını kontrol eder.
     * @param {Object} o Kontrol edilecek obje
     * @return {Number} o objesinin dizi içindeki indeks değeri (bulunamadığı durumda -1)
     */
    indexOf : function(o){
		for (var i = 0, len = this.length; i < len; i++){
			if(this[i] == o) return i;
		}
		return -1;
    },

    /**
     * Dizi içinden belirli bir objeyi siler.Silinmesi istenen objenin dizi içinde olmaması durumunda hiçbir şey yapmaz.
     * @param {Object} o Silinencek olan obje.
     * @return {Array} this Dizi
     */
    remove : function(o){
		var index = this.indexOf(o);
		if(index != -1){
			this.splice(index, 1);
		}
		return this;
    }
});
mj.apply(Array.prototype,{
	getIndex : function(key,keyValue){
		if(typeof this.length!='undefined')
			for(var i = 0, len = this.length; i < len; i++)
				if(this[i][key]==keyValue)	
					return i;
		else
			for(var i in this)
				if(typeof this[i]!='function')
					if(this[i][key]==keyValue)	
						return i;
		return -1;
	},
	extractKeyValues : function(key, arr, subArr, returnArr){
	    if(!arr)
	        arr = this;
	    if(!subArr)
	        subArr = 'data';
	    if(!returnArr)
	        returnArr = [];
	    for(var i=0,l=arr.length;i<l;i++){
	        returnArr.push(arr[i][key]);
	        if(arr[i][subArr])
	            this.extractKeyValues(key, arr[i][subArr], subArr, returnArr);
	    }
	    return returnArr;
	},
	filter : function(fn,quit,firstIndex){
		var tmp = [];
		if(typeof firstIndex == 'undefined' || firstIndex == -1)
			firstIndex = 0;
		for(var i = 0 + firstIndex ,len = this.length; i<len; i++){
			if(fn(this[i])){
				tmp.push(this[i]);
				if(quit)
					return tmp;
			}
		}
		return tmp;
	},
	_sort : function(key,dir,fn){
		var dKey = String(dir).toUpperCase() == "DESC" ? -1 : 1;
		fn = fn?fn:function(x,y){return x-y;};
		this.sort(function(x,y){
            var ret = fn(x[key], y[key]) * dKey;
            // if(ret == 0)
                // ret = (x.index < y.index ? -1 : 1);
            return ret;
        });
	}
});
/**
 * Ana init fonksiyonu. Http istekleri esnasında loader nesnesinin ekranda görünme ve gizlenme zamanları ile ilgili ayarların yapıldığı bölüm
 * @name mj.init
 * @function
 * @type {Function}
 */
mj.init = function(){
	$().ajaxStart(function(){
		if(!mj.dontShowLoader)
			mj.loaderShow();
	}).ajaxStop(function(){
		if(!mj.dontShowLoader)
			mj.loaderHide();
	});
};
mj.init();
/**
 * Parametre olarak gönderilen elementin parent elementleri içinde <i>mj-resize-handle</i> class'ına sahip ilk div elementini bulur, bu elemente
 <i>kkresize</i> eventi bind eder ve parametrelerde aldığı callback fonksiyonunu scope ile birlikte çalıştırır.
 * @name mj.bindResize
 * @function
 * @type {Function}
 * @param {HTMLElement} el Resize bind edilecek nesnenin içindeki container element.
 * @param {Function} cb resize esnasında çalıştırılacak fonksiyon.
 * @param {Object} scope Scope.
 */
mj.bindResize = function(el,cb,scope){
	var fp=$(el).parents('div.mj-resize-handle:first');
	if(fp.length==0)
		fp = mj.bd;
	var _el = $(el);
	if(fp && fp[0] && fp[0].tagName=='BODY')
		mj.onWindowResize(cb, scope);
	else{
		var listeners = fp.data('resizeListeners'), cbObj = {_el : _el, cb : cb};
		if(!listeners){
			listeners = [];
			fp.data('resizeListeners', listeners);
		}
		if(listeners.indexOf(cbObj)==-1){
			listeners.push(cbObj);
			fp.bind('kkresize', function(){
				var lc = {w:fp.width(), h:fp.height(), pv:fp.is(":visible"), ev:_el.is(":visible")};
				if(!(cbObj.lastCall) || (cbObj.lastCall && (cbObj.lastCall.w!=lc.w || cbObj.lastCall.h!=lc.h || cbObj.lastCall.pv!=lc.pv || cbObj.lastCall.ev!=lc.ev))){
					cbObj.lastCall = lc;
					cb.call(scope,arguments);
				}
			});
		}
	}
};
mj.onWindowResize = function(fn, scope){
	if(!mj._windowResizeListeners)
		mj._windowResizeListeners = [];
	mj._windowResizeListeners.push({fn:fn, scope:scope ? scope : this});
};
/**
 * Ana format objesi.Format dönüşümlerini yapacak olan fonksiyonları içerir.
 * @name mj.format
 */
mj.format = {	
	/**
	 * float2Money fonksiyonu
	 * @name mj.format.float2Money
	 * @function
	 * @type {Function}
	 * @param {Float} v Silinencek olan obje.
	 * @param {Integer} dp Ondalık sayısı(Ön tanımlı değeri : 2).
	 * @param {String} ds Ondalık ayracı.
	 * @param {String} gs Binlik ayracı.
	 */
	float2Money : function(v, dp,ds,gs){
		var num=v,decimalPrecision=!isNaN(dp) ? dp : 2;
		if(isNaN(num)){
			return '';
		}
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*Math.pow(10,decimalPrecision)+0.50000000001);
		cents = num%Math.pow(10,decimalPrecision);
		num = Math.floor(num/Math.pow(10,decimalPrecision)).toString();
		while(cents.toString().length<(Math.pow(10,decimalPrecision-1)).toString().length)
			cents = "0" + cents;
		for (var i = 0,val=Math.floor((num.length-(1+i))/3); i < val; i++)
			num = num.substring(0,num.length-(4*i+3))+gs+num.substring(num.length-(4*i+3));
		if(decimalPrecision > 0)
			return (((sign)?'':'-') + num + ds + cents);
		else
			return (((sign)?'':'-') + num);
	},
	d2h : function(d){return d.toString(16);},
	h2d : function(h){return parseInt(h,16);}
};
if($.browser.msie){
	try{
		document.execCommand("BackgroundImageCache",false,true);
	}catch(e){
	}
}
/**
 * getNumber fonksiyonu
 * @name mj.getNumber
 * @function
 * @type {Function}
 * @param {String} value
 * @return {Integer} Sayı değeri
 */
mj.getNumber = function(value){
	var x = value.split('0');
	if(x.length==2&&x[0]=='')
		return parseInt(x[1]);
	else 
		return parseInt(value);
};
/**
 * str2date fonksiyonu
 * @name mj.str2date
 * @function
 * @type {Function}
 * @param {String} value '12/02/2007' gibi bir tarih
 * @return {Date} Tarih değeri
 */
mj.str2date = function(value){
	var v = value.split('/');
	var time = new Date();
	time.setDate(mj.getNumber(v[0]));
	time.setMonth((mj.getNumber(v[1])-1));
	time.setFullYear(parseInt(v[2]));
	return time;
};
/**
 * date2str fonksiyonu
 * @name mj.date2str
 * @function
 * @type {Function}
 * @param {Date} date Date tipli değişken
 * @param {String} format Dönüştürülecek tarih formatı
 * @return {String} String olarak tarih değeri
 */
mj.date2str = function(date,format){
	return date.formatDate(format);
};
mj.oLength = function(obj){
	var l=0;
	for(var i in obj)
		if(typeof obj[i]!='function')
			l++;
	return l;
};
// mj.globalEvents = {};
// mj.addEvent = function(event){
	// var mjGlbE = mj.globalEvents;
	// if(!mjGlbE.events)
		// mjGlbE.events = {};
	// if(!mjGlbE.events[event])
		// mjGlbE.events[event] = {listeners : []};
// };
// mj.on = function(event, fn, scope){
		// mj.addEvent(event);
		// mj.globalEvents.events[event].listeners.push({fn:fn, scope:scope ? scope : mj});
// };
// mj.mon = function(event, fn){
	// var e = mj.globalEvents.events[event];
	// if(e && e.listeners){
		// var i = mj.getIndex(e.listeners, 'fn', fn);
		// if(i>-1)
			// e.listeners[i].fn = function(){};
	// }
	// return i>-1;
// };
// mj.trigger = function(){
	// var args = Array.prototype.slice.call(arguments, 0);
	// mj.addEvent(args[0]);
	// var e = mj.globalEvents.events[args[0]], l = e.listeners;
	// if(e && l){
		// return $(l).eachR(function(){
			// return this.fn.apply(this.scope, args.slice(1));
		// });
	// }
// };
// $(window).bind('beforeunload', function(){
	// return "ok1";
// });
// $(window).bind('beforeunload', function(){
	// return "ok2";
// });
mj.removeModified = function(obj){
	if(mj.modified && mj.modified.indexOf(obj)>-1)
		mj.modified.pop(obj);
};
mj.addModified = function(obj){
	if(!mj.modified)
		mj.modified = [];
	if(mj.modified.indexOf(obj)==-1)
		mj.modified.push(obj);
};
mj.translate = function(val){
	return (mj.lng.titles.modules&&mj.lng.titles.modules.general&&mj.lng.titles.modules.general[val])?mj.lng.titles.modules.general[val]:val;
};
// window.onbeforeunload = function() {
	// if(mj.modified && mj.modified.length>0)
		// return mj.lng.glb.exitQuestion;
// };
mj.newWindow = function(config){
	var cnf = mj.applyIf(config,{
		location : 0,
		status : 0,
		scrollbars : 1,
		resizable : 1,
		toolbar : 0,
		width : 800,
		height : 600
	});
	var win = window.open(cnf.url||"",cnf.id||mj.genId("mj-window"),"location="+cnf.location+",toolbar="+cnf.toolbar+",resizable="+cnf.resizable+",status="+cnf.status+",scrollbars="+cnf.scrollbars+",width="+cnf.width+",height="+cnf.height);
	if(cnf.title)
		win.document.title = cnf.title;
	return win;
};
mj.timeShow = function(time, msg, showSec){
	var str = '';
	var y = Math.floor(time/31536000000);
	time -= y*31536000000;
	var m = Math.floor(time/2592000000);
	time -= m*2592000000;
	var w = Math.floor(time/604800000);
	time -= w*604800000;
	var d = Math.floor(time/86400000);
	time -= d*86400000;
	var h = Math.floor(time/3600000);
	time -= h*3600000;
	var i = Math.floor(time/60000);
	time -= i*60000;
	var s=0;
	if(showSec){
		//s=Math.floor(time/1000);
		s=(time/1000);
		time-=s*1000;
	}
	if(y>0)
		str += y + (msg?" yıl ":":");
	if(m>0)
		str += m + (msg?" ay ":":");
	if(w>0)
		str += w + (msg?" hafta ":":");
	if(d>0)
		str += d + (msg?" gün ":":");
	if(h>0)
		str += h + (msg?" saat ":":");
	if(i>0)
		str += i + (msg?" dakika ":":");
	if(s>0)
		str+=s+(msg?" saniye":":");
	if(str[str.length-1]==':')
		str=str.substring(0,str.length-1);
	return str;
};

mj.timeShowHour = function(time, msg, showSec){
	var str = '';
	var y = Math.floor(time/31536000000);
	time -= y*31536000000;
	var m = Math.floor(time/2592000000);
	time -= m*2592000000;
	var w = Math.floor(time/604800000);
	time -= w*604800000;
	var d = Math.floor(time/86400000);
	time -= d*86400000;
	var h = Math.floor(time/3600000);
	time -= h*3600000;
	var i = Math.floor(time/60000);
	time -= i*60000;
	var s=0;
	if(showSec){
		s=Math.floor(time/1000);
		time-=s*1000;
	}
	// if(y>0)
		// str += y + (msg?" yıl ":":");
	// if(m>0)
		// str += m + (msg?" ay ":":");
	// if(w>0)
		// str += w + (msg?" hafta ":":");
	// if(d>0)
		// str += d + (msg?" gün ":":");
	h += 24*d + 168*w + 720*m + 8760*y;
	if(h>0)
		str += h + (msg?" saat ":":");
	if(i>0)
		str += i + (msg?" dakika ":":");
	if(s>0)
		str+=s+(msg?" saniye":":");
	if(str[str.length-1]==':')
		str=str.substring(0,str.length-1);
	return str;
};

mj.timeShowMinute = function(time){
	return Math.floor(time/60000);
};

mj.timeShowShort = function(time){
	var str = '', count = 2;
	var y = Math.floor(time/31536000000);
	time -= y*31536000000;
	var m = Math.floor(time/2592000000);
	time -= m*2592000000;
	var w = Math.floor(time/604800000);
	time -= w*604800000;
	var d = Math.floor(time/86400000);
	time -= d*86400000;
	var h = Math.floor(time/3600000);
	time -= h*3600000;
	var i = Math.floor(time/60000);
	time -= i*60000;
	var s=0;
	s=Math.floor(time/1000);
	time-=s*1000;
	if(y>0 && count-->0)
		str += y + " yıl ";
	if(m>0 && count-->0)
		str += m + " ay ";
	if(w>0 && count-->0)
		str += w + " hafta ";
	if(d>0 && count-->0)
		str += d + " gün ";
	if(h>0 && count-->0)
		str += h + " saat ";
	if(i>0 && count-->0)
		str += i + " dk ";
	if(s>0 && count-->0)
		str+=s+" sn";
	if(str[str.length-1]==':')
		str=str.substring(0,str.length-1);
	return str.trim();
};
mj.countDomEls = function(el){
    el = $(el ? el : mj.bd);
    for(var c=1,_c=el.children(),i=0,l=_c.length;i<l;i++)
        c += mj.countDomEls(_c[i]);
    return c;
};

mj.textReplacement = function(input, title){
	//mj.textReplacement($('#inputname')); / mj.textReplacement($('#inputname'), 'title');
	if(title && input.val()=='')
		input.val(title);
	var originalvalue = title || input.attr('title'), originalColor = input.css('color')||'#000'; 
	input.focus( function(){ 
		if( $.trim(input.val()) == originalvalue ){ input.val(''); input.css('color', originalColor);} 
	});
	input.blur( function(){
		if( $.trim(input.val()) == '' ){ input.val(originalvalue); input.css('color', '#565656');}
	});
};
mj.getDocHeight = function () {
	var D = document;
	return Math.max(
		Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
		Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
		Math.max(D.body.clientHeight, D.documentElement.clientHeight)
	);
};
//!!! myjui/source/base/string.prototype.js
String.prototype.ellipse = function(maxLength){
    if(this.length > maxLength){
        return this.substr(0, maxLength-3) + '...';
    }
    return this;
};

String.prototype.repeat = function(l){
	return new Array(l+1).join(this);
};

String.prototype.LTrim = function( ) {
	var re = /\s*((\S+\s*)*)/;
	return this.replace(re, "$1");
};

String.prototype.RTrim = function( ) {
	var re = /((\s*\S+)*)\s*/;
	return this.replace(re, "$1");
};

String.prototype.trim = function( ) {
	return this.LTrim().RTrim();
};

String.prototype.capitalize = function(){ //v1.0
    return this.replace(/\w+/g, function(a){
        return a.charAt(0).toUpperCase() + a.substr(1).toLowerCase();
    });
};
String.prototype.addPrefix = function(p,l){
	var val = p+this;
	var sl = val.length;
	return val.substring(sl-l,sl);
};
String.prototype.sanitize = function(){
	var tmp = this;
	var r=function(code, keys){
		if(!keys)keys='gi';
		return new RegExp(String.fromCharCode(code), keys);
	};
	var _f = [r(286),r(220),r(350),r(214),r(199),r(304),r(305, 'g'),r(32),r(45),/\W/g],_r = ['g','u','s','o','c','i','i','_','_', ''];
	for (var i=0,l=_f.length;i<l;i++)
	    tmp = tmp.replace(_f[i],_r[i]);
	return tmp.toLowerCase();
};
/*String.prototype.format = function(format){
	var str = this;
    for(var i=0,len=arguments.length;i<len;i++)
    {
        var re = new RegExp('\\{' + (i) + '\\}','gm');
        str = str.replace(re, arguments[i]);
    }
    return str;
};*/

mj.apply(String, {

    /**
     * Parametre olarak gönderilen string içinden ' ve \ karakterlerini temizler
     * @param {String} string Temizlenecek edilecek string
     * @return {String} Temizlenmiş edilmiş string
     * @static
     */
    escape : function(string) {
        return string.replace(/('|\\)/g, "\\$1");
    },

    leftPad : function (val, size, ch) {
        var result = new String(val);
        if(ch === null || ch === undefined || ch === '') {
            ch = " ";
        }
        while (result.length < size) {
            result = ch + result;
        }
        return result;
    },

    format : function(format){
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/\{(\d+)\}/g, function(m, i){
            return args[i];
        });
    }
});
//!!! myjui/source/base/date.prototype.js
// formatDate :
// a PHP date like function, for formatting date strings
// authored by Svend Tofte <www.svendtofte.com>
// the code is in the public domain
//
// see http://www.svendtofte.com/code/date_format/
// and http://www.php.net/date
//
// thanks to 
//  - Daniel Berlin <mail@daniel-berlin.de>,
//    major overhaul and improvements
//  - Matt Bannon,
//    correcting some stupid bugs in my days-in-the-months list!
//
// input : format string
// time : epoch time (seconds, and optional)
//
// if time is not passed, formatting is based on 
// the current "this" date object's set time.
//
// supported switches are
// a, A, B, c, d, D, F, g, G, h, H, i, I (uppercase i), j, l (lowecase L), 
// L, m, M, n, N, O, P, r, s, S, t, U, w, W, y, Y, z, Z
// 
// unsupported (as compared to date in PHP 5.1.3)
// T, e, o
$(function(){
	Date.strings = {
		daysLong : [mj.lng.glb.dayLSunday,mj.lng.glb.dayLMonday,mj.lng.glb.dayLTuesday,mj.lng.glb.dayLWednesday,mj.lng.glb.dayLThursday,mj.lng.glb.dayLFriday,mj.lng.glb.dayLSaturday],
		daysShort : [mj.lng.glb.daySSunday,mj.lng.glb.daySMonday,mj.lng.glb.daySTuesday,mj.lng.glb.daySWednesday,mj.lng.glb.daySThursday,mj.lng.glb.daySFriday,mj.lng.glb.daySSaturday],
		monthsShort : [mj.lng.glb.monthSJanuary, mj.lng.glb.monthSFebruary, mj.lng.glb.monthSMarch, mj.lng.glb.monthSApril, mj.lng.glb.monthSMay, mj.lng.glb.monthSJune, mj.lng.glb.monthSJuly, mj.lng.glb.monthSAugust, mj.lng.glb.monthSSeptember, mj.lng.glb.monthSOctober, mj.lng.glb.monthSNovember, mj.lng.glb.monthSDecember],
		monthsLong : [mj.lng.glb.monthLJanuary, mj.lng.glb.monthLFebruary, mj.lng.glb.monthLMarch, mj.lng.glb.monthLApril, mj.lng.glb.monthLMay, mj.lng.glb.monthLJune, mj.lng.glb.monthLJuly, mj.lng.glb.monthLAugust, mj.lng.glb.monthLSeptember, mj.lng.glb.monthLOctober, mj.lng.glb.monthLNovember, mj.lng.glb.monthLDecember]
	};
});
// Returns the ISO week of the date.
Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
    - 3 + (week1.getDay() + 6) % 7) / 7);
}
Date.prototype.formatDate = function (input,time) {
	
    var switches = { // switches object
        
        a : function () {
            // Lowercase Ante meridiem and Post meridiem
            return date.getHours() > 11? "pm" : "am";
        },
        
        A : function () {
            // Uppercase Ante meridiem and Post meridiem
            return (this.a().toUpperCase ());
        },
    
        B : function (){
            // Swatch internet time. code simply grabbed from ppk,
            // since I was feeling lazy:
            // http://www.xs4all.nl/~ppk/js/beat.html
            var off = (date.getTimezoneOffset() + 60)*60;
            var theSeconds = (date.getHours() * 3600) + 
                             (date.getMinutes() * 60) + 
                              date.getSeconds() + off;
            var beat = Math.floor(theSeconds/86.4);
            if (beat > 1000) beat -= 1000;
            if (beat < 0) beat += 1000;
            if ((String(beat)).length == 1) beat = "00"+beat;
            if ((String(beat)).length == 2) beat = "0"+beat;
            return beat;
        },
        
        c : function () {
            // ISO 8601 date (e.g.: "2004-02-12T15:19:21+00:00"), as per
            // http://www.cl.cam.ac.uk/~mgk25/iso-time.html
            return (this.Y() + "-" + this.m() + "-" + this.d() + "T" + 
                    this.h() + ":" + this.i() + ":" + this.s() + this.P());
        },
        
        d : function () {
            // Day of the month, 2 digits with leading zeros
            var j = String(this.j());
            return (j.length == 1 ? "0"+j : j);
        },
        
        D : function () {
		    return Date.strings.daysShort[self.getDay()];
        },
        
        F : function () {
            // A full textual representation of a month
            return Date.strings.monthsLong[date.getMonth()];
        },
        
        g : function () {
            // 12-hour format of an hour without leading zeros
            return date.getHours() > 12? date.getHours()-12 : date.getHours();
        },
        
        G : function () {
            // 24-hour format of an hour without leading zeros
            return date.getHours();
        },
        
        h : function () {
            // 12-hour format of an hour with leading zeros
            var g = String(this.g());
            return (g.length == 1 ? "0"+g : g);
        },
        
        H : function () {
            // 24-hour format of an hour with leading zeros
            var G = String(this.G());
            return (G.length == 1 ? "0"+G : G);
        },
        
        i : function () {
            // Minutes with leading zeros
            var min = String (date.getMinutes ());
            return (min.length == 1 ? "0" + min : min);
        },
        
        I : function () {
            // Whether or not the date is in daylight saving time (DST)
            // note that this has no bearing in actual DST mechanics,
            // and is just a pure guess. buyer beware.
            var noDST = new Date ("January 1 " + this.Y() + " 00:00:00");
            return (noDST.getTimezoneOffset () == 
                    date.getTimezoneOffset () ? 0 : 1);
        },
        
        j : function () {
            // Day of the month without leading zeros
            return date.getDate();
        },
        
        l : function () {
		    return Date.strings.daysLong[self.getDay()];
        },
        
        L : function () {
            // leap year or not. 1 if leap year, 0 if not.
            // the logic should match iso's 8601 standard.
            // http://www.uic.edu/depts/accc/software/isodates/leapyear.html
            var Y = this.Y();
            if (         
                (Y % 4 == 0 && Y % 100 != 0) ||
                (Y % 4 == 0 && Y % 100 == 0 && Y % 400 == 0)
                ) {
                return 1;
            } else {
                return 0;
            }
        },
        
        m : function () {
            // Numeric representation of a month, with leading zeros
            var n = String(this.n());
            return (n.length == 1 ? "0"+n : n);
        },
        
        M : function () {
            // A short textual representation of a month, three letters
            return Date.strings.monthsShort[date.getMonth()];
        },
        
        n : function () {
            // Numeric representation of a month, without leading zeros
            return date.getMonth()+1;
        },
        
        N : function () {
            // ISO-8601 numeric representation of the day of the week
            var w = this.w();
            return (w == 0 ? 7 : w);
        },
        
        O : function () {
            // Difference to Greenwich time (GMT) in hours
            var os = Math.abs(date.getTimezoneOffset());
            var h = String(Math.floor(os/60));
            var m = String(os%60);
            h.length == 1? h = "0"+h:1;
            m.length == 1? m = "0"+m:1;
            return date.getTimezoneOffset() < 0 ? "+"+h+m : "-"+h+m;
        },
        
        P : function () {
            // Difference to GMT, with colon between hours and minutes
            var O = this.O();
            return (O.substr(0, 3) + ":" + O.substr(3, 2));
        },      
        
        r : function () {
            // RFC 822 formatted date
            var r; // result
            //  Thu         ,     21               Dec              2000
            r = this.D() + ", " + this.d() + " " + this.M() + " " + this.Y() +
            //    16          :    01          :    07               0200
            " " + this.H() + ":" + this.i() + ":" + this.s() + " " + this.O();
            return r;
        },

        s : function () {
            // Seconds, with leading zeros
            var sec = String (date.getSeconds ());
            return (sec.length == 1 ? "0" + sec : sec);
        },        
        
        S : function () {
            // English ordinal suffix for the day of the month, 2 characters
            switch (date.getDate ()) {
                case  1: return ("st"); 
                case  2: return ("nd"); 
                case  3: return ("rd");
                case 21: return ("st"); 
                case 22: return ("nd"); 
                case 23: return ("rd");
                case 31: return ("st");
                default: return ("th");
            }
        },
        
        t : function () {
            // thanks to Matt Bannon for some much needed code-fixes here!
            var daysinmonths = [null,31,28,31,30,31,30,31,31,30,31,30,31];
            if (this.L()==1 && this.n()==2) return 29; // ~leap day
            return daysinmonths[this.n()];
        },
        
        U : function () {
            // Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
            return Math.round(date.getTime()/1000);
        },

        w : function () {
            // Numeric representation of the day of the week
            return date.getDay();
        },
        
        W : function () {
					/*
					// window.th=this;
					// https://weeknumber.net/how-to/javascript#:~:text=To%20get%20the%20ISO%20week,%2Ddigit%20year%2C%20use%20date%20.
					var date = new Date(this.U()*1000);
					date.setHours(0, 0, 0, 0);
					// Thursday in current week decides the year.
					date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
					// January 4 is always in week 1.
					var week1 = new Date(date.getFullYear(), 0, 4);
					// Adjust to Thursday in week 1 and count number of weeks from date to week1.
					return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
            - 3 + (week1.getDay() + 6) % 7) / 7);
					*/
					// Weeknumber, as per ISO specification:
					// http://www.cl.cam.ac.uk/~mgk25/iso-time.html
			
					var DoW = this.N ();
					var DoY = this.z ();

					// If the day is 3 days before New Year's Eve and is Thursday or earlier,
					// it's week 1 of next year.
					var daysToNY = 364 + this.L () - DoY;
					if (daysToNY <= 2 && DoW <= (3 - daysToNY)) {
							return 1;
					}

					// If the day is within 3 days after New Year's Eve and is Friday or later,
					// it belongs to the old year.
					if (DoY <= 2 && DoW >= 5) {
							return new Date (this.Y () - 1, 11, 31).formatDate ("W");
					}
					
					var nyDoW = new Date (this.Y (), 0, 1).getDay ();
					nyDoW = nyDoW != 0 ? nyDoW - 1 : 6;

					if (nyDoW <= 3) { // First day of the year is a Thursday or earlier
							return (1 + Math.floor ((DoY + nyDoW) / 7));
					} else {  // First day of the year is a Friday or later
							return (1 + Math.floor ((DoY - (7 - nyDoW)) / 7));
					}
        },
        
        y : function () {
            // A two-digit representation of a year
            var y = String(this.Y());
            return y.substring(y.length-2,y.length);
        },        
        
        Y : function () {
            // A full numeric representation of a year, 4 digits
    
            // we first check, if getFullYear is supported. if it
            // is, we just use that. ppks code is nice, but wont
            // work with dates outside 1900-2038, or something like that
            if (date.getFullYear) {
                var newDate = new Date("January 1 2001 00:00:00 +0000");
                var x = newDate .getFullYear();
                if (x == 2001) {              
                    // i trust the method now
                    return date.getFullYear();
                }
            }
            // else, do this:
            // codes thanks to ppk:
            // http://www.xs4all.nl/~ppk/js/introdate.html
            var x = date.getYear();
            var y = x % 100;
            y += (y < 38) ? 2000 : 1900;
            return y;
        },

        
        z : function () {
            // The day of the year, zero indexed! 0 through 366
            var t = new Date("January 1 " + this.Y() + " 00:00:00");
            var diff = date.getTime() - t.getTime();
            return Math.floor(diff/1000/60/60/24);
        },

        Z : function () {
            // Timezone offset in seconds
            return (date.getTimezoneOffset () * -60);
        }        
    
    }

    function getSwitch(str) {
        if (switches[str] != undefined) {
            return switches[str]();
        } else {
            return str;
        }
    }

    var date;
    if (time) {
        var date = new Date (time);
    } else {
        var date = this;
    }

    var formatString = input.split("");
    var i = 0;
    while (i < formatString.length) {
        if (formatString[i] == "\\") {
            // this is our way of allowing users to escape stuff
            formatString.splice(i,1);
        } else {
            formatString[i] = getSwitch(formatString[i]);
        }
        i++;
    }
    
    return formatString.join("");
}


// Some (not all) predefined format strings from PHP 5.1.1, which 
// offer standard date representations.
// See: http://www.php.net/manual/en/ref.datetime.php#datetime.constants
//

// Atom      "2005-08-15T15:52:01+00:00"
Date.DATE_ATOM    = "Y-m-d\\TH:i:sP";
// ISO-8601  "2005-08-15T15:52:01+0000"
Date.DATE_ISO8601 = "Y-m-d\\TH:i:sO";
// RFC 2822  "Mon, 15 Aug 2005 15:52:01 +0000"
Date.DATE_RFC2822 = "D, d M Y H:i:s O";
// W3C       "2005-08-15T15:52:01+00:00"
Date.DATE_W3C     = "Y-m-d\\TH:i:sP";
Date.firstDayOfWeek=1;
Date.prototype.getDayOld=Date.prototype.getDay;
Date.prototype.getDay=function(){
	return (this.getDayOld()-Date.firstDayOfWeek+7)%7;
};
Date.prototype.addDay=function(days){
	this.setTime(this.getTime()+86400000*days);
};
Date.prototype.getLastDay = function(date){
	var _tmp = new Date(date?date:this), dd = new Date(_tmp.getYear(),_tmp.getMonth(),0);
	return dd.getDate();
};

//!!! myjui/source/base/number.prototype.js
Number.prototype.toRadian = function(){
	return (Math.PI/180)*this;
};
Number.prototype.addPrefix = function(p,l){
	var val = this.toString();
	return val.addPrefix(p,l);
};
//!!! myjui/source/base/template.js
/** 
 * @fileoverview Template nesnesi.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.1 Beta 15.10.2007
 */
/**
 * Template nesnesi.
 * Örnek kullanımı aşağıdaki gibidir:
 * @example
	var a = eval('[{"title":"Normal Tab","html":"My content was added during construction.","iconCls":"tabs","closable":true,"id":"mj-tab-1"},{"title":"Ajax Tab 1","autoLoad":"ajax1.htm","closable":true,"iconCls":"tabs","refresh":false,"id":"mj-tab-2","loaded":false},{"title":"Ajax Tab 2","autoLoad":{"url":"ajax2.htm","params":"foo=bar&wtf=1"},"refresh":true,"closable":true,"id":"mj-tab-3","loaded":false},{"title":"Disabled Tab","disabled":true,"html":"Cant see me cause Im disabled","closable":true,"id":"mj-tab-4"}]');
	var tmp;
	var template = ['id:{id}', 'text:{title}', 'closable:{closable}'];
	var tt = new mj.template(template);
	for(b in a){
		tmp=a[b];
		if(typeof tmp!='function'){
			var h2=tt.apply(tmp);
			mj.log(h2);
		}
	}
 * @class
 * @name mj.template
 * @param {String/Array} html array ya da string olabilen html template'i.
 */
mj.template = function(html){
    if(html instanceof Array){
        html = html.join(" ");
    }
    this.html = html;
};
mj.template.prototype = {
	/**
	 * template stringinin replace'i için gerekli regularExpression.
	 * @name re
	 * @function
	 * @type {Function}
	 */
	re : /\{([\w-]+)(?:\:([\w\.]*)(?:\((.*?)?\))?)?\}/g,
	/**
	 * template stringini <i>obj</i> nesnesi içindeki değerlerle replace eden fonksiyon.
	 * @name apply
	 * @function
	 * @type {Function}
	 * @param {Object} obj template üzerinde değiştirilecek değişkenlerin değerlerini içeren obje.
	 * @return {String} derlenmiş html string.
	 */
	apply : function(obj){
		var html=this.html;
		var token=this.re.exec(html);
		while(token!=null){
			html=html.replace(token[0],obj[token[1]]);
			this.re.lastIndex = 0;
			token=this.re.exec(html);
		}
		return html;
	}
};
//!!! myjui/source/base/cookie.js
function setCookie( name, value, expires, path, domain, secure ) 
{
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );

	/*
	if the expires variable is set, make the correct 
	expires time, the current script below will set 
	it for x number of days, to make it for hours, 
	delete * 24, for minutes, delete * 60 * 24
	*/
	if ( expires )
	{
	expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );

	document.cookie = name + "=" +escape( value ) +
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
	( ( path ) ? ";path=" + path : "" ) + 
	( ( domain ) ? ";domain=" + domain : "" ) +
	( ( secure ) ? ";secure" : "" );
}
// this fixes an issue with the old method, ambiguous values 
// with this test document.cookie.indexOf( name + "=" );
function getCookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f
	
	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );
		
		
		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
	
		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found )
	{
		return null;
	}
}
function getCookieArray( check_name ) {
	var ret=[],cookies = document.cookie.split( ';' );
	for ( i = 0; i < cookies.length; i++ )
	{
		var cookie = cookies[i].split( '=' );
		if(cookie[0].indexOf(check_name)>-1)
			ret.push(cookie[0]);
	}
	return ret;
}		
// this deletes the cookie when called
function deleteCookie( name, path, domain ) {
	if ( getCookie( name ) ) document.cookie = name + "=" +
	( ( path ) ? ";path=" + path : "") +
	( ( domain ) ? ";domain=" + domain : "" ) +
	";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}
//!!! myjui/source/component/component.js
/** 
* @fileoverview Tüm bileşenlerin ortak kullandıkları özellikleri barındıran temel sınıf
* @hazırlayan kk
* @sürüm 0.1
*/

/**
* Tüm bileşenlerde kullanılacak en temel sınıf tanımlaması. Bileşenlerde sık kullanılacak özellikler bu sınıfa eklenmelidir.
* @class
* @name mj.component
* @param {String/Object} config String ya da Object tipinde olabilir. 
<i>config</i> parametresi ile gönderilen nesnenin tüm özellikleri, yeni oluşturulan nesneye atanır. 
<pre><code>
var newObject = new mj.component({width:100, height:50});
</pre></code>
Üstteki şekilde oluşturulan newObject nesnesinin width ve height özellikleri mevcuttur. 
String olarak verildiğinde <a href="#defaultConfigStr">defaultConfigStr</a> ile belirtilmiş olan alana ataması yapılır.
*/
mj.component = function(config){
	if(typeof config=='string'){
		var c = {};
		c[this.defaultConfigStr] = config;
		config = c;
	}
	if(config && config.renderTo)
		config.renderTo = $(config.renderTo);
	if(config && config.store && !config.store.componentClass)
		config.store = new mj.store({data:config.store});
	mj.apply(this, config);
	this.config=config;
	this.on('afterdestroy', this._afterDestroy, this);
	this.init();
};
mj.component.prototype = {
	/**
	 * Ön tanımlı bileşen sınıfı bilgisi. Bileşen tanımlaması ile ataması yapılır ve çalışma esnasında değiştirilmemelidir. 
	 Fonksiyonlara parametre olarak gönderilen nesnelerin hangi sınıftan türetildiğinin kolay bir şekilde anlaşılabilmesi için eklenmiştir. 
	 Örneğin bu sınıftan oluşturulan nesnelerin <i>componentClass</i> alanında 'mj.component' değeri vardır.
	 * @name componentClass
	 * @type {String}
	 * @memberOf  mj.component
	 */
	componentClass : 'mj.component',
	/**
	 * Config parametresinin string tipinde olması durumunda kullanılacak olan ön tanımlı değer. 
	 Standart olarak 'renderTo' değeri atanmıştır. Bu durumda alttaki iki tanımlama da aynı tipte nesneler oluşturmaktadır.
<pre><code>
var newObject1 = new mj.component(document.body);
var newObject2 = new mj.component({renderTo : document.body});
</pre></code>
	 * @name defaultConfigStr
	 * @type {String}
	 * @memberOf  mj.component
	 */
	defaultConfigStr : 'renderTo',
	/**
	 * Nesnenin hazır olup olmadığını gösterir. <a href="#init">bkz : init()</a>
	 * @name ready
	 * @type {boolean}
	 * @memberOf  mj.component
	 */
	ready : false,
	/**
	 * Bir nesne oluşturulduktan sonra, o nesnenin görsel içeriğinin oluşturulmasının gerçekleştirileceği fonksiyondur. 
	 İşlem tamamlandıktan sonra <i>init</i> olayı tetiklenir.
	 * @name init
	 * @function
	 * @type {Function}
	 * @memberOf  mj.component
	 */
	init : function(){
		this.ready = true;
		this.trigger('init', this);
	},
	addEvent : function(event){
		if(!this.events)
            this.events = {};
		if(!this.events[event])
			this.events[event] = {listeners : []};
	},
	/**
	 * Nesnenin bir olayına(event) dinleyici fonksiyon atamak için kullanılır. Olay yönetiminde nesnelerde dinlenecek olayın tanımlı olma şartı aranmaz, 
	 gerekli atama yapılır, <i><a href="#trigger">trigger</a></i> metoduyla tetiklenen olaylar sonrasında dinleyiciler çalıştırılır.
	 * @name on
	 * @function
	 * @type {Function}
	 * @memberOf  mj.component
	 * @param {String} event Dinlenecek olan olayın adı
	 * @param {Handle} fn Olay tetiklendiğinde çağırılacak olan fonksiyon
	 * @param {Scope} [scope] Çağırılan fonksiyonda kullanılacak olan referans nesne(this).
	 * @memberOf  mj.component
	 */
	on : function(event, fn, scope){
		this.addEvent(event);
		this.events[event].listeners.push({fn:fn, scope:scope ? scope : this});
	},
	/**
	 * Nesnenin olayını dinleyen bir fonksiyonun bağlantısının koparılması için kullanılır.
	 * @name mon
	 * @function
	 * @type {Function}
	 * @param {String} event Dinlenen olayın adı
	 * @param {Handle} fn Bağlantısı silinecek olan fonksiyon
	 * @return {Boolean} Dinleyici fonksiyonun bulunup bulunamadığı bilgisi
	 * @memberOf  mj.component
	 */
	mon : function(event, fn){
		var e = this.events[event];
		if(e && e.listeners){
			var i = mj.getIndex(e.listeners, 'fn', fn);
			if(i>-1)
				e.listeners[i].fn = function(){};
		}
		return i>-1;
	},
	onOnce : function(event, fn, scope){
		this.mon(event, fn);
		this.on(event, fn, scope);
	},
	/**
	 * Nesnenin bir olayını(event) tetiklemek için kullanılır. İlk parametre tetiklenecek olayın adı, sonraki parametreler ise opsiyonel olmak üzere
	 kullanıcıya bırakılmıştır. Bu parametreler dinleyici fonksiyonlara gönderilecek olan değerlerdir. Dinleyicilerden <i>false</i> gönderen olması
	 durumunda sonraki dinleyiciler tetiklenmez.
	* @example
var newObject = new mj.component();
var listenerFunction = function(name){
	alert(name);
};
newObject.on('namechange', listenerFunction);

newObject.name = 'New Name';
newObject.trigger('namechange', newObject.name);
	 * @name trigger
	 * @function
	 * @type {Function}
	 * @param {String} event Tetiklenecek olan olayın adı
	 * @return {Boolean} Dinleyici fonksiyonların tümünün çalıştırılması durumunda <i>true</i>, aksi durumda <i>false</i>
	 * @memberOf  mj.component
	 */
	trigger : function(){//parametreler : (, paramA, paramB, ...)
		var args = Array.prototype.slice.call(arguments, 0);
		//console.log(this.componentClass+'.'+args[0]);
		this.addEvent(args[0]);
		var e = this.events[args[0]], l = e.listeners;
		if(e && l){
			return $(l).eachR(function(){
				return this.fn.apply(this.scope, args.slice(1));
			});
		}
	},
	_afterDestroy : function(){
		for(var x in this)
			this[x] = null;
	},
	addRelated : function(el){
		if(!this.relatedItems)
			this.relatedItems = [];
		this.relatedItems.push(el);
	},
	destroy : function(){
		var t = this,_i;
		t.trigger('beforedestroy', t);
		t.destroyEl = t.destroyEl ? t.destroyEl : (t.renderTo ? t.renderTo : t.el);
		if(t.destroyEl){
			if(typeof t.destroyEl.remove != 'function')
				t.destroyEl = $(t.destroyEl);
			t.destroyEl.remove();
		}
		if(t.relatedItems)
			for(var i=0,l=t.relatedItems.length;i<l;i++)
				if(_i = t.relatedItems[i])
					(_i.componentClass && typeof _i.destroy == 'function') ? _i.destroy() : (typeof _i.remove == 'function' ? _i : $(_i)).remove();
		t.trigger('afterdestroy', t);
	}
};
//!!! myjui/source/data/store.js
/** 
* @fileoverview Tüm bileşenlerin ortak kullandıkları store sınıfı.
* @hazırlayan kk
* @sürüm 0.1
*/
/**
* Veri kaynağı olarak kullanılacak olan sınıf. Sunucudan gelen JSON ifadesini işleyerek <i>data</i> özelliğine yazar.
* @class
* @name mj.store
* @extends mj.component
* @param {String/Object} config Sunucudan veriyi alabilmek için gerekli olan parametreleri içerir. 
* String olarak verilmesi durumunda gelen değeri <i>url</i> olarak değerlendirir.
* @config {String} url İstek yapılacak olan sayfanın adresi
* @config {Object} params İstek yapılacak olan sayfaya gönderilecek parametreler. Gönderim metodu olarak POST kullanılır
*/
mj.store = function(config){
	if(typeof config !== 'undefined' && typeof config.data !== 'undefined' && config.data)
		this.preloaded = true;
	mj.store.superclass.constructor.call(this, config);
};
mj.store.prototype = {
	/**
	 * Ön tanımlı bileşen sınıfı bilgisi. Bileşen tanımlaması ile ataması yapılır ve çalışma esnasında değiştirilmemelidir. 
	 Fonksiyonlara parametre olarak gönderilen nesnelerin hangi sınıftan türetildiğinin kolay bir şekilde anlaşılabilmesi için eklenmiştir. 
	 Örneğin bu sınıftan oluşturulan nesnelerin <i>componentClass</i> alanında 'mj.store' değeri vardır.
	 * @name componentClass
	 * @type {String}
	 * @memberOf  mj.store
	 */
	componentClass : 'mj.store',
	defaultConfigStr : 'url',
	showLoader : true,
	method: 'POST',
	localSort : true,
	params:null,
	/**
	 * Sunucudan gerekli isteği yaparak gelen sonucu <i>data</i> özelliğine yazar. 
	 * İşlem tamamlandığında <i>load</i> olayı tetiklenir.
	 * @name load
	 * @function
	 * @type {Function}
	 * @memberOf  mj.store
	 */
	load : function(){
		var t = this;
		//t.params = mj.applyIf(t.params,{query:true});
		var success=typeof t.success==='function'?t.success:t.callback;
		var failure=typeof t.failure==='function'?t.failure:function(){};
		t.trigger('beforeload', t);
		var render = function(jsonData,cmRenderers){
			for(var d in jsonData){
				var data=jsonData[d];
				if(typeof data[d]!=='function')
					for(var r in cmRenderers){
						var renderer=cmRenderers[r];
						//if(r!='toJSONString')
						if(typeof data[r] !== 'undefined'){
							data[r+'_source'] = data[r];
							if(typeof renderer === 'function')
								data[r]=renderer(data[r]);
							else
								data[r]=renderer;
						}else{
							data[r]='';
							data[r+'_source']='';
						}
					}
					if(typeof data.data!=='undefined')
						render(data.data,cmRenderers);
			}
		};
		if(t.url&& !t.preloaded){
			//t.params.jsoncallback='?';
			//t.params.isStore='true';
			$.ajax({
				url: t.url,
				method: t.method,
				data: t.params,
				success: function(data, textStatus, jqXHR){
						//if(t.entity != "") {
						//    t.dataset = data;
						//    t.data = data[t.entity].records;
						//} else {
						//    t.data = data;
					//}
					if(t.params&&t.params.isStore){
						eval('data='+data);
					}
					if(typeof data.totalProperty!=='undefined'&&typeof data.records!=='undefined'){
						t.data = data.records;
						t.recordCount = data.totalProperty;
					}else{
						t.data = data[t.modulename].records;
						t.recordCount = data[t.modulename].totalProperty;
					}
					if (success)
							success.call(t.scope || t, jqXHR, arguments);
					t.trigger('load', t, jqXHR);
					t.loaded = true;
				},
				error: function(jqXHR, textStatus, errorThrown){
					t.trigger('error', t, jqXHR);
					failure.call(t.scope || t, jqXHR, arguments);
					mj.message({
						title: mj.lng.glb.error,
						msg: jqXHR.responseJSON.message + "<br>" + jqXHR.responseJSON.file,
						type: "error",
						html: true
					});
				},
				complete: function(jqXHR, textStatus){

				}
			});
			/*
			$.post(t.url,t.params,function(json){
				t._json = json;
				if(json && (typeof json.totalProperty != 'undefined' || typeof json.success != 'undefined')){
					t.recordCount = json.totalProperty;
					if(t.renderers)
						render(json.records,t.renderers);
					if(typeof json.records != 'undefined'){
						t.data = json.records;
						t.dm = [];
						if(t.data.length>0){
							for(var cl in t.data[0])
								if(typeof t.data[0][cl]!='function') 
									t.dm.push(cl);
						}
					}
					if(typeof json.detail != 'undefined'){
						t.detail = [];
						for(var i in json.detail){
							if(typeof json.detail[i] != 'function'){
								var cItem = json.detail[i];
								cItem.dm = [];
								if(cItem.records.length>0){
									for(var cl in cItem.records[0])
										if(typeof cItem.records[0][cl]!='function') 
											cItem.dm.push(cl);
								}
								cItem.data = cItem.records;
								delete cItem.records;
								t.detail.push(json.detail[i]);
							}
						}
					}
					if(json.error&&$('.mj-error-message').length==0)
						new mj.message({
							title : 'Bilgi',
							msg : json.msg,
							cls : 'mj-error-message'
						}); 
					t.trigger('load', t, json);
					t.loaded = true;
					if(success)
						success.call(t.scope||t,json,arguments);
				}else if(failure){
					failure.call(t.scope||t, json, arguments);
				}
			}, 'json');
			//});
			*/
		}else{
			delete t.preloaded;
			if(t.showLoader===true)
				mj.loaderShow();
			if(t.modulename){
				t.recordCount = t.data[t.modulename].totalProperty;
				t.data=t.data[t.modulename].records;
			}else{
				t.recordCount = t.data?(t.data.length ? t.data.length : 1):0;
				//t.data=t.data[0];//pager limit combo sorun oluyor
			}
			if(t.renderers)
				render(t.data, t.renderers);
			t.trigger('load', t);
			t.loaded = true;
			if(t.showLoader===true)
				mj.loaderHide();
		}
	},
	/**
	 * Store nesnesinin verisinin gruplanmasında kullanılır
	 * @name groupItemsData
	 * @function
	 * @type {Function}
	 * @memberOf  mj.store
	 * @param {String} dataIndex Gruplanacak olan alan bilgisi
	 * @return {Object} Gruplanmış veriyi içeren bir nesne
	 */
	groupItemsData : function(dataIndex){
		var x = {};
		var item;
		for(var i=0,len=mj.oLength(this.data);i<len;i++){
			item = this.data[i][dataIndex];
			if(!x[item]){
				x[item]=[];
			}	
			x[item].push(this.data[i]);
		}
		return x;
	},
	/**
	 * Gruplamanın yapılacağı alanı alır ve veriyi groupItemsData fonksiyonu ile gruplayıp <i>gData</i>ya yazar.
	 * @name groupBy
	 * @function
	 * @type {Function}
	 * @memberOf  mj.store
	 * @param {String} dataIndex Gruplanacak olan alan bilgisi
	 */
	groupBy : function(dataIndex){
		this.gData = this.gData||{};
		this.gData[dataIndex] = this.groupItemsData(dataIndex);
	},
	/**
	 * Gruplamanın yapılacağı alanı veriyi gruplayarak birbirinden farklı olan elemanları bir diziye yazar.
	 * @name groupItems
	 * @function
	 * @type {Function}
	 * @memberOf  mj.store
	 * @param {String} dataIndex Gruplanacak olan alan bilgisi
	 * @return {Array} gruplanmış veriyi içeren dizi
	 */
	groupItems : function(dataIndex){
		var x = [];
		var item;
		for(var i=0,len=mj.oLength(this.data);i<len;i++){
			item = this.data[i][dataIndex];
			if(x.indexOf(item)===-1)
				x.push(item);
		}
		return x;
	},
	/**
	 * Parametre olarak gönderilen <i>dataIndex</i>'e sahip alanların değerlerini tüm node'lardan bulup <i>this.collected</i>'a yazar.
	 * @name collect
	 * @function
	 * @type {Function}
	 * @param {Object} jsonData Node bilgisini içeren JSON objesi
	 * @param {String} dataIndex Bulunacak dataIndex
	 * @memberOf  mj.store
	 */
	collect : function(jsonData, dataIndex){
		if(!this.collected)
			this.collected = [];
		for(var d in jsonData){
			var data=jsonData[d];
			if(typeof data[d]!=='function'){
				if(typeof data[dataIndex]!=='undefined')
					this.collected.push(data[dataIndex]);
				if(typeof data.data!=='undefined')
					this.collect(data.data,dataIndex);
			}
		}
	},
	/**
	 * Parametre olarak gönderilen <i>dataIndex</i> verilerinden <i>val</i> ile benzerlik gösterenleri data'ya yazar,
	 orjinal data ise oData olarak saklanır.
	 * @name filter
	 * @function
	 * @type {Function}
	 * @param {Object} dataIndex Node bilgisini içeren JSON objesi
	 * @param {String} val Bulunacak dataIndex
	 * @memberOf  mj.store
	 */
	filter : function(dataIndex, val, dontrefresh, contains){
		this._isFiltering = true;
		var c=0;
		if(!this.oData){
			this.oData = [];
			for(var i in this.data)
				if(typeof this.data[i]!=='function')
					this.oData.push(this.data[i]);
		}
		if(!dontrefresh)
			this.clearFilter();
		var filterData = this.data;
		this.data = [];
		for(var j in filterData)
			if(typeof filterData[j]!=='function'&&typeof filterData[j][dataIndex]!='undefined')
				if(filterData[j][dataIndex] instanceof Date){
					if(!val instanceof Date){
						var g=1,a=1,y=1980,md=new Date();
						md.setDate(g);
						md.setMonth(a);
						md.setYear(y);
						val = val.split('/');
						if(val[0])
							md.setDate(val[0]);
						if(val[1])
							md.setMonth(val[1]);
						if(val[2])
							md.setYear(val[2]);
						val=md;
					}
					if(filterData[j][dataIndex]>=val){
						this.data.push(filterData[j]);
						c++;
					}
				}else if(contains && filterData[j][dataIndex].toString().toLowerCase().indexOf(val.toString())>-1){
					this.data.push(filterData[j]);
					c++;
				}else
					if(filterData[j][dataIndex].toString().toLowerCase().substring(0,val.toString().length)===val.toString().toLowerCase()){
						this.data.push(filterData[j]);
						c++;
					}
		this.recordCount=c;
		this.trigger('beforeload', this);
		//this.trigger('load', this);
		this._isFiltering = false;
		return this;
	},
	clearFilter : function(){
		if(this.oData)
			this.data = this.oData;
	},
	sort : function(key,dir){
		dir = dir || 'ASC';
		if(this.data instanceof Array){
			var fn = function(r1, r2){
				r1=String(r1).toLowerCase();
				r2=String(r2).toLowerCase();
				return r1 > r2 ? 1 : (r1 < r2 ? -1 : 0);
			};
			this.data._sort(key,dir,fn);
		}else
			return false;
	},
	find: function(dataIndex, val, childNode) {
        for(var i in this.data){
            if(this.data[i][dataIndex] == val)
                return this.data[i];
            if(childNode && this.data[i][childNode])
                for(var j in this.data[i][childNode]){
                    if(this.data[i][childNode][j][dataIndex] == val)
                        return this.data[i][childNode][j];
                }
        }
        return null;
    },
	init : function(){
		var t = this;
		t.on('load', function(){
			t.isEmpty = !t.data || t.data.length === 0;
		});
		mj.store.superclass.init.call(this);
	}
};
mj.extend(mj.store, mj.component);
//!!! myjui/source/data/pager.js
/** 
* @fileoverview Verinin sayfalara bölünerek listelenebilmesini sağlayan sınıf.
* @hazırlayan kk
* @sürüm 0.1
*/
/**
* Verinin sayfalar halinde sunucudan alınabilmesini sağlar.
* @class
* @name mj.pager
* @extends mj.component
* @param {Object} config Sunucudan veriyi sayfalar halinde alabilmek için gerekli olan parametreleri içerir. 
* @config {String} pos Page bar nesnesinin container içindeki konumunu belirtir.'bottom'/'top' olabilir.(Ön tanımlı değeri : 'bottom')
* @config {Integer} start Verinin kaçıncı sayfadan itibaren alınacağın belirtir.
* @config {Integer} limit Sayfa başına listelenecek kayıt sayısını belirtir.
* @config {Boolean} pageInfo Page Bar'ın sağ tarafında toplam kayıt sayısı ve gösterilen kayıtlar ile ilgili bilgi alanının gösterim durumunu 
belirtir.False olması halinde bu alan gösterilmeyecektir.(Ön tanımlı değeri : true)
*/
mj.pager = function(config){
	config = config || {};
	this.params = {start : 0, current:1, pos:'bottom'};
	if(!config.beforePageText){
		config.beforePageText=mj.lng.objects.pager.beforePageText;
	}
	if(config.limit < 1)
		config.limit = 10;
	if(!config.start||config.start < 0)
		config.start = 0;
	mj.apply(this.params,config);
	mj.pager.superclass.constructor.call(this, this.params);
};
mj.pager.prototype = {	
	/**
	 * Ön tanımlı bileşen sınıfı bilgisi. Bileşen tanımlaması ile ataması yapılır ve çalışma esnasında değiştirilmemelidir. 
	 Fonksiyonlara parametre olarak gönderilen nesnelerin hangi sınıftan türetildiğinin kolay bir şekilde anlaşılabilmesi için eklenmiştir. 
	 Örneğin bu sınıftan oluşturulan nesnelerin <i>componentClass</i> alanında 'mj.pager' değeri vardır.
	 * @name componentClass
	 * @type {String}
	 * @memberOf  mj.pager
	 */
	componentClass : 'mj.pager',
	pos : 'bottom',
	afterPageText : '{0}',
	displayMsg : "{0} - {1} / {2} ",
	defaultButtons : true,
	elements : {
		first : true,
		prev : true,
		next : true,
		last : true,
		refresh : true,
		pages : true,
		selectLimit : true
	},
	pageInfo : true,
	/**
	 * Verinin bulunduğu sayfa için tekrar yüklenmesini sağlar.
	 * @name refreshPageBar
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 */
	refreshPageBar : function(){
		var tb = this.tbar;
		if(this.defaultButtons&&this.elements.pages){
			var pc = this.getPageCount();
			var pn = pc>=this.params.current?this.params.current:pc;
			tb.input.val(pn);
			this.params.current = pn>0?pn:1;
			this.params.start = (this.params.current-1) * this.params.limit;
			tb.pageTotal.text('/ '+pc);
		}
		if(this.pageInfo)
			tb.pageInfo.text(String.format(this.displayMsg, this.params.start+1, this.params.start+this.store.data.length, this.store.recordCount));
	},
	/**
	 * Page Bar nesnesinin oluşturulmasını sağlar.
	 * @name render
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 * @param {Object} config Sunucudan veriyi sayfalar halinde alabilmek için gerekli olan parametreleri içerir. 
	 * @config {String} pos Page bar nesnesinin container içindeki konumunu belirtir.'bottom'/'top' olabilir.(Ön tanımlı değeri : 'bottom')
	 * @config {Integer} start Verinin kaçıncı sayfadan itibaren alınacağın belirtir.
	 * @config {Integer} limit Sayfa başına listelenecek kayıt sayısını belirtir.
	 * @config {Boolean} pageInfo Page Bar'ın sağ tarafında toplam kayıt sayısı ve gösterilen kayıtlar ile ilgili bilgi alanının gösterim durumunu 
	 belirtir.False olması halinde bu alan gösterilmeyecektir.(Ön tanımlı değeri : true)
	 */
	render : function(config){
		var cs = config.scope, cst = cs.store, pb = cs.pbar, f=false;
		mj.apply(cst.params,pb.params);
		pb.store = cst;
		if(pb.pos&&pb.pos=='top'){
			cs.pageBar = mj.NE(cs.cnt,{tag:'div', cls:'mj-paging'});
			cs.renderTo = mj.NE(cs.cnt,{tag:'div',style:'overflow:auto;height:'+(cs.cnt.height()-25)+'px;'});
		}else{
			cs.renderTo = mj.NE(cs.cnt,{tag:'div',style:'overflow:auto;height:'+((cs.cnt.height()||parseInt(cs.cnt[0].style.height))-25)+'px;'});
			cs.pageBar = mj.NE(cs.cnt,{tag:'div', cls:'mj-paging'});
		}
		pb.renderTo = cs.pageBar;			
		this.tbar ={};
		var t=this, tb = t.tbar, els = t.elements;
		tb.items = [];
		if(t.defaultButtons){
			if(els.first||els.prev||els.next||els.last)
				f=true;
			if(els.first)
				tb.items.push({id:'btnfirst' ,iconCls:'mj-first',alt:mj.lng.glb.first,handler:function(){t.trigger('sourcefirst',t);t.trigger('sourceload',t);t.first();}});
			if(els.prev)
				tb.items.push({id:'btnprev' ,iconCls:'mj-previous',alt:mj.lng.glb.previous,handler:function(){t.trigger('sourceprev',t);t.trigger('sourceload',t);t.prev();}});
			if(els.pages){
				tb.items.push('|');
				tb.items.push({id:'pageTotal'+this.id,html:'<div><span style="float:left;padding:5px;" unselectable="on" class="mj-page-before-text mj-unselectable">'+this.beforePageText+'</span><input type="text" size="1" value="1" class="mj-page-info" style="float:left;"></input><span style="float:left;padding:5px;" unselectable="on" class="mj-page-before-text mj-unselectable">/ '+String.format(this.afterPageText, 1)+'</span></div>'});
				tb.items.push('|');
			}
			if(els.next)
				tb.items.push({id:'btnnext' ,iconCls:'mj-next',alt:mj.lng.glb.next,handler:function(){t.trigger('sourcenext',t);t.trigger('sourceload',t);t.next();}});
			if(els.last)
				tb.items.push({id:'btnlast' ,iconCls:'mj-last',alt:mj.lng.glb.last,handler:function(){t.trigger('sourcelast',t);t.trigger('sourceload',t);t.last();}});
		}
		if(els.selectLimit){
			if(f)
				tb.items.push('|');	
			tb.items.push({id:'cmblimit', iconCls:'',alt:mj.lng.glb.recordLimit});
		}
		if(els.refresh){
			if(f)
				tb.items.push('|');	
			tb.items.push({id:'btnrefresh' ,iconCls:'mj-refresh', alt:mj.lng.glb.refresh, handler:function(){t.trigger('sourcerefresh',t);t.trigger('sourceload',t);t.refresh();}});
		}
		if(this.pageInfo)
			tb.items.push({id:'pageInfo'+this.id,html:'<div style="float:right;padding-bottom:5px;padding-top:5px;text-align:right;margin-right:5px;" unselectable="on" class="mj-page-before-text mj-unselectable"></div>'});
		tb.renderTo=this.renderTo;
		t.tbar = new mj.menu(tb);
		if(els.selectLimit){
			var elId = mj.getIndex(t.tbar.buttons,'id','cmblimit');
			if(elId>-1){
				var cmbCnt = t.tbar.buttons[elId]._el;
				cmbCnt.empty();
				var a=[];
				a.push({id:10,code:'10'});
				a.push({id:25,code:'25'});
				a.push({id:50,code:'50'});
				a.push({id:100,code:'100'});
				a.push({id:250,code:'250'});
				a.push({id:500,code:'500'});
				t.cmbItem = new mj.form.combo({
					renderTo : cmbCnt,
					title : '',
					value : t.params.limit,
					dataIndex : 'cmblimit',
					readOnly : true,
					store : a,
					width : 50,
					itemStyle  : 'padding-top:0px'
				});
				t.addRelated(t.cmbItem);
				t.cmbItem.view.on('itemclick',function(){
					var val = this.selected[0].id;
					t.params.limit = val;
					t.store.params.limit = val;
					var pc = t.getPageCount();
					t.params.current = t.params.current<=pc?t.params.current:pc;
					t.go2page(t.params.current);
				});
			}
		}
		t.tbar.input = t.tbar._el.find('input.mj-page-info');
		t.tbar.input.bind('change',function(){
			t.trigger('sourcefind',t);
			t.trigger('sourceload',t);
			t.go2page(this.value);
		});
		t.tbar.pageTotal = $(t.tbar._el.find('span')[1]);
		t.tbar.pageInfo = t.tbar._el.find('div:last');
		t.buttons = t.tbar.buttons;
		t.store.on('load',t.refreshPageBar,t);
	},
	/**
	 * Toplam kayıt sayısını sayfada gösterilecek kayıt sayısına göre hesaplayarak görüntülenecek sayfa sayısını bulur.
	 * @name getPageCount
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 * @return {Integer} Sayfa sayısı
	 */
	getPageCount : function(){
		return Math.ceil(this.store.recordCount/this.params.limit);
	},
	/**
	 * İstenilen sayfanın görüntülenmesini sağlar. Sayfa bilgisi toplam sayfa sayısından büyük olduğu durumda son sayfayı, 
	1'den küçük olması durumunda ise ilk sayfayı görüntüler.	 
	 * @name go2page
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 * @param {Integer} pageNumber Gösterilecek sayfanın numarası
	 */
	go2page : function(pageNumber){
		var pc = this.getPageCount();
		pageNumber = isNaN(parseInt(pageNumber))?1:pageNumber;
		if(pageNumber < 1)
			pageNumber = 1;
		if(pageNumber > pc)
			pageNumber = pc;
		this.params.current = pageNumber;
		this.params.start = pageNumber>0?(pageNumber-1) * this.params.limit:0;
		//mj.apply(this.store.params,this.params);
		var routeName = this.store.routename?this.store.routename:this.store.modulename + '-showall';
        var route = Routing.getRoute(routeName);
        var data = {
            'pg': this.params.current,
            'lm': this.params.limit
		};
		if(this.store.filters){
			data.filters=this.store.filters;
		}
		var url = Routing.generate(routeName, data);
		//console.log(url);
		this.store.url=url;
		this.store.load();
	},
	/**
	 * İlk sayfayı görüntüler.	 
	 * @name first
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 */
	first : function(){
		if(this.params.current > 1)
			this.go2page(1);
	},
	/**
	 * Önceki sayfayı görüntüler.	 
	 * @name prev
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 */
	prev : function(){
		if(this.params.current > 1)
			this.go2page(--this.params.current);
	},
	/**
	 * Sonraki sayfayı görüntüler.	 
	 * @name next
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 */
	next : function(){
		var pc = this.getPageCount();
		if(pc > this.params.current)
			this.go2page(++this.params.current);
	},
	/**
	 * Son sayfayı görüntüler.	 
	 * @name last
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 */
	last : function(){
		var pc = this.getPageCount();
		if(pc > this.params.current)
			this.go2page(pc);
	},
	/**
	 * Store'u tekrar load eder.	 
	 * @name refresh
	 * @function
	 * @type {Function}
	 * @memberOf  mj.pager
	 */
	refresh : function(){
		this.store.trigger('sourcerefresh',this.store);
		//this.store.url=this.store.url.split('?')[0];
		this.params.current=1;
		this.store.filters=this.store.routename?this.store.filters:false;
		var routeName = this.store.routename?this.store.routename:this.store.modulename + '-showall';
        var route = Routing.getRoute(routeName);
        var data = {
            'pg': this.params.current,
            'lm': this.params.limit
		};
		if(this.store.routename&&this.store.filters){
			data.filters=this.store.filters;
		}
		var url = Routing.generate(routeName, data);
		this.store.url=url;
		this.store.load();
		this.store.trigger('afterrefresh',this.store);
	}
};
mj.extend(mj.pager, mj.component);
//!!! myjui/source/data/dataFilter.js
mj.dataFilter = function(config){
	mj.dataFilter.superclass.constructor.call(this, config);
};
mj.dataFilter.prototype = {
	componentClass : 'mj.dataFilter',
	width : 460,
	height : 300,
	labelWidth : 150,
	button : false,
	iconCls : 'mj-filter',
	store : false,
	fields : false,
	grid : false,//bağlı olduğu grid elementi mutlaka olmalı
	filterEvent : 'getFilteredRecords',//değiştirme'
	methodName : false,
	splitter : '$_$',//değiştirme'
	countStart : 0,
	init : function(){
		var t = this;
		if(!t.store)
			t.store = t.grid.store;
		if(!t.fields)
			t.fields = t.grid.cm;
		t.grid.filter = t;
		//if(t.button)
		//	t.button.els.c.append(t.button.els.ci = $(mj.NE(t.button.els.c, {cls:'mj-tool-btn-center-icon '+t.iconCls})));
		t.cachedParams = {};
		mj.apply(t.cachedParams,t.store.params);
		t.win = new mj.window({
			renderTo : mj.bd,
			title : mj.lng.glb.filterRecords,
			modal : true,
			width : t.width,
			height : t.height,
			minWidth : t.width,
			minHeight : t.height,
			buttons : [
				{title: mj.lng.glb.exit, handler:function(){t.win.hide();}},
				{title: mj.lng.glb.clear, handler:function(){t.clearFilter();}},
				{title: mj.lng.glb.get, handler:function(){t.getFilteredData();}}
			]
		});
		t.hiddenElements = mj.NE(t.win.getBody(), {style:'position:absolute;top:-10000px;left:-100000px; display:none'});
		t.fakeInput = mj.NE(t.hiddenElements, {tag:'input'});
		var elements = t.getFilterElements(t.fields);
		t.form = new mj.form({
			renderTo : t.win.getBody(),
			items : elements,
			buttonPos:false
		});
		t.addRelated(t.win);
		t.win.addRelated(t.form);
		t.store.on('sourcerefresh',function(){
			t.clearFilter();
		});
	},
	focusFakeInput : function(){
		this.fakeInput.focus();
	},
	clearFilter : function(){
		var t = this, item;
		t.form.clear();
		t.store.params.q=JSON.stringify({});//({}).toJSONString();
		t.grid.clearSort();
	},
	getFilteredData : function(){
		var t = this, values;
		t.focusFakeInput.call(t);
		values = t.getFilterItemValues();
		var data = {
            'pg': 1,
			'lm': t.store.params.limit?t.store.params.limit:t.recordsperpage,
			'filters': values
		};
		t.store.filters=values;
        var url = Routing.generate(t.routeName, data);
		//t.store.params.q = JSON.stringify(values);//values.toJSONString();
		//t.store.params.event = t.filterEvent;
		//t.store.params.methodName = t.methodName?t.methodName:t.filterEvent;
		//t.grid.pbar.params.current = t.grid.pbar.current;
		//t.grid.pbar.params.start = t.grid.pbar.start;
		//t.store.params.current = t.grid.pbar.current;
		//t.store.params.start = t.grid.pbar.start;
		t.store.url=url;
		t.store.load();
		// mj.apply(t.store.params,t.cachedParams);
		// delete t.store.params.q;
		t.win.hide();
	},
	getFilterElements : function(fields){
		var t=this,item,item2,items = [], single, c=t.countStart-(this.tabPanel&&this.tabPanel.hideHeader?1:0);
		for(var i=0,len=fields.length;i<len;i++)
			if(typeof fields[i].filter != 'undefined'){
				single = (fields[i].filter == 'single');
				itemName = fields[i].filterIndex ? fields[i].filterIndex : fields[i].dataIndex;
				switch (fields[i].type){
					case 'check':
						item = new mj.form.checkBox({
							title : fields[i].header,
							labelWidth : this.labelWidth+'px',
							dataIndex : fields[i].dataIndex,
							width : 120,
							table : fields[i].table,
							name : itemName,
							alias: fields[i].alias,
							fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
							operator: (fields[i].operator?fields[i].operator:'=')
						});
						items.push(item);
						break;
					case 'combo':
						if(!single){
							item = new mj.form.combo({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex+'_1',
								store : typeof fields[i].store != 'undefined' ? fields[i].store : [{id:0,code:mj.lng.titles.buttons.no},{id:1,code:mj.lng.titles.buttons.yes}],
								width : 117,
								table : fields[i].table,
								mode : typeof fields[i].mode != 'undefined' ? fields[i].mode : 'remote',
								itemStyle : 'float:left;clear:none;',
								name : itemName + t.splitter + "1",
								alias: fields[i].alias,
								sameValueDisplay: (typeof fields[i].sameValueDisplay!==undefined?fields[i].sameValueDisplay: (fields[i].store?(fields[i].store.routedata?(fields[i].store.routedata.fieldId?false:true):true):true) ) ,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'>=')
							});
							items.push(item);
							item2 = new mj.form.combo({
								//left : 280,
								//top : c>0?c*27:0,
								itemStyle : 'padding-left:10px;float:left;clear:none;',
								dataIndex : fields[i].dataIndex+'_2',
								store : typeof fields[i].store != 'undefined' ? fields[i].store : [{id:0,code:mj.lng.titles.buttons.no},{id:1,code:mj.lng.titles.buttons.yes}],
								width : 117,
								table : fields[i].table,
								mode : typeof fields[i].mode != 'undefined' ? fields[i].mode : 'remote',
								name : itemName + t.splitter + "2",
								alias: fields[i].alias,
								sameValueDisplay: (typeof fields[i].sameValueDisplay!==undefined?fields[i].sameValueDisplay: (fields[i].store?(fields[i].store.routedata?(fields[i].store.routedata.fieldId?false:true):true):true) ) ,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'<=')
							});
							items.push(item2);
						}else{
							item = new mj.form.combo({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex,
								store : typeof fields[i].store != 'undefined' ? fields[i].store : [{id:0,code:mj.lng.titles.buttons.no},{id:1,code:mj.lng.titles.buttons.yes}],
								width : fields[i].width||117,
								table : fields[i].table,
								mode : typeof fields[i].mode != 'undefined' ? fields[i].mode : 'remote',
								name : itemName,
								alias: fields[i].alias,
								sameValueDisplay: (typeof fields[i].sameValueDisplay!==undefined?fields[i].sameValueDisplay: (fields[i].store?(fields[i].store.routedata?(fields[i].store.routedata.fieldId?false:true):true):true) ) ,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'=')
							});
							items.push(item);
						}
						break;
					case 'date':
						if(!single){
							item = new mj.form.dateField({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex+'_1',
								initValue : null,
								table : fields[i].table,
								//epoch : fields[i].epoch,
								itemStyle : 'float:left;clear:none;',
								name : itemName + t.splitter + "1",
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'<=')
							});
							items.push(item);
							item2 = new mj.form.dateField({
								// left : 280,
								// top : c>0?c*27:0,
								itemStyle : 'padding-left:25px;float:left;clear:none;',
								dataIndex : fields[i].dataIndex+'_2',
								initValue : null,
								table : fields[i].table,
								//epoch : fields[i].epoch,
								name : itemName + t.splitter + "2",
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'>=')
							});
							items.push(item2);
						}else{
							item = new mj.form.dateField({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex,
								initValue : null,
								table : fields[i].table,
								//epoch : fields[i].epoch,
								name : itemName,
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'=')
							});
							items.push(item);
						}
						break;
					case 'int':
						if(!single){
							item = new mj.form.numberField({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex+'_1',
								suffix : '',
								decimalPrecision : 0,
								table : fields[i].table,
								itemStyle : 'float:left;clear:none;',
								name : itemName + t.splitter + "1",
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'>=')
							});
							items.push(item);
							item2 = new mj.form.numberField({
								// left : 280,
								// top : c>0?c*27:0,
								itemStyle : 'padding-left:10px;float:left;clear:none;',
								dataIndex : fields[i].dataIndex+'_2',
								suffix : '',
								decimalPrecision : 0,
								table : fields[i].table,
								name : itemName + t.splitter + "2",
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'<=')
							});
							items.push(item2);
						}else{
							item = new mj.form.numberField({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex,
								suffix : '',
								decimalPrecision : 0,
								table : fields[i].table,
								name : itemName,
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'=')
							});
							items.push(item);
						}
						break;
					default:
						if(!single){
							item = new mj.form.textField({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex+'_1',
								table : fields[i].table,
								itemStyle : 'float:left;clear:none;',
								name : itemName + t.splitter + "1",
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'>=')
							});
							items.push(item);
							item2 = new mj.form.textField({
								// left : 280,
								// top : c>0?c*27:0,
								itemStyle : 'padding-left:10px;float:left;clear:none;',
								dataIndex : fields[i].dataIndex+'_2',
								table : fields[i].table,
								name : itemName + t.splitter + "2",
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'<=')
							});
							items.push(item2);
						}else{
							item = new mj.form.textField({
								labelWidth : this.labelWidth+'px',
								title : fields[i].header,
								dataIndex : fields[i].dataIndex,
								table : fields[i].table,
								name : itemName,
								alias: fields[i].alias,
								fieldname: (fields[i].fieldname?fields[i].fieldname:fields[i].dataIndex),
								operator: (fields[i].operator?fields[i].operator:'%.%')
							});
							items.push(item);
						}
				}
				item.on("change",function(r,e){
					if(e.keyCode==13){
						e.preventDefault();
						e.stopPropagation();
						r._onBlur.call(r);
						t.win.buttons[2].handler();
					}
				});
				t.addRelated(item);
				t.addRelated(item2);
				c++;
			}
		return items;
	},
	getFilterItemValues : function(){
		var t = this, item, val,_tmp=[];
		var parent=false;
		for(var i=0,len=t.form.items.length;i<len;i++){
			item = t.form.items[i];

			var q={};
			val = item.getValue(); 
			if((item.componentClass!=='mj.form.checkBox'&&val !== '' && val !== null && val !== undefined && val!==false)||(item.componentClass==='mj.form.checkBox'&&val!==false)){
				if(!parent){
					q.alias=item.alias;
					q.fieldname=item.fieldname;
					q.operator=item.operator;
					q.value=val;
					_tmp.push(q);
				}else{
					parent.operator='between';
					parent.value+="|"+val;
				}
			}
			if(item.dataIndex.indexOf('_')>-1){
				if(parseInt(item.dataIndex.split('_')[1])===1){
					parent=q;
				}else{
					parent=false;
				}
			}else{
				parent=false;
			}
		}
		return _tmp;
	},
	show : function(){
		this.win.show();
		if(this.form.items.length>0)
			this.form.items[0].focus();
	}
};
mj.extend(mj.dataFilter, mj.component);
mj.dataFilterTrigger = function(config){
	mj.dataFilterTrigger.superclass.constructor.call(this, config);
};
mj.dataFilterTrigger.prototype = {
	componentClass : 'mj.dataFilterTrigger',
	table : '',
	countStart : 1,
	returns : false,
	handler : false,
	filterMode : true,
	localSort : false,
	limit : 10,
	tabs : {},
	titles : {},
	init : function(){
		var t = this;
		mj.apply(t.titles,{tabs : {filter : mj.lng.glb.filter, records : mj.lng.glb.records}});
		t.store = new mj.store({url : t.url, localSort : t.localSort,params : {event : t.filterEvent, methodName : t.methodName, table : t.table}});
		mj.applyIf(t.store.params,t.params);
		var sc = this;
		t.win = new mj.window({
			renderTo : mj.NE(),
			title : mj.lng.glb.filterRecords,
			modal : true,
			width : t.width,
			height : t.height,
			minWidth : t.width,
			minHeight : t.height,
			buttons : [
				{title: mj.lng.glb.exit, handler:function(){
						if(typeof t.handler == 'function')
							t.handler(t, undefined);
						t.win.hide();
					}
				},
				{title: mj.lng.glb.clear, handler:function(){
					t.clearFilter();
					t.tabPanel.setActive(0);
					var str = sc.filterMode?mj.lng.glb.records:mj.lng.glb.filter;
					var x = sc.btns.getIndex('title',str);
					if(x>-1)
						sc.btns[x].setTitle(mj.lng.glb.records);
					sc.filterMode = true;
					}
				},
				{title: mj.lng.glb.records, handler:function(){
					if(sc.filterMode){
						t.getFilteredData();
						this.setTitle(mj.lng.glb.filter);
						sc.filterMode = false;
					}else{
						sc.tabPanel.setActive(0);
						this.setTitle(mj.lng.glb.records);
						sc.filterMode = true;
					}
				}}
			]
		});
		t.hiddenElements = mj.NE(t.win.getBody(), {style:'position:absolute;top:-10000px;left:-100000px; display:none'});
		t.fakeInput = mj.NE(t.hiddenElements, {tag:'input'});
		t.win.on('beforeclose',function(){
			this.buttons[0].handler();
		});
		t.btns = t.win.buttons;
		t.mask = new mj.mask({el:t.win.getBody()});
		t.panel = new mj.panel({
			renderTo : t.win.getBody()
		});
		t.tabPanel = new mj.tab({
			renderTo : t.panel.getBody(),
			activeTab : 0,
			border : false,
			hideHeader : true,
			tabWidth : 120,
			maxTitle : 12,
			items :[
				{
					title: t.titles.tabs.filter,
					iconCls:'tabs',
					closable:false
				},{
					title: t.titles.tabs.records,
					iconCls:'tabs',
					closable:false
				}
			]
		});
		t.tabs['filter'] = t.tabPanel.tabs[0];
		t.tabs['records'] = t.tabPanel.tabs[1];
		var elements = t.getFilterElements(t.fields);
		t.form = new mj.form({
			renderTo : t.tabs.filter.getBody(),
			items : elements
		});
		t.grid = new mj.grid({
			renderTo : t.tabs.records.getBody(),
			store : t.store,
			pbar : new mj.pager({pos:'bottom',limit:t.limit}),
			fitToParent : true,
			cm : t.fields
		});
		if(t.handler && typeof t.handler == 'function')
			t.grid.on('rowdblclick',function(g,s){
				if(t.handler(t, g.selectedRow.data)!=false)
					t.win.hide();
			},t);
		else if(t.returns)
			t.grid.on('rowdblclick',function(g,s){
				this.setValue(g.selectedRow.data[t.returns.values[0].name],true);
				this.setValue(g.selectedRow.data[t.returns.values[1].name]);
				this.trigger('dataselect', this, g.selectedRow.data);
				t.win.hide();
			},t.returns.el);
		t.store.on('sourcerefresh',function(){
			t.clearFilter();
		});
	},
	getFilteredData : function(){
		var t = this, values;
		t.focusFakeInput.call(t);
		t.mask.show(50);
		var i=0;
		while(++i<t.btns.length)
			t.btns[i].setDisable();
		values = t.getFilterItemValues();
		t.store.params.q = JSON.stringify(values);//values.toJSONString();
		t.store.params.event = t.filterEvent;
		if(t.className)
			t.store.params.className = t.className;
		t.store.params.methodName = t.methodName?t.methodName:t.filterEvent;
		t.grid.pbar.params.current = t.grid.pbar.current;
		t.grid.pbar.params.start = t.grid.pbar.start;
		t.store.params.current = t.grid.pbar.current;
		t.store.params.start = t.grid.pbar.start;
		t.store.load();
		// delete t.store.params.q;
		// t.store.on('load',function(){
			// t.tabPanel.setActive(1);
			// var i=0;
			// while(++i<t.btns.length)
				// t.btns[i].setEnable();
			// t.mask.hide();
		// });
		t.store.onOnce('load',t._onStoreLoad,t);
	},
	_onStoreLoad : function(){
		this.tabPanel.setActive(1);
		var i=0;
		while(++i<this.btns.length)
			this.btns[i].setEnable();
		this.mask.hide();
	},
	show : function(){
		this.win.show();
		return this.win;
	}
};
mj.extend(mj.dataFilterTrigger, mj.dataFilter);
//!!! myjui/source/drag/drag.js
/**
 * Bir elemanı sürüklenebilir hale getirmek için kullanılan sınıf
 * @class
 * @name mj.drag
 * @extends mj.component
 * @param {Object/String} config Konfigürasyon parametreleri. Sürüklenmek istenen elemanın id değeri string olarak verilebilir.
 * @config.el {HTMLElement/String} 
 */
mj.drag = function(config){
	mj.drag.superclass.constructor.call(this, config);
};

mj.drag.prototype = {
	componentClass : 'mj.drag',
	defaultConfigStr : 'el',
	proxy : true,
	dragType : 'vh',
	clientDrag : true,
	moving : true,
	copy : false,
	cls : '',
	startAction : 'mousedown',
	dropEls : [],
	curDrop : false,
	dropOverCls : 'mj-drag-drop-hover',
	dropBottomCls : 'mj-drag-drop-bottom',
	appendParent : false,
	keepAspectRatio : false,
	proxyOpacity : .5,
	/**
	 * Verilen elemanın sürüklenebilir bir nesne olabilmesi gerekli atamaları yapar.
	 * @name init
	 * @function
	 * @type {Function}
	 * @memberOf  mj.drag
	 */
	initDrops : function(){
		for(var i=0,len=this.dropEls.length;i<len;i++){
			var $el =$(this.dropEls[i]);
			ofs = $el.offset();
			reg = {};
			reg.l = ofs.left;
			reg.t = ofs.top;
			reg.r = ofs.left+$el.width();
			reg.b = ofs.top+$el.height();
			this.dropEls[i].reg = reg;
		}
	}, 
	fromDrops : function(x){
		var t=this;
		t.fromDrop = false;
		for(var i=0,len=t.dropEls.length;i<len;i++){
			var el = t.dropEls[i],$el = $(el);
			var reg = el.reg;
			if(x.left>=reg.l&&x.left<=reg.r&&x.top>=reg.t&&x.top<=reg.b)
				t.fromDrop = el;

		}
	}, 	
	checkDrops : function(sc){
		var t=this;
		t.curDrop = false;
		for(var i=0,len=t.dropEls.length;i<len;i++){
			var el = t.dropEls[i],$el = $(el);
			var reg = el.reg;
			t.bottomDrop = false;
			if(Math.abs(sc.Y-reg.b)<=4&& sc.X>=reg.l&&sc.X<=reg.r){
				$el.addClass(t.dropBottomCls);	
				t.curDrop = el;
				t.bottomDrop = true;
				t.trigger('onDropOver',t,el);
				$el.trigger('onDropOver');		
			}else
				if($el.hasClass(t.dropBottomCls)){
					$el.removeClass(t.dropBottomCls);
					t.trigger('onDropOut',t,el);
					$el.trigger('onDropOut');
				}
			
			if(t.bottomDrop !== true && sc.X>=reg.l&&sc.X<=reg.r&&sc.Y>=reg.t&&sc.Y<=reg.b){
				$el.addClass(t.dropOverCls);	
				t.curDrop = el;
				t.bottomDrop = false;
				t.trigger('onDropOver',t,el);		
				$el.trigger('onDropOver');	
			}else
				if($el.hasClass(t.dropOverCls)){
					$el.removeClass(t.dropOverCls);
					t.trigger('onDropOut',t,el);
					$el.trigger('onDropOut');
				}
		}
	}, 	
	destroy : function(){
		this._el.remove();
	},
	init : function(){
		var t = this,zy=this;
		if(!t.dragEl)
			t.dragEl = t.el;
		var el = t.el = (typeof t.el === 'string' ? mj.get(t.el) : t.el), 
			dragEl = t.dragEl = (typeof t.dragEl === 'string' ? mj.get(t.dragEl) : t.dragEl);
		if(!el)
			return;
		var esize;
		var _el = t._el = $(el), _dragEl = t._dragEl = $(dragEl);
		t.pwidth=0;
		t.pheight=0;
		if(!t.parent)
			t.parent = _el.parent();
		else{
			t.parent = $(t.parent);
			if(t.parent.css('position')!='static'||t.parent[0]==mj.bd[0])
				t.offsetParent = t.parent[0];
		}
		_el.addClass(t.cls);
		var elCss = _el.css('position');
		if(elCss!='relative' && elCss!='static') //buraya ayrıca bakılacak şimdilik relative kullanılmasın
			_el.css('position','absolute');
		if(t.side){
			_dragEl.addClass('mj-resizer-'+t.side);
			t.mode = 'R';
		}
		var width, height, p, lp = {}, dif = {}, ppos = {}, pos={},sc;
		if(t.keepAspectRatio)
			t.aspectRatio = _el.outerHeight()/_el.outerWidth();
		if(t.resizer){
			if(t.resizer.maxWidth){
				var left = parseInt(t.resizer._el.css('left'));
				if(left)
					t.resizer.setMaxWidth(t.resizer.config.maxWidth-left);
				if(!t.resizer.config.parentBorders || !t.resizer.config.parentBorders.maxWidth)
					t.resizer.config.parentBorders.maxWidth = t.resizer.config.maxWidth;
			}
			if(t.resizer.maxHeight){
				var top = parseInt(t.resizer._el.css('top'));
				if(top)
					t.resizer.setMaxHeight(t.resizer.config.maxHeight-top);
				if(!t.resizer.config.parentBorders || !t.resizer.config.parentBorders.maxHeight)
					t.resizer.config.parentBorders.maxHeight = t.resizer.config.maxHeight;
			}
		}
		var _setConstraints,_start,_stop,_move;
		_setConstraints = function(pos){
			var min1,min2,max1,max2;
			if(t.mode=='R'){
				min1 = t.minWidth, max1 = t.maxWidth;//||($.browser.msie?pos.E.target.parentElement.clientWidth-1:pos.E.view.innerWidth-3)-parseInt($(pos.E.target).css('left'));
				min2 = t.minHeight, max2 = t.maxHeight;//||300;
			}else{
				if(t.clientDrag){
					min1 = t.minWidth||t.clientReg.l, max1 = t.maxWidth||t.clientReg.r-width;
					min2 = t.minHeight||t.clientReg.t, max2 = t.maxHeight||t.clientReg.b-height;
				}
			}
			pos.X = pos.X<min1 ? min1 : (pos.X>max1 ? max1 : pos.X);
			pos.Y = pos.Y<min2 ? min2 : (pos.Y>max2 ? max2 : pos.Y);
			if(t.keepAspectRatio){
				if(pos.X * t.aspectRatio<min2 || pos.X * t.aspectRatio > max2)
					pos.X = pos.Y / t.aspectRatio;
				else
					pos.Y = pos.X * t.aspectRatio;
			}
			//mj.log('x='+pos.X+',y='+pos.Y);
			return {X:pos.X,Y:pos.Y};
		},_start = function(e){
			if((t.startAction=='mousedown' && e.which==1) && t.trigger('beforedrag', t, e, lp)!==false&&t.pause!==true && (e.target && $(e.target).attr("type") != 'text')){
				e.preventDefault();
				t.dragging = 'half';
				$(mj.bd).mouseup(_stop).mousemove(_move);
				t.trigger('dragstart', t, e, lp);
			}
		}, _stop = function(e){
			if(t.dragging=='half')
				$(mj.bd).unbind('mousemove',_move).unbind('mouseup',_stop);
			else if(t.dragging==true){
				$(mj.bd).unbind('mousemove',_move).unbind('mouseup',_stop);
				e.preventDefault();
				if(t.proxy)
					t.proxy[0].className='';
				if(t.trigger('dragstop', t,t.curDrop,t.fromDrop)!==false){
					if(t.mode=='R'){
						if(_el.hasAbsoluteParent())
							var fx=0,fy=0;
						else
							var fx=$.browser.msie?0:e.view.scrollMaxX,fy=$.browser.msie?0:e.view.scrollMaxY;
						_el.css({width:pos.X-fx, height:pos.Y-fy});
						t.trigger('resize', t, pos.X, pos.Y);
					}else if(t.moving){
						lp = p.offset();
						//alert('lpx='+lp.left+',lpy='+lp.top);
						//mj.log('lpx='+lp.left+',lpy='+lp.top);
						if(t.offsetParent!=mj.bd[0]){
							ppos = $(t.offsetParent).offset({border:true});
							lp.left = lp.left-ppos.left;
							lp.top = lp.top-ppos.top;
						}
						//alert('px='+ppos.left+',py='+ppos.top);
						//mj.log('px='+ppos.left+',py='+ppos.top);
						//lp['z-index'] = '32000';
						if(t.copy){
							t.xel = _el.clone().css(lp).appendTo(t.parent);
							var config = t.config;
							config.el = t.xel[0];
							config.copy = false;
							var ndrag = new mj.drag(config);
							ndrag.on('dragafterstop',function(e,x,y){
								t.trigger('dragafterstop',e,x,y);
							},t);
							ndrag.on('dragstop',function(e,x,y){
								t.trigger('dragstop',e,x,y);
							},t);
						}	
						else	
							_el.css(lp);//.appendTo(t.parent);
							if(t.appendParent)
								_el.appendTo(t.parent);
						if(t.parent && t.resizer){
							if(t.resizer.maxWidth){
								var pWidth = t.resizer.config.parentBorders.maxWidth-lp.left;
								t.resizer.setMaxWidth(pWidth > t.resizer.config.maxWidth ? t.resizer.config.maxWidth : pWidth);
							}
							if(t.resizer.maxHeight){
								var pHeight = t.resizer.config.parentBorders.maxHeight-lp.top;
								t.resizer.setMaxHeight(pHeight > t.resizer.config.maxHeight ? t.resizer.config.maxHeight : pHeight);
							}
						}
					}
					t.trigger('dragafterstop', t,t.curDrop,t.fromDrop);
				}
				if(t.curDrop)
					$(t.curDrop).removeClass(t.dropOverCls);
				if(t.proxy){
					t.proxy.remove();
					t.proxy = true;
				}
				t.dragging = null;
				window.mjDragging = null;
				
			}
			
		}, _move = function(e){
			e.preventDefault();
			if(t.dragging=='half'){
				width = _el.outerWidth();
				height = _el.outerHeight();
				t.pheight = t.parent[0].clientHeight;
				t.pwidth = t.parent[0].clientWidth;		
				t.offsetParent = t.offsetParent||t.el.offsetParent||mj.bd[0];
				if(t.clientDrag){
					var o={left:0,top:0};
					if(t.parent[0]!=t.offsetParent)
						var o = t.parent.offset({relativeTo : t.offsetParent,border:true});
					t.clientReg = { l : o.left, t : o.top, r: o.left + t.pwidth, b : o.top + t.pheight};
				}
				lp = _el.offset({relativeTo:t.offsetParent});
				t.dropEls = t.dropEls.length>0?t.dropEls:window.dropEls||[];				
				if(t.dropEls.length>0){
					t.initDrops();
					t.fromDrops(lp);
				}	
				if(t.proxy){
					if(t._el.hasAbsoluteParent()){
						var _l = parseInt(t._el.css('left')), _t=parseInt(t._el.css('top'));
						_l = !isNaN(_l) ? _l : 0;
						_t = !isNaN(_t) ? _t : 0;
						var off = {left:_l, top:_t};
					}else
						var off = t._el.offset({relativeTo:t.offsetParent});
					p = t.proxy = $(mj.NE(t.parent, {cls:'mj-proxy'})), op = {'z-index':'32001',position : (t.position||_el.css('position')=='static')?'absolute':(t.position||_el.css('position')), width:t.proxyEl?t.proxyEl.width:width, height:t.proxyEl?t.proxyEl.height:height, left:off.left, top:off.top, opacity:t.proxyOpacity};
					p.css(op);
				}else
					p = t._el;
				if(t.mode=='R')
					esize = { width:_el.width(), height:_el.height()};				
				t.dragging = true;
				window.mjDragging = true;
				dif.left = e.pageX;
				dif.top = e.pageY;
			}
			if(t.dragging){				
				if(t.mode=='R'){
					pos = {X : esize.width + e.pageX - dif.left, Y : esize.height + e.pageY - dif.top, E:e};
					sc=_setConstraints(pos);
					p.css({width:sc.X, height:sc.Y});
				}else{
					pos = {X : lp.left + e.pageX - dif.left, Y : lp.top + e.pageY - dif.top};
					sc=_setConstraints(pos);
					//mj.log('scx='+sc.X+',scy='+sc.Y);
					if(t.dragType=='vh')
						p.css({left:sc.X, top:sc.Y});
					else if(t.dragType=='v')
						p.css({top:sc.Y});
					else if(t.dragType=='h')
						p.css({left:sc.X});
					if(t.trigger('dragmove', t, sc.X, sc.Y)==false)
						_stop(e);
					if(t.dropEls.length>0)
						t.checkDrops(sc);
				}
			}
		};
		_dragEl.bind(this.startAction,_start);
		mj.drag.superclass.init.call(this);
	}
};

mj.extend(mj.drag, mj.component);
//!!! myjui/source/drag/itemSorter.js
mj.itemSorter = function(config){
	mj.itemSorter.superclass.constructor.call(this, config);
};

mj.itemSorter.prototype = {
	componentClass : 'mj.itemSorter',
	init : function(){
		var t = this;
		t.items = [];
		t.itemParents = [];
		for(var i=0,l=t.sortItems.length;i<l;i++){
			var el = t.sortItems[i];
			item = {
				el:$(el),
				index : i,
				drag : new mj.drag({
					el : el,
					parent : t.dragParent ? t.dragParent : mj.bd,
					moving : !!t.moving,
					dropEls : t.sortItems
				})
			};
			var p = item.el.parent();
			p.data('mj-item-sorter-parent', i);
			t.itemParents.push(p);
			item.drag.on('beforedrag',function(){
				item.drag.dropEls = t.sortItems;
			});
			item.drag.on('dragstop',function(e,target){
				if(target)
					t.moveItem(this, $(target));
			},item);
			item.el.css('cursor', 'move');
			t.items.push(item);
		}
		mj.itemSorter.superclass.init.call(t);
	},
	moveItem : function(item, target){
		var t = this;
		item.el.insertBefore(target);
	}
};

mj.extend(mj.itemSorter, mj.component);
//!!! myjui/source/resizer/resizer.js
/**
 * Bir elemanı tekrar boyutlanabilir hale getirmek için kullanılan sınıf
 * @class
 * @name mj.resizer
 * @extends mj.component
 * @param {Object/String} config Konfigürasyon parametreleri. Boyutu değiştirilmek istenen elemanın id değeri string olarak verilebilir.
 * @config.el {HTMLElement/String} 
 */
mj.resizer = function(config){
	mj.resizer.superclass.constructor.call(this, config);
};

mj.resizer.prototype = {
	componentClass : 'mj.resizer',
	defaultConfigStr : 'el',
	proxy : true,
	keepAspectRatio : false,
	init : function(){
		var t = this, el = t.el, _el = t._el = $(el);
		var rel = t.resizeEl = $(mj.NE(el, {html:mj.insertSpacer(10,10)}));
		t.se = new mj.drag({el:el, dragEl:rel, side:'se', minWidth : t.minWidth, maxWidth:t.maxWidth, minHeight:t.minHeight, maxHeight:t.maxHeight, keepAspectRatio : t.keepAspectRatio, proxy:t.proxy});
		t.se.on('resize', function(dragger, width, height){
			_el.trigger('resize');
		});
	},
	setMaxWidth : function(val){
		if(this.se)
			this.se.maxWidth = val;
	},
	setMaxHeight : function(val){
		if(this.se)
			this.se.maxHeight = val;
	}
};

mj.extend(mj.resizer, mj.component);
//!!! myjui/source/tools/rating.js
mj.rating = function(config){
	mj.rating.superclass.constructor.call(this, config);
};

mj.rating.prototype = {
	componentClass : 'mj.rating',
	disabled : false,
	iconPos : 'left',
	cls : 'mj-rating-star',
	count : 5,
	value : 0,
	enabled : true,
	init : function(){
		var t = this, id=t.id || mj.genId('mj-rating-');
		t.cnt = mj.NE(t.renderTo, {cls:t.cls, id: id});
		t._cnt = $(t.cnt);
		t._cnt.bind('mouseout', function(){
			t.setRawValue.call(t, t.value);
		});
		t.icons = [];
		for(var i=0; i<t.count; i++){
			t.icons.push({_el : $(mj.NE(t.cnt, {cls:'mj-rating-icon mj-off', id:id+'-icons-'+(i+1), html:mj.insertSpacer(12,12)}))});
			t.icons[i]._el.bind('mouseover', function(){
				if(t.enabled)
					t.setRawValue.call(t, this.id.toString().replace(id+'-icons-',''));
			});
			t.icons[i]._el.bind('click', function(){
				if(t.enabled){
					var val = this.id.toString().replace(id+'-icons-','');
					t.setValue.call(t, val);
					t.trigger('click', t, val);
				}
			});
		}
		t.setValue(t.value);
	},
	setIcon : function(index, val){
		var t = this, icon = t.icons[index];
		if(icon.value!=val){
			if(val){
				icon._el.removeClass('mj-off');
				icon._el.addClass('mj-on');
			}else{
				icon._el.removeClass('mj-on');
				icon._el.addClass('mj-off');
			}
		}
	},
	setRawValue : function(val){
		var t = this;
		val = parseInt(val);
		if(val>=0 && val<=t.count){
			for(var i=0;i<val;i++)
				t.setIcon(i, true);
			for(var j=val;j<t.count;j++)
				t.setIcon(j, false);
			return true;
		}
		return false;
	},
	setValue : function(val){
		var rVal = this.setRawValue(val);
		if(rVal)
			this.value = val;
		return rVal;
	},
	enable : function(){
		this.enabled = true;
	},
	disable : function(){
		this.enabled = false;
	}
};
mj.extend(mj.rating, mj.component);
//!!! myjui/source/tools/button.js
/**
 * Temel buton sınıfı.
 * @name mj.button
 * @class
 * @extends mj.component
 * @scope  mj.component
 * @param {Object} config
  */

mj.button = function(config){
	mj.button.superclass.constructor.call(this, config);
};

mj.button.prototype = {
	componentClass : 'mj.button',
	disabled : false,
	iconPos : 'left',
	buttonAlign : 'right',
	cls : '',
	invisible : false,
	insufficientRights : false,
	alt : false,
	//width:50,
	/**
	 * Buton bileşeninin görsel elemanlarının oluşturularak olay dinleyicilerinin atanması işlemini gerçekleştirir.
	 * @name init
	 * @function
	 * @type {Function}
	 * @memberOf  mj.button
	 */
	init : function(){
		var btn = this;
		this._cw = this.width ? ('width:'+(this.width-12)+'px') : '';
		var el = this.el = this.renderTo;
		var _e = this._el = $(el);
		_e.addClass('mj-button '+(this.buttonAlign=='left'?'mj-button-left1':'')+' mj-unselectable '+btn.cls);
		if(btn.invisible)
			_e.addClass('mj-invisible');
		if(btn.alt){
			btn.alt=mj.translate(btn.alt);
			_e.attr('title', btn.alt);
		}
		btn.title = mj.translate(btn.title);
		var insertIcon = function(){
			return btn.icon = $(mj.NE(btn.renderTo, {cls:'mj-button-icon '+btn.iconCls}));
		};
		mj.NE(this.renderTo, {cls:'mj-button-left'});
		var center = this._center = $(mj.NE(this.renderTo, {cls:'mj-button-center mj-unselectable', style:this._cw, unselectable:'on', html:this.title ? ('<center><span style="'+this._cw+'" unselectable="on">'+this.title+'</span></center>'):''}));
		if(this.iconCls)
			if(this.iconPos == 'right')
				center.append(insertIcon(center));
			else
				center.prepend(insertIcon(center));
		mj.NE(this.renderTo, {cls:'mj-button-right'});
		if(this.disabled)
			this.disable();
		
		_e.hover(function(){
			if(!btn.disabled)
				_e.addClass("mj-button-hover");
		},function(){
			if(!btn.disabled)
				_e.removeClass("mj-button-hover");
		});
		_e.mouseout(function(){
			_e.removeClass("mj-button-down");
		});
		_e.mousedown(function(){
			if(!btn.disabled)
				_e.addClass("mj-button-down");
		});
		_e.mouseup(function(){
			_e.removeClass("mj-button-down");
		});
		_e.click(function(e){
			if(!btn.disabled){
				if(mj._activeFormElement && typeof mj._activeFormElement.trigger == 'function')
					mj._activeFormElement.trigger('blur',mj._activeFormElement);
				btn.trigger('click', btn, btn.handlerId, e);
			}	
		});
		if(btn.handler)
			btn.on('click', btn.handler, btn.scope);
		if(btn._customRights){
			var _customRightsId = btn._customRights.split(',');
			_customRightsId = _customRightsId[0];
			if(mj._renderingModule && mj._renderingModule._customRights && mj._renderingModule._customRights.indexOf(_customRightsId)>-1){
				btn._el.addClass('mj-invisible');
				btn.insufficientRights = true;
				btn.setHandler(function(){return false;},this);
			}
		}
		mj.button.superclass.init.call(this);
	},
	setHandler : function(cb, scope){
		if(this.handler)
			this.mon('click', this.handler);
		this.handler = cb;
		this.scope = scope || this.scope || this;
		this.on('click', this.handler, this.scope);
	},
	setDisable : function(){
		this.disable();
	},
	setEnable : function(){
		this.enable();
	},
	disable : function(){
		if(!this._el.hasClass('mj-disabled')){
			this._el.addClass('mj-disabled');
			if(this.icon)
				this.icon.addClass(this.iconCls+'-disabled');
		}
		this.disabled = true;
	},
	enable : function(){
		this._el.removeClass('mj-disabled');
		if(this.icon)
			this.icon.removeClass(this.iconCls+'-disabled');
		this.disabled = false;
	},
	setAlt : function(alt){
		this.alt = mj.translate(alt);
		this._el.attr('title', this.alt);
	},
	setTitle : function(title){
		this.title = mj.translate(title);
		this._center[0].lastChild.innerHTML = this.title ? ('<span style="'+this._cw+'" unselectable="on">'+this.title+'</span>'):''; 
	},
	hide : function(){
		if(!this.invisible)
			this._el.addClass('mj-invisible');
		this.invisible = true;
	},
	show : function(){
		if(this.invisible && !this.insufficientRights)
			this._el.removeClass('mj-invisible');
		this.invisible = false;
	}
};
mj.extend(mj.button, mj.component);
//!!! myjui/source/tools/speedButton.js
mj.speedButton = function(config){
	mj.speedButton.superclass.constructor.call(this, config);
};

mj.speedButton.prototype = {
	componentClass : 'mj.speedButton',
	init : function(){
		var btn = this;
		var el = this.el = mj.NE(this.renderTo);
		var _e = this._el = $(el);
		_e.addClass('mj-speed-button mj-unselectable '+btn.cls);
		this.icon = $(mj.NE(_e,{cls:'mj-speed-button-icon '+btn.iconCls}));
		if(btn.alt){
			btn.alt=mj.translate(btn.alt);
			_e.attr('title', btn.alt);
		}
		if(this.disabled)
			this.disable();
		
		_e.hover(function(){
			if(!btn.disabled)
				_e.addClass("mj-speed-button-hover");
		},function(){
			if(!btn.disabled)
				_e.removeClass("mj-speed-button-hover");
		});
		_e.mouseout(function(){
			_e.removeClass("mj-speed-button-down");
		});
		_e.mousedown(function(){
			if(!btn.disabled)
				_e.addClass("mj-speed-button-down");
		});
		_e.mouseup(function(){
			_e.removeClass("mj-speed-button-down");
		});
		_e.click(function(e){
			if(!btn.disabled){
				if(mj._activeFormElement && typeof mj._activeFormElement.trigger == 'function')
					mj._activeFormElement.trigger('blur',mj._activeFormElement);
				btn.trigger('click', btn, btn.handlerId, e);
			}	
		});
		if(btn.handler)
			btn.on('click', btn.handler, btn.scope);
		if(btn._customRights){
			var _customRightsId = btn._customRights.split(',');
			_customRightsId = _customRightsId[0];
			if(mj._renderingModule && mj._renderingModule._customRights && mj._renderingModule._customRights.indexOf(_customRightsId)>-1){
				btn._el.addClass('mj-invisible');
				btn.setHandler(function(){return false;},this);
			}
		}
		mj.button.superclass.init.call(this);
	},
	setTitle : function(title){
		return false;
	}
};
mj.extend(mj.speedButton, mj.button);
//!!! myjui/source/tools/mask.js
mj.mask = function(config){
	mj.mask.superclass.constructor.call(this, config);
};
mj.mask.prototype = {
	componentClass : 'mj.mask',
	init : function(){
		var t=this, el = t.el = t.el ? t.el : document.body, _el = t._el = $(el);
		t.mask = $(mj.NE(el, {cls:'mj-mask'}));
		mj.bindResize(t.mask, t.doResize, t);
		mj.mask.superclass.init.call(this);
	},
	doResize : function(){
		var t = this, m = t.mask;
		var el = this.componentClass=='mj.mask'?t._el:undefined;
		if(el)
			if(t._el.parent().length>0){
				var ofs = el.offset();
				var tn = t._el.parent().prop('tagName');
				//m.css('left', (($.browser.msie&&tn=='BODY')?ofs.left:t.el.offsetLeft));
				//m.css('top', (($.browser.msie&&tn=='BODY')?ofs.top:t.el.offsetTop));
				m.css('height', (typeof(tn)=='undefined' || tn=='BODY' || tn=='HTML' || tn.toString()=='') ? mj.getDocHeight() : t.el.offsetHeight);
				m.css('width', t.el.offsetWidth);
			}
	},
	show : function(zIndex){
		this.doResize();
		this.mask.css('z-index', zIndex ? zIndex : this.zIndex);
		this.mask.show();
	},
	hide : function(){
		this.mask.hide();
	},
	destroy : function(){
		this.mask.remove();
	}
}
mj.extend(mj.mask, mj.component);
//!!! myjui/source/tools/randomColors.js
mj.randomColors = function(config){
	this.colors = ['red', 'blue', 'lights', 'green', 'darks'];
	mj.randomColors.superclass.constructor.call(this, config);
};
mj.randomColors.prototype = {
	componentClass : 'mj.randomColors',
	_activeIndex : 0,
	_autoPallette : true,
	r : 256,
	g : 256,
	b : 256,
	border : 360,
	border2 : 270,
	state : "",
	lights : false,
	darks : false,
	changeTint : function(col){
		var t = this;
		t.v = false;
		t.lights = false;
		t.darks = false;
		t.r = 256;
		t.g = 256;
		t.b = 256;			 
		if (col == "red"){
			t.r = 256;
			t.g = 40;
			t.b = 40; 
			t.state = "red";
		}else if (col == "blue"){
			t.r = 40;
			t.g = 40;
			t.b = 256; 
			t.state = "blue";
		}else if (col == "green") {
			t.r = 40;
			t.b = 40;
			t.g = 256; 
			t.state = "green";
		}else if (col =="darks") {
			darks = true;				
		}else if (col == "lights") { 
			lights = true;
		}else {
			t.r = 256;
			t.g = 256;
			t.b = 256;			 
		}	 
	},
	color : function(r,g,b){
		var t = this;
		t.red = r;
		t.green = g;
		t.blue = b;
	},
	generate : function(returnColor){
	    var t = this;
		if(t._autoPallette)
			t.changeTint(t.colors[t._activeIndex++]);
		t.a = 256;
		var r1 = Math.floor(Math.random() * t.r);
		var r2 = Math.floor(Math.random() * t.g);
		var r3 = Math.floor(Math.random() * t.b);		
		var sum = r1 + r2 + r3;

		if (t.lights) {
			while (t.sum < t.border) {				
				var choose = Math.floor(Math.random() * 3);
				switch (t.choose) {
					case 0:	
						r1 += (t.border - t.sum);
						break;
					case 1:
						r3 += (t.border - t.sum);
						break;
					case 2:
						r2 += (t.border - t.sum);
						break;
				}
				sum = r1 + r2 + r3;
			}
		}else if (t.darks) {
			r1 = Math.min(r1, 170);
			r2 = Math.min(r2, 170);
			r3 = Math.min(r3, 170);						
			while (sum >= t.border2) {
				var choose = Math.floor(Math.random() * 3);
				switch (choose) {
					case 0:	
						r1 = Math.max(r1 - 10, 1);
						break;
					case 1:
						r2 = Math.max(r2 - 10, 1);
						break;
					case 2:
						r3 = Math.max(r3 - 10, 1);
						break;
					default:
						break;
				}
				sum = r1 + r2 + r3;											
			}			
		}
		t.r = r1;
		t.g = r2;
		t.b = r3;
		if(returnColor)
			return {r:t.r, g:t.g, b:t.b};
		else{
			var col = "#" + t.toHex(r1) + t.toHex(r2) + t.toHex(r3);
			return col;
		}
	},
	toHex : function(num) {
		var t=this, x = Math.floor(num / 16);
		var y = num - (x * 16);
		var str = "";			 
		str += t.getHex(x) + t.getHex(y);
		return str;
	},
	getHex : function(digit) {
		var str = "";
		switch (digit) {
			case 10:
				str += "A";
				break;
			case 11:
				str += "B";
				break;						 
			case 12:
				str += "C";
				break;
			case 13:
				str += "D";
				break;						 
			case 14:
				str += "E";
				break;
			case 15:
				str += "F";						 						 						 						 						 
				break;
			default:
				str += digit;						 
		}
		return str;	
	},
	darken : function(minVal){
		var t = this;
		t.r = Math.min(minVal, t.r);
		t.g = Math.min(minVal, t.g);
		t.b = Math.min(minVal, t.b);
		return t;
	},
	limit : function(val,minVal,maxVal) {
		return Math.max(Math.min(val, maxVal), minVal);
	},
	normalize : function(){
		this.r = this.limit(parseInt(this.r), 0, 255);
		this.g = this.limit(parseInt(this.g), 0, 255);
		this.b = this.limit(parseInt(this.b), 0, 255);
		return this;
	},
	getSimilar : function(k){
		var t = this;
		t.r = Math.floor(k*40 + t.r);
		t.b = Math.floor(k*40 + t.b);
		t.g = Math.floor(k*40 + t.g);
		t.normalize();
		var col = "#" + t.toHex(t.r) + t.toHex(t.g) + t.toHex(t.b);
		return col;
	}
};
mj.extend(mj.randomColors, mj.component);
//!!! myjui/source/tools/shortcuts.js
mj.shortcuts = {
	on : function(){
		$.hotkeys.add.apply($.hotkeys, arguments);
	},
	mon : function(){
		$.hotkeys.remove.apply($.hotkeys, arguments);
	}
};
//!!! myjui/source/tools/fileManager.js
mj.fileManager = function(config){
	mj.fileManager.superclass.constructor.call(this, config);
};

mj.fileManager.prototype = {
	componentClass : 'mj.fileManager',
	url : false,
	root : false,
	node : false,
	cb : false,
	sc : false,
	_sc : false,
	ft : false,
	onlyFirstPage : false,
	richEditor : false,
	viewPath : false,
	init : function(){
		var t = this;
		t._node = t.node;
		var w = {};
		if(!t.viewPath)
			t.viewPath = '';
		w.cacheParam = t._sc?t._sc.cacheParam:function(store){
			var p = store.params, cp = {};
			for(var i in p)
				if(typeof p[i] != 'function'/*&&i!='toJSONString'*/)
					cp[i] = p[i];
			return cp;
		};
		w.setParam = t._sc?t._sc.setParam:function(store,params){
			store.params = {};
			return mj.apply(store.params,params);
		};
		w.cb = function(){
			var fileName = w.win.getFile();
			if(fileName){
				t.cb.call(t.sc||this, fileName,w);
				w.win.hide();
			}
		};
		if(!t.sc.fm){
			t.sc.fm = w;
			var _w=700,_h=350;
			w.win = new mj.window({
				renderTo : mj.NE(),
				title : t.title ? t.title : mj.lng.objects.browseImage.winTitle,
				modal : true,
				width : _w,
				height : _h,
				minWidth :350,
				minHeight : 350,
				buttons : [
					{title: mj.lng.titles.buttons.cancel, handler:function(){w.win.hide();}},
					{title: mj.lng.titles.buttons.ok, handler:w.cb}
				]
			});
			if(t._sc && t._sc.win){
				t._sc.win.add(w.win);
				t._sc.win.addRelated(w.win);
			}
			w.win.getFile = function(){
				switch(w.tab.activeTab){
					case 0 :
						if(w.view.selections.length>0)
							return w.view.selections[0].store.fileName;
						break;
					case 1 : 
						if(w.tree.selectedNode){
							w.formUpload.setValue({'klasor':w.tree.selectedNode.id});
							w.formUpload.submit({
								url : t.url+'?className=kkController&methodName=upload&fileTypes='+t.ft+'&root='+mj.escape(t.root),
								success : function(data){
									w.view.store.load();
									w.tab.setActive(0);
								},
								failure : function(data){
									mj.message(data.msg);
								},
								scope:this,
								encoded : true
							});
						}else
							new mj.message({
								title : mj.lng.glb.info,
								msg : mj.lng.objects.browseImage.selectFolderBefore,
								modal : true
							});
						break;
					case 2 :
						var url = w.fieldURL.getValue();
						if(url)
							return url;
						break;
				}
				return false;
			}
			if(t.richEditor){
				w.richLayout = new mj.layout({
					renderTo : mj.NE(w.win.getBody()),
					layout : 'border',
					items : [
						{
							region  : 'south',
							initial : 85,
							min : 85,
							max : 195,
							split: true,
							collapsible : true
						}
					]
				});
				w.imageForm = new mj.form({
					renderTo : w.richLayout.getBody('south'),
					layout : 'border',
					items : [
						new mj.form.fieldSet({
							title : mj.lng.objects.wysiwyg.imageProperties,
							items : [
								new mj.form.textField({
									dataIndex : 'url',
									title : mj.lng.glb.url,
									labelWidth : '120px',
									width : 320,
									readOnly : true
								}),
								new mj.form.textField({
									dataIndex : 'alt',
									title : mj.lng.objects.wysiwyg.altText,
									labelWidth : '120px',
									width : 320
								})
							]
						}),
						new mj.form.fieldSet({
							title : mj.lng.objects.wysiwyg.layout,
							items : [
								new mj.form.textField({
									dataIndex : 'width',
									title : mj.lng.glb.width,
									labelWidth : '120px',
									width : 110
								}),
								new mj.form.combo({
									dataIndex : 'alignment',
									title : mj.lng.glb.alignment,
									labelWidth : '90px',
									width : 110,
									right : true,
									clearOnTriggerClick : true,
									store : new mj.store({data:[
										{id:"left",text:mj.lng.glb.left},
										{id:"right",text:mj.lng.glb.right},
										{id:"texttop",text:mj.lng.objects.wysiwyg.texttop},
										{id:"absmiddle",text:mj.lng.objects.wysiwyg.absmiddle},
										{id:"baseline",text:mj.lng.objects.wysiwyg.baseline},
										{id:"absbottom",text:mj.lng.objects.wysiwyg.absbottom},
										{id:"bottom",text:mj.lng.glb.bottom},
										{id:"middle",text:mj.lng.glb.middle},
										{id:"top",text:mj.lng.glb.top}
									]})
								}),
								new mj.form.textField({
									dataIndex : 'height',
									title : mj.lng.glb.height,
									labelWidth : '120px',
									width : 110
								}),
								new mj.form.textField({
									dataIndex : 'hspace',
									title : mj.lng.objects.wysiwyg.hspace,
									right : true,
									labelWidth : '90px',
									width : 110
								}),
								new mj.form.numberField({
									dataIndex : 'border',
									title : mj.lng.objects.wysiwyg.border,
									labelWidth : '120px',
									width : 110
								}),
								new mj.form.textField({
									dataIndex : 'vspace',
									title : mj.lng.objects.wysiwyg.vspace,
									right : true,
									labelWidth : '90px',
									width : 110
								})
							]
						})
					]
				});
			}
						
			w.layout = new mj.layout({
				renderTo : mj.NE(t.richEditor?w.richLayout.getBody('center'):w.win.getBody()),
				layout : 'border',
				border : false,
				items : [
					{
						region  : 'west',
						initial : 190,
						min : 100,
						max : 190,
						split: false,
						collapsible : true
					}
				]
			});
			w.panelWest = new mj.panel({
				renderTo : w.layout.getBody('west'),
				fitToParent : true,
				border : false,
				buttons : [
					{title: '', iconCls:'mj-add', handler:function(){
						var val = prompt(mj.lng.objects.browseImage.addFolder, "").replace(" ","");
						if(val!==''){
							var sel = w.tree.getSelected();
							var cp = w.cacheParam(w.tree.store);
							cp.node = t._node;
							mj.apply(w.tree.store.params,{methodName : 'addDir', fileName : val, node:sel?sel.id:t._node ,oldParams : JSON.stringify(cp)/*cp.toJSONString()*/}); 
							w.tree.store.load();
							w.setParam(w.tree.store,cp);
						}
						// else
							// new mj.message({
								// title : mj.lng.glb.error,
								// msg : mj.lng.objects.browseImage.addFolder,
								// modal : true
							// });
					}},
					{title: '', iconCls:'mj-delete', disabled:true, handler:function(){
						new mj.message({
							title:mj.lng.glb.onay,
							modal : true,
							msg: mj.lng.objects.browseImage.deleteFolder,
							buttons:['NO','YES'],
							cb:function(el,btn){
								if(btn=='YES'){
									var sel = w.tree.getSelected();
									var cp = w.cacheParam(w.tree.store);
									cp.node = t._node;
									mj.apply(w.tree.store.params,{methodName : 'removeDir', node:sel.id ,oldParams : JSON.stringify(cp)/*cp.toJSONString()*/}); 
									w.tree.store.load();
									w.setParam(w.tree.store,cp);
								}
								el.window.close();
							}
						});
					}},
					{title: '', iconCls:'mj-refresh', handler:function(){
						w.tree.store.params.node = t._node;
						w.tree.load();
					}}
				]
			});
			w.storeTree = new mj.store({
				url : t.url,
				params : {
					className : "kkController",
					methodName : "getDir",
					myRoot : this.root,
					node : this.node,
					fileTypes : this.ft
				}
			});
			w.storeTree.on("load",function(){
				w.view.store.params.node = t.node;
				w.view.store.load();
			});
			w.tree = new mj.tree({
				renderTo : w.panelWest.getBody(),
				store : w.storeTree,
				mode : 'remote',
				// rootHide : true,
				// root : {
					// text : 'Uploads',
					// id : 'root',
					// expanded : true
				// },
				icon : true
			});
			var _tabs = [{
					title: mj.lng.objects.browseImage.files,
					iconCls:'tabs',
					closable : false
				},{
					title: mj.lng.objects.browseImage.upload,
					iconCls:'tabs',
					closable : false
				}
			];
			if(t.richEditor)
				_tabs.push({
					title: mj.lng.objects.browseImage.adres,
					iconCls:'tabs',
					closable : false
				});
			w.tab = new mj.tab({
				renderTo : w.layout.getBody('center'),
				activeTab : 0,
				tabWidth : 120,
				hideHeader : t.onlyFirstPage,
				border:false,
				items :_tabs
			});
			w.uploadPanelBody = w.tab.tabs[1].getBody();
			w.tab.on('tabchange', function(tab, newTab, oldTab){
				w.win.buttons[1].setTitle(newTab==1?mj.lng.objects.browseImage.upload:mj.lng.objects.browseImage.select);
				if(newTab==1){
					w.uploadPanelBody.empty();
					w.formUpload = new mj.form({
						renderTo : w.uploadPanelBody,
						items:[
							new mj.form.textField({
								hidden : true,
								type : 'hidden',
								name : 'klasor'
							}),
							new mj.form.fileInput({
								id:'resim',
								name : 'resim',
								maxFile : 10,
								title : mj.lng.objects.browseImage.browse,
								labelWidth : '110px'
							})
						]
					});
				}
			});
			var tmp=['<div class=\"thumb-wrap\">',
			'<div class=\"thumb\"><img src=\"'+t.viewPath+'{url}\" title=\"{title}\"></div>',
			'<span class=\"x-editable\" style=\"font-family:Arial;font-size:11px;\">{name}</span>',
			'</div>'];
			w.panelView = new mj.panel({
				renderTo : w.tab.tabs[0].getBody(),
				fitToParent : true
			});
			w.storeView = new mj.store({
				url : t.url,
				params : {
					className : "kkController",
					methodName : "getImageView",
					myRoot : this.root,
					node : this.node,
					fileTypes : t.ft
				}
			});
			w.view = new mj.view({
				renderTo : w.panelView.getBody(),
				store : w.storeView,
				pbar : new mj.pager({limit:10,pos:'bottom'}),
				tpl : new mj.template(tmp),
				selector : 'div.thumb-wrap'
			});
			w.view.pbar.tbar.addSplitter();
			w.deleteFileButton = w.view.pbar.tbar.addButton({id:'btnDeleteTable',alt:mj.lng.glb.del, iconCls : 'mj-delete', disabled: true, handler:function(){
					new mj.message({
						title:mj.lng.glb.onay,
						modal : true,
						msg:mj.lng.objects.browseImage.deleteFile,
						buttons:['NO','YES'],
						cb:function(el,btn){
							if(btn=='YES'){
								var cp = w.cacheParam(w.view.store);
								mj.apply(w.view.store.params,{methodName : 'deleteFile', fileName : w.view.selections[0].store.fileName,oldParams : JSON.stringify(cp)/*cp.toJSONString()*/}); 
								w.view.store.load();
								w.setParam(w.view.store,cp);
							}
							el.window.close();
						}
					});
				}
			});
			w.view.on('dblclick',function(a){
				w.cb();
			},w);
			w.view.on('itemclick',function(view,item){
				w.deleteFileButton.setEnable();
				if(t.richEditor){
					w.imageForm.clear();
					var d = item.store;
					w.imageForm.setValue({
						url : d.fileName,
						alt : d.name,
						width : d._width+'px',
						height : d._height+'px'
					});
				}
			},w);
			w.view.store.on('beforeload',function(item){
				w.deleteFileButton.setDisable();
			});
			w.tree.on('nodeclick', function(ptree,pnode){
				w.panelWest.buttons[1].setEnable();
				var ps = ptree.selectedNode;
				w.view.store.params.start = 0;
				w.view.pbar.params.start = 0;
				w.view.store.params.current = 1;
				w.view.pbar.params.current = 1;
				w.view.store.params.node = ps.id;
				w.view.store.load();
			});
			if(t.richEditor)
				w.formURL = new mj.form({
					renderTo : w.tab.tabs[2].getBody(),
					items:[
						w.fieldURL = new mj.form.textField({
							title : mj.lng.objects.browseImage.url,
							labelWidth : '125px',
							id : 'url',
							width : 330
						})
					]
				});
		}else
			w = t.sc.fm;
		w.win.buttons[1].setHandler(w.cb, this);
		if(t.richEditor)
			w.fieldURL.setValue('');
		if(!w.tree.store.loaded)
			w.tree.load();
		if(t._sc)
			t._sc.wins.push(w.win);
		w.win.show();
	}
};

mj.extend(mj.fileManager, mj.component);
//!!! myjui/source/menu/menu.js
/**
* Menu ve Araç Çubuğu bileşenlerinin ortak sınıfı. 
* @class
* @extends mj.component
* @param {Object} config AÇIKLAMA
*/
mj.menu = function(config){
	mj.menu.superclass.constructor.call(this,config);
};
mj.menu.prototype = {
	componentClass : 'mj.menu',
	width : 220,
	style : 'horizontal',
	visible : true,
	alignTo : 'tl',//tr, bl, br
	subMenuAlign : 'bl',
	canHide : false,
	drag : false,
	cls : '',
	buttons : false,
	init : function(){
		var dsp = this.visible ? 'block' : 'none';	
		var t = this, n = mj.NE, el = t.el = n(t.renderTo, {cls: 'mj-menu-'+t.style+' '+t.cls, style:'display:'+dsp}), _el = t._el = $(el);
		t._renderTo = $(t.renderTo);
		t.menuConfig = t.menuConfig ||t.config;
		t.buttons = [];
		if(t.style=='vertical'){
			var _h = 0;
			$(t.items).each(function(){
				_h += (this=='|' || this[0]=='|' || this.title=='|') ? (t.style=='horizontal' ? 2 : 1) : 22;
			});
			_el.height(t.height = _h);
			_el.width(t.width);
		}
		$(t.items).each(function(){
			//t.buttons = [];
			if(this.visible!=false){
				if(this[0]=='|' || this=='|' || this.title=='|')
					t.addSplitter();
				else{
					if(this.html || (this[0]&&this[0].html)){
						t._el.append(this.html||this[0].html);
					}else{
						var btn = t.addButton(this);
						//t.buttons.push(btn);
						if(t.style=='vertical')
							btn.els.c.width(t.width-35-($.browser.msie ? 2 : 0));
					}
				}
			}
		});
		t.on('itemclick', function(tbar, btn, btnId){
			if(t.canHide)
				t.hide();
			if(t.parent)
				t.parent.trigger('itemclick', t.parent, btn, btnId);
		});
		t.on('itemtoggle', function(tbar, btn, btnId){
			if(t.parent)
				t.parent.trigger('itemtoggle', t.parent, btn, btnId);
		});
		mj.menu.superclass.init.call(this);
	},
	addButton : function(config){
		var t = this;
		if(!t.buttons)
			t.buttons = [];
		mj.applyIf(config, {
			renderTo : t.el,
			menuConfig : t.menuConfig,
			parent : t,
			subMenuAlign : t.subMenuAlign,
			drag : t.drag||t.menuConfig.drag||mj.menu.prototype.drag,
			icon : t.icon,
			iconCls : t.iconCls
		});
		var btn = new mj.toolbutton(config);
		t.addRelated(btn);
		t.buttons.push(btn);
		if(t.style=='vertical')
			t._el.height(mj.oLength(t.buttons)*22 + (t.splitterCount>0?t.splitterCount:0));
		return btn;
	},
	addSplitter : function(){
		if(!this.splitterCount)
			this.splitterCount = 0;
		this.splitterCount++;
		mj.NE(this.el, {cls:'mj-menu-splitter',html:mj.insertSpacer()});
	},
	show : function(parentEl, position){
		var t=this, _el=t._el, top=0,left=0,x=0,y=0,pel = parentEl._el, ppos = pel.offset({ border: true, padding: true }), pw = pel.width(), ph = pel.height();
		switch(parentEl.subMenuAlign){
			case 'tl' :
				x = ppos.left;
				y = ppos.top;
				break;
			case 'tr' :
				x = ppos.left+pw;
				y = ppos.top;
				break;
			case 'bl' :
				x = ppos.left;
				y = ppos.top+ph;
				break;
			case 'br' :
				x = ppos.left+pw;
				y = ppos.top+ph-4;
				break;
		}
		t.showAt(x, y);
	},
	showAt : function(x,y){
		var t=this, _el=t._el, top=0,left=0,height=parseInt(_el[0].style.height),width=parseInt(_el[0].style.width);
		switch(t.alignTo){
			case 'tl' :
				left = x;
				top = y;
				break;
			case 'tr' :
				top = y;
				left = x-t.width;
				break;
			case 'bl' :
				left = x;
				top = y-height;
				break;
			case 'br' :
				top = y-height;
				left = x-t.width;
				break;
		}
		_el.css({position: 'absolute', top: top, left: left});
		if(t.parent){
			if(t.parent.parent.activeSub)
				t.parent.parent.activeSub.hide();
			t.parent.parent.activeSub=t;
		}
		var _hc = function(e){
			var p = e.target.parentNode, _p = $(p);
			t._hideCheck.call(t, e);
		};
		mj.bd.bind('mousedown', _hc);
		mj.bd.bind('dragstart', _hc);
		t.on('afterdestroy', function(){
			mj.bd.unbind('mousedown', _hc);
			mj.bd.unbind('dragstart', _hc);
		});
		_el.show();
	},
	_hideCheck:function(e){
		var p = $(e.target).hasParent(this.el);
		if(!p&&this.canHide)
			this.hide();
	},
	hide : function(){
		var t=this;
		if(t.canHide){
			if(t.parent)
				t.parent.parent.activeSub = null;
			t._el.hide();
		}
	},
	clear : function(){
		this.items=[];
		this.buttons=[];
		this._el.empty();
	},
	getItem : function(itemId, _items){
		var _items = _items ? _items : this.items;
		for(var i=0,l=_items.length;i<l;i++){
			var r = false;
			if(_items[i].id == itemId)
				r = _items[i];
			else if(_items[i].items && _items[i].items.length>0)
				r = this.getItem(itemId, _items[i].items);
			if(r) return r;
		}
		return false;
	}
};
mj.extend(mj.menu, mj.component);

mj.toolbutton = function(config){
	mj.toolbutton.superclass.constructor.call(this, config);
};
mj.toolbutton.prototype = {
	componentClass : 'mj.toolbutton',
	subMenuAlign : 'bl',
	type : 'default',
	drag : false,
	disabled:false,
	iconCls : '',
	init : function(){
		var elId = this.id ? ('mj-'+mj.genId()+'-'+this.id) : 'mj-'+mj.genId();
		this.id = this.id ? this.id : elId;
		var t = this, n = mj.NE, el = t.el = t.destroyEl = n(t.renderTo, {cls: 'mj-tool-btn mj-unselectable ',style : t.drag?'position:relative;':'',id:elId, title:mj.translate(t.alt), name:(t.name?t.name:elId)}), _el = t._el = $(el);
		t.els = {
			l : $(n(el, {cls:'mj-tool-btn-left',html:mj.insertSpacer(1,1)})),
			c : $(n(el, {cls:'mj-tool-btn-center'})),
			r : $(n(el, {cls:'mj-tool-btn-right',html:mj.insertSpacer(1,1)}))
		};
		if(t.disabled)
			t.setDisable();
		var style = '';
		if(t.iconCls)
			var iconCls = t.iconCls;
		if(t.icon)
			style += ';background:transparent url('+t.icon+') no-repeat';
		if(style!=='' && t.parent && t.parent.style=='horizontal')
			style += ';width:16px';
		if(t.parent.style=='horizontal' && !t.icon && !iconCls)
			style += 'width:0';
		t.els.ci = $(n(t.els.c, {cls:'mj-tool-btn-center-icon '+iconCls,style:style}));
		if(t.title){
			t.els.ct = $(n(t.els.c, {cls:'mj-tool-btn-center-text', html:mj.translate(t.title)}));
			t.els.ct.attr('unselectable','on');
		}
		if(t.items){
			t.els.csi = $(n(t.els.c, {cls:'mj-tool-btn-center-subicon',html:mj.insertSpacer(1,1)}));
			if(t.parent.style=='horizontal')
				_el.width(_el.width()+16);
			t.subMenu = new mj.menu({
				style : 'vertical',
				width : t.subMenuWidth||mj.menu.prototype.width,
				canHide : true,
				visible : false,
				parent : t,
				items : t.items,
				menuConfig : t.menuConfig,
				alignTo : t.alignTo||t.parent.alignTo||mj.menu.prototype.alingTo,
				subMenuAlign : t.subMenuAlign||t.parent.subMenuAlign||mj.menu.prototype.subMenuAlign,
				isSub : true
			});
			t.addRelated(t.subMenu);
		}
		var stopTimer = function(){
			if(t.subTimer){
				clearTimeout(t.subTimer);
				t.subTimer = null;
			}
		};
		if(t.drag){
			t.dragHandle = new mj.drag({el:_el,moving :false,position:'absolute',proxyEl:{width:30,height:30},parent : mj.bd});	
			t.dragHandle.on('beforedrag', function(_t,e){
				return !t.disabled;
			});
			t.dragHandle.on('beforedrag',function(_t,e){
				var p = this;	
				while(p.parent)
					p = p.parent;
				return p.trigger('beforedrag',_t,e,this);
			},t);
			t.dragHandle.on('dragstart',function(_t,e){
				var p = this;	
				while(p.parent)
					p = p.parent;
				p.trigger('dragstart',_t,e,this);
			},t);
			t.dragHandle.on('dragstop' ,function(_t){
				var p = this;	
				while(p.parent)
					p = p.parent;
				p.trigger('dragstop',_t,this);			
			},t);
		}	
		_el.hover(function(){
			if(!t.disabled){
				_el.addClass('mj-tool-btn-hover');
				if(t.subMenu&&!t.disabled)
					t.subTimer = setTimeout(function(){
						t.showSubMenu();
						stopTimer();
					}, mj.glb.menuDelay);
			}
		},function(){
			if(!t.disabled){
				_el.removeClass('mj-tool-btn-hover');
				stopTimer();
			}
		});
		_el.mouseout(function(){
			_el.removeClass("mj-tool-btn-down");
		});
		_el.mousedown(function(e){
			if(!t.disabled){
				_el.addClass("mj-tool-btn-down");
				if(t.type=='toggle'||t.type=='radio')
					e.stopPropagation();
			}
		});
		_el.mouseup(function(){
			_el.removeClass("mj-tool-btn-down");
		});
		_el.click(function(){
			if(!t.disabled)
				if(t.items)
					t.showSubMenu();
				else{
					t.trigger('click', t, t.id);
					if(t.parent && t.parent.events && t.parent.events.itemclick){
						if(t.parent.canHide && t.type=='default'){
							t.parent.hide();
							t.parent.trigger('itemclick', t.parent, t, t.id);
						}else
							t.parent.trigger('itemtoggle', t.parent, t, t.id);
					}
				}
		});
		t.on('itemclick' ,function(_t, b, bId){
			if(_t.parent && _t.parent.events && _t.parent.events.itemclick)
				_t.parent.trigger('itemclick', _t.parent, b, bId);
		});
		t.on('itemtoggle' ,function(_t, b, bId){
			if(_t.parent && _t.parent.events && _t.parent.events.itemclick)
				_t.parent.trigger('itemtoggle', _t.parent, b, bId);
		});
		if(t.handler)
			t.on('click', t.handler, t.scope);
		if(t.type=='toggle'){
			t.setToggle(t.state);
			t.on('click',t.toggle,t);
		}
		if(t.type=='radio'){
			t._el.addClass('mj-radio');
			if(t.state)
				t.setRadio(t.state);
			t.on('click',t.radio,t);
		}
		if(t._customRights){
			var _customRightsId = t._customRights.split(',');
			_customRightsId = _customRightsId[0];
			if(mj._renderingModule && mj._renderingModule._customRights && mj._renderingModule._customRights.indexOf(_customRightsId)>-1){
				t._el.addClass('mj-invisible');
				t.setHandler(function(){return false;},this);
			}
		}
		mj.toolbutton.superclass.init.call(this);
	},
	radio : function(){
		if(!this.disabled)
			this.setRadio(!this.state);
	},
	setRadio : function(value){
			var rBox = this._el.parent().find('div[NAME="'+$(this).attr('name')+'"]');
			var items = this.parent.items;
			for(var i=0,len=items.length;i<len;i++){
				var elem = items[i];
				if(elem.type&&elem.type=='radio')
					elem.state=false;
					if(this.id==elem.id)
						elem.state=true;
			}
			rBox.removeClass('mj-radio-checked');
			this._el.addClass('mj-radio-checked');
		this.state = true;
	},
	setHandler : function(cb, scope){
		if(this.handler)
			this.mon('click', this.handler);
		this.handler = cb;
		this.scope = scope || this.scope || this;
		this.on('click', this.handler, this.scope);
	},
	setDisable : function(){
		this.disabled=true;
		this._el.removeClass('mj-tool-btn-hover');
		this._el.addClass('mj-item-disabled');
	},
	setEnable : function(){
		this.disabled=false;
		this._el.removeClass('mj-item-disabled');
	},
	toggle : function(){
		this.setToggle(!this.state);
	},
	setAlt : function(alt){
		this.alt = alt;
		this._el.attr('title', alt);
	},
	setTitle : function(text){
		this.title = mj.translate(text);
		this.els.ct.text(this.title);
	},
	setToggle : function(value){
		if(value){
			this._el.removeClass('mj-toggle-unchecked');
			this._el.addClass('mj-toggle-checked');
		}else{
			this._el.removeClass('mj-toggle-checked');
			this._el.addClass('mj-toggle-unchecked');
		}
		this.state = value;
	},
	showSubMenu : function(){
		var t=this, m = t.subMenu;
		if(m)
			m.show(t, t.subMenuAlign);
	},
	hide : function(){
		if(!this.invisible){
			this._el.addClass('mj-invisible');
			this.invisible = true;
		}
	},
	show : function(){
		if(this.invisible){
			this._el.removeClass('mj-invisible');
			this.invisible = false;
		}
	}
};
mj.extend(mj.toolbutton, mj.component);
//!!! myjui/source/menu/contextMenu.js
mj.contextmenu = function(config){
	config.clientArea = config._dontBindParent ? false : $(config.parent ? config.parent : mj.bd);
	config.parent = null;
	mj.contextmenu.superclass.constructor.call(this,config);
};
mj.contextmenu.prototype = {
	componentClass : 'mj.contextmenu',
	style : 'vertical',
	subMenuAlign : 'tr',
	canHide : true,
	visible : false,
	init : function(){
		if(this.clientArea)
			this.bindParent(this.clientArea);
		mj.contextmenu.superclass.init.call(this);
	},
	bindParent : function(p){
		var t = this;
		p = $(p);
		p.bind('contextmenu',function(e){
			t.trigger('show',t, e.pageX, e.pageY);
			t.showAt(e.pageX, e.pageY);
			e.preventDefault();
			e.stopPropagation();
		});
	}
};
mj.extend(mj.contextmenu, mj.menu);
//!!! myjui/source/view/view.js
/** 
 * @fileoverview View nesnesi.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.0.1 16.10.2007
 */
/**
 * View nesnesi.
 * Örnek kullanımı aşağıdaki gibidir:
 * @example
	var a=[];
	a.push({id:'v-1',title:'deneme1',url:'../../images/trigger.gif'});
	a.push({id:'v-2',title:'deneme2',url:'../../images/trigger.gif'});
	a.push({id:'v-3',title:'deneme3',url:'../../images/trigger.gif'});
	a.push({id:'v-4',title:'deneme4',url:'../../images/trigger.gif'});
	var t=['<div class="thumb-wrap" id="{id}">',
	'<div class="thumb"><img src="{url}" title="{title}"></div>',
	'<span class="x-editable">{title}</span></div>']
	var view = new mj.view({				
		renderTo : mj.NE(mj.bd,{tag:'div',id:'view-cnt'}),
		store : new mj.store({data:a}),
		tpl : new mj.template(t),
		selector : 'div.thumb-wrap',
		multiSelect : true
	});
	//view.on('itemclick',function(){mj.log("1");});

 * @class
 * @name mj.view
 * @extends mj.component
 * @param {Object} config View objesini oluşturabilmek için gerekli olan parametreleri içerir. 
 * @config {HTMLElement} renderTo View nesnesinin render edileceği container nesnesi.
 * @config {Object} store View nesnesinin store nesnesi.
 * @config {Object} tpl View nesnesinin template'i.
 * @config {String} selector View nesnesinin selector'ü.Seçilebilen nesneleri select edebilecek selector.
 * @config {Boolean} multiSelect Çoklu seçim yapılabilmesi için gerekli parametre.<i>ctrl</i> ile birlikte çoklu seçime imkan verilmektedir.
	Ön tanımlı değeri:false.
 * @config {String} overClass View elementlerinin üzerinden mouse ile geçerken eklenecek class.
 * @config {String} selectedClass View elementlerinden seçilenleri belirtecek class.
 */
mj.view = function(config){
	mj.view.superclass.constructor.call(this, config);
};
mj.view.prototype = {
	componentClass : 'mj.view',
	/** 
	 * <i>ctrl</i> tuşu ile birlikte çoklu seçim yapışabilmesi isteniyorsa <i>true</i> olması gerekmektedir.Ön tanımlı değeri:false.
	 * @name multiSelect 
	 * @type {Boolean}
	 * @memberOf  mj.view
     */
	multiSelect : false,
	/** 
	 * View elementlerinin üzerinden mouse ile geçerken eklenecek class.
	 * @name overClass 
	 * @type {String}
	 * @memberOf  mj.view
     */
	overClass : 'view-over',
	/** 
	 * View elementlerinden seçilenleri belirtecek class.
	 * @name selectedClass 
	 * @type {String}
	 * @memberOf  mj.view
     */
	selectedClass : 'view-selected',
	items : [],
	selections : [],
	selected : [],
	loadMask : true,
	/**
	 * View nesnesi init fonksiyonu.Store'u load eder ve view'in load'unu çağırır.
	 * @name init
	 * @function
	 * @type {Function}
	 */
	init : function(){
		this.sel=[];
		this.cnt = $(this.renderTo);
		if(this.pbar&&typeof this.pbar.render=='function')
			this.pbar.render({scope:this});
		if(this.loadMask)
			this.mask = new mj.mask({el:this.cnt[0]});
		this.store.on('beforeload', function(){
			if(this.loadMask)
				this.mask.show(50);
		}, this);
		this.store.on('load',function(){
			this.load();
		},this);
		mj.bindResize(this.cnt, this.doLayout, this);
	},
	doLayout : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		$(this.renderTo).height(this.cnt.height()-(this.pbar?25:0));
		$(this.renderTo).trigger('kkresize', {sourceEl:this});
	},
	/**
	 * View'in elementlerini siler.
	 * @name removeAll
	 * @function
	 * @type {Function}
	 */
	removeAll : function(){
		this.clearSelections();
		this.items=[];
		$(this.renderTo).find('div').remove();
	},
	/**
	 * View'in elementlerini load eder.
	 * @name load
	 * @function
	 * @type {Function}
	 */
	load : function(){
		this.removeAll();
		if(this.filter){
			this.store.filter(this.filter.dI,this.filter.v);
			a=this.store.data;
		}else
			var a=this.store.data;
		if(!a)
			this.store.load();
		var tmp;
		for(b in a){
			tmp=a[b];
			if(typeof tmp!='function'){
				var el=this.tpl.apply(tmp);
				//var tplEl=$(mj.NE(this.renderTo,{tag:'div',id:mj.genId('view-el-'),html:el}));
				var tplEl=$(this.renderTo).append(el).find(this.selector+':last').attr('id',mj.genId('view-el-'));
				tmp.el = tplEl;
				if($.browser.msie)
					tplEl.attr('unselectable','on');
				var p={
					id:tplEl.attr('id'),
					el:mj.get(tplEl.attr('id')),
					jEL:tplEl,
					store:a[b],
					storeIndex:parseInt(b)
				};
				this.items.push(p);
				var t=this;
				tplEl.bind('click',{scope:this},function(e){
					var sc=t.view||t;
					sc.onClick(e);
				}).bind('dblclick',{scope:this},function(e){
					var sc=t.view||t;
					sc.onDblClick(e);
				}).bind('contextmenu',{scope:this},function(e){
					var sc=t.view||t;
					sc.onContextMenu(e);
				});
				
		        if(this.overClass){
					tplEl.hover(function(e){
								var sc=t.view||t;
								$(this).addClass(sc.overClass);
								sc.trigger('onItemOver',sc,this,e);
							},
							function(e){
								var sc=t.view||t;
								$(this).removeClass(sc.overClass);
								sc.trigger('onItemOut',sc,this,e);
							}
					);
		        }
			}
		}
		this.trigger('afterload',this);
		if(this.loadMask)
				this.mask.hide();
	},
	/**
	 * Seçilen view elementini geri döndürür.
	 * @name getViewEl
	 * @function
	 * @type {Function}
	 * @param {Object} e Event objesi.
	 * @return {Object} View elementi.
	 */
	getViewEl : function(e){
		return this.items[mj.getIndex(this.items,'id',$(e.target).parents(this.selector).attr('id')||$(e.target).attr('id'))];
	},
	/**
	 * Seçili view elementlerini temziler.
	 * @name clearSelections
	 * @function
	 * @type {Function}
	 */
	clearSelections : function(){
		$(this.renderTo).find(this.selector).removeClass(this.selectedClass);
		this.selections=[];
		this.selected=[];
		this.sel=[];
	},
	/**
	 * View'in onClick fonksiyonu.
	 * @name onClick
	 * @function
	 * @type {Function}
	 * @param {Object} e Event objesi.
	 */
	onClick : function(e){
		var item = this.getViewEl(e);
		this._onSelect(item,e);
		this.trigger('itemclick',this, item, e);
	},
	/**
	 * View'in _onSelect fonksiyonu.
	 * @name _onSelect
	 * @function
	 * @type {Function}
	 * @param {Object} item Seçilen view elementi.
	 * @param {Object} e Event objesi.
	 */
	_onSelect : function(item,e){
		if(e&&(!this.multiSelect||!e.ctrlKey))
			this.clearSelections();
		item.jEL.addClass(this.selectedClass);
		this.selections.push(item);
		var p=this.scope?
			{index:item.storeIndex,id:item.store[this.scope.idField],value:item.store[this.scope.displayField]}
			:{index:item.storeIndex,id:item.id,value:item.store.title};
		if(this.multiSelect){
			this.sel.push(p);
		}else{
			this.sel=p;
		}
		this.selected.push(this.sel);		
	},
	select : function(itemIndex){
		//if(this.selected[0].index!=itemIndex){
			//this.clearSelections();
			var item = this.items[itemIndex];
			if(item)
				this._onSelect(this.items[itemIndex]);
		//}
	},
	/**
	 * View'in onDblClick fonksiyonu.
	 * @name onDblClick
	 * @function
	 * @type {Function}
	 * @param {Object} e Event objesi.
	 */
	onDblClick : function(e){
		this.trigger('dblclick',this);
	},
	/**
	 * View'in onContextMenu fonksiyonu.
	 * @name onContextMenu
	 * @function
	 * @type {Function}
	 * @param {Object} e Event objesi.
	 */
	onContextMenu : function(e){
		e.preventDefault();
		this.trigger('contextmenu',this);
	}
};
mj.extend(mj.view, mj.component);

mj.view2 = function(config){
	mj.view2.superclass.constructor.call(this, config);
};
mj.view2.prototype = {
	componentClass : 'mj.view2',
	items : [],
	multiSelect : false,
	overClass : 'view-over',
	selectedClass : 'view-selected',
	selections : [],
	loadMask : true,
	init : function(){
		var t = this;
		t.cnt = t.renderTo;
		if(t.pbar&&typeof t.pbar.render=='function')
			t.pbar.render({scope:t});
		t.$cnt = $(t.renderTo);
		if(t.loadMask)
			t.mask = new mj.mask({el:t.cnt[0]});
		t.store.on('beforeload', function(){
			if(t.loadMask)
				t.mask.show(50);
		}, t);
		t.store.on('load',function(){
			t._onLoad();
		},t);
		mj.bindResize(t.cnt, t.doRender, t);
	},
	_onClick : function(e){
		this._onSelect(this.getViewEl(e),e);
		this.trigger('itemclick',this);
	},
	_onContextMenu : function(e){
		e.preventDefault();
		this.trigger('contextmenu',this);
	},
	_onDblClick : function(e){
		this.trigger('dblclick',this);
	},	
	_onLoad : function(){
		this.removeAll();
		var tmp, data;
		//combo filter kontrolü burada olacak
		data = this.store.data;
		for(var i=0,len=data.length;i<len;i++){
			tmp = data[i];
			this.add(tmp,i);
		}
		this.trigger('afterload',this);
		if(this.loadMask)
			this.mask.hide();
	},
	_onSelect : function(item,e){
		if(e&&(!this.multiSelect||!e.ctrlKey))
			this.clearAll();
		item.$el.addClass(this.selectedClass);
		var p=this.scope?item.store[this.scope.displayField]:item.store.title;
		item.value = p;
		this.selections.push(item);
	},
	clear : function(index){
		var item = this.get(index);
		if(item)
			item.$el.removeClass(this.selectedClass);
	},
	clearAll : function(){
		var sel = this.selections;
		for(var i=sel.length-1;i>-1;--i)
			this.clear(sel[i].index);
		this.selections=[];
	},
	doRender : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		this.$cnt.height(this.cnt.height()-(this.pbar?25:0));
		this.$cnt.trigger('kkresize', {sourceEl:this});
	},
	add : function(cfg,index){
		var t = this;
		var el = t.tpl.apply(cfg);
		var tplEl = t.$cnt.append(el).find(t.selector+':last').attr('id',mj.genId('view-el-'));
		if($.browser.msie)
			tplEl.attr('unselectable','on');
		var p={
			id:tplEl.attr('id'),
			el:mj.get(tplEl.attr('id')),
			$el:tplEl,
			store:cfg,
			index:index
		};
		t.items.push(p);
		tplEl.bind('click',{scope:t},function(e){
			var sc=t.view||t;
			sc._onClick(e);
		}).bind('dblclick',{scope:t},function(e){
			var sc=t.view||t;
			sc._onDblClick(e);
		}).bind('contextmenu',{scope:t},function(e){
			var sc=t.view||t;
			sc._onContextMenu(e);
		});		
		if(t.overClass){
			tplEl.hover(function(e){
						var sc=t.view||t;
						$(this).addClass(sc.overClass);
						sc.trigger('onItemOver',sc,this,e);
					},
					function(e){
						var sc=t.view||t;
						$(this).removeClass(sc.overClass);
						sc.trigger('onItemOut',sc,this,e);
					}
			);
		}
	},
	get : function(index){
		return this.items[index];
	},
	getViewEl : function(e){
		return this.get(mj.getIndex(this.items,'id',$(e.target).parents(this.selector).attr('id')||$(e.target).attr('id')));
	},
	load : function(){
		this.store.load();
	},
	remove : function(index){
		var item = this.get(index);
		if(item){
			item.$el.remove();
			this.items.splice(index,1);
		}
	},
	removeAll : function(){
		this.clearAll();
		var item = this.items;
		for(var i=item.length-1;i>-1;--i)
			this.remove(i);
	},
	select : function(index){
		var item = this.get(index);
		if(item)
			this._onSelect(item);
	}
};
mj.extend(mj.view2, mj.component);
//!!! myjui/source/form/form.js
mj.form = function(config){
	mj.form.superclass.constructor.call(this, config);
};

mj.form.prototype = {
	componentClass : 'mj.form',
	method : 'POST',
	buttonPos : 'bottom',
	hasUpload : false,
	encoded : true,
	fieldSets : false,
	buttons : false,
	buttonAlign : 'right',
	init : function(){
		var t=this,r=t.renderTo,_r=t._r=$(r);
		t.fieldSets = [];
		var fc = {
			tag : 'form',
			method : t.method,
			cls : 'mj-form'
		};
		if(t.hasUpload)
			fc.enctype = 'multipart/form-data';
		
		if(t.buttonPos=='bottom'){
			t.cnt = $(mj.NE(_r,{tag:'div',style:'overflow:auto;height:'+(_r[0].style.height+';')}));
			t.buttonContainer = $(mj.NE(_r, {tag:'div',cls:'mj-form-buttons-container'}));
		}else if(t.buttonPos=='top'){
			t.buttonContainer = $(mj.NE(_r, {tag:'div',cls:'mj-form-buttons-container'}));
			t.cnt = $(mj.NE(_r,{tag:'div',style:'overflow:auto;height:'+(_r[0].style.height+';')}));
		}
		if(!t.buttonPos){
			t.cnt = $(mj.NE(_r,{tag:'div',style:'overflow:auto;height:'+(_r[0].style.height+';')}));
		}
		if(t.buttons){
			t.buttonConfigs = t.buttons;
			t.buttons = [];
			$(t.buttonConfigs).each(function(){
				t.addButton.call(t,this);
			});
		}
		
		t.form = $(mj.NE(t.cnt, fc));
		if(!t.items)
			t.items = [];
		else
			for(var i=0,l=t.items.length;i<l;i++){
				var item = t.items[i];
				item.form = t;
				t.on('render', function(form){
					form.renderItem.call(form, this);
					form.addRelated(this);
				}, item);		
				if(item.componentClass=='mj.form.fileInput')
					t.hasUpload = true;
			}
		t.cachedItems = {};
		if(t.buttons)
			t.cnt.height(_r.height()-t.buttonContainer.height());
			// t.buttonContainer = $(mj.NE(_r, {tag:'div',cls:'mj-form-buttons-container'}));
			// $(t.buttons).each(function(){
				// t.addButton.call(t,this);
			// });
		// }
		mj.form.superclass.init.call(this);
		t.trigger('render' , t, t);
		mj.bindResize(t.cnt, t.doForm, t);
		this.modifiedFields = [];
		if(this.store)
			this.store.on('load',this.loadForm,this);
	},
	doForm : function(){
		this.cnt.height(this.renderTo.height()-(this.buttons?this.buttonContainer.height():0));
	},
	addButton : function(config){
		var t=this;
		if(!t.buttons)
			t.buttons = [];
		var b = config;
		if(!b.componentClass){
			mj.applyIf(b, {renderTo:mj.NE(t.buttonContainer)});
			b = new mj.button(b);
		}
		b.window = t;
		t.buttons.push(b);
	},
	renderItem : function(item,fieldSetIndex){
		if(typeof fieldSetIndex=='undefined')
			item.renderTo = this.form;
		else{
			if(typeof fieldSetIndex=='string'){
				fieldSetIndex = mj.getIndex(this.fieldSets,'id',fieldSetIndex);
				if(fieldSetIndex==-1)
					return;
			}
			item.renderTo = this.fieldSets[fieldSetIndex].innerEl;
		}
		if(item.right){
			var t = this, i = t.items.indexOf(item);
			if(i>0&&typeof t.items[i-1]!='undefined' && t.items[i-1]._el)
				t.items[i-1]._el.css('float','left');
			item.itemStyle +='clear:none;float:left;margin-left:5px;';
		}
		item.render.call(item,this);
	},
	/*
	form.field'dan türetilmiş herhangi bir form elemanı(Ör: add(new mj.form.textField(...)); )
	*/
	add : function(item,fieldSetIndex){
		if(item.componentClass=='mj.form.fileInput'){
			this.hasUpload = true;
		}
		if(item.right){
			var len = this.items.length;
			if(len>0&&typeof this.items[len-1]!='undefined')
				this.items[len-1]._el.css('float','left');
			item.itemStyle +='clear:none;float:left;margin-left:5px;';
		}
		this.renderItem(item,fieldSetIndex);
		item.form = this;
		this.addRelated(item);
		if(typeof fieldSetIndex == 'string')
			this.items[this.items.getIndex('id',fieldSetIndex)].items.push(item);
		if(typeof fieldSetIndex == 'number')
			this.fieldSets[fieldSetIndex].items.push(item);
		this.items.push(item);
		this.cachedItems[(item.componentClass == 'mj.form.checkBox'?item.id:item.dataIndex)] = item;
	},
	/*
	form.items içindeki elemanlardan name==fieldName olanı / fieldName sayısal verilirse items[fieldName] getirir
	*/
	getField : function(fieldName){
		if(this.cachedItems[fieldName])
			return this.cachedItems[fieldName];
		if(!isNaN(fieldName))
			return this.items[fieldName];
		for(var i=0,l=this.items.length;i<l;i++)
			if(this.items[i].name==fieldName||this.items[i].dataIndex==fieldName){
				this.cachedItems[fieldName] = this.items[i];
				return this.items[i];
			}
	},
	/*
	fN verilmezse tüm elemanların değerleri gelir
	fN string olarak verilirse istenen elemanın değeri gelir
	fN dizi olarak verilirse dizi içindeki elemanların değerleri gelir
	*/
	getValue : function(fieldName, raw){
		var r = {}, t = this, fn = '', p = -1, ofn = '', rw = false;
		if(!fieldName){
			fieldName = [];
			if(!t.items || t.items.length==0)
				return r;
			var item;
			for(var i=0,l=t.items.length;i<l;i++){
				item = t.items[i];
				if(item.componentClass!='mj.form.fieldSet'&&item.componentClass!='mj.panel'&&(item.componentClass=='mj.form.triggerField'||item.componentClass=='mj.form.dateField'||!item.readOnly||item.readOnlyGetValue)){
					fieldName.push(item.name ? item.name : (item.dataIndex ? item.dataIndex : i));
				}
				if(item.componentClass=='mj.form.triggerField'||item.componentClass=='mj.form.combo'){
					fieldName.push((item.name ? item.name : (item.dataIndex ? item.dataIndex : i))+'_lookup_');
				}
			}
		}
		if(typeof fieldName=='string')
			fieldName = [fieldName];
		for(var i=0,l=fieldName.length;i<l;i++){
			fn = fieldName[i];
			p = fn.indexOf('_lookup_');
			ofn = p == -1?fn:fn.substr(0,p);
			rw = raw || p>-1;
			if(rw)
				r[fn] = t.getField.call(t, ofn).getElValue();
			else
				r[fn] = t.getField.call(t, ofn).getValue();
		};
		return r;
	},
	/*
	fN parametresi getValue gibi.
	değer olarak dom.value getirir
	*/
	getElValue  : function(fieldName){
		return this.getValue(fieldName, true);
	},
	/*
	nV = {itemName : itemValue, itemName2 : itemValue2, ...}
	*/
	setValue : function(nameValue, raw){
		for(var x in nameValue)
			if(typeof nameValue[x] != 'function'){
				var f;
				if(f = this.getField(x)){
					if(typeof nameValue[x+'_source'] != 'undefined')
						f.setValue(nameValue[x+'_source']);
					else
						if(raw)
							f.setElValue(nameValue[x]);
						else
							f.setValue(nameValue[x]);
					if((f.componentClass == 'mj.form.triggerField' ||f.componentClass == 'mj.form.combo') && typeof nameValue[x+'_lookup_'] != 'undefined'){
						f.setValue(nameValue[x+'_lookup_']);
						f.setValue(nameValue[x],true);
					}
				}
			}
	},
	setElValue : function(nameValue){
		this.setValue(nameValue, true);
	},
	clear : function(){
		$(this.items).each(function(){
			if(typeof this.clear=='function')
				this.clear();
			if(typeof this.clearInvalid=='function')
				this.clearInvalid();
		});
	},
	removeAll : function(){
		this.clear();
		this.form.empty();
		var el;
		while(this.items.length>0){
			el = this.items[this.items.length-1];
			el.destroy();
			this.items.remove(el);
		}
		//this.items = [];
		this.cachedItems = {};
		this.modified = false;
		this.modifiedFields = [];
	},
	/**
	 * Formu submit eder.
	 * @name submit
	 * @function
	 * @type {Function}
	 * @param {Object} opt Parametreler
	 * @config {String} url Gönderilecek sayfa
	 * @config {String} method İstek yöntemi(POST/GET)
	 * @config {Boolean} encoded Kodlanmış(default:true)
	 * @config {Function} success 
	 * @config {Function} error 
	 */
	submit : function(opt){
		var S = opt.success;
		var F = opt.failure;
		if(S)
			delete opt.success;
		if(F)
			delete opt.failure;
		var defOpt = {
			url : this.url,
			type : this.method,
			encoded : this.encoded,
			success : function(data){
				//giyyin upload sorunu için eklendi. kaldırılabilir
				if(data.indexOf('<b>Fatal error</b>')>-1)
					F({msg:'Beklenmeyen bir hata oluştu!'});
				else{
					var d = eval('('+data+')');
					if(d)
						if(d.success&&S)
							S(d);
						if(!d.success&&F)
							F(d);
				}
			}
		};
		if(typeof opt=='string'){
			defOpt.url = opt;
			opt = defOpt;
		}
		mj.applyIf(opt, defOpt);
		//var data = opt.encoded ? mj.escape(this.getValue().toJSONString()) : this.getValue().toJSONString();
		var data = opt.encoded ? mj.escape(JSON.stringify(this.getValue())) : JSON.stringify(this.getValue());
		if(!opt.clearData)
			opt.data = 'data='+data;
		if(opt.params)
			for(var x in opt.params)
				if(typeof opt.params[x]!='function')
					opt.data += '&'+x+'='+opt.params[x];
		if(opt.url!=''){
			if(!this.hasUpload)
				$.ajax(opt);
			else{
				/*Alttaki bölüm jqForm plugin'inden alınmıştır
				URL : http://www.malsup.com/jquery/form/#getting-started
				SOURCE : http://jqueryjs.googlecode.com/svn/trunk/plugins/form/jquery.form.js
				*/
				var $form=$(this.form),options = opt;
				// private function for handling file uploads (hat tip to YAHOO!)
			    function fileUpload() {
			        var form = $form[0];
			        var opts = $.extend({}, $.ajaxSettings, options);

			        var id = 'jqFormIO' + mj.genId();//$.fn.ajaxSubmit.counter++;
			        var $io = $('<iframe id="' + id + '" name="' + id + '" />');
			        var io = $io[0];
			        var op8 = $.browser.opera && window.opera.version() < 9;
			        if ($.browser.msie || op8) io.src = 'javascript:false;document.write("");';
			        $io.css({ position: 'absolute', top: '-1000px', left: '-1000px' });

			        var xhr = { // mock object
			            responseText: null,
			            responseXML: null,
			            status: 0,
			            statusText: 'n/a',
			            getAllResponseHeaders: function() {},
			            getResponseHeader: function() {},
			            setRequestHeader: function() {}
			        };

			        var g = opts.global;
			        // trigger ajax global events so that activity/block indicators work like normal
			        if (g && ! $.active++) $.event.trigger("ajaxStart");
			        if (g) $.event.trigger("ajaxSend", [xhr, opts]);

			        var cbInvoked = 0;
			        var timedOut = 0;

			        // take a breath so that pending repaints get some cpu time before the upload starts
			        setTimeout(function() {
			            $io.appendTo('body');
			            // jQuery's event binding doesn't work for iframe events in IE
			            io.attachEvent ? io.attachEvent('onload', cb) : io.addEventListener('load', cb, false);

			            // make sure form attrs are set
			            var encAttr = form.encoding ? 'encoding' : 'enctype';
			            var t = $form.attr('target');
			            $form.attr({
			                target:   id,
			                method:  'POST',
			                action:   opts.url
			            });
			            form[encAttr] = 'multipart/form-data';

			            // support timout
			            if (opts.timeout)
			                setTimeout(function() { timedOut = true; cb(); }, opts.timeout);

			            form.submit();
			            $form.attr('target', t); // reset target
			        }, 10);

			        function cb() {
			            if (cbInvoked++) return;

			            io.detachEvent ? io.detachEvent('onload', cb) : io.removeEventListener('load', cb, false);

			            var ok = true;
			            try {
			                if (timedOut) throw 'timeout';
			                // extract the server response from the iframe
			                var data, doc;
			                doc = io.contentWindow ? io.contentWindow.document : io.contentDocument ? io.contentDocument : io.document;
			                xhr.responseText = doc.body ? doc.body.innerHTML : null;
			                xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;

			                if (opts.dataType == 'json' || opts.dataType == 'script') {
			                    var ta = doc.getElementsByTagName('textarea')[0];
			                    data = ta ? ta.value : xhr.responseText;
			                    if (opts.dataType == 'json')
			                        eval("data = " + data);
			                    else
			                        $.globalEval(data);
			                }
			                else if (opts.dataType == 'xml') {
			                    data = xhr.responseXML;
			                    if (!data && xhr.responseText != null)
			                        data = toXml(xhr.responseText);
			                }
			                else {
			                    data = xhr.responseText;
			                }
			            }
			            catch(e){
			                ok = false;
			                $.handleError(opts, xhr, 'error', e);
			            }

			            // ordering of these callbacks/triggers is odd, but that's how $.ajax does it
			            if (ok) {
			                opts.success(data, 'success');
			                if (g) $.event.trigger("ajaxSuccess", [xhr, opts]);
			            }
			            if (g) $.event.trigger("ajaxComplete", [xhr, opts]);
			            if (g && ! --$.active) $.event.trigger("ajaxStop");
			            if (opts.complete) opts.complete(xhr, ok ? 'success' : 'error');

			            // clean up
			            setTimeout(function() {
			                $io.remove();
			                xhr.responseXML = null;
			            }, 100);
			        };

			        function toXml(s, doc) {
			            if (window.ActiveXObject) {
			                doc = new ActiveXObject('Microsoft.XMLDOM');
			                doc.async = 'false';
			                doc.loadXML(s);
			            }
			            else
			                doc = (new DOMParser()).parseFromString(s, 'text/xml');
			            return (doc && doc.documentElement && doc.documentElement.tagName != 'parsererror') ? doc : null;
			        };
			    };
				fileUpload();
			}
		}
	},
	loadForm : function(){
		if(this.store && this.store.recordCount>0){
			var data = this.store.data;
			for(var i=0;i<this.items.length;i++){
				var item=this.items[i];
				if(item.dataIndex){
					//if(typeof item.setCmbValue == 'function')
					//	item.setCmbValue(data[item.dataIndex]);
					//else
					item.setValue(data[item.dataIndex]);
					if(item.componentClass == 'mj.form.triggerField'||item.componentClass == 'mj.form.combo'){
						if(data[item.dataIndex+'_lookup_']){
							item.setValue(data[item.dataIndex+'_lookup_']);
						}
						item.setValue(data[item.dataIndex],true);
					}
					if(item.componentClass == 'mj.form.combo' && item.sameValueDisplay)
						item.setElValue(data[item.dataIndex]);
				}
			}
		}
	},
	load : function(){
		this.store.load();
	},
	startLoad : function(){
		this.loading = true;
		this.modified = false;
		this.modifiedFields = [];
	},
	finishLoad : function(){
		this.loading = false;
	},
	setModified : function(field){
		this.modified = true;
		if(this.modifiedFields.indexOf(field)==-1)
			this.modifiedFields.push(field);
	}
};

mj.extend(mj.form, mj.component);
//!!! myjui/source/form/field.js
mj.form.field = function(config){
	mj.form.field.superclass.constructor.call(this,config);
};
mj.form.field.prototype = {
	componentClass : 'mj.form.field',
	labelWidth : '75px',
	labelAlign : 'left',
	itemCls : 'mj-form-item',
	titleCls : 'mj-unselectable mj-form-caption',
	inputCls : 'mj-form-input',
	hidden : false,
	readOnly : false,
	readOnlyGetValue : false,
	disabled : false,
	left : false,
	top : false,
	type : 'text',
	tag : 'input',
	labelPos : 'left',
	itemStyle : '',
	placeHolder : '',
	emptyValue : null,
	initValue : false,
	render : function(){
		var t = this, n = mj.NE, v='';
		if(t.hidden)
			t.itemCls += ' mj-invisible';
		if(t.left||t.top){
			t.left = typeof t.left != 'undefined' ? (isNaN(t.left) ? t.left : (t.left+'px')) : '';
			t.top = typeof t.top != 'undefined' ? (isNaN(t.top) ? t.top : (t.top+'px')) : '';
			v = 'position:absolute;left:'+t.left+';top:'+t.top;
		}
		var el = t.destroyEl = t.el = n(t.renderTo, {cls:t.itemCls,style:v+';'+t.itemStyle}), _el = t._el = $(el);
		if(t.title)
			t.titleEl = n(el, {html:this.title, cls:t.titleCls+(t.labelPos=='top'?' '+t.titleCls+'-top':''), style:'width:'+t.labelWidth+';text-align:'+t.labelAlign});
		var _w = t.width?('width:'+t.width + (isNaN(t.width)?'':'px;')):'';
		t.input = $(n(el, {tag:t.tag,id:(t.id||mj.genId('mj-form-item-')),style:''+(t.height?'height:'+t.height+'px;':'')+_w,type:t.type,cls:t.inputCls
			,MAXLENGTH:t.maxLength,readonly:t.readOnly,disabled:t.disabled,name:t.name,placeHolder:t.placeHolder,accept:(t.accept?t.accept:'')}));
		//if(t.title && t.labelPos == 'right')
		//	t.titleEl = n(el, {tag:'label',html:this.title, cls:t.titleCls, style:'width:'+t.labelWidth+';text-align:'+t.labelAlign});
		t.input.focus(function(){
			mj._activeFormElement = t;
			t.trigger('focus', t);
		});
		t.input.blur(function(){
			t.trigger('blur', t, t);
		});
		t.on('blur', function(){
			t._onBlur.call(t);
		});
		t.input.keyup(function(e){
			t.trigger('change', t, e);
		});
		t.input.keypress(function(e){
			t.trigger('keypress', t, e);
		});
		if(t._customRights){
			var _customRightsId = t._customRights.split(',');
			_customRightsId = _customRightsId[0];
			if(mj._renderingModule && mj._renderingModule._customRights && mj._renderingModule._customRights.indexOf(_customRightsId)>-1){
				t._el.addClass('mj-invisible');
			}
		}
		if(t.initValue)
			t.value = t.initValue;
		t.trigger('render', t);
	},
	setTitle : function(text){
		if(this.titleEl){
			this.title = text;
			$(this.titleEl).text(this.title);
		}
	},
	getValue : function(raw){
		if(raw)
			return this.input[0].value;
		else
			return this.value;//return this.value!==''?this.value:null;
	},
	getElValue : function(){
		return this.getValue(true);
	},
	formatValue : function(value){
		return value;
	},
	setValue : function(val){
		//val = this.readOnly && val == '' ? this.emptyValue : val;
		if(this.validate(val)!==false){
			this.value = val;
			this.setElValue(this.formatValue(val));
			if(this.form && !this.form.loading)
				this.form.setModified(this);
		}
	},
	setElValue : function(val){
		this.input[0].value = val;
	},
	clear : function(){
		this.setValue('');
	},
	validate : function(val){
	},
	clearValidate : function(){
		this.orjValidate = this.validate;
		this.validate = function(){return true;};
	},
	setOldValidate : function(){
		this.validate = this.orjValidate;
		this.orjValidate = null;
	},
	clearInvalid : function(){
		if(this.input){
			this.input.removeClass('mj-text-input-invalid');
			this.invalid=false;
		}
	},
	markInvalid : function(msg){
		if(this.input){
			this.input.addClass('mj-text-input-invalid');
			this.input.attr('title', msg);
			this.invalid=true;
			this.trigger('invalid',this,msg);
		}
	},
	setPos : function(x, y){
		this._el.css({
			position : 'absolute',
			left : x,
			top : y
		});
	},
	setWidth : function(width){
		this._el.width(width);
		if(this.title)
			width-=this.labelWidth;
		this.input.width(width-($.browser.msie?6:5));
	},
	_onBlur : function(){
		if(!this.readOnly){
			this.setValue(this.getElValue());
			if(mj._activeFormElement==this)
				mj._activeFormElement = null;
		}
	},
	_onChange : function(){
		if(!this.readOnly)
			this.setValue(this.getElValue());
	},
	setDisable : function(){
		var el = this.input;
		el.attr('readonly', 'true');
		el.attr('disabled', 'true');
		el.addClass('mj-item-disabled');
		this.disabled = true;
	},
	setEnable : function(){
		var el = this.input;
		el.attr('readonly', 'false');
		el.attr('disabled', 'false');
		el.removeAttr('readonly');
		el.removeAttr('disabled');
		el.removeClass('mj-item-disabled');
		this.disabled = false;
	}
};
mj.extend(mj.form.field, mj.component);
//!!! myjui/source/form/textField.js
mj.form.textField = function(config){
	mj.form.textField.superclass.constructor.call(this,config);
};
mj.form.textField.prototype = {
	componentClass : 'mj.form.textField',
	inputCls : 'mj-text-editor',
	init : function(){
		var t = this;
		t.on('render', t._onRender, t);	
		if(t.renderTo)
			t.render();
		//t.on('render', t._onRender, t);	
		mj.form.textField.superclass.init.call(this);
	},
	_onRender : function(){
		var t=this;
		if(t.width)
			t.input.width(t.width);
		t.input.focus(function(){
			t.input.addClass('focus');
			//t.trigger('focus', t);
		});
		t.input.blur(function(){
			t.input.removeClass('focus');
			//t.trigger('blur', t);
		});
		if(t.mask)
			t.input.mask(t.mask);
	},
	focus : function(){
		this.input.focus();
	}
};
mj.extend(mj.form.textField, mj.form.field);
//!!! myjui/source/form/textArea.js
mj.initEditor = function(id, config){
	if(config && config.style=='small'){
		var small = new WYSIWYG.Settings();
		small.ImagesDir = mj.glb.openwysiwyg+"images/";
		small.PopupsDir = mj.glb.openwysiwyg+"popups/";
		small.Width = "355px";
		small.Height = "100px";
		small.DefaultStyle = "font-family: Tahoma, helvetica, arial, sans-serif;font-size:11px;";
		small.Toolbar[0] = new Array("font", "fontsize", "bold", "italic", "underline","unorderedlist","orderedlist","outdent", "indent","inserttable","viewSource"); // small setup for toolbar 1
		small.Toolbar[1] = ""; // disable toolbar 2
		small.StatusBarEnabled = false;
		var settings = small;
	}else{
		var full = new WYSIWYG.Settings();
		full.ImagesDir = mj.glb.openwysiwyg+"images/";
		full.PopupsDir = mj.glb.openwysiwyg+"popups/";
		//::full.CSSFile = "../res/openwysiwyg/styles/wysiwyg.css";
		full.Width = "410px";
		full.Height = "200px";
		// customize toolbar buttons
		full.addToolbarElement("font", 3, 1); 
		full.addToolbarElement("fontsize", 3, 2);
		// openImageLibrary addon implementation
		full.ImagePopupFile = mj.glb.openwysiwyg+"addons/imagelibrary/insert_image.php";
		full.ImagePopupWidth = 600;
		full.ImagePopupHeight = 245;

		var settings = full;
	}
	if(config && config.toolbar){
		if(config.toolbar[0])
			settings.Toolbar[0] = settings.Toolbar[0].concat(config.toolbar[0]);
		if(config.toolbar[1])
			settings.Toolbar[1] = settings.Toolbar[1].concat(config.toolbar[1]);
	}
	mj.apply(settings, config);
	WYSIWYG.setSettings(id, settings);
	//::WYSIWYG_Core.includeCSS(WYSIWYG.config[id].CSSFile);
	WYSIWYG._generate(id, settings);
};

mj.form.textArea = function(config){
	mj.form.textArea.superclass.constructor.call(this, config);
};

mj.form.textArea.prototype = {
	componentClass : 'mj.form.textArea',
	inputCls : 'mj-text-area',
	type : 'textarea',
	tag : 'textArea',
	editor : false,
	init : function(){
		var t = this;
		t.on('render', t._onRender, t);
		if(t.renderTo)
			t.render();
		mj.form.textArea.superclass.init.call(this);
	},
	_onRender : function(){
		if(this.editor)
			mj.initEditor(this.id, this.config);
		//mj.form.textArea.superclass.init.call(this);
	},
	/*getEditorValue : function(raw){
		if(!this.editor) return false;
		var val=this.input.next().find('iframe:first')[0].contentDocument.body.innerHTML
		this.setValue(val)
		return this.getValue(raw);
	},*/
	getValue : function(raw){
		if(this.editor){
			var val=$('div#'+this.input[0].id+'_1 iframe')[0].contentWindow.document.body.innerHTML;
			this.setValue(val);
		}
		if(raw)
			return this.input[0].value;
		else
			return this.value;
	},
	setValue : function(val){
		if(this.validate(val)!==false){
			this.value = val;
			this.setElValue(this.formatValue(val));
			if(this.editor)
				$('div#'+this.input[0].id+'_1 iframe')[0].contentWindow.document.body.innerHTML=val;
		}
	}
};

mj.extend(mj.form.textArea, mj.form.field);
//!!! myjui/source/form/textAreaTMCE2.js
mj.initEditorTMCE = function(input, config, handle){
	var dconfig = {
		selector: "div.edit",
		language : "tr_TR",
		theme: "modern",
		menubar: false,
		plugins: [
			["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker"],
			["searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking"],
			["save table contextmenu directionality emoticons paste textcolor importcss"]
		],
		add_unload_trigger: false,
		schema: "html5",
		toolbar: "undo redo | cut copy paste pastetext | styleselect | fontselect | fontsizeselect | bold italic underline strikethrough superscript subscript | formats | removeformat | alignleft aligncenter alignright alignjustify | forecolor backcolor | table | bullist numlist outdent indent | anchor link image media hr | visualblocks | print preview code fullscreen",
		statusbar: false,
		image_advtab: true,
		content_css: mj.glb.editor_css,
		importcss_append: true,
		external_filemanager_path:location.pathname+"libs/filemanager/",
		filemanager_title:mj.lng.objects.browseImage.browse,
		external_plugins: { "filemanager" : location.pathname+"libs/filemanager/plugin.min.js"}
		//file_browser_callback : myFileBrowser
		,setup : function(ed) {
			if(handle){
				handle.editor = ed;
				// ed.onInit.add(function(ed) {
					// ed.setContent(handle.input.html());
					// tinyMCE.execCommand('mceRepaint');
				// });
				if(handle.form){
					if(handle.form.bindToWindow){
						handle.form.bindToWindow.on('beforeclose', function(){
							//handle.editor.focus();
							//handle.editor.remove();
							//handle.input[0].style.display = '';
							//handle.input.remove();
							tinyMCE.execCommand('mceRemoveControl', false, handle.id);
						});
					}
				}
			}
		}
	};
	if(!config)
		config = dconfig;
	config = mj.apply(dconfig, config);

	tinymce.init(config);
};
/*
function myFileBrowser(field_name, url, type, win){
	if(win.document)
		window.__backupWin = win;
	if(!window.myFM){
		window.myFM = new mj.fileManager({
			sc : this,
			richEditor : false,
			url : mj.glb.functionsPath,
			viewPath : mj.glb.viewPath,
			root : mj.glb.uploadPath,
			node : mj.glb.uploadFolder,
			ft : '*',
			cb : function(fileUrl,fm){
				if(!win.document && window.__backupWin)
					win = window.__backupWin;
				fileUrl = mj.glb.viewPath+fileUrl;
				win.document.getElementById(field_name).value = fileUrl;
				// are we an image browser
				if (typeof(win.ImageDialog) != "undefined") {
					// we are, so update image dimensions...
					if (win.ImageDialog.getImageData)
						win.ImageDialog.getImageData();

					// ... and preview if necessary
					if (win.ImageDialog.showPreviewImage)
						win.ImageDialog.showPreviewImage(fileUrl);
				}
				
				
				fm.win.close();
			}
		});
	}else
		window.myFM.sc.fm.win.show()
    window.myFM.sc.fm.win._el.css('z-index', 999999);
    return false;
}
*/
mj.form.textAreaTMCE2 = function(config){
	mj.form.textAreaTMCE2.superclass.constructor.call(this, config);
};

mj.form.textAreaTMCE2.prototype = {
	componentClass : 'mj.form.textAreaTMCE2',
	inputCls : 'mj-text-area',
	titleCls : 'mj-form-caption mj-unselectable mceEditor-title',
	type : 'textarea',
	tag : 'textArea',
	editor : false,
	init : function(){
		var t = this;
		t.id = mj.genId(t.id);
		t.on('render', t._onRender, t);
		if(t.renderTo)
			t.render();
		mj.form.textAreaTMCE2.superclass.init.call(this);
	},
	_onRender : function(){
		if(this.editor){
			mj.NE(this.el, {style:'float:left;'});
			this.config.selector = '#'+this.id;
			mj.initEditorTMCE(this.input, this.config, this);
			window.tt = this;
		}
	},
	destroy : function(){
		// this.input[0].innerHTML = this.editor.getContent();
		// this.editor.remove();
		// this.input[0].style.display = '';
		mj.form.textAreaTMCE2.superclass.destroy.call(this);
	},
	getValue : function(raw){
		//return tinymce.getInstanceById(this.id).getContent();
		return this.editor.getContent();
	},
	setValue : function(val){
		if(this.validate(val)!==false){
			if(!val)
				val = '';
			this.value = val;
			this.setElValue(this.formatValue(val));
			//var ed = tinymce.getInstanceById(this.id);
			var ed = this.editor;
			if(ed)
				ed.setContent(val);
		}
	}
};

mj.extend(mj.form.textAreaTMCE2, mj.form.field);
//!!! myjui/source/form/regExField.js
mj.form.regExField = function(config){
	mj.apply(this, {
	    invalidText : mj.lng.objects.regExField.invalidText
	});
	mj.form.regExField.superclass.constructor.call(this,config);
};
mj.form.regExField.prototype = {
	componentClass : 'mj.form.regExField',
	regEx : false,
	init : function(){
		var t = this;
		t.on('render', t._onRender, t);	
		if(t.renderTo)
			t.render();
		t.on('invalid', function(a,b,c,d){
			new mj.message({
				title : mj.lng.glb.info,
				msg : b,
				modal : true
			});
		}, t);	
		mj.form.regExField.superclass.init.call(this);
	},
	validate : function(val){
		this.clearInvalid();
		var ret = true;
		if(val!=''&&val!=null){
			ret = this.regEx.test(val);
			if(!ret)
				this.markInvalid(this.invalidText);
		}
		return ret;
	}
};
mj.extend(mj.form.regExField, mj.form.textField);
//!!! myjui/source/form/timeField.js
mj.form.timeField = function(config){
	mj.form.timeField.superclass.constructor.call(this,config);
	mj.apply(this, {
	    invalidText : mj.lng.objects.timeField.invalidText
	});
};
mj.form.timeField.prototype = {
	componentClass : 'mj.form.timeField',
	regEx : /^([01]\d|2[0-3]):([0-5][0-9])$/,
	invalidText : false,
	placeHolder : 'ss:dd',
	init : function(){
		this.invalidText = this.invalidText==false?mj.lng.objects.timeField.invalidText:this.invalidText;
		mj.form.timeField.superclass.init.call(this);
		this.on('change',this.checkPoint);
		this.on('keypress',this.checkPoint);
	},
	checkPoint : function(t,e){
		if(parseInt(e.keyCode)==mj.keys.DELETE){
			t.setValue('');
			e.preventDefault();
			return;
		}
		if(parseInt(e.keyCode)!=mj.keys.BACKSPACE){
			var v = t.getElValue();
			if(v.toString().length == 5) {
				e.preventDefault();
				return;
			}
			if(v.toString().length == 2)
				t.setElValue(v.toString()+':');
		}
	}
};
mj.extend(mj.form.timeField, mj.form.regExField);

mj.form.timeField2 = function(config){
	mj.form.timeField2.superclass.constructor.call(this,config);
	mj.apply(this, {
	    invalidText : mj.lng.objects.timeField2.invalidText
	});
};
mj.form.timeField2.prototype = {
	componentClass : 'mj.form.timeField2',
	regEx : /^([01]\d|2[0-3]):([0-5][0-9]):([0-5][0-9])$/,
	invalidText : false,
	placeHolder : 'ss:dd:ss',
	init : function(){
		this.invalidText = this.invalidText==false?mj.lng.objects.timeField2.invalidText:this.invalidText;
		mj.form.timeField2.superclass.init.call(this);
		this.on('change',this.checkPoint);
		this.on('keypress',this.checkPoint);
	},
	checkPoint : function(t,e){
		if(parseInt(e.keyCode)==mj.keys.DELETE){
			t.setValue('');
			e.preventDefault();
			return;
		}
		if(parseInt(e.keyCode)!=mj.keys.BACKSPACE){
			var v = t.getElValue();
			if(v.toString().length == 8) {
				e.preventDefault();
				return;
			}
			if(v.toString().length == 2||v.toString().length == 5)
				t.setElValue(v.toString()+':');
		}
	}
};
mj.extend(mj.form.timeField2, mj.form.regExField);
//!!! myjui/source/form/mailField.js
mj.form.mailField = function(config){
	mj.form.mailField.superclass.constructor.call(this,config);
};
mj.form.mailField.prototype = {
	componentClass : 'mj.form.mailField',
	regEx : /^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}$/,
	init : function(){
		this.invalidText = mj.lng.objects.mailField.invalidText;
		mj.form.mailField.superclass.init.call(this);
	}
};
mj.extend(mj.form.mailField, mj.form.regExField);
//!!! myjui/source/form/passField.js
mj.form.passField = function(config){
	mj.form.passField.superclass.constructor.call(this,config);
	// mj.apply(this, {
		// invalidText : mj.lng.objects.passField.invalidText
	// });
};
mj.form.passField.prototype = {
	componentClass : 'mj.form.passField',
	//regEx : /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,15}$/,
	regEx : /^[a-zA-Z]\w{3,15}$/,
	init : function(){
		this.type = 'password';
		this.invalidText = mj.lng.objects.passField.invalidText;
		mj.form.passField.superclass.init.call(this);
	}
};
mj.extend(mj.form.passField, mj.form.regExField);
//!!! myjui/source/form/checkBox.js
mj.form.checkBox = function(config){
	mj.form.checkBox.superclass.constructor.call(this,config);
};
mj.form.checkBox.prototype = {
	componentClass : 'mj.form.checkBox',
	inputCls : 'mj-text-editor',
	type : 'checkbox',
	labelPos : 'left',
	checked : false,
	cls : 'mj-checkbox',
	init : function(){
		var t = this;
		t.on('render', t._onRender, t);		
		if(t.renderTo)
			t.render();
		mj.form.checkBox.superclass.init.call(this);
	},
	_onRender : function(){
		var t=this;
		t.check = $(mj.NE(t._el,{tag:'div',cls:t.cls,disabled:t.disabled,style:'margin-top:2px;'}));
		t.input.addClass('mj-invisible');
		if(t.disabled){
			t.check.attr('disabled',true);
			t.check.addClass('mj-item-disabled');
		}else
			t.disabled=false;
		if(t.readOnly){
			t.check.attr('readonly',true);
			t.check.addClass('mj-item-readOnly');
		}else
			t.readOnly=false;
		t.check.click(function(){
			if(!t.readOnly&&!t.disabled){
				var cBox = $(this);
				var ch = cBox.prev()[0].checked == true;
				cBox.toggleClass(t.cls+'-checked');
				cBox.prev()[0].checked = !ch;
				t.checked = !ch;
				if(typeof t.handler == 'function')
					t.handler();
			}
		});
		if(t.checked)
			t.setValue(t.checked);
	},
	getValue : function(raw){
		if(raw)
			return this.input[0].checked;
		else
			return this.checked;
	},
	setValue : function(val){
		if(typeof val == "string")
			val = parseInt(val);
		if(this.validate(val)!==false){
			this.checked = Boolean(val);
			this.setElValue(this.formatValue(Boolean(val)));
		}
	},
	setElValue : function(val){
		if(typeof val == "string")
			val = parseInt(val);
		this.input[0].checked = val;
		if(val==true)
			this.check.addClass(this.cls+'-checked');
		else
			this.check.removeClass(this.cls+'-checked');
	},
	setDisable : function(){
		mj.form.checkBox.superclass.setDisable.call(this);
		var el = this.check;
		el.attr('readonly', 'true');
		el.attr('disabled', 'true');
		el.addClass('mj-item-disabled');
	},
	setEnable : function(){
		mj.form.checkBox.superclass.setEnable.call(this);
		var el = this.check;
		el.attr('readonly', 'false');
		el.attr('disabled', 'false');
		el.removeAttr('readonly');
		el.removeAttr('disabled');
		el.removeClass('mj-item-disabled');
	}
};
mj.extend(mj.form.checkBox, mj.form.field);
//!!! myjui/source/form/radio.js
mj.form.radio = function(config){
	mj.form.radio.superclass.constructor.call(this,config);
};
mj.form.radio.prototype = {
	componentClass : 'mj.form.radio',
	inputCls : 'mj-text-editor',
	//type : 'radio',
	labelPos : 'left',
	width : 50,
	checks : false,
	checked : false,
	radioPos : 'lr',//tb 
	init : function(){
		var t = this;
		t.template = ['<div class="mj-radio" name="{name}" style="width:{width}px;'+(t.radioPos=='lr'?'':'float:none;')+'">',
			'<input class="mj-invisible" type="text" value="{id}"/>',
			'<span unselectable="on" class="mj-unselectable" title="{desc}" style="padding-left: 16px; cursor: default;">{text}</span>',
			'</div>'];
		t.tpl = new mj.template(this.template);
		if(t.items&&!t.store)
			t.store = new mj.store({data:t.items});
		if(t.store&&!t.items)
			t.items = t.store.data;
		t.on('render', t._onRender, t);		
		if(t.renderTo)
			t.render();
		mj.form.radio.superclass.init.call(this);
	},
	_onRender : function(){
		var t=this;
		this.input.addClass('mj-invisible');
		if(this.items){
			this.checks=[];
			this.radioContainer = $(mj.NE(this._el,{style:'float:left;',disabled:this.disabled,readonly:this.readOnly}));
			for(var i=0,len=this.items.length;i<len;i++){
				var item = this.items[i];
				this.addItem(item,true);
			}
		}
	},
	addItem : function(item,noAdd){
		//var itemEl = $(mj.NE(this.radioContainer,{tag:'div',cls:'mj-radio',disabled:item.disabled,readonly:item.readOnly,name:this.name,html:'<input type="text" class="mj-invisible" value="'+item.id+'"></input><span style="padding-left:15px;cursor:default;">'+item.text+'</span>'}));
		item.name=this.dataIndex;
		if(!item.width)
			item.width=this.width;
		var el=this.tpl.apply(item);
		var itemEl=this.radioContainer.append(el).find('div:last');
		if(item.checked)
			$(itemEl).addClass('mj-radio-checked');
		if(item.disabled){
			itemEl.attr('disabled',true);
			itemEl.addClass('mj-item-disabled');
		}else
			item.disabled=false;
		if(item.readOnly){
			itemEl.attr('readonly',true);
			itemEl.addClass('mj-item-readOnly');
		}else
			item.readOnly=false;
		if(!noAdd)
			this.items.push(item);
		itemEl.bind('click',{scope:this,item:item},function(e){
			var scp = e.data.scope;
			var _item = e.data.item;
			if(!scp.readOnly&&!scp.disabled&&!_item.readOnly&&!_item.disabled){
				scp.trigger('itemclick',item);
				var rBox = $(this).parent().find('div[NAME="'+$(this).attr('name')+'"]');
				rBox.removeClass('mj-radio-checked');
				scp.setValue($(this).find('input').val());
				if(typeof scp.handler == 'function')
					scp.handler();
			}
		});
		this.checks.push(itemEl);
	},
	setElValue : function(val){
		if(typeof val == "string")
			val = parseInt(val);
		this.input[0].value = val;
		for(var i=0,len=this.items.length;i<len;i++){
			var item = this.items[i];
			if(item.id==val)
				this.checks[i].addClass('mj-radio-checked');
			else
				this.checks[i].removeClass('mj-radio-checked');
		}
	},
	setDisable : function(){
		mj.form.radio.superclass.setDisable.call(this);
		var el = this.radioContainer;
		el.attr({'readonly':'true'});
		el.attr({'disabled':'true'});
		el.addClass('mj-item-disabled');
	},
	setEnable : function(){
		mj.form.radio.superclass.setEnable.call(this);
		var el = this.radioContainer;
		el.attr({'readonly':'false'});
		el.attr({'disabled':'false'});
		el.removeAttr('readonly');
		el.removeAttr('disabled');
		el.removeClass('mj-item-disabled');
	}
};
mj.extend(mj.form.radio, mj.form.field);
//!!! myjui/source/form/numberField.js
mj.form.numberField = function(config){
	mj.apply(this, {
		minText : mj.lng.objects.numberField.minText,
	    maxText : mj.lng.objects.numberField.maxText,
	    nanText : mj.lng.objects.numberField.nanText
	});
	mj.form.numberField.superclass.constructor.call(this,config);
};
mj.form.numberField.prototype = {
	componentClass : 'mj.form.numberField',
	inputCls : 'mj-text-editor',
	decimalSeparator:',',
	groupingSeparator:'.',
	decimalPrecision:2,
	decimalPrecisionReal:2,
	prefix:'',
	suffix:' YTL',
	defaultZero : false,
	allowDecimals:true,
	allowNegative:true,
	allowGroupingEnter:true,
	money : false,
    minValue : Number.NEGATIVE_INFINITY,
    maxValue : Number.MAX_VALUE,
	_onRender : function(){
		var t = this;	
		if(!t.money){
			t.allowDecimals=false;
			t.allowGroupingEnter=false;
			t.decimalSeparator='';
			t.groupingSeparator='';
			t.decimalPrecision=0;
			t.decimalPrecisionReal=0;
			t.suffix='';
		}
		mj.form.numberField.superclass._onRender.call(this);
        var keyPress = function(e){
            var k = e.keyCode || e.charCode;
			if(k>=96&&k<=105)k=k-48;
            if(!$.browser.msie && (mj.keys.isSpecial(e) || k == mj.keys.BACKSPACE || k == mj.keys.DELETE)){
                return;
            }
			var allowed = "0123456789";
			var val = t.input[0].value;
			allowed+=t.allowDecimals&&val.indexOf(t.decimalSeparator)===-1?t.decimalSeparator:'';
			allowed+=t.allowNegative&&val.indexOf('-')===-1?'-':'';
			allowed+=t.allowGroupingEnter?t.groupingSeparator:'';
            if(allowed.indexOf(String.fromCharCode(k)) === -1){
                return false;
            }
        };
		t.on('focus', t._onFocus);
        t.input.keypress(keyPress);	
		t.input[0].style.textAlign = 'right';
	},
	_onBlur : function(){
		if(!this.readOnly){
			this.clearValue();
			this.setValue(this.getElValue());
		}
	},
	_onFocus : function(){
		if(!this.readOnly){
			this.value=this.value||'';
			var val = (this.decimalSeparator!='.')?(this.value+'').replace(/\./g,this.decimalSeparator):this.value;
			this.setElValue(val);
		}
	},
	clearValue : function(){
		var val=this.input[0].value;
		if(this.allowGroupingEnter){
			var pattern = new RegExp(('\\'+this.groupingSeparator),"g");
			val = val.replace(pattern,'');
		}
		if(this.decimalSeparator!=''&&this.decimalSeparator!='.'){
			var pattern = new RegExp(('\\'+this.decimalSeparator),"g");
			val = val.replace(pattern,'.');
		}
		this.input[0].value = val;
	},
	setValue : function(val){
		//if(typeof val=='number'&&this.validate(val)!==false){
		val = ((this.defaultZero&&val=='')?0:((val=='' && !this.emptyValue && typeof this.emptyValue == 'object') ? this.emptyValue : val));
		//val = this.readOnly && val == '' ? this.emptyValue : val;
		if(val==this.emptyValue){
			this.clearInvalid();
			this.value = val;
			this.setElValue('');
			if(this.form && !this.form.loading)
				this.form.setModified(this);
		}else
			if(((val!=''&&val!=null)||typeof val=='number')&&this.validate(val)!==false){
				this.value = parseFloat(val);
				if(this.decimalPrecisionReal&&!isNaN(this.decimalPrecisionReal))
					val=parseFloat(val).toFixed(this.decimalPrecisionReal);
				this.setElValue(this.formatValue(val));
				if(this.form && !this.form.loading)
					this.form.setModified(this);
			}
	},
	clearInvalid : function(){
		this.input.removeClass('mj-text-input-invalid');
		this.invalid=false;
	},
	markInvalid : function(msg){
		this.input.addClass('mj-text-input-invalid');
		this.invalid=true;
		this.trigger('invalid',this,msg);
	},
	validate : function(val){
		this.clearInvalid();
		if(isNaN(val)){
            this.markInvalid(String.format(this.nanText, this.minValue));
			return false;
		}
        if(val < this.minValue){
            this.markInvalid(String.format(this.minText, this.minValue));
			return false;
        }
        if(val > this.maxValue){
            this.markInvalid(String.format(this.maxText, this.maxValue));
			return false;
        }
		return true;
	},
	formatValue : function(value){
		return this.prefix + mj.format.float2Money(value,this.decimalPrecision,this.decimalSeparator,this.groupingSeparator) + this.suffix;
	},
	clear : function(){
		this.value = null;
		this.setElValue('');
	}
};
mj.extend(mj.form.numberField, mj.form.textField);
//!!! myjui/source/form/triggerField.js
/** 
 * @fileoverview triggerField nesnesi.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.0.1 02.10.2007
 */
 /**
 * triggerField nesnesi.
 * Örnek kullanımı aşağıdaki gibidir:
 * @example
	var edt1 = new mj.form.triggerField({
		renderTo :  mj.NE(mj.bd,{style:'padding:10px'}),
		triggerClass:'trigger-field',
		title : 'eww',
		onTriggerClick : function(){
			alert("1");
		}
	});

 * @class
 * @name mj.form.triggerField
 * @extends mj.form.textField
 * @param {Object} config triggerField objesini oluşturabilmek için gerekli olan parametreleri içerir. 
 * @config {HTMLElement} renderTo triggerField nesnesinin render edileceği container nesnesi.
 * @config {String} triggerClass Özel trigger buton imajı kullanımı gerektiğinde kullanılmalıdır.
 * @config {String} title triggerField yanındaki form elementi başlık değeri.
 * @config {Function} onTriggerClick triggerField trigger butonuna tıklandığında olan olayları içeren fonksiyon.
 */
mj.form.triggerField = function(config){
	if(config.width)
		config.width-=17;
	mj.form.triggerField.superclass.constructor.call(this,config);
};
mj.form.triggerField.prototype = {
	componentClass : 'mj.form.triggerField',
	inputCls : 'mj-text-editor',
	triggerClass : 'trigger-field',
	/** 
	 * .Ön tanımlı değeri:false.
	 * @name readOnly 
	 * @type {Boolean}
	 * @memberOf mj.form.triggerField
     */
	readOnly : true,
	_onRender : function(){
		mj.form.triggerField.superclass._onRender.call(this);
		var tag, t = this;
		//if($.browser.msie)
			tag='div';
		//else
			//tag='img';
		t.triggerField =  $(mj.NE(t.input[0].parentNode,{tag: tag, cls: "trigger " + t.triggerClass}));
		t.triggerField.hover(function(){
				$(this).addClass('trigger-arrow-over');
			},
			function(){
				$(this).removeClass('trigger-arrow-over');
			}
		);
		t.triggerField.mousedown(function(){
			t.triggerField.addClass('trigger-arrow-click');
		});
		t.triggerField.mouseup(function(){
			t.triggerField.removeClass('trigger-arrow-click');
		});
		t.triggerField.click(function(e){
			if(!t.disabled){
				t.trigger('triggerclick', t);
				e.stopPropagation();
			}
		});
		t.on('triggerclick',t.onTriggerClick);
		mj.bd.bind('click', t.hideAll);
		t.on('beforedestroy', function(){
			var i = mj.glb.views.indexOf(t);
			if(i>-1)
				mj.glb.views.splice(i, 1);
		});
	},
	hideAll : function(){
		el = mj.glb.views;
		for(var i=0,len=el.length;i<len;i++)
			if(typeof el[i]._hide == 'function')
				el[i]._hide();
		mj.glb.views = [];
	},
	onTriggerClick : function(){
		this.hideAll();
		if(this.clearOnTriggerClick)
			this.clear();
		if(typeof this.handler == 'function')
			this.handler();
	},
	getPosition : function(){
		var ofs = $.browser.msie?this.input.prev().offset():this.input.offset();
		if ($.browser.msie&&this.labelAlign=='left') ofs.left=ofs.left+parseInt(this.labelWidth);
		return ofs;
	},
	getValue : function(raw){
		if(raw)
			return this.input[0].value;
		else
			return this.value!==''?this.value:null;
	},
	setWidth : function(width){
		this._el.width(width);
		if(this.title)
			width-=this.labelWidth;
		this.input.width(width-($.browser.msie?23:22));	
	},
	setValue : function(val,trigger){
		if(trigger)
			this.value = val;
		else
			if(this.validate(val)!==false){
				this.displayValue = val;
				this.setElValue(this.formatValue(this.displayValue));
			}
	},
	clear : function(){
		this.setValue('');
		this.value = '';
	},
	setDisable : function(){
		this.triggerField.addClass(this.triggerClass+'-disabled');
		mj.form.triggerField.superclass.setDisable.call(this);
	},
	setEnable : function(){
		this.triggerField.removeClass(this.triggerClass+'-disabled');
		mj.form.triggerField.superclass.setEnable.call(this);
	}
};
mj.extend(mj.form.triggerField, mj.form.textField);
//!!! myjui/source/form/combo.js
/** 
 * @fileoverview Combo nesnesi.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.0.1 17.10.2007
 */
 /**
 * Combo nesnesi.
 * Örnek kullanımı aşağıdaki gibidir:
 * @example
	var a=[];
	a.push({id:'deneme1',text:'../'});
	a.push({id:'deneme2',text:'../../'});
	a.push({id:'deneme3',text:'../../images/'});
	a.push({id:'deneme4',text:'../../images/trigger.gif'});
	var cmb1 = new mj.form.combo({
		renderTo :  mj.NE(mj.bd,{style:'padding:10px'}),
		title : 'wwwwww',
		store : new mj.store({data:a}),
		width : 200
	});
	cmb1.setValue('deneme2');
	var cmb2 = new mj.form.combo({
		renderTo :  mj.NE(mj.bd,{style:'padding:10px'}),
		title : 'cccc',
		mode : 'remote',
		store : new mj.store({
				url : '../tree/data.php',
				params : {
					event : 'getCombo'
				}
			}),
		width : 250
	});

 * @class
 * @name mj.Combo
 * @extends mj.form.triggerField
 * @param {Object} config Combo objesini oluşturabilmek için gerekli olan parametreleri içerir. 
 * @config {HTMLElement} renderTo Combo nesnesinin render edileceği container nesnesi.
 * @config {Object} store Combo store objesi.
 * @config {String} title Combo yanındaki form elementi başlık değeri.
 * @config {Number} width Combo genişlik değeri.
 * @config {String} mode Combo çalışma modu.Ön tanımlı değeri:local.
 * @config {Number} maxHeight Combo view yükseklik değeri.Ön tanımlı değeri:200.
 * @config {String} idField Combo elementi id alan adı.Ön tanımlı değeri:id.
 * @config {String} displayField Combo elementi display alan adı.Ön tanımlı değeri:text.
 */
mj.form.combo = function(config){
	mj.form.combo.superclass.constructor.call(this,config);
};
mj.form.combo.prototype = {
	componentClass : 'mj.form.combo',
	triggerClass : '',
	/** 
	 * Combo elementi id alan adı.Ön tanımlı değeri:id.
	 * @name idField 
	 * @type {String}
	 * @memberOf  mj.combo
     */
	idField : 'id',
	/** 
	 * Combo elementi display alan adı.Ön tanımlı değeri:text.
	 * @name displayField 
	 * @type {String}
	 * @memberOf  mj.combo
     */
	displayField : 'code',
	/** 
	 * Combo çalışma modu.Ön tanımlı değeri:local.
	 * @name mode 
	 * @type {String}
	 * @memberOf  mj.combo
     */
    mode: 'local',
	/** 
	 * Combo view yükseklik değeri.Ön tanımlı değeri:200.
	 * @name maxHeight 
	 * @type {Number}
	 * @memberOf  mj.combo
     */
	maxHeight : 200,
	typeAhead: true,
    //emptyText:'Seçiniz...',
	viewing	: false,
	value : false,
	sameValueDisplay : true,
	first : false,
	readOnly : false,
	sel : false,
	change : false,
	clearOnTriggerClick : false,
	clearOnBeforeSet : false,
	_cleared : false,
	/**
	 * Combo nesnesi init fonksiyonu.Store'u load eder.
	 * @name init2
	 * @function
	 * @type {Function}
	 */
	init : function(){
		mj.form.combo.superclass.init.call(this);
		var t=this;
		t.sel=[];
		t.mode = (t.store && t.store.url && t.store.url != '') ? 'remote' : t.mode;
		t.cmbTpl = '<div class="combo-list-item mj-unselectable">{'+this.displayField+'}</div>';
		var clx=this.multiRow?'':'mj-invisible';
		t.first=true;
		t.viewEl = $(mj.NE(mj.bd,{tag:'div',id:mj.genId('mj-combo-view-'),cls:'combo-list '+clx}));
		t.viewInner = $(mj.NE(t.viewEl,{tag:'div',cls:'combo-list-inner'}));
		t.view = new mj.view({
			renderTo : t.viewInner,
			store : this.store,
			tpl : new mj.template(t.cmbTpl),
			selector : 'div.combo-list-item',
			overClass : 'combo-over',
			selectedClass : 'combo-selected',
			multiSelect : this.multiSelect,
			loadMask : false,
			//pbar : new mj.pager({limit:15,pos:'bottom'}),
			filter : false,
			scope : t
		});
		t.addRelated(t.view);
		t.addRelated(t.viewEl);
		var _itemClick = function(){
			if(this.selected[0]){
				var s = this.scope;
				s.setValue(this.selected[0].id,true);
				s.setValue(this.selected[0].value);
				if(this.view)
					this.sel=[];
				else{
					s.sel=[];
					if(this.sel.length)
						s.sel=this.sel;
					else
						s.sel.push(this.sel);
				}
				if(!s.multiRow)
					s._hide();
				if(s._el.hasClass('grid-editor-item'))
					s.trigger('editcomplete',this);
			}
		};
		this.view.on('itemclick',_itemClick);
		this.view.store.on('load',function(){
			this._show();
		},this);
		if(this.multiRow){
			this.view.store.load();
		}
		if(this.typeAhead){
			this.on('change',function(){
				if(this.disabled||this.readOnly)
					return;
				this.change = true;
				if(this.mode=='local'||typeof this.store.routedata == 'undefined'){
					this.view.filter ={dI:this.displayField,v:this.getValue(true)};
					this.view.load();
					this._show();
				}else{
					//this.store.params['dI']=this.displayField;
					//this.store.params['v']=mj.escape(this.getValue(true));
					var _data=this.store.routedata;
					_data.val=mj.escape(this.getValue(true)).split(".").join("|_|");
					var _url = Routing.generate(this.store.routename, _data);
					this.view.store.url=_url;
					this.view.store.load();
				}
			},this);
		}
		this.on('beforedestroy',function(){
			this.view.mon('itemclick',_itemClick);
			this.view.store.events['load'].listeners = [];
		},this);	
		if(this.value!=false)
			this.setValue(this.value);
	},
	/**
	 * Combo nesnesi onTriggerClick fonksiyonu.Trigger butonuna basıldığında çalışır.
	 * @name onTriggerClick
	 * @function
	 * @type {Function}
	 */
	onTriggerClick : function(){
		this.hideAll();
		if(this.clearOnTriggerClick){
			var val = typeof this.emptyValue != 'undefined' ? this.emptyValue : '';
			this._oVal = val;
			this.setValue(val,true);
			this.setValue(val);
			if(this.mode=='remote'){
				var _url = Routing.generate(this.store.routename, this.store.routedata);
				//this.store.params['dI']=this.displayField;
				//this.store.params['v']=mj.escape(this.getValue(true));
				this.store.url=_url;
			}else
				this.view.filter ={dI:this.displayField,v:this.getValue(true)};
		}
		this._hide();
		this.view.store.load();
	},
	/**
	 * Combo nesnesi findIndex fonksiyonu.Store'u load eder ve view'in load'unu çağırır.
	 * @name findIndex
	 * @function
	 * @type {Function}
	 * @param {String} val Combo store'u içinde aranacak elementin id değeri.
	 * @return {Number} Store içinde bulunan elementin index değeri,bulunamadığı durumda -1.
	 */
	findIndex : function(val){
		var ret = mj.getIndex(this.store.data,this.idField,val);
		if(ret==-1)
			ret = mj.getIndex(this.store.data,this.displayField,val);
		return ret;
	},
	/**
	 * Combo nesnesi getValue fonksiyonu.
	 * @name getValue
	 * @function
	 * @type {Function}
	 * @param {String} val Combo'dan get edilecek id değeri.
	 */
	getValue : function(raw){
		if(raw)
			return this.input[0].value;
		else
			return this.sameValueDisplay?this.displayValue:this.value!==''?this.value:null;
	},
	/**
	 * Combo nesnesi setValue fonksiyonu.
	 * @name setValue
	 * @function
	 * @type {Function}
	 * @param {String} val Combo'ya set edilecek id değeri.
	 */
	setValue : function(val,trigger){
		if(this.clearOnBeforeSet&&!this._cleared){
			this._cleared=true;
			this.clear();
			this._cleared=false;
		}
		if(!this.multiSelect)
			this.sel=[];
		if(this.mode=='local'||(this.view.store.loaded)){
			if(this.mode=='remote')
				this.store=this.view.store;
			if(val!=='' && val!=null){
				var fId=this.findIndex(val);
				if(fId!=-1){
					var data=this.store.data[fId];
					this.sel.push({index:fId,id:data.id,value:data[this.displayField]});
					this.value = this.sameValueDisplay?data[this.displayField]:data.id;
					this.displayValue = data[this.displayField];
					this.setElValue(this.formatValue(this.displayValue));
				}else{
					if(trigger)
						this.value = val;
					else{
						this.displayValue = this.lookupValue = val;
						this.setElValue(this.formatValue(this.displayValue));
					}
				}
			}else{
				if(this.store.params && this.store.params.v)
					delete this.store.params.v;
				this.value = ((this.clearOnTriggerClick && typeof this.emptyValue != 'undefined') ? this.emptyValue : val);
				//this.value = val;
				this.displayValue = val;
				this.setElValue(val);
			}
		}else{
			var t=this;
			if(trigger)
				t.value = val;
			else{
				t.displayValue = t.lookupValue = val;
				t.setElValue(t.formatValue(t.displayValue));
			}
			//selected özelliği iptal edildi
			/*this._oVal = val;
			this.view.store.onOnce('load', this._oLoad, this);
			if(!this.view.store.loaded)
				this.view.store.load();
			else if(this.mode=='remote'){
				var fId=t.findIndex(val);
				if(fId==-1){
					delete this.store.params.v;
					delete this.store.params.dI;
					this.view.clearSelections();
					this.view.store.load();
				}else{
					var data=t.store.data[fId];
					t.sel.push({index:fId,id:data.id,text:data[t.displayField]});
					t.view._onSelect(t.view.items[t.sel[t.sel.length-1].index]);
					t.value = data.id;
					t.lookupValue = data[t.displayField];
					t.setElValue(t.formatValue(t.lookupValue));
				}
			}*/
		}
		if(this.form && !this.form.loading)
			this.form.setModified(this);
	},
	_oLoad : function(){
		var t = this;
		if(!t.change){
			val = this._oVal;
			val = t.sel.length==1?t.sel[0].value:val;
			var fId=t.findIndex(val);
			if(fId>-1){
				var data=t.store.data[fId];
				t.sel.push({index:fId,id:data.id,text:data[t.displayField]});
				t.view._onSelect(t.view.items[t.sel[t.sel.length-1].index]);
				t.value = data.id;
				t.lookupValue = data[t.displayField];
				t.setElValue(t.formatValue(t.lookupValue));
			}
		}else
			t.change = false;
	},
	/**
	 * Combo nesnesi _show fonksiyonu.Combo'nun view'ını gösterir.
	 * @name _show
	 * @function
	 * @type {Function}
	 */
	_show : function(){
		this.viewEl.removeClass('mj-invisible');
		var ofs = this.getPosition();
		var elWidth = ($.browser.msie ? this.input.outerWidth() : this.input.outerWidth())+15;
		var elHeight=this.maxHeight>(this.view.store.recordCount*20)?this.view.store.recordCount*20:this.maxHeight;
		var _visibleMasks = $('.mj-win:visible')||$('.mj-mask:visible');
		var _zi=0;
		for(var _i=0;_i<_visibleMasks.length;_i++){
			_zi = _zi<parseInt("0"|_visibleMasks[_i].style.zIndex)?parseInt("0"|_visibleMasks[_i].style.zIndex):_zi;
		}
		this.viewEl.css('top',ofs.top+(this.multiRow?0:21)).css('left',ofs.left).css('width',elWidth).css('height',elHeight).css('z-index',_zi+5);
		if(this.multiRow)
			$(this.el).css('height',elHeight<this.maxHeight?elHeight:this.height);
		this.viewInner.css('width',elWidth).css('height',elHeight);
		this.viewing = true;
		var i = mj.glb.views.indexOf(this);
		if(i==-1)
			mj.glb.views.push(this);
	},
	/**
	 * Combo nesnesi _hide fonksiyonu.Combo'nun view'ını gizler.
	 * @name _hide
	 * @function
	 * @type {Function}
	 */
	_hide : function(){
		this.viewEl.css('z-index',0).addClass('mj-invisible');
		this.viewing = false;
	},
	_onBlur : function(){
		return false;
	}
};
mj.extend(mj.form.combo, mj.form.triggerField);
//!!! myjui/source/form/datePicker.js
mj.form.datePicker = function(config){
	mj.form.datePicker.superclass.constructor.call(this,config);
};
mj.form.datePicker.prototype = {
	componentClass : 'mj.form.datePicker',
	inputCls : 'mj-datepicker',
	weekend : [5,6],
	initValue : false,
	init : function(){
		this.render();
		if(this.initValue === false)
			this.initValue = new Date();
		this.fillMonth(this.initValue);
		mj.form.datePicker.superclass.init.call(this);
	},
	render : function(){
		var t = this, n = mj.NE, v='z-index:10000;';
		if(t.hidden)
			t.itemCls += ' mj-invisible mj-absolute';
		if(t.left||t.top){
			t.left = typeof t.left != 'undefined' ? (isNaN(t.left) ? t.left : (t.left+'px')) : '';
			t.top = typeof t.top != 'undefined' ? (isNaN(t.top) ? t.top : (t.top+'px')) : '';
			v += 'position:absolute;left:'+t.left+';top:'+t.top;
		}
		var el = t.destroyEl = t.el = n(t.renderTo, {cls:t.itemCls+' mj-unselectable',style:v}), _el = t._el = $(el);
		if(!t.inline)
		t.titleEl = n(el, {html:this.title, cls:t.titleCls, style:'width:'+t.labelWidth+';text-align:'+t.labelAlign});
		var c = t.input = $(n(el, {cls:t.inputCls,readOnly:t.readOnly}));
		t.cnts = {
			header : $(n(c,{cls:'mj-date-header'})),
			days : $(n(c,{cls:'mj-date-days'})),
			dates : $(n(c,{cls:'mj-date-dates'})),
			buttons : $(n(c,{cls:'mj-date-buttons'}))
		};
		var setHover = function(item){
			item.hover(function(){
				item.addClass('hover');
			},function(){
				item.removeClass('hover');
			});
			return item;
		};
		var h = t.cnts.header;
		t.headerEls = {
			prevYear : setHover($(n(h, {cls:'mj-date-prevyear', html:mj.insertSpacer(18,18)}))),
			prevMonth : setHover($(n(h, {cls:'mj-date-prevmonth', html:mj.insertSpacer(18,18)}))),
			current : setHover($(n(h, {cls:'mj-date-current'}))),
			nextMonth : setHover($(n(h, {cls:'mj-date-nextmonth', html:mj.insertSpacer(18,18)}))),
			nextYear : setHover($(n(h, {cls:'mj-date-nextyear', html:mj.insertSpacer(18,18)})))
		};
		var tmp = $(n(t.headerEls.current,{tag:'span',cls:'mj-date-currenttext'}));
		t.headerEls.dateSelect = setHover($(n(t.headerEls.current, {tag:'img',cls:'mj-date-select',src:mj.glb.blankImage})));
		var showMY = function(e){
			e.stopPropagation();
			t.showMonthYearSelect(t.current);
		};
		t.headerEls.dateSelect.click(showMY);
		tmp.click(showMY);
		t.headerEls.current=tmp;
		t.headerEls.prevYear.click(function(e){
			e.stopPropagation();
			t.fillMonth(t.getPrevYear(t.current));
		});
		t.headerEls.nextYear.click(function(e){
			e.stopPropagation();
			t.fillMonth(t.getNextYear(t.current));
		});
		t.headerEls.prevMonth.click(function(e){
			e.stopPropagation();
			t.fillMonth(t.getPrevMonth(t.current));
		});
		t.headerEls.nextMonth.click(function(e){
			e.stopPropagation();
			t.fillMonth(t.getNextMonth(t.current));
		});
		var d = t.cnts.days;
		t.dayTitles = [];
		for(var i=-1;i<7;i++){
			t.dayTitles.push($(n(d,{cls:'mj-date-daytitle', html:(i>-1?Date.strings.daysShort[(i + Date.firstDayOfWeek) % 7]:'')})));
		}
		var d = t.cnts.dates;
		t.dayButtons = [];
		var dayClick = function(item){
			item.click(function(){
				var datesplit = item[0].name.split('-');
				var selectedDate = new Date(), day = datesplit[5], month = datesplit[4], year = datesplit[3];
				selectedDate.setDate(1);
				selectedDate.setFullYear(year);
				selectedDate.setMonth(month-1);
				selectedDate.setDate(day);
				t.setValue(selectedDate);
				t.trigger('selectdate', t, selectedDate ,day, month, year);
			});
			return item;
		};
		for(i=0;i<6;i++){
			var a = [];
			for(var j=0;j<8;j++){
				var cell;
				if(j==0)	
					cell = $(n(d, {cls:'mj-date-week'}));
				else
					cell = dayClick(setHover($(n(d, {cls:'mj-date-day'}))));
				a.push(cell);
			}
			t.dayButtons.push(a);
		}
		var d = t.cnts.buttons;
		var todayClick = function(){
			var selectedDate = new Date();
			t.fillMonth(selectedDate);
			t.setValue(selectedDate);
			t.trigger('selectdate', t, selectedDate);
		};
		t.buttons = {
			today : new mj.button({renderTo : n(d,{cls:'mj-date-button-today'}),title : mj.lng.glb.today, handler:todayClick})
			//,reset : new mj.button({renderTo : n(d,{cls:'mj-date-button-reset'}),title : mj.lng.glb.reset})
		};
		var m = t.mySelect = $(n(c,{cls:'mj-date-myselect mj-invisible'}));
		var mc = n(m,{cls:'mj-date-monthcontainer'}), yc=n(m,{cls:'mj-date-yearcontainer'}), bc=n(m,{cls:'mj-date-buttoncontainer'});
		bc = n(bc,{cls:'mj-date-buttonalign'});
		var ms = t.monthSelectors = [];
		var assignMonthSelect = function(item, index){
			var select = function(e){
				e.stopPropagation();
				ms.selectedMonthCell.removeClass('mj-date-selected');
				ms.selectedMonth = index;
				ms.selectedMonthCell = item;
				item.addClass('mj-date-selected');
			};
			item.click(select);
			return item;
		};
		for(var i = 0;i<12;i++){
			var j = parseInt(i/2)+(i%2==1?6:0);
			ms.push(assignMonthSelect(setHover($(n(mc,{cls:'mj-date-monthselect',html:(Date.strings.monthsShort[j])}))),j));
		}
		var ys = t.yearSelectors = [];
		var assignYearSelect = function(item, index){
			var select = function(e){
				e.stopPropagation();
				ys.selectedYearCell.removeClass('mj-date-selected');
				ys.selectedYear = ys.firstYear + index;
				item.addClass('mj-date-selected');
				ys.selectedYearCell = item;
			};
			item.click(select);
			return item;
		};
		for(var i = 0;i<10;i++)
			t.yearSelectors.push(assignYearSelect(setHover($(n(yc,{cls:'mj-date-yearselect'}))),(i%2?parseInt(i/2)+5:parseInt(i/2))));
		t.buttons.prevYearGroup = new mj.button({renderTo : n(yc,{cls:'mj-date-button-prevyear'}),title : '<<'});
		t.buttons.prevYearGroup.on('click', function(btn,handlerId,e){
			e.stopPropagation();
			t.fillYears(t.curYear-10);
		});
		t.buttons.nextYearGroup = new mj.button({renderTo : n(yc,{cls:'mj-date-button-nextyear'}),title : '>>'});
		t.buttons.nextYearGroup.on('click', function(btn,handlerId,e){
			e.stopPropagation();
			t.fillYears(t.curYear+10);
		});
		t.buttons.ok = new mj.button({renderTo : n(bc,{cls:'mj-date-button-yearok'}),title : mj.lng.titles.buttons.ok});
		t.buttons.cancel = new mj.button({renderTo : n(bc,{cls:'mj-date-button-yearcancel'}),title : mj.lng.titles.buttons.cancel});
		t.buttons.ok.on('click', function(btn,handlerId,e){
			e.stopPropagation();
			var _tmp = new Date(t.current);
			_tmp.setMonth(ms.selectedMonth);
			_tmp.setFullYear(ys.selectedYear);
			t.fillMonth(_tmp);
			t.hideMonthYearSelect();
		});
		t.buttons.cancel.on('click', function(btn,handlerId,e){
			e.stopPropagation();
			t.hideMonthYearSelect();
		});
		t.trigger('render', t);
	},
	getPrevMonth : function(date){
		var _tmp = new Date(date);//, _d = _tmp.getDate(), _ld = _tmp.getLastDay();
		_tmp.setDate(1);
		_tmp.setMonth(_tmp.getMonth()-1);
		//_tmp.setDate(_d>_ld?_ld:_d);
		return _tmp;
	},
	getNextMonth : function(date){
		var _tmp = new Date(date);//, _d = _tmp.getDate(), _ld = _tmp.getLastDay();
		_tmp.setDate(1);
		_tmp.setMonth(_tmp.getMonth()+1);
		//_tmp.setDate(_d>_ld?_ld:_d);
		return _tmp;
	},
	getPrevYear : function(date){
		var _tmp = new Date(date);//, _d = _tmp.getDate(), _ld = _tmp.getLastDay();
		_tmp.setDate(1);
		_tmp.setYear(_tmp.getFullYear()-1);
		//_tmp.setDate(_d>_ld?_ld:_d);
		return _tmp;
	},
	getNextYear : function(date){
		var _tmp = new Date(date);//, _d = _tmp.getDate(), _ld = _tmp.getLastDay();
		_tmp.setDate(1);
		_tmp.setYear(_tmp.getFullYear()+1);
		//_tmp.setDate(_d>_ld?_ld:_d);
		return _tmp;
	},
	getFirstDayOfMonth : function(date){
		var _tmp = new Date(date);
		_tmp.setDate(1);
		return _tmp.getDay();
	},
	setCell : function(cell, type, value){
		cell[0].className='mj-date-day';
		if(this.value && this.value.getDate()==value.getDate()&&this.value.getMonth()==value.getMonth()&&this.value.getFullYear()==value.getFullYear()){
			if(this.selectedCell)
				this.selectedCell.removeClass('mj-date-selected');
			this.selectedCell = cell;
			cell[0].className+=' mj-date-selected';
		}else
			cell[0].className+=' mj-date-'+type;
		if(this.weekend.indexOf(value.getDay())>-1)
			cell[0].className+=' mj-date-weekend';
		var day = value.getDate();
		var month = value.getMonth() + 1;
		var year = value.getFullYear();
		cell[0].innerHTML = day;
		cell[0].name = 'mj-date-day-' + year + '-' + month + '-' + day;
		cell.addClass(cell[0].name);
	},
	fillYears : function(year){
		this.curYear = year;
		this.yearSelectors.firstYear = this.curYear - 4;
		for(var i = 0; i<10; i++){
			var t = this.curYear + i - 4, c = this.yearSelectors[i<5?(i*2):(i-5)*2+1];
			c.removeClass('mj-date-selected');
			c[0].innerHTML = t;
			if(this.current.getFullYear()==t){
				c.addClass('mj-date-selected');
				this.yearSelectors.selectedYear = t;
				this.yearSelectors.selectedYearCell = c;
			}
		}
		var ms = this.monthSelectors, m = this.current.getMonth();
		if(ms.selectedMonthCell)
			ms.selectedMonthCell.removeClass('mj-date-selected');
		ms.selectedMonth = m;
		var mi = m<6?(m*2):(m-6)*2+1;
		ms.selectedMonthCell = ms[mi];
		ms[mi].addClass('mj-date-selected');
	},
	showMonthYearSelect : function(date){
		this.mySelect.removeClass('mj-invisible');
		this.fillYears(date.getFullYear());
	},
	hideMonthYearSelect : function(){
		this.mySelect.addClass('mj-invisible');
	},
	fillMonth : function(date){
		var t = this, firstDay = t.getFirstDayOfMonth(date), b = t.dayButtons;
		var _tmp = new Date(date);
		_tmp.setDate(1);
		for(var i=firstDay+1;i>1;i--){
			_tmp.setDate(_tmp.getDate()-1);
			t.setCell(b[0][i-1], 'passive', _tmp);
		}
		var _tmp = new Date(date);
		_tmp.setDate(1);
		var cMonth = _tmp.getMonth(), cls = 'active';
		for(var i=0;i<6;i++){
			for(var j=1;j<8;j++){
				if(i>0||j>firstDay){
					t.setCell(b[i][j], cls, _tmp);
					_tmp.setDate(_tmp.getDate()+1);
					if(_tmp.getMonth()!=cMonth)
						cls = 'passive';
				}
			}
			// _tmp.setHours(0, 0, 0, 0);
			// console.log(_tmp.getFullYear(),_tmp.getWeek(),_tmp.formatDate('W'));
			var __w__=parseInt(_tmp.formatDate('W'))>1?_tmp.formatDate('W')-(_tmp.getFullYear()==2021?1:0):(_tmp.getFullYear()==2021?53:52);
			b[i][0][0].innerHTML = __w__;
		}
		t.current = date;
		t.headerEls.current[0].innerHTML = date.formatDate('F Y');
	},
	setValue : function(value){
		var refresh = false;
		var val = this.value || this.initValue;
		if(value.getMonth()!=val.getMonth()||value.getFullYear()!=val.getFullYear())
			refresh = true;
		this.value = value;
		if(refresh)
			this.fillMonth(value);
		else{
			if(this.selectedCell)
				this.selectedCell.removeClass('mj-date-selected');
			//var item = $('div[NAME*="mj-date-day-'+value.getFullYear()+'-'+(value.getMonth()+1)+'-'+value.getDate()+'"]',this.cnts.dates);
			var item = $('div.mj-date-day-'+value.getFullYear()+'-'+(value.getMonth()+1)+'-'+value.getDate(),this.cnts.dates);
			this.selectedCell = item;
			item[0].className+=' mj-date-selected';
		}
	},
	_hide : function(){
		this._el.addClass('mj-invisible');
	},
	_show : function(){
		this._el.removeClass('mj-invisible');
	}
};
mj.extend(mj.form.datePicker, mj.form.field);
//!!! myjui/source/form/dateField.js
/** 
 * @fileoverview dateField nesnesi.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.1 22.10.2007
 */
mj.form.dateField = function(config){
	//Kullanıldığı yerlerde her dateField için farklı metinler verilebilmesi için constructor içine yerleştirildi. Ör: minText : 'Sevk tarihi en fazla {0} olabilir' 
	mj.apply(this, {
		minText : mj.lng.objects.dateField.minText,
		maxText : mj.lng.objects.dateField.maxText,
		invalidText : mj.lng.objects.dateField.invalidText
	});
	mj.form.dateField.superclass.constructor.call(this,config);
};
mj.form.dateField.prototype = {
	componentClass : 'mj.form.dateField',
	inputCls : 'mj-date-field',
	width : '85px',
	format : "Y-m-d",
	altFormats : "d/m/Y|d-m-y|d-m-Y|d/m|d-m|dm|dmy|dmY|m",
	disabledDays : null,
	disabledDaysText : "",
	disabledDates : null,
	disabledDatesText : "",
	minValue : null,
	maxValue : null,
	clearOnTriggerClick : true,
	epoch : false,
	readOnly : false,
	initValue : false,
	defaultYear:false,
	maxLength:10,
	_onRender : function(){
		mj.form.dateField.superclass._onRender.call(this);
		var t = this;
		//////////////////
		var keyPress = function(e){
            var k = e.keyCode || e.charCode;
			if(k>=96&&k<=105)k=k-48;
            if(!$.browser.msie && (mj.keys.isSpecial(e) || k == mj.keys.BACKSPACE || k == mj.keys.DELETE)){
                return;
            }
			var allowed = "0123456789/";
			var val = t.input[0].value;
			if(allowed.indexOf(String.fromCharCode(k)) === -1){
                return false;
            }
        };
		var keyup = function(e){
            if(!$.browser.msie && (mj.keys.isSpecial(e) || k == mj.keys.BACKSPACE || k == mj.keys.DELETE)){
                return;
            }
			var val = t.input[0].value;
            var k = e.keyCode || e.charCode;
			if(k>=96&&k<=105){
				k=k-48;
			}else if(k>105){
				var subStr = val.substr(val.length - 1);
				k=subStr.charCodeAt(0);
			}
			var allowed = "0123456789";
			if(allowed.indexOf(String.fromCharCode(k)) == -1)return false;
			
			/*if(k>=96&&k<=105)k=k-48;
			var allowed = "0123456789/";
			if(allowed.indexOf(String.fromCharCode(k)) === -1){
                return false;
            }*/
			if(val.length == 2||val.length == 5){
				if(val.substr(val.length-1,1)=='/'){
					var arr=val.split('/');
					for(var i=0;i<arr.length-1;i++){
						if(arr[i].length==1)
							arr[i]="0"+arr[i];
					}
					t.input[0].value=arr.join('/');
				}else{
					if(val.length == 5&&t.defaultYear)
						t.input[0].value= val+'/'+t.defaultYear;
					else
						t.input[0].value= val+'/';
				}
			}
			val = t.input[0].value;
			if(val.length==8&&val.substr(6,2)<20){
				var arr=val.split('/');
				arr[2]="20"+arr[2];
				t.input[0].value=arr.join('/');
			}
			val = t.input[0].value;
			if(val.length==10){
				var re= /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
				if(val.match(re)){
					t.setValue(val);
				}else{
					t.input[0].value='';
				}
			}
        };
		t.input.keypress(keyPress);
		t.input.keyup(keyup);
		//t.input[0].readOnly=true;
		//////////////////
		t.picker = new mj.form.datePicker({
			inline : true,
			hidden : true,
			renderTo : mj.bd,
			scope:t
		});
		t.addRelated(t.picker);
		t.picker.on('selectdate', function(picker, date){
			t.setValue(t.formatValue(date));
			picker._hide();
			if(t._el.hasClass('grid-editor-item'))
				t.trigger('editcomplete',this);
		});
		if(this.initValue === false){
			var tmp = new Date();
			this.initValue = tmp.formatDate(this.format);
		}
		this.setValue(this.initValue);
		t._el.addClass('mj-date-field-cnt');
	},
	onTriggerClick : function(){
		if(this.disabled||this.readOnly)
            return;
		if(this.clearOnTriggerClick)
			this.clear();
		this.hideAll();
		ofs = this.getPosition();
		var _p = this.picker;
		_p._el.css('top',ofs.top+16).css('left',ofs.left+($.browser.msie?0:-1));
		if(window.windowManager)
			_p._el.css('z-index',window.windowManager.activeIndex+1);
		this.viewing = true;
		mj.glb.views.push(_p);		
		var time = this.value?mj.str2date(this.epoch?this.input[0].value:this.value):new Date();
		_p.fillMonth(time);
		_p.setValue(time);
		_p._show();
		if(typeof this.handler == 'function')
			this.handler();
	},
	formatValue : function(value){
		if(typeof value==='object'){
			return value.formatDate(this.format);
		}
		return (new Date(Date.parse(value))).formatDate(this.format);
	},
	validate : function(value){
		return !isNaN(Date.parse(value));
	},
	setWidth : function(width){
		this._el.width(width);
		if(this.title)
			width-=this.labelWidth;
		this.input.width(width-($.browser.msie?23:22));	
	},
	setValue : function(val){
		if(val===null){
			val='';
		}
		if(typeof val==='object'){
			if(val.date){
				val=val.date.split('.')[0].split(' ')[0];
			}
		}
		if(this.validate(val)!==false||val==''||(this.epoch&&(typeof val == 'number'||new Date(parseInt(val)*1000) instanceof Date))){
			if(this.epoch){
				if(val!=''&&val!=null){
					if(typeof val == 'string' && val.indexOf('/')>-1){
						var valx=val.split('/');
						if(valx.length==3){
							var tmp = [];
							tmp.push(valx[1]);
							tmp.push(valx[0]);
							tmp.push(valx[2]);
							this.value = new Date(tmp.join('/')).getTime()/1000;
						}else
							this.value = '';
					}else if(typeof val == 'number'){
						var tmp = new Date(val*1000);
						var dx = this.validate(tmp);
						if(dx!==false){
							this.value = val;
							this.setElValue(tmp.formatDate(this.format));
							return;
						}else{
							
						}
					}else if(new Date(parseInt(val)*1000) instanceof Date){
						var tmp = new Date(parseInt(val)*1000);
						var dx = this.validate(tmp);
						if(dx!==false){
							this.value = val;
							this.setElValue(tmp.formatDate(this.format));
							return;
						}else{
							
						}
					}
				}else
					this.value = 0;
			}else
				this.value = val;
			this.setElValue(val);
		}
	},
	setElValue : function(val){
		if(this.epoch&&(val==''||val==0)){
			this.input[0].value='';
			return;
		}
		this.input[0].value = val;
	}
};
mj.extend(mj.form.dateField, mj.form.triggerField);
//!!! myjui/source/form/fileInput.js
mj.form.fileInput = function(config){
	mj.form.fileInput.superclass.constructor.call(this,config);
	this.init();
};
mj.form.fileInput.prototype = {
	componentClass : 'mj.form.fileInput',
	itemCls : 'mj-form-item',
	titleCls : 'mj-form-caption',
	inputCls : 'mj-form-input',
	maxFile : 0,
	accept : false, //'jpg|gif|png'
	hidden : false,
	left : false,
	top : false,
	//accept : '',
	render : function(){
		var t = this, n = mj.NE, v='';
		if(t.hidden)
			t.itemCls += ' mj-invisible';
		if(t.left||t.top){
			t.left = typeof t.left != 'undefined' ? (isNaN(t.left) ? t.left : (t.left+'px')) : '';
			t.top = typeof t.top != 'undefined' ? (isNaN(t.top) ? t.top : (t.top+'px')) : '';
			v = 'position:absolute;left:'+t.left+';top:'+t.top;
		}
		var el = t.el = n(t.renderTo, {cls:t.itemCls,style:v}), _el = t._el = t.destroyEl = $(el);
		t.titleEl = n(el, {html:this.title, cls:t.titleCls, style:'width:'+t.labelWidth+';text-align:'+t.labelAlign});
		t.input = $(n(el, {tag:'input',type:'file',cls:t.inputCls,name:'file1[]',accept: t.accept}));
		t.input.MultiFile({
			max: t.maxFile,
			accept: t.accept,
			afterFileSelect : function(element, value, master){
				
			},/*
onFileSelect:
onFileAppend: 
afterFileAppend: 
afterFileSelect: 
onFileRemove: 
afterFileRemove: 
*/

			STRING: {
				remove:'Kaldır',
				selected:'Seçilen: $file',
				denied:'Geçersiz dosya tipi: $ext!'
			}
		});

		t.trigger('render', t);
	},
	init : function(){
		var t = this;

		if(t.renderTo)
			t.render();
		mj.form.fileInput.superclass.init.call(t);
	},
	getValue : function(raw){
		return '_isFile';
	},
	setValue : function(val){
		return;
	},
	setElValue : function(val){
		return;
	},
	clear : function(){
		var t = this;
		//console.log(t.input[0].MultiFile)
		t.input[0].MultiFile.list.find('a.MultiFile-remove').click();
		//t.input[0].MultiFile.reset();
		return;
	},
	validate : function(val){
	}
};
mj.extend(mj.form.fileInput, mj.form.field);
//!!! myjui/source/form/fileInput2.js
mj.form.fileInput2 = function(config){
	mj.form.fileInput2.superclass.constructor.call(this,config);
	this.init();
};
mj.form.fileInput2.prototype = {
	componentClass : 'mj.form.fileInput2',
	itemCls : 'mj-form-item mj-file2',
	titleCls : 'mj-form-caption',
	inputCls : 'mj-form-input',
	maxFiles : false,
	acceptedFiles: null,
	acceptedMimeTypes: null,
	maxFilesize: 256,
	maxFiles: null,
	paramName: "file",
	left : false,
	top : false,
	accept : '',
	url: '',
	width: 400,
	//path: 'files',
	_path: 'files/{id}',
	onItemDeleteClick : function(file){
		var t=this;
		$.ajax({
		    url: t.url,
		    type: "POST",
		    data: { 
		    	"className" : "kkController", 
		    	"methodName" : "dropDelete", 
		    	"fileName" : file.serverName, 
		    	"path" : file.serverPath,
		    	"recordId" : t.recordId,
		    	"table" : t.module.table,
		    	"dataIndex" : t.dataIndex.replace("fi_", ""),
		    	"newVal" : t.getValue(true)
		   	},
		    success: function(response){
		    	// console.log(response);
		    }
		});
	},
	onItemDownloadClick : function(file){ 
		var t = this;
		mj.newWindow({width : 100,height : 100,url:t.url+'?className=loader&methodName=down&mId='+t.mId+'&path='+file.serverPath+'&name='+file.name});
		//console.dir(file); 
	},
	onItemViewClick : function(file){ 
		var t = this;
		mj.newWindow({url:t.url+'?className=loader&methodName=load&mId='+t.mId+'&path='+file.serverPath+'&name='+file.name});
		//console.dir(file); 
	},
	render : function(){
		var t = this, n = mj.NE, v='';
		if(t.hidden)
			t.itemCls += ' mj-invisible';
		if(t.left||t.top){
			t.left = typeof t.left != 'undefined' ? (isNaN(t.left) ? t.left : (t.left+'px')) : '';
			t.top = typeof t.top != 'undefined' ? (isNaN(t.top) ? t.top : (t.top+'px')) : '';
			v = 'position:absolute;left:'+t.left+';top:'+t.top;
		}
		var el = t.el = n(t.renderTo, {cls:t.itemCls,style:v}), _el = t._el = $(el);
		
			t.titleEl = n(el, {html:this.title, cls:t.titleCls, style:'width:'+t.labelWidth+';text-align:'+t.labelAlign});
			t.zoneId = mj.genId('zone-');
			// t.form.form.attr('id', t.zoneId);
			// t.form.form.addClass('dropzone');
			t.input = $(n(el, {id:t.zoneId, cls:'dropzone dropzone-previews2 dz-message '+t.inputCls, style:'width: '+t.width+'px;'}));
			// https://github.com/enyo/dropzone/wiki/Combine-normal-form-with-Dropzone
			// Dropzone.options[t.zoneId] = {
			// console.log(t.renderTo.parents('.mj-win-cc')[0]);
			// new Dropzone('#'+t.zoneId, {
			Dropzone.autoDiscover = false;
			t.dropzone = new Dropzone('#'+t.zoneId/*t.renderTo.parents('.mj-win-cc')[0]*/, {
				autoProcessQueue: true,
				uploadMultiple: true,
				parallelUploads: 100,
				maxFiles: t.maxFiles,
				maxFilesize: t.maxFilesize,
				paramName: t.paramName,
				acceptedFiles: t.acceptedFiles,
				acceptedMimeTypes: t.acceptedMimeTypes,
				url: t.url+'?className=kkController&methodName=dropUpload&path={path}',
				previewsContainer : '#'+t.zoneId,//+' .dropzone-previews',
				clickable : '#'+t.zoneId,//+' .dropzone-previews',
				//previewTemplate: "<div class=\"dz-preview2\">\n	<div class=\"dz-fileName\"><span data-dz-name></span></div>\n	<div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n	<div class=\"dz-success-mark\"><span>?</span></div>\n	<div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>",
				previewTemplate: "<div class=\"dz-preview2\">\n		<div class=\"dz-document\"></div>\n	<div class=\"dz-controls\"><div class=\"dz-control\"><div class=\"mj-button-icon mj-zoom\"></div></div><div class=\"dz-control\"><div class=\"mj-button-icon mj-delete\"></div></div><div class=\"dz-control\"><div class=\"mj-button-icon mj-save\"></div></div></div>\n	<div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n		<div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>",
				dictDefaultMessage: "Yüklemek istediğiniz dosyaları buraya sürükleyin",
				dictFallbackMessage: "Tarayıcınız sürükle & bırak dosya yüklemeyi desteklemiyor.",
				dictFallbackText: "Dosyaları karşıya yüklemek için alttaki formu kullanabilirsiniz.",
				dictFileTooBig: "Dosya çok büyük ({{filesize}}MB). Max dosya sınırı: {{maxFilesize}}MB.",
				dictInvalidFileType: "Bu türde dosya yükleyemezsiniz.",
				dictResponseError: "HATA: Sunucu {{statusCode}} kodu döndürdü.",
				dictCancelUpload: "Yüklemeyi iptal et",
				dictCancelUploadConfirmation: "Yüklemeyi iptal etmek istediğinizden emin misiniz?",
				dictRemoveFile: "Dosyayı sil",
				dictMaxFilesExceeded: "Bu alana daha fazla dosya yükleyemezsiniz.",
				uploadprogress: function(file, progress, bytesSent) {
					var node, _i, _len, _ref, _results;
					if (file.previewElement) {
						_ref = file.previewElement.querySelectorAll("[data-dz-uploadprogress]");
						_results = [];
						for (_i = 0, _len = _ref.length; _i < _len; _i++) {
							node = _ref[_i];
							_results.push(node.style.width = "" + progress + "%");
						}
						if(progress == 100)
							$('.dz-progress', file.previewElement).hide();
						return _results;
					}
				},
				error: function(file, message) {
					var node, _i, _len, _ref, _results;
					alert(message);
					if (file.previewElement) {
						file.previewElement.remove();
					}
				},
				// The setting up of the dropzone
				init: function() {
					var myDropzone = this;
					/*
					this.on("sendingmultiple", function() {
						// Gets triggered when the form is actually being sent.
						// Hide the success button or the complete form.
					});
					this.on("successmultiple", function(files, response) {
						// Gets triggered when the files have successfully been sent.
						// Redirect user or notify of success.
						// console.log(response);
						// for(var i=0;i<files.length;i++)
						// 	files[i].name = response;
						// console.dir(files);
					});
					this.on("errormultiple", function(files, response) {
					});
					*/
					this.on("sendingmultiple", function(files) {
						t.trigger("sending", files);
					});
					this.on("completemultiple", function(files) {
						t.trigger("complete", files);
					});

					this.on("success", function(file, response) {
						// console.log(response);
						// for(var i=0;i<files.length;i++)
						// 	files[i].name = response;
						eval('r = '+response);
						if(r.success){
							file.serverName = r.serverName;
							file.serverPath = r.serverPath;
						}else{
							file.previewElement.remove();
							alert(r.msg);
						}
					});
					this.on("addedpreviewelement", function(file) {
						if(!t.maxFiles || (t.maxFiles && myDropzone.files.length<=t.maxFiles)){
							if(t._settingValue)
								myDropzone.files.push(file);
							file.btnDelete = $('.mj-delete', file.previewElement);
							file.btnDownload = $('.mj-save', file.previewElement);
							file.btnView = $('.mj-zoom', file.previewElement);
							file.btnDelete.click(function(){
								myDropzone.files = t._without(myDropzone.files, file);
								myDropzone.emit("removedfile", file);
								t.onItemDeleteClick(file);
								if (myDropzone.files.length === 0) {
									return myDropzone.emit("reset");
								}
							});
							file.btnDownload.click(function(){
								t.onItemDownloadClick(file);
							});
							file.btnView.click(function(){
								t.onItemViewClick(file);
							});
							$('.dz-spacer', myDropzone.previewsContainer).remove();
							$('<div class="dz-spacer" style="clear:both;"></div>').appendTo(myDropzone.previewsContainer);
						}else{
							if(file.previewElement)
								file.previewElement.remove();
							myDropzone.files = t._without(myDropzone.files, file);
						}
					});
				}
			});
			// t.dropzone.on("complete", (function(_this) {
			// 	return function(file) {
			// 	};
			// })(t.dropzone));
			t.form.form.append('<div style="clear:both;"></div>');
			// }
		
		t.trigger('render', t);
	},
	_without: function(list, rejectedItem) {
		var item, _i, _len, _results;
		_results = [];
		for (_i = 0, _len = list.length; _i < _len; _i++) {
			item = list[_i];
			if (item !== rejectedItem) {
				_results.push(item);
			}
		}
		return _results;
	},
	init : function(){
		var t = this;
		if(!t._init){
			t._init = true;
			if(!t.module && mj._renderingModule)
				t.module = mj._renderingModule;
			if(t.renderTo)
				t.render();
			mj.form.fileInput2.superclass.init.call(t);
		}
	},
	getWordsBetweenCurlies : function(str) {
		var results = [], re = /{([^}]+)}/g, text;
		while(text = re.exec(str))
			results.push(text[1]);
		return results;
	},
	getValue : function(withoutTmp){
		var t=this, dz = t.dropzone, f = [];
		for(var i=0;i<dz.files.length;i++){
			if(!withoutTmp || dz.files[i].serverPath != '_tmp')
				f.push(dz.files[i].serverPath+'|-|'+dz.files[i].serverName);
		}
		return f.join('||');
	},
	setValue : function(val){
		var t=this, dz = t.dropzone;
		t._settingValue = true;
		//form üstünden gelen setValue esnasında ilgili kaydın id değeri alınarak path oluşturuluyor
		if(t._path){
			var params = t.getWordsBetweenCurlies(t._path);
			for(var i=0;i<params.length;i++){
				if(t.module && t.module.grids && t.module.grids.records && t.module.grids.records.selectedRow){
					t.recordId = t.module.grids.records.selectedRow.data.id;	
					t.path = t._path.replace('{'+params[i]+'}', t.module.grids.records.selectedRow.data[params[i]]);
				}
			}
			if(!t._url)
				t._url = dz.options.url;
			dz.options.url = t._url.replace('{path}', t.path);
		}
		t.clear();
		if(val){
			val = val.split('||');
			for(var i=0;i<val.length;i++){
				var file = val[i].split("|-|");
				var _f = { name: file[1], serverName: file[1], serverPath: file[0], size: 1 };
				dz.options.addedfile.call(dz, _f);
				dz.options.success.call(dz, _f);
				dz.options.thumbnail.call(dz, _f, _f.name);
			}
		}
		t._settingValue = false;
		return true;
	},
	setElValue : function(val){
		return;
	},
	setPath : function(id){
		this.path = this._path.replace('{id}', id);
	},
	clear : function(){
		var dz = this.dropzone;
		dz.removeAllFiles();
		dz.emit("reset");
		return true;
	},
	validate : function(val){
	}
};
mj.extend(mj.form.fileInput2, mj.form.field);
//!!! myjui/source/form/imageField.js
mj.form.imageField = function(config){
	mj.form.imageField.superclass.constructor.call(this,config);
};
mj.form.imageField.prototype = {
	componentClass : 'mj.form.imageField',
	inputCls : 'mj-image-field',
	triggerCls : 'mj-image-upload',
	width : 100,
	height : 100,
	init : function(){
		var t = this;
		if(t.renderTo)
			t.render();
		mj.form.imageField.superclass.init.call(t);
	},
	render : function(){
		var t = this, n = mj.NE, v = '';
		if(t.left||t.top){
			t.left = typeof t.left != 'undefined' ? (isNaN(t.left) ? t.left : (t.left+'px')) : '';
			t.top = typeof t.top != 'undefined' ? (isNaN(t.top) ? t.top : (t.top+'px')) : '';
			v = 'position:absolute;left:'+t.left+';top:'+t.top;
		}
		var el = t.el = n(t.renderTo, {cls:t.itemCls,style:v+';'+t.itemStyle}), _el = t._el = $(el);
		if(t.title)
			if(t.labelPos == 'left')
				t.titleEl = n(el, {html:this.title, cls:t.titleCls, style:'width:'+t.labelWidth+';text-align:'+t.labelAlign});
		t.input = $(n(el, {tag:'img',id:(t.id||mj.genId('mj-form-item-')),style:''+(t.height?'height:'+t.height+'px;':'')+(t.width?'width:'+t.width+'px;':''),type:t.type,cls:t.inputCls, src:mj.glb.blankImage}));
		t.triggerEl = $(n(el, {cls:t.triggerCls, html:mj.insertSpacer(16,16)}));
		t.triggerEl.hover(function(){
			t.triggerEl.addClass('mj-image-upload-hover');
		},function(){
			t.triggerEl.removeClass('mj-image-upload-hover');
		});
		t.input.click(function(){
			t.trigger('click', t, t.value!==undefined&&t.value!==null&&t.value!='');
		});
		t.triggerEl.click(function(){
			if(t.trigger('triggerclick', t, t)!==false)
				t._triggerClick.call(t, t);
		});
	},
	getElValue : function(){
		return this.input.attr('src');
	},
	setElValue : function(val){
		//this.input.attr('src', val);
		mj.get(this.id).src=val;
	},
	setValue : function(val){
		this.value = val;
		this.setElValue(val?val:mj.glb.blankImage);
	},
	_triggerClick : function(imageField){
		return true;
	}
};
mj.extend(mj.form.imageField, mj.form.field);
//!!! myjui/source/form/imageFieldRFM.js
mj.form.imageFieldRFM = function(config){ //Responsive File Manager
	mj.form.imageFieldRFM.superclass.constructor.call(this,config);
};
mj.form.imageFieldRFM.prototype = {
	componentClass : 'mj.form.imageFieldRFM',
	render : function(){
		var t = this;
		mj.form.imageFieldRFM.superclass.render.call(this);
		t.fakeElId = mj.genId('if-f-');
		t.fakeEl = $(mj.NE(t.renderTo, {tag:'input', type:'text', style:'display:none;', id:t.fakeElId}));
		t.fakeEl.on('change', function(){
			// console.log(t.fakeEl.val());
			// window.tt = t;
			var url = t.fakeEl.val();
			if(url)
				t.setValue(url.replace(/^.*\/\/[^\/]+/, ''));
		});
	},
	_triggerClick : function(imageField){
		$.fancybox({
			'padding' : 0,
			'type' : 'iframe',
			'href' : this.href ? this.href : '/yonetim/libs/filemanager/dialog.php?type=1&field_id='+this.fakeElId,
			'transitionIn' : 'elastic',
			'transitionOut' : 'elastic',
			'minWidth' : 850,
			'minHeight' : 500
		}); 
		return true;
	}
};
mj.extend(mj.form.imageFieldRFM, mj.form.imageField);
//!!! myjui/source/form/fieldSet.js
mj.form.fieldSet = function(config){
	mj.form.superclass.constructor.call(this, config);
};
mj.form.fieldSet.prototype = {
	componentClass : 'mj.form.fieldSet',
	fieldsetCls : 'mj-fieldset',
	legendCls : 'mj-legend',
	style : '',
	itemStyle : '',
	_onRender : function(){
		var f=this;
		f.fieldSetEl = f.destroyEl = $(mj.NE(f.renderTo, {tag : 'fieldset', cls : f.fieldsetCls, style : f.style+f.itemStyle+''+(f.height?'height:'+f.height+'px;':'')+(f.width?'width:'+f.width+'px;':'')}));
		var x = f.scope.form;
		if(f.title){
			f.legendEl = $(mj.NE(f.fieldSetEl,{tag:'legend', cls : 'mj-unselectable '+f.legendCls}));
			f.titleEl = $(mj.NE(f.legendEl,{tag:'span', html:f.title}));
		}
		f.innerEl = $(mj.NE(f.fieldSetEl,{tag:'div', style : 'overflow:auto;'+(f.height?'height:'+(f.height-10)+'px;':'')}));
		f.scope.form = f.innerEl;
		if(!f.items)
			f.items = [];
		else
			for(var i=0,l=f.items.length;i<l;i++){
				var item = f.items[i];
				f.scope.add(item);
			}
		f.scope.form = x;
		f.scope.fieldSets.push(f);
		mj.form.fieldSet.superclass.init.call(this);
	},
	render : function(scope){
		this.scope = scope;
		this._onRender();
	},
	clear : function(){
		$(this.items).each(function(){
			this.clear();
		});
	}
};

mj.extend(mj.form.fieldSet, mj.component);
//!!! myjui/source/form/fileTrigger.js
mj.form.fileTrigger = function(config){
	mj.form.fileTrigger.superclass.constructor.call(this,config);
};
mj.form.fileTrigger.prototype = {
	componentClass : 'mj.form.triggerField',
	render : function(){
		var t = this;
		mj.form.fileTrigger.superclass.render.call(this);
		t.fakeElId = mj.genId('if-f-');
		t.fakeEl = $(mj.NE(t.renderTo, {tag:'input', type:'text', style:'display:none;', id:t.fakeElId}));
		t.fakeEl.on('change', function(){
			window.tt = t;
			var url = t.fakeEl.val();
			if(url)
				t.setValue(url.replace(/^.*\/\/[^\/]+/, ''), true);
			// console.log(url.replace(/^.*\/\/[^\/]+/, ''));
			// console.log(t.getValue());
		});
	},
	setValue : function(val,trigger){
		if(this.clearOnBeforeSet&&!this._cleared){
			this._cleared=true;
			this.clear();
			this._cleared=false;
		}
		this.value = val;
		this.displayValue = this.lookupValue = val;
		this.setElValue(val);
		if(this.form && !this.form.loading)
			this.form.setModified(this);
	},
	handler : function(imageField){
		$.fancybox({
			'padding' : 0,
			'type' : 'iframe',
			'href' : '/yonetim/libs/filemanager/dialog.php?type=2&fldr=basinbulteni&field_id='+this.fakeElId,
			'transitionIn' : 'elastic',
			'transitionOut' : 'elastic',
			'minWidth' : 850,
			'minHeight' : 500
		}); 
		return true;
	}
};
mj.extend(mj.form.fileTrigger, mj.form.triggerField);
//!!! myjui/source/grid/grid.js
mj.grid = function(config){
	mj.grid.superclass.constructor.call(this, config);
};
mj.grid.prototype={
	componentClass : 'mj.grid',
	width:50,
	height:40,
	contextMenuWidth : 135,
	contextSubMenuWidth : 175,
	selectMode : 'row',
	fitToParent : false,
	selectedRow : false,
	loadMask : true,
	noWrap : true,
	rowRightClick : false,
	scroll : 'auto',
	sortable : false,
	_colIndex : -1,
	_sortDir : false,
	getSize : function(){
		if(this.fitToParent){
			this.height = this.initialRenderTo.height()||parseInt(this.initialRenderTo[0].style.height);
			this.width = this.initialRenderTo.width()||parseInt(this.initialRenderTo[0].style.width);
		}
	},
	doGrid : function(){
		if(this.initialRenderTo[0].offsetHeight>0){
			this.getSize();
			if(this.height!=this.lastHeight||this.width!=this.lastWidth){
				this.cnt1.width(this.width);
				this.cnt1.height(this.height);
				$(this.cnt).width(this.width);
				$(this.headerWrap).width(this.width);
				var newHeight = this.height-(this.pageBar?$(this.pageBar).height():0);
				$(this.cnt).height(newHeight);
				$(this.drag.el).height(newHeight-16);
				newHeight -= $(this.header).height();
				$(this.gridScroll).width(this.width);
				$(this.gridScroll).height(newHeight);
				//$(this.dataContainer).height(newHeight-16);
				$(this.drag.el).css('left','0px');
				$(this.drag.el).css('top','0px');
				this.lastWidth = this.width;
				this.lastHeight = this.height;
			}
		}
	},
	init : function(){
		var t = this;
		t.selectedRow={};
		this.initialRenderTo = this.renderTo;
		this.getSize();
		this.cnt = this.cnt1 = $(mj.NE(this.renderTo, {style:'width:'+this.width+'px;height:'+this.height+'px;overflow:hidden', cls:'mj-grid mj-unselectable'}));
		if(this.pbar&&typeof this.pbar.render=='function'){
			this.pbar.render({scope:this});
			this.cnt=$(this.renderTo);
			this.pbar.on('sourceload',function(){
				t.selectedRow = {};
			});
		}
		this.cnt.css('width',this.width+'px');
		this.cnt.css('overflow','hidden');
		//this.width = parseInt(this.cnt.width());
		var _h = parseInt(this.cnt.css('height'));
		this.height = _h>0 ? _h : this.height;
		var gridStyle ='', gi = this.gridId = mj.genId(), ce = $.cssEngine, w = this.width, h = this.height;
		var cw = 0;
		this.clientHeight = 0;
		var _ci=0, _l=0, _cc=this.cm.length, _sw = this.width, t = this,_cw = this.width ;
		$(this.cm).each(function(){
			this.colIndex=_ci++;
			this.isLast = _ci==_cc;
			this.width = (this.isLast && (this.width+cw<_cw || !this.width)) ? _cw-cw-16 : (this.width ? this.width : 100);
			if(!this.hide){
				_l += this.width;
				cw += this.width;
			}
		});
		this._cw = cw;
		this.noWrapStyle = this.noWrap ? 'white-space : nowrap;':'';  		
		var pos = $.browser.msie?'':'position:relative';
		this.headerWrap =  mj.NE(this.cnt, {tag:'div', cls:'mj-grid-header-row',style:'width:'+this.width+'px;'});  
		this.header = mj.NE(mj.NE(this.headerWrap, {tag:'table', cellpadding:'0', cellspacing:'0', width:cw, style:pos, align:'left'}), {tag:'tr', cls:'mj-grid-header-row'}).parentNode;
		this.dragEl = $(mj.NE(this.cnt, {tag:'div', cls:'',style : 'position:absolute;width:2px;background:transparent;height:'+this.height+'px;'})); 
		this.drag = new mj.drag({
			el : this.dragEl[0],
			parent : this.cnt[0],
			dragType : 'h',
			cls : 'mj-drag-col'
		});
		this.drag.on('dragstop',function(e){
			var proxy=$(e.proxy), t = this;
			proxy=e.proxy[0].style;	
			tOfP=t.drag.el.offsetParent==mj.bd[0]?{left:0,top:0}:$(t.drag.el.offsetParent).offset();
			var el = $('td[name="column-'+t.curColumn+'"]',t.cnt);
			var eldiv = $('td[name="column-'+t.curColumn+'"] div.mj-cell-inner',t.cnt);
			var initWidth=t.cm[t.curColumn].width||60;
			var elOf=el.offset();
			var oldWidth = parseInt(el[0].style.width);
			var width = (parseInt(proxy.left)+tOfP.left) - (el.offset().left);
			var widthDiff = (width-oldWidth);
			$(t.header).width($(t.header).width() + widthDiff);
			$(t.dataContainer).width($(t.dataContainer).width() + widthDiff);
			el.css('width',width + 'px');
			eldiv.css('width',(width-10) + 'px');
			t.cm[t.curColumn].curWidth = width ;
		},this);
		var hd = t.header.rows[0];
		var _gs = this.gridScroll = mj.NE(this.cnt, {style:'position:relative;overflow:'+ this.scroll +';width:'+this.width+'px;height:'+(this.height-parseInt($(this.headerWrap).css('height')))+'px;'});
		var items = this._renderColumns();
		var tb = new mj.contextmenu({
			renderTo : mj.NE(mj.bd,{cls:'mj-grid-columns-menu'}),
			parent : hd,
			menuScope : this,
			width : this.contextMenuWidth,
			items : [
				{id:'_1', title:mj.lng.objects.grid.columns,subMenuWidth : this.contextSubMenuWidth, items : items},
				'|',
				{id:'_2', title:mj.lng.objects.grid.sortAZ,iconCls:'mj-menu-sort-asc',handler:function(){
					t.sort(t._contextCol,'ASC');
				}},
				{id:'_3', title:mj.lng.objects.grid.sortZA,iconCls:'mj-menu-sort-desc',handler:function(){
					t.sort(t._contextCol,'DESC');
				}}
			]
		});
		t.addRelated(tb);
		tb.on('itemtoggle',function(a,b,c,d){
			if(t.cm[b.index].hide)
				t.showColumn(b.index);
			else{
				t.hideColumn(b.index);
			}	
		},this);	
		this.clientWidth = cw;
		var _gd = this.dataContainer = mj.NE(_gs, {tag:'table', cellpadding:'0', cellspacing:'0', width:(cw)+'px', align:'left'});
		var _gh = this.header;
		$(_gs).scroll(function(e){
			$(_gd).css("top", -e.target.scrollTop);
			if(!$.browser.msie)
				$(_gh).css("left", -e.target.scrollLeft);
			else
				$(_gh).css("margin-left", -e.target.scrollLeft);
			$(_gd).css("left", -e.target.scrollLeft);
		});
		if(this.loadMask)
			this.mask = new mj.mask({el:this.cnt1[0]});
		this.store.on('beforeload', function(){
			if(this.loadMask)
				this.mask.show(50);
		}, this);
		this.store.on('load', this._onLoad, this);	
		this.store.on('load', function(){
			if(t.sortable)
			if(t.store.localSort||(t.pbar&&this.pbar.getPageCount()==1))
				if(t._colIndex!==-1)//if($('img',t.cnt.find('tr.mj-grid-header-row')).not($('div img.mj-invisible')).length>0){
					t.clientSort(t._colIndex,t._sortDir);
		}, this);
		this.store.on('sourcerefresh', function(){
			t._colIndex = -1;
			t._sortDir = false;
		}, this);
		this.on('rowclick', this.selectRow, this);
		this.on('cellclick', this.selectCell, this);
		if(t.pbar)
			this.addRelated(t.pbar);
		mj.grid.superclass.init.call(this);
		mj.bindResize(this.cnt1, this.doGrid, this);
	},
	clearSortImages : function(){
		$('img',this.cnt)
			.not($('div img.mj-invisible'))
			.addClass('mj-invisible')
			.removeClass('mj-sorter-asc')
			.removeClass('mj-sorter-desc');
	},
	setFieldSort : function(c,d){
		$('img',this.cm[c].el).removeClass('mj-invisible').addClass('mj-sorter-'+(String(d).toLowerCase()));
		this.cm[c].sortDir = d;
	},
	clientSort : function(c,d){
		this.clearSortImages();
		this.setFieldSort(c,d);
		this.store.sort(this.cm[c].dataIndex,d);
		this._onLoad();
	},
	remoteSort : function(c,d){
		var t = this;
		t.clearSortImages();
		var col = this.cm[c];
		t._sortParams = {
			sort : (col.table?col.table+'.':'')+(col.sortField||col.dataIndex),
			dir : d,
			colIndex : c
		};
		mj.apply(t.store.params,t._sortParams);
		t.store.on('load',function(){
			if(typeof this.params.colIndex != 'undefined'&&this.params.dir)
				t.setFieldSort(this.params.colIndex,this.params.dir);
		});
		t.load();
	},
	clearSort : function(){
		if(this.sortable){
			this.clearSortImages();
			delete this.store.params.colIndex;
			delete this.store.params.dir;
			delete this.store.params.sort;
		}
	},
	sort : function(c,d){
		if(this.sortable){
			this._colIndex = c;
			this._sortDir = d;
			if(this.store.localSort||(this.pbar&&this.pbar.getPageCount()==1))
				this.clientSort(c,d);
			else
				this.remoteSort(c,d);
		}else
			return false;
	},
	_renderColumn : function(colConfig, colIndex){
		var t = this, hd = t.header.rows[0];
		if(colConfig.hide)
			var hideCls = ' mj-invisible';
		else
			var hideCls = '';
		var x = colConfig.el = $(mj.NE(hd, {tag:'td',unselectable :'on',name : 'column-'+ (colIndex),cls : 'mj-unselectable mj-column-'+ (colIndex) + hideCls , html:'<div unselectable="on" class="mj-cell-inner mj-unselectable"><span class="mj-cell-inner-header-text">'+colConfig.header+'</span>'+mj.insertSpacer(13,10)+'</div>', style:'width:'+(colConfig.width-1)+'px'}));
		var sImg = $('img',x);
		sImg.attr('name','sorter-'+(colIndex));
		sImg.addClass('mj-unselectable mj-invisible mj-sorter');
		x.mouseover(function(e){
			var xOf=x.offset();
			if(e.clientX-xOf.left<6){
				t.curColumn = parseInt(colIndex);
				t.curColumn--;
				tOfP=t.drag.el.offsetParent==mj.bd[0]?{left:0,top:0}:$(t.drag.el.offsetParent).offset();
				t.drag.el.style.left = (xOf.left - tOfP.left) + 'px';
				t.drag.el.style.top = (xOf.top - tOfP.top) + 'px';
			}
		});
		x.click(function(e){
			var firstSort = typeof t.cm[colIndex].sortDir=='undefined';
			var dir = (firstSort||(!firstSort&&t.cm[colIndex].sortDir=='DESC'))?'ASC':'DESC';
			t.sort(colIndex,dir);
		});
		x.bind('contextmenu',function(){
			t._contextCol = colIndex;
		});
		return {index:colIndex ,title : colConfig.header,type : 'toggle',state : !colConfig.hide};
	},
	_renderColumns : function(){
		var t = this;
		var items = [], colIndex = 0;
		$(t.cm).each(function(){
			var retVal = t._renderColumn(this, colIndex++);
			if(retVal.title !== "" || mj.oLength(t.cm)-1 !== retVal.index)
				items.push(retVal);
		});
		return items;
	},
	selectRow : function(g, rowIndex, row){
		if(this.selectMode=='row'){
			if(this.selectedRow && this.selectedRow.el)
				this.selectedRow.el.removeClass('mj-grid-selected');
			//this.selectedRow.el = $(this.rows[rowIndex].el)
			this.selectedRow = this.rows[rowIndex];
			//this.selectedRow.data = this.store.data[rowIndex];
			this.selectedRow.index = rowIndex;
			this.selectedRow.el.addClass('mj-grid-selected');
		}
	},
	getSelected : function(){
		if(this.selectedRow)
			return this.selectedRow;
		else
			return false;
	},
	clearSelectedRow : function(){
		if(this.selectMode=='row'){
			if(this.selectedRow && this.selectedRow.el){
				this.selectedRow.el.removeClass('mj-grid-selected');
				this.selectedRow = {};
			}
		}
	},
	selectCell : function(g, rowIndex, colIndex, cell){
		if(this.selectMode=='cell'){
			if(this.selectedCell)
				this.selectedCell.removeClass('mj-grid-selected');
			this.selectedCell = cell;
			this.selectedCell.addClass('mj-grid-selected');
		}
	},
	getCellValue : function(rowIndex,colIndex){
		return this.rows[rowIndex].cols[colIndex].data;
	},
	setCellValue : function(rowIndex,colIndex,value,renderer,isHtml){
		var row = this.rows[rowIndex];
		var cell = row.cols[colIndex];
		var align = this.cm[colIndex].align || false;
		cell.data = value;
		row.data[cell.dataIndex] = value;
		if(renderer){
			value = renderer(value);
			if(align)
				value = '<div style="text-align:'+align+';">'+value+'</div>';
			if(isHtml)
				cell.el.find('div:first').html(value);
			else
				cell.el.find('div:first').text(value);
		}else if(typeof value == 'number' && !align){
			value = '<div style="text-align:right;">'+value+'</div>';
			cell.el.find('div:first').empty().append(value.toString());
		}else{
			if(align)
				value = '<div style="text-align:'+align+';">'+value+'</div>';
			cell.el.find('div:first').html(value);
		}
	},
	load : function(){
		this.store.load();
	},
	showColumn : function(index){
		var el = $('td[name="column-'+index+'"]',this.cnt);
		if (el.hasClass('mj-invisible')){
			var oldWidth = parseInt(el[0].style.width);
			var hw = $(this.header).width();
			var dw = $(this.dataContainer).width();
			el.removeClass('mj-invisible');
			$(this.header).width( hw+ oldWidth);
			$(this.dataContainer).width(dw+ oldWidth);				
		}
		this.cm[index].hide=false;

	},
	hideColumn : function(index){
		var el = $('td[name="column-'+index+'"]',this.cnt);
		if (!el.hasClass('mj-invisible')){
			var oldWidth = parseInt(el[0].style.width);
			var hw = $(this.header).width();
			var dw = $(this.dataContainer).width();
			el.addClass('mj-invisible');
			$(this.header).width( hw- oldWidth);
			$(this.dataContainer).width(dw- oldWidth);		
		}
		this.cm[index].hide=true;
	},
	_onLoad : function(){
		var g = this, cnt = this.dataContainer, cm = this.cm, gi = this.gridId;
		$(cnt).empty();
		if(typeof this.clearSelectedRow==='function'){
			this.clearSelectedRow();
		}
		var _r = this.rows = [], oeCls = 'odd';
		$(this.store.data).each(function(){
			oeCls = oeCls=='even' ? 'odd' : 'even';
			var r = mj.NE(cnt, {tag:'tr', cls:'mj-grid-data-row mj-'+oeCls}).rows, d = this, _ri=r.length-1;
			var r = r[_ri], _jqr=$(r);
			_jqr.data('_data', this);
			_jqr.click(function(){
				g.trigger('rowclick', g, _ri);
			});
			_jqr.dblclick(function(){
				g.trigger('rowdblclick', g, _ri);
			});
			if(g.rowRightClick){
				_jqr.bind('contextmenu',function(e){
					e.preventDefault();
					g.trigger('rowclick', g, _ri);
					//g.trigger('contextmenu', g, _ri);
				});
			}
			var colCount = 0;
			var robj = {};
			robj.cols = [];
			r.cols = [];
			$(cm).each(function(){
				if(g.cm[colCount].hide)
					var hideCls = ' mj-invisible';
				else
					var hideCls = '';
				//var tdwidth = (g.cm[colCount].curWidth||(this.isLast?this.width-(g.width<g._cw?0:1):this.width))-1;
				var tdwidth = (g.cm[colCount].curWidth||this.width)-1;
				var data = (!(d[this.dataIndex]==null || d[this.dataIndex]==undefined)?d[this.dataIndex]:(typeof this.renderer==='function'?d[this.dataIndex]:'&nbsp;'));
				if (typeof data=='string'&&(!g.store.renderer||!g.store.renderer[this.dataIndex]))
					data = data.replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
				if(data!==null && typeof data==='object'){
					if(data.date){
						switch(this.render) {
							case 'date':
								data=data.date.split('.')[0].split(' ')[0]
								break;
							case 'datetime':
								data=data.date.split('.')[0];
								break;
							case 'time':
								data=data.date.split('.')[0].split(' ')[1]
								break;
							default:
								data=data.date.split('.')[0].split(' ')[0]
						}
					}
				}
					
				var cell = $(mj.NE(r, {tag:'td',name : 'column-'+ (colCount),cls : 'mj-column-'+ (colCount), style:($.browser.mozilla?'height:23px;':'')+'width:'+tdwidth+'px', cls:'mj-cell mj-grid'+gi+'-col-'+this.colIndex + hideCls, html:'<div class="mj-cell-inner" unselectable="on" style="'+g.noWrapStyle+'width:'+(tdwidth-10)+'px;"></div>'}));
				var cellInner = $('.mj-cell-inner',cell);
				var align = g.cm[colCount].align || false;
				if (this.renderer)
					data = this.renderer(data,d,cellInner);
				else if(typeof data == 'number' && !align)
					data = '<div style="text-align:right;">'+data+'</div>';
				if(align)
					data = '<div style="text-align:'+align+';">'+data+'</div>';
				cellInner.append(data.toString());
				robj.cols.push({el:cell,data:d[this.dataIndex],dataIndex:this.dataIndex});
				var _ci = r.cols.push(cell);
				if(g.selectMode=='cell'){
					cell.click(function(e){
						e.stopPropagation();
						g.trigger('cellclick', g, _ri, _ci, cell);
					});
					cell.dblclick(function(e){
						e.stopPropagation();
						g.trigger('celldblclick', g, _ri, _ci, cell);
					});
				}
				colCount++;
			});
			_jqr.mouseover(function(){
				$(this).addClass("mj-grid-hover");
			});
			_jqr.mouseout(function(){
				$(this).removeClass("mj-grid-hover");
			});
			robj.el = $(r);
			robj.data = d;
			_r.push(robj);
		});
		if(this.loadMask)
			this.mask.hide();
		this.trigger('afterload', this);
	},
	getValue : function(rowIndex){
		var r = [], t = this;
		if(rowIndex)
			r.push(t.store.data[rowIndex]);
		else
			r=t.store.data;
		return r;
	},
	_saveLoadTrigger : function(store){
		store.params.limit = store._limitBackup;
		store.params.current = store._currentBackup;
		store.mon('load', this._saveLoadTrigger);
		this._saveAsXLS(this._filename);
	},
	saveAsXLS : function(filename){
		var t = this;
		if(parseInt(t.store.recordCount)!=t.store.data.length){
			t.store._limitBackup = t.store.params.limit;
			t.store._currentBackup = t.store.params.current;
			t.store.params.limit = parseInt(t.store.recordCount);
			t.store.params.current = 1;
			t._filename = filename;
			t.store.on('load', t._saveLoadTrigger, this);
			t.store.load();
		}else
			this._saveAsXLS(filename);
	},
	_saveAsXLS : function(filename){
		var t = this, n=mj.NE;
		var copyTo = $(n(mj.bd, {style:'display:none'}));
		copyTo.append(this.cnt[0].innerHTML);
		$('td',copyTo).attr('height', '22px');
		var tbl = $('table',copyTo);
		filename = filename || mj.genId('rapor')+'xls';
		tbl.attr('border', '0');
		tbl.css('border-left', '1px solid #000');
		tbl.css('border-top', '1px solid #000');
		$('td', tbl).css('border', '1px solid #000');
		$(tbl[1]).prepend($('tr',tbl[0]));
		$(tbl[0]).remove();
		var content = (t.fileTitle ? t.fileTitle : '')+copyTo[0].innerHTML;
		content = mj.escape(content);
		var frmCnt = $(n(mj.bd, {style:'display:none'}));
		var frm = n(frmCnt, {tag:'form',action:mj.glb.exportPath,method:'POST',html:'<input type="hidden" name="format" value="xls"/><input type="hidden" name="content" value="'+content+'"/><input type="hidden" name="filename" value="'+filename+'"/>'});
		frm.submit();
		frmCnt.remove();
		copyTo.remove();
	},
	copyContent : function(copyTo){
		copyTo.append(this.cnt[0].innerHTML);
		copyTo[0].lastChild.style.height=copyTo[0].lastChild.scrollHeight+50;
		copyTo[0].lastChild.style.overflowX="";
		copyTo[0].lastChild.style.overflowY="";
		copyTo[0].lastChild.style.height=copyTo[0].lastChild.scrollHeight+50;
		$('td',copyTo).attr('height', '0');
		var tbl = $('table',copyTo);
		tbl.attr('border', '0');
		tbl.css('border-left', '1px solid #000');
		tbl.css('border-top', '1px solid #000');
		$('td', tbl).css('border-right', '1px solid #000');
		$('td', tbl).css('border-bottom', '1px solid #000');
		$(tbl[1]).prepend($('tr',tbl[0]));
		$(tbl[0]).remove();
	},
	setTitle : function(colIndex, title){
		var cmItem = this.cm[colIndex];
		$('.mj-cell-inner-header-text', cmItem.el).html(title);
		cmItem.header = title;
	}
};
mj.extend(mj.grid, mj.component);
//!!! myjui/source/layout/layout.js
/** 
 * @fileoverview Layout nesnesi oluşturan sınıf.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.0.1 Beta 18.10.2007
 */
 /**
 * Yeni bir Layout nesenesi oluşturur.<br>
 * @example

 * @class
 * @name mj.Layout
 * @extends mj.component
 * @param {Object} config Tab objesini oluşturabilmek için gerekli olan parametreleri içerir.
 * @config {HTMLElement} renderTo Tab nesnesinin render edileceği container nesnesi.
 */
mj.layout = function(config){
	mj.layout.superclass.constructor.call(this, config);
};
mj.layout.prototype={
	componentClass : 'mj.layout',
	initialNorth : 50,
	initialWest : 100,
	initialEast : 100,
	initialSouth : 100,
	centerBorder : 'ltrb',
	createRegion : function(elem){
		var split=false,collapser=false,size=0;
		var style1='height',style2='width',style3='top',style4='left',initSize,elStyle;
		var collapseCls = elem.collapsed?'collapsed':'expanded',mxh=0,mnh=0,mxw=0,mnw=0;
		var cnt=mj.NE(this.renderTo,{tag:'div',id:(this.masterId+'-region-'+elem.region),cls:'border-panel '+elem.region+' '+(elem.collapsed?'mj-invisible':'')});
		elem.dType = 'v';
		switch (elem.region){
			case 'north':
				initSize=elem.collapsed?0:(elem.initial||initialNorth);
				size=elem.collapsed?(elem.initial||initialNorth):0;
				mxh=(elem.max||elem.initial)+this.borderSize,mnh=(elem.min||elem.initial)+this.borderSize;
				break;
			case 'south':
				initSize=elem.collapsed?0:(elem.initial||initialSouth);
				size=elem.collapsed?(elem.initial||initialSouth):0;
				//mxh=($(this.renderTo).height()-((elem.min||elem.initial)+(elem.split?this.splitSize:0))),mnh=($(this.renderTo).height()-((elem.max||elem.initial)+(elem.split?this.splitSize:0)));
				break;
			case 'west':
				style1='width',style2='height',style3='left',style4='top',elem.dType='h';
				initSize=elem.collapsed?0:(elem.initial||initialWest);
				size=elem.collapsed?(elem.initial||initialWest):0;
				mxw=(elem.max||elem.initial)+this.borderSize,mnw=(elem.min||elem.initial)+this.borderSize;
				break;
			case 'east':
				style1='width',style2='height',style3='left',style4='top',elem.dType='h';
				initSize=elem.collapsed?0:(elem.initial||initialEast);
				size=elem.collapsed?(elem.initial||initialEast):0;
				//mxw=($(this.renderTo).width()-((elem.min||elem.initial)-this.borderSize+(elem.split?this.splitSize:0))),mnw=($(this.renderTo).width()-((elem.max||elem.initial)-this.borderSize+(elem.split?this.splitSize:0)));
		}
		//collapseCls+='-'+elem.dType;
		elStyle = style4+':50%;'+style1+':5px;'+style2+':40px;';
		cnt.style[style1]=initSize+'px';
		cnt.style[style2]='100%';
		elem.scope=this;
		if(elem.split){
			split=mj.NE(this.renderTo,{tag:'div',id:cnt.id+'-splitter',cls:'layout-split splitbar-'+elem.dType+(' split-'+elem.region)});
			if(elem.collapsible){
				collapser=mj.NE(split,{tag:'div',id:cnt.id+'-collapser',cls:'collapser '+collapseCls,style:'position:absolute;'+elStyle});
				var clps = $(collapser);
				clps.hover(function(){
					if(clps.hasClass('expanded'))
						clps.addClass('ex-hover');
					else
						clps.addClass('hover');
				},function(){
					clps.removeClass('ex-hover');
					clps.removeClass('hover');
				});
			}
			var drag = new mj.drag({
				el : split,
				maxHeight : mxh,
				maxWidth : mxw,
				minHeight : mnh,
				minWidth : mnw,
				dragType : elem.dType
			});
			drag.on('beforedrag',function(e){
				return !this.scope.regions[this.region].collapsed;
			},elem);
			drag.on('dragstop',function(e){
				this.scope.refreshLayout(e,this.region);
			},elem);
			split.style[style1]=this.splitSize+'px';
			split.style[style2]='100%';
			split.style[style3]=initSize+'px';
		}
		if(elem.collapsible)
			$(collapser).bind('mousedown',{region:elem.region,t:this},function(e){
				e.stopPropagation();
				try{
		            this.id.indexOf('collapser');
		        }catch(e){
					return;
				}
				var reg=e.data.region;
				e.data.t._toggleRegion(reg,!e.data.t.regions[reg].collapsed);
			});
		this.regions[elem.region]={
			region:elem.region,
			config:elem,
			container:cnt,
			splitter:split||elem.split,
			collapser:collapser,
			collapsible:elem.collapsible,
			drag:drag,
			dType:elem.dType,
			collapsed:elem.collapsed||false,
			size:size,
			initial:initSize,
			hiding:false,
			getBody:function(){return cnt;},
			collapse:function(){this.scope._toggleRegion(this.region,true);},
			expand:function(){this.scope._toggleRegion(this.region,false);},
			hide:function(){this.scope._hideRegion(this.region);},
			show:function(){this.scope._showRegion(this.region)},
			scope:this
		};
	},
	addRegion : function(el){
		if(!this.regions[el.region]){
			if(!this.items)
				this.items = [];
			this.items.push(el);
			this.createRegion(el);
			this.doLayout();
		}
	},
	getStyleName : function(region){
		var style='width';
		switch (region){
			case 'north':
			case 'south':
				style='height';
		}
		return style;
	},
	_hideRegion : function(region){
		var style=this.getStyleName(region);
		if(this.regions[region]&&region!='center'){
			var re=this.regions[region];
			re.container.style[style]='0px';
			if(re.splitter){
				re.splitter.style[style]='0px';
				$(re.splitter).addClass('mj-invisible');
			}
			$(re.container).addClass('mj-invisible');
			this.regions[region].hiding=true;
			this.doLayout();
		}
	},
	_showRegion : function(region){
		var style=this.getStyleName(region);
		if(this.regions[region]&&region!='center'){
			var re=this.regions[region];
			re.container.style[style]=(re.collapsed?0:re.initial)+'px';
			if(re.splitter){
				re.splitter.style[style]=this.splitSize+'px';
				$(re.splitter).removeClass('mj-invisible');
			}
			if(!re.collapsed)
				$(re.container).removeClass('mj-invisible');
			this.regions[region].hiding=false;
			this.doLayout();
		}
	},
	_toggleRegion : function(region,ce){//ce:collapse-expand
		if(this.regions[region]&&region!='center'){
			var el = this.regions[region];
			if(el.collapsible){
				if(el.collapsed!=ce){
					var cont = el.container;
					$(cont).toggleClass('mj-invisible');
					$(el.collapser).swapClass('collapsed','expanded');
					switch (region){
						case 'north':
						case 'south':
							var val=el.collapsed?0:$(cont).height();
							cont.style.height=el.size+'px';
							break;
						default:
							var val=el.collapsed?0:$(cont).width();
							cont.style.width=el.size+'px';
					}
					el.collapsed=!el.collapsed;
					el.size=val;
					this.doLayout();
					this.trigger('toggle', this, el, ce);
				}
			}
		}
	},
	/**
	 * layout nesnesi init fonksiyonu.
	 * @name init
	 * @function
	 * @type {Function}
	 */
	init : function(){
		this.splitSize = 5;
		this.borderSize = 1;
		var cnt;
		this.regions={};
		$(this.renderTo).addClass('border-layout-ct mj-resize-handle');
		var rnrdt=this.renderTo[0].style;
		rnrdt.width="100%";
		rnrdt.height="100%";
		rnrdt.position="relative";
		rnrdt.overflow="hidden";
		this.masterId=this.renderTo.id||mj.genId('layout-');
		cnt=mj.NE(this.renderTo,{tag:'div',id:(this.masterId+'-region-center'),cls:'border-panel center',style:'width:'+this.renderTo.width()+'px;height:'+this.renderTo.height()+'px;'});
		if(this.centerBorder.length<4){
			if(this.centerBorder.indexOf('l')==-1)
				$(cnt).css('border-left','0');
			if(this.centerBorder.indexOf('t')==-1)
				$(cnt).css('border-top','0');
			if(this.centerBorder.indexOf('r')==-1)
				$(cnt).css('border-right','0');
			if(this.centerBorder.indexOf('b')==-1)
				$(cnt).css('border-bottom','0');
		}
		this.regions['center']={
			container :cnt,
			getBody:function(){return cnt;},
			splitter :false
		};
		if(this.items)
			for(var i in this.items)
				if(typeof this.items[i]!='function')
					this.createRegion(this.items[i]);
		this.doLayout(true);
		$(window).bind("resize", {scope:this},function(e){
			e.data.scope.doLayout();
		});
		mj.bindResize(this.renderTo, this.doLayout, this);
	},
	getBody : function(region){
		if(this.regions[region])
			return this.regions[region].getBody();
	},
	refreshLayout : function(e,region){
		var proxy=$(e.proxy);
		proxy=e.proxy[0].style;
		var container=this.regions[region].container.style;
		switch (region) {
			case 'north':
				container.height=(parseInt(proxy.top)-this.borderSize)+'px';
				break;
			case 'west':
				container.width=(parseInt(proxy.left)-this.borderSize)+'px';
				break;
			case 'south':
				container.height=(this.renderTo.height()-parseInt(proxy.top)-this.borderSize-this.splitSize)+'px';
				break;
			case 'east':
				container.width=(this.renderTo.width()-parseInt(proxy.left)-this.borderSize-this.splitSize)+'px';
				break;
		}
		this.doLayout();
	},
	doLayout : function(bool){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		var rndT = this.renderTo[0],c = false, s = false,h = false,tRegs = this.regions,tRCS,ly = {regions:{}},size = 0;
		for(var r in tRegs)
			if(typeof tRegs[r] != 'function'){
				c = !!tRegs[r].collapsed;
				s = !!tRegs[r].splitter;
				h = !!tRegs[r].hiding;
				tRCS = tRegs[r].container.style;
				switch (r){
					case 'center':
						s = false;
						h = false;
						ly.regions[r]={height:rndT.offsetHeight, width:rndT.offsetWidth, top:0, left:0, collapse:c, split:s, hiding:h};
						break;
					case 'north':
						size = parseInt(tRCS.height);
						ly.regions[r]={height:size, width:'100%', top:0, left:0, collapse:c, split:s, hiding:h};
						break;
					case 'south':
						size = parseInt(tRCS.height);
						ly.regions[r]={height:size, width:'100%', top:0, left:0, collapse:c, split:s, hiding:h};
						break;
					case 'west':
						size = parseInt(tRCS.width);
						ly.regions[r]={height:0, width:size, top:0, left:0, collapse:c, split:s, hiding:h};
						break;
					case 'east':
						size = parseInt(tRCS.width);
						ly.regions[r]={height:0, width:size, top:0, left:0, collapse:c, split:s, hiding:h};
						break;
				}
			}
		var center = ly.regions.center,centerStyle = tRegs.center.container.style;
		var nr = ly.regions['north'];
		var nrs=0,srs=0,wrs=0,ers=0;
		if(nr){
			nrs = nr.hiding?0:((nr.split?this.splitSize:0)+this.borderSize);
			center.top+=nr.height+nrs;
			center.height-=nr.height+nrs;
		}else
			nr={height:0,width:0,top:0,left:0,collapse:false,split:false,hiding:false};
		var sr = ly.regions['south'];
		if(sr){
			srs = sr.hiding?0:((sr.split?this.splitSize:0)+this.borderSize);
			center.height-=sr.height+srs;
			ly.regions.south.top=center.height+nr.height+srs+nrs-this.borderSize;
		}else
			sr={height:0,width:0,top:0,left:0,collapse:false,split:false,hiding:false};
		var wr = ly.regions['west'];
		if(wr){
			wrs = wr.hiding?0:((wr.split?this.splitSize:0)+this.borderSize);
			center.left+=wr.width+wrs;
			center.width-=wr.width+wrs;
			ly.regions.west.top+=nr.height+nrs;
			ly.regions.west.height+=rndT.offsetHeight-(nr.height+sr.height+srs+nrs);
		}else
			wr={height:0,width:0,top:0,left:0,collapse:false,split:false,hiding:false};
		var er = ly.regions['east'];
		if(er){
			ers = er.hiding?0:((er.split?this.splitSize:0)+this.borderSize);
			center.width-=er.width+ers;
			ly.regions.east.top+=nr.height+nrs;
			ly.regions.east.height=rndT.offsetHeight-(nr.height+sr.height+srs+nrs);
			ly.regions.east.left+=wr.width+center.width+wrs+ers-this.borderSize;
		}else
			er={height:0,width:0,top:0,left:0,collapse:false,split:false,hiding:false};
		var regCon,regSpl,obj,obj2;
		var w = rndT.offsetWidth, h = rndT.offsetHeight;
		this.ly=ly;
		this.trigger('layoutbeforeresize',this);
		centerStyle.left=(center.left-(wr.hiding?0:wr.collapse?1:0))+'px';
		var www=(center.width-(2*this.borderSize)+(wr.hiding?0:wr.collapse?1:0)+(er.hiding?0:er.collapse?1:0));
		centerStyle.width=(www>0?www:0)+'px';
		var hhh=(center.height-(2*this.borderSize)+(nr.hiding?0:nr.collapse?1:0)+(sr.hiding?0:sr.collapse?1:0));
		centerStyle.height=(hhh>0?hhh:0)+'px';
		centerStyle.top=(center.top-(nr.hiding?0:nr.collapse?1:0))+'px';
		for(var r2 in ly.regions){
			if(typeof ly.regions[r2]!='function'&&r2!='center'){
				obj2=tRegs[r2];
				cfg=obj2.config
				regCon=obj2.container.style;
				obj=ly.regions[r2];
				switch (r2){
					case 'north':
						regCon.left=obj.left+'px';
						regCon.width=obj.width+'';
						regCon.height=obj.height+'px';
						regCon.top=obj.top+'px';
						if(ly.regions[r2].split){
							regSpl=obj2.splitter.style;
							regSpl.top=(obj.height+(obj.collapse?0:this.borderSize))+'px';
						}
						break;
					case 'south':
						regCon.left=obj.left+'px';
						regCon.width=obj.width+'';
						regCon.height=obj.height+'px';
						regCon.top=obj.top+'px';
						if(ly.regions[r2].split){
							regSpl=obj2.splitter.style;
							regSpl.top=(center.height+nr.height+nrs+(obj.collapse?this.borderSize:0))+'px';
							obj2.drag.maxHeight=(h-((cfg.min||obj2.initial)+this.borderSize+this.splitSize));
							obj2.drag.minHeight=(h-((cfg.max||obj2.initial)+this.borderSize+this.splitSize));
						}
						break;
					case 'west':
						regCon.left=obj.left+'px';
						regCon.width=(obj.width)+'px';
						regCon.height=centerStyle.height;
						regCon.top=centerStyle.top;
						if(ly.regions[r2].split){
							regSpl=obj2.splitter.style;
							regSpl.top=regCon.top;
							regSpl.height=(parseInt(regCon.height)+(2*this.borderSize))+'px';
							regSpl.left=(obj.width+(obj.collapse?0:this.borderSize))+'px';
						}
						break;
					case 'east':
						regCon.left=obj.left+'px';
						regCon.width=(obj.width)+'px';
						regCon.height=centerStyle.height;
						regCon.top=centerStyle.top;
						if(ly.regions[r2].split){
							regSpl=obj2.splitter.style;
							regSpl.top=regCon.top;
							regSpl.height=(parseInt(regCon.height)+(2*this.borderSize))+'px';
							regSpl.left=(obj.left-this.splitSize+(obj.collapse?this.borderSize:0))+'px';
							obj2.drag.maxWidth=(w-((cfg.min||obj2.initial)+this.borderSize+this.splitSize));
							obj2.drag.minWidth=(w-((cfg.max||obj2.initial)+this.borderSize+this.splitSize));
						}
				}
			}
		}
		this.renderTo.trigger('kkresize', {sourceEl:this});
		this.trigger('layoutafterresize',this);
	}
};
mj.extend(mj.layout, mj.component);

//!!! myjui/source/panel/panel.js
/** 
 * @fileoverview Panel nesnesi oluşturan sınıf.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.0.1 Beta 19.10.2007
 */
mj.panel = function(config){
	mj.panel.superclass.constructor.call(this, config);
};
mj.panel.prototype={
	componentClass : 'mj.panel',
	title : false,
	type : 'default',
	loaded : false,
	collapsible : false,
	border : false,
	collapsedCls : 'mj-panel-collapsed',
	disabled : false,
	fitToParent : true,
	buttonPos : 'bottom',
	init : function(){
		if(this.renderTo)
			this.render();
	},
	render : function(){
		var t = this;
		var id=t.id||mj.genId('kkpanel-');
		if(t.border&&!t.form)
			$(t.renderTo).css('border','1px solid #A3AEB7');
		if(!t.width)
			t.width = $(t.renderTo).width()||parseInt($(t.renderTo).css('width'));
		if(isNaN(t.width))
			t.width = 0;
		$(t.renderTo).css('width',t.width+'px');
		t.container = mj.NE(t.renderTo,{id:id+'-container',cls:'mj-unselectablex',style:t.itemStyle+'width:'+$(t.renderTo).css('width')+';overflow:hidden;'+(t.height?'':('height:'+($(t.renderTo).height())+'px;'))});
		$(t.container).attr('unselectable','on');
		if(t.border&&t.form)
			$(t.container).css('border','1px solid #A3AEB7');
		if(t.title){
			t.header = mj.NE(t.container,{id:id+'-header',cls:'mj-panel-header '+(t.collapsed?t.collapsedCls:'')});
			if(t.collapsible)
				t.collapseEl = mj.NE(t.header,{cls:'mj-tool mj-tool-toggle'});
			t.titleEl = mj.NE(t.header,{tag:'span',cls:["mj-title-text ", t.titleCls," mj-unselectable"].join(""),html:t.title});
			$(t.titleEl).attr('unselectable','on');
			$(t.header).attr('unselectable','on').addClass('mj-unselectable');
			if(t.collapsible){
				$(t.header).click(function(){
					if(!t.disabled){
						if(!t.collapsed)
							t.collapse();
						else
							t.expand();
						if(typeof t.handler=='function')
							t.handler();
					}
					});
			}
		}
		t.body = mj.NE(t.container,{id:id+'-body',cls:'mj-resize-handle '+(t.collapsed?'mj-invisible':''),style:'height:'+(t.height?t.height:$(t.container).innerHeight()-(t.title?24:0))+'px;width:100%;background-color:white;overflow:auto;',html:t.html||''});
		if(t.buttons){
			var x=t.buttons;
			t.buttons = [];
			if(t.attachTb){
				t.tbar = new mj.menu({
					renderTo:t.container,
					items : []
				});
				t.buttonContainer = t.tbar._el;
			}else{
				t.buttonContainer = $(mj.NE(t.container, {cls:'mj-panel-buttons-container'}));
				$(t.body).height($(t.body).height()-t.buttonContainer.height());
			}
			for(var i=0,l=x.length;i<l;i++)
				t.addButton.call(t,x[i]);
			if(t.collapsed)
				t.buttonContainer.addClass("mj-invisible");
		}
		t.getBody = function(){
			return t.body;
		};
		if(t.autoLoad){
			window.tmp = t.body;
			t.autoLoad.url += '?h=tmp';
			mj.load(t.body,t.autoLoad);
			t.loaded = true;
		}
		if(t.disabled)
			this.setDisable();
		mj.bindResize(t.container, t.doPanel, t);
		t.doPanel();
	},
	collapse : function(){
		this.collapsed = true;
		$(this.header).addClass("mj-panel-collapsed");
		$(this.body).addClass("mj-invisible");
		if(this.buttonContainer)
			this.buttonContainer.addClass("mj-invisible");
		this.height=this.renderTo.height();
		this.renderTo.height($(this.header).height()-1);
	},
	expand : function(){
		this.collapsed = false;
		$(this.header).removeClass("mj-panel-collapsed");
		$(this.body).removeClass("mj-invisible");
		if(this.buttonContainer)
			this.buttonContainer.removeClass("mj-invisible");
		this.renderTo.height(this.height>0?($(this.body).height()+$(this.header).height()+(this.buttonContainer?this.buttonContainer.height():0)):0);
		if(this.autoLoad)
			if(this.refresh){
				mj.load(this.body,this.autoLoad);
			}else{
				if(!this.loaded){
					mj.load(this.body,this.autoLoad);
					this.loaded = true;
				}
			}
		this.doPanel();
	},
	doPanel : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		//var h=parseInt(this.renderTo.('height'));
		var h=parseInt(this.renderTo.height());
		var w=this.renderTo[0].style.width.indexOf('%')>-1?this.renderTo.width():parseInt(this.renderTo[0].style.width);
		this.body.style.overflow = "hidden";
		if(h!=this.lastHeight||w!=this.lastWidth){
			var th=this.header?$(this.header).height():0,tf=this.buttonContainer?parseInt($(this.buttonContainer).css('height')):0;
			h-=th;
			h-=tf;
			if(parseInt(h)>0){
				var tc = $(this.container);
				if(!this.height||this.fitToParent)
					tc.height(h+th+tf);
				if(!this.width||this.fitToParent)
					tc.width(w);
				var tb=$(this.body);
				tb.height(h);
				tb.width(w);
				tb.trigger('kkresize', {sourceEl:this});
			}
			this.lastWidth = w;
			this.lastHeight = h;
		}
		this.body.style.overflow = "auto";
	},
	addButton : function(config){
		var t=this;
		if(!t.buttons){
			t.buttons = [];
			if(t.attachTb){
				t.tbar = new mj.menu({
					renderTo:t.container,
					items : []
				});
				t.buttonContainer = t.tbar._el;
			}else
				t.buttonContainer = $(mj.NE(t.container, {cls:'mj-panel-buttons-container'}));
			$(t.body).height($(t.body).height()-t.buttonContainer.height());
		}
		var b = config;
		if(!b.componentClass){
			mj.applyIf(b, {renderTo:mj.NE(t.buttonContainer)});
			if(t.attachTb)
				b = t.tbar.addButton(b);
			else
				b = new mj.button(b);
		}
		b.window = t;
		t.buttons.push(b);
	},
	getTitle : function(){
		return this.title;
	},
	setTitle : function(text){
		if(this.titleEl){
			this.titleEl.innerHTML = text;
			this.title = text;
		}
	},
	setDisable : function(){
		this.disabled=true;
		$(this.header).addClass('mj-item-disabled');		
	},
	setEnable : function(){
		this.disabled=false;
		$(this.header).removeClass('mj-item-disabled');
	}
};
mj.extend(mj.panel, mj.component);
//!!! myjui/source/tab/tab.js
/** 
 * @fileoverview Tab nesnesi oluşturan sınıf.
 * @hazırlayan kk yazılım info@mj.com
 * @sürüm 0.0.1 Beta 01.10.2007
 */
 /**
 * Yeni bir tab nesenesi oluşturur.<br>
 * @example
		var tab2 = new mj.tab({
	//renderTo : $(mj.NE(myTab.tabContents[0],{tag:'div',id:'tab-cnt2'})),
	renderTo : myTab.tabContents[0],
	innerTab : true,
	tabPosition:'bottom',
	//padding:'',
	tabScroll:false,
	border:false,
	height:377,
	items:[
		{
			title: 'Tab88',
			html: "99",
			closable:true
		}
	]
});
 * @class
 * @name mj.tab
 * @extends mj.component
 * @param {Object} config Tab objesini oluşturabilmek için gerekli olan parametreleri içerir.
 * @config {HTMLElement} renderTo Tab nesnesinin render edileceği container nesnesi.
 * @config {Number} width Genişlik.Ön tanımlı değeri:600.
 * @config {Number} height Yükseklik.Ön tanımlı değeri:250.
 * @config {Object} innerTab İç içe tab olması durumunda parent tab objesi değeri.Ön tanımlı değeri:false.
 * @config {Boolean} defaultActive İlk tabın aktif edilmesini sağlar.Ön tanımlı değeri:true.
 * @config {Boolean} tabScroll Tab sayısı gösterilecek maksimum sayıdan fazla ise sağ ve solda scroll çıkmasını sağlar.Ön tanımlı değeri:false.
	Daha kodu yazılmadı.
 * @config {Boolean} border Border olup olmamasını belirler.<i>innerTab=true</i> olması durumunda false olur.Ön tanımlı değeri:true.
 * @config {String} tabPosition Tabların pozisyon bilgisi.Ön tanımlı değeri:'top'.Belki daha sonra 'left-right' eklenebilir.
 * @config {String} padding Padding kullanılacaksa '' gönderilmelidir.Class adını içerir.Ön tanımlı değeri:'no-padding'.
 * @config {Number} activeTab Aktive eilecek olan tab nesnesi indeksi.Ön tanımlı değeri:-1.
 * @config {Object} items Tab nesneleri config bilgisi.
 */
 mj.tab = function(config){
	mj.tab.superclass.constructor.call(this, config);
};
mj.tab.prototype={
	componentClass : 'mj.tab',
	index : 0,
	/** 
	 * Tabların genişlik değeri.Ön tanımlı değeri:120.
	 * @name tabWidth 
	 * @type {Number}
	 * @memberOf  mj.tab
     */
    tabWidth: 120,
	maxTitle : 14,
	/**
	 * Genişlik.Ön tanımlı değeri:600.
	 * @name width
	 * @type {Number}
	 * @memberOf  mj.tab
	 */
	width : 600,
	/**
	 * Yükseklik.Ön tanımlı değeri:250.
	 * @name height
	 * @type {Number}
	 * @memberOf  mj.tab
	 */
	height : 250,
	/**
	 * İç içe tab olması durumunda <i>true</i> olması gereken değer.Ön tanımlı değeri:false.
	 * @name innerTab
	 * @type {Boolean}
	 * @memberOf  mj.tab
	 */
	innerTab : false,
	/**
	 * İlk tabın aktif edilmesini sağlar.Ön tanımlı değeri:true.
	 * @name defaultActive
	 * @type {Boolean}
	 * @memberOf  mj.tab
	 */
	defaultActive : true,
	/**
	 * Tab sayısı gösterilecek maksimum sayıdan fazla ise sağ ve solda scroll çıkmasını sağlar.Ön tanımlı değeri:true.
	Daha kodu yazılmadı.
	 * @name tabScroll
	 * @type {Boolean}
	 * @memberOf  mj.tab
	 */
	tabScroll : false,
	/**
	 * Border olup olmamasını belirler.<i>innerTab</i> olması durumunda false olur.Ön tanımlı değeri:true.
	 * @name border
	 * @type {Boolean}
	 * @memberOf  mj.tab
	 */
	border : true,
	/**
	 * Tabların pozisyon bilgisi.Ön tanımlı değeri:'top'.Belki daha sonra 'left-right' eklenebilir.
	 * @name tabPosition
	 * @type {String}
	 * @memberOf  mj.tab
	 */
	tabPosition : 'top',
	hideHeader : false,
	/**
	 * Padding kullanılacaksa '' gönderilmelidir.Ön tanımlı değeri:'no-padding'.
	 * @name padding
	 * @type {String}
	 * @memberOf  mj.tab
	 */
	padding : 'no-padding',
	tabs : false,
	tabBodies : false,
	tabContents : false,
	/**
	 * Aktive eilecek olan tab nesnesi indeksi.Ön tanımlı değeri:-1.
	 * @name activeTab
	 * @type {Number}
	 * @memberOf  mj.tab
	 */
	activeTab : -1,
	activeItem : false,
	/**
	 * <i>itemIndex</i> parametresi ile verilen tab nesnesini <i>activeItem</i> nesnesine ve <i>itemIndex</i> değerini <i>activeTab</i>'a atar.
	 * @name setActive
	 * @function
	 * @type {Function}
	 * @param {Number} itemIndex Seçilecek tab nesnesinin index değeri.
	 */
	setActive : function(itemIndex){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		var oldTab = this.activeTab, newTab = itemIndex;
		if(this.activeTab!=-1){
			if(this.activeItem){
				this.activeItem.active = false;
				this.activeItem.bm.addClass('mj-invisible');
				this.activeItem.title.removeClass('mj-tab-active');
			}
			this.activeTab = -1;
		}
		this.activeItem = {};
		this.tabs[itemIndex].title.addClass('mj-tab-active');
		this.activeTab = itemIndex;
		this.activeItem = this.tabs[itemIndex];
		if(this.activeItem.autoLoad){
			window.tmp = this.activeItem.bc;
			var u = this.activeItem.autoLoad.url||this.activeItem.autoLoad;
			var sign=(u.indexOf('?')>-1?'&':'?');
			u += (sign+'h=tmp');
			if(typeof this.activeItem.autoLoad=='string')
				this.activeItem.autoLoad = u;
			if(typeof this.activeItem.autoLoad=='object')
				this.activeItem.autoLoad.url = u;
			if(this.activeItem.refresh){
				mj.load(this.activeItem.bc,this.activeItem.autoLoad);
			}else{
				if(!this.activeItem.loaded){
					mj.load(this.activeItem.bc,this.activeItem.autoLoad);
					this.activeItem.loaded = true;
				}
			}
		}
		this.activeItem.bm.removeClass('mj-invisible');
		this.activeItem.bc.trigger('kkresize', {sourceEl:this});
		this.tabs[itemIndex].active = true;
		this.trigger('tabchange', this, newTab, oldTab);
	},
	/**
	 * <i>item</i> parametresi ile verilen tab nesnesini <i>disable</i> eder.
	 * @name setDisable
	 * @function
	 * @type {Function}
	 * @param {Object} item Disable edilecek tab nesnesi.
	 */
	setDisable : function(item){
		item.title.addClass('mj-tab-disabled');
		item.disabled = true;
	},
	/**
	 * <i>item</i> parametresi ile verilen tab nesnesini <i>enable</i> eder.
	 * @name setEnable
	 * @function
	 * @type {Function}
	 * @param {Object} item Enable edilecek tab nesnesi.
	 */
	setEnable : function(item){
		item.title.removeClass('mj-tab-disabled');
		item.disabled = false;
	},
	/**
	 * Tab'ın addTab fonksiyonu.
	 * @name setEnable
	 * @function
	 * @type {Function}
	 * @param {Object} tc Eklenecek tab nesnesi config objesi.
	 * @config {String} title Başlık.
	 * @config {String} html Tab body içine yazılacak metin.
	 * @config {Boolean} active Active tab olması istendiğinde true yapılması gereken parametre.
	 * @config {String} iconCls Başlık metni yanında görüntülenecek resim class'ı.
	 * @config {Boolean} closable Kapanabilir olma özelliği parametresi.
	 * @config {Boolean} disabled Disable olma özelliği parametresi.
	 * @config {Boolean} refresh Her seferinde yüklenme özelliği parametresi.
	 * @config {String/Object} autoLoad Server üzerinden tabın yükleneceğini gösterir parametre.
	 * @param {Boolean} itemsNoAdd Sonradan eklenen tab'lar için null,<i>items</i> içindeki tabların eklenmesinde true.Bu parametre ile sonradan
	 bu methodun çağırılması ile eklenen tabların <i>items</i> dizisine eklenmesi sağlanmaktadır.
	 */
	addTab : function(tc,itemsNoAdd,index){//tabConfig
		var tmpID = mj.genId(), t=this;
		var item = {
			activate : function(){
				t.setActive(item.itemIndex);
			}
		};
		t.scrollPos = 0;
		item.name = tc.name?tc.name:'';
		item.title = $(mj.NE(this.tabsContainer, {cls:'mj-tab-title',style:'width:'+t.tabWidth+'px;'}));
		mj.NE(item.title, {cls:'mj-tab-title-left'});
		var c = $(mj.NE(item.title, {cls:'mj-tab-title-center'}));
		mj.NE(item.title, {cls:'mj-tab-title-right'});
		c.attr('unselectable','on');
		c.width(t.tabWidth-13);
		var ind = t.tabs.push(item)-1;
		if(tc.iconCls)
			item.icon = mj.NE(c, {cls:'mj-tab-icon '+tc.iconCls});
		item.titleText = tc.title||'';
		item.titleEl = mj.NE(c, {cls:'mj-tab-text', html:item.titleText.ellipse(t.maxTitle)});
		$(item.titleEl).attr('unselectable','on');
		item.setDisable = function(){
			this.title.addClass('mj-tab-disabled');
			this.disabled = true;
		};
		item.setEnable = function(){
			this.title.removeClass('mj-tab-disabled');
			this.disabled = false;
		};
		if(tc.disabled)
			item.setDisable();
		if(tc.closable!==false){
			item.close = $(mj.NE(c, {cls:'mj-tab-close',html:mj.insertSpacer(11,11)}));
			item.close.click(function(e){
				var ind = t.tabs.indexOf(item);
				if(!item.disabled && t.trigger('closeclick', t, item, ind)!==false)
					t.remove(ind);
				e.stopPropagation();
			});
		}
		item.title.click(function(){
			if(!item.active && !item.disabled){
				var ind = t.tabs.indexOf(item);
				t.trigger('click', t, ind);
				t.setActive(ind);
				item.title.removeClass('mj-tab-hover');
			}
		});
		item.title.hover(function(){
			if(!item.active && !item.disabled)
				item.title.addClass('mj-tab-hover');
		}, function(){
			item.title.removeClass('mj-tab-hover');
		});
		item.bm = $(mj.NE(this.body,{tag:'div',cls:'mj-tab-panel-b panel-noborder '+(tc.active?'':'mj-invisible'),style:'width:'+(this.width)+'px;'}));
		item.bw = mj.NE(item.bm,{tag:'div',cls:'tab-panel-bwrap'});
		item.container = item.bc = $(mj.NE(item.bw,{tag:'div',cls:'mj-resize-handle panel-body panel-body-noheader mj-tab-noborder '+(this.padding||'panel-body-pad'),style:'overflow: auto; width: '+(this.width)+'px; height: '+(this.height-(this.padding?28:48))+'px;',html:tc.html}));
		item.autoLoad = tc.autoLoad;
		item.refresh = tc.refresh;
		item.getBody = function(){
			return this.bc;
		};
		item.getTitle = function(){
			return this.titleText;
		};
		item.setTitle = function(text){
			this.titleText = text;
			this.titleEl.innerHTML = text.ellipse(t.maxTitle);
		};
		if(!itemsNoAdd){
			if(!this.items)
				this.items=[];
			this.items.push(item);
			var ind = this.items.length-1
		}	
		else{
			this.items[index]=item;
			var ind = index;
		}	
		item.itemIndex = ind;
		item.name = tc.name?tc.name:ind;
		if(!item.disabled)
			this.setActive(ind);
		t.itemx=item;
		t.doAddTab();
		//mj.bindResize(item.bm, t.doAddTab, t);
		this._scrollLeft();
	},
	doAddTab : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		var w = parseInt(this.body[0].style.width)||this.body.width();
		var h = parseInt(this.body[0].style.height)||this.body.height();
		this.itemx.bm.width(w);
		this.itemx.bm.height(h);
		$(this.itemx.bw).height(h);
		this.itemx.bc.width(w);
		this.itemx.bc.height(h);
		var l = this.items.length, width = this.tabsScroller.width(), itemCount = Math.floor(width / this.tabWidth);
		this.scrollParams = {l:l, width:width, itemCount:itemCount,scrollLeft:(-(l-itemCount)*this.tabWidth)};
		this.itemx.bc.trigger('kkresize', {sourceEl:this});
	},
	_scrollLeft : function(){
		var l = this.scrollParams.l, width = this.scrollParams.width, itemCount = this.scrollParams.itemCount, scrollLeft = this.scrollParams.scrollLeft;
		if(l>itemCount&&width>this.tabWidth)
			this.scrollPos  = scrollLeft;
		else
			this.scrollPos = 0;
		if(!$.browser.msie)
			this.tabsContainer.css('left', this.scrollPos);
		else
			this.tabsContainer.css('margin-left', this.scrollPos);
		
		this._updateScroll();		
	},
	_scrollRight : function(){
		var sayi = this.items.length+(this.scrollPos/this.tabWidth);
		if(sayi>this.scrollParams.itemCount){			
			this.scrollPos -= this.tabWidth;
			if(!$.browser.msie)
				this.tabsContainer.css('left', this.scrollPos);
			else
				this.tabsContainer.css('margin-left', this.scrollPos);
			this._updateScroll();	
		}
	},
	/**
	 * <i>itemIndex</i> parametresi ile verilen tab nesnesini <i>remove</i> eder.
	 * @name remove
	 * @function
	 * @type {Function}
	 * @param {Number} itemIndex Remove edilecek tab nesnesi.
	 */
	remove : function(itemIndex){
		var l = this.tabs.length;
		if(l>1){
			var item = this.tabs[itemIndex];
			item.title.remove();
			item.bm.remove();
			this.tabs.splice(itemIndex,1);
			this.items.splice(itemIndex,1);
			var l = this.items.length, width = this.tabsScroller.width(), itemCount = Math.floor(width / this.tabWidth);
			this.scrollParams = {l:l, width:width, itemCount:itemCount,scrollLeft:(-(l-itemCount)*this.tabWidth)};
			this._updateScroll();
			for(var i=0;i<l;i++)
				if(!this.tabs[i].disabled){
					this.setActive(i);
					break;
				}
		}
		//this._scrollLeft();
	},
	_updateScroll : function(){
		var t=this,scl=t.scrollLeft,scr=t.scrollRight;
		if(t.scrollPos == 0){
			scl.disabled = true;
			scl.addClass('mj-tab-scl-disabled');
		}else{
			scl.disabled = false;
			scl.removeClass('mj-tab-scl-disabled');
		}
		//if(t.scrollPos == t.scrollParams.scrollLeft){
		var sayi = this.items.length+(this.scrollPos/this.tabWidth);
		if(sayi==this.scrollParams.itemCount){
			scr.disabled = true;
			scr.addClass('mj-tab-scr-disabled');
		}else{
			scr.disabled = false;
			scr.removeClass('mj-tab-scr-disabled');
		}
	},
	/**
	 * Tab nesnesi init fonksiyonu.
	 * @name init
	 * @function
	 * @type {Function}
	 */
	init : function(){
		var fark={w:0,h:0}, at = this.activeTab, t=this;
		this.tabs = [],this.tabBodies = [],this.tabContents = [];
		t.cnt = t.renderTo;
		if(this.innerTab){
			this.width=this.innerTab.body.width()||parseInt(this.innerTab.activeItem.getBody()[0].style.width);
			this.height=(this.innerTab.body.height()||parseInt(this.innerTab.activeItem.getBody()[0].style.height))-(this.hideHeader ? 0 : 26);
			if($.browser.msie)
				fark.w=0,fark.h=0;
			else
				fark.w=2,fark.h=4;
			this.renderTo.empty();
		}else
			this.renderTo = $(mj.NE($(this.renderTo).empty()));
		this.renderTo.width((this.width-fark.w)+'px').height((this.height-fark.h)+'px').addClass('mj-tab-panel '+(this.border?'':' mj-tab-noborder'));
		if(this.innerTab) this.border=false;
		if(this.tabPosition=='top'){
			this.header = $(mj.NE(this.renderTo,{tag:'div',id:mj.genId('t-'),cls:'mj-tab-panel-header mj-unselectable '+(this.tabScroll?'tab-scrolling':''),style:'-moz-user-select: none;width: '+(this.width)+'px;'+(this.hideHeader ? 'display:none;' : '')}));
			this.bodyContainer = $(mj.NE(this.renderTo,{tag:'div',id:mj.genId('t-'),cls:'tab-panel-bwrap'}));
		}else{
			this.bodyContainer = $(mj.NE(this.renderTo,{tag:'div',id:mj.genId('t-'),cls:'tab-panel-bwrap'}));
			this.header = $(mj.NE(this.renderTo,{tag:'div',id:mj.genId('t-'),cls:'mj-tab-panel-footer mj-unselectable '+(this.tabScroll?'tab-scrolling':''),style:'-moz-user-select: none;width: '+(this.width)+'px;'+(this.hideHeader ? 'display:none;' : '')}));
		}
		t.scl = this.scrollLeft = $(mj.NE(this.header,{cls:'mj-tab-scroll-left'}));
		t.scl.hover(function(){
			if(!t.scl.disabled)
				t.scl.addClass('mj-tab-scroll-left-hover');
		},function(){
			t.scl.removeClass('mj-tab-scroll-left-hover');
		});
		t.scl.click(function(){
			if(!t.scl.disabled){
				t.scrollPos += t.tabWidth;
				if(!$.browser.msie)
					t.tabsContainer.css('left', t.scrollPos);
				else
					t.tabsContainer.css('margin-left', t.scrollPos);
				t._updateScroll();
			}
		});
		this.tabsScroller = $(mj.NE(this.header,{cls:'mj-tab-container'}));
		t.scr = this.scrollRight = $(mj.NE(this.header,{cls:'mj-tab-scroll-right'}));
		t.scr.hover(function(){
			t.scr.addClass('mj-tab-scroll-right-hover');
		},function(){
			t.scr.removeClass('mj-tab-scroll-right-hover');
		});
		t.scr.click(function(){
			if(!t.scr.disabled)
				t._scrollRight();
		});
		this.tabsContainer = $(mj.NE(this.tabsScroller,{cls:'mj-tab-container-scroll'}));
		if($.browser.msie)
			this.tabsContainer.css('position','static');
		this.body = $(mj.NE(this.bodyContainer,{tag:'div',id:mj.genId('t-'),cls:"mj-resize-handle tab-panel-body"+(this.border?'':' mj-tab-noborder')+" tab-panel-body-"+this.tabPosition,style:'width: '+(this.width-2)+'px; height: '+(this.height-28)+'px;>'}));	
		t.doTab();
		if(this.items){
			for(var i in this.items){
				if(typeof this.items[i]!='function')
					this.addTab(this.items[i],true,i);
			}
			if(typeof at=='number' && at>-1)
				this.setActive(at);
		}
		mj.bindResize(t.cnt, t.doTab, t);
	},
	doTab : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		var t = this;
		if(t.innerTab){
			var w=t.innerTab.body.width()||parseInt(t.innerTab.body[0].style.width);
			var h=t.innerTab.body.height()||parseInt(t.innerTab.body[0].style.height);
			t.cnt.width(w);
			t.cnt.height(h);
		}
		var nW = t.cnt.width()||parseInt(t.cnt[0].style.width), nH = t.cnt.height()||parseInt(t.cnt[0].style.height);
		if(t.width != nW || t.height != nH){
			t.width = nW;
			t.height = nH;
			t.cnt[0].style.overflow = "hidden";
			if(t.width!=t.lastWidth||t.height!=t.lastHeight){
				t.renderTo.width(t.width-(t.border?2:0));
				t.renderTo.height(t.height-(t.border?2:0));
				//t.renderTo.trigger('kkresize', {sourceEl:this});
				var w=parseInt(t.renderTo[0].style.width)||t.renderTo.width(),h=parseInt(t.renderTo[0].style.height)||t.renderTo.height();
				t.header.width((w>0?w:0));
				var bdH=h>27?h-(this.hideHeader ? 0 : 27):0;
				t.bodyContainer.width(w);
				t.bodyContainer.height(bdH);
				t.body.height(bdH);
				t.body.width(w);
				var w = parseInt(this.body[0].style.width)||this.body.width();
				var h = parseInt(this.body[0].style.height)||this.body.height();
				if(this.items){
					for(var i=0,len=this.items.length;i<len;i++){
						if(this.items[i].bm){ 
							this.items[i].bc[0].style.overflow = "hidden";
							// this.items[i].bw.style.overflow = "hidden";
							this.items[i].bm.width(w);
							this.items[i].bm.height(h);
							$(this.items[i].bw).height(h);
							this.items[i].bc.width(w);
							this.items[i].bc.height(h);
							this.items[i].bc.trigger('kkresize', {sourceEl:this});
							this.items[i].bc[0].style.overflow = "auto";
							// this.items[i].bw.style.overflow = "auto";
						}
					}
					if(t.scrollable!==true && t.width >= (t.items.length * t.tabWidth)){
						t.scl.addClass('mj-invisible');
						t.scr.addClass('mj-invisible');
						t.tabsScroller.width(t.width);
					}else
						t.tabsScroller.width((t.width-32-(t.border?2:0))>0?(t.width-32-(t.border?2:0)):0);
					var l = t.items.length, width = t.tabsScroller.width(), itemCount = Math.floor(width / t.tabWidth);
					t.scrollParams = {l:l, width:width, itemCount:itemCount,scrollLeft:(-(l-itemCount)*t.tabWidth)};
				}
				t.body.trigger('kkresize', {sourceEl:this});
				//t.renderTo.trigger('kkresize', {sourceEl:this});
				t.lastWidth = t.width;
				t.lastHeight = t.height;
			}
		}
		t.cnt[0].style.overflow = "auto";
	},
	getShortCuts : function(){
		var t=this, s = {};
		for(var i=0,l=t.tabs.length;i<l;i++)
			s[t.tabs[i].name] = t.tabs[i];
		return s;
	}
};
mj.extend(mj.tab, mj.component);

//!!! myjui/source/tree/tree.js
/**
	//data icindeki iconCls classi verilirken .mj-tree-node-el .classAdi seklinde bir class tanimlanmali
*/
mj.tree = function(config){
	mj.tree.superclass.constructor.call(this, config);
};
mj.tree.prototype = {
	componentClass : 'mj.tree',
	root : {
		text : 'myJUI',
		id : 'root',
		expanded : true
	},
	rootHide : true,
	check : false,
	icon : false,
	store : false,
	loadMask : true,
	lines : false,
	mode : false,
	expanded : false,
	clearOnBeforeLoad : true,
	trackMouseOver : true,
	dblClickToggle : true,
	drag : false,
	dragExpandDelay : 600,
	//private
	_dragHandle : false,
	//private
	_nodes : false,
	//private
	_nodesObj : false,
	//private
	_dragEls : false,
	//private
	_dropEls : false,
	//private
	_jsonData : false,
	init : function(){
		this._dropEls = [];
		this._dragEls = [];
		this.cnt = $(mj.NE(this.renderTo,{cls:'mj-tree',style:'position:relative;'}));
		this.cnt.bind('dblclick',{scope:this},this._dblClick);
		this.cnt.bind('mousedown',{scope:this},this._onMouseDown);
		this.cnt.bind('click',{scope:this},this._onClick);
		this.cnt.bind('contextmenu',{scope:this},this._context);
		if(this.trackMouseOver){
			this.cnt.bind('mouseover',{scope:this},this._onOverFn);
			this.cnt.bind('mouseout',{scope:this},this._onOutFn);
		}
		if(this.loadMask)
			this.mask = new mj.mask({el:this.renderTo[0]});
		this.store.on('beforeload', function(){
			if(this.loadMask)
				this.mask.show(50);
			if(this.clearOnBeforeLoad){
				var cnt = !this.loadingNode ? this.cnt : this.loadingNode.el.next();
				cnt.empty();
			}
			if(this.drag){
				for(var i=0;i<this._dragEls.length;i++)
					this._dragEls[i].dropEls = [];
				this._dropEls = [];
			}
		}, this);
		this.store.on('load',function(){
			this._treeLoad.call(this);
		},this);
		// this.expand._oScope = this;
		mj.bindResize(this.cnt, this.doRender, this);
	},
	load : function(){
		if(this.store)
			this.store.load();
	},
	//private
	_treeLoad : function(){
		if(this.mode=='remote')
			var tmp = this.selectedNode;
		this.selectedNode = false;
		var par = false,f=false, pNode;
		var _d = this.store.data;
		if(this.cm){
			var _hEl = $('div.mj-tree-header-row',this.cnt);
			if(_hEl.length == 0){
				var _w = 0, _hc = [];
				for(var i = 0, l = this.cm.length;i<l;i++){
					var item = this.cm[i];
					_w += item.width||0;
					_hc.push('<div class="mj-tree-c-hd mj-tree-c-hd-'+(i==0?'first':'')+'" style="width:'+(i==0?(item.width):item.width)+'px;"><div class="mj-tree-c-hd-text">'+item.header+'</div></div>');
				}
				this.cnt.width(_w+20);
				_hc.push('<div class="mj-tree-c-clear"></div>');
				mj.NE(this.cnt, {id:this.root.id+'-header',cls:"mj-grid-header-row mj-unselectable", html:_hc.join('')});
			}
		}
		if(this.loadingNode){
			loadCnt = this.loadingNode.el.next();
			par = this.loadingNode;
			par.children = _d;
		}else{
			this._nodes = {};
			this._nodesObj = {};
			var loadCnt = $(mj.NE(this.cnt,{tag:"ul",id:this.root.id,cls:"mj-tree-root-ct mj-tree"+(this.lines?'':'-no')+"-lines"}));
			if(!this.rootHide){
				this.root['root'] = true;
				this.root.nodeId = false; 
				this._addNode([this.root],loadCnt,par,false,false);
				loadCnt = $('ul.mj-tree-node-ct',this.root.cnt);
				if(!this.root.expanded)
					this.collapse(this.root);
				par = this.root;
			}else
				loadCnt = $(mj.NE(loadCnt,{cls:"mj-tree-root-node"}));
			this.root.nodeId = "0";
			this.root.children = _d;
		}
		this._addNode(_d,loadCnt,par,false,false);
		if(this.loadMask)
			this.mask.hide();
		this.loadingNode = false;
		if(this.mode=='remote')
			this.selectedNode = tmp;
		if(this.mode != 'remote' && this.expanded)
			this.expandAll();
		this._createNodesObj(false,true);
		this.trigger('load',this);
	},
	addNode : function(data,cnt,parent,after){
		this._addNode(data,cnt,parent,after,true);
		this._createNodesObj(false,true);
	},
	//private
	_addNode : function(data,cnt,parent,after,addData){
		var i=-1,d=[];
		if(data instanceof Array == false){
			d.push(data);
		}else
			d = data;
		var f = parent.nodeId?parent.nodeId+'/':false;
		while(++i<d.length){
			var item = d[i];
			if(item){
				var x,v=false;
				
				if(after){
					var n = this.getNodeById($('div:first',after)[0].id);
					if(n.root)
						v = 1;
					else
						if(n.leaf)
							v = 2;
						else 
							if(n.expanded)
								v = 3;
							else
								v = 4;
				}else
					v = 5;
				
				if(!after){
					x = f?(f + (addData&&parent.children?parent.children.length:i.toString())):i.toString();
				}else{
					var node,nIdx;
					switch (v) {
					case 1:
						node = this.getNodeById($('div:first',n.el.next())[0].id);
						nIdx = 0;
						break;
					case 2:
						node = this.getNodeById($('div:first',after)[0].id);
						nIdx = this.getNodeIndex(node,'id',node.id)+1;
						break;
					case 3:
						node = this.getNodeById($('div:first',n.el.next())[0].id);
						nIdx = 0;
						break;
					case 4:
						node = this.getNodeById($('div:first',after)[0].id);
						nIdx = this.getNodeIndex(node,'id',node.id)+1;
						break;
					case 5:
						break;
				};
				x = f + nIdx;
				}
				if(!item.id)
					item.id = mj.genId("tree-node-");
				item.parent = parent;
				if(parent.leaf){
					parent.leaf = false;
					parent.el.replaceclass('mj-tree-node-leaf','mj-tree-node-collapsed');
					$('img.mj-tree-ce-icon:first',parent.el).addClass('mj-tree-elbow-plus');
				}
				if(addData){
					parent.children = parent.children||[];
					if(!after)
						parent.children.push(item);
					else
						parent.children.splice(nIdx,0,item);
					if(this.check)
						item.checked = parent.checked;
				}
				item.nodeId = x.toString();
				item.cnt = cnt;
				item.leaf = (typeof item.leaf == 'undefined')?(item.children||item.root)?false:true:item.leaf;
				var tmpl = this._getNodeTemplate(item);
				switch (v) {
					case 1:
						v = $(tmpl).insertBefore($('li:first',cnt));
						item.el = $('div.mj-tree-node-el:first',v);
						break;
					case 2:
						v = $(tmpl).insertAfter(after);
						item.el = $('div.mj-tree-node-el:first',v);
						break;
					case 3:
						v = $(tmpl).insertBefore($('li:first',n.el.next()));
						item.el = $('div.mj-tree-node-el:first',v);
						break;
					case 4:
						v = $(tmpl).insertAfter(n.el.parent());
						item.el = $('div.mj-tree-node-el:first',v);
						break;
					case 5:
						item.cnt.append(tmpl);
						item.el = $('div.mj-tree-node-el:last',item.cnt);
						break;
				};
				if(this.drag){
					if(!item.root){
						var t = this;
						item.dragEl = new mj.drag({el:$('a>span',item.el),moving :false,position:'static',dropOverCls:'mj-tree-drag-drop-hover',dropBottomCls:'mj-tree-drag-drop-bottom',dropEls:this._dropEls,proxyEl:{width:80,height:18},parent : mj.bd});
						this._dragEls.push(item.dragEl);
						if(!t._dragHandle)
							t._dragHandle = item.dragEl;
						item.dragEl.on('dragstop',function(e,b){
							if(b){
								if(e.curDrop&&e.fromDrop!=e.curDrop){
									var fEl=t.getNodeById(e.fromDrop[0].id), tEl=t.getNodeById(b[0].id), bt = false;
									if(b.hasClass(e.dropBottomCls)){
										tEl = (tEl.leaf||!tEl.expanded)?tEl.parent:tEl;
										bt = b.parent();
									}
									if(fEl&&tEl)
										t._moveNode(fEl,tEl,bt);
								}
								if(b.hasClass(e.dropBottomCls))
									b.removeClass(e.dropBottomCls);
							}
						});
						item.dragEl.on('onDropOver',function(e,b){
							if(e.curDrop&&e.fromDrop!=e.curDrop){
								var tEl = t.getNodeById(b[0].id);
								if(tEl&&!tEl.leaf&&!tEl.expanded&&!b.hasClass(e.dropBottomCls)){
									if(t._expandTimer)
										clearTimeout(t._expandTimer);
									t._expandTimer = setTimeout(function(){t.expand(tEl,t);},t.dragExpandDelay);
								}
							}
						});
						item.dragEl.on('onDropOut',function(e,b){
							if(t._expandTimer)
								clearTimeout(t._expandTimer);
						});
					}
					this._dropEls.push(item.el);
				}
				if(!item.leaf&&item.expanded)
					this.expand(item);
				this._nodes[item.id] = item;
				if(this.check && item.checked)
					this._checkfn(item,item.checked);
				if(item.children&&!item.root)
					this._addNode(item.children,$('ul',item.el.parent()),item,false,false);
				this.trigger('addnode',item);
			}
		}
	},
	removeNode : function(node){
		if(!node.root){
			if(node.children){
				while(node.children&&node.children.length>0)
					this.removeNode(node.children[0]);
				delete node.children;
				this.removeNode(node);
			}else{
				if(this.check)
					this._checkfn(node);
				this._dropEls.remove(this._nodes[node.id].el);
				delete this._nodes[node.id];
				$(node.el).parent().remove();
				var p = node.parent, d = p.children;
				if(d){
					d.remove(d[d.indexOf(node)]);
					if(d.length == 0 && this.mode != 'remote'){
						p.leaf = true;
						p.el.replaceclass('mj-tree-node-'+(p.expanded?'expanded':'collapsed'),'mj-tree-node-leaf');
						$('img.mj-tree-ce-icon:first',p.el).removeClass('mj-tree-elbow-'+(p.expanded?'minus':'plus'));
						p.expanded = false;
						delete p.children;
					}
				}
			}
		}
	},
	//private
	_moveNode : function(from,to,after){
		var obj = {};
		for(var s in from)
			if(typeof from[s]!== 'function' && typeof from[s]!== 'object' )
				obj[s] = from[s];
			else if (s=='children'){
				obj[s] = [];
				mj.cloneObject(from[s],'children',obj[s]);
			}
		this.removeNode(from);
		this._addNode(obj,to.el.next(),to,after,true);
		this._createNodesObj(false,true);
	},
	//private
	_createNodesObj : function(items,clear){
		if(clear){
			this._nodesObj = {};
			if(!this.rootHide)
				this._nodesObj[this.root.nodeId] = this.root;
		}
		if(!items)
			items = this.root.children;
		if(items)
			for(var i=0,len=items.length;i<len;i++){
				//var f = (items[i].parent?items[i].parent.nodeId:items[i].nodeId) + '/' + i;
				var f = (items[i].parent?items[i].parent.nodeId + '/' + i:i);
				items[i].prev = items[i-1];
				items[i].next = items[i+1];
				items[i].nodeId = f.toString();
				this._nodesObj[f] = items[i];
				if(items[i].children)
					this._createNodesObj(items[i].children,false);
			}
	},
	//private
	_getNodeTemplate : function(data){
		var tmp = [],ci = '';
		var ce = (data.children||data.root)?((!data.leaf&&data.expanded)?'mj-tree-node-expanded':'mj-tree-node-collapsed'):((this.mode!=='remote'||data.leaf)?'mj-tree-node-leaf':(!data.leaf&&data.expanded)?'mj-tree-node-expanded':'mj-tree-node-collapsed');
		if(ce == 'mj-tree-node-collapsed')
			ci = 'mj-tree-elbow-plus';
		else if(ce == 'mj-tree-node-expanded')
			ci = 'mj-tree-elbow-minus';
		//ci += data.root ? '-end':'';
		var iconCls = false;
		if(data.iconCls)
			iconCls = data.iconCls;
		if(!data.root){
			tmp.push('<li class="mj-tree-node">');
			tmp.push('<div unselectable="on" class="mj-tree-node-el mj-unselectable '+ce+'" id="'+data.id+'">');
			if(this.cm)
				tmp.push('<div name="c-col-0" class="mj-tree-c-col c-col-0" style="width:'+this.cm[0].width+'px;">');
			tmp.push('<span class="mj-tree-indent">');
			var i=-1;
			while(++i<data.nodeId.toString().split('/').length-1){
				var lc = (!this.rootHide && i>0||this.rootHide)? 'mj-tree-elbow-line' : '';
				tmp.push('<img class="mj-tree-icon '+lc+'" src="'+mj.glb.blankImage+'"/>');
			}
			tmp.push('</span>');
			tmp.push('<img class="mj-tree-ce-icon mj-tree-elbow '+ci+'" src="'+mj.glb.blankImage+'"/>');//line
			if(this.check){
				tmp.push('<input class="node-cb mj-unselectable mj-invisible" unselectable="on" type="checkbox" ' + (data.checked ? 'checked="checked" />': ' />'));
				tmp.push('<div style="width: 16px;height:16px;float:left;" class="mj-checkbox '+(data.checked?'mj-checkbox-checked':'')+'"></div>' );
			}
			if(this.icon)
				tmp.push('<img style="'+(data.icon?'background-image:url('+mj.glb.blankImage+');':'')+'" unselectable="on" class="mj-tree-node-icon '+(iconCls||'')+'" src="'+(data.icon?data.icon:mj.glb.blankImage)+'" id="'+data.id+'-dd"/>');//ikon
			tmp.push('<a id="'+data.id+'-a" href="" class="mj-tree-node-anchor" hidefocus="on">');//link
			tmp.push('<span unselectable="on" id="'+data.id+'-text">');
			tmp.push(data.text);
			tmp.push('</span>');
			tmp.push('</a>');
			if(this.cm){
				tmp.push('</div>');
				var j=0, el;
				while(++j<this.cm.length){
					el = this.cm[j];
					var cellData = el.renderer ? (data.data?(typeof data.data[el.value] == 'undefined'?'&nbsp;': this.cm[j].renderer.call(this, data.data[el.value], data.data)) :'&nbsp;'): data.data[el.value];
					tmp.push('<div name="c-col-'+j+'" class="mj-tree-c-col c-col-'+j+'" style="width:'+el.width+'px;padding-left:1px;"><div class="mj-tree-c-text c-text-'+j+'">'+cellData+'</div></div>');
				}
				tmp.push('<div class="mj-tree-c-clear"></div>');
			}
			tmp.push('</div>');
			tmp.push('<ul class="mj-tree-node-ct mj-invisible" style="position:static;"/>');
			tmp.push('</li>');
		}else{
			tmp.push('<li class="mj-tree-node">');
			tmp.push('<div unselectable="on" class="mj-tree-node-el mj-unselectable '+ce+'" id="'+data.id+'">');
			if(this.cm)
				tmp.push('<div name="c-col-0" class="mj-tree-c-col c-col-0" style="width:'+this.cm[0].width+'px;">');
			tmp.push('<span class="mj-tree-indent">');
			var i=-1;
			while(++i<data.nodeId.toString().split('/').length-1)
				tmp.push('<img class="mj-tree-icon" src="'+mj.glb.blankImage+'"/>');
			tmp.push('</span>');
			tmp.push('<img class="mj-tree-ce-icon mj-tree-elbow '+ci+'" src="'+mj.glb.blankImage+'"/>');//line
			if(this.check){
				tmp.push('<input class="node-cb mj-unselectable mj-invisible" unselectable="on" type="checkbox" ' + (data.checked ? 'checked="checked" />': ' />'));
				tmp.push('<div style="width: 16px;height:16px;float:left;" class="mj-checkbox '+(data.checked?'mj-checkbox-checked':'')+'"></div>' );
			}
			if(this.icon)
				tmp.push('<img style="'+(data.icon?'background-image:url('+mj.glb.blankImage+');':'')+'" unselectable="on" class="mj-tree-node-icon '+(iconCls||'')+'" src="'+(data.icon?data.icon:mj.glb.blankImage)+'" id="'+data.id+'-dd"/>');//ikon
			tmp.push('<a id="'+data.id+'-a" href="" class="mj-tree-node-anchor" hidefocus="on">');//link
			tmp.push('<span unselectable="on" id="'+data.id+'-text">');
			tmp.push(data.text);
			tmp.push('</span>');
			tmp.push('</a>');
			if(this.cm){
				tmp.push('</div>');
				var j=0;
				while(++j<this.cm.length){
					var cellData = this.cm[j].renderer ? (data.data?(typeof data.data[this.cm[j].value] == 'undefined'?'&nbsp;': this.cm[j].renderer(data.data[this.cm[j].value], data.data)):'&nbsp;') : data.data[this.cm[j].value];
					tmp.push('<div name="c-col-'+j+'" class="mj-tree-c-col c-col-'+j+'" style="width:'+this.cm[j].width+'px;padding-left:1px;"><div class="mj-tree-c-text c-text-'+j+'">'+(cellData)+'</div></div>');
				}
				tmp.push('<div class="mj-tree-c-clear"></div>');
			}
			tmp.push('</div>');
			tmp.push('<ul class="mj-tree-node-ct" style="position:static;"/>');
			tmp.push('</li>');
		}
		return tmp.join('');
	},
	doRender : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		$(this.cnt).height(this.cnt.height());
		$(this.cnt).trigger('kkresize', {sourceEl:this});
	},
	collapse : function(node){
		if(!node||node.leaf)
			return false;
		if(this.rootHide&&!node.parent)
			node.nodeId = "0";
		node.expanded = false;
		var nodeEl = node.el;
		var iconEl = $('img.mj-tree-ce-icon:first',nodeEl);
		nodeEl.replaceclass('mj-tree-node-expanded','mj-tree-node-collapsed');
		iconEl.replaceclass('mj-tree-elbow-minus','mj-tree-elbow-plus');
		iconEl.replaceclass('mj-tree-elbow-minus-end','mj-tree-elbow-plus-end');
		nodeEl.next().addClass('mj-invisible');
		if(this.mode == 'remote' && !node.root)
			while(node.children&&node.children.length>0)
				this.removeNode(node.children[0]);
		if(this.drag&&this._dragHandle)
			this._dragHandle.initDrops();
	},
	collapseAll : function(){
		for(var item in this._nodes)
			if(typeof this._nodes[item] != 'function' && this._nodes[item].expanded)
				this.collapse(this._nodes[item]);
	},
	expand : function(node,sc){
		// if(this != arguments.callee._oScope)
			// return arguments.callee.apply(arguments.callee._oScope, arguments);
		var scope = sc || this;
		if(!node||node.leaf)
			return false;
		node.expanded = true;
		var nodeEl = node.el;
		var iconEl = $('img.mj-tree-ce-icon:first',nodeEl);
		nodeEl.replaceclass('mj-tree-node-collapsed','mj-tree-node-expanded');
		iconEl.replaceclass('mj-tree-elbow-plus','mj-tree-elbow-minus');
		iconEl.replaceclass('mj-tree-elbow-plus-end','mj-tree-elbow-minus-end');
		if(scope.mode == 'remote' && !node.root){
			scope.loadingNode = node;
			scope.store.params.node = node.id;
			scope.store.load();
			//scope.store.params.node = scope.root.id;
		}
		nodeEl.next().removeClass('mj-invisible');
		if(scope.drag&&scope._dragHandle)
			scope._dragHandle.initDrops();
	},
	expandAll : function(){
		for(var item in this._nodes)
			if(typeof this._nodes[item] != 'function' && !this._nodes[item].leaf && !this._nodes[item].expanded)
				this.expand(this._nodes[item]);
	},
	checkAll : function(){
		var node;
		if(this.check)
			for(var item in this._nodes)
				if(typeof this._nodes[item] != 'function' && !this._nodes[item].checked){
					node = this._nodes[item];
					this._checkfn(node,true);
				}
	},
	clearCheckedAll : function(){
		var node;
		if(this.check)
			for(var item in this._nodes)
				if(typeof this._nodes[item] != 'function' && this._nodes[item].checked){
					node = this._nodes[item];
					this._checkfn(node,false);
				}
	},
	getRootNode : function(){
		return this.root;
	},
	getNodeById : function(name){
		if(this._nodes[name])
			return this._nodes[name];
		return false;
	},
	getNodeIndex : function(node,key,keyValue){
		return parseInt(node.parent.children.getIndex(key,keyValue));
	},
	getNodeIdByKey : function(key,keyValue){
		return mj.getIndex(this._nodes,key,keyValue);
	},
	getNodeByKey : function(key,keyValue){
		var ni = mj.getIndex(this._nodes,key,keyValue);
		if(ni != -1)
			return this._nodes[ni];
		else
			return false;
	},
	clearSelected : function(){
		if(this.selectedNode){
			this.selectedNode.el.removeClass("mj-tree-selected");
			delete this.selectedNode;
		}
	},
	selectNode : function(node,select){
		this.clearSelected();
		if(node){
			this.selectedNode = node;
			node.el.addClass("mj-tree-selected");
			if(select){
				var x=node.nodeId.split('/'),name='';
				for(var i=0,l=x.length;i<l;i++){
					name+=x[i];
					if(!this._nodesObj[name].expanded)
						this.expand(this._nodesObj[name]);
					name+='/';
				}
			}
		}
	},
	getSelected : function(){
		if(this.selectedNode)
			return this.selectedNode;
		else
			return false;
	},
	//private
	_checkfn : function(node,check){
		var t = this;
		if(node){
			var el = $('div.mj-checkbox',node.el),elp=el.prev(),ch=(typeof check != 'undefined')?check:!elp.attr('checked'),pNode;
			node.checked = ch;
			elp.addClass('checked');
			elp.parents('li:first').find('input').attr('checked',ch).css('opacity','1');
			var l=el.parents('li');
			if(ch){
				l.filter('has("checked")').find('input:first').attr('checked',ch);
				el.addClass('mj-checkbox-checked').css('opacity','1');
			}else{
				el.parents('li:first').find('.mj-checkbox').each(function(){
					$(this).removeClass('mj-checkbox-checked').css('opacity','1');
					var myNode = t.getNodeById($(this).parent()[0].id);
					myNode.checked = false;
				});
			}
			var c=l.find('input:checked');
			for(var i=0,len=c.length;i<len;i++){
				var p=$(c[i]).parents('li:first'),lc=p.find('input:checked').length,luc=p.find('input:checkbox').length;
				pNode = this.getNodeById($(c[i]).parent()[0].id);
				if(lc==1&&lc!=luc){
					$(c[i]).attr('checked',false).css('opacity','1');
					$(c[i]).next().removeClass('mj-checkbox-checked').css('opacity','1');
					pNode.checked = false;
				}else if(lc!=0&&lc<luc){
					$(c[i]).attr('checked',true).css('opacity','0.2');
					$(c[i]).next().addClass('mj-checkbox-checked').css('opacity','0.4');
				}else if(lc==luc){
					$(c[i]).attr('checked',true).css('opacity','1');
					$(c[i]).next().addClass('mj-checkbox-checked').css('opacity','1');
					pNode.checked = true;
				}
			}
		}
	},
	//private
	_onClick : function(e){
		var t = e.data.scope;
		e.preventDefault();
		return false;
	},
	//private
	_onMouseDown : function(e){
		var t = e.data.scope;
		var event = t._getEventType(e);
		e.preventDefault();
		e.stopPropagation();
		var node = t.getNodeById(t._getNodeIdFromEvent(e));
		switch(event){
			case 'check':
				t._checkfn(node);
				break;
			case 'iconSelect':
				t.selectNode(node);
				t.trigger('nodeclick',t,node);
				break;
			case 'nodeSelect':
				t.selectNode(node);
				t.trigger('nodeclick',t,node);
				t.trigger('cellclick',t,node);
				break;
			case 'nodeSelectDiv':
				t.selectNode(node);
				t.trigger('nodeclick',t,node);
				break;
			case 'columnSelectDiv':
				t.selectNode(node);
				if($(e.target).hasClass('mj-tree-c-col'))
					var tar = $(e.target);
				else
					var tar = $(e.target).parents('.mj-tree-c-col:first');
				if(tar.length>0){
					var ci = tar.attr('name').split('c-col-');
					ci = ci.length>1?parseInt(ci[1]):0;
					t.trigger('cellclick',t,node,tar,ci);
				}
				break;
			case 'toggle':
				if(node.expanded)
					t.collapse(node);
				else
					t.expand(node);
				t.trigger('togglenode',t,node);
				break;
		}
	},
	//private
	_dblClick : function(e){
		var t = e.data.scope;
		var event = t._getEventType(e);
		var node = t.getNodeById(t._getNodeIdFromEvent(e));
		if(event)
			t.trigger('nodedblclick',t,node);
		switch(event){
			case 'iconClick':
				t.selectNode(node);
				if(t.dblClickToggle){
					if(node.expanded)
						t.collapse(node);
					else
						t.expand(node);
					t.trigger('togglenode',t,node);
				}
				break;
			case 'nodeClick':
				if(t.dblClickToggle){
					if(node.expanded)
						t.collapse(node);
					else
						t.expand(node);
					t.trigger('togglenode',t,node);
				}
				break;
			case 'nodeSelectDiv':
				t.selectNode(node);
				if(t.dblClickToggle){
					if(node.expanded)
						t.collapse(node);
					else
						t.expand(node);
					t.trigger('togglenode',t,node);
				}
				break;
			case 'columnSelectDiv':
				t.selectNode(node);
				if(t.dblClickToggle){
					if($(e.target).hasClass('mj-tree-c-col'))
						var tar = $(e.target);
					else
						var tar = $(e.target).parents('.mj-tree-c-col:first');
					if(node.expanded)
						t.collapse(node);
					else
						t.expand(node);
					if(tar.length>0){
						var ci = tar.attr('name').split('c-col-');
						ci = ci.length>1?parseInt(ci[1]):0;
						t.trigger('celldblclick',t,node,tar,ci);
					}
				}
				break;
		}
	},
	//private
	_context : function(e){
		var t = e.data.scope;
		var event = t._getEventType(e);
		var node = t.getNodeById(t._getNodeIdFromEvent(e));
		if(event)
			t.trigger('contextmenu', t, node, e);
			// mj.log('contenxt - '+event);
	},
	//private
	_onOver : function(node){
		if(node){
			this.lastOverNode = node;
			node.el.addClass("mj-tree-node-over");
		}
	},
	//private
	_onOverFn : function(e){
		var t = e.data.scope;
		var event = t._getEventType(e);
		switch(event){
			case 'mouseover':
				if(t.lastOverNode){
					t._onOut(t.lastOverNode);
					delete t.lastOverNode;
				}
				t._onOver(t.getNodeById(t._getNodeIdFromEvent(e)));
				break;
		}
	},
	//private
	_onOut : function(node){
		if(node)
			node.el.removeClass("mj-tree-node-over");
	},
	//private
	_onOutFn : function(e){
		var t = e.data.scope;
		var event = t._getEventType(e);
		switch(event){
			case 'mouseout':
				if(t.lastOverNode){
					t._onOut(t.lastOverNode);
					delete t.lastOverNode;
				}
				t._onOut(t.getNodeById(t._getNodeIdFromEvent(e)));
				break;
		}
	},
	//private
	_getNodeIdFromEvent : function(e){
		return (e.target.tagName == 'DIV' && $(e.target).hasClass('mj-tree-node-el')) ? e.target.id : $(e.target).parents('div.mj-tree-node-el:first').length>0?$(e.target).parents('div.mj-tree-node-el:first')[0].id:false;
	},
	//private
	_getEventType : function(e){
		e.preventDefault();
		if(e.target.className.indexOf('ce-icon')>-1 && e.type == 'mousedown')
			return 'toggle';
		if((e.target.className.indexOf('node-icon')>-1 || e.target.className.indexOf('tree-icon')>-1) && e.type == 'dblclick')
			return 'iconClick';
		if((e.target.className.indexOf('node-icon')>-1 || e.target.className.indexOf('tree-icon')>-1) && e.type == 'mousedown')
			return 'iconSelect';
		if(e.target.id.indexOf('-text')>-1 && e.type == 'dblclick')
			return 'nodeClick';
		if(e.target.id.indexOf('-text')>-1 && e.type == 'mousedown')
			return 'nodeSelect';
		if(this.cm){
			if(($(e.target).hasClass('mj-tree-node-el') || e.target.className.indexOf('-c-col')>-1 || e.target.className.indexOf('-c-text')>-1) && (e.type == 'dblclick' || e.type == 'mousedown'))
				return 'columnSelectDiv';
			if($(e.target).parents('div.mj-tree-node-el:first').length>0 && (e.type == 'dblclick' || e.type == 'mousedown'))
				return 'columnSelectDiv';
		}else{
			if(e.target.className.indexOf('mj-checkbox')>-1 && e.type == 'mousedown')
				return 'check';
			if(e.target.tagName == 'DIV' && $(e.target).hasClass('mj-tree-node-el') && (e.type == 'dblclick' || e.type == 'mousedown'))
				return 'nodeSelectDiv';
		}
		if(e.type == 'mouseover' || e.type == 'mouseout' || e.type == 'contextmenu')
			return e.type;
		return false;
	},
	getData : function(items,clear){
		if(clear)
			this._jsonData = {};
		if(!items)
			items = this.root.children;
		for(var i=0,len=items.length;i<len;i++){
			this._jsonData[items[i].id] = {id:items[i].id,parentId:parseInt(items[i].parent.id)||0,sira:i};
			if(items[i].children)
			this.getData(items[i].children,false);
		}
	}
};
mj.extend(mj.tree, mj.component);
//!!! myjui/source/window/windowManager.js
mj.windowManager = function(config){
	mj.windowManager.superclass.constructor.call(this, config);
	//this.init();
};
mj.windowManager.prototype = {
	componentClass : 'mj.windowManager',
	modalIndex : mj.glb.modalIndex,
	activeIndex : false,
	activeWin : false,
	init : function(){
		this.windows = [];
		this.w = $(this.windows);
		mj.windowManager.superclass.init.call(this);
	},
	add : function(win){
		win.manager = this;
		win.managerId = this.windows.push(win);
		if(win.parentWin)
			win.parentWin.add(win);
		if(!this.activeIndex)
			this.activeIndex = win.lastIndex + 2;
		win.on('show', this.onWindowShow, this);
		win.on('hide', this.onWindowHide, this);
		win.on('close', this.onWindowClose, this);
		win.on('init', this.onWindowInit, this);
	},
	onWindowShow : function(win){
		this.bringToFront(win);
	},
	onWindowHide : function(win){
		if(win.mask)
			win.mask.hide();
		this.activeWin = false;
		this.activeIndex = win.lastIndex;
		win._el.css('z-index', win._zBackup);
	},
	onWindowClose : function(win){
		if(win.mask)
			win.mask.destroy();
		this.activeWin = false;
		this.activeIndex = win.lastIndex;
		this.windows.splice(win.managerId,1);
	},
	onWindowInit : function(win){
		if(win.modal){
			win.mask = new mj.mask({zIndex:this.modalIndex});
			win.addRelated(win.mask);
		}
	},
	bringToFront : function(win){
		if(this.activeWin != win){
			//this.activeIndex = window.windowManager&&window.windowManager.activeIndex<this.activeIndex?this.activeIndex:window.windowManager.activeIndex;
			var aI = this.activeIndex ? this.activeIndex : this.modalIndex;
			for(var i=0;i<this.windows.length;i++){
				var w=this.windows[i], zI=-1;
				if(w._el){
					zI = w._el.css('z-index');
					if(!isNaN(zI))
						aI = Math.max(aI, zI);
				}
			}
			win.lastIndex = aI;
			this.activeIndex = i = aI + 2;
			if(this.activeWin)
				this.activeWin.trigger('sendtoback',this.activeWin);
			this.activeWin = win;
			if(window.windowManager){
				window.windowManager.activeIndex = i;
				window.windowManager.activeWin = win;
			}
			if(win.mask)
				win.mask.show(i-1);
			win._zBackup = win._el.css('z-index');
			win._el.css('z-index', i);
		}
		win.trigger('activate', win);
		this.trigger('activate', win);
	}
};
mj.extend(mj.windowManager, mj.component);
//!!! myjui/source/window/window.js
mj.window = function(config){
	mj.applyIf(config, {parent:config.parent||mj.bd});
	if(config.wM){
		config.wM.add(this);
	}else{
		if(!window.windowManager)
			window.windowManager = new mj.windowManager();
		window.windowManager.add(this);
	}
	mj.window.superclass.constructor.call(this,config);
};
mj.window.prototype = {
	componentClass : 'mj.window',
	width : 600,
	height : 500,
	resizable : true,
	drag:true,
	destroyOnClose : false,
	minimizable : false,
	maximizable : false ,
	closable : true,
	opacity : 1,
	init : function(){
		var t = this, n = mj.NE, el = t.el = n(t.renderTo,{cls:'mj-win'+(t.cls ? ' '+t.cls : ''),style:'width:'+t.width+'px;height:'+t.height+'px;'}), _el = t._el = t.destroyEl = $(el);
		t.updating = true;
		t.width = _el.width();
		t.height = _el.height();
		t._cnt = _el;
		t.els ={
			tl : $(n(el, {cls:'mj-win-tl'})),
			tc : $(n(el, {cls:'mj-win-tc mj-unselectable', unselectable:'on'})),
			tr : $(n(el, {cls:'mj-win-tr'})),
			lc : $(n(el, {cls:'mj-win-lc'})),
			rc : $(n(el, {cls:'mj-win-rc'})),
			bl : $(n(el, {cls:'mj-win-bl'})),
			bc : $(n(el, {cls:'mj-win-bc'})),
			br : $(n(el, {cls:'mj-win-br'})),
			cs : $(n(el, {cls:'mj-win-cs'})),
			cn : $(n(el, {cls:'mj-win-cn'})),
			cc : $(n(el, {cls:'mj-win-cc mj-resize-handle', style:'opacity:'+t.opacity+';-moz-opacity:'+t.opacity+';filter:alpha(opacity='+(t.opacity*100)+');'}))
		};
		t.els.tcDrag = $(n(t.els.tc, {cls:'mj-win-drag'}));
		t.els.cc.bind('mousedown', function(){
			t.manager.bringToFront.call(t.manager, t);
		});
		t.els.tcDrag.bind('mousedown', function(){
			t.manager.bringToFront.call(t.manager, t);
		});		
		t.title = mj.translate(t.title);
		t.titleEl=n(t.els.tcDrag, {tag:'span', html:t.title});
		t.body = t.els.cc;
		t.body.win=t;
		t.getBody = function(){
			return t.body;
		};
		t.waitMask = $(n(t.els.cc,{
			cls:'mj-page-wait-mask mj-opacity-8',
			style:'display:none;width:100%;height:100%;',
			html:'<table width="100%" height="100%"><tr><td align="center" valign="middle"><img src="'+mj.glb.imagePath+'ajax-loader.gif"/><br/><br/><span class="mj-page-wait-title">'+mj.lng.glb.pleaseWait+'</span></td></tr></table>'
		}));
		t.waitMask.renderTo = t.els.cc;
		t.addRelated(t.waitMask);
		if(t.minimizable){
			var cm = t.els.minimize = $(n(t.els.tc, {cls:'mj-win-btn mj-win-minimize'}));
			cm.hover(function(){
				cm.addClass('mj-win-minimize-hover');
			},function(){
				cm.removeClass('mj-win-minimize-hover');
			});
			var cmfn = t.minimize;
			cm.click(function(){
				cmfn.call(t,t.swf);
			});
			if(!t.maximizable)
				cm.css('right','14px');
		}
		if(t.maximizable){
			var cma = t.els.maximize = $(n(t.els.tc, {cls:'mj-win-btn mj-win-maximize'}));
			cma.hover(function(){
				if(t.maximized)
					cma.addClass('mj-win-restore-hover');
				else
					cma.addClass('mj-win-maximize-hover');
			},function(){
				cma.removeClass('mj-win-restore-hover');
				cma.removeClass('mj-win-maximize-hover');
			});
			var cmafn = t.maximize;
			cma.click(function(){
				cmafn.call(t);
			});
			t.els.tc.dblclick(function(){
				cmafn.call(t);
			});
		}
		if(t.closable){
			var c = t.els.close = $(n(t.els.tc, {cls:'mj-win-btn mj-win-close'}));
			c.hover(function(){
				c.addClass('mj-win-close-hover');
			},function(){
				c.removeClass('mj-win-close-hover');
			});
			var cfn = t.close;
			c.click(function(){
				cfn.call(t);
			});
		}

		if(t.drag) {
			t.dragHandle = new mj.drag({el:el,dragEl:t.els.tcDrag,parent:t.parent});
			t.dragHandle.on('beforedrag',function(){
				if(this.maximized) return false;
			},t);
		}	
		if(t.resizable){
			t.resizer = new mj.resizer({
				el : el,
				minWidth : t.minWidth||t.width,
				maxWidth : t.maxWidth,
				minHeight : t.minHeight||t.height,
				maxHeight : t.maxHeight
			});
		}
		if(t.buttons){
			t.buttonsConfig = t.buttons;
			t.buttons = null;
			if(t.buttonsConfig)
				$(t.buttonsConfig).each(function(){
					t.addButton.call(t,this);
				});
		}
		if(t.tbar){
			mj.apply(t.tbar,{renderTo:t.els.cn});
			t.tbar = new mj.menu(t.tbar);
		}
		t.updating = null;
		t._layout();
		var b = $(document.body), l = typeof t.left != 'undefined'?t.left:Math.floor((b.width()-t.width)/2), top = typeof t.top != 'undefined'?t.top:Math.floor(($(document).height()-t.height)/2);
		t._el.css('left', l>0 ? l : 0);
		t._el.css('top', top>0 ? top : 0);
		t._el.resize(function(){
			var w = t.width = _el.width(), h = t.height = _el.height();
			t.trigger('resize', t, w, h);
		});
		t._el.bind('resize',function(){
			t.width = _el.width();
			t.height = _el.height();
			t._layout();
			t.trigger('afterresize', t, t.width, t.height);
		});
		if(t.autoLoad){
			window.tmp = t.body;
			t.autoLoad.url += '?h=tmp';
			//mj.load(t.body,t.autoLoad);
			$(t.body).load(t.autoLoad.url, t.autoLoad, function(responseText, textStatus, jqXHR){
				if(typeof t.app != 'undefined' && typeof t.app.reload == 'function' && jqXHR.status == 403){
					t.app.reload();
				} else {
					t.trigger('load', t);
				}
			});
			t.loaded = true;
		}
		t.relatedItems = [];
		t.on('hide', t._onHide, t);
		t.on('beforeclose', t._onBeforeClose, t);
		mj.bindResize(t.el,t.doFit, t);			
		//e.cc.kkresizewidth(w-2, true);
		mj.window.superclass.init.call(t);
	},
	_onHide : function(){
		if(this.subWindows){
			var s = $(this.subWindows);
			return s.eachR(function(){
				return this.hide();
			});
		}
		return true;
	},
	_onBeforeClose : function(){
		if(this.subWindows){
			var s = $(this.subWindows);
			return s.eachR(function(){
				return this.close();
			});
		}
		return true;
	},
	_layout : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		if(!this.updating){
			var t=this,e=t.els, w = t.width-12, h = t.height-29;
			e.cc[0].style.overflow = "hidden";
			t._cnt.width(t.width);
			t._cnt.height(t.height);
			e.tc.width(w);
			e.bc.width(w);
			e.lc.height(h);
			e.rc.height(h);
			e.cc.width(w-2);
			//e.cc.kkresizewidth(w-2, true);
			e.cs.width(w+2);
			e.cn.width(w-2);
			if(t.buttons){
				e.cs.height(26);
				h = h - 29;
			}
			if(t.tbar){
				e.cn.height(25);
				e.cc.css('top', $.browser.msie ? 48 : 49);
				h = h - 26;
			}
			e.cc.height(h+($.browser.msie ? 1 : 0));
			t.els.tcDrag.width(w- 15);
			e.cc[0].style.overflow = "auto";
			e.cc.trigger('kkresize', {sourceEl:this});
		}
	},
	show : function(swf){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		var t=this;
		t.trigger('show', this);
		t.trigger('activate', this);
		if(swf){
			this._el.css('visibility','visible');
			swf.css('visibility','visible');
		}else
			t._el.show();
		t.els.cc.trigger('kkresize', {sourceEl:this});
		t.trigger('aftershow', this);
		t.minimized = false;	
	},
	hide : function(){
		if(this.trigger('hide', this))
			this._el.hide();
	},
	minimize : function(swf){
		if(this.trigger('minimize', this)){
			if(swf){
				this._el.css('visibility','hidden');
				swf.css('visibility','hidden');
			}else	
				this._el.hide();
			this.minimized = true;	
		}
	},	
	close : function(){
		if(this.trigger('beforeclose')===false)
			return false;
		if(this.destroyOnClose){
			if(this.trigger('close', this)!==false)
				this.destroy();
			return false;
		}else
			return this.hide();
	},
	resize : function(w,h){
		this.resizing = true;
		this.onWidth(w);
		this.onHeight(h);
	},
	onWidth : function(w){
		this._el.width(w);
		this._layout();
		if(!this.resizing)
			this.trigger('resize', this, w, this._el.height());
		this.resizing = null;
	},
	onHeight : function(h){
		this._el.height(h);
		this._layout();
	},
	doFit : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		if(this.maximized){
			var w,h;
			w = $(this.parent).width();
			h = $(this.parent).height();	
			this.width = parseInt(w);
			this.height = parseInt(h);		
			this.onWidth(w);
			this.onHeight(h);
			this.trigger('afterresize', this, w, h);		
		}
	},
	maximize : function(){
		var w,h,t,l;
		if(!this.maximized){
			this.trigger('maximizeclick',this);
			this.oldPosition = {
				top : this._el.css('top'),
				left : this._el.css('left'),
				width : this._el.width(),
				height : this._el.height()
			};
			w = $(this.parent).width();
			h = $(this.parent).height();
			t = ($(this.parent).offset()).top+'px';
			//if($.browser.msie)
				t='0px';
			l = '0px';
			this.maximized = true ;
			this.els.maximize.addClass('mj-restore-btn');
		}else{
			w = this.oldPosition.width;
			h = this.oldPosition.height;
			t = this.oldPosition.top;
			l = this.oldPosition.left;			
			this.maximized = false ;
			this.oldPosition = null;
			this.els.maximize.removeClass('mj-restore-btn');
		}
		this._el.css('top',t);
		this._el.css('left',l);
		this.width = parseInt(w);
		this.height = parseInt(h);
		this.onWidth(w);
		this.onHeight(h);
		this.trigger('afterresize', this, w, h);
	},		
	addButton : function(config){
		var t=this;
		if(!t.buttons)
			t.buttons = [];
		var b = config;
		if(!b.componentClass){
			mj.apply(b, {renderTo:mj.NE(t.els.cs)});
			b = new mj.button(b);
		}
		b.window = t;
		t.buttons.push(b);
		t._layout();
	},
	add : function(win){
		var t = this;
		if(!t.subWindows)
			t.subWindows = [];
		win.on('close', function(){
			t.subWindows.splice(win._subIndex, 1);
		});
		win._subIndex = t.subWindows.push(win);
	},
	setTitle : function(text){
		if(this.titleEl){
			this.title = mj.translate(text);
			this.titleEl.innerHTML = this.title;
		}
	},
	waitMaskResize : function(){
		var wm = this.waitMask;
		wm.css({'width':wm.renderTo.width(), 'height':wm.renderTo.height()});
	},
	waitMaskShow : function(){
		this.waitMask[0].style.display='block';
	},
	waitMaskHide : function(){
		this.waitMask[0].style.display='none';
	}
};
mj.extend(mj.window, mj.component);
//!!! myjui/source/window/message.js
mj.message = function(config){
	if(typeof config==='string')
		config = {msg:config};
	config.msg=mj.translate(config.msg);
	config.destroyOnClose = config.destroyOnClose || true;
	var i = window.windowManager ? window.windowManager.activeIndex+1 : 1000, def = mj.message.defaults;
	var _bT = {
		'OK' : mj.lng.titles.buttons.ok,
		'CANCEL' : mj.lng.titles.buttons.cancel,
		'YES' : mj.lng.titles.buttons.yes,
		'NO' :  mj.lng.titles.buttons.no
	};
	def.buttonTitles = def.buttonTitles ? mj.apply(def.buttonTitles, _bT) : _bT;
	var cnt = config.renderTo = config.renderTo ? config.renderTo : mj.NE();
	mj.applyIf(config, def);
	var btnCb = function(btn, h){
		var fn = config.cb;
		if(typeof fn === 'function')
			fn.call(config.scope, btn, btn.handlerId);
		else{
			btn.window.close();
		}
	};
	if(config.buttons){
		var btns=[],cnfb = config.buttons;
		for(var i =0 , l = cnfb.length; i<l ; i++){
			if(typeof cnfb[i] === 'string')
				btns.push({title: def.buttonTitles[cnfb[i]], handler: btnCb, handlerId : cnfb[i]});
			else
				btns.push(cnfb[i]);
		}
	}
	config.buttons = btns;
	var win = mj.message.activeMessageWin = new mj.window(config);
	win.on('close', function(){
		$(cnt).remove();
		mj.message.activeMessageWin = false;
	});
	mj.NE(win.body, {html:config.msg, cls:'mj-message-text'});
	win.show();
};
mj.message.defaults = {
	cls : 'mj-message-window',
	modal : false,
	width : 400,
	height : 200,
	title : 'Bilgi',
	buttons : [
		'OK'
	],
	buttonTitles : {}, //silme
	cb:function(el,btn){
		el.window.close();
	}
};
//!!! myjui/source/desktop/desktop.js
mj.desktop = function(config){
	mj.desktop.superclass.constructor.call(this, config);
};
mj.desktop.prototype = {
	componentClass : 'mj.desktop',
	position : 'bottom',
	initTabWidth : 150,
	background : '#fff',
	contextMenu : true,
	//fishEye : false,
	multipleWindow : false,
	helpPath : false,
	showSystemTray : false,
	deskIconTpl : new mj.template(['<div class="desktop-icon" id="deskItem-{id}" style="position:absolute;{positionStyle}">',
				'<table><tr><td align="center"><div class=""><img id="img-{id}" src="{deskIcon}" style="z-index:0;" title="{fullTitle}"></div></td></tr>',
				'<tr><td align="center"><span id="span-{id}"  class="x-editable" title="{fullTitle}">{title}</span></td></tr></table></div>']),
	QLIconTpl : new mj.template('<div class="quick-launch-item" id="QLItem-{id}"><img id="ql-img-{id}" src="{QLIcon}" title="{title}"></div>'),
	render : function(){
		var t=this;
		
		if(t.position=='top'){
			t.tBarCnt = mj.NE(t.renderTo,{tag:'div', cls:'mj-desktop-taskbar-top'});
			t.body = mj.NE(t.renderTo,{tag:'div',cls:'mj-desktop-body mj-resize-handle',style:'background:'+t.background+';overflow:hidden;width:'+(t.renderTo.width())+'px;height:'+(t.renderTo.height()-28)+'px;'});
		}else if(t.position=='bottom'){
			t.body = mj.NE(t.renderTo,{tag:'div',cls:'mj-desktop-body mj-resize-handle',style:'background:'+t.background+';overflow:hidden;width:'+(t.renderTo.width())+'px;height:'+(t.renderTo.height()-28)+'px;'});
			t.tBarCnt = mj.NE(t.renderTo,{tag:'div', cls:'mj-desktop-taskbar-bottom'});
		} 
		t.tBarMenu = mj.NE(t.tBarCnt,{tag:'div', cls:'mj-desktop-taskbar-menu'});
		t.tBarQL = mj.NE(t.tBarCnt,{tag:'div', cls:'mj-desktop-taskbar-ql'});
		t.tBarTab = mj.NE(t.tBarCnt,{tag:'div', cls:'mj-desktop-taskbar-tab'});
		if(t.showSystemTray){
			t.tBarRA = mj.NE(t.tBarCnt,{tag:'div', cls:'mj-desktop-taskbar-ra'});
			mj.loading = $(mj.NE(t.tBarRA, {cls:'mj-desktop-taskbar-loader'}));
		}
		t.tabContainerWidth = t.renderTo.width()-310;
		var ofs=$(t.tBarQL).offset();
		width=$(t.tBarQL).width();
		height=$(t.tBarQL).height();
		ofs.right = ofs.left + width;
		ofs.bottom = ofs.top + height;

		$().mousemove(function(e){
			if(window.mjDragging){
	            x=e.clientX;
				y=e.clientY;
				if( x>= ofs.left &&x <= ofs.right &&y >= ofs.top &&y <= ofs.bottom){
					$(t.tBarQL).addClass('mj-ql-hover');
					t.onQL=true;
				} 		
				else {
					$(t.tBarQL).removeClass('mj-ql-hover');
					t.onQL=false;
				}
			}
		});

		t.tBarSD = mj.NE(t.tBarQL,{tag:'div', cls:'mj-desktop-taskbar-show-desktop'});
		$(t.tBarSD).bind('click',{scope:t},function(e){
			var s = e.data.scope,w;
			if(s.beforeSD.length>0){
				for(var i=0,len=s.beforeSD.length;i<len;i++){
					w = s.windows[s.beforeSD[i]].window;
					w.show(w.swf);
				}
				s.beforeSD=[];	
			}else{
				while(s.winHistory.length>0){
					s.beforeSD.push(s.winHistory[0]);
					w = s.windows[s.winHistory[0]].window;
					w.minimize(w.swf);		
				}
			
			}
		});
		
		if(t.menuItems){
			mj.apply(t.menuItems,{renderTo:t.tBarMenu,drag : true});
			t.menu = new mj.menu(t.menuItems);
			t.menu.on('itemclick',t._menuItemClick,t);
			t.menu.on('beforedrag',function(d,e,b){
				return !b.subMenu;
			},this);
			t.menu.on('dragstop',function(e,b){
				if(mj.getIndex(this.desktopItems,'id',b.id)==-1){
					var el = $(this.menu.activeSub?this.menu.activeSub._el:b.renderTo);
					var reg=el.offset();
					reg.right=reg.left + el.width();
					reg.bottom=reg.top + el.height();
					var t=parseInt(e.proxy[0].style.top),l=parseInt(e.proxy[0].style.left);
					if(l<reg.left||l>reg.right||t<reg.rop||t>reg.bottom){
						var str = 'top:'+t+'px;left:'+l+'px;'
						var x = this.getMenuItemById(this.menuItems.items,b.id);
						x.positionStyle = str;
						if(x)
							this.desktopItems.push(x)
						this.addDesktopIcon(this.desktopItems.length-1,x);				
						setCookie('deskItem-'+b.id,str,365,'/');
					}
				}
			},this);
		
		}
		if(t.desktopItems){
			var cookieIcons=getCookieArray('deskItem-');
			for(var i=0,len = cookieIcons.length;i<len;i++){
				var x = cookieIcons[i].replace('deskItem-','').trim();
				t.desktopItems.remove(x);
				t.desktopItems.push(x);
			}
		
			for(var i = 0, len = t.desktopItems.length; i < len; i++){
				var x = t.getMenuItemById(t.menuItems.items,t.desktopItems[i]);
				if(x)
					t.desktopItems[i] = x;
			}
			t.showDesktopIcon();
		}
		if(t.QLItems){
			var QLIcons=getCookieArray('QLItem-');
			for(var i=0,len = QLIcons.length;i<len;i++){
				var x = QLIcons[i].replace('QLItem-','').trim();
				t.QLItems.remove(x);
				t.QLItems.push(x);
			}
			for(var i = 0, len = t.QLItems.length; i < len; i++){
				var x = t.getMenuItemById(t.menuItems.items,t.QLItems[i]);
				if(x)
					t.QLItems[i] = x;
			}
			t.showQLIcon();
		}
		// if(t.fishEye)
			// t.addFishEyeMenu();
		/*var bgCook=getCookie('backgroundImage');
		if(bgCook)
			this.changeBackground(bgCook);*/
		$(this.body).bind('contextmenu',{scope : this},function(e){
			if($(e.target).hasClass('mj-desktop-body')){
				/*var tb = new mj.menu({
					renderTo : mj.NE(),
					canHide : true,
					style : 'vertical',
					width : 170,
					items : [
						{id:'_1', title:'Masaüstü Değiştir',iconCls:''}
					]
				});
				if(e.data.scope.contextMenu){
					tb.trigger('show',tb, e.pageY, e.pageX);
					tb.showAt(e.pageX, e.pageY);
				}*/
				e.preventDefault();
				e.stopPropagation();
				/*tb.on('itemclick',function(a,b,c,d){
					this.scope.runApp(this.scope.getMenuItemById(this.scope.menuItems.items,'changeBackground'));
				},e.data);*/
			}
		});
		$(window).bind("resize", {scope:t},function(e){
			e.data.scope.doDesktop();
		});		
		mj.bindResize(t.body, t.doDesktop, t);		
	},	
	changeBackground : function(url){
		var bg = 'url(\''+url+'\')';
		$(this.body).css('background',bg);
		setCookie('backgroundImage',url,365,'/');
	},
	/* addFishEyeMenu : function(){
		var t=[	'<td class="fisheye" style="vertical-align:bottom;"><div><img src="{FEIcon}" title="{title}" style="width:48px;height:48px;margin-top:0px;"></div></td>'];
		this.shiftFish = mj.NE(this.body,{tag:'div',style:'width:1px;height:'+($(this.body).height()-150)+'px'});
		var tmp=mj.NE(this.body,{tag:'table',style : 'width :100%;height:150px;',html:'<tbody><tr><td style="width:50%;"><td style="vertical-align:bottom;"><table><tr class="fisheye"></tr></table></td><td style="width:50%;"></tr></tbody>'});
		var el=$(".fisheye",tmp)[0];
		$(tmp).bind('contextmenu',{scope : this},function(e){
			e.preventDefault();
			e.stopPropagation();
		});
		this.fisheyeMenu = new mj.fisheye({
			size : 48,
			magnitude : 1.5,
			renderTo : el,
			mouseCnt : tmp,
			store : new mj.store({
					data : this.QLItems
				}),
			tpl : new mj.template(t),
			overClass : false,
			selectedClass : false,
			selector : 'td.fisheye'
		});
		this.fisheyeMenu.on('itemclick',function(x){
			//var eldiv=$("div",x.selections[0].el);
			//var h=$("img",x.selections[0].el).height();
			this.runApp(x.selections[0].store);
		},this);
		this.fisheyeMenu.load();
	}, */
	addQLIcon : function(i,x){
		var t = this;
		$(this.tBarQL).append(this.QLIconTpl.apply(x));
		this.QLItems[i].qel = mj.get('QLItem-'+this.QLItems[i].id);
		$(this.QLItems[i].qel).bind('contextmenu',{item:this.QLItems[i],scope:this},function(e){
			var tb = new mj.menu({
				renderTo : mj.NE(),
				canHide : true,
				style : 'vertical',
				width : 130,
				items : [
					{id:'_1', title:'Sil',iconCls:'mj-menu-delete-icon'}
				]
			});
			tb.trigger('show',tb, e.pageY, e.pageX);
			tb.showAt(e.pageX, e.pageY);
			e.preventDefault();
			e.stopPropagation();
			tb.on('itemclick',function(a,b,c,d){
				this.scope.removeQLIcon(this.item);
			},e.data);
		});
		$(this.QLItems[i].qel).bind('click',{item:this.QLItems[i],scope:this},function(e){
			e.data.scope.runApp(e.data.item);
		
		});		
		/* if(t.fishEye&&t.fisheyeMenu){
			$(t.fisheyeMenu.renderTo).empty();
			t.fisheyeMenu.load();
		}	 */
	},
	showQLIcon : function(){
		for(var i = 0, len = this.QLItems.length; i < len; i++){
			var x=this.QLItems[i];
			if(typeof x=='object'){	
				this.addQLIcon(i,x)
			}
		}	
	},
	removeQLIcon : function(x){
		this.QLItems.remove(x)
		$(x.qel).remove();
		deleteCookie('QLItem-'+x.id,'/');
		/* if(this.fishEye&&this.fisheyeMenu){
			$(this.fisheyeMenu.renderTo).empty();
			this.fisheyeMenu.load();
		}	 */
	},	
	showDesktopIcon : function(){
		var pos = {w:$(this.body).width(),h:$(this.body).height()};
		for(var i = 0, len = this.desktopItems.length; i < len; i++){
			var x=this.desktopItems[i];
			if(typeof x=='object'){
				var cook=getCookie('deskItem-'+x.id);
				if(!cook)
					if(x.position){
						if(x.position.start&&x.position.start!='tl'){
							switch(x.position.start){
								case 'tr' :
									x.position.x = pos.w-parseInt(x.position.x);
									break;
								case 'bl' :
									x.position.y = pos.h-parseInt(x.position.y);
									break;
								case 'br' :
									x.position.x = pos.w-parseInt(x.position.x);
									x.position.y = pos.h-parseInt(x.position.y);
									break;
							}		
						}
						x.positionStyle = 'top:'+x.position.y+'px;left:'+x.position.x+'px;'
					}else{
						x.positionStyle = 'top:'+this.iconTop+'px;left:'+this.iconLeft+'px;'
						if(this.iconTop+140<pos.h){
							this.iconTop=20;
							this.iconLeft+=70; 
						}else
							this.iconTop+=70;
					}				
				else
					x.positionStyle = cook;
				this.addDesktopIcon(i,x);

			}
		}
	},
	removeDesktopIcon : function(x){
		this.desktopItems.remove(x)
		$(x.el).remove();
		deleteCookie('deskItem-'+x.id,'/');
	},
	deselectAll : function(){
		var t = this;
		for(var i=0,l=t.desktopItems.length;i<l;i++){
			var item = t.desktopItems[i];
			if(item.el){
				$(item.el).removeClass('mj-desktop-icon-selected');
				t.setTitle(item,item._shortTitle);
			}
		}
	},
	setTitle : function(item, title){
		item.titleEl[0].innerHTML = title;
	},
	addDesktopIcon : function(i,x){
		var t = this;
		var title=x.title;
		// title = title.split(' ');
		// var j=0;
		// for(var k=0,l=title.length;k<l;k++){
			// j+=title[k].length;
			// if(j>10){
				// title[k]=title[k]+'<br/>';
				// j=0;
			// }
		// }
		// title = title.join(' ');
			
		this.desktopItems[i]._title = title;
		this.desktopItems[i]._shortTitle = title;//(x.title.length>12) ? (x.title.slice(0,10)+'...') : x.title;
		x.title = this.desktopItems[i]._shortTitle;
		x.fullTitle = this.desktopItems[i]._title;
		$(this.body).append(this.deskIconTpl.apply(x));
		this.desktopItems[i].el = mj.get('deskItem-'+this.desktopItems[i].id);
		this.desktopItems[i].titleEl = $('span', this.desktopItems[i].el);
		$(this.desktopItems[i].el).bind('contextmenu',{item:this.desktopItems[i],scope:this},function(e){
			var tb = new mj.menu({
				renderTo : mj.NE(),
				canHide : true,
				style : 'vertical',
				width : 130,
				items : [
					{id:'_1', title:mj.lng.glb.del,iconCls:'mj-menu-delete-icon'}
				]
			});
			tb.trigger('show',tb, e.pageY, e.pageX);
			tb.showAt(e.pageX, e.pageY);
			e.preventDefault();
			e.stopPropagation();
			tb.on('itemclick',function(a,b,c,d){
				this.scope.removeDesktopIcon(this.item);
			},e.data);
		});
		$(this.body).bind('mousedown',{scope : this},function(e){
			e.data.scope.deselectAll();
		});
		$(this.desktopItems[i].el).bind('click',{item:this.desktopItems[i],scope:this},function(e){
			//t.deselectAll();
			$(e.data.item.el).addClass('mj-desktop-icon-selected');
			t.setTitle(e.data.item,e.data.item._title);
		});
		$(this.desktopItems[i].el).bind('dblclick',{item:this.desktopItems[i],scope:this},function(e){
			e.data.scope.runApp(e.data.item);
		});		
		if(!this.desktopItems[i].fixed){
			this.desktopItems[i].dragHandle = new mj.drag({
				el:this.desktopItems[i].el,
				parent:this.body,
				proxy:false});	
			this.desktopItems[i].dragHandle.on('dragstart',function(e,b){
				var el = $(this.el);
				this.beforeDragXY = {top:el.css('top'),left:el.css('left')};
			},this.desktopItems[i]);
			this.desktopItems[i].dragHandle.on('dragstop',function(e,b){
				if(t.onQL){
					if(mj.getIndex(t.QLItems,'id',this.id)==-1){
						if(t.QLItems.length<5){
							t.QLItems.push(this);
							t.addQLIcon(t.QLItems.length-1,this);	
							setCookie('QLItem-'+this.id,'OK',365,'/');
						}
					}
					var el = $(this.el);
					el.css('top',this.beforeDragXY.top);
					el.css('left',this.beforeDragXY.left);	
					$(t.tBarQL).removeClass('mj-ql-hover');	
				}else{
					var str = 'top:'+parseInt(e._el[0].style.top)+'px;left:'+parseInt(e._el[0].style.left)+'px;'
					setCookie('deskItem-'+this.id,str,365,'/');
				}
			},this.desktopItems[i]);
		}
	},
	getMenuItemById : function(items,id){
		for(var i = 0, len = items.length; i < len; i++){
			if(items[i].items){
				var r = this.getMenuItemById(items[i].items,id);
				if(r)
					return r;
			}else{
				if(items[i].id==id)	
					return items[i];
			}
		}
		return false;
	},
	doDesktop : function(){
		if(arguments && arguments[0] && arguments[0][1]){
			var data = arguments[0][1];
			if(data.sourceEl == this)
				return;
		}
		$(this.body).width(this.renderTo.width());
		var h=this.renderTo.height()-28;
		$(this.body).height(this.renderTo.height()-28);
		if(this.shiftFish)
			$(this.shiftFish).height(h-150);
		/* if(this.fishEye&&this.fisheyeMenu)
			this.fisheyeMenu.getCenterPoint();*/
		$(this.body).trigger('kkresize', {sourceEl:this});
	},
	_menuItemClick : function(a,b,c){
		if(b.url || b.script || b.modulename){
			if(b.modulename){
				var _x_=b.modulename.split('-');
				if(_x_.length>1){
					b.modulename=_x_[0];
					b.reportname=_x_[1];
				}
			}else{
				b.reportname=null;
			}
			return this.runApp(b);
		}
	},
	runApp : function(item){
		var t=this,w,_x;
		if(item.optparams)
			eval('_x='+item.optparams);
		if(item.script){
			$.globalEval(item.script);
		}else{
			if(item.multiplewindow || !t.windows[item.id]){
				window.tmpModule = item;
				var winDesktopId = item.multiplewindow ? mj.genId('win') : item.id;
				var windowConfig = {
					app: t.app,
					cls: typeof item.cls !== 'undefined' ? item.cls : '',
					destroyOnClose: true,
					minimizable: typeof item.minimizable !== 'undefined' ? item.minimizable : true,
					maximizable: typeof item.maximizable !== 'undefined' ? item.maximizable : true,
					closable: typeof item.closable !== 'undefined' ? item.closable : true,
					resizable: typeof item.resizable !== 'undefined' ? item.resizable : true,
					wM: t.app.desktop.wM,
					id: item.modulename,
					parent: t.app.desktop.body,
					renderTo: t.app.desktop.body,
					modal: item.modal,
					title: item.fullTitle || item.title,
					width: item.width || t.defaultWindowWidth,
					height: item.height || t.defaultWindowHeight,
					minWidth: item.minWidth || item.width,
					minHeight: item.minHeight || item.height,
					buttons: item.buttons
				};
				if(typeof Routing != 'undefined'){
					windowConfig.autoLoad = {
						url: typeof Routing != 'undefined' ? Routing.generate(item.modulename + "-module") : '',
						modulename: item.modulename,
						reportname: item.reportname,
						menuId: item.id
					};
				}
				var win = new mj.window(windowConfig);
				window.tmpModule.win=win;
				if(_x&&_x.maximized)
					win.maximize();
				win.on('close',function(x,y){
					var t=this;
					win.tab.remove();
					win=null;
					this.winHistory.remove(x.winDesktopId);//?				
					//this.windows[item.multiplewindow?x.winDesktopId:x.id]=null;
					this.windows[x.id]=null;
					this.windows[x.winDesktopId]=null;
					if(this.tabWidth<this.initTabWidth&&(this.winHistory.length)*(this.tabWidth+12)<(this.tabContainerWidth-12)){
						this.tabWidth = parseInt((this.tabContainerWidth-12)/(this.winHistory.length))-12
						if(this.tabWidth>this.initTabWidth)
							this.tabWidth = this.initTabWidth;
						$(".mj-desktop-taskbar-tab-item").width(this.tabWidth);
						$(".mj-desktop-taskbar-tab-item span").each(function(e){
							this.innerHTML = this.getAttribute('name').ellipse(parseInt((t.tabWidth)/6));
						});
					}
					if(this.winHistory.length>0&&!this.windows[this.winHistory[this.winHistory.length-1]].window.minimized)
						this.windows[this.winHistory[this.winHistory.length-1]].window.show();
				},t);
				win.on('activate', function(win){
					t.activeWindow = win;
				});
				win.on('minimize',function(x,y){
					this.windows[x.winDesktopId].tab.removeClass('active-tab');
					this.winHistory.remove(x.winDesktopId);
					if(this.winHistory.length>0&&!this.windows[this.winHistory[this.winHistory.length-1]].window.minimized)
						this.windows[this.winHistory[this.winHistory.length-1]].window.show();
				},t);			
				var obj={window : win};
				if(item.multiplewindow)
					win.winDesktopId = t.windows.push(obj,winDesktopId);
				else{
					t.windows[item.id] = obj;
					win.winDesktopId = item.id;
				}
				obj.id=win.winDesktopId;
				if((t.winHistory.length+1)*(t.tabWidth+12)>(t.tabContainerWidth-12)){
					t.tabWidth = parseInt((t.tabContainerWidth-12)/(t.winHistory.length+1))-12
					$(".mj-desktop-taskbar-tab-item").width(t.tabWidth);
					$(".mj-desktop-taskbar-tab-item span").each(function(e){
						this.innerHTML = this.getAttribute('name').ellipse(parseInt((t.tabWidth)/6));
					});
				}	
				var tb = obj.tab = win.tab = $(mj.NE(t.tBarTab,{tag:'div', cls:'mj-desktop-taskbar-tab-item mj-unselectable',style:'width:'+(t.tabWidth)+'px;',html:'<span name="'+item.title+'">'+item.title.ellipse(parseInt((t.tabWidth)/6))+'</span>'}));
				tb.bind('contextmenu',{obj:obj,scope:t},function(e){
					var tb = new mj.menu({
						renderTo : mj.NE(),
						canHide : true,
						style : 'vertical',
						width : 130,
						items : [
							{id:'_1', title:'Kapat',iconCls:'mj-menu-close-icon'}
						]
					});
					tb.trigger('show',tb, e.pageY, e.pageX);
					tb.showAt(e.pageX, e.pageY);
					e.preventDefault();
					e.stopPropagation();
					tb.on('itemclick',function(a,b,c,d){
						this.obj.window.close();
					},e.data);
				});
				tb.bind('click',{obj:obj,scope:t},function(e){
					var s = e.data.scope,obj=e.data.obj;
					if($(this).hasClass('active-tab')){
						obj.window.minimize(obj.window.swf);
					}else{
						obj.window.show(obj.window.swf);
					}
				});
			}
			w = (obj||t.windows[item.id]).window;
			w.show(w.swf);
			w.waitMaskShow();
			return w;
		}
	},
	showHelp : function(helpId){
		if(this.helpPath){
			var url = helpId ? (this.helpPath+'index.html?context='+helpId) : this.helpPath+'index.html';
			this.helpWin = mj.newWindow({url:url});
		}
	},
	init : function(){
		var t = this, wM = window.windowManager ? window.windowManager : new mj.windowManager();
		t.wM = window.windowManager = wM;
		t.tabStrLen =t.initTabStrLen;
		t.tabWidth =t.initTabWidth;
		t.iconLeft = 20;
		t.iconTop = 20;		
		var _w = t.windows={};
		t.windows.push = function(o,id){
			o.winDesktopId = id;
			_w[o.winDesktopId]=o;
			return o.winDesktopId;
		};
		t.winHistory=[];
		t.beforeSD=[];
		t.render();
		t.wM.on('activate',function(e){
			if(e.winDesktopId){
				$(".mj-desktop-taskbar-tab-item").removeClass('active-tab');
				this.windows[e.winDesktopId].tab.addClass('active-tab');
				this.winHistory.remove(e.winDesktopId);
				this.winHistory.push(e.winDesktopId);
			}
		},t);
		if(t.showSystemTray){
			mj.loaderShow = function(){
				mj.loading.addClass('show-loader');
			};
			mj.loaderHide = function(){
				mj.loading.removeClass('show-loader');
			};		
			$().ajaxStart(function(){
				mj.loaderShow();
			}).ajaxStop(function(){
				mj.loaderHide();
			});
		}
		var _t = this;
		mj.shortcuts.on('F1', function(){
			_t.showHelp(_t.activeWindow && _t.activeWindow.helpId ? _t.activeWindow.helpId : false);
		});
		t.fakeInput = mj.NE(mj.NE(mj.bd,{style:'display:none;position:absolute;left:-9999px;top:-9999px;'}),{tag:'input'});
		if(!$.browser.msie)
			t.fakeInput.focus();
	}
};
mj.extend(mj.desktop, mj.component);
mj.uretimProgramiView = function(config){
	mj.uretimProgramiView.superclass.constructor.call(this, config);
};
mj.uretimProgramiView.prototype = {
	componentClass : 'mj.uretimProgramiView',
	init : function(){
		var t = this, n = mj.NE;
		t.isAppPres=document.location.href.indexOf('172.16.1.149/pres')>-1;
		t.store.on('load', function(){
			t._load();
		});
		//var lng = mj.lng.titles.modules.rprUretimProgrami;
		t.el = t.destroyEl = $(n(t.renderTo,{
			tag:'table', 
			style:'font-size:11px;font-family:Tahoma,Verdana,Arial;margin:5px;border-collapse:collapse;width:890px;', 
			align:'center', cellpadding:'2', cellspacing:'0', 
			children:[
				{
					tag:'thead', 
					style : 'white-space:wrap;font-weight:bold;',
					align : 'center',
					children : [
						{
							tag:'tr', 
							html : '<td style="border:1px solid black;width:150px;">İstasyon</td>'+
							'<td style="border:1px solid black;width:50px;">Sıra</td>'+
							'<td style="border:1px solid black;width:110px;white-space:wrap;">Operasyon</td>'+
							(t.isAppPres?'<td style="border:1px solid black;width:110px;white-space:wrap;">Kalıp</td>':'')+
							'<td style="border:1px solid black;width:70px;white-space:normal;">Erp Ref No</td>'+
							'<td style="border:1px solid black;width:70px;white-space:normal;text-align:center;vertical-align:middle;">Adam Sayı</td>'+
							'<td style="border:1px solid black;width:70px;white-space:wrap;">Plan Miktar</td>'+
							'<td style="border:1px solid black;width:70px;white-space:wrap;">Saatlik Hedef</td>'+
							'<td style="border:1px solid black;width:70px;white-space:normal;text-align:center;">Pl. Başlama</td>'+
							'<td style="border:1px solid black;width:70px;white-space:normal;text-align:center;">Pl. Bitiş</td>'
						}
					]
				}
				,{
					tag:'tbody', 
					cls:'mj-rpr-body',
					style : 'white-space:nowrap;',
					align : 'center',
					children : [
						{
							tag:'tr', 
							html : '<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>'
						}
					]
				},{
					tag:'tfoot', 
					cls:'mj-rpr-footer',
					style : 'mso-height-source:userset;',
					align : 'left',
					children : [
						{
							tag:'tr', 
							html : '<td colspan="8" style="white-space:normal;text-align:left;vertical-align:top;">&nbsp;</td>'
						}
					]
				}
			]
		}));
		t._body = $('.mj-rpr-body', t.el);
		t._footer = $('.mj-rpr-footer', t.el);
		t.titles = {
			vardiya : $('.mj-rpr-vardiya', t.el),
			vardiyaSaati : $('.mj-rpr-vardiya-saati', t.el)
		};
		mj.uretimProgramiView.superclass.init.call(this);
	},
	copyContent : function(copyTo){
		mj.NE(copyTo, {tag:'table', style:'font-size:11px;font-family:Tahoma,Verdana,Arial;margin:5px;border-collapse:collapse;', align:'center', cellpadding:'2', cellspacing:'0', html : this.el[0].innerHTML});
	},
	_load : function(){
		var t = this, _b = t._body, d = t.store.data, p = d.program, tt = t.titles, n = mj.NE, lr = t._loadRenderer, _f = t._footer, notlar = d.notlar;

		t._body.empty();
		tt.vardiya.html(d.vardiyaAdi);
		tt.vardiyaSaati.html(d.tarihVardiya);
		for(var i=0,l=p.length;i<l;i++){
			var s = p[i], _tasks = s.tasks;
			var _t = _tasks[0] ? _tasks[0] : [];
			if(_tasks.length>0){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;font-size:12px;font-weight:bold;white-space:wrap;" align="center" valign="middle" rowspan="'+_tasks.length+'">'+s.code+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">1</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
			var _t = _tasks[1] ? _tasks[1] : [];
			if(_tasks.length>1){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">2</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
				
			var _t = _tasks[2] ? _tasks[2] : [];
			if(_tasks.length>2){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">3</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
			var _t = _tasks[3] ? _tasks[3] : [];
			if(_tasks.length>3){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">4</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
			var _t = _tasks[4] ? _tasks[4] : [];
			if(_tasks.length>4){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">5</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
			var _t = _tasks[5] ? _tasks[5] : [];
			if(_tasks.length>5){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">6</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
			var _t = _tasks[6] ? _tasks[6] : [];
			if(_tasks.length>6){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">7</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
			var _t = _tasks[7] ? _tasks[7] : [];
			if(_tasks.length>7){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">8</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
			var _t = _tasks[8] ? _tasks[8] : [];
			if(_tasks.length>8){
				n(_b, {
					tag:'tr', 
					html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">9</td>'+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opname'])+'</td>'+
						(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['opdescription'])+'</td>':'')+
						'<td style="border:1px solid black;text-align:left;vertical-align:middle;">'+lr(_t['erprefnumber'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['opcounter'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
						'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'
				});
			}
		}
		var str='';
		for(var i=0,l=notlar.length;i<l;i++){
			var s = notlar[i];
			str += s.description+'<br/>';
		}
		$('td',_f).html(str);
	},
	_loadRenderer : function(val){
		return val ? val : '&nbsp;';
	},
	saveAsXLS : function(filename){
		var t = this, n=mj.NE;
		var copyTo = $(n(mj.bd, {style:'display:none'}));
		mj.NE(copyTo, {tag:'table', style:'font-size:11px;font-family:Tahoma,Verdana,Arial;white-space:nowrap;margin:5px;border-collapse:collapse;', align:'center', cellpadding:'2', cellspacing:'0', html : this.el[0].innerHTML});
		var content = (t.fileTitle ? t.fileTitle : '')+copyTo[0].innerHTML;
		content = mj.escape(content);
		var frmCnt = $(n(mj.bd, {style:'display:none'}));
		var frm = n(frmCnt, {tag:'form',action:mj.glb.exportPath,method:'POST',html:'<input type="hidden" name="landscape" value="1"/><input type="hidden" name="fit" value="1"/><input type="hidden" name="format" value="xls"/><input type="hidden" name="content" value="'+content+'"/><input type="hidden" name="filename" value="'+filename+'"/>'});
		frm.submit();
		frmCnt.remove();
		copyTo.remove();
	}
}
mj.extend(mj.uretimProgramiView, mj.component);

mj.uretimProgramiView2 = function(config){
	mj.uretimProgramiView2.superclass.constructor.call(this, config);
};
mj.uretimProgramiView2.prototype = {
	componentClass : 'mj.uretimProgramiView2',
	init : function(){
		var t = this, n = mj.NE;
		t.isAppPres=document.location.href.indexOf('172.16.1.149/pres')>-1;
		t.store.on('load', function(){
			t._load();
		});
		//var lng = mj.lng.titles.modules.rprUretimProgrami;
		t.el = t.destroyEl = $(n(t.renderTo,{
			tag:'table', 
			style:'font-size:11px;font-family:Tahoma,Verdana,Arial;margin:5px;border-collapse:collapse;width:890px;', 
			align:'center', cellpadding:'2', cellspacing:'0', 
			children:[
				{
					tag:'thead', 
					style : 'white-space:wrap;font-weight:bold;',
					align : 'center',
					children : [
						{
							tag:'tr', 
							html : '<td style="border:1px solid black;width:30px;">Sıra</td>'+
							'<td style="border:1px solid black;width:110px;white-space:wrap;">Operasyon</td>'+
							(t.isAppPres?'<td style="border:1px solid black;width:110px;white-space:wrap;">Kalıp</td>':'')+
							'<td style="border:1px solid black;width:70px;white-space:normal;">Erp Ref No</td>'+
							'<td style="border:1px solid black;width:70px;white-space:normal;text-align:center;">Pl. Başlama</td>'+
							'<td style="border:1px solid black;width:70px;white-space:normal;text-align:center;">Pl. Bitiş</td>'+
							'<td style="border:1px solid black;width:50px;white-space:wrap;">Plan Miktar</td>'+
							'<td style="border:1px solid black;width:50px;white-space:wrap;">Saatlik Hedef</td>'+
							'<td style="border:1px solid black;width:70px;">İstasyon</td>'+
							'<td style="border:1px solid black;width:30px;">✓</td>'+
							'<td style="border:1px solid black;width:130px;">Açıklama</td>'
						}
					]
				}
				,{
					tag:'tbody', 
					cls:'mj-rpr-body',
					style : 'white-space:nowrap;',
					align : 'center',
					children : [
						{
							tag:'tr', 
							html : '<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>'+(t.isAppPres?'<td>&nbsp;</td>':'')
						}
					]
				},{
					tag:'tfoot', 
					cls:'mj-rpr-footer',
					style : 'mso-height-source:userset;',
					align : 'left',
					children : [
						{
							tag:'tr', 
							html : '<td colspan="'+(t.isAppPres?'11':'10')+'" style="white-space:normal;text-align:left;vertical-align:top;">&nbsp;</td>'
						}
					]
				}
			]
		}));
		t._body = $('.mj-rpr-body', t.el);
		t._footer = $('.mj-rpr-footer', t.el);
		t.titles = {
			vardiya : $('.mj-rpr-vardiya', t.el),
			vardiyaSaati : $('.mj-rpr-vardiya-saati', t.el)
		};
		mj.uretimProgramiView2.superclass.init.call(this);
	},
	copyContent : function(copyTo){
		mj.NE(copyTo, {tag:'table', style:'font-size:11px;font-family:Tahoma,Verdana,Arial;margin:5px;border-collapse:collapse;', align:'center', cellpadding:'2', cellspacing:'0', html : this.el[0].innerHTML});
	},
	_load : function(){
		var t = this, _b = t._body, d = t.store.data, p = d.tasks, tt = t.titles, n = mj.NE, lr = t._loadRenderer, _f = t._footer, notlar = d.notlar;

		t._body.empty();
		tt.vardiya.html(d.vardiyaAdi);
		tt.vardiyaSaati.html(d.tarihVardiya);
		for(var i=0,l=p.length;i<l;i++){
			var _t = p[i];
			n(_b, {
				tag:'tr', 
				html : '<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+(i+1)+'</td>'+
					'<td style="border:1px solid black;text-align:left;vertical-align:middle;white-space:wrap;">'+lr(_t['opname'])+'</td>'+
					(t.isAppPres?'<td style="border:1px solid black;text-align:left;vertical-align:middle;white-space:wrap;">'+lr(_t['opdescription'])+'</td>':'')+
					'<td style="border:1px solid black;text-align:left;vertical-align:middle;white-space:wrap;">'+lr(_t['erprefnumber'])+'</td>'+
					'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['ps'])+'</td>'+
					'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['pf'])+'</td>'+
					'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['vardiyaplanlanan'])+'</td>'+
					'<td style="border:1px solid black;text-align:center;vertical-align:middle;">'+lr(_t['hedefuretimsaat'])+'</td>'+
					'<td style="border:1px solid black;font-size:12px;font-weight:bold;white-space:wrap;" align="center" >'+_t.client+'</td>'+
					'<td style="border:1px solid black;text-align:center;vertical-align:middle;">&nbsp;</td>'+
					'<td style="border:1px solid black;text-align:center;vertical-align:middle;">&nbsp;</td>'
			});
		}
		var str='';
		for(var i=0,l=notlar.length;i<l;i++){
			var s = notlar[i];
			str += s.description+'<br/>';
		}
		$('td',_f).html(str);
	},
	_loadRenderer : function(val){
		return val ? val : '&nbsp;';
	},
	saveAsXLS : function(filename){
		var t = this, n=mj.NE;
		var copyTo = $(n(mj.bd, {style:'display:none'}));
		mj.NE(copyTo, {tag:'table', style:'font-size:11px;font-family:Tahoma,Verdana,Arial;white-space:nowrap;margin:5px;border-collapse:collapse;', align:'center', cellpadding:'2', cellspacing:'0', html : this.el[0].innerHTML});
		var content = (t.fileTitle ? t.fileTitle : '')+copyTo[0].innerHTML;
		content = mj.escape(content);
		var frmCnt = $(n(mj.bd, {style:'display:none'}));
		var frm = n(frmCnt, {tag:'form',action:mj.glb.exportPath,method:'POST',html:'<input type="hidden" name="landscape" value="1"/><input type="hidden" name="fit" value="1"/><input type="hidden" name="format" value="xls"/><input type="hidden" name="content" value="'+content+'"/><input type="hidden" name="filename" value="'+filename+'"/>'});
		frm.submit();
		frmCnt.remove();
		copyTo.remove();
	}
}
mj.extend(mj.uretimProgramiView2, mj.component);
mj.gridfis = function(config){
	mj.gridfis.superclass.constructor.call(this, config);
};
mj.gridfis.prototype={
	componentClass : 'mj.gridfis',
	defaultYear:false,
	recordCount:false,
	btnAdd:false,
	btnDel:false,
	firstColumnDataIndex:false,
	lastColumnDataIndex:false,
	decimalSeparator:',',
	groupingSeparator:'.',
	decimalPrecision:2,
	decimalPrecisionReal:2,
	recordCount:0,
	prefix:'',
	suffix:'',
	init:function(){
		var t = this;
		t.firstColumnDataIndex='';
		t.lastColumnDataIndex='';
		var x=new Date();
		t.defaultYear=x.getFullYear();
		t.render();
		t.on('cellclick', t.cellclick, t);
		t.on('keydown', t.keydown, t);
		t.on('keypress', t.keypress, t);
		t.on('change', t.change, t);
		t.on('focus', t.focus, t);
		t.on('blur', t.blur, t);
	},
	render:function(){
		var t=this;
		t.recordCount=0;
		t.data=[];
		t.headerCnt = mj.NE(t.renderTo,{style:'position:relative;overflow:hidden;width:'+t.bodyWidth+'px;'});
		t.tableHeader = $(mj.NE(t.headerCnt
			,{tag:'table', cellpadding:'0', cellspacing:'0', style:"background:white;table-layout:fixed;background:white;border-color:black;border-collapse:collapse;"}
		));
		var _headcols = "<td name='column-sira' style='border:none;font-weight:bold;text-align:center;width:30px;border:1px solid black;'>&nbsp;</td>";
		if(t.btnAdd){
			_headcols +="<td name='column-ekle' style='border:none;font-weight:bold;text-align:center;width:16px;border:1px solid black;'>&nbsp;</td>";
		}
		var colIndex = 0;
		var tableW = 0;
		$(t.cm).each(function(){
			if(this.header!=''&&!this.hide){
				_headcols += "<td name='column-"+(colIndex++)+"' style='border:none;font-weight:bold;text-align:center;width:"+this.width+"px;border:1px solid black;'>"+this.header+"</td>";
				tableW += this.width;
			}
		});
		if(t.btnDel){
			_headcols +="<td name='column-sil' style='border:none;font-weight:bold;text-align:center;width:16px;border:1px solid black;'>&nbsp;</td>";
		}
		t.tableHeader.append("<thead><tr style='background:#E1E6EE;Text-Align:Center;Border-Right-Style:none;Border-Left-Style:none;Border-Top-Style:none;Border-Bottom-Style:none;font-name:Tahoma;font-size:11px;font-weight:bold;'>"+_headcols+"</tr></thead>");
		t.bodyCnt=$(mj.NE(t.renderTo,{style:'position:relative;overflow:auto;width:'+t.bodyWidth+'px;height:'+t.bodyHeight+'px;'}));
		t.tablebody = $(mj.NE(t.bodyCnt
			,{tag:'table', cellpadding:'0', cellspacing:'0', style:"background:white;table-layout:fixed;background:white;border-color:black;border-collapse:collapse;"}
		));
		t.tableHeader.width(tableW);
		t.tablebody.width(tableW);
		t.bodyCnt.scroll(function(e){
			t.tableHeader.css("margin-left", -e.target.scrollLeft);
		});
	},
	loadData:function(ri){
		var t=this;
		clearTimeout(this.sto);
		for(var i=(ri?ri:0);i<t.data.length;i++){
			if(ri){ 
				$('.row-'+i,t.tablebody).remove();
				t.addRow(t.data[i],i);
			}else{
				t.addRow(t.data[i]);
			}
			t.satirHesapla(i);
			t.setMoneyColumns(i);
		}
		if(ri){
			$('.row-'+i,t.tablebody).remove();
			t.recordCount=t.recordCount-1;
		}
		if(t.data.length==0){
			t.recordCount=0;
			t.addRow();
			t.satirHesapla(0);
		}
	},
	addRow:function(datarow,ri){
		var t=this;
		var colIndex=0, rowIndex=(typeof ri!=='undefined'?parseInt(ri):t.recordCount);
		//if(rowIndex>0)
		//	if(parseInt(0+t.data[rowIndex-1].borc)==0&&parseInt(0+t.data[rowIndex-1].alacak)==0) return;
		var _bodycols = "<td class='column-"+(rowIndex+'-'+colIndex)+"' id='column-"+(rowIndex+'-'+colIndex)+"' style='border:none;font-weight:normal;text-align:right;width:30px;border:1px solid black;'>"+(rowIndex+1)+"</td>";
		if(t.btnAdd){
			_bodycols += "<td class='column-"+(rowIndex+'-'+colIndex)+" mj-add' id='column-"+(rowIndex+'-'+colIndex)+"' style='border:none;font-weight:normal;text-align:right;width:16px;border:1px solid black;cursor:pointer;'><img src='"+mj.glb.blankImage+"' style='width:16px;'/></td>";
		}
		
		var oeCls = rowIndex%2==0 ? 'mj-even' : 'mj-odd';
		var _tmp='',_tmpObj={};
		$(t.cm).each(function(){
			if(typeof datarow=='undefined'){
				_tmpObj[this.dataIndex]=''//;typeof this.type=='undefined'?'':(this.type=='money'||this.type=='number')?'':'';
			}
			if(typeof this.type!='undefined'){
				if(this.type=='money'&&typeof datarow!='undefined'){
					//console.log(this.dataIndex+':'+datarow[this.dataIndex]);
					datarow[this.dataIndex]=parseFloat("0"+datarow[this.dataIndex]);
				}
			}
			if(this.header==''||this.hide){
				_tmp+="<input type='hidden' id="+(this.dataIndex+'-'+rowIndex+'-'+colIndex)+" value='' />"
			}else{
				if(t.firstColumnDataIndex===''){ 
					t.firstColumnDataIndex=this.dataIndex;
				}
				t.lastColumnDataIndex=this.dataIndex;
				colIndex++;
				_bodycols += "<td class='column-"+(rowIndex+'-'+colIndex)+"' id='column-"+(rowIndex+'-'+colIndex)+"' "+
					"style='border:none;font-weight:normal;text-align:center;width:"+this.width+"px;border:1px solid black;"+(this.type=='search'?'position:relative;':'')+"'>"+_tmp
				if(typeof this.type!='undefined'&&typeof this.store!='undefined'&&this.type=='combo'){
					_bodycols +="<select name='"+(this.dataIndex+"-"+rowIndex+"-"+colIndex)+"' ";
					_bodycols +=" class='"+oeCls+" mj-type-combo "+(this.readonly?"readonly":'writable')+"' ";
					_bodycols +=" id="+(this.dataIndex+"-"+rowIndex+"-"+colIndex);
					_bodycols +=" style='border: medium none;width:"+this.width+"px;padding:0px;"+(typeof this.align!='undefined'?'text-align:'+this.align+';':'')+"' ";
					_bodycols +="><option value=''></option>";
						for(var i=0;i<this.store.length;i++){
							_bodycols +="<option "+(typeof datarow!='undefined'&&typeof datarow[this.dataIndex]!='undefined'&&datarow[this.dataIndex]==this.store[i]['text']?'selected':'')+" value='"+this.store[i].id+"'>"+this.store[i]['text']+"</option>";
						}
					_bodycols +="</select>";
				}else{
					if(typeof this.type!='undefined'&&this.type=='date'){
						this.maxlength=10;
					}
					_bodycols +="<input "+(this.readonly?"readonly=true":'')+" "+(typeof this.maxlength!='undefined'?"maxlength="+this.maxlength:'')+
						" class='"+oeCls+" mj-type-"+(typeof this.type!='undefined'?this.type:'text')+" "+(this.readonly?"readonly":'writable')+"' type='text'"+
						" id="+(this.dataIndex+"-"+rowIndex+"-"+colIndex)+
						" style='border: medium none;width:"+this.width+"px;padding:0px;"+(typeof this.align!='undefined'?'text-align:'+this.align+';':'')+
						//(this.readonly?"background: none repeat scroll 0 0 #E1E6EE;":"")+
						"' value='"+(typeof datarow!='undefined'&&typeof datarow[this.dataIndex]!='undefined'&&datarow[this.dataIndex]!=''?
							(typeof this.type!='undefined'?
								(this.type=='money'?t.formatValue(datarow[this.dataIndex]):datarow[this.dataIndex])
							:datarow[this.dataIndex])
							:'')+"' />"+
						(this.type=='search'?'<div id="filter-'+(rowIndex+'-'+colIndex+'-'+this.dataIndex)+'" style="display:none;cursor:pointer;width:16px;height:16px;position:absolute;right:0px;top:0px;" class="'+this.dataIndex+'-'+(rowIndex+'-'+colIndex)+' mj-filter"></div>':'');
				}
				
				_bodycols +="</td>";
				_tmp='';
			}
		});
		if(typeof datarow=='undefined') t.data.push(_tmpObj);
		colIndex++;	
		if(t.btnDel){
			_bodycols += "<td class='column-"+(rowIndex+'-'+colIndex)+" mj-delete' id='column-"+(rowIndex+'-'+colIndex)+"' style='border:none;font-weight:normal;text-align:right;width:16px;border:1px solid black;cursor:pointer;'><img src='"+mj.glb.blankImage+"' style='width:16px;'/></td>";
		}	
		t.tablebody.append("<tr class='row-"+rowIndex+" "+oeCls+"' style='Text-Align:Center;Border-Right-Style:none;Border-Left-Style:none;Border-Top-Style:none;Border-Bottom-Style:none;font-name:Tahoma;font-size:11;font-weight:bold;'>"+_bodycols+"</tr>");
		var row=$('.row-'+rowIndex,t.bodyCnt);
		var cells = row.find('td');
		$(cells).each(function(){
			if(!this.readOnly){
				var _x=$(this);
				var _arr=this.id.split("-");
				var _di=_arr[0];
				var _ri=_arr[1];
				var _ci=_arr[2];
				_x.click(function(e){
					e.stopPropagation();
					t.trigger('cellclick', t, _ri, _ci, _x, _di);
				});
			}
		});
		var filters = row.find('div.mj-filter');
		$(filters).each(function(){
			if(!this.readOnly){
				var _x=$(this);
				var _arr=this.id.split("-");
				var _di=_arr[0];
				var _ri=_arr[1];
				var _ci=_arr[2];
				var _di=_arr[3];
				_x.click(function(e){
					e.stopPropagation();
					t.trigger('cellclick', t, _ri, _ci, _x, _di);
				});
			}
		});
		//var inputs = $('.row-'+rowIndex,t.bodyCnt).find('input:visible');
		var elements = $($($('.row-'+rowIndex,t.bodyCnt).find(':visible')).not('td')).not('option');
		$(elements).each(function(){
			if(!this.readOnly){
				var _arr=this.id.split("-");
				var _di=_arr[0];
				var _ri=_arr[1];
				var _ci=_arr[2];
				var _x=$(this);
				_x.focus(function(e){
					t.trigger('focus', t, e, _ri, _ci, _x, _di);
				});
				_x.blur(function(e){
					t.trigger('blur', t, e, _ri, _ci, _x, _di);
				});
				_x.keydown(function(e){
					t.trigger('keydown', t, e, _ri, _ci, _x, _di);
				});
				_x.keypress(function(e){
					t.trigger('keypress', t, e, _ri, _ci, _x, _di);
				});
				_x.keyup(function(e){
					t.trigger('change', t, e, _ri, _ci, _x, _di);
				});
				_x.change(function(e){
					t.trigger('change', t, e, _ri, _ci, _x, _di);
				});
			}
		});
		if(typeof datarow=='undefined'){
			//if(rowIndex>0) 
			//	if(t.data[rowIndex-1].muhhesplanid>0) t.satirHesapla(rowIndex-1);
			$('#'+t.firstColumnDataIndex+'-'+rowIndex+'-1').focus();
		}
		if(typeof ri=='undefined'){
			t.recordCount++;
		}
	},
	clear:function(ri){
		if(typeof ri==='undefined'){
			this.recordCount=0;
			this.data=[];
			this.tablebody[0].innerHTML='';
		}else{
			this.data.splice(ri,1);
			$('.row-'+ri,this.tablebody).remove();
		}
		this.loadData(ri);
	},
	cellclick:function(g, rowIndex, colIndex, cell, dataIndex){
		//console.log('cellclick:'+rowIndex+'-'+colIndex+'-'+dataIndex);
		var cis=(cell[0].id).split('-');
		var filters=g.bodyCnt.find('.mj-filter:visible');
		$(filters).each(function(){
			var tis=($(this)[0].id).split('-');
			if(!(tis.length>=3&&cis.length>=3&&tis[1]==cis[1]&&tis[2]==cis[2])){
				$(this).hide();
			}	
		});
		var row=g.data[rowIndex];
		if(cell.hasClass('mj-filter')){
			$(cell).hide();
			// this.satirHesapla(rowIndex);
			g.trigger('recordsfilter', g, rowIndex, colIndex, $(cell[0].previousSibling), dataIndex);
		}
		if(cell.hasClass('mj-add')){
			var rc=g.recordCount;
			g.addRow();
			if(g.recordCount==rc)
				g.focus(g,null,rowIndex,1,$('.column-'+rowIndex+'-'+1,g.bodyCnt).find('input:visible'),dataIndex);
		}
		if(cell.hasClass('mj-delete')){
			g.trigger('clickrowdelete', g, rowIndex, row);
			if(g.data.length==rowIndex&&rowIndex>0)
				g.satirHesapla(rowIndex-1);
		}
	},
	keydown:function(g, e, rowIndex, colIndex, cell, dataIndex){
		var k = e.keyCode || e.charCode;
		if(k>=96&&k<=105)k=k-48;
		var filtercolumn=cell.next().hasClass('mj-filter');
		var combocolumn=cell.hasClass('mj-type-combo');
		if(filtercolumn){
			if(k==113){//F2
				e.preventDefault();
				e.stopPropagation();
				g.trigger('recordsfilter', g, rowIndex, colIndex, cell, dataIndex);
				return false;
			}else if(k==mj.keys.ENTER){
				e.preventDefault();
				e.stopPropagation();
				g.trigger('recordsfilter', g, rowIndex, colIndex, cell, dataIndex);
				return false;
			}
		}
		if(dataIndex==g.lastColumnDataIndex&&g.btnAdd&&(k==mj.keys.RIGHT||k==mj.keys.DOWN||(k==mj.keys.TAB&&!e.shiftKey))){
			e.preventDefault();
			e.stopPropagation();
			var rc=g.recordCount;
			//console.log('g.recordCount:'+g.recordCount+' -rc:'+rc+' -rowIndex:'+rowIndex);
			if(rowIndex==rc-1)g.addRow();
			//console.log('g.recordCount:'+g.recordCount+' -rc:'+rc+' -rowIndex:'+rowIndex);
			if(g.recordCount==rc)
				g.focus(g,e,rowIndex,colIndex,$('.column-'+rowIndex+'-'+colIndex,g.bodyCnt).find('input:visible'),dataIndex);
			if(rowIndex<g.recordCount-1){
				var ri=parseInt(rowIndex)+1;
				var ci=(dataIndex==g.lastColumnDataIndex?1:colIndex);
				if(k==mj.keys.DOWN){
					g.focus(g,e,ri,ci,$('.column-'+ri+'-'+ci,g.bodyCnt).find('input:visible'),dataIndex);
				}
				if(k==mj.keys.RIGHT||k==mj.keys.TAB)
					g.focus(g,e,ri,1,$('.column-'+ri+'-'+1,g.bodyCnt).find('input:visible'),dataIndex);
			}
			return false;
		}
		if(k==mj.keys.TAB){
			e.preventDefault();
			e.stopPropagation();
			if(e.shiftKey&&colIndex==1){
				if(rowIndex>0&&e.shiftKey){
					var el=$('.row-'+(parseInt(rowIndex)-1),g.bodyCnt).find('.writable').last();
					g.focus(g,e,rowIndex,colIndex,el,dataIndex);
				}
			}else{
				var ci=parseInt(colIndex)+(e.shiftKey?-1:1);
				//while($('#column-'+rowIndex+'-'+ci,g.bodyCnt).find('input:visible').is('[readonly]')){
				while($($($($('#column-'+rowIndex+'-'+ci,g.bodyCnt).find(':visible')).not('td')).not('option')).is('[readonly]')){
					ci+=(e.shiftKey?-1:1);
				}
				ci=ci<1&&e.shiftKey?1:ci;
				//g.focus(g,e,rowIndex,ci,$('.column-'+rowIndex+'-'+ci,g.bodyCnt).find('input:visible'),dataIndex);
				g.focus(g,e,rowIndex,ci,$($($('.column-'+rowIndex+'-'+ci,g.bodyCnt).find(':visible')).not('td')).not('option'),dataIndex);
			}
			return false;
		}
		if(k==mj.keys.LEFT&&!filtercolumn){
			e.preventDefault();
			e.stopPropagation();
			if(colIndex>1){
				var ci=parseInt(colIndex)-1;
				while($('#column-'+rowIndex+'-'+ci,g.bodyCnt).find('input:visible').is('[readonly]')){
					ci--;
				}
				ci=ci<1?1:ci;
				g.focus(g,e,rowIndex,ci,$('.column-'+rowIndex+'-'+ci,g.bodyCnt).find('input:visible'),dataIndex);
			}else{
				if(rowIndex>0){
					var el=$('.row-'+(parseInt(rowIndex)-1),g.bodyCnt).find('.writable').last();
					g.focus(g,e,rowIndex,colIndex,el,dataIndex);
				}
			}
			return false;
		}
		if(k==mj.keys.UP){
			e.preventDefault();
			e.stopPropagation();
			if(rowIndex>=1){
				var ri=parseInt(rowIndex)-1;
				g.focus(g,e,ri,colIndex,$('.column-'+ri+'-'+colIndex,g.bodyCnt).find('input:visible'),dataIndex);
			}
			return false;
		}
		if(k==mj.keys.RIGHT&&!filtercolumn){
			e.preventDefault();
			e.stopPropagation();
			var ci=parseInt(colIndex)+1;
			while($('#column-'+rowIndex+'-'+ci,g.bodyCnt).find('input:visible').is('[readonly]')){
				ci++;
			}
			g.focus(g,e,rowIndex,ci,$('.column-'+rowIndex+'-'+ci,g.bodyCnt).find('input:visible'),dataIndex);
			return false;
		}
		if(k==mj.keys.DOWN&&g.btnAdd&&!combocolumn){
			e.preventDefault();
			e.stopPropagation();
			var rc=g.recordCount;
			if(rowIndex==rc-1){
				g.addRow();
			}else{
				if(g.recordCount==rc){
					g.focus(g,e,rowIndex,ci,$('.column-'+rowIndex+'-'+colIndex,g.bodyCnt).find('input:visible'),dataIndex);
				}
				if(rowIndex<g.recordCount-1){
					var ri=parseInt(rowIndex)+1;
					g.focus(g,e,ri,colIndex,$('.column-'+ri+'-'+colIndex,g.bodyCnt).find('input:visible'),dataIndex);
				}
			}
			return false;
		}
	},
	keypress:function(g, e, rowIndex, colIndex, cell, dataIndex){
		//console.log(rowIndex+'-'+colIndex);
		//console.dir(cell);
		var _type=cell.hasClass('mj-type-number')?'number':cell.hasClass('mj-type-money')?'money':'';
		var k = e.keyCode || e.charCode;
		if(k>=96&&k<=105)k=k-48;
		//console.log(rowIndex+'-'+colIndex+'-'+k+'-'+g.recordCount);
		if(cell.hasClass('mj-type-number')||cell.hasClass('mj-type-money')||cell.hasClass('mj-type-date')){		
      if(!$.browser.msie && (mj.keys.isSpecial(e) || k == mj.keys.BACKSPACE || k == mj.keys.DELETE)){
				//console.log(k);
				return;
      }
			var allowed = "0123456789";
			allowed+=_type=='money'?',':'';
			allowed+=_type=='date'?'/':'';
			if(allowed.indexOf(String.fromCharCode(k)) === -1){
				e.preventDefault();
				e.stopPropagation();
        return false;
      }
		}
	},
	change:function(g, e, rowIndex, colIndex, cell, dataIndex){
		var val=$(cell).hasClass('mj-type-money')?(cell[0].value!=''?cell[0].value.indexOf(",")!=-1?parseFloat("0"+cell[0].value.replace(".","").replace(",",".")):cell[0].value:''):cell[0].value;
		//console.log('change'+dataIndex+'type:'+typeof cell[0].value+'float:'+parseFloat("0"+cell[0].value.replace(".","").replace(",","."))+'val:'+val);
		if($(cell).hasClass('mj-type-combo')){
			var _idx=g.parent.kur.getIndex('id',val);
			if(_idx>-1){
				g.bulcombo(rowIndex,g.parent.kur[_idx],dataIndex);
			}
		}else if($(cell).hasClass('mj-type-date')){
			var k = e.keyCode || e.charCode;
			//console.log("change:"+val+" e.keyCode:"+e.keyCode+" e.key:"+e.key+" e.charCode:"+e.charCode+" e.char:"+e.char+" e.which:"+e.which+" k:"+k);
			if(k>=96&&k<=105){
				k=k-48;
			}else if(k>105){
				var subStr = val.substr(val.length - 1);
				k=subStr.charCodeAt(0);
			}
			var allowed = "0123456789";
			if(allowed.indexOf(String.fromCharCode(k)) == -1)return false;
			if(val.length == 2||val.length == 5){
				if(val.substr(val.length-1,1)=='/'){
					var arr=val.split('/');
					for(var i=0;i<arr.length-1;i++){
						if(arr[i].length==1)
							arr[i]="0"+arr[i];
					}
					cell[0].value=arr.join('/');
				}else{
					if(val.length == 5&&g.defaultYear)
						cell[0].value= val+'/'+g.defaultYear;
					else
						cell[0].value= val+'/';
				}
			}
			val = cell[0].value;
			if(val.length==8&&val.substr(6,2)<20){
				var arr=val.split('/');
				arr[2]="20"+arr[2];
				cell[0].value=arr.join('/');
			}
			val = cell[0].value;
			if(val.length==10){
				var re= /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
				if(val.match(re)){
					cell[0].value=val;
					var arr=val.split('/');
					//g.data[rowIndex][dataIndex]=new Date(arr[2],arr[1]-1,arr[0]).getTime()/1000;
					g.data[rowIndex][dataIndex]=new Date(arr[2],arr[1]-1,arr[0]).formatDate('Y-m-d');
				}else{
					cell[0].value='';
				}
			}else 
				return;
		}else{
			g.data[rowIndex][dataIndex]=val;
		}
		if(dataIndex!=g.lastColumnDataIndex){
			g.satirHesapla(rowIndex,dataIndex);
		}
	},
	focus:function(g, e, rowIndex, colIndex, cell, dataIndex){
		//console.log('focus:'+rowIndex+'-'+colIndex+'-'+dataIndex);
		if($(cell).hasClass('readonly')) return;
		var filters=g.bodyCnt.find('.mj-filter:visible');
		$(filters).each(function(){
			$(this).hide();
		});
		var input=$('input.focused',g.bodyCnt);
		if($(input).hasClass('mj-type-search')){
			$($(input).next()).hide();
		}
		input.removeClass('focused').css('background','transparent');
		if($(cell).hasClass('mj-type-combo')){
			$(cell).addClass('focused').css('background','#C1D6FE');
			document.getElementById($(cell)[0].id).focus();
		}else{
			$(cell).select().addClass('focused').css('background','#C1D6FE');
		}
		if($(cell).hasClass('mj-type-search')){
			$($(cell).next()).show();
		}
	},
	blur:function(g, e, rowIndex, colIndex, cell, dataIndex){
		//console.log('blur:'+rowIndex+'-'+colIndex+'-'+dataIndex);
		try{
			//console.log(e.target);
			//console.log(e.originalEvent.explicitOriginalTarget);
			if(e.target.id!=e.originalEvent.explicitOriginalTarget.id){
				if($(e.originalEvent.explicitOriginalTarget).hasClass(e.target.id)){
					e.preventDefault();
					e.stopPropagation();
					return;
				}
			}
		}
		catch(err){
			//do nothing if okfunc fails ?
		}
		
		var input=$('input.focused',g.bodyCnt);
		if($(input).hasClass('mj-type-search')){
			//$($(input).next()).hide();
		}else if($(cell).hasClass('mj-type-date')){
			var val = cell[0].value;
			if(val.length<10){
				g.data[rowIndex][dataIndex]='';
				cell[0].value='';
			}
		}
		input.removeClass('focused').css('background','transparent');
		var input=$('select.focused',g.bodyCnt);
		input.removeClass('focused').css('background','transparent');
		g.satirHesapla(rowIndex);
	},
	bulcombo : function(ri,data,dataIndex){
		dataIndex=dataIndex.replace('_lookup_','');
		this.data[ri].parabirimiId=parseInt(data.id);
		/*if(dataIndex=='parabirimiId'){
			this.data[ri].kur=parseFloat("0"+data.kur);
		}*/
		this.satirHesapla(ri);
	},
	setMoneyColumns:function(ri){
		var row=this.data[ri];
		var columns=$('.mj-type-money',$('.row-'+ri,this.bodyCnt).find(':visible'));
		$(columns).each(function(){
			this.readOnly=false;
			$(this).addClass('writable').removeClass('readonly').removeAttr('readonly','true');
			if(!this.readOnly){
				this.readOnly=true;
				$(this).addClass('readonly').removeClass('writable').attr('readonly','true');
			}
		});
	},
	satirHesapla : function(ri,di){
		//console.log('satirHesapla:'+ri);
		var row=this.data[ri];
		// console.log('satirHesapla');
		// console.log(row);
		// console.log(ri);
		// console.log(pos);
		this.setRowValues(ri,row,di);
	},
	setRowValues:function(idx,data,dataIndex){
		if(!data) return;
		var t=this;
		var items = $('.row-'+idx,t.bodyCnt);
		var inputs=items.find('input:visible');
		//console.log('setRowValues');
		//console.log(data);
		$(inputs).each(function(){
			var _di=this.id.split("-")[0];
			if(_di==dataIndex) return;
			var idx=t.cm.getIndex('dataIndex',_di);	
			//var _v=(typeof t.cm[idx].type!='undefined'&&t.cm[idx].type=='money'&&data[_di]!='')?t.formatValue(data[_di]):data[_di];
			if(typeof t.cm[idx].type!='undefined'&&data[_di]!=''){
				if(t.cm[idx].type=='money') _v=t.formatValue(data[_di]);
				else if(t.cm[idx].type=='date') _v=new Date(data[_di]).formatDate('d/m/Y');//_v=new Date(data[_di]*1000).formatDate('d/m/Y');
				else _v=data[_di];
			}else
				_v=data[_di];
			/*if(t.cm[idx].type=='money'){
				console.log('di:'+_di+' _v:'+_v+' idx:'+idx+' data[di]:'+data[_di]);
				console.dir(data);
			}*/
			$(this).val(_v);
		});
		var selects=items.find('select:visible');
		$(selects).each(function(){
			var _di=this.id.split("-")[0].replace('_lookup_','');
			if(_di==dataIndex) return;
			var idx=t.cm.getIndex('dataIndex',_di);
			var _v=data[_di];
			$(this).val(_v);
		});
	},
	formatValue : function(value){
		return this.prefix + mj.format.float2Money(value,this.decimalPrecision,this.decimalSeparator,this.groupingSeparator) + this.suffix;
	},
	setNextColumnFocus:function(rowIndex,colIndex,dataIndex){
		var ci=parseInt(colIndex)+1;
		while($($($($('#column-'+rowIndex+'-'+ci,this.bodyCnt).find(':visible')).not('td')).not('option')).is('[readonly]')){
			ci+=1;
		}
		ci=ci<1?1:ci;
		this.focus(this,0,rowIndex,ci,$($($('.column-'+rowIndex+'-'+ci,this.bodyCnt).find(':visible')).not('td')).not('option'),dataIndex);
	},
	getValue:function(){
		return this.data;
	}
};
mj.extend(mj.gridfis, mj.component);
mj.version='v0.8-b3476';