import { b as bootstrapLazy } from './index-5e330570.js';
import { p as patchBrowser, g as globalScripts } from './app-globals-00076de1.js';

patchBrowser().then(options => {
  globalScripts();
  return bootstrapLazy([["kai-scheduler",[[0,"kai-scheduler",{"locale":[1025],"cancel":[64],"clearHistory":[64],"dragEventBox":[64],"dump":[64],"dumpEvent":[64],"getStage":[64],"getState":[64],"load":[64],"redo":[64],"removeSelectedFromSchedule":[64],"save":[64],"undo":[64],"zoomIn":[64],"zoomOut":[64],"forceDraw":[64],"testScroll":[64]}]]]], options);
});
