/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */
import { HTMLStencilElement, JSXBase } from "./stencil-public-runtime";
import { SUPPORTED_LOCALES } from "./components/kai-scheduler/shared/i18n/translate-service";
import { IEvent } from "./components/kai-scheduler/event/event.interface";
import { EventBox } from "./components/kai-scheduler/event-container/event-box";
import { DataState } from "./components/kai-scheduler/event/data.state";
import { ISchedulerState } from "./components/kai-scheduler/kai-scheduler.store";
import { IKaiSchedulerConfig } from "./components/kai-scheduler/kai-scheduler-config";
import { Action } from "redux";
import { ISaveData } from "./components/kai-scheduler/kai-scheduler";
export namespace Components {
    interface KaiScheduler {
        "cancel": () => Promise<any>;
        "clearHistory": () => Promise<any>;
        "dragEventBox": (eventId: string, movementPx: number) => Promise<DataState.actionTypes.DragEventBox>;
        "dump": (resourceId?: string) => Promise<any>;
        "dumpEvent": (eventId?: string) => Promise<any>;
        "forceDraw": () => Promise<void>;
        "getStage": () => Promise<any>;
        "getState": () => Promise<ISchedulerState>;
        "load": (config: IKaiSchedulerConfig) => Promise<void>;
        "locale": SUPPORTED_LOCALES;
        "redo": () => Promise<Action<any>>;
        "removeSelectedFromSchedule": () => Promise<Action<any>>;
        "save": () => Promise<ISaveData>;
        "testScroll": (pos: any) => Promise<any>;
        "undo": () => Promise<Action<any>>;
        "zoomIn": () => Promise<number>;
        "zoomOut": () => Promise<number>;
    }
}
declare global {
    interface HTMLKaiSchedulerElement extends Components.KaiScheduler, HTMLStencilElement {
    }
    var HTMLKaiSchedulerElement: {
        prototype: HTMLKaiSchedulerElement;
        new (): HTMLKaiSchedulerElement;
    };
    interface HTMLElementTagNameMap {
        "kai-scheduler": HTMLKaiSchedulerElement;
    }
}
declare namespace LocalJSX {
    interface KaiScheduler {
        "locale"?: SUPPORTED_LOCALES;
        "onBtnSaveClick"?: (event: CustomEvent<any>) => void;
        "onEventBoxDblClick"?: (event: CustomEvent<{ event: IEvent, eventBox: EventBox }>) => void;
    }
    interface IntrinsicElements {
        "kai-scheduler": KaiScheduler;
    }
}
export { LocalJSX as JSX };
declare module "@stencil/core" {
    export namespace JSX {
        interface IntrinsicElements {
            "kai-scheduler": LocalJSX.KaiScheduler & JSXBase.HTMLAttributes<HTMLKaiSchedulerElement>;
        }
    }
}
