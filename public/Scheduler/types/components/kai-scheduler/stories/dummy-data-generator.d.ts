import { SchedulerRelationTarget } from '../shared/enums/scheduler-relation-target';
export declare const dummyDataGenerator: () => {
    locale: string;
    events: any[];
    resources: ({
        id: number;
        title: string;
        children: {
            id: number;
            title: string;
        }[];
        dontAllowSchedule?: undefined;
    } | {
        id: number;
        title: string;
        dontAllowSchedule: boolean;
        children: {
            id: number;
            title: string;
        }[];
    } | {
        id: number;
        title: string;
        children?: undefined;
        dontAllowSchedule?: undefined;
    })[];
    plannedStops: {
        50: {
            id: number;
            dateStart: number;
            dateEnd: number;
            description: string;
            color: string;
        }[];
        51: {
            id: number;
            dateStart: number;
            dateEnd: number;
            description: string;
            color: string;
        }[];
        52: {
            id: number;
            dateStart: number;
            dateEnd: number;
            description: string;
            color: string;
        }[];
    };
    translations: {
        en: {
            "resource-list": {
                "toggler-title": string;
            };
            "event-list": {
                "toggler-title": string;
            };
        };
        tr: {
            "resource-list": {
                "toggler-title": string;
            };
            "event-list": {
                "toggler-title": string;
            };
        };
    };
    predecessors: (event: any, events: any) => {
        event: any;
        relationTarget: SchedulerRelationTarget;
        timeDiffMs: any;
    }[];
    successors: (event: any, events: any) => {
        event: any;
        relationTarget: SchedulerRelationTarget;
        timeDiffMs: any;
    }[];
    zoomLevel: number;
    eventListColumns: ({
        key: string;
        title: string;
        width: number;
        cellRenderer?: undefined;
    } | {
        key: string;
        title: string;
        width: number;
        cellRenderer: (cellData: any, _rowData: any, _column: any, canvasGroup: any, font: any, horizontalPadding: any, rowHeight: any) => any;
    })[];
};
