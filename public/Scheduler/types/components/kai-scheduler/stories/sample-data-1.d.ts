export declare const data: {
    Client: {
        totalProperty: number;
        records: {
            id: string;
            title: string;
            children: any[];
        }[];
    };
    Mould: {
        totalProperty: number;
        records: {
            mould: string;
            mouldgroup: string;
            opcode: string;
            opnumber: number;
            opname: string;
            leafmask: string;
            leafmaskcurrent: string;
            productionmultiplier: number;
            intervalmultiplier: string;
            setup: number;
            cycletime: string;
            client1: string;
            client2: string;
            client3: string;
            client4: any;
            client5: any;
        }[];
    };
    Scheduler: {
        totalProperty: number;
        records: {
            id: number;
            ptid: number;
            opcode: string;
            title: string;
            title2: string;
            duration: number;
            _production: number;
            tpp: number;
            erprefnumber: string;
            start: string;
            scheduled: boolean;
            resourceId: string;
            importid: number;
            listid: number;
        }[];
    };
    plannedStops: {
        totalProperty: number;
        records: ({
            id: number;
            clients: string;
            losttype: string;
            name: string;
            repeattype: string;
            days: string;
            startday: any;
            starttime: string;
            finishday: any;
            finishtime: string;
            record_id: string;
            create_user_id: number;
            created_at: string;
            update_user_id: number;
            updated_at: string;
            delete_user_id: any;
            deleted_at: any;
            version: number;
            start: string;
            finish: any;
            clearonovertime: boolean;
        } | {
            id: number;
            clients: string;
            losttype: string;
            name: string;
            repeattype: string;
            days: any;
            startday: string;
            starttime: string;
            finishday: string;
            finishtime: string;
            record_id: string;
            create_user_id: number;
            created_at: string;
            update_user_id: number;
            updated_at: string;
            delete_user_id: any;
            deleted_at: any;
            version: number;
            start: string;
            finish: string;
            clearonovertime: boolean;
        })[];
    };
    overtimes: {
        totalProperty: number;
        records: any[];
    };
};
