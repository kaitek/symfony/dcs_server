import Konva from 'konva';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { ScrollerState } from './scroller.state';
import { ISchedulerState } from '../kai-scheduler.store';
export declare type KaiCanvasScrollerOrientation = 'vertical' | 'horizontal';
export interface IKaiSchedulerScrollerConfig extends ISchedulerComponentConfig, ScrollerState.State {
}
export declare class ScrollPosChangedEvent extends SchedulerComponentEvent {
    pos: number;
    constructor(pos: number);
}
export declare class KaiSchedulerScrollerEvents extends SchedulerComponentEvents {
    posChanged: ScrollPosChangedEvent;
}
export declare class KaiSchedulerScroller extends SchedulerComponent<KaiSchedulerScrollerEvents> implements IKaiSchedulerScrollerConfig {
    baseColor: string;
    baseColorHover: string;
    barColor: string;
    barColorHover: string;
    barGrowFactor: number;
    barSize: number;
    canvas: {
        barGroup?: Konva.Group;
        base?: Konva.Rect;
        baseGroup?: Konva.Group;
        bar?: Konva.Rect;
    };
    canvasBarHeight: number;
    canvasBarWidth: number;
    private get canvasBarScrollAmount();
    canvasBaseHeight: number;
    canvasBaseWidth: number;
    private _pos;
    get pos(): number;
    set pos(value: number);
    scrollDuration: number;
    size: number;
    stateKey: string;
    private _margin;
    get margin(): number;
    set margin(value: number);
    private _max;
    orientation: KaiCanvasScrollerOrientation;
    private _scrollSize;
    get scrollSize(): number;
    set scrollSize(value: number);
    visible: boolean;
    visibleSize: number;
    private _x;
    get x(): number;
    set x(value: number);
    private _y;
    get y(): number;
    set y(value: number);
    constructor(config: IKaiSchedulerScrollerConfig);
    private _animateToPos;
    private emitPosChanged;
    initStore(preloadedState: ISchedulerState): void;
    prepareCanvas(): void;
    private _setPosInner;
    setPos(pos: number): void;
    setState(state: ScrollerState.State): void;
}
