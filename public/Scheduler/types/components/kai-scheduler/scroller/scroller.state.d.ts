import { IStateKey, KeyAction } from '../key-action';
import { KaiCanvasScrollerOrientation } from './scroller';
export declare namespace ScrollerState {
    interface State extends IStateKey {
        baseColor?: string;
        baseColorHover?: string;
        barColor?: string;
        barColorHover?: string;
        barSize?: number;
        barGrowFactor?: number;
        margin?: number;
        orientation?: KaiCanvasScrollerOrientation;
        pos?: number;
        scrollDuration?: number;
        scrollSize?: number;
        size?: number;
        visibleSize?: number;
        x?: number;
        y?: number;
    }
    interface States {
        [stateKey: string]: State;
    }
    const getInitialState: (override?: State) => State;
    namespace actionTypes {
        enum names {
            SET_STATE = "[SCROLLER] set state",
            SET_POS = "[SCROLLER] set pos"
        }
        class SetStateAction implements KeyAction {
            stateKey: string;
            state: State;
            readonly type = names.SET_STATE;
            constructor(stateKey: string, state: State);
        }
        class SetPosAction implements KeyAction {
            stateKey: string;
            pos: number;
            readonly type = names.SET_POS;
            constructor(stateKey: string, pos: number);
        }
        const contains: (action: any) => action is actions;
    }
    type actions = actionTypes.SetStateAction | actionTypes.SetPosAction;
    const itemReducer: (state: State, action: actions) => State;
    const reducer: (states: States, action: actions) => States;
    const setPos: (stateKey: string, pos: number) => actionTypes.SetPosAction;
    const setState: (stateKey: string, state: State) => actionTypes.SetStateAction;
}
