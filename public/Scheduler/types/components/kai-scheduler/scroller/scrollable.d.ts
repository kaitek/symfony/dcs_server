export interface IScrollable {
    stateKeyScrollerHorizontal?: string;
    stateKeyScrollerVertical?: string;
    scrolled?: () => void;
}
