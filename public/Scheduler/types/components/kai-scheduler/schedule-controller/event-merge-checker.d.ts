import { IEvent } from '../event/event.interface';
import { IMergeCandidate } from './event-merge-candidate';
export interface IMergedEvents {
    start: number;
    end: number;
    duration: number;
    durationWithPlannedStops: number;
    preProcessDuration: number;
    preProcessDurationWithPlannedStops: number;
    postProcessDuration: number;
    postProcessDurationWithPlannedStops: number;
    events: string[];
    merge: IMergeCandidate;
}
export declare type EventMergeChecker = (event: IEvent, events: IEvent[]) => IMergeCandidate[];
export declare type EventMergedDurationCalculator = (event: IEvent, duration: number, events: IEvent[], groupName: string, merge: IMergeCandidate) => number;
export declare type EventMergedRemainingDurationCalculator = (event: IEvent, recentMerges: IMergedEvents[]) => number;
