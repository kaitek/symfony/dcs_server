import { SchedulerRelationTarget } from '../shared/enums/scheduler-relation-target';
export interface IScheduleItem {
    eventId: string;
    relationTarget?: SchedulerRelationTarget;
    timeDiffMs?: number;
    _userManagedDiffMs?: number;
}
