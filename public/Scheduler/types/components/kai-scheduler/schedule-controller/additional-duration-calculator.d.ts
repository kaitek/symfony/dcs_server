import { IEvent } from '../event/event.interface';
import { IMergedEvents } from './event-merge-checker';
export declare type EventAdditionalDurationCalculator = (event: IEvent, merges: IMergedEvents[]) => number;
export declare type MergeAdditionalDurationCalculator = (event: IEvent, merges: IMergedEvents[], merge: IMergedEvents, mergeIndex: number) => number;
