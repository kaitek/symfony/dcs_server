import { IEvent } from '../event/event.interface';
import { EventMergedDurationCalculator, EventMergedRemainingDurationCalculator } from './event-merge-checker';
import { MergeAdditionalDurationCalculator } from './additional-duration-calculator';
interface IMergeCandidateDetails {
    [key: string]: IEvent[];
}
export interface IMergeCandidate {
    groupName: string;
    detail: IMergeCandidateDetails;
    durationCalculator: EventMergedDurationCalculator;
    remainingDurationCalculator: EventMergedRemainingDurationCalculator;
    preProcessDurationCalculator: MergeAdditionalDurationCalculator;
    postProcessDurationCalculator: MergeAdditionalDurationCalculator;
    targetResources: string;
    allowPartialMerge: boolean;
}
export {};
