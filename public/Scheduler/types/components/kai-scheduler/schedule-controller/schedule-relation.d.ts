import { IMergeCandidate } from './event-merge-candidate';
import { IScheduleItem } from './schedule-item';
export interface IScheduleRelation {
    eventId: string;
    prev: IScheduleItem[];
    next: IScheduleItem[];
    mergeCandidates?: IMergeCandidate[];
}
