import { IEvent } from '../event/event.interface';
import { IScheduleItem } from './schedule-item';
export declare type EventRelationChecker = (event: IEvent, events: IEvent[]) => IScheduleItem[];
