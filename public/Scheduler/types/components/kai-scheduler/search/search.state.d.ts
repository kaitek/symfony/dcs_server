import { Action } from 'redux';
import { IFontParameters } from '../shared/interfaces/font-parameters';
export declare namespace SearchState {
    interface State {
        backgroundColor?: string;
        backgroundColorHover?: string;
        color?: string;
        colorHover?: string;
        expandedWidth?: number;
        font?: IFontParameters;
        iconOpacity?: number;
        margin?: number;
        padding?: number;
        query?: string;
    }
    const getInitialState: (override?: State) => State;
    namespace actionTypes {
        const SEARCH_SAMPLE_ACTION = "[SEARCH] sample action";
        class SearchSampleAction implements Action {
            readonly type = "[SEARCH] sample action";
        }
    }
    type actions = actionTypes.SearchSampleAction;
    const reducer: (state: State, _action: actions) => State;
}
