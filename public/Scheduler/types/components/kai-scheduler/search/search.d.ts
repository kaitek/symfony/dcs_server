import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { SearchState } from './search.state';
export declare class SearchQueryChangeEvent extends SchedulerComponentEvent {
    newVal: string;
    constructor(newVal: string);
}
export declare class SearchInputEvents extends SchedulerComponentEvents {
    change: SearchQueryChangeEvent;
}
export declare class SearchInput extends SchedulerComponent<SearchInputEvents> implements SearchState.State, ISchedulerComponentConfig {
    backgroundColor: string;
    backgroundColorHover: string;
    private get backgroundShape();
    canvas: {
        background?: Konva.Shape;
        collapser?: Konva.Path;
        icon?: Konva.Path;
    };
    color: string;
    colorHover: string;
    private _expanded;
    expandedWidth: number;
    fontSize: number;
    input: HTMLInputElement;
    margin: number;
    padding: number;
    private _query;
    get query(): string;
    set query(newQuery: string);
    private _size;
    get size(): number;
    set size(value: number);
    private _width;
    get width(): number;
    set width(value: number);
    private _x;
    get x(): number;
    set x(value: number);
    private _xLeft;
    get xLeft(): number;
    set xLeft(value: number);
    constructor(config: ISchedulerComponentConfig);
    initStore(preloadedState: ISchedulerState): void;
    emitQueryChange(newVal: string): void;
    hide(): void;
    hideInput(): void;
    prepareCanvas(): void;
    show(): void;
    toggle(): void;
}
