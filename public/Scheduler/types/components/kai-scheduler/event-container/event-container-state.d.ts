import { Action } from 'redux';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { SchedulerDragMode } from '../shared/enums/drag-mode';
import { IFontParameters } from '../shared/interfaces/font-parameters';
import { IEventBoxBorders } from './event-box-borders';
import { IModeSwitchParameters } from './mode-switch/mode-switch-parameters';
import { TextToggleButtonGroupState } from './mode-switch/text-toggle-button-group.state';
import { EventRenderer } from './event-renderers';
export declare namespace EventContainerState {
    interface State {
        backgroundColors?: string[];
        backgroundDragJumpAvailableColor?: string;
        backgroundDragJumpUnavailableColor?: string;
        backgroundHoverColor?: string;
        backgroundSelectedColor?: string;
        boxBackgroundRenderer?: EventRenderer;
        boxBorder?: IEventBoxBorders;
        boxRenderer?: EventRenderer;
        boxTitleRenderer?: EventRenderer;
        cornerRadius?: number;
        eventHeight?: number;
        eventMargin?: number;
        failedMergeColor?: string;
        modeSwitchParams?: IModeSwitchParameters;
        postProcessColor?: string;
        preProcessColor?: string;
        renderer?: EventRenderer;
        titleParams?: IFontParameters;
    }
    const STATE_KEY_DRAG_MODE = "dragMode";
    const STATE_KEY_DRAG_SUB_MODE = "dragSubMode";
    const STATE_KEY_CONTROLLER_BUTTONS = "controller";
    const STATE_KEY_BTN_UNDO = "undo";
    const STATE_KEY_BTN_REDO = "redo";
    const STATE_KEY_BTN_UNSCHEDULE = "unschedule";
    const STATE_KEY_BTN_CANCEL = "cancel";
    const STATE_KEY_BTN_SAVE = "save";
    const getInitialState: (override: State) => {
        eventContainer: State;
        dragModeSwitch: TextToggleButtonGroupState.State;
        dragSubModeSwitch: TextToggleButtonGroupState.State;
    };
    namespace actionTypes {
        const SET_DRAG_MODE = "[EVENT_CONTAINER] set drag mode";
        const SET_PRESERVE_FREE_TIME_MODE = "[EVENT_CONTAINER] set preserve free time";
        class SetDragModeAction implements Action {
            dragMode: number;
            readonly type = "[EVENT_CONTAINER] set drag mode";
            constructor(dragMode: number);
        }
        class SetPreserveFreeTimeModeAction implements Action {
            dragSubMode: number;
            readonly type = "[EVENT_CONTAINER] set preserve free time";
            constructor(dragSubMode: number);
        }
    }
    type actions = actionTypes.SetDragModeAction | actionTypes.SetPreserveFreeTimeModeAction;
    const reducer: (state: State, _action: actions) => State;
    const afterAction: (state: ISchedulerState, _action: SchedulerAction) => ISchedulerState;
    const setDragMode: (dragMode: SchedulerDragMode) => actionTypes.SetDragModeAction;
    const setPreserveFreeTimeMode: (dragSubMode: number) => actionTypes.SetPreserveFreeTimeModeAction;
}
