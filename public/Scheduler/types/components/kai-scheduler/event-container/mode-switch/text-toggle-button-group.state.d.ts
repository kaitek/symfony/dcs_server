import { IStateKey, KeyAction } from '../../key-action';
import { ISchedulerState, SchedulerAction } from '../../kai-scheduler.store';
import { IBorderParameters } from '../../shared/interfaces/border-parameters';
import { IFontParameters } from '../../shared/interfaces/font-parameters';
import { IButtonGroupOption } from './button-group-option';
export declare namespace TextToggleButtonGroupState {
    let _translateService: any;
    interface State extends IStateKey {
        backgroundColor?: string;
        backgroundColorHover?: string;
        backgroundColorSelected?: string;
        border?: IBorderParameters;
        borderColorHover?: string;
        font?: IFontParameters;
        fontColorHover?: string;
        fontColorSelected?: string;
        options?: IButtonGroupOption[];
        padding?: number;
        value?: number;
        visible?: boolean;
        width?: number;
        height?: number;
        x?: number;
        y?: number;
    }
    interface States {
        [stateKey: string]: State;
    }
    const getInitialState: (override?: State) => State;
    namespace actionTypes {
        enum names {
            SET_STATE = "[TEXT_TOGGLE_BUTTON_GROUP] set state",
            SET_VALUE = "[TEXT_TOGGLE_BUTTON_GROUP] set value"
        }
        class SetStateAction implements KeyAction {
            stateKey: string;
            state: State;
            readonly type = names.SET_STATE;
            constructor(stateKey: string, state: State);
        }
        class SetValueAction implements KeyAction {
            stateKey: string;
            value: number;
            readonly type = names.SET_VALUE;
            constructor(stateKey: string, value: number);
        }
        const contains: (action: any) => action is actions;
    }
    type actions = actionTypes.SetStateAction | actionTypes.SetValueAction;
    const itemReducer: (state: State, action: actions) => State;
    const reducer: (states: States, action: actions) => States;
    const afterAction: (state: ISchedulerState, _action: SchedulerAction) => ISchedulerState;
    const setState: (stateKey: string, state: State) => actionTypes.SetStateAction;
    const setValue: (stateKey: string, value: number) => actionTypes.SetValueAction;
}
