export interface IModeSwitchTitles {
    jump?: string;
    jumpTooltip?: string;
    multiple?: string;
    multipleTooltip?: string;
    single?: string;
    singleTooltip?: string;
    preserveFreeTime?: string;
    preserveFreeTimeTooltip?: string;
    dontPreserveFreeTime?: string;
    dontPreserveFreeTimeTooltip?: string;
}
