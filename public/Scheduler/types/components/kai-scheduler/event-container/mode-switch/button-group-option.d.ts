export interface IButtonGroupOption {
    title: string;
    tooltip: string;
}
