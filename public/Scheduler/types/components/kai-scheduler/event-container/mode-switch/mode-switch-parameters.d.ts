import { IModeSwitchTitles } from './mode-switch-titles';
export interface IModeSwitchParameters {
    stateKeyDragMode: string;
    stateKeyDragSubMode: string;
    titles: IModeSwitchTitles;
    margin: number;
}
