import Konva from 'konva';
import { SchedulerComponentEvents, ISchedulerComponentConfig, SchedulerComponent } from '../../shared/scheduler-component';
import { ISchedulerState } from '../../kai-scheduler.store';
import { TextToggleButtonGroupState } from './text-toggle-button-group.state';
import { IButtonGroupOption } from './button-group-option';
import { IBorderParameters } from '../../shared/interfaces/border-parameters';
import { IFontParameters } from '../../shared/interfaces/font-parameters';
export interface ITextToggleButtonGroupConfig extends ISchedulerComponentConfig, TextToggleButtonGroupState.State {
}
export declare class TextToggleButtonGroupEvents extends SchedulerComponentEvents {
}
export declare class TextToggleButtonGroup extends SchedulerComponent<TextToggleButtonGroupEvents> implements ITextToggleButtonGroupConfig {
    backgroundColor: string;
    backgroundColorHover: string;
    backgroundColorSelected: string;
    border: IBorderParameters;
    borderColor: string;
    borderColorHover: string;
    canvas: {
        buttonBackgrounds?: Konva.Rect[];
        buttonGroups?: Konva.Group[];
        buttonTitles?: Konva.Text[];
        tooltip?: {
            label: Konva.Label;
            tag: Konva.Tag;
            text: Konva.Text;
        };
    };
    font: IFontParameters;
    fontColor: string;
    fontColorHover: string;
    fontColorSelected: string;
    options: IButtonGroupOption[];
    padding: number;
    stateKey: string;
    value: number;
    visible: boolean;
    width: number;
    height: number;
    x: number;
    y: number;
    constructor(config: ITextToggleButtonGroupConfig);
    initStore(preloadedState: ISchedulerState): void;
    prepareCanvas(): void;
    setValue(value: number): void;
}
