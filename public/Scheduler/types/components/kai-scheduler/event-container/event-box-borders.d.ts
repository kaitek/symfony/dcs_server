import { IBorderParameters } from '../shared/interfaces/border-parameters';
export interface IEventBoxBorders {
    default: IBorderParameters;
    dragging: IBorderParameters;
    pinned: IBorderParameters;
    shifting: IBorderParameters;
    selected: IBorderParameters;
}
