import Konva from 'konva';
import { IEvent } from '../event/event.interface';
import { EventBox } from './event-box';
export interface Utils {
    Konva: any;
    createSelector: any;
}
export declare type EventRenderer = (event: IEvent, box: EventBox, addTo: Konva.Container, utils: Utils) => Konva.Shape;
export declare const DEFAULT_EVENT_TITLE_RENDERER: EventRenderer;
export declare const DEFAULT_EVENT_BACKGROUND_RENDERER: EventRenderer;
export declare const DEFAULT_EVENT_BOX_RENDERER: EventRenderer;
