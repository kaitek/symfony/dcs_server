import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { IFontParameters } from '../shared/interfaces/font-parameters';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { IEventBoxBorders } from './event-box-borders';
import { EventContainerState } from './event-container-state';
import { IModeSwitchParameters } from './mode-switch/mode-switch-parameters';
import { TextToggleButtonGroup } from './mode-switch/text-toggle-button-group';
import { EventBox, EventBoxClickEvent, EventBoxDblClickEvent } from './event-box';
import { EventRenderer } from './event-renderers';
import { IconButtonGroup, IconButtonGroupClickEvent } from '../icon-button/icon-button-group';
export interface IModeSwitchTitles {
    single: string;
    singleTooltip: string;
    jump: string;
    jumpTooltip: string;
    multiple: string;
    multipleTooltip: string;
    preserveFreeTime: string;
    preserveFreeTimeTooltip: string;
    dontPreserveFreeTime: string;
    dontPreserveFreeTimeTooltip: string;
}
export interface IEventContainerConfig extends ISchedulerComponentConfig, EventContainerState.State {
    addModeSwitchTo?: Konva.Container;
}
export declare class EventContainerEvents extends SchedulerComponentEvents {
    boxclick: EventBoxClickEvent;
    boxdblclick: EventBoxDblClickEvent;
}
export declare class EventContainer extends SchedulerComponent<EventContainerEvents> implements IEventContainerConfig {
    addModeSwitchTo: Konva.Container;
    private _backgroundColorIndex;
    backgroundColors: string[];
    backgroundDragJumpAvailableColor: string;
    backgroundDragJumpUnavailableColor: string;
    backgroundHoverColor: string;
    backgroundSelectedColor: string;
    boxBackgroundRenderer: EventRenderer;
    boxRenderer: EventRenderer;
    boxTitleRenderer: EventRenderer;
    boxBorder: IEventBoxBorders;
    boxes: {
        [eventId: string]: EventBox;
    };
    controllerButtons: IconButtonGroup;
    controllerGroup: Konva.Group;
    cornerRadius: number;
    private _currentState;
    dragModeSwitch: TextToggleButtonGroup;
    dragSubModeSwitch: TextToggleButtonGroup;
    eventHeight: number;
    eventMargin: number;
    failedMergeColor: string;
    modeSwitchParams: IModeSwitchParameters;
    _scheduledEvents: string[];
    titleParams: IFontParameters;
    constructor(config: IEventContainerConfig);
    private emitBoxClick;
    private emitBoxDblClick;
    getBackgroundColor(): string;
    getBoxOfEvent(eventId: string): EventBox;
    initStore(preloadedState: ISchedulerState): void;
    onControllerButtonClick(e: IconButtonGroupClickEvent): void;
    prepareCanvas(): void;
}
