export declare enum EventBoxDragDisplayMode {
    NONE = "NONE",
    DRAGGING = "DRAGGING",
    SHIFTING = "SHIFTING"
}
