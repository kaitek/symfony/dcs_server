import Konva from 'konva';
import { KonvaEventObject } from 'konva/types/Node';
import { IEvent } from '../event/event.interface';
import { ISchedulerState } from '../kai-scheduler.store';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { EventRenderer } from './event-renderers';
import { CanvasComponentMouseEvent } from '../shared/canvas-component';
import { IScheduleRelation } from '../schedule-controller/schedule-relation';
import { DragDirection } from '../shared/enums/drag-direction';
export interface IEventBoxConfig extends ISchedulerComponentConfig, IEvent {
    stateKey: string;
    backgroundColor: string;
}
export declare class EventBoxClickEvent extends SchedulerComponentEvent {
    event: IEvent;
    box: EventBox;
    constructor(event: IEvent, box: EventBox);
}
export declare class EventBoxDblClickEvent extends SchedulerComponentEvent {
    event: IEvent;
    box: EventBox;
    constructor(event: IEvent, box: EventBox);
}
export declare class EventBoxRefreshEvent extends SchedulerComponentEvent {
    event: IEvent;
    values: IEventBoxDrawParams;
    constructor(event: IEvent, values: IEventBoxDrawParams);
}
export declare class EventBoxEvents extends SchedulerComponentEvents {
    click: EventBoxClickEvent;
    dblclick: EventBoxDblClickEvent;
    refresh: EventBoxRefreshEvent;
}
export interface IEventBoxDrawParams {
    backgroundColor: string;
    backgroundHoverColor: string;
    backgroundRenderer: EventRenderer;
    borderColor: string;
    borderSize: number;
    cornerRadius: number;
    height: number;
    msPerPx: number;
    renderer: EventRenderer;
    titleClipWidth: number;
    titleFontColor: string;
    titleFontFamily: string;
    titleFontSize: number;
    titleFontStyle: string;
    titleRenderer: EventRenderer;
    width: number;
}
export declare class EventBox extends SchedulerComponent<EventBoxEvents> implements IEventBoxConfig, IEventBoxDrawParams {
    backgroundColor: string;
    backgroundHoverColor: string;
    backgroundRenderer: EventRenderer;
    borderColor: string;
    borderSize: number;
    canvas: {
        node?: Konva.Shape;
    };
    cornerRadius: number;
    private _dragDirection;
    get dragDirection(): DragDirection;
    set dragDirection(val: DragDirection);
    _dragInvolvedEvents: IScheduleRelation[];
    _dragLastPosX: number;
    event: IEvent;
    failedMergeColor: any;
    height: number;
    hoverTween: Konva.Tween;
    msPerPx: number;
    postProcessColor: any;
    preProcessColor: any;
    renderer: EventRenderer;
    stateKey: string;
    titleClipWidth: number;
    titleFontColor: string;
    titleFontFamily: string;
    titleFontSize: number;
    titleFontStyle: string;
    titleRenderer: EventRenderer;
    width: number;
    x: number;
    y: number;
    constructor(config: IEventBoxConfig);
    destroy(): void;
    private dragBoundFunc;
    private emitClick;
    private emitDblClick;
    emitRefresh(values?: IEventBoxDrawParams): void;
    initStore(preloadedState: ISchedulerState): void;
    protected onDragEnd(e: any): void;
    protected onDragMove(e: any): void;
    protected onDragStart(e: KonvaEventObject<DragEvent>): void;
    protected onMouseEnter(event: CanvasComponentMouseEvent): void;
    protected onMouseLeave(_e: any): void;
    prepareCanvas(): void;
}
