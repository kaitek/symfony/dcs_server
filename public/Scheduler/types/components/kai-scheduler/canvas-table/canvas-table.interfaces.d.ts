import Konva from 'konva';
import { CanvasTableState } from './canvas-table.state';
import { ISchedulerComponentConfig } from '../shared/scheduler-component';
import { IFontParameters } from '../shared/interfaces/font-parameters';
import { CanvasTable } from './canvas-table';
import { SUPPORTED_LOCALES } from '../shared/i18n/translate-service';
import { Utils } from '../event-container/event-renderers';
export interface ICanvasRowData {
    [key: string]: any;
}
export declare type CanvasTableCellRenderer = (cellData: any, rowData: ICanvasRowData, column: ICanvasTableColumn, locale: SUPPORTED_LOCALES, canvasGroup?: Konva.Group, font?: IFontParameters, horizontalPadding?: number, rowHeight?: number, utils?: Utils) => string;
export declare type CanvasTableHeaderRenderer = (column: ICanvasTableColumn, canvasGroup: Konva.Group, table: CanvasTable, utils?: Utils) => void;
export interface ICanvasTableColumn {
    key: string;
    title?: string;
    width?: number;
    cellRenderer?: CanvasTableCellRenderer;
    headerRenderer?: CanvasTableHeaderRenderer;
}
export interface ICanvasTableConfig extends ISchedulerComponentConfig, CanvasTableState.State {
}
export declare class CanvasTableTextFilter {
    text: string;
}
export declare class CanvasTablePropertyFilter {
    query: {
        [key: string]: any;
    };
}
export declare class CanvasTableTextAndPropertyFilter {
    text: string;
    query: {
        [key: string]: any;
    };
}
export declare type CanvasTableFilter = CanvasTableTextFilter | CanvasTablePropertyFilter | CanvasTableTextAndPropertyFilter;
export declare const CANVAS_TABLE_DEFAULT_CELL_RENDERER: CanvasTableCellRenderer;
