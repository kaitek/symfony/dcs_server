import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { KaiSchedulerScroller } from '../scroller/scroller';
import { CanvasComponentEvent } from '../shared/canvas-component';
import { IFontParameters } from '../shared/interfaces/font-parameters';
import { SchedulerComponent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { CanvasTableCellRenderer, CanvasTableFilter, CanvasTableHeaderRenderer, ICanvasTableColumn, ICanvasTableConfig } from './canvas-table.interfaces';
import { CanvasTableState } from './canvas-table.state';
import { SUPPORTED_LOCALES } from '../shared/i18n/translate-service';
import { KonvaEventObject } from 'konva/types/Node';
export interface IMinMax {
    min: number;
    max: number;
}
export declare class CanvasTableLongPressEvent extends CanvasComponentEvent {
    rowIndex: number;
    rowData: any;
    constructor(rowIndex: number, rowData: any);
}
export declare class CanvasTableRowClickEvent extends CanvasComponentEvent {
    rowIndex: number;
    rowData: any;
    id: any;
    constructor(rowIndex: number, rowData: any, id: any);
}
export declare class CanvasTableScrollWheelEvent extends CanvasComponentEvent {
    sourceEvent: KonvaEventObject<WheelEvent>;
    constructor(sourceEvent: KonvaEventObject<WheelEvent>);
}
export declare class CanvasTableEvents extends SchedulerComponentEvents {
    longpress: CanvasTableLongPressEvent;
    rowClick: CanvasTableRowClickEvent;
    wheel: CanvasTableScrollWheelEvent;
}
export declare class CanvasTable extends SchedulerComponent<CanvasTableEvents> implements ICanvasTableConfig {
    addTo: Konva.Container;
    canvas: {
        headerColumnGroups?: {
            [colIndex: number]: Konva.Group;
        };
        rowBackgroundGroup?: Konva.Group;
        rowBackgrounds?: {
            [key: string]: Konva.Rect;
        };
        rowColumnGroups?: {
            contentColumnBaseGroups: {
                [colIndex: number]: Konva.Group;
            };
            rows: {
                [key: string]: {
                    [colIndex: number]: Konva.Group;
                };
            };
        };
        contentGroup?: Konva.Group;
        header: {
            background?: Konva.Rect;
            bottomBorder?: Konva.Line;
            contentGroup?: Konva.Group;
        };
        hoverTweens?: {
            [key: string]: Konva.Tween;
        };
    };
    columns: ICanvasTableColumn[];
    data: {
        [key: string]: any;
    };
    defaultCellRenderer: CanvasTableCellRenderer;
    defaultHeaderRenderer: CanvasTableHeaderRenderer;
    filteredDataIndexes: any[];
    private _font;
    get font(): IFontParameters;
    set font(value: IFontParameters);
    private _headerFont;
    get headerFont(): IFontParameters;
    set headerFont(value: IFontParameters);
    headerHeight: number;
    height: number;
    horizontalPadding: number;
    locale: SUPPORTED_LOCALES;
    multiSelect: boolean;
    rowColors: string[];
    rowColorHover: string;
    rowColorSelected: string;
    rowHeight: number;
    scrolled: () => void;
    scrollerHorizontal: KaiSchedulerScroller;
    scrollerVertical: KaiSchedulerScroller;
    private _scrollerVerticalPos;
    get scrollerVerticalPos(): number;
    set scrollerVerticalPos(newPos: number);
    selectable: boolean;
    selectedRowIds: any[];
    get selectedRows(): any[];
    stateKey: string;
    visibleIndexes: IMinMax;
    private _visibleRowIndexesCache;
    width: number;
    constructor(config?: ICanvasTableConfig);
    deSelectRow(id: string): void;
    initStore(preloadedState: ISchedulerState): void;
    isRowSelected(id: any): boolean;
    prepareCanvas(): void;
    prepareColumns(): void;
    destroy(): void;
    private _destroyRowElements;
    prepareRows(): void;
    private refreshRowVisibility;
    private scrollToVerticalPos;
    selectRow(id: string): void;
    setFilter(filter: CanvasTableFilter): void;
    setRowVerticalPosition(rowId: number, newPosition: number): void;
    setState(state: CanvasTableState.State): void;
}
