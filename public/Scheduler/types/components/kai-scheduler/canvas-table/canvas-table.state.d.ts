import { IStateKey, KeyAction } from '../key-action';
import { IScrollable } from '../scroller/scrollable';
import { ScrollerState } from '../scroller/scroller.state';
import { CanvasTableCellRenderer, CanvasTableFilter, ICanvasTableColumn } from './canvas-table.interfaces';
import { IBorderParameters } from '../shared/interfaces/border-parameters';
import { IFontParameters } from '../shared/interfaces/font-parameters';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { IMinMax } from './canvas-table';
import { SUPPORTED_LOCALES } from '../shared/i18n/translate-service';
export declare namespace CanvasTableState {
    interface State extends IStateKey, IScrollable {
        border?: IBorderParameters;
        columns?: ICanvasTableColumn[];
        data?: {
            [key: string]: any;
        };
        defaultCellRenderer?: CanvasTableCellRenderer;
        filter?: CanvasTableFilter;
        filteredDataIndexes?: any[];
        font?: IFontParameters;
        headerBackgroundColor?: string;
        headerBackgroundColorStops?: Array<string | number>;
        headerFont?: IFontParameters;
        headerHeight?: number;
        height?: number;
        horizontalPadding?: number;
        locale?: SUPPORTED_LOCALES;
        multiSelect?: boolean;
        rowColors?: string[];
        rowColorHover?: string;
        rowColorSelected?: string;
        rowCount?: number;
        rowHeight?: number;
        scrolled?: () => void;
        selectable?: boolean;
        selectedRowIds?: any[];
        stateKeyScrollerHorizontal?: string;
        stateKeyScrollerVertical?: string;
        visibleIndexes?: IMinMax;
        width?: number;
        x?: number;
    }
    interface States {
        [stateKey: string]: State;
    }
    const INITIAL_TABLE_STATE: State;
    const getInitialState: (override: State) => {
        table: State;
        scrollerHorizontal: ScrollerState.State;
        scrollerVertical: ScrollerState.State;
    };
    namespace actionTypes {
        enum names {
            SET_STATE = "[TABLE] set state",
            SET_FILTER = "[TABLE] set filter",
            REFRESH_FILTER = "[TABLE] refresh filter",
            ADD_NEW = "[TABLE] add new table",
            DESTROY = "[TABLE] destroy table",
            SELECT_ROW = "[TABLE] select row",
            DESELECT_ROW = "[TABLE] deselect row"
        }
        class SetStateAction implements KeyAction {
            stateKey: string;
            state: State;
            readonly type = names.SET_STATE;
            constructor(stateKey: string, state: State);
        }
        class SetFilterAction implements KeyAction {
            stateKey: string;
            filter: CanvasTableFilter;
            readonly type = names.SET_FILTER;
            constructor(stateKey: string, filter: CanvasTableFilter);
        }
        class RefreshFilterAction implements KeyAction {
            stateKey: string;
            readonly type = names.REFRESH_FILTER;
            constructor(stateKey: string);
        }
        class AddNewTableAction implements KeyAction {
            stateKey: string;
            override: State;
            readonly type = names.ADD_NEW;
            constructor(stateKey: string, override?: State);
        }
        class DestroyTableAction implements KeyAction {
            stateKey: string;
            readonly type = names.DESTROY;
            constructor(stateKey: string);
        }
        class DeselectRowAction implements KeyAction {
            stateKey: string;
            id: any;
            readonly type = names.DESELECT_ROW;
            constructor(stateKey: string, id?: any);
        }
        class SelectRowAction implements KeyAction {
            stateKey: string;
            id: any;
            readonly type = names.SELECT_ROW;
            constructor(stateKey: string, id?: any);
        }
        const contains: (action: any) => action is actions;
    }
    type actions = actionTypes.SetStateAction | actionTypes.SetFilterAction | actionTypes.RefreshFilterAction | actionTypes.AddNewTableAction | actionTypes.DestroyTableAction | actionTypes.SelectRowAction | actionTypes.DeselectRowAction;
    const itemReducer: (state: State, action: actions) => State;
    const reducer: (states: States, action: actions) => States;
    const beforeAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const afterAction: (state: ISchedulerState, _action: SchedulerAction) => ISchedulerState;
    const refreshFilter: (stateKey: string) => actionTypes.RefreshFilterAction;
    const setFilter: (stateKey: string, filter: CanvasTableFilter) => actionTypes.SetFilterAction;
    const setState: (stateKey: string, state: State) => actionTypes.SetStateAction;
}
