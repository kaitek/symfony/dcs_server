import Konva from 'konva';
import { Vector2d } from 'konva/types/types';
import { ISchedulerState } from '../kai-scheduler.store';
import { IMergedStop } from '../shared/interfaces/merged-stop';
import { IPlannedStop } from '../shared/interfaces/planned-stop';
import { IKaiSchedulerTreeItemCanvas, IKaiSchedulerTreeItemConfig, KaiSchedulerTreeItem } from '../tree/tree-item';
export interface IKaiSchedulerResourceCanvas extends IKaiSchedulerTreeItemCanvas {
    groupStops?: Konva.Group;
}
export declare class KaiSchedulerResource extends KaiSchedulerTreeItem {
    canvas: IKaiSchedulerResourceCanvas;
    children: KaiSchedulerResource[];
    private eventHeight;
    private eventMargin;
    get height(): number;
    get id(): any;
    mergedStops: IMergedStop[];
    private msPerPx;
    rowCount: number;
    stops: IPlannedStop[];
    private visibleMinDateMs;
    private visibleMaxDateMs;
    constructor(config: IKaiSchedulerTreeItemConfig);
    destroy(): void;
    initStore(preloadedState: ISchedulerState): void;
    onTitleClick(_pos: Vector2d): void;
    prepareChildren(): void;
    refreshHeightAndVerticalPos(): void;
    renderStops(): void;
    toggle(): void;
}
