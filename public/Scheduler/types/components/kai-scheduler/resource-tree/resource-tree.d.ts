import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { IKaiSchedulerTreeConfig, Tree, TreeEvents, TreeItemCanvasPreparedEvent } from '../tree/tree';
import { KaiSchedulerResource } from './resource';
export interface IKaiSchedulerResourceTreeConfig extends IKaiSchedulerTreeConfig {
    addObjectsTo: Konva.Container;
    eventHeight?: number;
    eventMargin?: number;
}
export declare class ResourceTreeEvents extends TreeEvents {
}
export declare class ResourceTree<T extends ResourceTreeEvents> extends Tree<T> {
    addObjectsTo: Konva.Container;
    eventHeight: number;
    eventMargin: number;
    protected itemCache: {
        [resourceId: number]: KaiSchedulerResource;
    };
    private lastShowedTooltipText;
    msPerPx: number;
    stopsPopulated: boolean;
    visibleMinDateMs: number;
    visibleMaxDateMs: number;
    constructor(config: IKaiSchedulerResourceTreeConfig);
    private _getPlannedStopAtMousePos;
    initStore(preloadedState: ISchedulerState): void;
    itemCanvasPrepared(ev: TreeItemCanvasPreparedEvent): void;
    protected _renderItems(): void;
}
