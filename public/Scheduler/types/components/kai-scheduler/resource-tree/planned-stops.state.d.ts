import { IPlannedStop, PlannedStopFactory } from '../shared/interfaces/planned-stop';
import { IMergedStop } from '../shared/interfaces/merged-stop';
import { IKaiSchedulerConfig } from '../kai-scheduler-config';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
export declare namespace PlannedStopsState {
    export interface CalculatedStops {
        [resourceId: string]: {
            stops: IPlannedStop[];
            mergedStops: IMergedStop[];
        };
    }
    interface ResourceParentsMap {
        id: string;
        parents: string[];
    }
    export interface States {
        calculated: CalculatedStops;
        calculatedRange: {
            min: number;
            max: number;
        };
        plannedStopsFactory: PlannedStopFactory;
        _resourceParentsMap: ResourceParentsMap[];
    }
    export const getInitialState: (config: IKaiSchedulerConfig, pivotDateMs: number, pivotDateInitialVisibleMinDateDiffMs: number) => PlannedStopsState.States;
    export namespace actionTypes {
        enum names {
            SET_STATE = "[PLANNED_STOPS] set state"
        }
        class SetStateAction {
            state: States;
            readonly type = names.SET_STATE;
            constructor(state: States);
        }
        const contains: (action: any) => action is SetStateAction;
    }
    export type actions = actionTypes.SetStateAction;
    export const reducer: (state: States, action: actions) => States;
    export const beforeAction: (state: ISchedulerState, _action: SchedulerAction) => ISchedulerState;
    export const setState: (state: States) => actionTypes.SetStateAction;
    export {};
}
