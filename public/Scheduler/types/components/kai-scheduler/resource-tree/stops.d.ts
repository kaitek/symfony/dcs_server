import { SchedulerComponentEvents, SchedulerComponent, ISchedulerComponentConfig } from '../shared/scheduler-component';
export declare class StopsContainerEvents extends SchedulerComponentEvents {
}
export declare class StopsContainer extends SchedulerComponent<StopsContainerEvents> implements ISchedulerComponentConfig {
    constructor(config: ISchedulerComponentConfig);
}
