import { Action } from 'redux';
import { IFontParameters } from './shared/interfaces/font-parameters';
import { IVisibleRange } from './shared/interfaces/visible-range';
import { TimelineZoomParameters } from './timeline/zoom-parameters';
import { ISchedulerState, SchedulerAction } from './kai-scheduler.store';
import { EventRelationChecker } from './schedule-controller/event-relation';
import { EventMergeChecker } from './schedule-controller/event-merge-checker';
import { IKaiSchedulerConfig } from './kai-scheduler-config';
import { SUPPORTED_LOCALES } from './shared/i18n/translate-service';
import { EventAdditionalDurationCalculator } from './schedule-controller/additional-duration-calculator';
export declare namespace MainState {
    interface State {
        eventListWidth?: number;
        font?: IFontParameters;
        height?: number;
        locale?: SUPPORTED_LOCALES;
        mergeCandidates?: EventMergeChecker;
        msPerPx?: number;
        pivotDate?: Date;
        pivotDateInitialVisibleMinDateDiffMs?: number;
        pivotDateMs?: number;
        postProcessDurationCalculator?: EventAdditionalDurationCalculator;
        predecessors?: EventRelationChecker;
        preProcessDurationCalculator?: EventAdditionalDurationCalculator;
        readonly?: boolean;
        resources?: any;
        resourceTreeWidth?: number;
        stateKeyResourceTree?: string;
        successors?: EventRelationChecker;
        viewDate?: Date;
        visibleRange?: IVisibleRange;
        width?: number;
        zoomChart?: TimelineZoomParameters[];
        zoomLevel?: number;
        zoomStep?: number;
    }
    const getInitialState: (width: number, height: number, viewDate: Date, config: IKaiSchedulerConfig) => State;
    namespace actionTypes {
        enum names {
            INIT = "[SCHEDULER] init preloaded state",
            SET_DIMENSIONS = "[SCHEDULER] set dimensions",
            SET_MS_PER_PX = "[SCHEDULER] set msPerPx",
            SET_STATE = "[SCHEDULER] set state",
            SET_VIEW_DATE = "[SCHEDULER] set view date",
            SET_ZOOM_LEVEL = "[SCHEDULER] set zoomLevel",
            SYNC_EVENT_LIST_WIDTH = "[SCHEDULER] sync event list width",
            UNDO = "[SCHEDULER] undo",
            REDO = "[SCHEDULER] redo"
        }
        class INIT implements Action {
            state: State;
            readonly type = names.INIT;
            constructor(state: State);
        }
        class SetDimensions implements Action {
            update: {
                width?: number;
                height?: number;
            };
            readonly type = names.SET_DIMENSIONS;
            constructor(update: {
                width?: number;
                height?: number;
            });
        }
        class SetMsPerPx implements Action {
            msPerPx: number;
            zoomLevel: number;
            readonly type = names.SET_MS_PER_PX;
            constructor(msPerPx: number, zoomLevel: number);
        }
        class SetState implements Action {
            state: State;
            readonly type = names.SET_STATE;
            constructor(state: State);
        }
        class SetViewDate implements Action {
            viewDate: Date;
            readonly type = names.SET_VIEW_DATE;
            constructor(viewDate: Date);
        }
        class SetZoomLevel implements Action {
            zoomLevel: number;
            readonly type = names.SET_ZOOM_LEVEL;
            constructor(zoomLevel: number);
        }
        class SyncEventListWidth implements Action {
            newWidth: number;
            readonly type = names.SYNC_EVENT_LIST_WIDTH;
            constructor(newWidth: number);
        }
        const contains: (action: any) => action is actions;
    }
    type actions = actionTypes.INIT | actionTypes.SetDimensions | actionTypes.SetMsPerPx | actionTypes.SetState | actionTypes.SetViewDate | actionTypes.SetZoomLevel | actionTypes.SyncEventListWidth;
    const setState: (state: State) => actionTypes.SetState;
    const setViewDate: (viewDate: Date) => actionTypes.SetViewDate;
    const syncEventListWidth: (newWidth: number) => actionTypes.SyncEventListWidth;
    const reducer: (state: State, action: MainState.actions) => State;
    const beforeAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const afterAction: (state: ISchedulerState, _action: SchedulerAction) => ISchedulerState;
    const setZoomLevel: (zoomLevel: number) => actionTypes.SetZoomLevel;
}
