import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { ScrollerState } from '../scroller/scroller.state';
import { TreeState } from '../tree/tree.state';
export declare namespace TargetPickerState {
    interface State {
        backgroundColor?: string;
        btnBackgroundColor?: string;
        btnBackgroundColorHover?: string;
        btnColor?: string;
        btnColorHover?: string;
        btnFontSize?: number;
        btnIconOpacity?: number;
        btnMargin?: number;
        btnPadding?: number;
        stateKeyTree?: string;
        x?: number;
        y?: number;
    }
    const TARGET_PICKER_STATE_KEY = "targetPicker";
    const TARGET_TREE_STATE_KEY: string;
    const TARGET_PICKER_TABLE_PREFIX: string;
    const getInitialState: (height: number) => {
        targetPicker: State;
        tree: TreeState.State;
        scrollerHorizontal: ScrollerState.State;
        scrollerVertical: ScrollerState.State;
    };
    namespace actionTypes {
        enum names {
            SET_STATE = "[TARGET_PICKER] set state"
        }
        class SetStateAction {
            state: State;
            readonly type = names.SET_STATE;
            constructor(state: State);
        }
        const contains: (action: any) => action is SetStateAction;
    }
    type actions = actionTypes.SetStateAction;
    const reducer: (state: State, action: actions) => State;
    const afterAction: (state: ISchedulerState, _action: SchedulerAction) => ISchedulerState;
    const setState: (state: State) => actionTypes.SetStateAction;
}
