import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvents, SchedulerComponentEvent } from '../shared/scheduler-component';
import { TargetPickerState } from './target-picker.state';
import { Tree, TreeEvents, TreeItemCanvasPreparedEvent, TreeItemClickEvent } from '../tree/tree';
import { IEvent } from '../event/event.interface';
import { IKaiSchedulerTreeItem } from '../tree/tree-item.interface';
import { IMergeCandidate } from '../schedule-controller/event-merge-candidate';
export interface ITargetPickerConfig extends ISchedulerComponentConfig, TargetPickerState.State {
}
export declare class TargetPickerCancelEvent extends SchedulerComponentEvent {
}
export declare class TargetPickerSelectEvent extends SchedulerComponentEvent {
    selectedResource: IKaiSchedulerTreeItem;
    mergeCandidate: IMergeCandidate;
    selectedEvents: IEvent[];
    constructor(selectedResource: IKaiSchedulerTreeItem, mergeCandidate: IMergeCandidate, selectedEvents: IEvent[]);
}
export declare class TargetPickerEvents extends SchedulerComponentEvents {
    cancel: TargetPickerCancelEvent;
    select: TargetPickerSelectEvent;
}
export declare class TargetPicker extends SchedulerComponent<TargetPickerEvents> implements ITargetPickerConfig {
    private activeEvent;
    backgroundColor: string;
    btnBackgroundColorHover: string;
    btnColorHover: string;
    btnBackgroundColor: string;
    btnColor: string;
    btnFontSize: number;
    btnIconOpacity: number;
    btnMargin: number;
    btnPadding: number;
    private _candidateDummyIdMapCache;
    canvas: {
        background?: Konva.Rect;
        btnCancelGroup?: Konva.Group;
        btnCancelBackground?: Konva.Circle;
        btnCancelIcon?: Konva.Path;
    };
    private tableColumns;
    private eventListHeaderHeight;
    private eventListRowHeight;
    private _filteredData;
    get filteredData(): IKaiSchedulerTreeItem[];
    set filteredData(data: IKaiSchedulerTreeItem[]);
    private _isActive;
    get isActive(): boolean;
    private itemInitialHeight;
    private itemTitlePaddingPx;
    private mergeCandidatesData;
    resources: IKaiSchedulerTreeItem[];
    private _resourcesCache;
    private _resourceTitlesCache;
    private scrollBarMargin;
    private scrollBarSize;
    private _targets;
    get targets(): number[];
    set targets(value: number[]);
    targetTree: Tree<TreeEvents>;
    private visibleTables;
    private width;
    x: number;
    y: number;
    constructor(config: ITargetPickerConfig);
    cancel(): void;
    protected emitSelect(selectedResource: IKaiSchedulerTreeItem, mergeCandidate: IMergeCandidate, selectedEvents: IEvent[]): void;
    private _filter;
    private getResourceFromTitle;
    getResourceFromId(resourceId: any): IKaiSchedulerTreeItem;
    getResourceIds(targetResources: any): number[];
    getTargetResources(event: IEvent): number[];
    hide(): void;
    initStore(preloadedState: ISchedulerState): void;
    treeItemCanvasPrepared(ev: TreeItemCanvasPreparedEvent): void;
    treeItemClick(event: TreeItemClickEvent): void;
    prepareCanvas(): void;
    show(event: IEvent): void;
}
