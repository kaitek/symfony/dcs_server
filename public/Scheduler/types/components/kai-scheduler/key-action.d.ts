import { Action } from 'redux';
export interface IStateKey {
    stateKey?: string;
}
export interface KeyAction extends Action, IStateKey {
}
