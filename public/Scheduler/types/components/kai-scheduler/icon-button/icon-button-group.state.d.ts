import { IStateKey } from '../key-action';
import { ISchedulerIcon } from './icons';
import { IBorderParameters } from '../shared/interfaces/border-parameters';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
export declare namespace IconButtonGroupState {
    interface ButtonStyle {
        backgroundColor?: string;
        border?: IBorderParameters;
        iconColor?: string;
    }
    type ButtonStyleState = 'default' | 'hover' | 'disabled';
    interface ButtonStyles {
        default?: ButtonStyle;
        hover?: ButtonStyle;
        disabled?: ButtonStyle;
    }
    interface ButtonState extends IStateKey {
        disabled?: boolean;
        icon?: ISchedulerIcon;
        styles?: ButtonStyles;
        tooltip?: string;
        x?: number;
        visible?: boolean;
    }
    interface ButtonStates {
        [stateKey: string]: ButtonState;
    }
    interface GroupState extends IStateKey {
        buttons?: ButtonStates;
        buttonSpacing?: number;
        buttonPadding?: number;
        styles?: ButtonStyles;
        iconSize?: number;
        left?: number;
        margin?: number;
        right?: number;
        top?: number;
        visible?: boolean;
    }
    interface GroupStates {
        [stateKey: string]: GroupState;
    }
    const getInitialState: (initialState: GroupStates) => GroupStates;
    const reducer: (states: GroupStates, _action: any) => GroupStates;
    const afterAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
}
