import Konva from 'konva';
import { ISchedulerComponentConfig, SchedulerComponentEvents, SchedulerComponent, SchedulerComponentEvent } from '../shared/scheduler-component';
import { IconButtonGroupState } from './icon-button-group.state';
import { ISchedulerState } from '../kai-scheduler.store';
import { IconButtonGroup } from './icon-button-group';
export interface IIconButtonConfig extends ISchedulerComponentConfig, IconButtonGroupState.ButtonState {
    parent: IconButtonGroup;
    index: number;
}
export declare class IconButtonClickEvent extends SchedulerComponentEvent {
    button: IconButton;
    constructor(button: IconButton);
}
export declare class IconButtonEvents extends SchedulerComponentEvents {
    click: IconButtonClickEvent;
}
export declare class IconButton extends SchedulerComponent<IconButtonEvents> implements IIconButtonConfig {
    canvas: {
        background: Konva.Circle;
        icon: Konva.Path;
    };
    disabled: boolean;
    index: number;
    parent: IconButtonGroup;
    get parentStyles(): IconButtonGroupState.ButtonStyles;
    stateKey: string;
    get stateKeyParent(): string;
    styles: IconButtonGroupState.ButtonStyles;
    private _styleState;
    protected get styleState(): IconButtonGroupState.ButtonStyleState;
    protected set styleState(value: IconButtonGroupState.ButtonStyleState);
    constructor(config: IIconButtonConfig);
    protected applyStyles(): void;
    initStore(preloadedState: ISchedulerState): void;
    prepareCanvas(): void;
}
