export interface ISchedulerIcon {
    width: number;
    height: number;
    data: string;
}
export declare const ICON_LOCK: ISchedulerIcon;
export declare const ICON_LOCK_OPEN: ISchedulerIcon;
export declare const ICON_MINUS_SQUARE: ISchedulerIcon;
export declare const ICON_CHECK: ISchedulerIcon;
export declare const ICON_SAVE: ISchedulerIcon;
export declare const ICON_SEARCH: ISchedulerIcon;
export declare const ICON_TIMES: ISchedulerIcon;
export declare const ICON_REDO: ISchedulerIcon;
export declare const ICON_UNDO: ISchedulerIcon;
