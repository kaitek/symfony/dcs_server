import { IconButtonGroupState } from './icon-button-group.state';
import { ISchedulerComponentConfig, SchedulerComponentEvents, SchedulerComponent, SchedulerComponentEvent } from '../shared/scheduler-component';
import { ISchedulerState } from '../kai-scheduler.store';
import { IconButton } from './icon-button';
export interface IIconButtonGroupConfig extends ISchedulerComponentConfig, IconButtonGroupState.GroupState {
}
export declare class IconButtonGroupClickEvent extends SchedulerComponentEvent {
    button: IconButton;
    buttonGroup: IconButtonGroup;
    constructor(button: IconButton, buttonGroup: IconButtonGroup);
}
export declare class IconButtonGroupEvents extends SchedulerComponentEvents {
    click: IconButtonGroupClickEvent;
}
export declare class IconButtonGroup extends SchedulerComponent<IconButtonGroupEvents> implements IIconButtonGroupConfig {
    private readonly buttonEls;
    canvas: {};
    stateKey: string;
    styles: IconButtonGroupState.ButtonStyles;
    constructor(config: IIconButtonGroupConfig);
    initStore(preloadedState: ISchedulerState): void;
    prepareCanvas(): void;
}
