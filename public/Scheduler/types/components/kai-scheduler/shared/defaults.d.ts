export declare namespace Defaults {
    namespace Common {
        const BACKGROUND_COLOR = "rgb(250, 250, 250)";
        const BORDER_COLOR = "rgb(210, 210, 210)";
        const BORDER_SIZE = 1;
        const LOCALE = "en";
        const FONT_FAMILY = "Arial";
        const FONT_SIZE = 14;
    }
    namespace Controller {
        const BUTTON_MARGIN = 8;
        const BUTTON_GROUP_MARGIN = 8;
        const BUTTON_BACKGROUND_COLOR = "#339af0";
        const BUTTON_BACKGROUND_COLOR_HOVER = "#228be6";
        const BUTTON_BACKGROUND_COLOR_DISABLED = "#accbe5";
        const BUTTON_BORDER_COLOR = "#fff";
        const BUTTON_BORDER_SIZE = 2;
        const BUTTON_COLOR = "#e3eff9";
        const BUTTON_COLOR_HOVER = "#f2f7fc";
        const BUTTON_COLOR_DISABLED = "#d4eafc";
        const BUTTON_ICON_SIZE = 16;
        const BUTTON_PADDING = 8;
        const BUTTON_SAVE_BACKGROUND_COLOR = "#1B5E20";
        const BUTTON_SAVE_BACKGROUND_COLOR_HOVER = "#43A047";
        const BUTTON_SAVE_COLOR = "rgba(255, 255, 255, .8)";
        const BUTTON_SAVE_COLOR_HOVER = "rgba(255, 255, 255, .9)";
        const BUTTON_SAVE_TOOLTIP = "event-container.buttons.save";
        const BUTTON_CANCEL_BACKGROUND_COLOR = "#E53935";
        const BUTTON_CANCEL_BACKGROUND_COLOR_HOVER = "#F44336";
        const BUTTON_CANCEL_COLOR = "rgba(255, 255, 255, .8)";
        const BUTTON_CANCEL_COLOR_HOVER = "rgba(255, 255, 255, .9)";
        const BUTTON_CANCEL_TOOLTIP = "event-container.buttons.cancel";
        const BUTTON_UNSCHEDULE_BACKGROUND_COLOR = "#FB8C00";
        const BUTTON_UNSCHEDULE_BACKGROUND_COLOR_HOVER = "#FF9800";
        const BUTTON_UNSCHEDULE_COLOR = "rgba(255, 255, 255, .8)";
        const BUTTON_UNSCHEDULE_COLOR_HOVER = "rgba(255, 255, 255, .9)";
        const BUTTON_UNSCHEDULE_TOOLTIP = "event-container.buttons.unschedule";
        const BUTTON_UNDO_TOOLTIP = "event-container.buttons.undo";
        const BUTTON_REDO_TOOLTIP = "event-container.buttons.redo";
        const UNDO_HISTORY_LIMIT = 200;
    }
    namespace EventContainer {
        const EVENT_BACKGROUND_COLORS: string[];
        const EVENT_BACKGROUND_HOVER_COLOR = "#B3E5FC";
        const EVENT_BACKGROUND_HOVER_DURATION = 0.2;
        const EVENT_BACKGROUND_SELECTED_COLOR = "#FB8C00";
        const EVENT_BORDER_COLOR = "#757575";
        const EVENT_BORDER_COLOR_DRAGGING = "#1B5E20";
        const EVENT_BORDER_COLOR_PINNED = "#EA4335";
        const EVENT_BORDER_COLOR_SHIFTING = "#F44336";
        const EVENT_BORDER_COLOR_SELECTED = "#FF0000";
        const EVENT_BORDER_SIZE = 1;
        const EVENT_BORDER_SIZE_DRAGGING = 1;
        const EVENT_BORDER_SIZE_PINNED = 2;
        const EVENT_BORDER_SIZE_SHIFTING = 1;
        const EVENT_BORDER_SIZE_SELECTED = 1;
        const EVENT_BOX_HEIGHT = 28;
        const EVENT_BOX_MARGIN_PX = 6;
        const EVENT_BOX_SNAP_TRESHOLD_PX = 5;
        const EVENT_BOX_TITLE_FONT_COLOR = "rgba(0, 0, 0, .7)";
        const EVENT_BOX_TITLE_FONT_FAMILIY = "Arial";
        const EVENT_BOX_TITLE_FONT_SIZE = 14;
        const EVENT_BOX_TITLE_FONT_STYLE = "bold";
        const EVENT_CORNER_RADIUS = 2;
        const EVENT_FAILED_MERGE_COLOR = "#ff2600";
        const EVENT_FOCUS_ANIMATION_DURATION_MS = 200;
        const EVENT_POST_PROCESS_COLOR = "#3a1c00";
        const EVENT_PRE_PROCESS_COLOR = "#ffea07";
    }
    namespace EventList {
        const FONT_COLOR = "rgba(11, 51, 60, .5)";
        const FONT_SIZE = 12;
        const INITIAL_WIDTH = 300;
        const MAX_WIDTH = 500;
        const MIN_WIDTH = 100;
        const TOGGLER_TITLE = "event-list.toggler-title";
        const TOGGLER_WIDTH = 30;
    }
    namespace Resizer {
        const BACKGROUND_COLOR = "rgb(230, 230, 230)";
        const DOT_BUTTON_SIZE_PX = 28;
        const DOT_COLOR = "rgb(95, 99, 104)";
        const DOT_SIZE = 3;
        const DOT_SPACING = 5;
        const HOVER_COLOR = "rgb(210, 210, 210)";
        const SIZE_PX = 4;
        const TOGGLE_DURATION_MS = 350;
        const TOGGLER_FONT_COLOR = "rgb(95, 99, 104)";
        const TOGGLER_FONT_FAMILY = "Arial";
        const TOGGLER_FONT_SIZE = 14;
        const TOGGLER_PADDING = 4;
    }
    namespace ResourceTree {
        const PLANNED_STOPS_OPACITY = 0.3;
        const PLANNED_STOPS_CALCULATION_TRESHOLD_DAYS = 15;
        const TEXT_COLOR = "rgb(95, 99, 104)";
        const TEXT_SIZE_PX = 16;
        const TOGGLER_TITLE = "resource-list.toggler-title";
    }
    namespace Scroller {
        const BAR_COLOR = "rgba(60, 66, 82, .25)";
        const BAR_COLOR_HOVER = "rgba(60, 66, 82, .50)";
        const BAR_GROW_FACTOR = 2;
        const BAR_MARGIN = 5;
        const BAR_SIZE = 12;
        const BASE_COLOR = "rgba(60, 66, 82, .1)";
        const BASE_COLOR_HOVER = "rgba(60, 66, 82, .15)";
        const DURATION = 100;
    }
    namespace Search {
        const BUTTON_COLOR = "#fff";
        const BUTTON_COLOR_HOVER = "#fff";
        const BUTTON_BACKGROUND_COLOR = "#339af0";
        const BUTTON_BACKGROUND_COLOR_HOVER = "#228be6";
        const BUTTON_EXPANDED_WIDTH = 180;
        const BUTTON_FONT_SIZE = 16;
        const BUTTON_ICON_OPACITY = 0.8;
        const BUTTON_MARGIN = 8;
        const BUTTON_PADDING = 8;
    }
    namespace Table {
        const HEADER_BACKGROUND_COLOR_STOPS: (string | number)[];
        const HEADER_FONT_COLOR = "rgba(11, 51, 60, .5)";
        const HEADER_FONT_SIZE = 12;
        const HEADER_HEIGHT = 24;
        const HORIZONTAL_PADDING = 3;
        const LONG_PRESS_LATENCY_MS = 500;
        const ROW_COLORS: string[];
        const ROW_HEIGHT = 30;
        const ROW_HOVER_COLOR = "rgb(232, 248, 255)";
        const ROW_HOVER_DURATION = 0.2;
    }
    namespace TargetPicker {
        const BUTTON_BACKGROUND_COLOR = "#E53935";
        const BUTTON_BACKGROUND_COLOR_HOVER = "#F44336";
        const BUTTON_COLOR = "#fff";
        const BUTTON_COLOR_HOVER = "#fff";
        const BUTTON_FONT_SIZE = 16;
        const BUTTON_ICON_OPACITY = 0.8;
        const BUTTON_MARGIN = 10;
        const BUTTON_PADDING = 8;
        const ROW_COLORS: string[];
        const ROW_HOVER_COLOR = "rgb(200, 230, 201)";
        const ROW_RIPPLE_COLOR = "rgb(150, 200, 150)";
        const ROW_SELECTED_COLOR = "rgba(27, 94, 32, .4)";
        const TABLE_FONT_SIZE = 12;
    }
    namespace TextToggleButtonGroup {
        const BACKGROUND_COLOR = "#FEFEFE";
        const BACKGROUND_COLOR_HOVER = "#B3E5FC";
        const BACKGROUND_COLOR_SELECTED = "#E5E5E5";
        const FONT_COLOR = "rgba(11, 51, 60, .5)";
        const FONT_COLOR_HOVER = "rgba(11, 51, 60, .5)";
        const FONT_COLOR_SELECTED = "rgba(11, 51, 60, .5)";
        const FONT_SIZE = 12;
        const MARGIN = 8;
        const PADDING = 5;
        const TITLE_SINGLE = "event-container.drag-mode.single";
        const TITLE_SINGLE_TOOLTIP = "event-container.drag-mode.single-tooltip";
        const TITLE_MULTIPLE = "event-container.drag-mode.multiple";
        const TITLE_MULTIPLE_TOOLTIP = "event-container.drag-mode.multiple-tooltip";
        const TITLE_PRESERVE_FREE_TIME = "event-container.drag-mode.preserve-free-time";
        const TITLE_PRESERVE_FREE_TIME_TOOLTIP = "event-container.drag-mode.preserve-free-time-tooltip";
        const TITLE_DONT_PRESERVE_FREE_TIME = "event-container.drag-mode.dont-preserve-free-time";
        const TITLE_DONT_PRESERVE_FREE_TIME_TOOLTIP = "event-container.drag-mode.dont-preserve-free-time-tooltip";
    }
    namespace Timeline {
        const DAYS_TO_LOAD = 7;
        const FONT_SIZE_PX = 12;
        const LINE_PADDING_VERTICAL = 4;
        const NOW_INDICATOR_COLOR = "rgb(1, 172, 237)";
        const NOW_INDICATOR_OPACITY = 0.5;
        const NOW_INDICATOR_SIZE_FACTOR = 10;
        const ROW_BACKGROUND_COLOR_STOPS: (string | number)[];
        const ROW_HEIGHT_PX = 24;
        const SEPARATOR_COLOR = "rgb(210, 210, 210)";
        const TEXT_COLOR = "rgba(11, 51, 60, .5)";
        const TEXT_PADDING_HORIZONTAL = 4;
        const ZOOM_DURATION = 100;
        const ZOOM_LEVEL = 30;
        const ZOOM_MAX_LEVEL = 40;
        const ZOOM_STEP = 14941.78125;
    }
    namespace Tooltip {
        const FILL = "rgba(0, 0, 0, .8)";
        const FONT_SIZE = 12;
        const POINTER_SIZE = 8;
        const SHADOW_BLUR = 6;
        const SHADOW_COLOR = "rgb(0, 0, 0)";
        const SHADOW_OFFSET: {
            x: number;
            y: number;
        };
        const SHADOW_OPACITY = 0.2;
        const SHOW_LATENCY_MS = 250;
    }
    namespace Tree {
        const ROW_HOVER_DURATION = 0.2;
        const COLLAPSE_ICON_SIZE_PX = 10;
        const ITEM_INITIAL_HEIGHT_PX = 40;
        const ITEM_RIPPLE_DURATION = 0.65;
        const ITEM_TITLE_PADDING_PX = 10;
        const ROW_COLORS: string[];
        const ROW_HOVER_COLOR = "rgb(232, 248, 255)";
        const ROW_RIPPLE_COLOR = "rgb(79, 195, 247)";
        const TOGGLE_DURATION = 0.5;
        const WIDTH_MIN_PX = 50;
        const WIDTH_MAX_PX = 350;
        const WIDTH_PX = 280;
    }
}
