import { SchedulerAction } from '../kai-scheduler.store';
import { CanvasComponent, CanvasComponentCanvasPreparedEvent, CanvasComponentEvent, CanvasComponentEvents, ICanvasComponentConfig } from './canvas-component';
import { TranslateService } from './i18n/translate-service';
import { IBorderParameters } from './interfaces/border-parameters';
import { ICommonParameters } from './interfaces/common-parameters';
import { IKaiSchedulerShared } from './kai-scheduler-shared';
export declare class SchedulerComponentEvent extends CanvasComponentEvent {
}
export declare class SchedulerComponentResize extends SchedulerComponentEvent {
    newWidth: number;
    newHeight: number;
    constructor(newWidth: number, newHeight: number);
}
export declare class SchedulerComponentEvents extends CanvasComponentEvents {
    resize: SchedulerComponentResize;
}
export interface ISchedulerComponentConfig extends ICanvasComponentConfig {
    parentScheduler: IKaiSchedulerShared;
}
export declare class SchedulerComponent<T extends SchedulerComponentEvents> extends CanvasComponent<T> {
    parentScheduler: IKaiSchedulerShared;
    get border(): IBorderParameters;
    get common(): ICommonParameters;
    get store(): any;
    private storeUnsubscribe;
    get translateService(): TranslateService;
    constructor(config: ISchedulerComponentConfig);
    destroy(): void;
    dispatch(action: SchedulerAction): SchedulerAction;
    protected emitResize(newWidth: number, newHeight: number): void;
    protected onCanvasPrepared(event: CanvasComponentCanvasPreparedEvent): void;
    protected storeOnChange(listener: () => void): void;
}
