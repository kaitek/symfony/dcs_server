/**
 * Source: https://kodhus.com/easings/
 *
 * @param t current time/current step
 * @param b starting position
 * @param c amount of change (end - start)
 * @param d total animation time/steps
 */
export declare function easeOutBounce(t: any, b: any, c: any, d: any): number;
export declare function easeOutQuad(t: number, b: number, c: number, d: number): number;
