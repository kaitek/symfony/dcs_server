import Konva from 'konva';
export interface IKaiSchedulerCanvasElements {
    baseLayer?: Konva.Layer;
    baseLayerBg?: Konva.Rect;
    borderBottom?: Konva.Line;
    borderRight?: Konva.Line;
    eventListLayer?: Konva.Layer;
    objectLayerBaseGroup?: Konva.Group;
    objectLayerEventsGroup?: Konva.Group;
    objectLayerInnerGroup?: Konva.Group;
    objectLayerStopsGroup?: Konva.Group;
    resourceBackgroundsGroup?: Konva.Group;
    stage?: Konva.Stage;
    tooltipOfPlannedStops?: {
        label?: Konva.Label;
        tag?: Konva.Tag;
        text?: Konva.Text;
    };
}
