export interface IBorderParameters {
    color?: string;
    size?: number;
}
