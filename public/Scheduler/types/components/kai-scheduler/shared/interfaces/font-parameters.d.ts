export interface IFontParameters {
    color?: string;
    family?: string;
    size?: number;
    style?: string;
}
