import { IBorderParameters } from './border-parameters';
import { IFontParameters } from './font-parameters';
export interface ICommonParameters {
    backgroundColor?: string;
    border?: IBorderParameters;
    font?: IFontParameters;
}
