export interface IMergedStop {
    dateStart: number;
    dateEnd: number;
    duration: number;
}
