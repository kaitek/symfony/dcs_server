import Konva from 'konva';
export interface IPlannedStop {
    id: number;
    color: string;
    description: string;
    dateStart: number;
    _dateStart?: Date;
    dateEnd: number;
    _dateEnd?: Date;
    _widthDiffMs?: number;
    _xDiffMs?: number;
    __x?: number;
    __width?: number;
    __rect?: Konva.Rect;
    ___mouse?: number;
    ___mouseInterval?: any;
    ___tooltipShowTriggered?: boolean;
}
export interface IPlannedStopList {
    [resourceId: string]: IPlannedStop[];
}
export declare type PlannedStopFactory = (resourceId: any, startMs: number, endMs: number) => IPlannedStop[];
