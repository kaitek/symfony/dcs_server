export interface IVisibleRange {
    contentAreaWidth: number;
    maxDate: Date;
    maxDateMs: number;
    minDate: Date;
    minDateMs: number;
}
