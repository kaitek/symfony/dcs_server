export declare enum SchedulerRelationTarget {
    START = "START",
    END = "END"
}
