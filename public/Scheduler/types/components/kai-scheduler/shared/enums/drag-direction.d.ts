export declare enum DragDirection {
    FORWARD = "FORWARD",
    BACKWARD = "BACKWARD"
}
