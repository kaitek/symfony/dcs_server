import Konva from 'konva';
import { KonvaEventObject } from 'konva/types/Node';
export interface ICanvasComponentConfig {
    addTo?: Konva.Container;
    zIndex?: number;
}
export declare class CanvasComponentEvent {
}
export declare class CanvasComponentMouseEvent extends CanvasComponentEvent {
    canvasEvent: KonvaEventObject<MouseEvent>;
    constructor(canvasEvent: KonvaEventObject<MouseEvent>);
}
export declare type CanvasEventListener<T = CanvasComponentEvent> = (event: T) => void;
export declare class CanvasComponentCanvasPreparedEvent extends CanvasComponentEvent {
}
export declare class CanvasComponentDestryoEvent extends CanvasComponentEvent {
}
export declare class CanvasComponentEvents {
    canvasPrepared: CanvasComponentCanvasPreparedEvent;
    destroy: CanvasComponentDestryoEvent;
    mouseenter: CanvasComponentMouseEvent;
    mouseleave: CanvasComponentMouseEvent;
}
export declare class CanvasComponent<T extends CanvasComponentEvents> {
    addTo: Konva.Container;
    protected canvasContainer: Konva.Container;
    __eventTypes: {
        [key: string]: any;
    };
    __eventListeners: {
        [eventName in keyof T]?: Array<CanvasEventListener<T[eventName]>>;
    };
    private _zIndex;
    get zIndex(): number;
    set zIndex(value: number);
    constructor(config: ICanvasComponentConfig);
    cache(): void;
    clearCache(): void;
    destroy(): void;
    emit<K extends keyof T, P extends T[K]>(eventName: K, event: P): void;
    protected emitCanvasPrepared(): void;
    protected emitDestroy(): void;
    protected emitMouseEnter(ev: KonvaEventObject<MouseEvent>): void;
    protected emitMouseLeave(ev: KonvaEventObject<MouseEvent>): void;
    off<K extends keyof T>(eventName: K, listener: CanvasEventListener<T[K]>): this;
    on<K extends keyof T>(eventName: K, listener: CanvasEventListener<T[K]>): this;
    protected onCanvasPrepared(_event: CanvasComponentCanvasPreparedEvent): void;
    passWheelEvent(event: KonvaEventObject<WheelEvent>): void;
}
