import { ResizerState } from '../resizer/resizer.state';
import { IBorderParameters } from './interfaces/border-parameters';
import { IFontParameters } from './interfaces/font-parameters';
export declare namespace CommonState {
    interface State {
        backgroundColor?: string;
        border?: IBorderParameters;
        font?: IFontParameters;
        resizer?: ResizerState.State;
    }
    const INITIAL_STATE: State;
    const getInitialState: (override?: State) => State;
    namespace actionTypes {
        const SET_STATE = "[COMMON] set state";
        class SetStateAction {
            state: State;
            readonly type = "[COMMON] set state";
            constructor(state: State);
        }
    }
    type actions = actionTypes.SetStateAction;
    const reducer: (state: State, action: actions) => State;
    const setState: (state: State) => actionTypes.SetStateAction;
}
