import { Store, Action } from 'redux';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { ResourceTree, ResourceTreeEvents } from '../resource-tree/resource-tree';
import { CommonState } from './common.state';
import { TranslateService } from './i18n/translate-service';
import { KonvaEventObject } from 'konva/types/Node';
import { ISaveData } from '../kai-scheduler';
export interface IKaiSchedulerShared {
    common: CommonState.State;
    cursor: string;
    readonly isPanning: boolean;
    readonly mousePositionDateMs: number;
    readonly resourceTree?: ResourceTree<ResourceTreeEvents>;
    readonly: boolean;
    readonly store: Store<ISchedulerState, SchedulerAction>;
    readonly translateService?: TranslateService;
    cancel: () => Promise<any>;
    focusToEvent: (eventId: string) => void;
    getYForResource: (resourceId: any) => number;
    passWheelEvent: (event: KonvaEventObject<WheelEvent>) => void;
    redo: () => Promise<Action<any>>;
    removeSelectedFromSchedule: () => Promise<Action<any>>;
    save: () => Promise<ISaveData>;
    tooltipForStopShow: (text: any, x: any, y: any) => void;
    tooltipForStopHide: () => void;
    undo: () => Promise<Action<any>>;
}
