import { TOptions, StringMap } from 'i18next';
export declare type SUPPORTED_LOCALES = 'en' | 'tr';
export declare type ISchedulerTranslation = {
    [key in SUPPORTED_LOCALES]: any;
};
export declare class TranslateService {
    private _fixedT;
    get locale(): SUPPORTED_LOCALES;
    set locale(newLocale: SUPPORTED_LOCALES);
    private readonly _namespace;
    constructor(lng: SUPPORTED_LOCALES, debug: boolean, onReady: () => void);
    addTranslation(locale: any, resourceBundle: any): void;
    t(key: any, options?: string | TOptions<StringMap>): any;
}
