import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { ResizerState } from './resizer.state';
export interface ResizerConfig extends ISchedulerComponentConfig, ResizerState.State {
}
export declare class ResizerResizedEvent extends SchedulerComponentEvent {
    newWidth: number;
    constructor(newWidth: number);
}
export declare class ResizerEvents extends SchedulerComponentEvents {
    resized: ResizerResizedEvent;
}
export declare class Resizer extends SchedulerComponent<ResizerEvents> implements ResizerConfig {
    backgroundColor: string;
    canvas: {
        background?: Konva.Rect;
        dotButtonCircle?: Konva.Circle;
        icon?: Konva.Line;
        titleText?: Konva.Text;
        toggleAnimation?: Konva.Animation;
        __collapseTimeoutHandler?: any;
    };
    private _collapsed;
    get collapsed(): boolean;
    set collapsed(value: boolean);
    collapsedTitle: string;
    collapsedSize: number;
    hoverColor: string;
    private get _isCollapsible();
    private _isVertical;
    min?: number;
    max?: number;
    target: 'width';
    parentWidth: number;
    private _parentWidthBeforeToggle;
    private _parentWidthDiff;
    position: 'start' | 'end';
    private _posX;
    size: number;
    stateKey: string;
    toggleDurationMs: number;
    togglerPadding: number;
    private _y;
    constructor(config: ResizerConfig);
    private _animateToggle;
    private _collapse;
    protected emitResized(newVal?: number): void;
    private _expand;
    initStore(preloadedState: ISchedulerState): void;
    prepareCanvas(): void;
    setState(state: ResizerState.State): void;
    toggle(): void;
}
