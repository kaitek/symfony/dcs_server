import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { IStateKey, KeyAction } from '../key-action';
import { IBorderParameters } from '../shared/interfaces/border-parameters';
import { IFontParameters } from '../shared/interfaces/font-parameters';
export declare type ResizerPosition = 'start' | 'end';
export declare type ResizerTarget = 'width';
export declare namespace ResizerState {
    interface State extends IStateKey {
        backgroundColor?: string;
        border?: IBorderParameters;
        collapsed?: boolean;
        collapsedSize?: number;
        collapsedTitle?: string;
        dotButtonSizePx?: number;
        _dotButtonPosX?: number;
        _dotButtonPosY?: number;
        dotColor?: string;
        dotSize?: number;
        dotSpacing?: number;
        font?: IFontParameters;
        _height?: number;
        hoverColor?: string;
        _iconPosX?: number;
        _iconPosY?: number;
        min?: number;
        max?: number;
        target?: ResizerTarget;
        _textPosX?: number;
        parentHeight?: number;
        parentWidth?: number;
        position?: ResizerPosition;
        size?: number;
        toggleDurationMs?: number;
        togglerPadding?: number;
        _width?: number;
        _x?: number;
        y?: number;
    }
    const getInitialState: (override?: State) => State;
    interface States {
        [stateKey: string]: State;
    }
    namespace actionTypes {
        enum names {
            SET_STATE = "[RESIZER] set state"
        }
        class SetStateAction implements KeyAction {
            stateKey: string;
            state: State;
            readonly type = names.SET_STATE;
            constructor(stateKey: string, state: State);
        }
        const contains: (action: any) => action is SetStateAction;
    }
    type actions = actionTypes.SetStateAction;
    const itemReducer: (state: State, action: actions) => State;
    const reducer: (states: States, action: actions) => States;
    const afterAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const setState: (stateKey: string, state: State) => actionTypes.SetStateAction;
}
