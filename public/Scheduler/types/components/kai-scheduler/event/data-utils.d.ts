import { ISchedulerState } from '../kai-scheduler.store';
import { IMergedEvents } from '../schedule-controller/event-merge-checker';
import { IScheduleRelation } from '../schedule-controller/schedule-relation';
import { DragDirection } from '../shared/enums/drag-direction';
import { SchedulerDragMode } from '../shared/enums/drag-mode';
import { IEvent } from './event.interface';
export interface IOverlappedEvent {
    rel: IScheduleRelation;
    previousDuration: number;
    remainingDuration: number;
    preProcessDuration: number;
    preProcessDurationWithPlannedStops: number;
    postProcessDuration: number;
    postProcessDurationWithPlannedStops: number;
}
export declare namespace DataUtils {
    const clearDurationCache: () => void;
    const dragEvent: (state: ISchedulerState, eventId: string, movementPx: number) => void;
    const getAbsolutePosXForMs: (state: ISchedulerState, ms: number) => number;
    const getDurationOfRelationWithPlannedStops: (state: ISchedulerState, scheduleRelation: IScheduleRelation, direction?: DragDirection, dateToCalc?: number) => number;
    const getMaxEnd: (state: ISchedulerState, scheduleRelation: IScheduleRelation, dragMode?: SchedulerDragMode) => number;
    const getMergedEvents: (state: ISchedulerState, scheduleRelation: IScheduleRelation, direction?: DragDirection, _dateToCalc?: number) => IMergedEvents[];
    const getMinStart: (state: ISchedulerState, scheduleRelation: IScheduleRelation, dragMode?: SchedulerDragMode) => number;
    const getTotalDurationOfRelationWithStops: (state: ISchedulerState, scheduleRelation: IScheduleRelation, direction?: DragDirection, dateToCalc?: number) => number;
    const initData: (state: ISchedulerState) => void;
    const refreshRowIndexesOfInvolvedEvents: (state: ISchedulerState, eventId: string) => void;
    const scheduleTo: (state: ISchedulerState, resourceId: string, event: IEvent, date?: Date, initialLoad?: boolean, dontModifyDate?: boolean, mergedEvents?: IMergedEvents[]) => boolean;
    const shiftEvent: (state: ISchedulerState, relationOfEventToShift: IScheduleRelation, movementMs: number, dragSessionTimestamp: number) => void;
}
