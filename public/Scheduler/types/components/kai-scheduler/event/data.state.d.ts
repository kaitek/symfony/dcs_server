import { StateWithHistory } from 'redux-undo';
import { Action, AnyAction, Reducer } from 'redux';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { KeyAction } from '../key-action';
import { EventMergeChecker, IMergedEvents } from '../schedule-controller/event-merge-checker';
import { EventRelationChecker } from '../schedule-controller/event-relation';
import { IScheduleRelation } from '../schedule-controller/schedule-relation';
import { DragDirection } from '../shared/enums/drag-direction';
import { IEvent } from './event.interface';
export interface IDiffCache {
    eventA: number | string;
    eventB: number | string;
    userManagedDiffMs: number;
}
export declare namespace DataState {
    interface IEventStates {
        [eventId: string]: IEvent;
    }
    interface IScheduleStates {
        [resourceId: string]: IScheduleRelation[];
    }
    interface IRowCounts {
        [resourceId: string]: number;
    }
    interface States {
        events?: IEventStates;
        schedule?: IScheduleStates;
        _diffCache?: IDiffCache[];
        _dragDirection?: DragDirection;
        _draggingEvent?: IEvent;
        _draggingEvents?: string[];
        _dragInvolvedEvents?: IScheduleRelation[];
        _isAllowedToInsert?: boolean;
        _jumpBackupStart?: number;
        _dragMergedEvents?: IMergedEvents[];
        _eventKeys?: string[];
        _modified?: boolean;
        _rowCounts?: IRowCounts;
        _scheduledEvents?: string[];
        _selectedEvents?: string[];
    }
    const getInitialState: (preloadedEvents: IEvent[], predecessors: EventRelationChecker, successors: EventRelationChecker, mergeCandidates: EventMergeChecker) => StateWithHistory<States>;
    namespace actionTypes {
        enum names {
            DRAG_EVENT_BOX = "[EVENT] drag event box",
            SCHEDULE_EVENTS = "[EVENT] schedule event(s)",
            UNSCHEDULE_EVENTS = "[EVENT] remove event(s) from schedule",
            SELECT_EVENT = "[EVENT] select event",
            DESELECT_EVENT = "[EVENT] deselect event",
            SET_EVENT_STATE = "[EVENT] set state",
            SET_SCHEDULE_STATE = "[SCHEDULE] set state",
            SET_DIFF_CACHE = "[SCHEDULE] set cache of user managed diff between events",
            SET_DRAG_DIRECTION = "[DATA] set drag direction",
            SET_STATES = "[DATA] set states",
            STEP_HISTORY = "[DATA] step history"
        }
        class DragEventBox implements KeyAction {
            eventId: string;
            movementPx: number;
            readonly type = names.DRAG_EVENT_BOX;
            constructor(eventId: string, movementPx: number);
        }
        class ScheduleEvents implements KeyAction {
            events: IEvent[];
            resourceId: string;
            readonly type = names.SCHEDULE_EVENTS;
            constructor(events: IEvent[], resourceId: string);
        }
        class UnScheduleEvents implements KeyAction {
            events: IEvent[];
            readonly type = names.UNSCHEDULE_EVENTS;
            constructor(events: IEvent[]);
        }
        class SelectEvent implements KeyAction {
            event: IEvent;
            readonly type = names.SELECT_EVENT;
            constructor(event: IEvent);
        }
        class DeselectEvent implements KeyAction {
            event: IEvent;
            readonly type = names.DESELECT_EVENT;
            constructor(event: IEvent);
        }
        class SetEventStateAction implements KeyAction {
            eventId: string;
            state: IEvent;
            readonly type = names.SET_EVENT_STATE;
            constructor(eventId: string, state: IEvent);
        }
        class SetScheduleStateAction implements KeyAction {
            resourceId: string;
            state: IScheduleRelation[];
            readonly type = names.SET_SCHEDULE_STATE;
            constructor(resourceId: string, state: IScheduleRelation[]);
        }
        class SetDiffCacheAction implements Action {
            _diffCache: IDiffCache[];
            readonly type = names.SET_DIFF_CACHE;
            constructor(_diffCache: IDiffCache[]);
        }
        class SetDragDirection implements Action {
            dragDirection: DragDirection;
            readonly type = names.SET_DRAG_DIRECTION;
            constructor(dragDirection: DragDirection);
        }
        class SetStatesAction implements Action {
            states: States;
            readonly type = names.SET_STATES;
            constructor(states: States);
        }
        class StepHistoryAction implements Action {
            readonly type = names.STEP_HISTORY;
        }
        const contains: (action: any) => action is actions;
    }
    type actions = actionTypes.DragEventBox | actionTypes.ScheduleEvents | actionTypes.SelectEvent | actionTypes.DeselectEvent | actionTypes.SetEventStateAction | actionTypes.SetScheduleStateAction | actionTypes.SetDiffCacheAction | actionTypes.SetDragDirection | actionTypes.SetStatesAction | actionTypes.StepHistoryAction | actionTypes.UnScheduleEvents;
    const reducer: Reducer<StateWithHistory<States>, AnyAction>;
    const beforeAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const afterAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const dragEventBox: (eventId: string, movementPx: number) => actionTypes.DragEventBox;
    const scheduleEvents: (events: IEvent[], resourceId: string) => actionTypes.ScheduleEvents;
    const selectEvent: (event: IEvent) => actionTypes.SelectEvent;
    const deselectEvent: (event: IEvent) => actionTypes.DeselectEvent;
    const setEventState: (eventId: string, state: IEvent) => actionTypes.SetEventStateAction;
    const setScheduleState: (stateKey: string, state: IScheduleRelation[]) => actionTypes.SetScheduleStateAction;
    const setDiffCache: (_diffCache: IDiffCache[]) => actionTypes.SetDiffCacheAction;
    const setDragDirection: (dragDirection: DragDirection) => actionTypes.SetDragDirection;
    const setStates: (states: States) => actionTypes.SetStatesAction;
    const stepHistory: () => actionTypes.StepHistoryAction;
    const unScheduleEvents: (events: IEvent[]) => actionTypes.UnScheduleEvents;
}
