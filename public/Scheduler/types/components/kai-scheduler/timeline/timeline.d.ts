import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { IBorderParameters } from '../shared/interfaces/border-parameters';
import { IFontParameters } from '../shared/interfaces/font-parameters';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { INowIndicatorParameters, NowIndicator } from './now-indicator';
import { SUPPORTED_LOCALES } from '../shared/i18n/translate-service';
import { CanvasComponentEvent } from '../shared/canvas-component';
export interface ITimelineParameters {
    border: IBorderParameters;
    font: IFontParameters;
    linePaddingVertical: number;
    nowIndicatorParams: INowIndicatorParameters;
    rowBackground?: string;
    rowBackgroundGradientColorStops?: Array<number | string>;
    rowHeight: number;
    separatorColor: string;
    textColor: string;
    textPaddingHorizontal: number;
}
export interface ITimelineCell {
    text: Konva.Text;
    line: Konva.Line;
    active?: boolean;
}
export interface ITimelineConfig extends ISchedulerComponentConfig, ITimelineParameters {
}
export declare class TimelineWheelEvent extends CanvasComponentEvent {
    evt: WheelEvent;
    constructor(evt: WheelEvent);
}
export declare class TimelineEvents extends SchedulerComponentEvents {
    wheel: TimelineWheelEvent;
}
export declare class Timeline extends SchedulerComponent<TimelineEvents> implements ITimelineConfig {
    canvas: {
        backgroundOfRow0?: Konva.Rect;
        backgroundOfRow1?: Konva.Rect;
        border?: Konva.Line;
        cells?: ITimelineCell[];
        elementsGroup?: Konva.Group;
        mouseCatcher?: Konva.Rect;
    };
    font: IFontParameters;
    linePaddingVertical: number;
    get locale(): SUPPORTED_LOCALES;
    nowIndicator: NowIndicator;
    nowIndicatorParams: INowIndicatorParameters;
    rowHeight: number;
    separatorColor: string;
    textColor: string;
    textPaddingHorizontal: number;
    private _getCell;
    private getZoomParameterIndexForMsPerPx;
    private getZoomParametersForMsPerPx;
    initStore(preloadedState: ISchedulerState): void;
    private _markCellsAsInActive;
    private render;
    private renderTimelineContent;
}
