import { Action } from 'redux';
import { INowIndicatorParameters } from './now-indicator-parameters';
import { IBorderParameters } from '../shared/interfaces/border-parameters';
import { IFontParameters } from '../shared/interfaces/font-parameters';
export declare namespace TimelineState {
    interface State {
        border: IBorderParameters;
        font: IFontParameters;
        linePaddingVertical: number;
        nowIndicatorParams: INowIndicatorParameters;
        rowBackground?: string;
        rowBackgroundGradientColorStops?: Array<number | string>;
        rowHeight: number;
        separatorColor: string;
        textColor: string;
        textPaddingHorizontal: number;
    }
    const getInitialState: (override?: State) => State;
    namespace actionTypes {
        const DUMMY = "[TIMELINE] dummy";
        class DummyAction implements Action {
            readonly type = "[TIMELINE] dummy";
        }
    }
    type actions = actionTypes.DummyAction;
    const reducer: (state: State, _action: actions) => State;
}
