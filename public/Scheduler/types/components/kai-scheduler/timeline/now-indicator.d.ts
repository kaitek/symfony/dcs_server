import Konva from 'konva';
import { ISchedulerComponentConfig, SchedulerComponentEvents, SchedulerComponent } from '../shared/scheduler-component';
import { IVisibleRange } from '../shared/interfaces/visible-range';
import { ISchedulerState } from '../kai-scheduler.store';
export interface INowIndicatorParameters {
    color?: string;
    opacity?: number;
    sizeFactor?: number;
}
export interface INowIndicatorConfig extends ISchedulerComponentConfig, INowIndicatorParameters {
}
export declare class NowIndicatorEvents extends SchedulerComponentEvents {
}
export declare class NowIndicator extends SchedulerComponent<NowIndicatorEvents> implements INowIndicatorConfig {
    canvas: {
        bottomTriangle?: Konva.RegularPolygon;
        line?: Konva.Line;
        topTriangle?: Konva.RegularPolygon;
    };
    private _color;
    get color(): string;
    set color(value: string);
    private _halfTriangleSize;
    get halfTriangleSize(): number;
    set halfTriangleSize(value: number);
    private _height;
    get height(): number;
    set height(value: number);
    leftMargin: number;
    private _lineStrokeSize;
    get lineStrokeSize(): number;
    set lineStrokeSize(value: number);
    msPerPx: number;
    private _opacity;
    get opacity(): number;
    set opacity(value: number);
    private _pivotDateMs;
    get pivotDateMs(): number;
    set pivotDateMs(value: number);
    private _sizeFactor;
    get sizeFactor(): number;
    set sizeFactor(value: number);
    private _strokeSize;
    get strokeSize(): number;
    set strokeSize(value: number);
    visibleRange: IVisibleRange;
    private y;
    constructor(config: INowIndicatorConfig);
    initStore(preloadedState: ISchedulerState): void;
    refreshLineDash(): void;
    refreshLinePoints(): void;
    refreshLineY(): void;
    refreshPosition(): void;
    refreshTriangleRadius(): void;
    render(): void;
}
