export interface TimelineRowZoomParameters {
    interval: number;
    format: string;
    formatObj: any;
}
export interface TimelineZoomParameters {
    msPerPx: number;
    msPerPxMin: number;
    row0: TimelineRowZoomParameters;
    row1: TimelineRowZoomParameters;
}
export declare const ZOOM_CHART_DEFAULTS: TimelineZoomParameters[];
