export interface INowIndicatorParameters {
    color?: string;
    opacity?: number;
    sizeFactor?: number;
}
