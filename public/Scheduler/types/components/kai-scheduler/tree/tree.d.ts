import Konva from 'konva';
import { ISchedulerState } from '../kai-scheduler.store';
import { Resizer, ResizerResizedEvent } from '../resizer/resizer';
import { ResizerState } from '../resizer/resizer.state';
import { KaiSchedulerScroller } from '../scroller/scroller';
import { ScrollerState } from '../scroller/scroller.state';
import { ISchedulerComponentConfig, SchedulerComponent, SchedulerComponentEvent, SchedulerComponentEvents } from '../shared/scheduler-component';
import { KaiSchedulerTreeItem } from './tree-item';
import { IKaiSchedulerTreeItem } from './tree-item.interface';
import { TreeState } from './tree.state';
export interface IKaiSchedulerTreeConfig extends ISchedulerComponentConfig, TreeState.State {
    addBackgroundTo?: Konva.Container;
    resizerParams?: ResizerState.State;
    scrollerParamsVertical?: ScrollerState.State;
}
export declare class TreeItemCanvasPreparedEvent extends SchedulerComponentEvent {
    item: KaiSchedulerTreeItem;
    constructor(item: KaiSchedulerTreeItem);
}
export declare class TreeItemClickEvent extends SchedulerComponentEvent {
    item: KaiSchedulerTreeItem;
    data: IKaiSchedulerTreeItem;
    constructor(item: KaiSchedulerTreeItem, data: IKaiSchedulerTreeItem);
}
export declare class TreePosChangedVerticalEvent extends SchedulerComponentEvent {
    newPos: number;
    constructor(newPos: number);
}
export declare class TreeScrolledEvent extends SchedulerComponentEvent {
    newPos: number;
    constructor(newPos: number);
}
export declare class TreeEvents extends SchedulerComponentEvents {
    itemCanvasPrepared: TreeItemCanvasPreparedEvent;
    itemClick: TreeItemClickEvent;
    posChangedVertical: TreePosChangedVerticalEvent;
    scrolled: TreeScrolledEvent;
}
export declare class Tree<T extends TreeEvents> extends SchedulerComponent<T> implements IKaiSchedulerTreeConfig {
    id: string;
    addTo: Konva.Container;
    addBackgroundTo: Konva.Container;
    get backgroundContainer(): Konva.Container;
    borderColor: string;
    borderSize: number;
    dotButtonSizePx: number;
    fontColor: string;
    fontFamily: string;
    fontSize: number;
    fontStyle: string;
    height: number;
    itemBackgroundWidth: number;
    protected itemCache: {
        [itemId: number]: KaiSchedulerTreeItem;
    };
    get itemContainer(): Konva.Container;
    itemHoverDuration: number;
    itemInitialHeight: number;
    itemTitlePaddingPx: number;
    posChangedVertical: (newPos: number) => void;
    resizerSizePx: number;
    resizerParams?: ResizerState.State;
    rowColors: string[];
    rowHoverColor: string;
    rowRippleColor: string;
    private _scrollSizeValueAtScroller;
    private _scrollSize;
    get scrollSize(): number;
    set scrollSize(value: number);
    stateKey?: string;
    stateKeyResizer?: string;
    stateKeyScrollerVertical?: string;
    toggle: any;
    toggleDurationMs: number;
    collapseIconSizePx: number;
    canvas: {
        innerGroup?: Konva.Group;
        background?: Konva.Rect;
        backgroundGroup?: Konva.Group;
        border?: Konva.Line;
        resizer?: Resizer;
        resizerGroup?: Konva.Group;
        scrollerGroup?: Konva.Group;
    };
    protected _data: IKaiSchedulerTreeItem[];
    getItem(itemId: number): KaiSchedulerTreeItem;
    isTitleVisible: boolean;
    items: KaiSchedulerTreeItem[];
    scrollerVertical: KaiSchedulerScroller;
    get scrollerVerticalWidth(): number;
    width: number;
    widthMin: number;
    widthMax: number;
    y: number;
    constructor(config: IKaiSchedulerTreeConfig);
    cache(): void;
    emitItemCanvasPrepared(item: KaiSchedulerTreeItem): void;
    emitItemClick(item: KaiSchedulerTreeItem, data: IKaiSchedulerTreeItem): void;
    focusToItem(itemToFocus: string | KaiSchedulerTreeItem): void;
    initStore(preloadedState: ISchedulerState): void;
    itemCanvasPrepared(ev: TreeItemCanvasPreparedEvent): void;
    protected _onResized(event: ResizerResizedEvent): void;
    private _posChangedVertical;
    prepareCanvas(): void;
    refreshScrollSize(): void;
    protected _renderItems(): void;
    registerItem(item: KaiSchedulerTreeItem): void;
    setState(state: TreeState.State): void;
}
