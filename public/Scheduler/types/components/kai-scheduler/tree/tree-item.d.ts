import Konva from 'konva';
import { Tree, TreeEvents } from './tree';
import { IKaiSchedulerTreeItem } from './tree-item.interface';
import { ISchedulerComponentConfig, SchedulerComponentEvents, SchedulerComponent } from '../shared/scheduler-component';
import { Vector2d } from 'konva/types/types';
import { ISchedulerState } from '../kai-scheduler.store';
export interface IKaiSchedulerTreeItemConfig extends ISchedulerComponentConfig {
    data: IKaiSchedulerTreeItem;
    initialHeight: number;
    parentItem?: KaiSchedulerTreeItem;
    parentTree: Tree<TreeEvents>;
    subLevel: number;
}
export interface IKaiSchedulerTreeItemCanvas {
    debugText?: Konva.Text;
    backgroundRect?: Konva.Rect;
    groupTasks?: Konva.Group;
    groupTitle?: Konva.Group;
    hoverTween?: Konva.Tween;
    pivotDate?: Date;
    pivotDateMs?: number;
    rippleGroup?: Konva.Group;
    rotateTween?: Konva.Tween;
    titlePaddingPx?: number;
    titleText?: Konva.Text;
    toggleIcon?: Konva.Line;
}
export declare class TreeItemEvents extends SchedulerComponentEvents {
}
export declare class KaiSchedulerTreeItem extends SchedulerComponent<TreeEvents> implements IKaiSchedulerTreeItemConfig {
    private _backgroundColor;
    get backgroundColor(): string;
    set backgroundColor(val: string);
    get backgroundWidth(): number;
    canvas: IKaiSchedulerTreeItemCanvas;
    children: KaiSchedulerTreeItem[];
    data: IKaiSchedulerTreeItem;
    dontAllowSchedule: boolean;
    initialHeight: any;
    private _height;
    get height(): number;
    set height(newHeight: number);
    isOpen: boolean;
    parentItem: KaiSchedulerTreeItem;
    parentTree: Tree<TreeEvents>;
    protected _previousVisibleItems: KaiSchedulerTreeItem[];
    get previousVisibleItems(): KaiSchedulerTreeItem[];
    set previousVisibleItems(val: KaiSchedulerTreeItem[]);
    protected _previousVisibleItemsHeight: number;
    get previousVisibleItemsHeight(): number;
    subLevel: number;
    private get _toggleIconPosX();
    protected _visibleIndex: any;
    get visibleIndex(): number;
    set visibleIndex(val: number);
    visibleHeight: number;
    private _width;
    constructor(config: IKaiSchedulerTreeItemConfig);
    private _calculateWidth;
    destroy(): void;
    initStore(_preloadedState: ISchedulerState): void;
    onParentResize(): void;
    onTitleClick(pos: Vector2d): void;
    prepareCanvas(): void;
    prepareChildren(): void;
    private _refreshBackgroundColor;
    refreshVisibleHeight(): void;
    refreshHeightAndVerticalPos(): void;
    refreshSizes(): void;
    private _refreshPreviousVisibleItemsHeight;
    refreshToggleIconPosX(): void;
    toggle(): void;
}
