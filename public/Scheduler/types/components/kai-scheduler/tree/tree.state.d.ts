import { IStateKey, KeyAction } from '../key-action';
import { IScrollable } from '../scroller/scrollable';
import { ScrollerState } from '../scroller/scroller.state';
import { IKaiSchedulerTreeItem } from './tree-item.interface';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { IBorderParameters } from '../shared/interfaces/border-parameters';
import { IFontParameters } from '../shared/interfaces/font-parameters';
export declare namespace TreeState {
    interface State extends IStateKey, IScrollable {
        stateKeyResizer?: string;
        stateKeyScrollerVertical?: string;
        border?: IBorderParameters;
        collapseIconSizePx?: number;
        data?: IKaiSchedulerTreeItem[];
        font?: IFontParameters;
        height?: number;
        itemBackgroundWidth?: number;
        itemHoverDuration?: number;
        itemInitialHeight?: number;
        itemTitlePaddingPx?: number;
        rowColors?: string[];
        rowHoverColor?: string;
        rowRippleColor?: string;
        scrollerVerticalPos?: number;
        width?: number;
        widthMax?: number;
        widthMin?: number;
        x?: number;
        y?: number;
    }
    interface States {
        [stateKey: string]: State;
    }
    const INITIAL_TREE_STATE: TreeState.State;
    const getInitialState: (override: State) => {
        tree: State;
        scrollerHorizontal: ScrollerState.State;
        scrollerVertical: ScrollerState.State;
    };
    namespace actionTypes {
        enum names {
            SET_STATE = "[TREE] set state",
            ADD_NEW = "[TREE] add new tree"
        }
        class SetStateAction implements KeyAction {
            stateKey: string;
            state: State;
            readonly type = names.SET_STATE;
            constructor(stateKey: string, state: State);
        }
        class AddNewTreeAction implements KeyAction {
            stateKey: string;
            override: State;
            readonly type = names.ADD_NEW;
            constructor(stateKey: string, override?: State);
        }
        const contains: (action: any) => action is actions;
    }
    type actions = actionTypes.SetStateAction | actionTypes.AddNewTreeAction;
    const itemReducer: (state: State, action: actions) => State;
    const reducer: (states: States, action: actions) => States;
    const beforeAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const afterAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const setState: (stateKey: string, state: State) => actionTypes.SetStateAction;
}
