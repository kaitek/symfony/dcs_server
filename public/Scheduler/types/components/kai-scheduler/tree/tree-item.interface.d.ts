export interface IKaiSchedulerTreeItem {
    id: any;
    title: string;
    children?: IKaiSchedulerTreeItem[];
    dontAllowSchedule?: boolean;
}
