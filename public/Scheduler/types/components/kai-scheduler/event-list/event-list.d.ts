import Konva from 'konva';
import { CanvasTable, CanvasTableLongPressEvent } from '../canvas-table/canvas-table';
import { EventListState } from './event-list.state';
import { CanvasTableState } from '../canvas-table/canvas-table.state';
import { ResizerState } from '../resizer/resizer.state';
import { ISchedulerComponentConfig, SchedulerComponentEvent, SchedulerComponentEvents, SchedulerComponent } from '../shared/scheduler-component';
import { Resizer } from '../resizer/resizer';
import { ISchedulerState } from '../kai-scheduler.store';
import { TargetPicker } from '../target-picker/target-picker';
export interface IEventListConfig extends ISchedulerComponentConfig, EventListState.State {
    tableParams?: CanvasTableState.State;
    resizerParams?: ResizerState.State;
}
export declare class EventListTargetPickerActivatedEvent extends SchedulerComponentEvent {
}
export declare class EventListEvents extends SchedulerComponentEvents {
    longpress: CanvasTableLongPressEvent;
    targetPickerActivated: EventListTargetPickerActivatedEvent;
}
export declare class EventList extends SchedulerComponent<EventListEvents> implements IEventListConfig {
    resizerParams?: ResizerState.State;
    backgroundColor: string;
    canvas: {
        border?: Konva.Line;
        resizer?: Resizer;
        targetPickerGroup?: Konva.Group;
    };
    parentHeight: number;
    parentWidth: number;
    stateKeyResizer: string;
    stateKeyTable: string;
    stateKeyScrollerHorizontal: string;
    stateKeyScrollerVertical: string;
    table: CanvasTable;
    targetPicker: TargetPicker;
    targets: any[];
    constructor(config: IEventListConfig);
    initStore(preloadedState: ISchedulerState): void;
    protected emitLongpress(rowIndex: number, rowData: any): void;
    protected emitTargetPickerActivated(): void;
    filter(text: string): void;
    prepareCanvas(): void;
    refreshFilter(): void;
    reset(): void;
}
