import { CanvasTableState } from '../canvas-table/canvas-table.state';
import { ResizerState } from '../resizer/resizer.state';
import { ISchedulerState, SchedulerAction } from '../kai-scheduler.store';
import { ScrollerState } from '../scroller/scroller.state';
import { DataState } from '../event/data.state';
import { ICanvasTableColumn } from '../canvas-table/canvas-table.interfaces';
export declare namespace EventListState {
    interface State {
        stateKeyResizer?: string;
        stateKeyTable?: string;
    }
    const EVENT_LIST_STATE_KEY = "eventList";
    const TABLE_STATE_KEY: string;
    const getInitialState: (height: number, data: DataState.IEventStates, width: number, columns: ICanvasTableColumn[], collapsed: boolean) => {
        eventList: State;
        table: CanvasTableState.State;
        resizer: ResizerState.State;
        scrollerHorizontal: ScrollerState.State;
        scrollerVertical: ScrollerState.State;
    };
    namespace actionTypes {
        enum names {
            SET_STATE = "[EVENT_LIST] set state"
        }
        class SetStateAction {
            state: State;
            readonly type = names.SET_STATE;
            constructor(state: State);
        }
        const contains: (action: any) => action is SetStateAction;
    }
    type actions = actionTypes.SetStateAction;
    const reducer: (state: State, action: actions) => State;
    const afterAction: (state: ISchedulerState, action: SchedulerAction) => ISchedulerState;
    const setState: (state: State) => actionTypes.SetStateAction;
    const refreshFilter: (stateKeyTable: string) => CanvasTableState.actionTypes.RefreshFilterAction;
}
