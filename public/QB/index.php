<?php
function get_web_page($url)
{
    $options = array(
      CURLOPT_RETURNTRANSFER => true,     // return web page
      CURLOPT_HEADER         => false,    // don't return headers
      CURLOPT_FOLLOWLOCATION => true,     // follow redirects
      CURLOPT_ENCODING       => "",       // handle all encodings
      CURLOPT_USERAGENT      => "spider", // who am i
      CURLOPT_AUTOREFERER    => true,     // set referer on redirect
      CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
      CURLOPT_TIMEOUT        => 120,      // timeout on response
      CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
      CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
      CURLOPT_SSL_VERIFYHOST => false
    );

    $headers = [
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'content-length: 0',
        ];

    $ch      = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $err     = curl_errno($ch);
    $errmsg  = curl_error($ch);
    $header  = curl_getinfo($ch);
    curl_close($ch);

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}
$fault_types = '[{"id":"ARIZA KALIP-APARAT","name":"ARIZA KALIP-APARAT"},{"id":"ARIZA MAKİNE ELEKTRİK","name":"ARIZA MAKİNE ELEKTRİK"},{"id":"ARIZA MAKİNE MEKANİK","name":"ARIZA MAKİNE MEKANİK"},{"id":"ARIZA ÜRETİM","name":"ARIZA ÜRETİM"}]';
$ret = get_web_page("https://" . $_SERVER['HTTP_HOST'] . $_SERVER['BASE'] . "/tr/JobRotation/getAllData");
$jr = '[{"id":"07:30-15:30","name":"07:30-15:30"},{"id":"15:30-23:30","name":"15:30-23:30"},{"id":"23:30-07:30","name":"23:30-07:30"}]';
$ca = '[{"id":"GÖREVE KATILMA","name":"GÖREVE KATILMA"},{"id":"GÖREVİ BİTİRME","name":"GÖREVİ BİTİRME"},{"id":"KALİTE ONAY","name":"KALİTE ONAY"},{"id":"KALİTE ONAY İŞ SONU","name":"KALİTE ONAY İŞ SONU"},{"id":"OTONOM KONTROL","name":"OTONOM KONTROL"},{"id":"SETUP-AYAR","name":"SETUP-AYAR"},{"id":"SOP","name":"SOP"}]';
$lgd = '[{"id":"zorunlu","name":"Zorunlu"},{"id":"zorunsuz","name":"Zorunsuz"}]';
if ($ret['errno'] != 0) {
    var_dump($ret);
} else {
    $jr = $ret['content'];
}
$report = $_GET['report'];
$route = 'Reports';
$rules = "{condition: 'AND',rules: []}";
switch ($report) {
    case 'client-production-signals':{
      $title = 'İstasyon Sinyal Raporu';
      $filters = '[
        {"id":"client","field":"client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
      ]';
        /*
        ,{"id":"mould","field":"mould","label":"Kalıp Kodu","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
            if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
              return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
            }
          }}
        */
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
      break;
    }
    case 'client-product-analyze':{
        $title = 'İstasyon Üretim Analizi Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cpd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cpd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        /*
        ,{"id":"mould","field":"cpd.mould","label":"Kalıp Kodu","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
            if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
              return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
            }
          }}
        */
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'productiondetail-client':{
        $title = 'İstasyon Üretim Detay Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cpd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cpd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
        {
          condition: 'AND',
          rules: [{
            id: 'day',
            operator: 'between',
            flags: {
              filter_readonly: true,
              no_delete: true
            }
          }]
        }";
        //valueSetter: function(rule, value) {
        //   rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
        //}
        break;
    }
    case 'productiondetail-client-total':{
        $title = 'İstasyon Üretim Toplam Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cpd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cpd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        //valueSetter: function(rule, value) {
        //   rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
        //}
        break;
    }
    case 'productiondetail-total':{
        $title = 'Üretim Toplam Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cpd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        //valueSetter: function(rule, value) {
        //   rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
        //}
        break;
    }
    case 'productiondetail-employee':{
        $title = 'Operatör Üretim Detay Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cpd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"cpd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cpd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'productiondetail-employee-total':{
        $title = 'Operatör Üretim Toplam Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cpd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"cpd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cpd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'client-work-time':{
        $title = 'İstasyon Toplam Çalışma Süresi Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'cnc-work-time':{
        $title = 'CNC Toplam Çalışma Süresi Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'cnc-full-time':{
        $title = 'CNC Çalışma-Kayıp Toplam Raporu';
        $filters = '[
        {"id":"client","field":"cnc.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cnc.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cnc.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'cnc-full-time-detail':{
        $title = 'CNC Çalışma-Kayıp Detay Raporu';
        $filters = '[
        {"id":"client","field":"cnc.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cnc.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cnc.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'employee-efficiency-1':{
        $title = 'Günlük Operatör Verim Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date",operators: ["equal","between"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"employee","field":"cpd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,operators: ["equal"],"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cpd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'lostdetail-client':{
        $title = 'İstasyon Kayıp Detay Raporu';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"losttype","field":"cld.losttype","label":"Kayıp Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'lostdetail-client-group':{
        $title = 'İstasyon Kayıp Detay Raporu (Grup)';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"losttype","field":"cld.losttype","label":"Kayıp Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'lostdetail-client-total':{
        $title = 'İstasyon Kayıp Toplam Raporu';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"losttype","field":"cld.losttype","label":"Kayıp Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'lostdetail-employee':{
        $title = 'Operatör Kayıp Detay Raporu';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"losttype","field":"cld.losttype","label":"Kayıp Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"lostgroupcode","field":"lgd.lostgroupcode","label":"Kayıp Grubu","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"id","labelField":"name","searchField":"name","sortField":"name","options":' . $lgd . '}}
        ,{"id":"employee","field":"cld.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'lostdetail-employee-total':{
        $title = 'Operatör Kayıp Toplam Raporu';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"losttype","field":"cld.losttype","label":"Kayıp Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"cld.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'lostdetail-client-task':{
        $title = 'İstasyon Görev Kayıp Raporu';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cld.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"losttype","field":"cld.losttype","label":"Kayıp Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cld.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'lostdetail-employee-task':{
        $title = 'Operatör Görev Kayıp Raporu';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cld.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"losttype","field":"cld.losttype","label":"Kayıp Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"cld.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'task-reject-detail-client':{
        $title = 'İstasyon Iskarta Detay Raporu';
        $filters = '[
        {"id":"client","field":"trd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"trd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"trd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"trd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"rejecttype","field":"trd.rejecttype","label":"Iskarta Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"trd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"trd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'task-reject-detail-employee':{
        $title = 'Operatör Iskarta Detay Raporu';
        $filters = '[
        {"id":"client","field":"trd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"trd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"trd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"trd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"rejecttype","field":"trd.rejecttype","label":"Iskarta Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"trd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"trd.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'mh-gec-tasima':{
        $title = 'Geciken Taşıma (Hedef:Üretim)';
        $filters = '[
        {"id":"client","field":"cm.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cm.starttime","label":"Başlama Tarihi","type":"date",operators: ["between","equal"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"erprefnumber","field":"cm.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"locationsource","field":"cm.locationsource","label":"LOT/Seri","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'mh-gec-tasima-client':{
        $title = 'Geciken Taşıma (Kaynak:Üretim)';
        $filters = '[
        {"id":"client","field":"cm.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cm.starttime","label":"Başlama Tarihi","type":"date",operators: ["between","equal"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"erprefnumber","field":"cm.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"locationsource","field":"cm.locationsource","label":"LOT/Seri","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'mh-performans':{
        $title = 'Lojistik Personel Performans';
        $filters = '[
        {"id":"day","field":"t1","label":"Atama Tarihi","type":"date",operators: ["between","equal"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'mh-client-task-cases':{
        $title = 'Hat Başı Stok Raporu';
        $filters = '[
        {"id":"client","field":"tc.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"opname","field":"tc.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        //valueSetter: function(rule, value) {
        //   rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
        //}
        break;
    }
    case 'mh-carrier-time':{
        $title = 'Taşıyıcı Görev Süreleri';
        $filters = '[
        {"id":"t1","field":"t1","label":"Bitiş Tarihi","type":"date",operators: ["between","equal"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 't1',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'mh-employee-time':{
        $title = 'Sicil Görev Süreleri';
        $filters = '[
        {"id":"t1","field":"t1","label":"Bitiş Tarihi","type":"date",operators: ["between","equal"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 't1',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'mh-silinen-etiketler':{
        $title = 'İptal Edilen Etiketler';
        $filters = '[
        {"id":"t1","field":"cl.canceltime","label":"İptal Tarihi","type":"date",operators: ["between","equal"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 't1',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'mh-hata-listesi':{
        $title = 'Hata Listesi';
        $filters = '[
        {"id":"t1","field":"time","label":"İptal Tarihi","type":"date",operators: ["between","equal"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"message","field":"message","label":"Hata İçeriği","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 't1',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'oa-list':{
        $title = 'Yetkinlik Listesi';
        $filters = '[
        {"id":"opname","field":"oa.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"oa.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'opname',
          operator: 'between',
          flags: {
            //no_delete: true
          }
        },{
          id: 'employee',
          operator: 'between',
          flags: {
            //no_delete: true
          }
        }]
      }";
        break;
    }
    case 'oa-history':{
        $title = 'Yetkinlik Geçmişi';
        $filters = '[
        {"id":"opname","field":"oah.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"oah.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'opname',
          operator: 'between',
          flags: {
            //no_delete: true
          }
        },{
          id: 'employee',
          operator: 'between',
          flags: {
            //no_delete: true
          }
        }]
      }";
        break;
    }
    case 'oa-log':{
        $title = 'Yetkinlik Sistem Kayıtları';
        $filters = '[
      {"id":"client","field":"oadl.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"opname","field":"oadl.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"oadl.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'client',
          operator: 'between',
          flags: {
            //no_delete: true
          }
        },{
          id: 'opname',
          operator: 'between',
          flags: {
            //no_delete: true
          }
        },{
          id: 'employee',
          operator: 'between',
          flags: {
            //no_delete: true
          }
        }]
      }";
        break;
    }
    case 'oa-require-list':{
        $title = 'Yetkinlik Gereklilik Listesi';
        $filters = '[
        {"id":"opname","field":"tlop.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"e.code","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        break;
    }
    case 'oa-forward-list':{
        $title = 'Yetkinlik İlerleme Listesi';
        $filters = '[
      {"id":"opname","field":"oa.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"e.code","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        break;
    }
    case 'work-out-system':{
        $title = 'Sistem Dışı Çalışmalar';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        /*,{"id":"opnames","field":"cpd.opnames","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}*/
        ,{"id":"employee","field":"cpd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'work-out-system-summary':{
        $title = 'Sistem Dışı Çalışma Özeti';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        /*,{"id":"employee","field":"cpd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}*/
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'pres-setup-detail':{
        $title = 'Ayar Süreleri';
        $filters = '[
        {"id":"client","field":"client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opdescription","field":"opdescription","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        /*,{"id":"employee","field":"employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}*/
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'fault-list':{
        $title = 'Arıza Listesi';
        $filters = '[
        {"id":"client","field":"client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"losttype","field":"losttype","label":"Arıza Tipi","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $fault_types . '}}
        ,{"id":"start","field":"start","label":"Başlangıç","type":"date",operators: ["between"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'start',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'kanban-full':{
        $title = 'Dolu Kanban Kartları';
        $filters = '[
        {"id":"client","field":"client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"product","field":"product","label":"Mamül","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        break;
    }
    case 'kanban-over-log':{
        $title = 'Kanban Log Kayıtları';
        $filters = '[
        {"id":"client","field":"client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"product","field":"product","label":"Mamül","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        break;
    }
    case 'control-answers':{
        $title = 'Kontrol Cevapları';
        $filters = '[
        {"id":"client","field":"ca.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"ca.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"ca.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"ca.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"type","field":"ca.type","label":"Tip","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $ca . '}}
        ,{"id":"employee","field":"ca.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'fault-sessions':{
        $title = 'Arıza Listesi';
        $filters = '[
        {"id":"client","field":"fs.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"losttype","field":"fs.losttype","label":"Arıza Tipi","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"faulttype","field":"fs.faulttype","label":"Arıza Kodu","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"faultdevice","field":"fs.faultdevice","label":"Arızalı Ekipman","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"fs.employee","label":"Bildiren Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"ef.name","label":"Bildiren Personel","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee2","field":"fs.employeeintervention","label":"Müdahale Eden Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name2","field":"efi.name","label":"Müdahale Eden Personel","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee3","field":"fs.employeeinterventionconfirm","label":"Onaylayan Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name3","field":"efc.name","label":"Onaylayan Personel","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"start","field":"fs.start","label":"Arıza Başlangıç","type":"date",operators: ["between"],"validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ]';
        // $rules = "
        // {
        //   condition: 'AND',
        //   rules: [{
        //     id: 'day',
        //     operator: 'between',
        //     flags: {
        //       filter_readonly: true,
        //       no_delete: true
        //     }
        //   }]
        // }";
        break;
    }
    case 'remote-quality-reject-list':{
        $title = 'Kalite Red Listesi';
        $filters = '[
        {"id":"client","field":"cld.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cld.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cld.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cld.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"cld.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"cld.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'taskplan-employee':{
        $title = 'Operatör Çalışma Planı Raporu';
        $filters = '[
        {"id":"client","field":"tpe.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"tpe.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        //$rules = "
        //{
        //  condition: 'AND',
        //  rules: [{
        //    id: 'day',
        //    operator: 'between',
        //    flags: {
        //      filter_readonly: true,
        //      no_delete: true
        //    }
        //  }]
        //}";
        break;
    }
    case 'taskplan-reality':{
        $title = 'Planlanan-Gerçekleşen Raporu';
        $filters = '[
        {"id":"client","field":"client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"deadline","field":"deadline","label":"Vade Tarihi","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"plannedstart","field":"plannedstart","label":"Planlı Başlangıç","type":"date","validation":{"format":"YYYY-MM-DD HH:mm:ss"},"plugin":"datetimepicker","plugin_config":{"format":"YYYY-MM-DD HH:mm:ss","locale":"tr","widgetPositioning": { "vertical":"bottom" }}}
        ,{"id":"opname","field":"opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        //$rules = "
        //{
        //  condition: 'AND',
        //  rules: [{
        //    id: 'plannedstart',
        //    operator: 'between',
        //    flags: {
        //      filter_readonly: true,
        //      no_delete: true
        //    }
        //  }]
        //}";
        break;
    }
    case 'task_sessions_logs':{
        $title = 'Üretim Oturumları Raporu';
        $filters = '[
        {"id":"client","field":"client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"deadline","field":"deadline","label":"Vade Tarihi","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"plannedstart","field":"plannedstart","label":"Planlı Başlangıç","type":"date","validation":{"format":"YYYY-MM-DD HH:mm:ss"},"plugin":"datetimepicker","plugin_config":{"format":"YYYY-MM-DD HH:mm:ss","locale":"tr","widgetPositioning": { "vertical":"bottom" }}}
        ,{"id":"opname","field":"opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"erprefnumber","field":"erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        //$rules = "
        //{
        //  condition: 'AND',
        //  rules: [{
        //    id: 'plannedstart',
        //    operator: 'between',
        //    flags: {
        //      filter_readonly: true,
        //      no_delete: true
        //    }
        //  }]
        //}";
        break;
    }
    case 'task_cases_logs':{
        $title = 'Kasa Hareketleri Raporu';
        $filters = '[
        {"id":"erprefnumber","field":"erprefnumber","label":"Erp Ref Number",operators: ["equal"],"value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false}}
      ]';
        $rules = "
        {
          condition: 'AND',
          rules: [{
            id: 'erprefnumber',
            operator: 'equal',
            flags: {
              filter_readonly: true,
              no_delete: true
            }
            }]
        }";
        break;
    }
    case 'task_cases_logs_delivery':{
        $title = 'Tesellüm Hareketleri Raporu';
        $filters = '[
        {"id":"erprefnumber","field":"erprefnumber","label":"Erp Ref Number",operators: ["equal"],"value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false}}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'erprefnumber',
          operator: 'equal',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
          }]
      }";
        break;
    }
    case 'reworks_working':{
        $title = 'Yeniden İşlem Kontrolleri Raporu';
        $filters = '[
        {"id":"client","field":"cpd.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"day","field":"cpd.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
        ,{"id":"jobrotation","field":"cpd.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
        ,{"id":"opname","field":"cpd.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"employee","field":"cpd.employee","label":"Sicil No","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
        ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
          if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
            return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
          }
        }}
      ]';
        $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
        break;
    }
    case 'delivery_logs':{
      $title = 'Tesellüm Listesi Raporu';
        $filters = '[
        {"id":"time","field":"time","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ]';
        $rules = "
        {
          condition: 'AND',
          rules: [{
            id: 'time',
            operator: 'equal',
            flags: {
              filter_readonly: true,
              no_delete: true
            }
            }]
        }";
      break;
    }
    case 'document_view_logs':{
      $title = 'SOP Eğitimleri';
      $filters = '[
      {"id":"client","field":"dvl.client","label":"İstasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
        if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
          return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
        }
      }}
      ,{"id":"day","field":"dvl.day","label":"Gün","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":true,"autoclose":true,"language":"tr"}}
      ,{"id":"jobrotation","field":"dvl.jobrotation","label":"Vardiya","type":"string","input":"select","multiple":true,"plugin":"selectize","operators":["in","not_in","is_empty","is_not_empty","is_null","is_not_null"],"plugin_config":{"valueField":"name","labelField":"name","searchField":"name","sortField":"name","options":' . $jr . '}}
      ,{"id":"opname","field":"dvl.opname","label":"Operasyon","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
        if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
          return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
        }
      }}
      ,{"id":"erprefnumber","field":"dvl.erprefnumber","label":"Erp Ref Number","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
        if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
          return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
        }
      }}
      ,{"id":"employee","field":"dvl.employee","label":"Sicil No","value_separator":",","type":"string","size":30,operators: ["equal"],"validation":{"allow_empty_value":false},description: function(rule) {
        if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
          return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
        }
      }}
      ,{"id":"name","field":"e.name","label":"Operatör","value_separator":",","type":"string","size":30,"validation":{"allow_empty_value":false},description: function(rule) {
        if (rule.operator && [\'in\', \'not_in\'].indexOf(rule.operator.type) !== -1) {
          return \'"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz\';
        }
      }}
    ]';
      $rules = "
      {
        condition: 'AND',
        rules: [{
          id: 'day',
          operator: 'between',
          flags: {
            filter_readonly: true,
            no_delete: true
          }
        }]
      }";
      //valueSetter: function(rule, value) {
      //   rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
      //}
      break;
  }
    default:{
        echo "Rapor belirtilmemiş!";
        die;
        break;
    }
}
//$filters = json_encode($filters);
?>
<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	  <title>DCS Server Report Viewer</title>

	  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" id="bt-theme">
	  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
	  <link rel="stylesheet" href="bower_components/bootstrap-select/dist/css/bootstrap-select.min.css">
	  <link rel="stylesheet" href="bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css">
	  <link rel="stylesheet" href="bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css">
	  <link rel="stylesheet" href="bower_components/selectize/dist/css/selectize.bootstrap3.css">

	  <link rel="stylesheet" href="dist/css/query-builder.default.css" id="qb-theme">

	  <style>
      .flag {
        display: inline-block;
      }
	  </style>
	</head>

	<body>
    <div class="container">
		  <div class="col-md-12 col-lg-10 col-lg-offset-1">
        <div class="page-header">
            <h1><?=$title?>
            </h1>
        </div>

        <div class="btn-group">
          <button class="btn btn-danger reset">Temizle</button>
        </div>
		
        <div class="btn-group">
          <!--<button class="btn btn-default" disabled>Get:</button>
          <button class="btn btn-primary parse-json">Getir J</button>
          <button class="btn btn-primary parse-sql" data-stmt="false">SQL</button>
          <button class="btn btn-primary parse-sql" data-stmt="question_mark">SQL ?</button>-->
          <button class="btn btn-primary parse-sql" data-stmt="named">Getir</button>
          <!--<button class="btn btn-primary parse-mongo">MongoDB</button>-->
        </div>
        <div id="builder"></div>
        <div id="result" class="hide">
          <h3>Output</h3>
          <pre></pre>
        </div>
		  </div>
		</div>
    
		<!--<script src="bower_components/jquery/dist/jquery.min.js"></script>-->
		<script src="../lib/jquery/jquery-2.1.4.min.js"></script>
		<script src="../lib/jquery/jquery.binarytransport.js"></script>
		<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="bower_components/moment/min/moment.min.js"></script>
		<script src="bower_components/moment/locale/tr.js"></script>
		<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
		<script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.tr.min.js" charset="UTF-8"></script>
    <script src="bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
		<script src="bower_components/bootbox/bootbox.js"></script>
		<script src="bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
		<script src="bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
		<script src="bower_components/jquery-extendext/jQuery.extendext.min.js"></script>
		<script src="bower_components/sql-parser/browser/sql-parser.js"></script>
		<script src="bower_components/doT/doT.js"></script>
		<script src="bower_components/interactjs/dist/interact.min.js"></script>

		<script src="dist/js/query-builder.min.js?id=1"></script>
    <script src="dist/i18n/query-builder.tr.js"></script>
		<script>
      function loadframe(e){
        try {
          //debugger
          //$('.parse-sql').prop('disabled',false);
          if($(e).contents()[0].body.innerText!==""){
            var ret=JSON.parse($(e).contents()[0].body.innerText);
            alert(ret.message);
          }
        } catch (error) {
          alert("Hata Oluştu.");
        }
      }
      $("body").append('<iframe class="ifrm" id="ifrm" onload="loadframe(this)" style="display:none;left:10px;position:relative;z-index:1;"></iframe>');
		  $('[data-toggle="tooltip"]').tooltip();
      var $b = $('#builder');

		  var options = {
			  allow_empty: true,
        sort_filters: true,
        plugins: {
          'bt-tooltip-errors': { delay: 100 },
          'sortable': null,
          'filter-description': { mode: 'bootbox' },
          'bt-selectpicker': null,
          'unique-filter': null,
          'bt-checkbox': { color: 'primary' },
          'invert': null,
          'not-group': null
        },

        // standard operators in custom optgroups
        operators: [
          { type: 'equal', optgroup: 'basit' },
          { type: 'not_equal', optgroup: 'basit' },
          { type: 'in', optgroup: 'basit' },
          { type: 'not_in', optgroup: 'basit' },
          { type: 'less', optgroup: 'sayı' },
          { type: 'less_or_equal', optgroup: 'sayı' },
          { type: 'greater', optgroup: 'sayı' },
          { type: 'greater_or_equal', optgroup: 'sayı' },
          { type: 'between', optgroup: 'sayı' },
          { type: 'not_between', optgroup: 'sayı' },
          { type: 'begins_with', optgroup: 'karakter' },
          { type: 'not_begins_with', optgroup: 'karakter' },
          { type: 'contains', optgroup: 'karakter' },
          { type: 'not_contains', optgroup: 'karakter' },
          { type: 'ends_with', optgroup: 'karakter' },
          { type: 'not_ends_with', optgroup: 'karakter' },
          { type: 'is_empty' },
          { type: 'is_not_empty' },
          { type: 'is_null' },
          { type: 'is_not_null' }
        ],

			  filters: <?=$filters?>
        ,rules: <?=$rules?>
		  };

		  // init
		  $('#builder').queryBuilder(options);
		  
		  $('#builder').on('afterCreateRuleInput.queryBuilder', function(e, rule) {
        if (rule.filter.plugin == 'selectize') {
          rule.$el.find('.rule-value-container').css('min-width', '200px')
          .find('.selectize-control').removeClass('form-control');
        }
        if (rule.filter.plugin === 'datetimepicker') {
          var $input = rule.$el.find('.rule-value-container [name*=_value_]');
          $input.on('dp.change', function() {
            $input.trigger('change');
          });
        }
		  });
		  // reset builder
		  $('.reset').on('click', function() {
        $('#builder').queryBuilder('reset');
        $('#result').addClass('hide').find('pre').empty();
        if(typeof options.rules=='object'){
          $('#builder').queryBuilder('setRules', options.rules);
        }
		  });

      $('.parse-json').on('click', function() {
        var res = $('#builder').queryBuilder('getRules', {
            get_flags: true,
            skip_empty: true
        });
        $('#result').removeClass('hide')
            .find('pre').html(JSON.stringify(res, undefined, 2));
        $.ajax({

            // The URL for the request
            url: "../tr/<?=$route?>",

            // The data to send (will be converted to a query string)
            data: JSON.stringify(res),

            // Whether this is a POST or GET request
            type: "VIEW",

            // The type of data we expect back
            //dataType : "json",
        })
        // Code to run if the request succeeds (is done);
        // The response is passed to the function
            .done(function( json ) {
                //$( "<h1>" ).text( json.title ).appendTo( "body" );
                //$( "<div class=\"content\">").html( json.html ).appendTo( "body" );
                console.log(json);
            })
            // Code to run if the request fails; the raw request and
            // status codes are passed to the function
            .fail(function( xhr, status, errorThrown ) {
                console.log( "Error: " + errorThrown );
                console.log( "Status: " + status );
                console.dir( xhr );
            });
      });

      $('.parse-sql').on('click', function() {
			  var res = $('#builder').queryBuilder('getSQL', $(this).data('stmt'), false);
        //$('#result').removeClass('hide')
        //  .find('pre').html(
        //  res.sql + (res.params ? '\n\n' + JSON.stringify(res.params, undefined, 2) : '')
        //);
        if(res){
          $('.parse-sql').prop('disabled',true);
          var url='../tr/<?=$route?>?sql='+encodeURIComponent(res.sql)+'&params='+encodeURIComponent(JSON.stringify(res.params))+'&report=<?=$report?>'+'&title='+encodeURIComponent('<?=$title?>');
          //console.log("wait");
          // $("#ifrm")[0].src=url;
          $.ajax({
            url: url,
            method: "POST",
            headers: { 
              "Content-Type": "application/json",
              "X-HTTP-Method-Override": "GET"
            },
            data: JSON.stringify({test:'test-1'}),
            dataType: 'binary',
            processData: false,
            success: function(data, textStatus, xhr) {
              //t.activateTabGrid();
              if(textStatus==='success'){
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                  var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                  var matches = filenameRegex.exec(disposition);
                  if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                }
                var type = xhr.getResponseHeader('Content-Type');
                var blob = typeof File === 'function'
                  ? new File([data], filename, { type: type })
                  : new Blob([data], { type: type });
                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                  // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                  window.navigator.msSaveBlob(blob, filename);
                } else {
                  var URL = window.URL || window.webkitURL;
                  var downloadUrl = URL.createObjectURL(blob);
                  if (filename) {
                    // use HTML5 a[download] attribute to specify filename
                    var a = document.createElement("a");
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                      window.location = downloadUrl;
                    } else {
                      a.href = downloadUrl;
                      a.download = filename;
                      document.body.appendChild(a);
                      a.click();
                    }
                  } else {
                    window.location = downloadUrl;
                  }
                  setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                }
              }
            },
            error: function(xhr, textStatus, errorThrown) {
              if(xhr.responseJSON&&xhr.responseJSON.message){
                console.log(xhr.responseJSON.message);
                // mj.message({
                //   title: mj.lng.glb.error,
                //   msg: xhr.responseJSON.message,
                //   type: "error",
                //   html: true
                // });
              }
            },
            complete: function(jqXHR, textStatus) {
              //console.log('complete');
              $('.parse-sql').prop('disabled',false);
            }
          });
          //$("body").append('<iframe class="ifrm" id="ifrm" onload="loadframe(this)" src="'+url+'" style="display:none;left:10px;position:relative;z-index:1;"></iframe>');
        }
		  });            
		</script>
    <!--<iframe class="ifrm" id="ifrm" onload="loadframe(this)" src="" style="display:none;left:10px;position:relative;z-index:1;"></iframe>-->
	</body>
</html>
<?php
/*
[
  {
  id: 'name',
  field: 'username',
  label: {
    en: 'Name',
    tr: 'Nom'
  },
  value_separator: ',',
  type: 'string',
  optgroup: 'core',
  default_value: 'Mistic',
  size: 30,
  validation: {
    allow_empty_value: true
  },
  unique: true
  },
  {
  id: 'age',
  label: 'Age',
  type: 'integer',
  input: 'text',
  value_separator: '|',
  optgroup: 'core',
  description: function(rule) {
    if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
      return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde | kullanabilirsiniz';
    }
  }
  },
  {
  id: 'bson',
  label: 'BSON',
  type: 'string',
  input: 'textarea',
  operators: ['equal'],
  size: 30,
  rows: 3
  },
  {
  id: 'category',
  label: 'Category',
  type: 'integer',
  input: 'checkbox',
  optgroup: 'core',
  values: {
    1: 'Books',
    2: 'Movies',
    3: 'Music',
    4: 'Tools',
    5: 'Goodies',
    6: 'Clothes'
  },
  colors: {
    1: 'foo',
    2: 'warning',
    5: 'success'
  },
  operators: ['in', 'not_in', 'equal', 'not_equal', 'is_null', 'is_not_null']
  },
  {
  id: 'continent',
  label: 'Continent',
  type: 'string',
  input: 'select',
  optgroup: 'core',
  placeholder: 'Select something',
  values: {
    'eur': 'Europe',
    'asia': 'Asia',
    'oce': 'Oceania',
    'afr': 'Africa',
    'na': 'North America',
    'sa': 'South America'
  },
  operators: ['equal', 'not_equal', 'is_null', 'is_not_null']
  },
  {
  id: 'state',
  label: 'State',
  type: 'string',
  input: 'select',
  multiple: true,
  plugin: 'selectize',
  plugin_config: {
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    sortField: 'name',
    options: [
      { id: "AL", name: "Alabama" },
      { id: "AK", name: "Alaska" },
      { id: "AZ", name: "Arizona" },
      { id: "AR", name: "Arkansas" },
      { id: "CA", name: "California" },
      { id: "CO", name: "Colorado" },
      { id: "CT", name: "Connecticut" },
      { id: "DE", name: "Delaware" },
      { id: "DC", name: "District of Columbia" },
      { id: "FL", name: "Florida" },
      { id: "GA", name: "Georgia" },
      { id: "HI", name: "Hawaii" },
      { id: "ID", name: "Idaho" }
    ]
  },
  operators: ['in', 'not_in', 'is_empty', 'is_not_empty', 'is_null', 'is_not_null'],
  valueSetter: function(rule, value) {
    rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
  }
  },
  {
  id: 'in_stock',
  label: 'In stock',
  type: 'integer',
  input: 'radio',
  optgroup: 'plugin',
  values: {
    1: 'Yes',
    0: 'No'
  },
  operators: ['equal']
  },
  {
  id: 'price',
  label: 'Price',
  type: 'double',
  size: 5,
  validation: {
    min: 0,
    step: 0.01
  },
  data: {
    class: 'com.example.PriceTag'
  }
  },
  {
  id: 'rate',
  label: 'Rate',
  type: 'integer',
  validation: {
    min: 0,
    max: 100
  },
  plugin: 'slider',
  plugin_config: {
    min: 0,
    max: 100,
    value: 0
  },
  onAfterSetValue: function(rule, value) {
    var input = rule.$el.find('.rule-value-container input');
    input.slider('setValue', value);
    input.val(value); // don't know why I need it
  }
  },
  {
  id: 'id',
  label: 'Identifier',
  type: 'string',
  optgroup: 'plugin',
  placeholder: '____-____-____',
  size: 14,
  operators: ['equal', 'not_equal'],
  validation: {
    format: /^.{4}-.{4}-.{4}$/,
    messages: {
      format: 'Invalid format, expected: AAAA-AAAA-AAAA'
    }
  }
  },
  {
  id: 'coord',
  label: 'Coordinates',
  type: 'string',
  default_value: 'C.5',
  description: 'The letter is the cadran identifier:\
  <ul>\
  <li><b>A</b>: alpha</li>\
  <li><b>B</b>: beta</li>\
  <li><b>C</b>: gamma</li>\
  </ul>',
  validation: {
    format: /^[A-C]{1}.[1-6]{1}$/
  },
  input: function(rule, name) {
    var $container = rule.$el.find('.rule-value-container');

    $container.on('change', '[name=' + name + '_1]', function() {
      var h = '';

      switch ($(this).val()) {
        case 'A':
          h = '<option value="-1">-</option> <option value="1">1</option> <option value="2">2</option>';
          break;
        case 'B':
          h = '<option value="-1">-</option> <option value="3">3</option> <option value="4">4</option>';
          break;
        case 'C':
          h = '<option value="-1">-</option> <option value="5">5</option> <option value="6">6</option>';
          break;
      }

      $container.find('[name$=_2]')
        .html(h).toggle(!!h)
        .val('-1').trigger('change');
    });

    return '\
  <select name="' + name + '_1"> \
  <option value="-1">-</option> \
  <option value="A">A</option> \
  <option value="B">B</option> \
  <option value="C">C</option> \
  </select> \
  <select name="' + name + '_2" style="display:none;"></select>';
  },
  valueGetter: function(rule) {
    return rule.$el.find('.rule-value-container [name$=_1]').val()
      + '.' + rule.$el.find('.rule-value-container [name$=_2]').val();
  },
  valueSetter: function(rule, value) {
    if (rule.operator.nb_inputs > 0) {
      var val = value.split('.');

      rule.$el.find('.rule-value-container [name$=_1]').val(val[0]).trigger('change');
      rule.$el.find('.rule-value-container [name$=_2]').val(val[1]).trigger('change');
    }
  }
  },
  {
  id: 'date',
  label: 'Datepicker',
  type: 'date',
  validation: {
    format: 'YYYY-MM-DD'
  },
  plugin: 'datepicker',
  plugin_config: {
    format: 'yyyy-mm-dd',
    todayBtn: 'linked',
    todayHighlight: true,
    autoclose: true,
    language: "tr"
  }
  }
]
*/
?>